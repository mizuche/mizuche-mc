# Mizuche Mc

Mizuche MCが再び動くようにするプロジェクト

## 0. intro

explain each component

- mizusim : GEANT4 simulation
- mizulib : Data libirary for MC
- analisis : sample analysis tool
- mizudaq : DAQ code

## 1. compile data library

```console
$ cd mizulib
$ make clean
$ make
```

## For MC

### 1. compile GEANT4 MC

```console
$ cd mizusim
$ make
```

### 2. start MC

You need to arrange the input.card as your study.

```console
// at the case of generating single particle MC
./bin/Linux-g++/T2KWC -c [input.card] -o [output.root] 

// at the case of using neut file.
./bin/Linux-g++/T2KWC -c [input.card] -o [output.root] -i [neutfile.root]
```

## For DAQ

### 1. compile DAQ

```console
$ cd mizudaq/src
$ make
```

### 2. start DAQ

