#ifndef _IREG_H_
#define _IREG_H_

#include <assert.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <Rtypes.h>
#include <Riostream.h>

const int mapsize = 16;

struct ireg_register {
    UShort_t	latch_low;	/* 0 */
    UShort_t	latch_high;	/* 2 */
    UShort_t	gate_low; 	/* 4 */
    UShort_t	gate_high;	/* 6 */
    UShort_t	through_low;	/* 8 */
    UShort_t	through_high;	/* A */
    UShort_t	csr;		/* C */
};

class ireg {
private:
    int fd;
    int size;
    int nbin;
    volatile ireg_register *mma;

    caddr_t vme_mapopen(off_t start, size_t size)
    {
	void *addr =
	    mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, start);
	assert(addr != MAP_FAILED);
	return (caddr_t)addr;
    };

public:

    ireg() : fd(-1), size(0), mma(NULL) { };

    ireg(const int start) :
	fd(open("/dev/vmedrv24d16", O_RDWR)), 
	size(mapsize),
	mma((ireg_register *)vme_mapopen(start, size))
    {
	assert(fd != -1);
	assert(mma != MAP_FAILED);
    };

    virtual ~ireg() {
	if (fd != -1) {
	    munmap((void *)mma, size);
	    close(fd);
	}
    };

    void Open(const int start) {
	assert(fd == -1);
	fd = open("/dev/vmedrv24d16", O_RDWR);
	size = mapsize;
	mma = (ireg_register *)vme_mapopen(start, size);
    };

    void Close() {
	assert(fd != -1);
	munmap((void *)mma, size);
	close(fd);
	fd = -1;
    };

    virtual void Clear() {
	assert(fd != -1);
	assert(mma != NULL);
	assert(mma != MAP_FAILED);
	mma->csr = 4;
    };

    virtual Bool_t IsTriggered() {
	assert(fd != -1);
	assert(mma != NULL);
	assert(mma != MAP_FAILED);
	return (mma->csr & 2);
    };

    virtual void Print(Option_t * opt = "") const {
	assert(fd != -1);
	assert(mma != MAP_FAILED);
	assert(mma != NULL);
	UShort_t * usp = (UShort_t *)mma;
	for (int i=0; i<8; i+=2) {
	    printf("%x %04x\n", i*2, usp[i]);
	}
    };


    UShort_t GetLatchLow() { return mma->latch_low; };
    UShort_t GetLatchHigh() { return mma->latch_high; };
    UShort_t GetThroughLow() { return mma->through_low; };
    UShort_t GetThroughHigh() { return mma->through_high; };
    UShort_t GetGateLow() { return mma->gate_low; };
    UShort_t GetGateHigh() { return mma->gate_high; };

};
#endif
