#include <iostream>
#include <unistd.h>

#include <Rtypes.h>
#include <Riostream.h>

#include "./ireg.h"

// VME-IREG
static const unsigned int IREG_BASEADDR = 0x010000;

int
main( int argc, char* argv[] )
{
  int flag = 0;
  if(argc > 1) {
    flag = atoi(argv[1]);
  }

  //
  ireg input_register(IREG_BASEADDR);
  input_register.Clear();

  UShort_t tag = 0;
  while( 1 ) {

    // read event
    if(input_register.IsTriggered()) {
      tag = input_register.GetLatchLow();
      system("date");
      std::cout << "->spill#=" << tag << std::endl;
      input_register.Clear();

    }
    sleep(2);

  }

  return( 0 );
}

