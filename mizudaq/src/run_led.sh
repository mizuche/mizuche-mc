#!/bin/sh

##
#trap 'killall run_beam.sh' 2
#trap 'killall beam_poll' 2

###
dir="/home/mizuche/exp/led/120817"
lastrun_text="$dir/lastrun.txt"

## read last run number
exec 3< $lastrun_text
read start 0<&3
start=`expr $start + 1`

#start=`expr 400000 + $1` ## runnum start from 400001 in MR Run40
#nrun=$1
nrun=5


#thr=-300
thr=-1000
ncyc=1000

irun=0
while [ $irun -lt $nrun ]
do
  echo Run$start Start
  echo $start > $lastrun_text
  
  ./beam_poll $dir $start $ncyc $thr

  irun=`expr $irun + 1`
  start=`expr $start + 1`

  sleep 0.5

done
