/*
 * tko_single_repeat.c,v 1.1 2002/09/04 Masato Shiozawa
 *          : TKO single action
 *            2002/9/4  modify tko_single.c
 */

/*
  #define BROADCAST_SINGLE
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "tkoprot.h"
#include "mydef.h"

int main( int argc, char **argv)
{
  int i;
  int tko_num, bc=0, ma, sa, fn, data;
  u_short sdata;
  int status;

  /* check argument */
  tko_num = (argc>1)? atoi(argv[1]) : 0;
  
  /* open TKO */
  TKO_open(1);
  
#ifdef BROADCAST_SINGLE
  printf("\nBC MA SA FN >? ");
  scanf("%d %d %d %d",&bc,&ma,&sa,&fn);
#else
  printf("\nMA SA FN >? ");
  scanf("%d %d %d",&ma,&sa,&fn);
#endif

  if (fn<8) {
    while (1) {
      printf("BC:%d MA:%d SA:%d FN:%d\n",bc,ma,sa,fn);
      status = TKO_single_act( tko_num, bc, ma, sa, fn, &sdata ); 
      printf("DATA:0x%04X, TOUT %d, Q %d\n",sdata,(status>>1)&1,status&1);
      usleep(100000);
    }
  }
  else {
    printf("DATA(hex) >? ");
    scanf("%x",&data);
    sdata = data;
    while (1) {
      printf("BC:%d MA:%d SA:%d FN:%d, DATA:0x%04X\n",bc,ma,sa,fn,sdata);
      status = TKO_single_act( tko_num, bc, ma, sa, fn, &sdata ); 
      printf("TOUT %d, Q %d\n",(status>>1)&1,status&1);
      usleep(100000);
    }
  }
  
  /* close TKO */
  TKO_close();
}

