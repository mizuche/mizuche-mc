#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "tkoprot.h"
#include "atmvar.h"
#include "gongvar.h"


const int n_used_smp = 1;


//
void read_tko_data_tesko()
{
  int i;
  int wdata[8000];
  int result,read_length;

  int high_bit,low_bit;
  int ma_num,sa_num;
  int tag;
  int word_num, event_num, ch,type,adc_data;

  // read memory data
  printf("Reading Data in SMP's memory (after sds)\n");
  result = TKO_read( wdata, &read_length, 1 );
  if (result<0) {
    printf("reading SMP is failed. (%d [bytes])\n", read_length);
  }
  else {
    printf("reading SMP is completed. (%d [bytes])\n", read_length);
  }
  // 1 word = 4 byte = 32 bit ???


  if( read_length/4<8000 ) {

    for (i=0;i<read_length/4;i++) {

      high_bit = (wdata[i]>>16)&0xFFFF;
      low_bit = wdata[i]&0xFFFF;

      ma_num = (high_bit>>11)&0x1F;
      sa_num = high_bit&0x7FF;

      tag = (low_bit>>14)&0x3;

      event_num = (low_bit>>6)&0xFF;
      type = (low_bit>>5)&0x1;
      ch = low_bit&0xF;
      adc_data = low_bit&0x0FFF; 
      
      printf("%dth  0x%.8x = high:0x%.4x (ma:%d,sa:%d), low:0x%.4x (tag:%d)",
              i,wdata[i],high_bit,ma_num,sa_num,low_bit,tag);

      if( i==0 ) {
        printf(" -> word#: %d\n",low_bit);
      }
      else {
        if( ma_num>0 && ma_num<24 ) {
          if(tag==0) 
            printf(" -> event#: %d, type: %d, ch: %d\n",event_num, type, ch);

          else if(tag==1) 
            printf(" -> TDC : %d\n",adc_data);

          else if(tag==2) 
            printf(" -> QDC : %d\n",adc_data);

        }
        else printf("\n");
      }
    }

  }

  else {
    printf("Read data length longer than expectation (%d)\n",8000);
  }
};


////
int main( void )
{
  int ismp,ievent;
  u_short sdata;
  int broad_cast = 1;

  // TKO Initialization
  TKO_open(n_used_smp);

  // set config for operation mode
  printf("Start to operation mode ...\n");
  for (ismp=0; ismp<n_used_smp; ismp++) {

    // init all ATMs
    printf("initialize all ATMs conneting to smp#%d ...\n",ismp);
    TKO_single_act( ismp, broad_cast, 1, 0, ATM_INITIALIZE_F, &sdata );

    sdata = ATM_OPERATE_MODE;
    printf("Start AMT operation mode ...\n");
    TKO_single_act( ismp, broad_cast, 1, 0, ATM_WRITE_MODE_F, &sdata );
    TKO_single_act( ismp, broad_cast, 1, 0, ATM_FIFO_CLEAR_F, &sdata );

    // init GONG
    sdata = GONG_OPERATE_MODE;
    printf("Start GONG operation mode ...\n");
    TKO_single_act( ismp, 0, GONG_MA, GONG_SA, GONG_WRITE_MODE_F, &sdata );

    // Set HIT threshold (range in 0 ~ -1240 mV)
    // this threshold value corresponds to input pulse height * 100.
    //sdata = ATM_THRDAC(-400.);  // means 4mV threshold to input signal
    sdata = ATM_THRDAC(-900.);
    printf("Set HIT Threshold : 0x%x\n",sdata);
    TKO_single_act( ismp, broad_cast, 1, 0, ATM_WRITE_THRDAC_F, &sdata );

  }

  // wait for ATM come to be stable
  sleep(1);

/*
  // start polling (wait for accept signal)
  for (ievent=1; ievent<100; ievent++) {
    printf("start %dth waitevent ...\n", ievent);

    for (ismp=0; ismp<n_used_smp; ismp++) {

      if( TKO_sds_accept( ismp )==0 ) {
        read_tko_data_tesko();
        usleep(100);
      }

    }

  } // end of event loop
*/

  TKO_close();
  printf("TKO closed!\n");
};
