/*
 * trglib.c,v 1.1 1994/11/12 Masato Shiozawa
 *
 *     v 1.1: 1994/11/12
 *            library source file for a VME-TRG.
 *            first definition by Masato SHiozawa(ICRR)
 *     v 1.2: 1994/12/13
 *            TRG_Read is changed to be done by direct access.
 *     v 1.2: 1995/5/23
 *            way of access using read()/write() is supported(not completed).
 *     v 1.2: 1995/5/24
 *            additional routine setting AM code directly for each access.
 */

/*
#define SOL2
#define PCIBUS
#ifdef PCIBUS
#define BT945
#endif
*/

#include "trglib.h"
#include "trgprot.h"
#include <errno.h>

#define min(A,B) ((A<B)?A:B)

int trg_fd;
caddr_t trg_vme_base;
TRGREG *trg;

//////////////////
int TRG_open(void)
{
  printf("TRG base address: 0x%.8x\n",TRG_BASE);

  if ((trg_fd = open("/dev/vmedrv16d16", O_RDWR)) < 0 ) {
    perror("ERROR: Could not open device /dev/vmedrv16d16");
    return(-1);
  }

  /* Mapping onto TRG */
  trg_vme_base = mmap(NULL, TRG_MAP_SIZE, PROT_READ | PROT_WRITE
		      , MAP_SHARED, trg_fd, TRG_BASE);

  if (trg_vme_base == (caddr_t)-1) {
    perror("Mapping: ");
    return(-1);
  }
  trg = (TRGREG *)trg_vme_base;
  //#ifdef DEBUG
  printf("trg = 0x%08X\n", (u_int)trg);
  //#endif //DEBUG

  /* TRG reset */
  u_short sdata = TRG_TRGRST;
  TRG_write( 0, &sdata );
    
  return(0);
}

//////////////////
int TRG_Open(void)
{
  return(TRG_open());
}

///////////////////
int TRG_close(void)
{
  //if (munmap((void*)TRG_BASE, (size_t)TRG_MAP_SIZE)!=0) {
  if (munmap((void*)trg_vme_base, (size_t)TRG_MAP_SIZE)!=0) {
    perror("Unmapping: ");
    return(-1);
  }

  return (close(trg_fd));
}

///////////////////
int TRG_Close(void)
{
  return(TRG_close());
}


int TRG_read( int offset, u_short *sdata )
{
  if (lseek( trg_fd, (TRG_BASE+offset*2), SEEK_SET) == -1) {
    perror("TRG_read: Could not lseek() successfully");
    return(-1);
  }
  errno = 0;
  int ret = read(trg_fd, sdata, 2);
  if (ret!=2) {
    fprintf(stderr,
	    "TRG_read: read failed. offset = %d, return code = %d\n",
	    offset, ret);
    fprintf(stderr,
	    "TRG_read: errno = %d (%s)\n", errno, strerror(errno));
    return(-1);
  }
  return(0);
}

int TRG_write( int offset, u_short *sdata )
{
  if (lseek(trg_fd, (TRG_BASE+offset*2), SEEK_SET)==-1) {
    perror("TRG_write: Could not lseek() successfully");
    return(-1);
  }
  errno = 0;
  int ret = write( trg_fd, sdata, 2);
  if (ret!=2) {
    fprintf(stderr,
	    "TRG_write: write failed. offset = %d, return code = %d\n",
	    offset, ret);
    fprintf(stderr,
	    "TRG_write: errno = %d (%s)\n", errno, strerror(errno));
    return(-1);
  }
  return(0);
}

int TRG_command( u_short value )
{
  return( TRG_write( 0, &value ) );
}

int TRG_Command( u_short value )
{
  return( TRG_command( value ) );
}

u_short TRG_status( void )
{
  u_short status;
  TRG_read( 0, &status );
  return( status );
}

u_short TRG_Status( void )
{
  return( TRG_status() );
}

int TRG_mask( u_short value )
{
  u_short mask;
  TRG_write( 1, &value );
  TRG_read( 1, &mask );
  if (mask != value) {
    fprintf(stderr, "Could not set mask bits.\n");
    return(-1);
  }
  return(0);
}

int TRG_Mask( u_short value )
{
  return( TRG_mask( value ));
}

u_short TRG_FIFO_count( void )
{
  u_short value;
  TRG_read( 2, &value );
  return( (value&0x1FFF) );
}

u_short TRG_FIFO_Count( void )
{
  return(TRG_FIFO_count());
}

u_int TRG_Read( TRG *trg_fifo, u_int max_event )
{
  int i;

  u_short event = trg->fifo_count;
  max_event = min( max_event, event );
  
  if (event<2000) {
    for (i=0;i<(int)max_event;i++) {
      trg_fifo->fifo_a = trg->fifo_a;
      trg_fifo->fifo_b = trg->fifo_b;
      trg_fifo->fifo_c = trg->fifo_c;
      trg_fifo->fifo_d = trg->fifo_d;
      trg_fifo->fifo_e = trg->fifo_e;
      trg_fifo++;
    }
  }
  else {
    /* Dead Time recording mode OFF */
    u_short save_csr = trg->csr&0x0003;
    trg->csr = save_csr & ~TRG_TRGENA;
    for (i=0;i<(int)max_event;i++) {
      trg_fifo->fifo_a = trg->fifo_a;
      trg_fifo->fifo_b = trg->fifo_b;
      trg_fifo->fifo_c = trg->fifo_c;
      trg_fifo->fifo_d = trg->fifo_d;
      trg_fifo->fifo_e = trg->fifo_e;
      trg_fifo++;
    }
    
    /* Dead Time recording mode ON */
    trg->csr = save_csr;
  }
  
  return( (u_int)max_event );
}
