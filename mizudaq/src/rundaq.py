#! /usr/bin/env python
# -*- coding : utf-8 -*-

import os, sys
import time
from optparse import OptionParser

filename = __file__[2:]

class Daq(object):
    ## __________________________________________________
    def __init__(self, dir, nevent, thr, debug = False):
        self.setDir(dir)
        self.setNevent(nevent)
        self.setAtmThr(thr)
        self.debug = debug
        if debug:
            self.txt = os.path.join('exp/dump', 'lastrunfordebug.txt')
        else:
            self.txt = os.path.join(dir, 'lastrun.txt')

    ## __________________________________________________
    def setDir(self, dir):
        self.dir = dir

    ## __________________________________________________
    def setNevent(self, nevent):
        self.nevent = nevent

    ## __________________________________________________
    def setAtmThr(self, thr):
        self.thr = thr

    ## __________________________________________________
    def getRunStart(self):
        ifn = self.txt
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.stderr.write('\tDo you want to make new one ? > (y/[n])')
            ans = raw_input('')
            if ans in ['y', 'Y']:
                self.writeLastRun(0)
            sys.exit(-1)

        sys.stderr.write('[%s]\tRead "%s".\n' % (filename, ifn))
        with open(ifn, 'r') as fin:
            lines = fin.readlines()
        return int(lines[0])
    
    ## __________________________________________________
    def writeLastRun(self, run = 0):
        ofn  = self.txt
        line = '%d\n' % run
        sys.stderr.write('[%s]\tWrite "%s".\n' % (filename, ofn))
        with open(ofn, 'w') as fout:
            fout.write(line)
            
    ## __________________________________________________
    def run(self):
        d = self.dir
        n = self.nevent
        t = self.thr
        s = self.getRunStart() + 1

        argv = {'dir':d, 'nevent':n, 'atmthr':t, 'start':s}
        com = './beam_poll %(dir)s %(start)d %(nevent)d %(atmthr)d' % argv
        
        try:
            sys.stderr.write('[%s]\tRun "%s".\n' % (filename, com))
            r = os.system(com)
            self.writeLastRun(s)
            if (r==-1): sys.exit(-1)
        except KeyboardInterrupt:
            sys.stderr.write('[%s]\t"Ctrl-C" was pressed. DAQ stopped.\n' % filename)
            self.writeLastRun(s)
            sys.exit(-1)
        except:
            sys.stderr.write('[%s]\tUnknown failure. DAQ stopped.\n' % filename)
            sys.exit(-1)

    ## __________________________________________________
    def loop(self, nloop):
        try:
            for i in range(0, nloop):
                self.run()
                #if self.debug:
                sys.stderr.write('[loop]\tsleep(60*30sec)\n')
                time.sleep(60*30)
        except KeyboardInterrupt:
            print '"Ctrl-C" was pressed. DAQ stopped.'
            sys.exit(-1)
            
## __________________________________________________        
if __name__ == '__main__':

    usage = 'Usage:\t%s [-hbnclqd (date)] loop#' % sys.argv[0]

    parser = OptionParser(usage)

    parser.add_option('-b', '--beam',
                      dest   = 'beam',
                      help   = 'Run DAQ in beam measurment mode')
    parser.add_option('-n', '--noise',
                      dest   = 'noise',
                      help   = 'Run DAQ in noise measurement mode')
    parser.add_option('-c', '--cosmic',
                      dest   = 'cosmic',
                      help   = 'Run DAQ in cosmic measurement mode')
    parser.add_option('-l', '--led',
                      dest   = 'ledcalib',
                      help   = 'Run DAQ in LED calibration mode')
    parser.add_option('-q', '--qt',
                      dest   = 'qtcalib',
                      help   = 'Run DAQ in QT calibration mode')
    parser.add_option('-f', '--fadc',
                      dest   = 'fadc',
                      help   = 'Run DAQ in FADC test mode')
    parser.add_option('-p', '--ped',
                      dest   = 'ped',
                      help   = 'Run DAQ in pedestal only')
    parser.add_option('-t', '--thrd',
                      dest   = 'thrd',
                      help   = 'set ATM hit threshold.')
    parser.add_option('-d', '--debug',
                      action = 'store_true',
                      dest   = 'debug',
                      help   = 'dry run for DEBUG. DAQ does not run.')

    parser.set_defaults(beam     = False,
                        noise    = False,
                        cosmic   = False,
                        ledcalib = False,
                        qtcalib  = False,
                        fadc     = False,
                        ped      = False,
                        debug    = False,
                        thrd     = None)
                        
    (options, args) = parser.parse_args()

    if len(args) > 1:
        sys.stderr.write('Warning:\tToo many arguments (%d)\n' % len(args))
        print usage
        sys.exit(-1)
    
    l = 1
    if len(args) > 0:
        l = int(args[0])
        
    if options.beam:
        s = 'Beam data'
        d = 'exp/beam/'
        d = os.path.join(d, options.beam)
        n = 1000
        t = -300
    elif options.noise:
        s = 'Noise'
        d = 'exp/noise/'
        d = os.path.join(d, options.noise)
        n = 1000
        t = -300
        #t = -200
        #t = -250
    elif options.ledcalib:
        s = 'LED calibration'
        d = 'exp/led/'
        d = os.path.join(d, options.ledcalib)
        n = 100
        t = -300
    elif options.qtcalib:
        s = 'QT calibration'
        d = 'exp/qtcalib/'
        d = os.path.join(d, options.qtcalib)
        n = 100
        t = -300
    elif options.cosmic:
        s = 'Cosmic'
        d = 'exp/cosmic/'
        d = os.path.join(d, options.cosmic)
        n = 100
        t = -300
    elif options.fadc:
        s = 'FADC'
        d = 'exp/fadc/'
        d = os.path.join(d, options.fadc)
        n = 100
        t = -300
    elif options.ped:
        s = 'PED'
        d = 'exp/ped/'
        d = os.path.join(d, options.ped)
        n = 100
        t = -300        
    else:
        s = 'dump'
        d = 'exp/dump/'
        n = 1000
        t = -300

    if options.thrd:
        t = int(options.thrd)
        
    daq = Daq(d, n, t, debug = options.debug)
    srun = daq.getRunStart()

    sys.stderr.write('[%s]\tDAQ mode      : "%s"\n'   % (filename, s))
    sys.stderr.write('[%s]\tSave data to  : "%s"\n'   % (filename, d))
    sys.stderr.write('[%s]\tATM threshold : %4d mV\n' % (filename, t))
    sys.stderr.write('[%s]\t# of events   : %4d events / run\n'    % (filename, n))
    sys.stderr.write('[%s]\t# of runs     : %4d runs\n'    % (filename, l))
    sys.stderr.write('[%s]\tStart run from: %4d\n'    % (filename, srun+1))

    sys.stderr.write('[%s]\tIs parameter above correct ? (y/[n]) > ' % (filename))
    ans = raw_input('')
    if len(ans) > 1: ans = ans[0]
    if ans in ['y', 'Y']:
        try:
            daq.loop(l)
        except KeyboardInterrupt:
            print '"Ctrl-C" was pressed. DAQ stopped.'
            sys.exit(-1)
    else:
        sys.stderr.write('\tQuit. Bye Bye.\n')
        sys.exit(-1)
        
