/*
 * tkolib.c,v 1.6 1995/2/5 Masato Shiozawa
 *     library source for new VME-SMP and TKO system
 *
 *     v 1.1: 1994/5/9
 *            first definition by Masato SHiozawa(ICRR)
 *     v 1.2: 1994/6/1
 *            signal block is added around code enabling interrupt.
 *     v 1.3: 1994/11/23
 *            Arguments for TKO_open() is changed.
 *            Arguments for TKO_read() is changed.
 *     v 1.4: 1994/12/13
 *            Reading SMP register routine(SMP_reg_read) id added.
 *     v 1.5: 1995/1/11
 *            Add terminator(FFFFFFFF) in the case of 0 read length
 *            in TKO_read().
 *     v 1.6: 1995/2/5
 *            test read function TKO_test_read() is added.
 *     v 1.6: 1995/3/9
 *            test write function TKO_test_write() is added.
 *     v 1.7: 1995/3/12
 *            setting switch mask function SMP_Switch_Mask() is added.
 *     v 1.8: 1995/5/9
 *            return code 0 is added in TKO_open function.
 *     v 2.0: 1995/9/15
 *            New function of masking some SMPs is achieved.
 *            Masking TKO-open TKO_select_open() is added.
 *     v 2.1: 1995/9/21
 *            munmap is added in TKO_close() function.
 *     v 2.2: 1995/9/26
 *            when SMP memory is full, read length(data_size[entry]) is changed
 *            to SMP_MEM_SIZE-4.
 *     v 2.3: 1996/2/19
 *            New function 'SMP_TKO_memory_full()' is added.
 *     v 2.4: 1996/3/7
 *            argument of sigprocmask is changed from SIG_BLOCK to SIG_SETMASK.
 *     v 2.5: 1996/3/9
 *            TKO_buffer_switch routine is added.
 *     v 2.6: 1996/4/11
 *            all printf can be masked by macro variable 'MESSAGE'
 *     v 2.7: 1996/8/28
 *            SMP_scount_Clear is added.
 *     v 2.8: 1996/9/21
 *            TKO_program_select_all is added.
 *     v 3.0: 2001/5/25
 *            confirmed with PRIMEPOWER200(Solaris 2.6)
 *     v 4.0: 2003/1/27
 *            sigsuspend was replaced by sigwait
 *
 */

#include <errno.h>
#include <string.h>
#include <asm/page.h>
#include "smpreg.h"
#include "tkolib.h"
#include "tkoprot.h"
#include "mydef.h"
#include "vmedrv.h"

#define DEBUG
//#define mysleep

///////////////////////////
int TKO_open(int smp_entry)
{
  SMP_Entry = ( smp_entry <= SMP_MAX_ENTRY )? smp_entry : SMP_MAX_ENTRY;
  SMP_select_Entry = SMP_Entry;
  SMP_mask = 0;
  intr_generator = INTR_GENERATOR;
    
  event_number_thr = (EVENT_NUMBER_THR<SMP_MAX_EVENT_NUMBER_THR)? 
    EVENT_NUMBER_THR : SMP_MAX_EVENT_NUMBER_THR;
  data_size_thr    = (DATA_SIZE_THR<SMP_MAX_DATA_SIZE_THR)?
    DATA_SIZE_THR : SMP_MAX_DATA_SIZE_THR;
  
  event_number_mask = EVENT_NUMBER_MASK;
  data_size_mask    = DATA_SIZE_MASK;

  if (TKO_bit3_initialize()!=0) {
    fprintf(stderr,"Cannot initialize BIT3.\n");
    return(-1);
  }
  if (smp_map()!=0) {
    fprintf(stderr,"Cannot mapping smp.\n");
    return(-1);
  }
  if (TKO_signal_setup()!=0) {
    fprintf(stderr,"Cannot setup signals.\n");
    return(-1);
  }

  return(0);
}

//////////////////////////////////////////////
int TKO_select_open(int smp_entry, u_int mask)
{
  int i,j,k;
  
  SMP_Entry = ( smp_entry <= SMP_MAX_ENTRY )? smp_entry : SMP_MAX_ENTRY;
  SMP_mask = mask;
  SMP_select_Entry=0;
  for (i=smp_entry-1;i>=0;i--) {
    if (SMP_SELECTED(i)) {
      SMP_select_Entry++;
      intr_generator = i;
    }
  }
  if (SMP_select_Entry<1) {
#ifdef MESSAGE
    printf("No SMP is selected.\n");
#endif //MESSAGE
    return(-1);
  }
  event_number_thr = (EVENT_NUMBER_THR<SMP_MAX_EVENT_NUMBER_THR)? 
    EVENT_NUMBER_THR : SMP_MAX_EVENT_NUMBER_THR;
  data_size_thr    = (DATA_SIZE_THR<SMP_MAX_DATA_SIZE_THR)?
    DATA_SIZE_THR : SMP_MAX_DATA_SIZE_THR;
  
  event_number_mask = EVENT_NUMBER_MASK;
  data_size_mask    = DATA_SIZE_MASK;

  if (TKO_bit3_initialize()!=0) {
    fprintf(stderr,"Cannot initialize BIT3.\n");
    return(-1);
  }
  if (smp_map()!=0) {
    fprintf(stderr,"Cannot mapping smp.\n");
    return(-1);
  }
  if (TKO_signal_setup()!=0) {
    fprintf(stderr,"Cannot setup signals.\n");
    return(-1);
  }

  return(0);  
}

/////////////////////////
int TKO_bit3_initialize()
{
  if ((tko_vme_fd = open("/dev/vmedrv32d32", O_RDWR)) < 0 ) {
    //if ((tko_vme_fd = open("/dev/vmedrv32d32dma", O_RDWR)) < 0 ) {
    perror("ERROR: Could not open device");
    return(-1);
  }

  vmedrv_interrupt_property_t interrupt_property;
  interrupt_property.irq = 3;
  interrupt_property.vector = 0xff78;
  interrupt_property.signal_id = TKO_USESIG;

  ioctl(tko_vme_fd,VMEDRV_IOC_REGISTER_INTERRUPT,&interrupt_property);
  //ioctl(tko_vme_fd,VMEDRV_IOC_ENABLE_INTERRUPT);

  return(0);
}

/////////////
int smp_map()
{
  int entry;

  // map VME A32 space
  if (SMP_Entry!=0) {
    u_int map_size = SMP_MAP_SIZE * SMP_Entry;
    u_int base_address = smp_base_address[0];
    
    fprintf(stderr,"map_size     = x%.8x\n",map_size);
    fprintf(stderr,"base_address = x%.8x\n",base_address);

    tko_vme_base = mmap(NULL, map_size, PROT_READ | PROT_WRITE
			, MAP_SHARED, tko_vme_fd, base_address);

    if (tko_vme_base == (caddr_t)-1) {
      fprintf(stderr, "Can't map SMP: base_address = 0x%.8x\n",
	      smp_base_address[0]);
      return(-1);
    }
    
    printf("tko_vme_base = 0x%08X\n",tko_vme_base);
    
    for (entry=0;entry<SMP_Entry;entry++) {
      smp[entry] = (SMP *)(tko_vme_base+SMP_MAP_SIZE * entry);
#ifdef DEBUG
      printf("smp[%d] = 0x%08X\n", entry, (u_int)smp[entry]);
#endif //DEBUG
    }
  }

  // initialize SMP board
  SMP_All_Reset();

  return(0);
}

//////////////////////
int TKO_signal_setup()
{
  errno = 0;
  signal(SIGINT, TKO_process_end);
  if (errno > 0) {
    perror("Could not set up signals\n");
    return(errno);
  }
  signal( SIGALRM, SIG_IGN );
  signal( TKO_USESIG, SIG_IGN );

  if (sigprocmask( 0, NULL, &tko_blockmask) < 0) {
    fprintf(stderr,"sigprocmask() error\n");
    return(-1);
  }
  sigaddset( &tko_blockmask, TKO_USESIG );
  sigaddset( &tko_blockmask, SIGALRM );
 
  if (sigprocmask( 0, NULL, &tko_waitmask) < 0) {
    fprintf(stderr,"sigprocmask() error\n");
    return(-1);
  }
  sigdelset( &tko_waitmask, TKO_USESIG );
  sigdelset( &tko_waitmask, SIGALRM );

  return(0);
}

///////////////
int TKO_close()
{
  u_int map_size = SMP_MAP_SIZE * SMP_Entry;
  if (munmap((void*)tko_vme_base, (size_t)map_size)!=0) {
    perror("Unmapping: ");
    return(-1);
  }

#ifdef DEBUG
  printf("TKO closed.\n");
#endif //DEBUG

  return (close(tko_vme_fd));
}

/////////////////////////////
void TKO_int_process(int sig)
{ 
#ifdef DEBUG
  printf("interrupt received!!!!!\n");
  printf("signal num. = %d\n",sig);
#endif //DEBUG

  // clean signal set & timeout alarm
  alarm(0);
  signal( SIGALRM, SIG_IGN );
  signal( TKO_USESIG, SIG_IGN );
  
  tko_signal_number = sig;
}

////////////////////////////
void TKO_alarm_hdlr(int sig)
{
#ifdef DEBUG
  fprintf(stderr,"timeout alarm occured.\n");
#endif //DEBUG

  // clean signal set & timeout alarm
  alarm(0);
  signal( SIGALRM, SIG_IGN );
  signal( TKO_USESIG, SIG_IGN );

  tko_waitevent_timeout = 1;
}

////////////////////////////
void TKO_dummy_hdlr(int sig)
{
#ifdef DEBUG
  fprintf(stderr,"TKO_dummy_hdlr is called.\n");
#endif //DEBUG
}

/////////////////////////////
void TKO_process_end(int sig)
{
#ifdef DEBUG
  printf("\nstop by interrupt\n");
#endif //DEBUG

  TKO_close();
  exit(0);
}

///////////////////////////////////////////////////////////////////
int TKO_single_act(int tko_num, int tko_bc, int tko_ma, int tko_sa,
		   int tko_fn, u_short *tko_data ) 
{
  u_int vme_return, status;
  
#ifdef DEBUG
  if (tko_num>=SMP_Entry) {
    fprintf(stderr,"TKO_single_act: invarid tko number rquested. #%d(max #%d)\n"
	    , tko_num, SMP_Entry);
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
    fprintf(stderr,"TKO_single_act: masked tko number requested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
    return(-1);
  }

  printf("smp: %08X\n", (u_int)smp[tko_num]);
  printf("bc %d, ma %d, sa %d, fn %d, data %04X\n"
	 ,tko_bc, tko_ma, tko_sa, tko_fn, *tko_data);
#endif //DEBUG

  smp[tko_num]->reg.csr = (smp[tko_num]->reg.csr&0x1F)|((tko_fn&0xF)<<7);
  if (tko_fn<8) {
    vme_return = smp[tko_num]->tko.bc[tko_bc].ma[tko_ma].sa[tko_sa];
    *tko_data = (u_short)(vme_return&0xFFFF);
  }
  else {
    smp[tko_num]->tko.bc[tko_bc].ma[tko_ma].sa[tko_sa] 
      = (u_int)(*tko_data);
  }
    
  status = smp[tko_num]->reg.csr;
  return((SMP_TKO_TOUT(status)<<1)|SMP_TKO_Q(status));
}

////////////////////////////////////////////////
int TKO_program_select(int tko_num, int program)
{
  int i;
  u_int  csr, csr_ret;

#ifdef DEBUG
  if (tko_num>=SMP_Entry) {
    fprintf(stderr,"TKO_program_select: invarid tko number rquested. #%d(max #%d)\n"
	    , tko_num, SMP_Entry);
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
    fprintf(stderr,"TKO_program_select: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
    return(-1);
  }
#endif //DEBUG
  csr = ( smp[tko_num]->reg.csr & 0xFF80 )|( program & 0x1F );
  smp[tko_num]->reg.csr = csr;
  csr_ret = smp[tko_num]->reg.csr;

#ifdef DEBUG
  if ((csr&0x1F)!=(csr_ret&0x1F)) {
    fprintf(stderr,"TKO_program_select: Could not set program number.\n");
    return(-1);
  }
#endif //DEBUG

  return(0);
}

///////////////////////////////////////
int TKO_program_select_all(int program)
{
  int tko_num;
  u_int  csr[SMP_MAX_ENTRY];
  
  /* this routine should be fast because switching all SMP at almost same time
     to be enabled is needed for SK.
     added by M.Shiozawa(Sep.-21-96) */
  for (tko_num=0;tko_num<SMP_Entry;tko_num++) {
    if (SMP_SELECTED(tko_num)!=0) {
      csr[tko_num] = ( smp[tko_num]->reg.csr & 0xFF80 )|( program & 0x1F );
    }
  }
  for (tko_num=0;tko_num<SMP_Entry;tko_num++) {
    if (SMP_SELECTED(tko_num)!=0) {
      smp[tko_num]->reg.csr = csr[tko_num];
    }
  }
  
  return(0);
}


int TKO_sds_go( int tko_num, int program )
{
  u_int status, new_status;
  int busy;

#ifdef DEBUG
  if (tko_num>=SMP_Entry) {
    fprintf(stderr,"TKO_sds_go: invarid tko number rquested. #%d(max #%d)\n"
	    , tko_num, SMP_Entry);
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
    fprintf(stderr,"TKO_sds_select: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
    return(-1);
  }
#endif //DEBUG

  /* select SMP SDS program (PSEL) */
  TKO_program_select( tko_num, program );

  // This polling seems not to be necessary ...
  // At this timing, only set PSEL register
  while (1) {
    busy = SMP_SQ_BUSY(smp[tko_num]->reg.csr);
    if (busy==0)
      break;
#ifdef DEBUG
    else
      printf("TKO_sds_go : wait %d\n",busy);

#ifdef mysleep
    sleep(1); 
#endif

#endif //DEBUG
  }

  // Run SMP Sequence
  status = smp[tko_num]->reg.csr;
  new_status = (status|SMP_SDS_GO)&~SMP_BOARD_RESET;
#ifdef DEBUG
  printf("before go: status     %08X\n",status);
  printf("before go: new_status %08X\n",new_status);
#endif //DEBUG
  smp[tko_num]->reg.csr = new_status;

  return(0);
}    


int TKO_sds_accept( int tko_num )
{

#define SDS_ACCEPT_TRG 0x00
  const int wait_usec = 10000;
  const int wait_timeout = wait_usec * 500; // means 5 sec

  u_int before_sncr, delta_sncr;
  int busy,i;

#ifdef DEBUG
  if (tko_num>=SMP_Entry) {
    fprintf(stderr,"TKO_sds_go: invarid tko number rquested. #%d(max #%d)\n"
	    , tko_num, SMP_Entry);
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
    fprintf(stderr,"TKO_sds_select: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
    return(-1);
  }
#endif //DEBUG

  /* select SMP SDS program (PSEL) */
  // Enable ACCEPT signal for SDS start
  TKO_program_select( tko_num, SDS_ACCEPT_TRG );

  while (1) {
    busy = SMP_SQ_BUSY(smp[tko_num]->reg.csr);
    if (busy==0)
      break;

#ifdef DEBUG
    else
      printf("TKO_sds_go : wait %d\n",busy);

#ifdef mysleep
    sleep(1); 
#endif

#endif //DEBUG

  }

  // Clear serial number counter
  //SMP_scount_Clear(tko_num); 
  // -> comment out for check serial # in output header data

  before_sncr = smp[tko_num]->reg.sncr;

  printf("Start to wait for accept signal in\n");
  i=0;
  while(1) {

    delta_sncr = smp[tko_num]->reg.sncr - before_sncr;

    // wait for incriment of total # of SDS process
    if( delta_sncr>0 ) {
      printf("At least One time SDS complete!\n");
      break;
    }
    else if( delta_sncr<0 ) {
      printf("** Delta Serial Number Counter Register < 0 ! **\n");
      return(-1);
    }

    usleep(wait_usec);
    i++;

#ifdef DEBUG
    if( i%50==0 ) printf("...waiting for accept signal\n");
#endif

    if( i>wait_timeout ) {
      printf("TIMEOUT of waiting for accept signal\n");
      return(-1);
    }

  }

  return(0);

}


//////////////////////////////////
int TKO_waitevent(int tko_timeout)
{
  u_int data;
  int   wait_timeout;
  int   entry;
  u_int save_int_num;
  sigset_t tko_savemask;
  int sigret;
  int signo;

  wait_timeout = (tko_timeout<=0)? TKO_TIMEOUT : tko_timeout;

  /* -- SMP setup -- */
  smp[intr_generator]->reg.ivr = SMP_INT_VECTOR;

  u_int inv = smp[intr_generator]->reg.ivr;  
  if ((inv&0xff) != SMP_INT_VECTOR) {
    perror("Could not set up a interrupt vector.\n");
    return(-1);
  }

  /* -- block signals -- */
  if (sigprocmask( SIG_SETMASK, &tko_blockmask, &tko_savemask) < 0 ) {
    perror("sigprocmask() error\n");
    return(-1);
  }

  tko_vector_received = 0;
  tko_waitevent_timeout = 0;
  errno = 0;
  signal(TKO_USESIG, TKO_int_process);
  if (errno > 0) {
    perror("Could not set up signals\n");
    return(-1);
  }
  signal(SIGALRM, TKO_alarm_hdlr);
  if (errno > 0) {
    perror("Could not set up signals\n");
    return(-1);
  }

  /* -- enable interrupt -- */
  smp[intr_generator]->reg.bcr 
    = (~SMP_SWITCH_ENABLE)&smp[intr_generator]->reg.bcr;

  data = ( SMP_IRQ_LEVEL3 | SMP_INT_AUTO_CLR | SMP_INT_ENABLE
	   | SMP_INT_FLG_AUTO_CLR | SMP_INT_FLAG );
  smp[intr_generator]->reg.icr = data;

  // set threshold of stored event# or data size
  data = ( SMP_SWITCH_AUTO_CLR | SMP_SWITCH_ENABLE
	   | SMP_SWITCH_FLG_AUTO_CLR | SMP_SWITCH_FLAG 
	   | event_number_mask | data_size_mask);
  for (entry=0;entry<SMP_Entry;entry++) {
    if (SMP_SELECTED(entry)) {
      smp[entry]->reg.scr = event_number_thr | data_size_thr;
      smp[entry]->reg.bcr = data;
    }
  }

  ioctl(tko_vme_fd,VMEDRV_IOC_ENABLE_INTERRUPT);


  /* -- waiting interrupt -- */
  alarm(wait_timeout);
  if ((sigwait( &tko_blockmask, &sigret )) == -1 ) {
    perror("sigwait() error.\n");
    alarm(0);
    return(-1);
  }
  alarm(0);

  ioctl(tko_vme_fd,VMEDRV_IOC_DISABLE_INTERRUPT);

  if (sigret==TKO_USESIG) {
    TKO_int_process(sigret);
  }
  else if (sigret==SIGALRM) {
    TKO_alarm_hdlr(sigret);
  }
  else {
    fprintf(stderr,"sigwait() error. unexpected returned siganl=%d.\n",sigret);
    return(-1);
  }
    
  if (sigprocmask( SIG_SETMASK, &tko_savemask, NULL) < 0 ) {
    fprintf(stderr,"sigprocmask() error\n");
    return(-1);
  }

  if (tko_waitevent_timeout==1) {
    return(-2);
  }

  return(0);
}

u_int TKO_buffer_size( void )
{
  int entry;
  u_int total=0, data_size;
  
  for (entry=0;entry<SMP_Entry;entry++) {
    if (SMP_SELECTED(entry)) {
      data_size = smp[entry]->reg.dsr;
      if ((data_size&0x80000000)==0x80000000)
        data_size = SMP_MEM_SIZE;
      total+=data_size;
    }
  }

  return(data_size);
}


u_int TKO_read( u_int *ptr, u_int *total_length, int switch_option )
{
  int i,j,k;
  int entry, busy, buffer[2][SMP_MAX_ENTRY];
  u_int data_size[SMP_MAX_ENTRY], read_length;
  u_int data, data1;
  int error=0;
  int try=0;
  
  if (switch_option==1) {
    while (1) {
      busy=0;
      for (entry=0;entry<SMP_Entry;entry++)
        if (SMP_SELECTED(entry))
          busy = busy + SMP_SQ_BUSY(smp[entry]->reg.csr);
        if (busy==0)
          break;
#ifdef DEBUG
        else
          printf("TKO_read : wait %d\n",busy);
#endif //DEBUG
      }

    for (i=0;i<SMP_Entry;i++) {
      if (SMP_SELECTED(i)) {
        buffer[0][i] = SMP_CURRENT_BUFFER(smp[i]->reg.bcr);
#ifdef DEBUG
        printf("%d ",buffer[0][i]);
#endif //DEBUG
      }
    }
#ifdef DEBUG
    printf("\n");
#endif //DEBUG
    
    data = ( SMP_SWITCH_AUTO_CLR | SMP_SWITCH_ENABLE
	     | SMP_SWITCH_FLG_AUTO_CLR | SMP_SWITCH_FLAG 
	     | SMP_EVENTNUM_SWITCH_MASK | SMP_DATASIZE_SWITCH_MASK);

    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry))
        smp[entry]->reg.bcr = data;
    }

    /* -- buffer switch -- */
    smp[intr_generator]->reg.bcr 
      = smp[intr_generator]->reg.bcr | SMP_SWITCH_EXEC;

    for (i=0;i<SMP_Entry;i++) {
      if (SMP_SELECTED(i)) {
        buffer[1][i] = SMP_CURRENT_BUFFER(smp[i]->reg.bcr);
#ifdef DEBUG
        printf("%d ",buffer[1][i]);
#endif //DEBUG
      }
    }
#ifdef DEBUG
    printf("\n");
#endif //DEBUG
    
    for (i=0;i<SMP_Entry;i++) {
      if (SMP_SELECTED(i)) {
        if (buffer[0][i]==buffer[1][i]) {
#ifdef MESSAGE
          fprintf(stderr,"buffer switch failed(tko #%d). %d -> %d\n"
                ,i,buffer[0][i],buffer[1][i]);
#endif //MESSAGE
          error = -1;
        }
      }
    }
  } // end of switch option

  *total_length = 0;
  for (entry=0;entry<SMP_Entry;entry++) {
    if (SMP_SELECTED(entry)) {
      data_size[entry] = smp[entry]->reg.dsr;
      if ((data_size[entry]&0x80000000)==0x80000000)
        data_size[entry] = SMP_MEM_SIZE-4;
    }
    else {
      data_size[entry] = 0;
      read_length = 4;
    }
    *ptr = (entry<<24)|(data_size[entry]+4);
    ptr++;
    *total_length += 4;

    if (SMP_SELECTED(entry)) {
      try = 0;
      while (++try<=10) {
      //printf("SMP%d read.\n",entry);

      //if (try>1) printf("  retry to lseek.....................................................");
      lseek( tko_vme_fd, smp_base_address[entry], SEEK_SET);

      //if (try>1) printf("  retry to read......................................................");
      errno = 0;
      read_length = read(tko_vme_fd, ptr, data_size[entry]+4);

        if (read_length!=(data_size[entry]+4)) {
#ifdef MESSAGE
          fprintf(stderr,"read error!!!  smp#%d: length %08X[byte], expected %08X[byte]\n"
            ,entry,read_length,(data_size[entry]+4));
          fprintf(stderr,"   dsr = 0x%.8x\n", smp[entry]->reg.dsr);
#endif //MESSAGE
        }
        else break;
      }
      if (try>10) {
        fprintf(stderr,"gave up to read SMP#%1d!!!\n",entry);
        error = -2;
      }
#ifdef SUPERK
      else if (try>1) error_message("  suceed to read SMP#%1d.  error recovered.",entry);
#endif //SUPERK
    }
    /* if data_size = 0, data are overwritten with terminator(FFFFFFFF). */
    if (data_size[entry] == 0) *ptr = 0xFFFFFFFF;
    
    *total_length += read_length;
    ptr += (read_length>>2);
  }

  return(error);
}


u_int TKO_buffer_switch( void )
{
  int i;
  int entry, busy, buffer[2][SMP_MAX_ENTRY];
  u_int data;
  int error=0;

#ifdef DEBUG
  printf("IN:TKO_buffer_switch()\n");
#endif //DEBUG

  while (1) {
    busy=0;
    for (entry=0;entry<SMP_Entry;entry++)
      if (SMP_SELECTED(entry))
        busy = busy + SMP_SQ_BUSY(smp[entry]->reg.csr);
    if (busy==0)
      break;
#ifdef DEBUG
    else
      printf("TKO_buffer_switch : wait %d\n",busy);

#ifdef mysleep
    sleep(1);
#endif

#endif //DEBUG
  }

#ifdef DEBUG
  printf("buffer ");
#endif //DEBUG
  for (i=0;i<SMP_Entry;i++) {
    if (SMP_SELECTED(i)) {
      buffer[0][i] = SMP_CURRENT_BUFFER(smp[i]->reg.bcr);
#ifdef DEBUG
      printf("%d ",buffer[0][i]);
#endif //DEBUG
    }
  }
#ifdef DEBUG
  printf("---> \n");
#endif //DEBUG
  
  data = ( SMP_SWITCH_AUTO_CLR | SMP_SWITCH_ENABLE
	   | SMP_SWITCH_FLG_AUTO_CLR | SMP_SWITCH_FLAG 
	   | SMP_EVENTNUM_SWITCH_MASK | SMP_DATASIZE_SWITCH_MASK);
  for (entry=0;entry<SMP_Entry;entry++) {
    if (SMP_SELECTED(entry))
      smp[entry]->reg.bcr = data;
  }
  

  /* -- buffer switch -- */
  smp[intr_generator]->reg.bcr 
    = smp[intr_generator]->reg.bcr | SMP_SWITCH_EXEC;

#ifdef DEBUG
  printf("---> ");
#endif //DEBUG
  for (i=0;i<SMP_Entry;i++) {
    if (SMP_SELECTED(i)) {
      buffer[1][i] = SMP_CURRENT_BUFFER(smp[i]->reg.bcr);
#ifdef DEBUG
      printf("%d ",buffer[1][i]);
#endif //DEBUG
    }
  }
#ifdef DEBUG
  printf("\n");
#endif //DEBUG
  
  for (i=0;i<SMP_Entry;i++) {
    if (SMP_SELECTED(i)) {
      if (buffer[0][i]==buffer[1][i]) {
#ifdef MESSAGE
        fprintf(stderr,"buffer switch failed(tko #%d). %d -> %d\n"
                ,i,buffer[0][i],buffer[1][i]);
#endif //MESSAGE
        error = -1;
      }
    }
  }
  return(error);
}

int SMP_reg_read( int tko_num, SMPREG *buffer )
{
#ifdef DEBUG
  if (tko_num>=SMP_Entry) {
    fprintf(stderr,"SMP_reg_read: invarid tko number rquested. #%d(max #%d)\n"
	    , tko_num, SMP_Entry);
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
    fprintf(stderr,"SMP_reg_read: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
    return(-1);
  }
#endif //DEBUG
  
  buffer->csr   = smp[tko_num]->reg.csr;
  buffer->snccr = smp[tko_num]->reg.snccr;
  buffer->sncr  = smp[tko_num]->reg.sncr;
  buffer->dsr   = smp[tko_num]->reg.dsr;
  buffer->enr   = smp[tko_num]->reg.enr;
  buffer->bcr   = smp[tko_num]->reg.bcr;
  buffer->scr   = smp[tko_num]->reg.scr;
  buffer->icr   = smp[tko_num]->reg.icr;
  buffer->ivr   = smp[tko_num]->reg.ivr;

  return(0);
}

int SMP_All_Reset( void  )
{
  int i;
  int entry;
  u_int data;
  
  data = ( SMP_SWITCH_AUTO_CLR | SMP_SWITCH_ENABLE
	   | SMP_SWITCH_FLG_AUTO_CLR | SMP_SWITCH_FLAG 
	   | SMP_EVENTNUM_SWITCH_MASK | SMP_DATASIZE_SWITCH_MASK);
  
  if (SMP_Entry==SMP_select_Entry) {
    
    for (entry=0;entry<SMP_Entry;entry++) {
      smp[entry]->reg.bcr = data;
    }
    
    /* -- buffer switch -- */
#ifdef RESET_BUG
    smp[intr_generator]->reg.bcr 
      = smp[intr_generator]->reg.bcr | SMP_SWITCH_EXEC;
#endif //RESET_BUG

    for (i=0;i<SMP_Entry;i++) {
      /* initialize SMP board */
      smp[i]->reg.csr = SMP_BOARD_RESET;
      smp[i]->reg.scr = event_number_thr | data_size_thr;
    }
  }
  else {
    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry))
	smp[entry]->reg.bcr = data;
    }
    
    /* -- buffer switch -- */
    smp[intr_generator]->reg.bcr 
      = smp[intr_generator]->reg.bcr | SMP_SWITCH_EXEC;

    for (i=0;i<SMP_Entry;i++) {
      /* initialize SMP board */
      if (SMP_SELECTED(i)) {
	smp[i]->reg.csr = SMP_BOARD_RESET;
	smp[i]->reg.scr = event_number_thr | data_size_thr;
      }
    }
  }

#ifdef DEBUG
  printf("ALL SMP Reset Complete\n");
#endif

  return(0);
}

int SMP_Reset( int tko_num  )
{
  if (tko_num>=SMP_Entry) {
#ifdef MESSAGE
    fprintf(stderr,"SMP_Reset: Invarid arg. of tko number.\n");
#endif //MESSAGE
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
#ifdef MESSAGE
    fprintf(stderr,"SMP_Reset: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
#endif //MESSAGE
    return(-1);
  }

  /* initialize SMP board */
  smp[tko_num]->reg.csr = SMP_BOARD_RESET;
  smp[tko_num]->reg.scr = event_number_thr | data_size_thr;
  return(0);
}

int SMP_Switch_Mask( int tko_num, u_int en_mask, u_int ds_mask )
{
  u_int data;

  if (tko_num>=SMP_Entry) {
#ifdef MESSAGE
    fprintf(stderr,"SMP_Switch_Mask; Invarid arg. of tko number.\n");
#endif //MESSAGE
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
#ifdef MESSAGE
    fprintf(stderr,"SMP_Switch_Mask: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
#endif //MESSAGE
    return(-1);
  }
  event_number_mask = (en_mask & 1)<<1;
  data_size_mask    = (ds_mask & 1);
  
  data = ((smp[tko_num]->reg.bcr & 0xFFFFFFFC )
	  | event_number_mask | data_size_mask);
  smp[tko_num]->reg.bcr = data;
  return(0);
}

u_int TKO_test_read( u_int *ptr, u_int *total_length, int switch_option
		     , u_int smp_data_size )
{
  int i,j,k;
  int entry, busy, buffer[2][SMP_MAX_ENTRY];
  u_int data_size[SMP_MAX_ENTRY], read_length;
  u_int data, data1;
  int error=0;

#ifdef DEBUG
  printf("IN:TKO_read()\n");
#endif //DEBUG

  if (switch_option==1) {
    while (1) {
      busy=0;
      for (entry=0;entry<SMP_Entry;entry++) {
	if (SMP_SELECTED(entry))
	  busy = busy + SMP_SQ_BUSY(smp[entry]->reg.csr);
      }
      if (busy==0)
	break;
#ifdef DEBUG
      else
	printf("TKO_read : wait %d\n",busy);
#endif //DEBUG
    }

#ifdef DEBUG
    printf("buffer ");
#endif //DEBUG
    for (i=0;i<SMP_Entry;i++) {
      if (SMP_SELECTED(i)) {
	buffer[0][i] = SMP_CURRENT_BUFFER(smp[i]->reg.bcr);
#ifdef DEBUG
	printf("%d ",buffer[0][i]);
#endif //DEBUG
      }
    }
#ifdef DEBUG
    printf("---> \n");
#endif //DEBUG

    data = ( SMP_SWITCH_AUTO_CLR | SMP_SWITCH_ENABLE
	     | SMP_SWITCH_FLG_AUTO_CLR | SMP_SWITCH_FLAG 
	     | SMP_EVENTNUM_SWITCH_MASK | SMP_DATASIZE_SWITCH_MASK);
    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry))
	smp[entry]->reg.bcr = data;
    }

    /* -- buffer switch -- */
    /* -- changed because CCompiler may compress the accesses 
       to smp[0]->reg.bcr 
       94/5/18 Masato Shiozawa -- */
    /*
      smp[0]->reg.bcr = data1;
    */
    smp[intr_generator]->reg.bcr 
      = smp[intr_generator]->reg.bcr | SMP_SWITCH_EXEC;

#ifdef DEBUG
    printf("---> ");
#endif //DEBUG
    for (i=0;i<SMP_Entry;i++) {
      if (SMP_SELECTED(i)) {
	buffer[1][i] = SMP_CURRENT_BUFFER(smp[i]->reg.bcr);
#ifdef DEBUG
	printf("%d ",buffer[1][i]);
#endif //DEBUG
      }
    }
#ifdef DEBUG
    printf("\n");
#endif //DEBUG

    for (i=0;i<SMP_Entry;i++) {
      if (SMP_SELECTED(entry)) {
	if (buffer[0][i]==buffer[1][i]) {
#ifdef MESSAGE
	  fprintf(stderr,"buffer switch failed(tko #%d). %d -> %d\n"
		  ,i,buffer[0][i],buffer[1][i]);
#endif //MESSAGE
	  error = -1;
	}
      }
    }
  }

  *total_length = 0;
  for (entry=0;entry<SMP_Entry;entry++) {
    /*      data_size[entry] = smp[entry]->reg.dsr;*/
    if (SMP_SELECTED(entry)) {
      data_size[entry] = smp_data_size;
      if ((data_size[entry]&0x80000000)==0x80000000)
	data_size[entry] = SMP_MEM_SIZE-4;
    }
    else {
      data_size[entry] = 0;
      read_length = 4;
    }
    *ptr = (entry<<24)|(data_size[entry]+4);
    ptr++;
    *total_length += 4;      
    
    if (SMP_SELECTED(entry)) {
      lseek( tko_vme_fd, smp_base_address[entry], SEEK_SET);
#ifdef DEBUG
      printf("SMP%d read.\n",entry);
#endif //DEBUG
      read_length = read(tko_vme_fd, ptr, data_size[entry]+4);
      if (read_length!=(data_size[entry]+4)) {
#ifdef MESSAGE
	fprintf(stderr,"read error!!!  length %08X[byte], expected %08X[byte]\n"
		,read_length,(data_size[entry]+4));
#endif //MESSAGE
	error = -2;
      }
    }
    /* if data_size = 0, data are overwritten with terminator(FFFFFFFF). */
    if (data_size[entry] == 0) *ptr = 0xFFFFFFFF;
    
    *total_length += read_length;
    ptr += (read_length>>2);
  }

  return(error);
}

u_int TKO_test_write(  int tko_num, u_int *ptr, u_int total_length )
{
  int i;
  u_int length;

  if (tko_num>=SMP_Entry) {
#ifdef MESSAGE
    fprintf(stderr,"TKO_test_write: invarid tko number rquested. #%d(max #%d)\n"
	    , tko_num, SMP_Entry);
#endif //MESSAGE
    return(-1);
  }
  else if (SMP_SELECTED(tko_num)==0) {
#ifdef MESSAGE
    fprintf(stderr,"TKO_test_write: masked tko number rquested. #%d(mask %X)\n"
	    , tko_num, SMP_mask);
#endif //MESSAGE
    return(-1);
  }
  if (total_length>SMP_MEM_SIZE)
    total_length = SMP_MEM_SIZE;
    
  for (i=0;i<(total_length/4);i++)
    smp[tko_num]->memory[i] = ptr[i];

#ifdef WRITE_WRITE
  lseek( tko_vme_fd, smp_base_address[tko_num], SEEK_SET);
  /************************************************/
  /* define unit length not to use block transfer */
  /************************************************/
#define LENGTH (1<<7)
  for (i=0;i<(total_length>>2);i+=(LENGTH>>2)) {
    length = (((i<<2)+LENGTH)>total_length)? (total_length-(i<<2)) : LENGTH; 
    write( tko_vme_fd, &(ptr[i]), length );
  }
#endif //WRITE_WRITE
}


int SMP_TKO_memory_full( void )
{
  int tko_num, full=0;

  for (tko_num=0;tko_num<SMP_Entry;tko_num++) {
    if (SMP_SELECTED(tko_num)!=0) {
      full |= ((smp[tko_num]->reg.csr)&0x00001000)>>(12-tko_num);
    }
  }

  return(full);
}

int SMP_scount_Clear( int tko_num )
{
  if (SMP_SELECTED(tko_num)!=0) {
    smp[tko_num]->reg.snccr = SMP_SCOUNT_CLEAR;
  }
  return(0);
}

int SMP_scount_Clear_all( void )
{
  int tko_num;
  
  for (tko_num=0;tko_num<SMP_Entry;tko_num++) {
    if (SMP_SELECTED(tko_num)!=0) {
      smp[tko_num]->reg.snccr = SMP_SCOUNT_CLEAR;
    }
  }
  return(0);
}

int TKO_param( int flag, u_int value )
{
  int entry;
    
  /* change parameters for TKO library */
  switch (flag) {
  case TKO_NUMTHR:
    event_number_thr = (value<SMP_MAX_EVENT_NUMBER_THR)? 
      value : SMP_MAX_EVENT_NUMBER_THR;
    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry)) {
	smp[entry]->reg.scr = event_number_thr | data_size_thr;
      }
    }
    break;
  case TKO_SIZTHR:
    data_size_thr    = (value<SMP_MAX_DATA_SIZE_THR)?
      value : SMP_MAX_DATA_SIZE_THR;
    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry)) {
	smp[entry]->reg.scr = event_number_thr | data_size_thr;
      }
    }
    break;
  case TKO_NUMMSK:
    if ((int)value)
      event_number_mask = 1;
    else
      event_number_mask = 0;
    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry))
	SMP_Switch_Mask( entry, event_number_mask, data_size_mask );
    }
    break;
  case TKO_SIZMSK:
    if ((int)value)
      data_size_mask    = 1;
    else
      data_size_mask    = 0;
    for (entry=0;entry<SMP_Entry;entry++) {
      if (SMP_SELECTED(entry))
	SMP_Switch_Mask( entry, event_number_mask, data_size_mask );
    }
    break;
  default:
#ifdef MESSAGE
    fprintf(stderr,"TKO_param: invalid flag %d\n",flag);
#endif //MESSAGE
    return(-1);
    break;
  }

  return(0);
}
