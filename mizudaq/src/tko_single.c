/*
 * tko_single.c,v 1.1 1994/4/20 Masato Shiozawa
 *          : TKO single action
 *                    1994/5/10 changed to use TKO library.
 */

/*
  #define BROADCAST_SINGLE
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "tkoprot.h"
#include "mydef.h"

int main( int argc, char **argv)
{
  int i;
  int tko_num, bc=0, ma, sa, fn, data;
  u_short sdata;
  int status;

  int broad_cast = 1;
  
  u_int* wdata;
  u_int* read_length;

  /* check argument */
  tko_num = (argc>1)? atoi(argv[1]) : 0;

  /* open TKO */
  TKO_open(1);

  // init all ATMs
  printf("initialize all ATMs conneting to smp#%d ...\n",tko_num);
  TKO_single_act( 0, broad_cast, 1, 0, ATM_INITIALIZE_F, &sdata );
  TKO_single_act( 0, broad_cast, 1, 0, ATM_FIFO_CLEAR_F, &sdata );

/*
  sdata = ATM_OPERATE_MODE;
  printf("Start AMT operation mode ...\n");
  TKO_single_act( tko_num, broad_cast, 1, 0, ATM_WRITE_MODE_F, &sdata );

  // init GONG
  sdata = GONG_OPERATE_MODE;
  printf("Start GONG operation mode ...\n");
  TKO_single_act( tko_num, 0, GONG_MA, GONG_SA, GONG_WRITE_MODE_F, &sdata );
*/

  // Set HIT threshold (range in 0 ~ -1240 mV)
  // this threshold value corresponds to input pulse height * 100.
  sdata = ATM_THRDAC(-400.);  // means 4mV threshold to input signal
  //sdata = ATM_THRDAC(-500.);  // means 5mV threshold to input signal
  //sdata = ATM_THRDAC(-1000.);  // means 10mV threshold to input signal
  printf("Set HIT Threshold : 0x%x\n",sdata);
  TKO_single_act( 0, broad_cast, 1, 0, ATM_WRITE_THRDAC_F, &sdata );


/*
  while (1) {
#ifdef BROADCAST_SINGLE
    printf("\nBC MA SA FN >? ");
    scanf("%d %d %d %d",&bc,&ma,&sa,&fn);
#else
    printf("\nMA SA FN >? ");
    scanf("%d %d %d",&ma,&sa,&fn);
#endif
    if (fn<8) {
      printf("BC:%d MA:%d SA:%d FN:%d\n",bc,ma,sa,fn);
      status = TKO_single_act( tko_num, bc, ma, sa, fn, &sdata ); 
      printf("DATA:0x%04X, TOUT %d, Q %d\n",sdata,(status>>1)&1,status&1);
    }
    else {
      printf("DATA(hex) >? ");
      scanf("%x",&data);
      sdata = data;
      printf("BC:%d MA:%d SA:%d FN:%d, DATA:0x%04X\n",bc,ma,sa,fn,sdata);
      status = TKO_single_act( tko_num, bc, ma, sa, fn, &sdata ); 
      printf("TOUT %d, Q %d\n",(status>>1)&1,status&1);
    }
  }
*/
  
  /* close TKO */
  TKO_close();
}

