#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "tkoprot.h"
#include "gongvar.h"
#include "atmvar.h"

#define TIMEOUT 10
#define SDS_USE_GOBIT_TRG (1<<4)

const int n_used_smp = 1;


////
int main( void )
{
  int i,j,k;
  u_short sdata;
  int broad_cast = 1;
  int n_pedestals = 2;
  int wdata[2000];


  printf("start tko open ...\n");
  if( TKO_open(n_used_smp)<0 ) exit(1);

  // TKO buffer switch
  printf("start tko buffer switch ...\n");
  if (TKO_buffer_switch()) {
    perror("buffer switch error\n");
    return(-1);
  }

  // set pedestal mode for check
  printf("start to set pedestal mode for check ...\n");
  for (i=0;i<n_used_smp;i++) {
      printf("\nset pedestal mode, smp#%d ...\n",i);

      // set higher threshold
      // range in 0 ~ -1240 mV
      printf("set hit threshold ...\n");
      sdata = ATM_THRDAC(-1240.);
      TKO_single_act( i, broad_cast, 1, 0, ATM_WRITE_THRDAC_F, &sdata );

      // initialize all ATMs
      printf("initialize all ATMs conneting to smp#%d ...\n",i);
      TKO_single_act( i, broad_cast, 1, 0, ATM_INITIALIZE_F, &sdata );

      sdata = ATM_PEDESTAL_MODE;
      printf("set AMT pedestal mode ...\n");
      TKO_single_act( i, broad_cast, 1, 0, ATM_WRITE_MODE_F, &sdata );
      TKO_single_act( i, broad_cast, 1, 0, ATM_FIFO_CLEAR_F, &sdata );

      sdata = GONG_PEDESTAL_MODE;
      printf("set GONG pedestal mode ...\n");
      TKO_single_act( i, 0, GONG_MA, GONG_SA, GONG_WRITE_MODE_F, &sdata );

      // delay time
      // sdata = 5; // about 550 nsec
      sdata = 7; // about 750 nsec
      printf("set GONG delay time to write data ...\n");
      TKO_single_act( i, 0, GONG_MA, GONG_SA, GONG_WRITE_DELAY_F, &sdata );
  }
  // wait for ATM come to be stable
  sleep(1);

  printf("start of pedestal mode ...");
  printf("(%d pedestal cycle)\n",n_pedestals);
  for(j=1;j<=n_pedestals;j++) {
    for (i=0;i<n_used_smp;i++) {
      TKO_single_act( i, 0, GONG_MA, GONG_SA, GONG_PEDESTAL_F, &sdata );
    }
    usleep(1000);
  }

  printf("Select SDS program & start of TKO_sds_go ...\n");
  for (i=0;i<n_used_smp;i++) {
    TKO_sds_go( i, SDS_USE_GOBIT_TRG );
  }

  read_tko_data( &wdata[0] );

  TKO_close();
  printf("TKO Close!\n");
}
