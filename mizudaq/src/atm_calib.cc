#include <iostream>
#include <unistd.h>
#include <csignal>
#include <time.h>
#include <sys/time.h>

#include <TFile.h>
#include <TTree.h>

#define DECODE

#define TIMEOUT 300

#define OPERATE_RUN 1
#define PEDESTAL_RUN 2

extern "C" {
  #include "fadcprot.h"
  #include "tkoprot.h"
  #include "trgprot.h"
  #include "./my_utility.h"
}



#define MAX_nTRG 100
#define MAX_nATM 2000
#define MAX_nFADC 2000

typedef struct {
  int nTRG;
  unsigned short TRG[MAX_nTRG];
} trg_data;

typedef struct {
  int nATM;
  unsigned int ATM[MAX_nATM];
} atm_data;

typedef struct {
  int nFADC;
  unsigned int FADC[MAX_nFADC];
} fadc_data;

typedef struct{
  int runtype;
  trg_data trg;
  atm_data s_atm;
  atm_data p_atm;
  fadc_data fadc;
} mizu_raw_data;

static mizu_raw_data r_data_dummy;

char *output_name = "atm_calib.root";
TTree *tree;

#define n_used_smp 1
const int smp_num[n_used_smp] = { 0 };
const int all_atms_adc_time = 1000;
//= 14borads * 12ch *~5.5usec = 924 usec

#define SDS_USE_GOBIT_TRG 0x10

#include "VmeIoreg.h"
#define VIOREG_BASEADDR 0xF000


//////////////////////////

volatile sig_atomic_t eflag = 0;

void handler(int signum)
{
  eflag = 1;
}

void set_tree(TTree *tree, mizu_raw_data &r_data)
{
  tree->Branch("RUNTYPE", &(r_data.runtype), "RUNTYPE/I");
  tree->Branch("TRG", &(r_data.trg), "nTRG/I:TRG[nTRG]/s");
  tree->Branch("ATM", &(r_data.s_atm), "nATM/i:ATM[nATM]/i");

  std::cout << "Set Output tree branch" << std::endl;
}


////
int main(int argc, char* argv[])
{
  if(argc<3) {
    printf("usage : (program) (#_of_cycle) (run type) (output)\n");
    exit(1);
  }

  // input cycle#
  int cycle = atoi(argv[1]);
  std::cout << "input cycle : " << cycle << std::endl;

  // input output-file name
  int runtype = atoi(argv[2]);
  if( runtype==OPERATE_RUN ) {
    output_name="mizu_calib_operate.root";
  }
  else if( runtype==PEDESTAL_RUN ) {
    output_name="mizu_calib_pedestal.root";
  }
  if(argc>3) {
    output_name = argv[3];
  }

  //
  TRG trg0[100];
  mizu_raw_data r_data = r_data_dummy;

  tree = new TTree("r_atm","ATM raw data");
  set_tree(tree, r_data);
  r_data.runtype = runtype;

  double start, stop, wait_time;
  unsigned int wdata_tmp[2000];
  unsigned int read_length_tmp;

  // inturrupt from terminal
  if( signal( SIGINT, handler )==SIG_ERR ) {
    if( eflag==1 ) {
      TFile fout(output_name,"recreate");
      fout.cd();
      tree->Write();
      fout.Close();

      printf("Terminate!!!\n");

      TRG_close();
      TKO_close();

      exit(1);
    }
  }

  // Initialization of all modules
  TKO_open(n_used_smp);
  TKO_init(smp_num[0]);

  TRG_open();

   // Initialization of I/O register
  VmeIoreg vioreg( VIOREG_BASEADDR );

  // start veto untill daq ready
  vioreg.start_veto_latch();

  // check elec (by pedestal operation)
  std::cout << "*** Start to check elec (by pedestal operation) ***" << std::endl;
  set_tko_pedestal_mode( smp_num[0] );

  for(int i=0; i<2; i++) {
    start_tko_pedestal( smp_num[0] );
    usleep(1000);
  }

  printf("Start of TKO SDS ...\n");
  TKO_sds_go( 0, SDS_USE_GOBIT_TRG );
  usleep(100000);

  printf("Reading Data in SMP's memory (after sds_go)\n");
  if( TKO_read( wdata_tmp, &read_length_tmp, 1 )<0 ) {
    printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
  }
  else {
    printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);
  }

  // clear SDS count
  SMP_scount_Clear( smp_num[0] );

  // Init TRG
  TRG_Command( TRG_TRGENA );

  // Select TKO measurement mode
  if( runtype==OPERATE_RUN ) {
    set_tko_operate_mode( smp_num[0] );

    // set event threshold for SMP interrupt
    // default : 1*4 = 4 event threshold
    //TKO_param( TKO_NUMTHR, (5<<2) );
    // => 5*4 = 20 event / 1 entry
    //cycle = (int)(cycle/4);

    if( cycle<1 ) {
      std::cout << "invalid input cycle#" << std::endl;
      exit(1);
    }

  }

  else if( runtype==PEDESTAL_RUN ) {
    set_tko_pedestal_mode(smp_num[0]);

    //cycle = (int)(cycle/20);
  }

  else {
    std::cout << "invalid run type" << std::endl;
    exit(1);
  }

  // Start event loop
  for(int icycle=0; icycle<cycle; icycle++) {

    // waitevent (until limit event# or size)
    printf("*** %dth cycle start ***\n",icycle);

    // start timer
    start = gettimeofday_usec();

    // OPERATE RUN
    if( runtype==OPERATE_RUN ) {

      // stop veto latch
      vioreg.stop_all_latch();

      int tmp = TKO_waitevent( TIMEOUT );

      // start veto latch
      vioreg.start_veto_latch();  // -> channel 1

      switch( tmp ) {
        case -1 : 
          printf("waitevent faild.\n");
          exit(1);

        case -2 :
          printf("WARNING: waitevent timeout.\n");
          printf("TKO_WAIT_EVENT FAILED(code -2)\n");

          // check if SMP is full or not.
          if( SMP_TKO_memory_full() ) {
            printf("SMP'memory full\n");

            // fifo is full and SMP might be never switched.
            // So switch by force and read.
            //r_data.nATM = read_tko_data( r_data.wATM, 1 );
            printf("Reading Data in SMP's memory (after sds)\n");
            if( TKO_read( wdata_tmp, &read_length_tmp, 1 )<0 ) 
              printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
            else 
              printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);
 
            break;
          }
        
        case 0:
        default:
          printf("received interrupt from SMP\n");

          //read memory data.
          if( TKO_read( r_data.s_atm.ATM, &read_length_tmp, 0 )<0 ) 
            printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
          else 
            printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

          r_data.s_atm.nATM = (read_length_tmp/4);

#ifdef DECODE
          decode_tko_data(r_data.s_atm.ATM, r_data.s_atm.nATM);
#endif
          break;
      }

    } // end of OPERATE_RUN

    // PEDESTAL RUN
    if( runtype==PEDESTAL_RUN ) {

//      for(int j=0; j<20; j++) {
        for(int i=0; i<2; i++) {
          start_tko_pedestal(smp_num[0]);
          usleep(1000); // 1kHz
        }
//      }

      printf("Start of TKO SDS ...\n");
      TKO_sds_go( 0, SDS_USE_GOBIT_TRG );
      usleep(100000); // 10Hz -> copy of SK source code (COLLECTOR)
      //sleep(1);

      printf("reading pedestal data\n");
      printf("Reading Data in SMP's memory (after sds)\n");
      if( TKO_read( r_data.s_atm.ATM, &read_length_tmp, 1 )<0 ) 
        printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
      else 
        printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

      r_data.p_atm.nATM = (read_length_tmp/4);

#ifdef DECODE
//      decode_tko_data(r_data.s_atm.ATM, r_data.s_atm.nATM);
#endif

    } // end of PEDESTAL_RUN

    // readout trigger #
    r_data.trg.nTRG = TRG_Read( trg0, 20);
    std::cout << "Read TRG count : " << r_data.trg.nTRG << std::endl;

    for(int i=0; i<(r_data.trg.nTRG); i++) {
      r_data.trg.TRG[i] = trg0[i].fifo_a;
#ifdef DECODE
      std::cout << "  trg_fifo_a :"  << r_data.trg.TRG[i] << std::endl;
      //std::cout << "  trg_fifo_b :"  << trg0[i].fifo_b << std::endl;
      //std::cout << "  trg_fifo_c :"  << trg0[i].fifo_c << std::endl;
#endif
    }

    // stop timer 
    stop = gettimeofday_usec();

    // time in this cycle
    wait_time = stop - start;
    std::cout << "time in this cycle [usec] :" << wait_time << std::endl;

    // Fill data of this event
    tree->Fill();

  }

////// End of Run //////
  TFile fout(output_name,"recreate");
  fout.cd();
  tree->Write();
  fout.Close();

  TKO_close();
  TRG_close();
};
