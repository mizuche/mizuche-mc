#include <iostream>
#include <unistd.h>
#include <csignal>
#include <time.h>
#include <sys/time.h>

extern "C" {
  #include "fadcprot.h"
  #include "tkoprot.h"
  #include "trgprot.h"
  #include "my_utility.h"
}

#include "VmeIoreg.h"
#define VIOREG_BASEADDR 0xF000
VmeIoreg vioreg( VIOREG_BASEADDR );

#define TIMEOUT 600 // [sec]

#define n_used_smp 1
const int smp_num[n_used_smp] = { 0 };

#include "TRawData.h"
TRawData *raw_data;

mizu_raw_data r_data, r_data_dummy;
TRG trg0[100], trg0_dummy[100];
unsigned int wdata_tmp[2000];
unsigned int read_length_tmp;

//#define DECODE

//
void handler(int signum)
{
  printf("Terminate!!!\n");

  raw_data->Write();
  TRG_close();
  TKO_close();
  exit(1);
}

#if 0
void start_pedestal()
{
  r_data = r_data_dummy;
  trg0 = trg0_dummy;

  start_tko_pedestal( smp_num[0] );

  //read memory data of pedestal.
  //if( TKO_read( r_data.p_atm.ATM, &read_length_tmp, 1 )<0 ) 
  if( TKO_read( r_data.atm.ATM, &read_length_tmp, 1 )<0 ) 
    printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
  else 
    printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

  //r_data.p_atm.nATM = (read_length_tmp/4);
  r_data.atm.nATM = (read_length_tmp/4);
  r_data.mode = PEDESTAL_MODE;

#ifdef DECODE
//    decode_tko_data(r_data.p_atm.ATM, r_data.p_atm.nATM);
#endif

  // read memory data of TRG
  r_data.trg.nTRG = TRG_Read( trg0, 100);
  std::cout << "Read TRG count : " << r_data.trg.nTRG << std::endl;

  for(int i=0; i<(r_data.trg.nTRG); i++) {
    r_data.trg.TRG[i] = trg0[i].fifo_a;

#ifdef DECODE
    std::cout << "  trg_fifo_a :"  << r_data.trg.TRG[i] << std::endl;
#endif
  }

  // Fill output tree
  raw_data->Fill( r_data );

}


void start_operate()
{
  r_data = r_data_dummy;
  trg0 = trg0_dummy;

  // Stop VETO latch 
  vioreg.stop_all_latch();

  // Start to wait for interupt from REQUEST NIM input singal
  // or more events than threshold
  int ret = TKO_waitevent( TIMEOUT ); 

  // start veto latch
  vioreg.start_veto_latch();  // -> channel 1

  switch( ret ) {
    case -1 : 
      printf("waitevent faild.\n");
      exit(1);

    case -2 :
      printf("WARNING: waitevent timeout.\n");
      printf("TKO_WAIT_EVENT FAILED(code -2)\n");

      // check if SMP is full or not.
      if( SMP_TKO_memory_full() ) {
        printf("SMP'memory full\n");

        // fifo is full and SMP might be never switched.
        // So switch by force and read.
        printf("Reading Data in SMP's memory (after sds)\n");
        if( TKO_read( wdata_tmp, &read_length_tmp, 1 )<0 ) 
          printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
        else 
          printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

        break;
      }
      
    case 0 :
    default :
      printf("received interrupt from SMP\n");

      //read memory data of operate
      //if( TKO_read( r_data.s_atm.ATM, &read_length_tmp, 0 )<0 ) 
      if( TKO_read( r_data.atm.ATM, &read_length_tmp, 0 )<0 ) 
        printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
      else 
        printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

      //r_data.s_atm.nATM = (read_length_tmp/4);
      r_data.atm.nATM = (read_length_tmp/4);
      r_data.mode = OPERATE_MODE;

#ifdef DECODE
      decode_tko_data(r_data.atm.ATM, r_data.atm.nATM);
#endif
      break;
  }

  // read memory data of TRG
  r_data.trg.nTRG = TRG_Read( trg0, 100);
  std::cout << "Read TRG count : " << r_data.trg.nTRG << std::endl;

  for(int i=0; i<(r_data.trg.nTRG); i++) {
    r_data.trg.TRG[i] = trg0[i].fifo_a;

#ifdef DECODE
    std::cout << "  trg_fifo_a :"  << r_data.trg.TRG[i] << std::endl;
#endif
  }

  // Fill output tree
  raw_data->Fill( r_data );

}
#endif

////
int main(int argc, char* argv[])
{
  if(argc<2) {
    printf("usage : (program) (#_of_cycle) (output)\n");
    exit(1);
  }

  // input # of events
  int nevents = atoi(argv[1]);
  std::cout << "input cycle : " << nevents << std::endl;

  char output_name[200] = "atm_calib2.root";
  // input output-file name
  if(argc>2) {
    sprintf(output_name,"%s",argv[2]);
  }
	//raw_data = new TRawData( output_name );

  //
/*
  TRG trg0[100];
  mizu_raw_data r_data = r_mizu_dummy;
*/

  //
  double start_time, stop_time, wait_time;
/*
  unsigned int wdata_tmp[2000];
  unsigned int read_length_tmp;
*/

  // inturrupt from terminal
  signal( SIGINT, handler );
  signal( SIGABRT, handler );

  // Initialization of I/O register
  //VmeIoreg vioreg( VIOREG_BASEADDR );

  // start veto untill daq ready
  vioreg.stop_all_latch();
  while(1) {

    vioreg.start_veto_latch();
    ss_sleep(1);
    vioreg.stop_all_latch();
    ss_sleep(1);

  }

  // TKO Initialization

#if 0
  TKO_open(n_used_smp);
  TKO_init(smp_num[0]);

  // set SMP intrupt event#
  TKO_param( TKO_NUMTHR, (5<<2) );
  //-> means 5*4 = 20 events

  // Initialization of TRG
  TRG_open();
  TRG_Command( TRG_TRGENA );

  // check elec (by pedestal operation)
  pedestal_test( smp_num[0] );

  // clear SDS count
  SMP_scount_Clear( smp_num[0] );

  // Start event loop
  for(int ievent=0; ievent<nevents; ievent++) {

    printf("\n=== %dth cycle start ===\n",ievent);

    // start timer
    start_time = gettimeofday_usec();

    // Init
    r_data = r_mizu_dummy;

    // Start pedestal run
    start_pedestal(raw_data);
/*
    start_tko_pedestal( smp_num[0] );

    //read memory data of pedestal.
    //if( TKO_read( r_data.p_atm.ATM, &read_length_tmp, 1 )<0 ) 
    if( TKO_read( r_data.atm.ATM, &read_length_tmp, 1 )<0 ) 
      printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
    else 
      printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

    //r_data.p_atm.nATM = (read_length_tmp/4);
    r_data.atm.nATM = (read_length_tmp/4);
    r_data.mode = PEDESTAL_MODE;

#ifdef DECODE
//    decode_tko_data(r_data.p_atm.ATM, r_data.p_atm.nATM);
#endif

    // read memory data of TRG
    r_data.trg.nTRG = TRG_Read( trg0, 100);
    std::cout << "Read TRG count : " << r_data.trg.nTRG << std::endl;

    for(int i=0; i<(r_data.trg.nTRG); i++) {
      r_data.trg.TRG[i] = trg0[i].fifo_a;

#ifdef DECODE
      std::cout << "  trg_fifo_a :"  << r_data.trg.TRG[i] << std::endl;
#endif
    }
    // Fill output tree
		raw_data->Fill( r_data );
*/
    printf("End pedestal mode --> Start operate mode\n");
    //r_data = r_mizu_dummy;

    // start ATM operate mode
    start_operate(raw_data);
/*
    set_tko_operate_mode( smp_num[0] );

    // Stop VETO latch 
    vioreg.stop_all_latch();

    // Start to wait for interupt from REQUEST NIM input singal
    // or more events than threshold
    int ret = TKO_waitevent( TIMEOUT ); 

    // start veto latch
    vioreg.start_veto_latch();  // -> channel 1

    switch( ret ) {
      case -1 : 
        printf("waitevent faild.\n");
        exit(1);

      case -2 :
        printf("WARNING: waitevent timeout.\n");
        printf("TKO_WAIT_EVENT FAILED(code -2)\n");

        // check if SMP is full or not.
        if( SMP_TKO_memory_full() ) {
          printf("SMP'memory full\n");

          // fifo is full and SMP might be never switched.
          // So switch by force and read.
          printf("Reading Data in SMP's memory (after sds)\n");
          if( TKO_read( wdata_tmp, &read_length_tmp, 1 )<0 ) 
            printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
          else 
            printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);
 
          break;
        }
        
      case 0 :
      default :
        printf("received interrupt from SMP\n");

        //read memory data of operate
        //if( TKO_read( r_data.s_atm.ATM, &read_length_tmp, 0 )<0 ) 
        if( TKO_read( r_data.atm.ATM, &read_length_tmp, 0 )<0 ) 
          printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
        else 
          printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

        //r_data.s_atm.nATM = (read_length_tmp/4);
        r_data.atm.nATM = (read_length_tmp/4);
        r_data.mode = OPERATE_MODE;

#ifdef DECODE
        decode_tko_data(r_data.s_atm.ATM, r_data.s_atm.nATM);
#endif
        break;
    }

    // read memory data of TRG
    r_data.trg.nTRG = TRG_Read( trg0, 100);
    std::cout << "Read TRG count : " << r_data.trg.nTRG << std::endl;

    for(int i=0; i<(r_data.trg.nTRG); i++) {
      r_data.trg.TRG[i] = trg0[i].fifo_a;

#ifdef DECODE
      std::cout << "  trg_fifo_a :"  << r_data.trg.TRG[i] << std::endl;
#endif
    }

    // Fill output tree
		raw_data->Fill( r_data );
*/

    // stop timer 
    stop_time = gettimeofday_usec();

    // time in this cycle
    wait_time = stop_time - start_time;
    std::cout << "====  time / cycle [usec] :" << wait_time << "  ====" << std::endl;

  } // end of cycle loop

////// End of Run //////
  raw_data->Write();
  TKO_close();
  TRG_close();

#endif
  return 0;
};
