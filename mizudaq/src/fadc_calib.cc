#include <iostream>
#include <unistd.h>
#include <csignal>
#include <time.h>
#include <sys/time.h>

#include <TFile.h>
#include <TTree.h>

#define MAX_NFADC 10000 // temporary

char *output_name = "fadc_calib.root";

extern "C" {
  #include "fadcprot.h"
  #include "fadclib.h"
  #include "my_utility.h"
}

typedef struct {
  int nFADC;
  unsigned int wFADC[max_fadc_data];
} fadc_data_struct;

static fadc_data_struct r_data_dummy;

TTree *tree = new TTree("r_fadc","Raw FADC data");

void terminate(int dummy)
{
  FADC_close();

  //signal(SIGINT, SIG_DFL);
  exit(1);

  TFile fout(output_name,"recreate");
  fout.cd();
  tree->Write();
  fout.Close();
}

void set_tree(TTree *tree, fadc_data_struct &r_data)
{
  tree->Branch("nFADC", &(r_data.nFADC), "nFADC/I");
  tree->Branch("wFADC", (r_data.wFADC), "wFADC[nFADC]/i");

  std::cout << "Set Output tree branch!" << std::endl;
}


////
int main(int argc, char* argv[])
{
  if(argc<2) {
    printf("usage : (program) (#_of_cycle) (output fname)\n");
    exit(1);
  }

  int cycle = atoi(argv[1]);
  std::cout << "input cycle : " << cycle << std::endl;

  // input output-file name
  if(argc>2) {
    output_name = argv[2];
  }

  fadc_data_struct r_data = r_data_dummy;

  set_tree(tree, r_data);

  double t1;

  // at Ctrl-C stop
  signal( SIGINT, terminate );

  // Initialization of all modules
  FADC_init();
  FADC_daq_mode(1); // register controlled mode

  // Start event loop
  for(int icycle=0; icycle<cycle; icycle++) {

    std::cout << icycle << std::endl;

    // Start daq
    FADC_daq_start();
  
    // start timer (after end beam gate)
    t1 = gettimeofday_usec();

    // polling until end of Beam Gate
    wait_fadc_trigger();

    // stop FADC
    FADC_daq_stop();

    // readout data of FADC
    r_data.nFADC = FADC_Read( &(r_data.wFADC[0]) );
  
    // Fill data of this event
    tree->Fill();

    printf("DAQ time / event : %.3f [msec]\n",
          (gettimeofday_usec()-t1)/1000.);

  }

////// End of Run //////
  // close all modules;
  FADC_close();

  TFile fout(output_name,"recreate");
  fout.cd();
  tree->Write();
  fout.Close();

  return(0);

};
