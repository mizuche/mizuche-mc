#include <iostream>
#include <unistd.h>
#include <csignal>
#include <time.h>
#include <sys/time.h>

extern "C" {
  #include "fadcprot.h"
  #include "tkoprot.h"
  #include "trgprot.h"
  #include "my_utility.h"
}

#include "TRawData.h"
TRawData *raw_data;
mizu_raw_data r_data;
static mizu_raw_data r_data_dummy;
TRG trg0[MAX_nTRG];

#define TIMEOUT 600 // [sec]

#define n_used_smp 1
const int smp_num[n_used_smp] = { 0 };

#define OPERATE_PEDESTAL_RUN 0
#define PEDESTAL_RUN 1
//#define NOISE_MEAS


//#define FADC_ENABLE

#define ENABLE_IOREG
#ifdef ENABLE_IOREG
#include "VmeIoreg.h"
#define VIOREG_BASEADDR 0xF000
VmeIoreg vioreg( VIOREG_BASEADDR );
#endif

//#define DEBUG
//#define OLDVER

//
extern "C" void handler(int signum)
{
  std::cerr << "Terminate!!!\n" << std::endl;;

  raw_data->Write();
  TRG_close();
  TKO_close();
  exit(1);
}

void read_fadc_data()
{
  r_data.fadc.nFADC = FADC_Read(r_data.fadc.FADC);
}

void read_trg_data()
{
  r_data.trg.nTRG = TRG_Read( trg0, MAX_nTRG);
#ifdef DEBUG
  std::cout << "Read TRG count : " << r_data.trg.nTRG << std::endl;
#endif

#ifdef OLDVER
    r_data.trg.TRG[0] = trg0[0].fifo_a;
    r_data.trg.TRG[1] = trg0[0].fifo_b;
    r_data.trg.TRG[2] = trg0[0].fifo_c;
    r_data.trg.TRG[3] = trg0[0].fifo_d;
    r_data.trg.TRG[4] = trg0[0].fifo_e;
    std::cout << "  trg count :"  << r_data.trg.TRG[0] << std::endl;
    std::cout << "  trg id :"  << r_data.trg.TRG[1] << std::endl;

    unsigned long long clk48;
    clk48 = r_data.trg.TRG[2] * 65536 * 65536;
    clk48 += r_data.trg.TRG[3] * 65536;
    clk48 += r_data.trg.TRG[4];
    std::cout << "  trg 48bit clock : "  << clk48*2.*1.e-8 << " [sec]" << std::endl;

#else

  for(int i=0; i<(r_data.trg.nTRG); i++) {
    r_data.trg.TRG[i][0] = trg0[i].fifo_a;
    r_data.trg.TRG[i][1] = trg0[i].fifo_b;
    r_data.trg.TRG[i][2] = trg0[i].fifo_c;
    r_data.trg.TRG[i][3] = trg0[i].fifo_d;
    r_data.trg.TRG[i][4] = trg0[i].fifo_e;
#ifdef DEBUG
    std::cout << "  trg count :"  << r_data.trg.TRG[i][0] << std::endl;
    std::cout << "  trg id :"  << r_data.trg.TRG[i][1] << std::endl;
    
    unsigned long long clk48;
    clk48 = r_data.trg.TRG[i][2] * 65536 * 65536;
    clk48 += r_data.trg.TRG[i][3] * 65536;
    clk48 += r_data.trg.TRG[i][4];
    std::cout << "  trg 48bit clock : "  << clk48*2.*1.e-8 << " [sec]" << std::endl;
#endif
  }
#endif
}

void read_atm_data(int runmode)
{
  int flag;
  unsigned int read_length_tmp;

  if(runmode==PEDESTAL_MODE) flag=1;
  else if(runmode==OPERATE_MODE) flag=0;
  else {
    std::cout << "Invalid run mode!!!" << std::endl;
    exit(-1);
  }

  if( TKO_read( r_data.atm.ATM, &read_length_tmp, flag )<0 ) 
    printf("reading SMP is failed. (%d [bytes])\n", read_length_tmp);
  else 
    printf("reading SMP is completed. (%d [bytes])\n", read_length_tmp);

  r_data.atm.nATM = (read_length_tmp/4);
}

void start_pedestal()
{
  // init
  r_data = r_data_dummy;
  r_data.mode = PEDESTAL_MODE;

  // Set Pedestal Run mode
  start_tko_pedestal( smp_num[0] );

  //read data of pedestal.
  read_atm_data(PEDESTAL_MODE);

  // read data of TRG
  read_trg_data();

  // Fill output tree
  raw_data->Fill( r_data );

  // End pedestal run mode -> Change Operate mode
#ifdef DEBUG
  std::cout << "End pedestal -> Start operate" << std::endl;
#endif

  set_tko_operate_mode( smp_num[0] );

}

void start_operate()
{
  std::cout << "\nStart operation mode..." << std::endl;
  // init
  r_data = r_data_dummy;
  r_data.mode = OPERATE_MODE;

#ifdef FADC_ENABLE
  FADC_daq_start();
#endif

  // Stop VETO latch 
#ifdef ENABLE_IOREG
  vioreg.level_out( 0 );
#endif

  // Start to wait for interupt from REQUEST NIM input singal
  // or more events than threshold
  int ret = TKO_waitevent( TIMEOUT ); 

  // start veto latch
#ifdef ENABLE_IOREG
  vioreg.level_out( 0x01 ); // IO ch1->TRG VETO
#endif

#ifdef FADC_ENABLE
  FADC_daq_stop();
#endif

 
  switch( ret ) {
    case -1 : 
      printf("waitevent faild.\n");
      exit(1);

    case -2 :
      printf("WARNING: waitevent timeout.\n");
      printf("TKO_WAIT_EVENT FAILED(code -2)\n");

      // check if SMP is full or not.
      if( SMP_TKO_memory_full() ) {
        printf("SMP'memory full\n");
        printf("FIFO is full and SMP might be never switched.So switch by force and read.\n");
        read_atm_data(PEDESTAL_MODE);

        break;
      }
      
    case 0 :
    default :
#ifdef DEBUG
      printf("Interrupt from SMP!!!\n");
#endif
      
      // Read ATM data
      read_atm_data(OPERATE_MODE);

      break;
  }

  // Read TRG data
  read_trg_data();

  // Read FADC data
#ifdef FADC_ENABLE
  read_fadc_data();
#endif

  // Fill output tree
  raw_data->Fill( r_data );

}

void Usage()
{
  printf("usage : <program> <#_of_cycle> <output name> <run mode (0:ope+ped, 1:ped)>\n");
  exit(1);
}

////
int main(int argc, char* argv[])
{
  if(argc<2) Usage();

  // input # of events
  int ncycles = atoi(argv[1]);
  std::cout << "input cycle : " << ncycles << std::endl;

  // output-file name
  char output_name[200] = "output_mizu_daq.root";
  if(argc>2) {
    sprintf(output_name,"%s",argv[2]);
  }
	raw_data = new TRawData( output_name );

  // select run mode
  // 0 -> operate + pedestal
  // 1 -> pedestal
  int run_mode = OPERATE_PEDESTAL_RUN;
  if(argc>3) run_mode = atoi(argv[3]);

  // set inturrupt from terminal
  signal( SIGINT, handler );
  signal( SIGABRT, handler );

  // Start VETO 
#ifdef ENABLE_IOREG
  vioreg.level_out( 0x01 );
#endif

  // TKO Initialization
  TKO_open(n_used_smp);
  TKO_init(smp_num[0],-150);

  // Set threshold of SMP intrupt event#
  TKO_param( TKO_NUMTHR, (5<<2) );  //-> 5*4 = 20 events
  //TKO_param( TKO_NUMTHR, (50<<2) ); //-> 50*4 = 200 events
  
  // TRG Initialization
  TRG_open();
  TRG_Command( TRG_TRGENA );

#ifdef FADC_ENABLE
  // FADC Initialization
  FADC_init(0x8000);
  FADC_daq_mode(1);
#endif

  // check elec (by pedestal run)
  pedestal_test( smp_num[0] );

  // clear SDS count
  SMP_scount_Clear( smp_num[0] );

  // Start event loop
  for(int icycle=0; icycle<ncycles; icycle++) {

    printf("==== %dth cycle ====\n",icycle);

    // start timer
    double start_time = gettimeofday_usec();
    printf("\n---------- Timer start ----------\n");

    // Init
    r_data = r_data_dummy;

    // Start pedestal run
    start_pedestal();
    printf("\n---------- time stop ---> interval=%.1f[msec]\n",
            (gettimeofday_usec()-start_time)/1000.);

    // Start operate cycle
    if(run_mode==OPERATE_PEDESTAL_RUN) {

#ifdef ATMCALIB
      // For ATM calibration w/ QT
      for(int i=0; i<200; i++) {

        if(i%10==0) printf("= %dth cycle =\n",i);

        // start ATM operate mode
        start_time = gettimeofday_usec();
        start_operate();

        // stop timer 
        if(i%10==0)
          printf(" operate time [msec] : %f\n",(gettimeofday_usec()-start_time)/1000.);
      }
#endif

#ifdef NOISE_MEAS
      // For noise rate meas or cosmic meas..
      // noise rate meas. : 1 cycle = 10 triggers
      // cosimic meas. : 1cyle = 1 event triger
      for(int itrg=0; itrg<10; itrg++) {
        printf("== %dth trigger : %dth event ==\n",icycle,itrg);
#endif

        // start ATM operate mode
        start_time = gettimeofday_usec();
        printf("\n---------- Timer start ----------\n");

        start_operate();

       // stop timer 
        printf("\n--------- time stop ---> interval=%.1f[msec]\n",
                (gettimeofday_usec()-start_time)/1000.);


#ifdef NOISE_MEAS
      } // end of event loop
#endif

    } // end of operate mode
       
    else if(run_mode==PEDESTAL_RUN) {
      ss_sleep(500);
    }

  } // end of cycle loop


////// End of Run //////
  raw_data->Write();
  TKO_close();
  TRG_close();

  return 0;
};
