#include <time.h>
#include <sys/time.h>
#include <sys/select.h>
#include "my_utility.h"

void short_2bit(short val)
{
  int i;
  unsigned int tmp[16];

  for(i=0; i<16; i++) {
    tmp[i] = (val>>i) & 0x1;
  }
  for(i=0; i<16; i++){
    if(i>0&&i%4==0) printf(" ");
    printf("%d",tmp[31-i]);
  }
  printf("\n");

}

void int_2bit(int val)
{
  int i;
  unsigned int tmp[32];

  for(i=0; i<32; i++) {
    tmp[i] = (val>>i) & 0x1;
  }
  for(i=0; i<32; i++){
    if(i>0&&i%4==0) printf(" ");
    printf("%d",tmp[31-i]);
  }
  printf("\n");

}

int ss_sleep(int millisec)
{
  struct timeval tv;
  int status;

  tv.tv_sec = (int)(millisec/1000);
  tv.tv_usec = (millisec%1000) * 1000;

//  printf("ss_sleep : %d sec, %d usec\n",tv.tv_sec, tv.tv_usec);

/*
  status = select(0, NULL, NULL, NULL, &tv);
  if(status==-1) perror("error of ss_sleep");
*/
  usleep(millisec*1000);

  return status;
}

double gettimeofday_usec()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return (double)(tv.tv_sec*1.e6 + tv.tv_usec);
}

double gettimeofday_sec()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (double)(tv.tv_sec + tv.tv_usec*1.e-6);
}

double gettimeofday_sec_usec(int *sec, int *usec)
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    *sec = tv.tv_sec;
    *usec = tv.tv_usec;
    return (double)(tv.tv_sec + tv.tv_usec*1.e-6);
}

