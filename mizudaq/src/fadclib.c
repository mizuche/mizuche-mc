// -*- c++ -*-
#include "vmedrv.h"
#include "fadclib.h"
#include "fadcprot.h"
#include "my_utility.h"

//#define FADC_DEBUG

// __________________________________________________
void write_mem(int fd, unsigned int address, unsigned int word)
{
    int written_size;
    address = address | FADC_BASE_ADD;

    if ( lseek(fd, address, SEEK_SET) == -1) {
        perror("ERROR: lseek()");
        exit(1);
    }

    if ( (written_size = write(fd, &word, sizeof(word))) == -1) {
        perror("ERROR: write()");
        exit(1);
    }

}

// __________________________________________________
void read_mem(int fd, unsigned int address, unsigned int *word, unsigned int n_array)
{
    int read_size;
    unsigned int word_size = n_array * sizeof(unsigned int);
    address = address | FADC_BASE_ADD;

    if ( lseek(fd, address, SEEK_SET) == -1) {
        perror("ERROR: lseek()");
        exit(1);
    }

    // read(fd, pointer of array, size of read words[byte])
    if ( (read_size = read(fd, word, word_size)) == -1) { 
        perror("ERROR: read()");
        exit(1);
    }

}

// __________________________________________________
void set_run_mode(int fd, unsigned int run_ctrl_reg)
{
    unsigned int daq_status;

    write_mem(fd, ACQUISITION_CONTROL_ADD, run_ctrl_reg);

#ifdef FADC_DEBUG
    printf("FADC_DEBUG:[%s]\tCheck Run ctrl status: ", __FUNCTION__);
    read_mem(fd, ACQUISITION_CONTROL_ADD, &run_ctrl_reg, 1);
    int_2bit(run_ctrl_reg);
#endif

    printf("[%s]\tCheck board ready for acquisition ", __FUNCTION__);
    while(1) {
        read_mem(fd, ACQUISITION_STATUS_ADD, &daq_status, 1);
        if( (daq_status&BOARD_STAT)==BOARD_STAT ) {
            printf("... OK\n");
            break;
        }

#ifdef FADC_DEBUG
        printf("... NG\n\tdaq status: ");
        int_2bit(daq_status);
#endif
        ss_sleep(1);
    }

}

// __________________________________________________
void wait_fadc_trigger()
{
    int i = 0;
    unsigned int address;
    unsigned int daq_status;

    printf("[%s]\tWaiting for trigger coming & Data-read ready.\n", __FUNCTION__);

    while(1){ 
        read_mem(fadc_vme_fd, ACQUISITION_STATUS_ADD, &daq_status, 1);

        if( (daq_status&EVENT_READY)==EVENT_READY ) {
            printf("[%s]\tAt least One event ready to readout! ", __FUNCTION__);    
            break;
        }

#ifdef FADC_DEBUG
        if(i%100==0) {
            printf("... NG\n\tdaq status: ");
            int_2bit(daq_status);
        }
#endif
		ss_sleep(10);
        i++;
    }
    printf("... FADC FIRED!\n");
}

// __________________________________________________
void read_data_single32(int fd, unsigned int address, unsigned int *word, unsigned int read_n_array)
{
    int i=0;
    int read_size;
    unsigned int read_buff_size_once = MAX_READ_BUFF_SIZE;
    //unsigned int read_buff_size_once = 1;
    address = address | FADC_BASE_ADD;

#ifdef FADC_DEBUG
    printf("FADC_DEBUG:\t[%s]\tread_n_array:%d / read_n_array_once:%d\n",
           __FUNCTION__, read_n_array,read_buff_size_once);
#endif

    if ( lseek(fd, address, SEEK_SET) == -1) {
        perror("ERROR: lseek()");
        exit(1);
    }

    while( i<read_n_array ) {
        if ( (read_size = read(fd, word+i, read_buff_size_once)) == -1) {
            perror("ERROR: read()");
            exit(1);
        }
        //i += read_buff_size_once;
        i += (int)(read_buff_size_once/sizeof(unsigned int));
        //ss_sleep(10);
    }

};

/*
// Block Transfer method -> not ready
void read_data_blt(int fd, unsigned int address, unsigned int *word)
{
address = BLT_EVENT_NUMBER;
unsigned int blt_event_num;
read_mem(fd, address, &blt_event_num, 1);
printf("Including BLT event #:%d",blt_event_num);
}
*/

// __________________________________________________
int read_fadc_data(int fd, unsigned int *wdata)
{
    int i;

    int n_headers = 4;
    //unsigned int wdata[max_fadc_data];
    int magic_num;
    int event_size;
    int sampling_nbins;
    double sampling_time;
    short adc_val[2];
    short adc_offset = 0x0800; // temporary

    // check event size
    read_mem(fd, MEMORY_HEAD_ADD, &wdata[0], 1);
    magic_num = (wdata[0]>>28)&0xF;
    event_size = wdata[0]&0x0FFFFFFF;

#ifdef FADC_DEBUG
    printf("FADC_DEBUG:\t[%s]\tMagic number : %d\n", __FUNCTION__, magic_num);
    printf("FADC_DEBUG:\t[%s]\tEvent Size   : %d\n", __FUNCTION__, event_size);
#endif
    if(magic_num!=10) { // means 1010
        fprintf(stderr, "Error:\t[%s]\tInvalid magic number (%d) of FADC data ! (correct:10)\n", __FUNCTION__, magic_num);
        perror("Error : read_fadc_data : Invalid magic number of FADC data ! (correct:10)");
        exit(1);
    }

    // Single D32 readout method (simple consecutive readout)
    if( max_fadc_data < event_size ) {
        fprintf(stderr, "Error:\t[%s]\tSize of FADC array is too small to readout data. (%d < %d)\n", __FUNCTION__, max_fadc_data, event_size);
        perror("Error : read_fadc_data : Size of FADC array is too small to readout data.\n");
        exit(1);
    }
    read_data_single32(fd, MEMORY_HEAD_ADD, wdata+1, (event_size-1));
    
    sampling_nbins = event_size - n_headers;
    sampling_time = sampling_nbins*2. / used_fadc_ch * (1.e9/sampling_rate);

    // check read sampling data (simple decode)
#ifdef FADC_DEBUG
    for(i=0; i<event_size; i++) {
        // header part
        if(i<n_headers) {
            printf("FADC_DEBUG:[%s]\t%d : ", __FUNCTION__, i, wdata[i]);
            int_2bit(wdata[i]);
        }

        // Sampling part
        else {
            adc_val[0] = (wdata[i+n_headers] & 0x0FFF) - adc_offset;
            adc_val[1] = ((wdata[i+n_headers]>>16) & 0x0FFF) - adc_offset;
            printf("FADC_DEBUG:[%s]\t%d : %d  %d <= ",
                   __FUNCTION__, i, adc_val[1], adc_val[0]);
            int_2bit(wdata[i]);
        }
        //wdata_out[i] = wdata[i];
    }
#endif

    printf("[%s]\tSampling time width: %.f nsec\n", __FUNCTION__, sampling_time);

    return (event_size);

}

/*
  void clear_all_ch_config(int fd)
  {
  int i;
  for(i=0;i<8;i++) {
  write_mem(fd, CH_CONFIG_CLR_ADD, CHN_CONFIG_CLR(i));
  }
  }; // not use now...
*/

// __________________________________________________
void FADC_init(unsigned int fadc_offset)
{
    unsigned int word;
    int i;

    // -------------------- Init Bit3
    printf("[%s]\tFADC Bit3 initialization ", __FUNCTION__);
    if ((fadc_vme_fd = open(FADC_DEV_FILE, O_RDWR)) == -1) {
        perror("ERROR: open()");
        exit(1);
    }
    //printf("fadc_vme_fd:%d",fadc_vme_fd);
    printf("...OK\n");

    // -------------------- Software reset (return to default setting)
    write_mem(fadc_vme_fd, SOFTWARE_RESET_ADD, 1);

    // -------------------- set monitor mode -> Volt level mode
#ifdef FADC_DEBUG
    //write_mem(fadc_vme_fd, MONITOR_MODE_ADD, 0x4);
#endif

    // -------------------- change front TRG/CLK signal level NIM->TTL
    // NIM level signal is used by default
    // --> for S-IN Gate Run mode ( S-IN should be positive )
    // write_mem(fadc_vme_fd, FRONT_IO_CONTROL_ADD, 1);

    // Divide output buffer size
    // -> Similar to define buffer size / events
    // (FADC buffer not divided by event. Need to identify just by header.
    write_mem(fadc_vme_fd, BUFF_ORGANIZATION_ADD, 0x00);   // buffer=1024k sample, # of buffers=1
    //write_mem(fadc_vme_fd, BUFF_ORGANIZATION_ADD, 0x0A); // buffer=1k sample ,# of buffers=1024
    printf("[%s]\tSet divide output buffer size.\n", __FUNCTION__);

    // Set post-trigger sample
    unsigned int post_trigger_value = 0x40; // = 64
    //unsigned int post_trigger_value = 0x100;
    // # of post trigger = 4 * post_trigger_value + latency(offset?) [sample]
    write_mem(fadc_vme_fd, POST_TRIGGER_ADD, post_trigger_value);
    printf("[%s]\tSet Post-trigger sample : %d + latency sample\n", __FUNCTION__, post_trigger_value*4);

    // Enable to external trigger source
    write_mem(fadc_vme_fd, TRG_SOURCE_MASK_ADD, ENABLE_EX_TRG);
    printf("[%s]\tEnable external trigger source (from front panel TRG-IN)\n", __FUNCTION__);

    // channel mask
    write_mem(fadc_vme_fd, CH_ENABLE_MASK_ADD, ch_mask_reg);
    printf("[%s]\tSet FADC channel mask : ", __FUNCTION__);
    int_2bit(ch_mask_reg);

    // Add DC offset to input signal of each channel
#ifdef FADC_DEBUG
    for(i=0; i<used_fadc_ch; i++) {
        read_mem(fadc_vme_fd, CH_DAC_OFFSET_ADD(i), &word, 1);
        printf("FADC_DEBUG:[%s]\tDefault CH%d DC offset : ", __FUNCTION__, i);
        int_2bit(word);
    }
#endif
    //write_mem(fadc_vme_fd, CH_DAC_OFFSET_ADD(0), fadc_dc_offset);
    write_mem(fadc_vme_fd, CH_DAC_OFFSET_ADD(0), fadc_offset);
    
    for(i=0; i<used_fadc_ch; i++) {
        while(1) {
            read_mem(fadc_vme_fd, CH_STATUS_ADD(i), &word, 1);
            if( word!=CH_DAQ_BUSY ) {
                printf("[%s]\tAdd DC Offset to input signal (ch%d) ",__FUNCTION__, i);
                break;
            }
            ss_sleep(1);
        }
        read_mem(fadc_vme_fd, CH_DAC_OFFSET_ADD(i), &word, 1);
        printf("==> CH%d DC offset : 0x%x = ",i,fadc_offset); int_2bit(word);
    }

    // check channel configuration
#ifdef FADC_DEBUG
    read_mem(fadc_vme_fd, CH_CONFIG_ADD, &word, 1);
    printf("FADC_DEBUG:\t[%s]\tCheck channel configuration (default 0x10): ", __FUNCTION__);
    int_2bit(word);
#endif
}

// __________________________________________________
void FADC_daq_mode(int mode)
{
    if(mode==0) {
        //-> use external gate (SIN Gate)
        set_run_mode(fadc_vme_fd, (SIN_GATE_MODE|ACQUISITION_RUN));
        //set_run_mode(fadc_vme_fd, (SIN_GATE_MODE));
        printf("[%s]\tStart EXTERNAL GATE MODE DAQ\n", __FUNCTION__);
    }
    else if(mode==1) {
        // -> start/stop run via register 
        set_run_mode(fadc_vme_fd, REGISTER_CONTROLLED_MODE);
        printf("[%s]\tStart REGISTER-CONTROLLED MODE\n", __FUNCTION__);
    }
  
}

// __________________________________________________
void FADC_daq_start()
{
    write_mem(fadc_vme_fd, ACQUISITION_CONTROL_ADD, ACQUISITION_RUN);
    printf("[%s]\tFADC data aqcuisition\n", __FUNCTION__);
}

// __________________________________________________
void FADC_daq_stop()
{
    write_mem(fadc_vme_fd, ACQUISITION_CONTROL_ADD, ACQUISITION_STOP);
    printf("[%s]\tStop FADC data aqcuisition\n", __FUNCTION__);
}

// __________________________________________________
int FADC_Read(unsigned int *wdata)
{
    // readout data
    int read_event_size = read_fadc_data(fadc_vme_fd, wdata);
    // End readout -> Clear all buffer
    write_mem(fadc_vme_fd, SOFTWARE_CLEAR_ADD, 1);
    return (read_event_size);
}

// __________________________________________________
int FADC_close()
{
    printf("[%s]\tFADC closed\n", __FUNCTION__);
    return(close(fadc_vme_fd));
}
