#! /usr/bin/env python
# -*- coding : utf-8 -*-

import os
import sys
import time
import datetime
from optparse import OptionParser
import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='debug.log',
                    filemode='a')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

## __________________________________________________
class Config(object):
    Mode = 'dump'
    Nevent = 1000
    Thrd = -300
    LogFile = 'lastrun.txt'
    Interval = None

## __________________________________________________
class Daq(object):
    '''
    MizuDAQ wrapper.
    Enables to switch data taking easily.
    '''
    
    ## __________________________________________________
    def __init__(self, datadir, nevent, logfile, thrd):
        self.datadir = datadir
        self.nevent = nevent
        self.thrd = thrd
        self.logfile = logfile
        self.logger = logging.getLogger('Daq')
        self.logger.debug('initialized')

    ## __________________________________________________
    def __str__(self):
        s = 'Last run# log : "{0}"\n'.format(self.logfile)
        s += 'Save data to  : "{0}"\n'.format(self.datadir)
        s += '# of events   : {0:4d} events / run\n'.format(self.nevent)
        s += 'ATM threshold : {0:4d} mV\n'.format(self.thrd)
        return s

    ## __________________________________________________
    def get_last_run(self, ifn):
        '''
        read last run# from the input file.
        if the input file does not exist, (in case of runnning DAQ in new directory),
        create new log file with '0' written to it
        '''
        if not os.path.exists(ifn):
            self.logger.error('file "{0}" does not exist.'.format(ifn))
            self.logger.warning('Do you want to make new one ? > (y/[n])')
            ans = raw_input('')
            if ans in ['y', 'Y']:
                self.set_last_run(ifn, 0)
            sys.exit(-1)

        self.logger.info('Read "{0}".'.format(ifn))
        with open(ifn, 'r') as fin:
            lines = fin.readlines()
        return int(lines[0])

    ## __________________________________________________
    def set_last_run(self, ofn, run):
        '''
        write run# to output file
        '''
        line = '{0}\n'.format(run)
        self.logger.info('Write "{0}".'.format(ofn))
        with open(ofn, 'w') as fout:
            fout.write(line)
        return
    
    ## __________________________________________________
    def run(self):
        func = '[run]\t'
        
        fn = self.logfile
        srun = self.get_last_run(fn) + 1

        argv = {'datadir': self.datadir,
                'nevent': self.nevent,
                'thrd': self.thrd,
                'run': srun
                }

        com = './beam_poll {0[datadir]} {0[run]} {0[nevent]} {0[thrd]}'.format(argv)
        try:
            self.logger.info('Run "{0}".'.format(com))
            res = os.system(com)
            self.set_last_run(fn, srun)
            if (res==-1):
                sys.exit(-1)
        except KeyboardInterrupt:
            self.logger.warning('"Ctrl-C" was pressed. DAQ stopped.')
            self.set_last_run(fn, srun)
            sys.exit(-1)
        except:
            self.logger.error('Unknown failure. DAQ stopped.')
            sys.exit(-1)

    ## __________________________________________________
    def loop(self, nrun, interval):
        '''
        '''
        try:
            for i in range(nrun):
                self.run()
                if interval:
                    d = datetime.datetime.now()
                    now = d.strftime('%Y/%m/%d %H:%M:%S')
                    self.logger.info('DAQ sleep for {0} sec since {1}'.format(interval, now))
                    time.sleep(int(interval))
        except KeyboardInterrupt:
            self.logger.warning('"Ctrl-C" was pressed. DAQ stopped.')
            sys.exit(-1)
            
## __________________________________________________        
if __name__ == '__main__':

    usage = '%prog [options] nrun'
    usage += '\n'
    usage += '  Ex1) %prog -b MR49 1000\n'
    usage += '  Ex2) %prog -n 130508 -i 1800 1000\n'
    usage += "  Ex3) %prog -l 130508 -t '-100' 1000\n"
    usage += "  Ex4) %prog\n"
    usage += '\n'
    parser = OptionParser(usage)

    parser.add_option('-b',
                      dest   = 'beam',
                      metavar = 'DATE',
                      help   = 'Run DAQ in beam measurment mode')
    parser.add_option('-n',
                      dest   = 'noise',
                      metavar = 'DATE',
                      help   = 'Run DAQ in noise measurement mode')
    parser.add_option('-c',
                      dest   = 'cosmic',
                      metavar = 'DATE',
                      help   = 'Run DAQ in cosmic measurement mode')
    parser.add_option('-l',
                      dest   = 'ledcalib',
                      metavar = 'DATE',
                      help   = 'Run DAQ in LED calibration mode')
    parser.add_option('-q',
                      dest   = 'qtcalib',
                      metavar = 'DATE',
                      help   = 'Run DAQ in QT calibration mode')
    parser.add_option('-f',
                      dest   = 'fadc',
                      metavar = 'DATE',
                      help   = 'Run DAQ in FADC test mode')
    parser.add_option('-p',
                      dest   = 'ped',
                      metavar = 'DATE',
                      help   = 'Run DAQ in pedestal only')
    parser.add_option('-t',
                      dest   = 'thrd',
                      type = 'int',
                      help   = 'set ATM hit threshold.')
    parser.add_option('-i',
                      dest   = 'interval',
                      type = 'int',
                      help   = 'set interval second between nruns.')

    parser.set_defaults(beam = None,
                        noise = None,
                        cosmic = None,
                        ledcalib = None,
                        qtcalib = None,
                        fadc = None,
                        ped = None,
                        thrd = Config.Thrd,
                        interval = None)
                        
    (options, args) = parser.parse_args()

    if len(args) > 1:
        parser.error('Too many arguments ({0})\n'.format(len(args)))
        
    nrun = 1
    if len(args) > 0:
        nrun = int(args[0])

    mode = Config.Mode
    nevent = Config.Nevent

    if options.beam:
        mode = 'beam'
        date = options.beam
    elif options.noise:
        mode = 'noise'
        date = options.noise
    elif options.ledcalib:
        mode = 'led'
        date = options.ledcalib
        nevent = 100
    elif options.qtcalib:
        mode = 'qtcalib'
        nevent = 100
    elif options.cosmic:
        mode = 'cosmic'
        nevent = 100
    elif options.fadc:
        mode = 'fadc'
        nevent = 100
    elif options.ped:
        mode = 'ped'
        d = os.path.join(d, options.ped)
        nevent = 100
    else:
        date = ''
        nevent = 100

    datadir = os.path.normpath(os.path.join('exp', mode, date))
    logfile = os.path.join(datadir, Config.LogFile)

    daq = Daq(datadir, nevent, logfile, thrd=options.thrd)

    ### Check the last run number and also the existence of the logfile
    srun = daq.get_last_run(logfile)

    s = '\n' + '-' * 50 + '\n'
    s += 'DAQ mode      : "{0}"\n'.format(mode)
    s += 'Save data to  : "{0}"\n'.format(daq.datadir)
    s += 'Last run# log : "{0}"\n'.format(daq.logfile)
    s += 'ATM threshold : {0:4d} mV\n'.format(daq.thrd)
    s += 'Interval      : {0} sec\n'.format(options.interval)
    s += '# of events   : {0:4d} events / run\n'.format(daq.nevent)
    s += '# of runs     : {0:4d} runs\n'.format(nrun)
    s += 'Run# from     : {0:4d}\n'.format(srun+1)
    s += '-' * 50 + '\n'

    daq.logger.info(s)
    sys.stderr.write('Is parameter above correct ? (y/[n]) > ')
    ans = raw_input('')
    if len(ans) > 1:
        ans = ans[0]
    
    if ans in ['y', 'Y']:
        try:
            daq.loop(nrun, interval=options.interval)
        except KeyboardInterrupt:
            daq.logger.error('"Ctrl-C" was pressed. DAQ stopped.')
            sys.exit(-1)
    else:
        daq.logger.error('\tQuit. Bye Bye.')
        sys.exit(-1)
        
