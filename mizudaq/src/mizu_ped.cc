#include <iostream>
#include <unistd.h>
#include <csignal>
#include <time.h>
#include <sys/time.h>

#include <TFile.h>
#include <TTree.h>

#define TIMEOUT 10

extern "C" {
  #include "fadcprot.h"
  #include "tkoprot.h"
  #include "trgprot.h"
}

typedef struct {
  int runtype;
  int trgnum;
  //int fadc_size;
  int atm_size;
  //unsigned int fadc_wdata[2000];
  int atm_wdata[2000];
} raw_data_struct;

static raw_data_struct r_data_dummy;

#include "../vmeioreg/src/VmeIoreg.cc"
static const unsigned int VIOREG_BASEADDR = 0xF000;
VmeIoreg vioreg( VIOREG_BASEADDR );

void start_veto_latch()
{
  vioreg.level_out( 0x01 ); 
}

void stop_all_latch()
{
  vioreg.level_out( 0 );       // end latch
}

void output_pedestal_trigger()
{
  vioreg.level_out( 0x80 );
  vioreg.level_out( 0 );
}


#define n_used_smp 1
const int smp_num[n_used_smp] = { 0 };
const int all_atms_adc_time = 1000; 
//= 14borads * 12ch *~5.5usec = 924 usec

#define SDS_USE_GOBIT_TRG 0x10

TTree *tree;

void terminate(int dummy)
{
  TKO_close();
  TRG_close();
  //FADC_close();

  TFile fout("mizu_calib.root","recreate");
  fout.cd();
  tree->Write();
  fout.Close();

  exit(1);
}

double gettimeofday_usec()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return (double)(tv.tv_sec*1.e6 + tv.tv_usec);
}

double gettimeofday_sec()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return (double)(tv.tv_sec + tv.tv_usec*1.e-6);
}


void set_tree(TTree *tree, raw_data_struct &r_data)
{
  tree->Branch("runtype", &(r_data.runtype), "runtype/I");
  tree->Branch("trgnum", &(r_data.trgnum), "trgnum/I");
  //tree->Branch("fadc_size", &(r_data.fadc_size), "fadc_size/I");
  //tree->Branch("fadc_wdata", (r_data.fadc_wdata), "fadc_wdata[fadc_size]/i");
  tree->Branch("atm_size", &(r_data.atm_size), "atm_size/I");
  tree->Branch("atm_wdata", (r_data.atm_wdata), "atm_wdata[atm_size]/I");

  std::cout << "Set Output tree branch" << std::endl;
}


////
int main(int argc, char* argv[])
{
  if(argc!=3) {
    printf("usage : (program) (#_of_cycle) (run type)\n");
    exit(1);
  }

  int cycle = atoi(argv[1]);
  std::cout << "input cycle : " << cycle << std::endl;

  int event_thr = 3;

  raw_data_struct r_data = r_data_dummy;

  tree = new TTree("mizu_raw","mizuche raw data");
  set_tree(tree, r_data);

  double start, stop;
  double wait_time,veto_time;


  // at Ctrl-C stop
  signal( SIGINT, terminate );

  // Initialization of all modules
  TKO_open(n_used_smp);

  TRG_open();
  TRG_Command( TRG_TRGENA );

  //FADC_init();

  // Set TKO mode
  if( atoi(argv[2])==2 ) {
    set_tko_pedestal_mode(smp_num[0]);
    start_tko_pedestal(smp_num[0]);
  }
  else {
    set_tko_operate_mode(smp_num[0]);
  }

  // Start event loop
  for(int icycle=0; icycle<=cycle; icycle++) {

        // start timer
    start = gettimeofday_sec();

    // waitevent (until limit event# or size)
    printf("%dth cycle start\n",icycle);
    TKO_waitevent(TIMEOUT, event_thr);

    // stop FADC data-taking
    //FADC_daq_stop();

    // readout data of FADC
    //r_data.fadc_size = FADC_data_read( &(r_data.fadc_wdata[0]) );
  
    // stop timer
    stop = gettimeofday_sec();

    // wait enough time for AD conversion of all ATM
    wait_time = stop - start;
    std::cout << "wait time in waitevent [sec] :" << wait_time << std::endl;

    // readout data of ATMs
    printf("Start of TKO SDS ...\n");
    TKO_sds_go( 0, SDS_USE_GOBIT_TRG );
    r_data.atm_size = read_tko_data( &(r_data.atm_wdata[0]) );

    // readout trigger #
    //r_data.trgnum = TRG_dump();
    //std::cout << "TRG FIFO Count : " << TRG_FIFO_Count() << std::endl;

    // Fill data of this event
    tree->Fill();

    // Stop VETO latch
    //stop_all_latch();

  }

////// End of Run //////
  // close all modules;
  TKO_close();
  TRG_close();
  //FADC_close();

  TFile fout("mizu_calib.root","recreate");
  fout.cd();
  tree->Write();
  fout.Close();

};
