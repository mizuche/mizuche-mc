/* daq.c */
/* Edited by T.Kikawa */
/* modified for larger samplling by A.K.Ichikawa */
/* modified by akira.m */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include "vmedrv.h"

#define FADC_DEBUG

//
#define DEV_FILE "/dev/vmedrv24d32"
#define FADC_BASE_ADD     0x00300000  // for v24

// at d32, word size should be 32 bit -> word = unsigned int
// at d16, word size should be 16 bit -> word = u_short

//
const double sampling_rate = 250e6;
//const double post_trg_time = 500;  // sample
//const unsigned int const_latency = 100;  // sample, temporary

const unsigned int num_h_word = 4;
const unsigned int max_event_size = 10000; // temporary

// Address map (General)
#define MEMORY_HEAD_ADD           0x0000
#define MAX_READ_BUFF_SIZE   0x0FFC
// EVENT READOUT BUFFER : 0x0000 ~ 0x0FFC

#define CH_CONFIG_ADD             0x8000
#define CH_CONFIG_CLR_ADD         0x8008
#define BUFF_ORGANIZATION_ADD     0x800C
#define ACQUISITION_CONTROL_ADD   0x8100
#define ACQUISITION_STATUS_ADD    0x8104
#define TRG_SOURCE_MASK_ADD       0x810C
#define POST_TRIGGER_ADD          0x8114
#define CH_ENABLE_MASK_ADD        0x8120
#define SET_MONITOR_DAC_ADD       0x8138
#define MONITOR_MODE_ADD          0x8144
#define FRONT_IO_CONTROL_ADD      0x811C
#define BLT_EVENT_NUMBER_ADD      0xEF1C
#define SOFTWARE_RESET_ADD        0xEF24
#define SOFTWARE_CLEAR_ADD        0xEF28

// Address map about each channel ("n" means channel#)
#define CH_ZS_THRE_ADD(n)         0x1024|(n<<8)
#define CH_THRESHOLD_ADD(n)       0x1080|(n<<8)
#define CH_STATUS_ADD(n)          0x1088|(n<<8)
#define CH_DAC_OFFSET_ADD(n)      0x1098|(n<<8)

// Channel mask
#define CH_NOT_MASK(n) (1<<n)
#define CH_MASK(n)     (0<<n)
const unsigned int ch_mask_reg =
  CH_NOT_MASK(0) | CH_MASK(1) | CH_MASK(2) |
  CH_MASK(3) | CH_MASK(4) | CH_MASK(5) |
  CH_MASK(6) | CH_MASK(7);
const unsigned int used_ch = 1;  // use only channel #0

// channel config
#define FULL_SUPPRESS       (1<<16)|(1<<17)
#define MEM_SEQUENT_ACCESS  (1<<4)

const unsigned int fadc_dc_offset = 0x8300; // [bit]
// this offset is temporary adjustment of pedestal level
//
// 16bit DAC allows to add DC offset to input signal (+-1V range)
// FSR = Full Scale Range (16bit) correspond to 2V range
// default DC offset = FSR/2 = 0x8000 (15bit)
// negative uniporlar (-2V~0V): offset = 0 bit
// bipolar (-1V~1V) : FSR/2
// positibe uniporlar (0V~2V) : offset = FSR


#define CHN_CONFIG_CLR(n)       (1<<n)

// Channel Status
#define CH_MEMORY_EMPTY (1<<1)
#define CH_MEMORY_FULL  (1<<0)
#define CH_DAQ_BUSY     (1<<2)

// Buffer organization code
#define BUFF_DIV_REG       0x01
//-> one buffer size = 512k sample, # of buffer = 2

// Acquisition Control
#define ACQUISITION_RUN     (1<<2)
#define ACQUISITION_STOP    (0<<2)
#define SIN_CONTROLLED_MODE (1<<0)
#define SIN_GATE_MODE       (1<<1)

#define BOARD_STAT          (1<<8)
#define EVENT_FULL          (1<<4)
#define EVENT_READY         (1<<3)
#define RUN_ON_OFF          (1<<2)


////
void show_with_2bit(unsigned int val)
{
  int i;
  unsigned int tmp[32];

  for(i=0; i<32; i++) {
    tmp[i] = (val>>i) & 0x1;
  }
  for(i=0; i<32; i++){
    if(i>0&&i%4==0) printf(" ");
    printf("%d",tmp[31-i]);
  }
  printf("\n");

};


////
void write_mem(int fd, unsigned int address, unsigned int word)
{
  int written_size;
  address = address | FADC_BASE_ADD;

  if ( lseek(fd, address, SEEK_SET) == -1) {
    perror("ERROR: lseek()");
    exit(EXIT_FAILURE);
  }

  if ( (written_size = write(fd, &word, sizeof(word))) == -1) {
    perror("ERROR: write()");
    exit(EXIT_FAILURE);
  }

};

void read_mem(int fd, unsigned int address, unsigned int *word, unsigned int n_array)
{
  int read_size;
  unsigned int word_size = n_array * sizeof(unsigned int);
  address = address | FADC_BASE_ADD;

  if ( lseek(fd, address, SEEK_SET) == -1) {
    perror("ERROR: lseek()");
    exit(EXIT_FAILURE);
  }

  if ( (read_size = read(fd, word, word_size)) == -1) { 
    perror("ERROR: read()");
    exit(EXIT_FAILURE);
  }
};

void set_run_mode(int fd, unsigned int run_ctrl_reg)
{
  unsigned int daq_status;

  write_mem(fd, ACQUISITION_CONTROL_ADD, run_ctrl_reg);
  printf("External gate(S_IN) run mode.\n");

#ifdef FADC_DEBUG
  printf("Check Run ctrl status: ");
  read_mem(fd, ACQUISITION_CONTROL_ADD, &run_ctrl_reg, 1);
  show_with_2bit(run_ctrl_reg);
#endif

  printf("Check board ready for acquisition\n");
  while(1) {
    read_mem(fd, ACQUISITION_STATUS_ADD, &daq_status, 1);

    if( (daq_status&BOARD_STAT)==BOARD_STAT ) {
      printf(" -> OK\n");
      break;
    }

#ifdef FADC_DEBUG
    printf("...daq status: ");
    show_with_2bit(daq_status);
#endif

    usleep(1);
  }
}

void wait_trigger(int fd)
{
  int i = 0;
  unsigned int address;
  unsigned int daq_status;

  printf("Waiting for trigger coming & Data-read ready.\n");

  while(1){ 
    read_mem(fd, ACQUISITION_STATUS_ADD, &daq_status, 1);

    if( (daq_status&EVENT_READY)==EVENT_READY ) {
      printf("One event ready !\n");    
      break;
    }

    if(i%20==0) {
#ifdef FADC_DEBUG
      printf("daq status: ");
      show_with_2bit(daq_status);
#endif
      printf("...waiting\n");
    }

    usleep(500);
    i++;

    if(i>=8000) { // means 4 sec
      printf("timeout of waitng trigger !\n");
      exit(1);
    }
  }
  printf("fire!\n");
}

void read_data_single32(int fd, unsigned int address, unsigned int *word, unsigned int read_n_array)
{
  int i=0;
  int read_size;
  unsigned int read_n_array_once = MAX_READ_BUFF_SIZE;
  address = address | FADC_BASE_ADD;

  //printf("read_n_array_once:%d\n",read_n_array_once);

  if ( lseek(fd, address, SEEK_SET) == -1) {
    perror("ERROR: lseek()");
    exit(EXIT_FAILURE);
  }

  while( i<read_n_array ) {
    if ( (read_size = read(fd, word+i, read_n_array_once)) == -1) {
      perror("ERROR: read()");
      exit(EXIT_FAILURE);
    }
    i += read_n_array_once;
  }
};

/*
// Block Transfer method -> not ready
void read_data_blt(int fd, unsigned int address, unsigned int *word)
{
  address = BLT_EVENT_NUMBER;
  unsigned int blt_event_num;
  read_mem(fd, address, &blt_event_num, 1);
  printf("Including BLT event #:%d",blt_event_num);
}
*/

void read_fadc_data(int fd, unsigned int *data_word)
{
  int i;

  unsigned int h_word[num_h_word];
  unsigned int magic_num;
  unsigned int event_size;
  unsigned int event_counter;
  unsigned int store_n_arrays;

  double sampling_time;
  short adc_val[2];
  short adc_offset = 0x0800;
  
  // check header component
  read_mem(fd, MEMORY_HEAD_ADD, h_word, num_h_word);

  magic_num = (h_word[0]>>28)&0xF;
  event_size = h_word[0]&0x0FFFFFFF;
  event_counter = h_word[2]&0x00FFFFFF;

  printf("Magic# (1010 is correct) : "); show_with_2bit(magic_num);
  printf("Event Size : %d\n",event_size);
  printf("Event Counter : %d\n",event_counter);
  printf("TRG TIME TAG : %d\n",h_word[3]);

  // Single D32 readout method (simple consecutive readout)
  store_n_arrays = event_size - num_h_word;
  read_data_single32(fd, MEMORY_HEAD_ADD, data_word, store_n_arrays);

  sampling_time = store_n_arrays*2. / used_ch * (1.e9/sampling_rate);

  // check read sampling data (simple decode)
  for(i=0; i<store_n_arrays; i++) {
  //for(i=0; i<200; i++) {

    // decode data
    adc_val[0] = (data_word[i] & 0x0FFF) - adc_offset;
    adc_val[1] = ((data_word[i]>>16) & 0x0FFF) - adc_offset;

    printf("%d : %d  %d <= ",i, adc_val[1], adc_val[0]);
    show_with_2bit(data_word[i]);
  }

  printf("Sampling time width: %.f nsec\n",sampling_time);

};

/*
void clear_all_ch_config(int fd)
{
  int i;
  for(i=0;i<8;i++) {
    write_mem(fd, CH_CONFIG_CLR_ADD, CHN_CONFIG_CLR(i));
  }
}; // not use now...
*/


//##########################################################

int main(int argc, char* argv[])
{
  if(argc!=2) {
    printf("usage : (program) (#_of_cycle)\n");
    exit(1);
  }

  int fd,fevent;
  unsigned int word;
  unsigned int data_word[max_event_size];

  int cycle = atoi(argv[1]);
  
//#### Bit3 initialize
  if ((fd = open(DEV_FILE, O_RDWR)) == -1) {
    perror("ERROR: open()");
    exit(EXIT_FAILURE);
  }
  printf("FADC-Bit3 initialization OK\n");

//#### Software reset (return to default setting)
  write_mem(fd, SOFTWARE_RESET_ADD, 1);

//#### set monitor mode -> Volt level mode
#ifdef FADC_DEBUG
  write_mem(fd, MONITOR_MODE_ADD, 0x4);
#endif

//#### change front TRG/CLK signal level NIM->TTL
// default use NIM level signal
// -> for S-IN Gate Run mode ( S-IN should be positive )
  write_mem(fd, FRONT_IO_CONTROL_ADD, 1);

//#### Set post trigger time
/*
  if(post_trg_time < const_latency) {
    printf("invalid post trigger time, const latency !\n");
    exit(1);
  }
  word = (post_trg_time - const_latency) / 4;
  // -> the calc of FADC document about post trigger setting

  write_mem(fd, POST_TRIGGER_ADD, post_trg_val);
  printf("Post Trigger Sample Number is setted.\n");
*/
  
//#### Divide output buffer size (mainly for block transfer)
  write_mem(fd, BUFF_ORGANIZATION_ADD, BUFF_DIV_REG);
  printf("Set divide output buffer size.\n");

//#### channel mask
// at Mizuche, use only channel# 0
  write_mem(fd, CH_ENABLE_MASK_ADD, ch_mask_reg);
  printf("CH mask is setted : ");
  show_with_2bit(ch_mask_reg);
 
//#### enable channel 0 trigger
// use with "trigger source" setting.
/*
  write_mem(fd, TRG_SOURCE_MASK_ADD, ch_mask_reg);
  printf("Channel 0 trigger is enabled.\nStart data datking.\n");
*/

//#### Add DC offset to input signal of each channel
#ifdef FADC_DEBUG
  read_mem(fd, CH_DAC_OFFSET_ADD(0), &word, 1);
  printf("Default CH0 DC offset : "); show_with_2bit(word);
#endif

  write_mem(fd, CH_DAC_OFFSET_ADD(0), fadc_dc_offset);

  while(1) {
    read_mem(fd, CH_STATUS_ADD(0), &word, 1);
    if( word!=CH_DAQ_BUSY ) {
      printf("Add DC Offset to input signal\n");
      break;
    }
    usleep(10);
  }
#ifdef FADC_DEBUG
  read_mem(fd, CH_DAC_OFFSET_ADD(0), &word, 1);
  printf(" -> CH0 DC offset : "); show_with_2bit(word);
#endif

//#### channel trigger threshold
/*
  word = 0x0FFF;
  write_mem(fd, CH_THRESHOLD_ADD(0), word);
  printf("Trigger threshold is setted.\n");
*/

//#### ZS AMP (negative logic)
/*
  word = FULL_SUPPRESS | MEM_SEQUENT_ACCESS;
  write_mem(fd, CH_CONFIG_ADD, word);
  
  word = (1<<31); // negative
  write_mem(fd, CH_ZS_THRE_ADD(0), word);
*/

//#### check channel configuration
#ifdef FADC_DEBUG
  read_mem(fd, CH_CONFIG_ADD, &word, 1);
  printf("Ch0 config (default 0x10): "); show_with_2bit(word);
#endif

/*
  read_mem(fd, CH_ZS_THRE_ADD(0), &word, 1);
  printf("Ch0 ZM_THRE : ");show_with_2bit(word);
*/

//#### start measurement
  for(fevent=1; fevent<=cycle; fevent++) {

  //#### Set Run mode & run start
    set_run_mode(fd, (SIN_GATE_MODE|ACQUISITION_RUN) );
    //-> use external gate (SIN Gate)
    printf("%dth event start!!!\n",fevent);

  //#### wait trigger or gate (polling)
    wait_trigger(fd);
    
  //#### run stop
    write_mem(fd, ACQUISITION_CONTROL_ADD, ACQUISITION_STOP);
    printf("Stop %dth event data aqcuisition\n",fevent);

  //#### read data
    read_fadc_data(fd, data_word);

  //#### End readout -> Clear all buffer
    write_mem(fd, SOFTWARE_CLEAR_ADD, 1);

  } // end of fevent loop

/*
  // debug output of read data voltage from MOM-LEMO output
  word = data_word[200] & 0x0FFF;
  write_mem(fd, SET_MONITOR_DAC_ADD, word);
*/

//#### End of mearsurement
  close(fd);
  printf("DAQ ended successfully!!\n");

  return 0;
};
