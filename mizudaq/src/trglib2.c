
#include "trglib2.h"
#include "trgprot2.h"

#include "vmedrv.h"


unsigned short TRG_dump()
{
  unsigned short i,j;
  TRG_INFO trginfo;

  printf("Status                  : %2x\n",ptrg->csr&0xff);
  i = ptrg->fifo_ctr & 0xfff;
  printf("# of events in the FIFO : %2x\n",i);

  for ( j = 0 ; j < i ; j++){
    trginfo.event_no = ptrg->event_no;
    trginfo.trig_id  = ptrg->trig_id;
    trginfo.clk48    = (ptrg->clk48h * 65536 * 65536);
    trginfo.clk48   += (ptrg->clk48m * 65536);
    trginfo.clk48   += (ptrg->clk48l);

    printf("Event #%5d : Trig. ID %2x : 48bit CLK : %lf usec \n",
         trginfo.event_no,trginfo.trig_id,trginfo.clk48*20.*1.e3);
  }

  return (trginfo.event_no);
}

void TRG_reset()
{
  printf("Status before reset     : %2x\n",ptrg->csr&0xff);
  ptrg->csr |= 0xf0;
  ptrg->trig_mask = 0x00;
  printf("Status after reset      : %2x\n",ptrg->csr&0xff);

}

void TRG_enable()
{
  printf("Status before enable     : %2x\n",ptrg->csr&0xff);
  ptrg->csr |= 0x02;
  printf("Status after enable      : %2x\n",ptrg->csr&0xff);
}

void TRG_open()
{
  int           size             = TRG_MAP_SIZE;
  off_t         trg_base_offsets = TRG_BASE;
  off_t         map_base, offset;
  
  btd = open(a16_logical_name,O_RDWR);

  if (btd >0){

    map_base = trg_base_offsets & PAGE_MASK;
    offset   = trg_base_offsets  - map_base;
    map_size = size + offset;

    trg_vme_base = mmap(0, map_size, PROT_WRITE, MAP_SHARED, btd, map_base);
    if (trg_vme_base == MAP_FAILED) {
      perror("ERROR: mmap()");
      exit(1);
    }
    ptrg = (TRG_REGS_STR*)(trg_vme_base + offset);
  }
  else{
    fprintf(stderr, "Cannot open TRG\n");
    exit(1);
  }

}

void TRG_close()
{
  if( munmap((void*)trg_vme_base, map_size)!=0) {
    perror("Unmapping:");
    exit(1);
  }
  close(btd);
}
