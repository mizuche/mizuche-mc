#include <stdio.h>
#include "../include/mizu_data.h"


const short fadc_offset = 0x0800;

void decode_fadc(MIZURAW *raw)
{
  int i;

  raw->fadc_magic = ((raw->fadc_raw[0])>>28)&0xF;
  raw->fadc_event_size = (raw->fadc_raw[0])&0x0FFFFFFF;
  raw->fadc_board_info = (raw->fadc_raw[1]);
  raw->fadc_event_count = (raw->fadc_raw[2])&0x00FFFFFF;
  raw->fadc_trg_time = (raw->fadc_raw[3]);

  raw->nsamp = ( (raw->fadc_len) - n_fadc_head ) * 2;
  for(i=0;i<((raw->fadc_len) - n_fadc_head);i++) {

    raw->fadc[2*i] = ( (raw->fadc_raw[i]) & 0x0FFF) - fadc_offset;
    raw->fadc[2*i+1] = ( ((raw->fadc_raw[i])>>16) & 0x0FFF) - fadc_offset;
  }

  printf("decode fadc data !\n");

};

void decode_atm(MIZURAW *raw)
{
  int i;
  int high_bit,low_bit;
  int ma_num,sa_num;
  int tag;
  int event_num, ch,type,adc_data;
  
  unsigned int word;

  raw->nhits = 0;

  if( (raw->atm_len)/4 >+ 8000) {
    printf("too long atm data length!\n");
    exit(1);
  }

  for (i=0; i<(raw->atm_len); i++) {
    
    word = raw->atm_raw[i];

    high_bit = (word>>16)&0xFFFF;
    low_bit = word&0xFFFF;

    ma_num = (high_bit>>11)&0x1F;
    sa_num = high_bit&0x7FF;
    tag = (low_bit>>14)&0x3;

    event_num = (low_bit>>6)&0xFF;
    type = (low_bit>>5)&0x1;
    ch = low_bit&0xF;
    adc_data = low_bit&0x0FFF; 
      
    if( i==0 ) {
      printf("serial number : %d\n", high_bit);
      printf("# of atm data word: %d\n", low_bit);
    }

    else if (high_bit==0) {
      raw->spillnum = low_bit;
    }

    else {
      if( ma_num>0 && ma_num<24 ) {

        if(tag==0) {
          raw->atm_id[raw->nhits] = type;
          raw->atm_ch[raw->nhits] = ch;
        }

        else if(tag==1) {
          raw->atm_tdc[raw->nhits] = adc_data;
        }

        else if(tag==2) {
          raw->atm_qdc[raw->nhits] = adc_data;
          raw->nhits++;
        }

      }
    }

  } // end of for loop

  printf("decode atm data !\n");

};
