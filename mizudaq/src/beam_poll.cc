#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <assert.h>
#include <poll.h>
#include <sys/ioctl.h>

#include <TH1.h>
#include <TSocket.h>
#include <TServerSocket.h>
#include <TSystem.h>
#include <TMessage.h>
#include <TObjString.h>
#include <TMonitor.h>
#include <TThread.h>
#include <TList.h>
#include <TStopwatch.h>

extern "C" {
#include "fadcprot.h"
#include "tkoprot.h"
#include "trgprot.h"
#include "my_utility.h"
}

#include "TRawData.h"
#include "T2KWCRaw.h"

TRawData *raw_data;
//mizu_raw_data r_data;
//mizu_info info;
TRG trg0[MAX_nTRG];

SMPREG smp_reg;

static mizu_raw_data r_data_dummy; // dummy

TList client_list;

// ---------- SMP ----------
#define n_used_smp 1
const int smp_num[n_used_smp] = { 0 };

// --------------------
#include "VmeIoreg.h"
#define VIOREG_BASEADDR 0xF000
VmeIoreg vioreg( VIOREG_BASEADDR );

// --------------------
#include "ireg.h"
#define VIREG_BASEADDR 0x010000
ireg vireg(VIREG_BASEADDR);

// ---------- DEBUG FLAGS ----------
//#define DEBUG      // Show ALL DEBUG messages
//#define DEBUG2     // More precise messages
//#define DEBUG_TRG  // Show DEBUG messages related to TRG data
//#define DEBUG_ATM  // Show DEBUG messages related to ATM data
//#define DEBUG_FADC // Show DEBUG messages related to FADC data
//#define DEBUG_DAQ  // Show DEBUG messages related to main DAQ
//#define DEBUG_RAW  // Show DEBUG messages related to RAW data
//#define DEBUG_SEND   // Show DEBUG messages related to sending process


// __________________________________________________
// functions for socket process
int is_closed(int fd)
{
#if defined(DEBUG) || defined(DEBUG_SEND)
    printf("DEBUG_SEND:\t[%s]\tfd : %d\n", __FUNCTION__, fd);
#endif

    pollfd fds[1];
    fds[0].fd = fd;
    fds[0].events = POLLERR | POLLHUP | POLLNVAL;
    fds[0].revents = 0;

    int ret = poll(fds, 1, 0);

    if (ret == 1 && (fds[0].revents & POLLHUP))
        return 1;

    return 0;
}

// __________________________________________________
void send_it(TObject * item)
{
#if defined(DEBUG) || defined(DEBUG_SEND)
    printf("DEBUG_SEND:\t[%s]\n", __FUNCTION__);
#endif
    
    assert(item);
    TMessage msg(kMESS_OBJECT);
    msg.WriteObject(item);

//    Int_t len = msg.CompLength();

    TIter next(&client_list);
    while (TObject * o = next()) {
        assert(o->IsA() == TSocket::Class());
        TSocket * s = (TSocket *)o;
        if (!s->IsValid()) {
            printf("[%s]\t'%s' is invalid, closed\n",
                   __FUNCTION__, s->GetInetAddress().GetHostName());
            client_list.Remove(s);
            continue;
        }
        
        if (is_closed(s->GetDescriptor())) {
            printf("[%s]\t'%s' closed connection, remove the client\n",
                   __FUNCTION__, s->GetInetAddress().GetHostName());
            client_list.Remove(s);
        }

        /*
          if (sendq(s->GetDescriptor()) > len * 2)
          continue;
        */
        if (-1 == s->Send(msg)) {
            printf("[%s]\tError sending data to '%s', remove the client\n",
                   __FUNCTION__, s->GetInetAddress().GetHostName());
            client_list.Remove(s);
        }
    }
}

// __________________________________________________
void acceptor(void * arg)
{
    
    //
    //TThread::SetCancelOn();

    TServerSocket * socket = (TServerSocket *)arg;
    assert(socket);
    assert(socket->IsA() == TServerSocket::Class());

    printf("[%s]\tacceptor starts\n", __FUNCTION__);

    while (TSocket * client_socket = socket->Accept()) {
        client_list.Add(client_socket);
        printf("[%s]\tNew client arrive. # of client is %d\n",
               __FUNCTION__, client_list.GetEntries());
    }
}


// __________________________________________________
void read_fadc_data(mizu_raw_data &r_data)
{
    r_data.fadc.nFADC = FADC_Read(r_data.fadc.FADC);
#if defined(DEBUG) || defined(DEBUG_FADC)
    printf("DEBUG_FADC:\t[%s]\tnFADC : %d\n", __FUNCTION__, r_data.fadc.nFADC);
#endif    
}

// __________________________________________________
void read_trg_data(mizu_raw_data &r_data)
{
    r_data.trg.nTRG = TRG_Read( trg0, MAX_nTRG);
#if defined(DEBUG) || defined(DEBUG_TRG)
    fprintf(stderr, "DEBUG_TRG:\t[%s]\tTRG count : %d\n", __FUNCTION__, r_data.trg.nTRG);
#endif

    for(int i=0; i<(r_data.trg.nTRG); i++) {
        r_data.trg.TRG[i][0] = trg0[i].fifo_a;
        r_data.trg.TRG[i][1] = trg0[i].fifo_b;
        r_data.trg.TRG[i][2] = trg0[i].fifo_c;
        r_data.trg.TRG[i][3] = trg0[i].fifo_d;
        r_data.trg.TRG[i][4] = trg0[i].fifo_e;
#if defined(DEBUG) || defined(DEBUG_TRG)
        printf("DEBUG_TRG:\t[%s]\tTRG count : %d\n", __FUNCTION__, r_data.trg.TRG[i][0]);
        printf("DEBUG_TRG:\t[%s]\tTRG id    : %d\n", __FUNCTION__, r_data.trg.TRG[i][1]);
        unsigned long long clk48;
        clk48 = r_data.trg.TRG[i][2] * 65536 * 65536;
        clk48 += r_data.trg.TRG[i][3] * 65536;
        clk48 += r_data.trg.TRG[i][4];
        printf("DEBUG_TRG:\t[%s]\tTRG 48bit clock: %f [sec]\n", __FUNCTION__, clk48*2.*1.e-8);
#endif
    }
}

// __________________________________________________
void read_atm_data(mizu_raw_data &r_data, int runmode)
{
#if defined(DEBUG) || defined(DEBUG_ATM)
    printf("DEBUG_ATM:\t[%s]\trunmode : %d\n", __FUNCTION__, runmode);
#endif
    int flag;
    unsigned int read_length_tmp;

    if(runmode==PEDESTAL_MODE)     flag=1;
    else if(runmode==OPERATE_MODE) flag=0;
    else {
        printf("Error:\t[%s]\tInvalid run mode!!! (RUNMODE:%d)\n",
               __FUNCTION__, runmode);
        exit(-1);
    }

    if( TKO_read( r_data.atm.ATM, &read_length_tmp, flag )<0 ) {
        printf("Error @ read_atm_data\n");
        printf("[%s]\tReading SMP failed. (%d [bytes])\n", __FUNCTION__, read_length_tmp);
    }
    r_data.atm.nATM = (read_length_tmp/4);
    printf("[%s]\t\tReading SMP completed. (SMP: %d [B]\tNATM: %d [B])\n",
           __FUNCTION__, read_length_tmp, r_data.atm.nATM);

#if (defined(DEBUG) || defined(DEBUG_ATM)) && defined(DEBUG2)
    for (int i = 0; i < r_data.atm.nATM; i++) {
        UShort_t h = (r_data.atm.ATM[i] >> 16) & 0xFFFF;
        UShort_t l = (r_data.atm.ATM[i]) & 0xFFFF;
        if ( (i%1000==0 && runmode == 0) || runmode==1)
            printf("DEBUG_ATM2:\t[%s]\tATM[%5d]: %d (H:%d L:%d)\n",
                   __FUNCTION__, i, r_data.atm.ATM[i], h, l);
    }
#endif
}

// __________________________________________________
void start_pedestal(mizu_raw_data &r_data)
{
    printf("[%s]\tStart PEDESTAL mode\n", __FUNCTION__);
    r_data.mode = PEDESTAL_MODE;          // Set DAQ mode to PEDESTAL
    start_tko_pedestal( smp_num[0] );     // Start taking pedestals
    read_atm_data(r_data, PEDESTAL_MODE); // Read pedestal data
    read_trg_data(r_data);                // Read TRG data
    raw_data->Fill( r_data );             // Fill output tree
    printf("[%s]\tSwitch to OPERATE mode\n",  __FUNCTION__);
    set_tko_operate_mode( smp_num[0] );   // Change DAQ mode to OPERATE

}

// __________________________________________________
void start_operate(mizu_raw_data &r_data)
{
    printf("[%s]\t\tStart OPERATE mode\n", __FUNCTION__);
    r_data.mode = OPERATE_MODE;          // Set DAQ mode to PEDESTAL
    //set_tko_operate_mode( smp_num[0] );
    vioreg.out( 1<<(3-1) );              // Clear VETO; Out from ch3
    wait_fadc_trigger();                 // Wait untile FADC gate comes
    read_trg_data(r_data);               // Read TRG data
    read_fadc_data(r_data);              // Read FADC data
                                         // Read spill#
    unsigned short spill;
    if(vireg.IsTriggered())  {
        spill = vireg.GetLatchLow();
        vireg.Clear();
    }
    else {
        fprintf(stderr, "Not ready to read spill# !!!\n");
        spill = 0;
    }
    r_data.spill = spill;                // Set spill#

    printf("[%s]\t\tSpill#: %d\n", __FUNCTION__, spill);

    ss_sleep(10);                        // --> wait for SDS and switch memory by NIM signal
    read_atm_data(r_data, OPERATE_MODE); // Read ATM data
    raw_data->Fill( r_data );            // Fill output tree
}


// __________________________________________________
void convert_raw(mizu_raw_data r_data, T2KWCRaw &rw)
{
    // Convert TRG data
    rw.nTRG = r_data.trg.nTRG;
#if defined(DEBUG) || defined(DEBUG_RAW) || defined(DEBUG_TRG)
    printf("DEBUG_RAW:\t[%s]\tnTRG : %d\n", __FUNCTION__, rw.nTRG);
#endif    
    for(int i=0; i<rw.nTRG; i++) {
        for(int j=0; j<5; j++) {
            rw.TRG[i][j] = r_data.trg.TRG[i][j];
#if (defined(DEBUG) || defined(DEBUG_RAW)) && defined(DEBUG2)
            printf("DEBUG_RAW2:[%s]\tTRG[%d/%d][%d/5]:\t%d\n",
                   __FUNCTION__, i, r_data.trg.nTRG, j, r_data.trg.TRG[i][j]);
#endif            
        }
    }

    // Convert ATM data
    rw.nATM = r_data.atm.nATM;
#if defined(DEBUG) || defined(DEBUG_RAW) || defined(DEBUG_ATM)
    printf("DEBUG_RAW:\t[%s]\tnATM : %d\n", __FUNCTION__, rw.nATM);
#endif    
    for(int i=0; i<rw.nATM; i++) {
        rw.ATM[i] = r_data.atm.ATM[i];
#if (defined(DEBUG) || defined(DEBUG_RAW)) && defined(DEBUG2)
        if ( (i%1000==0 && r_data.mode == 0) || r_data.mode == 1 )
            printf("DEBUG_RAW2:\t[%s]\tATM[%5d/%5d]:\t%d\n",
                   __FUNCTION__, i, r_data.atm.nATM, r_data.atm.ATM[i]);
#endif
    }

    // Convert FADC data
    rw.nFADC = r_data.fadc.nFADC;
#if defined(DEBUG) || defined(DEBUG_RAW) || defined(DEBUG_FADC)
    printf("DEBUG_RAW:\t[%s]\tnFADC : %d\n", __FUNCTION__, rw.nFADC);
#endif        
    for(int i=0; i<rw.nFADC; i++) {
        rw.FADC[i] = r_data.fadc.FADC[i];
#if (defined(DEBUG) || defined(DEBUG_RAW)) && defined(DEBUG2)        
        if ( (i%500==0 && r_data.mode == 0) || r_data.mode == 1 )
            printf("DEBUG_RAW2:\t[%s]\tFADC[%4d/%4d]:\t%d\n",
                   __FUNCTION__, i, r_data.fadc.nFADC, r_data.fadc.FADC[i]);
#endif
        
    }
    rw.run   = r_data.run;
    rw.mode  = r_data.mode;
    rw.spill = r_data.spill;
    rw.time_sec = r_data.time_sec;
    rw.time_nano = r_data.time_nano;
#if defined(DEBUG) || defined(DEBUG_RAW)
    printf("DEBUG_RAW:\t[%s]\tRun   : %d\n", __FUNCTION__, rw.run);
    printf("DEBUG_RAW:\t[%s]\tMode  : %d\n", __FUNCTION__, rw.mode);
    printf("DEBUG_RAW:\t[%s]\tSpill : %d\n", __FUNCTION__, rw.spill);
    printf("DEBUG_RAW:\t[%s]\tTime  : %d [sec]\n", __FUNCTION__, rw.time_sec);
    printf("DEBUG_RAW:\t[%s]\tTime  : %d [usec]\n", __FUNCTION__, rw.time_nano);
#endif            
}

// __________________________________________________
void Usage()
{
    fprintf(stderr, "Usage : ./beam_poll <data dir> <run #> <#_of_cycle> (atm threshold> (<fadc offset>)\n");
    exit(1);
}

// __________________________________________________
extern "C" void terminate(int sig)
{
    fprintf(stderr, "[%s]\tCaught signal %d.\n", __FUNCTION__, sig);
    raw_data->Write();
    TRG_close();
    TKO_close();
    exit(-1);
}

// __________________________________________________
int main(int argc, char* argv[])
{
    // Check arguments
    if (argc < 4) Usage();
    
    // Initialization
    int run_number = atoi(argv[2]);
    int ncycles    = atoi(argv[3]);
    int port       = 11000;

    TString ofn;
    ofn.Form("%s/run%07d.root", argv[1], run_number);
    raw_data = new TRawData(ofn.Data(), true);

    mizu_raw_data r_data;
    r_data.run = run_number;
    r_data.spill = 0;
    r_data.atm.nATM = 0;
    r_data.trg.nTRG = 0;
    r_data.fadc.nFADC = 0;
    r_data.time_sec = 0;
    r_data.time_nano = 0;
    
    mizu_info info;
    T2KWCRaw raw[2];

    // Set Runinfo
    int atm_threshold = -500;
    if(argc>4) atm_threshold = atoi(argv[4]);
    unsigned int fadc_offset = 0x8000;
    if(argc>5) fadc_offset = (unsigned int)atoi(argv[5]);
    info.atm_thr = atm_threshold;
    info.fadc_off = fadc_offset;
    raw_data->SetInfo(info);

    printf("**************************************************\n");
    printf("\tRun Information:\n");
    printf("\t\tRUN#: %07d\tCycle#: %4d\n", run_number, ncycles);
    printf("\t\tPort# for Online Display : %d\n", port);
    printf("\t\tATM threshold: %d\n", atm_threshold);
    printf("\t\tFADC offset: %d\n", fadc_offset);
    printf("**************************************************\n");
    
    printf("**************************************************\n");
    printf("\tStarting process\n");
    printf("**************************************************\n");

    double start_time = gettimeofday_usec();
    gettimeofday_sec_usec(&r_data.time_sec, &r_data.time_nano);

    // -------------------- Start VETO
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tSet VETO\n", __FUNCTION__);
#endif    
    vioreg.level_out( 0x02 ); // VETO to beam triggeer by init complete

    // -------------------- Init TKO
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tInitialize TKO\n", __FUNCTION__);
#endif
    TKO_open(n_used_smp);
    TKO_init(smp_num[0], (double)atm_threshold);

    // -------------------- Init ATM
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tCheck ATM channels\n", __FUNCTION__);
#endif    
    pedestal_test( smp_num[0] );
    SMP_scount_Clear( smp_num[0] ); // -> clear SDS count in elec-test

    // -------------------- Init TRG
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tInitialize TRG\n", __FUNCTION__);
#endif        
    TRG_open();
    TRG_Command( TRG_TRGENA );

    // -------------------- Init FADC
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tInitialize FADC\n", __FUNCTION__);
#endif            
    FADC_init(fadc_offset);
    FADC_daq_mode(0); // -> Gate mode

    // -------------------- Clear Input register
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tClear Input Register\n", __FUNCTION__);
#endif                
    vireg.Clear();

    // -------------------- Clear VETO
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tFinished initialization\n", __FUNCTION__);
    printf("DEBUG_DAQ\t[%s]\tClear VETOn\n", __FUNCTION__);
#endif                    
    vioreg.out( 1<<(3-1) ); // from ch3
    vioreg.level_out( 0 );

    // -------------------- Start thread of socket process
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tConstruct TSocketServer\n", __FUNCTION__);
#endif
    TServerSocket server_socket(port, kTRUE);
    assert(server_socket.IsValid());
    TThread thread("acceptor", acceptor, &server_socket);
    thread.Run();

    // ==================== Start Event Loop ====================
#if defined(DEBUG) || defined(DEBUG_DAQ)
    printf("DEBUG_DAQ\t[%s]\tStart Event Loop\n", __FUNCTION__);
#endif    
    //TTree *tree = NULL;
    for(int icycle=0; icycle<ncycles; icycle++) {
        
        if (signal(SIGINT,  terminate) == SIG_ERR ||
            signal(SIGTERM, terminate) == SIG_ERR ||
            signal(SIGQUIT, terminate) == SIG_ERR ||
            signal(SIGHUP,  terminate) == SIG_ERR ||
            signal(SIGFPE,  terminate) == SIG_ERR ||
            signal(SIGABRT, terminate) == SIG_ERR ||
            signal(SIGSEGV, terminate) == SIG_ERR ) {
            fprintf(stderr, "Error:\tRegistering signal handler failed.\n");
            return (-1);
        }
        
        printf("\n==================================================\n");
        printf("\tRUN %d Spill %d (%dth cycle)\n", r_data.run, r_data.spill, icycle);
        printf("--------------------------------------------------\n");
        
        double start_cycle = gettimeofday_usec();
        // -------------------- Start PEDESTAL mode
        r_data = r_data_dummy;
        r_data.run = run_number;
        gettimeofday_sec_usec(&r_data.time_sec, &r_data.time_nano);
        start_pedestal(r_data);
        convert_raw(r_data, raw[0]);
        // raw[0].Print();

        // -------------------- Start OPERATE mode
        r_data = r_data_dummy;
        r_data.run = run_number;
        gettimeofday_sec_usec(&r_data.time_sec, &r_data.time_nano);
        start_operate(r_data);
        convert_raw(r_data, raw[1]);
        // raw[1].Print();

        printf("**************************************************\n");
        
        printf("\tRUN %d Spill %d (%dth cycle)\n", r_data.run, r_data.spill, icycle);
        printf("--------------------------------------------------\n");

        // Send T2KWCRaw data to online monitors
        send_it(&raw[0]);
        send_it(&raw[1]);

        printf("\tCycleInterval = %.1f [msec]\n",  (gettimeofday_usec()-start_cycle)/1000.);

        printf("**************************************************\n\n");
        
    }
    // ==================== End of Event Loop ====================

    // -------------------- Set VETO while writing to files
    vioreg.level_out( 0x02 ); // VETO to beam triggeer
    raw_data->Write();
    TKO_close();
    TRG_close();
    FADC_close();

    printf("**************************************************\n");
    printf("\tEnd of process ===> interval = %.1f [msec]\n",
           (gettimeofday_usec()-start_time)/1000.);
    printf("**************************************************\n");
    return 0;
};
