#include <iostream>
#include <unistd.h>

#include "../vmeioreg/vmeioreg/VmeIoreg.h"
#include "../vmeioreg/src/VmeIoreg.cc"


// VME-IOREG
static const unsigned int VIOREG_BASEADDR = 0xF000;
VmeIoreg vioreg( VIOREG_BASEADDR );

int
main( int argc, char* argv[] )
{
  int flag = 0;
  if(argc > 1) {
    flag = atoi(argv[1]);
  }

  // init
  /*
  vioreg.disableInt();
  vioreg.clear();
  vioreg.mask();
  vioreg.level_out( 0x00 );
  */

  if(flag==1) {
    std::cout << "start latch : ch1" << std::endl;
    //vioreg.level_out( 0x01 );
    vioreg.out( 0x01 );
  }
  else if (flag==2) {
    std::cout << "start latch : ch2" << std::endl;
    //vioreg.level_out( 0x02 );
    vioreg.out( 0x02 );
  }
  else if(flag==0) {
    std::cout << "stop latch" << std::endl;
    vioreg.level_out( 0 );
  }
  else {
    std::cout << "temporary channel" << std::endl;
    vioreg.level_out( 0x20 );
    //vioreg.level_out( 0 );
  }



  return( 0 );
}

