#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/page.h>

#include "vmedrv.h"

#define TRG_BASE  0x1000

#define TRG_RESET   0x0001
#define TRG_DUMP    0x0002
#define TRG_ENABLE  0x0004

char a16_logical_name[17]="/dev/vmedrv16d16";

struct TRG_REGS_STR{
  unsigned short csr;
  unsigned short trig_mask;
  unsigned short fifo_ctr;
  unsigned short event_no;
  unsigned short trig_id;
  unsigned short clk48h;
  unsigned short clk48m;
  unsigned short clk48l;
};


struct TRG_INFO{
  unsigned int event_no;
  unsigned int trig_id;
  unsigned long long clk48;
};

void
show_usage(char **argv)
{
  fprintf(stderr,
	  "Usage: %s -r | -e | -d\n",
	  argv[0]);
  fprintf(stderr,"               -r : reset \n");
  fprintf(stderr,"               -e : enable \n");
  fprintf(stderr,"               -d : dump \n");
  exit(0);
}


void
trg_dump( volatile struct TRG_REGS_STR *p)
{
  unsigned short i,j;
  struct TRG_INFO trginfo;

  printf("Status                  : %2x\n",p->csr&0xff);
  i = p->fifo_ctr & 0xfff;
  printf("# of events in the FIFO : %2x\n",i);

  for ( j = 0 ; j < i ; j++){
	trginfo.event_no = p->event_no;
	trginfo.trig_id  = p->trig_id;
	trginfo.clk48    = (p->clk48h * 65536 * 65536);
	trginfo.clk48   += (p->clk48m * 65536);
	trginfo.clk48   += (p->clk48l);

	printf("Event #%5d : Trig. ID %2x : 48bit CLK : %lf usec \n",
		   trginfo.event_no,trginfo.trig_id,trginfo.clk48*20.*1.e3);
  }
}

void
trg_reset( volatile struct TRG_REGS_STR *p)
{
  printf("Status before reset     : %2x\n",p->csr&0xff);
  p->csr |= 0xf0;
  p->trig_mask = 0x00;
  printf("Status after reset      : %2x\n",p->csr&0xff);

}

void
trg_enable( volatile struct TRG_REGS_STR *p)
{
  printf("Status before enable     : %2x\n",p->csr&0xff);
  p->csr |= 0x02;
  printf("Status after enable      : %2x\n",p->csr&0xff);
}


int
main( int argc , char **argv )
{

  int cmd,i;

  int btd;

  int           size             = 0x10;
  off_t         trg_base_offsets = TRG_BASE;
  off_t         map_base, offset;
  size_t        map_size;
  volatile void *trg_base_addr;

  char *cmdstr;

  if (argc < 2){
    show_usage(argv);
  }

  i = 1;

  cmdstr = *(argv+i);
  
  if (*cmdstr != '-'){
    show_usage(argv);
  }
  
  switch (*(cmdstr+1)){
    case 'r':
      cmd = TRG_RESET;
      printf("Command: TRG_RESET\n");
      i = argc;
      break;
	
    case 'd':
      cmd = TRG_DUMP;
      printf("Command: TRG_DUMP\n");
      i = argc;
      break;
	
    case 'e':
      cmd = TRG_ENABLE;
      printf("Command: TRG_ENABLE\n");
      break;
      
    default:
      show_usage(argv);
      break;
  }
  

  btd = open(a16_logical_name,O_RDWR);

  if (btd >0){

    map_base = trg_base_offsets & PAGE_MASK;
    offset   = trg_base_offsets  - map_base;
    map_size = size + offset;

    trg_base_addr = mmap(0, map_size, PROT_WRITE, MAP_SHARED, btd, map_base);
    if (trg_base_addr == MAP_FAILED) {
      perror("ERROR: mmap()");
      exit(EXIT_FAILURE);
    }
    trg_base_addr += offset;

    switch (cmd){
      case TRG_RESET:
        trg_reset(trg_base_addr);
        break;
      
      case TRG_DUMP:
        trg_dump(trg_base_addr);
        break;
      
      case TRG_ENABLE:
        trg_enable(trg_base_addr);
        break;

      default:
        fprintf(stderr,"Unknown command...\n");
        break;
    }
    
    close(btd);
    exit (0);
  }else{
    exit (EXIT_FAILURE);
  }

}

