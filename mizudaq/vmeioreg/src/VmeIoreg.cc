// -*- C++ -*-
//
// vmeioreg.cc
// example code
//
// Author:  Mitsuhiro YAMAGA (yamaga@post.kek.jp)
// Created: Fri Feb  1 18:30:01 JST 2002
//
// $Id: VmeIoreg.cc,v 1.1.1.1 2007/01/31 15:23:45 kensh Exp $
//
// Revision History
//
// $Log: VmeIoreg.cc,v $
// Revision 1.1.1.1  2007/01/31 15:23:45  kensh
// Initial version.
//
// Revision 1.2  2004/02/17 12:57:12  yamaga
// init() publically accessible.
//
// Revision 1.1.1.1  2002/08/13 12:48:12  kensh
//
//
// Start version.
//
//

#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "../vmeioreg/VmeIoreg.h"


//////
// static members
//int          VmeIoreg::m_fd32 = -1;
//ioctl_irq_t  VmeIoreg::m_intr = {0,0,0,0};


//////
// constants
static const unsigned int  VMEIOREG_MAPSIZE = 0x20;


//////
inline int
addrindex( int addr )
{
  return( addr >> 1 );
}


//////
#define DEBUG 0


//////
VmeIoreg::VmeIoreg()
  : m_debugLevel( 0 ),
    m_fd16( -1 ),
    m_baseaddr( 0 ),
    m_mapsize( 0 ),
    m_mapbase( (unsigned short*)-1 ),
    m_map(     (unsigned short*)-1 )
{
#if DEBUG
  std::cout << "VmeIoreg::VmeIoreg()" << std::endl;
#endif
}


//////
VmeIoreg::VmeIoreg( unsigned int baseaddr, int debugLevel )
  : m_debugLevel( debugLevel ),
    m_fd16( -1 ),
    m_baseaddr( baseaddr ),
    m_mapsize( 0 ),
    m_mapbase( (unsigned short*)-1 ),
    m_map(     (unsigned short*)-1 )
{
#if DEBUG
  std::cout << "VmeIoreg::VmeIoreg(addr)" << std::endl;
#endif

  // initialize
  if ( !init( m_baseaddr,debugLevel ) ){
    std::cout << "VmeIoreg::VmeIoreg(addr) : init failed" << std::endl;
    exit( 1 );
  }

};


//////
VmeIoreg::~VmeIoreg()
{
#if DEBUG
  std::cout << "VmeIoreg::~VmeIoreg()" << std::endl;
#endif
  
  if ( (caddr_t)m_mapbase != (caddr_t)-1 ){
    munmap( (char*)m_mapbase,m_mapsize );
  }
  
  if ( m_fd16 >= 0 ){
    close( m_fd16 );
  }
  
  //termVuiInterrupt();
}


//////
bool
VmeIoreg::init( unsigned int baseaddr,int debugLevel )
{
#if DEBUG
  std::cerr << "VmeIoreg::init()" << std::endl;
#endif
  
  m_debugLevel = debugLevel;
  
  // open VME device file 
  //m_fd16 = open( "/dev/vme16d16",O_RDWR );
  m_fd16 = open( "/dev/vmedrv16d16",O_RDWR );
  if ( m_fd16 == -1 ){
    perror( "VmeIoreg::init : open /dev/vmedrv16d16" );
    return( false );
  }

  // base addr
  m_baseaddr = baseaddr;
  unsigned int  mbase = m_baseaddr & 0xffffe000; // pagesize = 8192 = 0x2000
  
  // mmap
  m_mapsize = VMEIOREG_MAPSIZE + m_baseaddr - mbase;
  m_mapbase = (unsigned short*)mmap( 0, m_mapsize,
                                     PROT_WRITE|PROT_READ,MAP_SHARED,
                                     m_fd16, mbase );
  m_map = (unsigned short*)((unsigned int)m_mapbase + m_baseaddr - mbase);
  if ( m_debugLevel )
    std::cout << "VmeIoreg::init : mapbase=" << m_mapbase
              << " : map=" << m_map 
              << std::hex
              << " : baseaddr=" << m_baseaddr
              << " : mapsize=" << m_mapsize
              << " : diff=" << (int)m_map - (int)m_mapbase
              << std::dec
              << std::endl;
  if ( (caddr_t)m_mapbase  == (caddr_t)-1 ){
    perror( "VmeIoreg::init : mmap" );
    return( false );
  }

  return( true );
}


//////
bool
VmeIoreg::busy()
{
  return((m_map[addrindex(VMEIOREG_CSR1)] & (VMEIOREG_BUSY1|VMEIOREG_BUSY3))||
         (m_map[addrindex(VMEIOREG_CSR2)] & (VMEIOREG_BUSY2|VMEIOREG_BUSY3)));
}


//////
bool
VmeIoreg::busy( int i )
{
  switch( i ){
  case 1:
    return( (m_map[addrindex(VMEIOREG_CSR1)] & VMEIOREG_BUSY1 ) != 0 );
    break;
  case 2:
    return( (m_map[addrindex(VMEIOREG_CSR2)] & VMEIOREG_BUSY2 ) != 0 );
    break;
  case 3:
    return( (m_map[addrindex(VMEIOREG_CSR1)] & VMEIOREG_BUSY3 ) != 0 );
    break;
  }
  return( false );
}


//////
void
VmeIoreg::clear()
{
  m_map[addrindex(VMEIOREG_CSR1)] |= (VMEIOREG_CLR1 | VMEIOREG_CLR3);
  m_map[addrindex(VMEIOREG_CSR2)] |= (VMEIOREG_CLR2 | VMEIOREG_CLR3);
}


//////
void
VmeIoreg::clear( int i )
{
  switch( i ){
  case 1:
    m_map[addrindex(VMEIOREG_CSR1)] |= VMEIOREG_CLR1;
    break;
  case 2:
    m_map[addrindex(VMEIOREG_CSR2)] |= VMEIOREG_CLR2;
    break;
  case 3:
    m_map[addrindex(VMEIOREG_CSR1)] |= VMEIOREG_CLR3;
    m_map[addrindex(VMEIOREG_CSR2)] |= VMEIOREG_CLR3;
    break;
  }
}


//////
void
VmeIoreg::enableInt()
{
  m_map[addrindex(VMEIOREG_CSR1)] |= (VMEIOREG_ENABLE1 | VMEIOREG_ENABLE3);
  m_map[addrindex(VMEIOREG_CSR2)] |= (VMEIOREG_ENABLE2 | VMEIOREG_ENABLE3);
}

//////
void
VmeIoreg::enableInt( int i )
{
  switch( i ){
  case 1:
    m_map[addrindex(VMEIOREG_CSR1)] |= VMEIOREG_ENABLE1;
    break;
  case 2:
    m_map[addrindex(VMEIOREG_CSR2)] |= VMEIOREG_ENABLE2;
    break;
  case 3:
    m_map[addrindex(VMEIOREG_CSR1)] |= VMEIOREG_ENABLE3;
    m_map[addrindex(VMEIOREG_CSR2)] |= VMEIOREG_ENABLE3;
    break;
  }
}


//////
void
VmeIoreg::disableInt()
{
  m_map[addrindex(VMEIOREG_CSR1)] &= ~(VMEIOREG_ENABLE1 | VMEIOREG_ENABLE3);
  m_map[addrindex(VMEIOREG_CSR2)] &= ~(VMEIOREG_ENABLE2 | VMEIOREG_ENABLE3);
}


//////
void
VmeIoreg::disableInt( int i )
{
  switch( i ){
  case 1:
    m_map[addrindex(VMEIOREG_CSR1)] &= (~VMEIOREG_ENABLE1);
    break;
  case 2:
    m_map[addrindex(VMEIOREG_CSR2)] &= (~VMEIOREG_ENABLE2);
    break;
  case 3:
    m_map[addrindex(VMEIOREG_CSR1)] &= (~VMEIOREG_ENABLE3);
    m_map[addrindex(VMEIOREG_CSR2)] &= (~VMEIOREG_ENABLE3);
    break;
  }
}


//////
void
VmeIoreg::mask()
{
  m_map[addrindex(VMEIOREG_CSR1)] |= VMEIOREG_MASK1;
  m_map[addrindex(VMEIOREG_CSR2)] |= VMEIOREG_MASK2;
}


//////
void
VmeIoreg::mask( int i )
{
  switch( i ){
  case 1:
    m_map[addrindex(VMEIOREG_CSR1)] |= VMEIOREG_MASK1;
    break;
  case 2:
    m_map[addrindex(VMEIOREG_CSR2)] |= VMEIOREG_MASK2;
    break;
  }
}


//////
void
VmeIoreg::out( int data )
{
  m_map[addrindex(VMEIOREG_OUTREG)] = data;
}


//////
void
VmeIoreg::level_out( int data )
{
  m_map[addrindex(VMEIOREG_LEVELREG)] = data;
}


//////
int
VmeIoreg::latch( int ch )
{
  switch( ch ){
  case 1:
    return( m_map[addrindex(VMEIOREG_IN_LATCH1)] );
    break;
  case 2:
    return( m_map[addrindex(VMEIOREG_IN_LATCH2)] );
    break;
  }
  return( 0 );
}


//////
int
VmeIoreg::input_flipflop()
{
  return( m_map[addrindex(VMEIOREG_IN_FF)] );
}


//////
int
VmeIoreg::input_through()
{
  return( m_map[addrindex(VMEIOREG_IN_THROUGH)] );
}


//////
int
VmeIoreg::read( int reg )
{
  if ( 0x00 <= reg && reg <= 0x0e && !(0x08<=reg && reg<0x0c ) ){
    return( m_map[addrindex(reg)] );
  }
  std::cout << "VmeIoreg::read : Illegal register : " 
              << std::hex << reg << std::dec 
              << std::endl;
  return( 0xffffffff );
}


//////
void
VmeIoreg::write( int reg,int data )
{
  if ( 0x08 <= reg && reg <= 0x0e ){
    m_map[addrindex(reg)] = data;
  }
  std::cout << "VmeIoreg::write : Illegal register : " 
              << std::hex << reg << std::dec 
              << std::endl;
}


//////
void 
VmeIoreg::initInterrupt( int irq, int vect )
{
  //
  m_intr_prop.irq       = irq;
  m_intr_prop.vector    = vect;
  m_intr_prop.signal_id = 0;

  // regist interrupt 
  if ( ioctl(m_fd16, VMEDRV_IOC_REGISTER_INTERRUPT, &m_intr_prop) < 0 ) {
    perror("ERROR: ioctl(REGISTER_INTERRUT)");
    exit(0);
  }

  // enable interrupt
  if ( ioctl(m_fd16, VMEDRV_IOC_ENABLE_INTERRUPT) < 0 ) {
    perror("ERROR: ioctl(ENABLE_INTERRUT)");
    exit(0);
  }

}

//////
bool 
VmeIoreg::waitInterrupt( int timeout )
{
  //
  m_intr_prop.timeout = timeout;
  int ret = ioctl(m_fd16, VMEDRV_IOC_WAIT_FOR_INTERRUPT, &m_intr_prop);

  if ( ret < 0 ) {
    perror("ERROR: ioctrl(WAIT_FOR_INTERRUPT)");
    exit(0);
  }
  else if( ret > 0 ) {
    return( true );
  }
  return( false );
}

//////
void
VmeIoreg::termInterrupt()
{
  // disable interrupt
  if( ioctl(m_fd16, VMEDRV_IOC_DISABLE_INTERRUPT) < 0 ) {
    perror("ERROR: ioctl(DISABLE_INTERRUT)");
    exit(0);
  }

  // 
  if( ioctl(m_fd16, VMEDRV_IOC_UNREGISTER_INTERRUPT, &m_intr_prop) < 0 ) {
    perror("ERROR: ioctl(UNREGISTER_INTERRUT)");
    exit(0);
  }
}

////
void VmeIoreg::start_veto_latch()
{
  std::cout << "start_veto" << std::endl;
  level_out( 0x01 );
}

////
void VmeIoreg::stop_all_latch()
{
  std::cout << "stop_veto" << std::endl;
  level_out( 0 );
}
