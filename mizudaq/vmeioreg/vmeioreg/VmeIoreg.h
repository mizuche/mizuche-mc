// -*- C++ -*-
//
// VmeIoreg.h
// VME IOregister accessing class
//
// Author:  Mitsuhiro YAMAGA (yamaga@post.kek.jp)
// Created: Wed Nov 21 13:14:21 JST 2001
//
// $Id: VmeIoreg.h,v 1.1.1.1 2007/01/31 15:23:45 kensh Exp $
//
// Revision History
//
// $Log: VmeIoreg.h,v $
// Revision 1.1.1.1  2007/01/31 15:23:45  kensh
// Initial version.
//
// Revision 1.2  2004/02/17 12:57:12  yamaga
// init() publically accessible.
//
// Revision 1.1.1.1  2002/08/13 12:48:12  kensh
//
//
// Start version.
//
//

#ifndef  VMEIOREG_H_INCLUDED
#define  VMEIOREG_H_INCLUDED

// includes
//#include <sys/vme.h>
//#include "vmedrv/vmedrv.h"
//#include "./vmedrv.h"
//#include "/home/mizuche/mizu_daq/driver/vmedrv/vmedrv.h"
#include "../../driver/vmedrv/vmedrv.h"

//////
// constants
static const unsigned int  VMEIOREG_IN_LATCH1  = 0x00;
static const unsigned int  VMEIOREG_IN_LATCH2  = 0x02;
static const unsigned int  VMEIOREG_IN_FF      = 0x04;
static const unsigned int  VMEIOREG_IN_THROUGH = 0x06;
static const unsigned int  VMEIOREG_OUTREG     = 0x08;
static const unsigned int  VMEIOREG_LEVELREG   = 0x0a;
static const unsigned int  VMEIOREG_CSR1       = 0x0c;
static const unsigned int  VMEIOREG_CSR2       = 0x0e;

static const unsigned int  VMEIOREG_BUSY1    = 0x20;
static const unsigned int  VMEIOREG_BUSY2    = 0x20;
static const unsigned int  VMEIOREG_BUSY3    = 0x80;
static const unsigned int  VMEIOREG_CLR1     = 0x02;
static const unsigned int  VMEIOREG_CLR2     = 0x02;
static const unsigned int  VMEIOREG_CLR3     = 0x01;
static const unsigned int  VMEIOREG_ENABLE1  = 0x10;
static const unsigned int  VMEIOREG_ENABLE2  = 0x10;
static const unsigned int  VMEIOREG_ENABLE3  = 0x40;
static const unsigned int  VMEIOREG_MASK1    = 0x08;
static const unsigned int  VMEIOREG_MASK2    = 0x08;


//////
class VmeIoreg {
public:
  // constructor/destructor
  VmeIoreg();
  VmeIoreg( unsigned int baseaddr, int debugLevel=0 );
  ~VmeIoreg();
  // operator
  // static methods
  //static  bool  initVuiInterrupt( int prop, int level, int vect );
  //static  void  termVuiInterrupt();

  void initInterrupt( int irq, int vect );
  bool waitInterrupt( int timeout=3 );
  void termInterrupt();


  // methods
  bool    busy();
  bool    busy( int ch );
  void    clear();
  void    clear( int ch );
  void    enableInt();
  void    enableInt( int ch );
  void    disableInt();
  void    disableInt( int source );
  void    mask();
  void    mask( int source );
  void    out( int data );
  void    level_out( int data );
  int     latch( int ch );
  int     input_flipflop();
  int     input_through();
  int     read( int reg );
  void    write( int reg, int pattern );

  // by akira.m
  void start_veto_latch();
  void stop_all_latch();

  //
  bool    init( unsigned int baseaddr,int debugLevel=0 );
private:
  // static data
  //static int          m_fd32;
  //static ioctl_irq_t  m_intr;

  struct vmedrv_interrupt_property_t m_intr_prop;

  // data
  int              m_debugLevel;
  int              m_fd16;
  unsigned int     m_baseaddr;
  unsigned int     m_mapsize;
  unsigned short*  m_mapbase;
  unsigned short*  m_map;  
  // methods
};

//
#endif // VMEIOREG_H_INCLUDED
