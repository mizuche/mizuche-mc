#! /usr/bin/env python

import os, sys
from optparse import OptionParser


class SwitchDetMap(object):
    ## __________________________________________________
    def __init__(self, fin = 'notset', fout = 'detector_map.txt'):
        self.ifn = fin
        self.ofn = fout

    ## __________________________________________________
    def __str__(self):
        d = {}
        d['source'] = self.ifn
        d['target'] = self.ofn
        com = 'Command to be executed: ==> rm %(target)s; ln -s %(source)s %(target)s; ls -l %(target)s' % d
        return com

    ## __________________________________________________
    def switch(self):
        d = {}
        d['source'] = self.ifn
        d['target'] = self.ofn
        if not os.path.exists(self.ifn):
            print 'switch:\tFile "%(source)s" does not exist.' % d
            sys.exit(1)
        if os.path.lexists(self.ofn):
            com = 'rm %(target)s; ln -s %(source)s %(target)s; ls -l %(target)s' % d
        else:
            com = 'ln -s %(source)s %(target)s; ls -l %(target)s' % d            
        print 'switch:\t%s' % com
        os.system(com)

    ## __________________________________________________
    def show(self):
        d = {}
        d['target'] = self.ofn
        com = 'ls -l %(target)s' % d
        print 'show:\t%s' % com
        os.system(com)

## __________________________________________________
if __name__ == '__main__':

    usage = 'usage: %s [-fsh] filename' % sys.argv[0]
    parser = OptionParser(usage)
    parser.add_option('-f', '--ofn',
                      dest = 'filename',
                      help ='specify output filename (default="detector_map.txt")')
    parser.add_option('-s', '--show',
                      action = 'store_true',
                      dest = 'isShow',
                      help = 'show current version and exit')
    parser.set_defaults(filename = 'detector_map.txt',
                        isShow  = False)

    (options, args) = parser.parse_args()

    if options.isShow:
        sdm = SwitchDetMap()
        sdm.show()
        sys.exit(0)

    if len(args) == 1:
        ifn = args[0]
        ofn = options.filename
        sdm = SwitchDetMap(ifn, ofn)
        sdm.switch()
    else:
        print usage
        
    
        
