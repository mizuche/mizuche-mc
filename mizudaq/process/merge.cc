//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 16:40:53+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
//
#ifndef __CINT__
#ifndef ROOT_TApplication
#include "TApplication.h"
#endif

// PUT HEADERS HERE
#include "TCanvas.h"
#include "TStopwatch.h"
#include "Merger.h"
#include "TString.h"

#endif

// __________________________________________________
Int_t Usage()
{
    TString usage, desc, option;
    usage = "Usage: ./merge [options] UpkRun#\n";
    usage += "\n";
    usage += " Ex): ./merge 490001\n";
    usage += "      ./merge -v 490001 > hoge.txt\n";
    usage += "      ./merge -vv 490001 > hoge.txt\n";
    usage += "      ./merge -vg 490001 > hoge.txt\n";
    usage += "      ./merge -vge 490001 > hoge.txt\n";
    usage += "\n";

    desc = "Desctiption\n";
    desc += "    This program merges UPK and BSD data.\n";
    desc += "  UPK-BSD run# table is written in the sorce.\n";
    desc += "  You can change verbose/debug/error message level.\n";
    desc += "\n";

    option = "Options\n";
    option += "  -h:  show help\n";
    option += "  -u:  set UpkData directory\n";
    option += "  -d:  set DstData directory\n";
    option += "  -q:  set quiet\n";
    option += "  -w:  force overwrite\n";
    option += "  -v:  set verbose level\n";
    option += "  -g:  set debug level\n";
    option += "  -e:  set error level\n";
    option += "\n";
    
    fprintf(stderr, "%s", usage.Data());
    fprintf(stderr, "%s", desc.Data());
    fprintf(stderr, "%s", option.Data());
    
    return 0;
}

// __________________________________________________
Int_t GetRuns(const Int_t &run, Int_t *srun, Int_t *erun)
{
    
    if      (run == 420001) { *srun = 420021; *erun = 420028; }
    else if (run == 420002) { *srun = 420028; *erun = 420057; }
    else if (run == 420003) { *srun = 420057; *erun = 420057; }
    else if (run == 420004) { *srun = 420064; *erun = 420068; }
    else if (run == 420005) { *srun = 420069; *erun = 420116; }
    else if (run == 420006) { *srun = 420125; *erun = 420163; }
    else if (run == 420007) { *srun = 420163; *erun = 420171; }
    else if (run == 420008) { *srun = 420171; *erun = 420189; }
    else if (run == 420009) { *srun = 420189; *erun = 420193; }
    else if (run == 420010) { *srun = 420193; *erun = 420210; }
    else if (run == 420011) { *srun = 420210; *erun = 420218; }
    else if (run == 420012) { *srun = 420218; *erun = 420240; }
    else if (run == 420013) { *srun = 420240; *erun = 420258; }
    else if (run == 420014) { *srun = 420258; *erun = 420271; }
    else if (run == 420015) { *srun = 420271; *erun = 420307; }
    else if (run == 420016) { *srun = 420307; *erun = 420333; }

    else if (run == 430001) { *srun = 430005; *erun = 430029; }
    else if (run == 430002) { *srun = 430029; *erun = 430052; }
    else if (run == 430003) { *srun = 430052; *erun = 430053; }
    else if (run == 430004) { *srun = 430062; *erun = 430069; }

    else if (run == 450001) { *srun = 450097; *erun = 450097; }
    else if (run == 450002) { *srun = 450097; *erun = 450102; }
    else if (run == 450003) { *srun = 450097; *erun = 450102; }

    else if (run == 460001) { *srun = 460024; *erun = 460051; }
    else if (run == 460002) { *srun = 460051; *erun = 460058; }
    else if (run == 460003) { *srun = 460058; *erun = 460062; }
    else if (run == 460004) { *srun = 460062; *erun = 460084; }
    else if (run == 460005) { *srun = 460084; *erun = 460092; }
    else if (run == 460006) { *srun = 460092; *erun = 460101; }

    else if (run == 460007) { *srun = 460106; *erun = 460112; }
    else if (run == 460008) { *srun = 460112; *erun = 460119; }
    else if (run == 460009) { *srun = 460119; *erun = 460142; }

    else if (run == 470001) { *srun = 470003; *erun = 470019; }
    else if (run == 470002) { *srun = 470019; *erun = 470025; }
    else if (run == 470003) { *srun = 470025; *erun = 470145; }
    else if (run == 470004) { *srun = 470145; *erun = 470152; }
    else if (run == 470005) { *srun = 470152; *erun = 470170; }
    else if (run == 470006) { *srun = 470170; *erun = 470176; }
    else if (run == 470007) { *srun = 470176; *erun = 480020; }
    else if (run == 470008) { *srun = 480002; *erun = 480007; }
    else if (run == 470009) { *srun = 480007; *erun = 480020; }

    else if (run == 490001) { *srun = 490001; *erun = 490038; }
    else if (run == 490002) { *srun = 490038; *erun = 490038; }
    
    else {
        fprintf(stderr, "Error:\tRun# %d is not on the list.\n", run);
        exit(-1);
    }
    return 0;
};


// __________________________________________________
int merge(int &argc, char **argv)
{
    TStopwatch sw;
    sw.Start();

    // DEFINE YOUR MAIN FUNCTION HERE
    TString upkdir = "exp/beam/upk/";
    TString dstdir = "exp/beam/dst/";
    Bool_t quiet = kFALSE;
    Bool_t overwrite = kFALSE;
    Int_t vlevel = 0;
    Int_t dlevel = 0;
    Int_t elevel = 0;
    Int_t opt;
    while ((opt = getopt(argc, argv, "hu:d:qwvge")) != -1) {
        switch(opt) {
        case 'h':
            fprintf(stderr, "show this help\n");
            Usage();
            exit(-1);
        case 'u':
            fprintf(stderr, "[getopt]\tset UpkData directory to : '%s'\n", optarg);
            upkdir = optarg;
            break;
        case 'd':
            fprintf(stderr, "[getopt]\tset DstData directory to : '%s'\n", optarg);
            dstdir = optarg;
            break;
        case 'q':
            quiet = kTRUE;
            fprintf(stderr, "[getopt]\tOption '-q' was selected.\n");
            break;
        case 'w':
            overwrite = kTRUE;
            fprintf(stderr, "[getopt]\tOption '-w' was selected.\n");
            break;
        case 'v':
            vlevel++;
            fprintf(stderr, "[getopt]\tOption '-v' was selected.\n");
            break;
        case 'g':
            dlevel++;
            fprintf(stderr, "[getopt]\tOption '-g' was selected.\n");
            break;
        case 'e':
            fprintf(stderr, "[getopt]\tOption '-e' was selected.\n");
            elevel++;
            break;
        }
    }

    argc -= optind;
    argv += optind;
    
    if (argc != 1) {
        fprintf(stderr, "Wrong number of arguments. (%d)\n", argc);
        Usage();
        exit(-1);
    }
    
    const Int_t run = atoi(argv[0]);
    Int_t srun = -1, erun = -1;
    GetRuns(run, &srun, &erun);

    TString ifn, ofn, t2krun;
    ifn.Form("%supk%07d.root", upkdir.Data(), run);
    ofn.Form("%sdst%07d.root", dstdir.Data(), run);
    t2krun = "t2krun3";
    if (run / 10000 > 43) t2krun = "t2krun4";

    TString info, tmpinfo;
    info.Form("===== Merge Information =====\n");
    tmpinfo.Form("  Input filename   : %s\n", ifn.Data());
    info += tmpinfo;
    tmpinfo.Form("  Output filename  : %s\n", ofn.Data());
    info += tmpinfo;
    tmpinfo.Form("  Run #         : %d\n", run);
    info += tmpinfo;
    tmpinfo.Form("  BSD version   : %s\n", t2krun.Data());
    info += tmpinfo;
    tmpinfo.Form("  Bsd Start #         : %d\n", srun);
    info += tmpinfo;
    tmpinfo.Form("  Bsd End   #         : %d\n", erun);
    info += tmpinfo;
    fprintf(stderr, "%s", info.Data());

    // Confirm parameter if it is not quiet-mode
    if (!quiet) {
        fprintf(stderr, "Warning:\tIs parameter above correct? (y/[n]) > ");
        Int_t answer, readch;
        readch = getchar();
        answer = readch;
        while (readch != '\n' && readch != EOF) readch = getchar();
        if (answer == 'y' || answer == 'Y') {
            fprintf(stderr, "\tOK\n");
        }
        else if (answer == 'q' || answer == 'Q') {
            fprintf(stderr, "\tAbort. Bye Bye\n");
            gSystem->Abort();
        } else {
            fprintf(stderr, "\tQuit. Bye Bye\n");
            exit(-1);
        }
    } else {
        vlevel = 0;
        dlevel = 0;
        elevel = 0;
    }
    

    Merger *m = new Merger();
    m->SetVerboseLevel(vlevel);
    m->SetDebugLevel(dlevel);
    m->SetErrorLevel(elevel);
    
    m->ReadUpk(ifn.Data());
    m->ReadBsds(srun, erun, t2krun.Data(), "v01_2");
    m->OpenDst(ofn.Data(), overwrite);
    //m->PreMerge();
    m->Merge();
    m->CloseDst();
    fprintf(stderr, "\nFinished\n");
    sw.Print();
    return 0;
}

// __________________________________________________
#ifndef __CINT__
int main(int argc, char** argv)
{
    //fprintf(stderr, "[%s]\n", __FUNCTION__);
    // TApplication mergeApp("mergeApp", &argc, argv);
    int retVal = merge(argc, argv);
    // mergeApp.Run();
    return retVal;
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
