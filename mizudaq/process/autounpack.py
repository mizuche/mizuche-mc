#! /usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys
from optparse import OptionParser

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='autounpack.log',
                    #filename='debugautounpack.log',
                    filemode='a')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter(' %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

## __________________________________________________
class Config(object):
    DetDir = 'detmap'
    UpkDir = 'upk'
    WaveDir = 'wave'
    FadcNped = 100
    FadcHitThrd = 10
    
## __________________________________________________
class DataBase(object):
    ## __________________________________________________
    def __init__(self):
        self.logger = logging.getLogger('DataBase')
        self.logger.debug('-' * 50)

    ## __________________________________________________
    def read(self, ifn):
        with open(ifn, 'r') as fin:
            lines = fin.readlines()

        header = lines[0]
        data = lines[1:]
        for d in data:
            self.logger.debug(d)
            
## ________________________________________________________________________________
class AutoUnpack:
    ## __________________________________________________
    def __init__(self, options=None):
        self.nped = options.nped
        self.thrd = options.thrd
        self.dryrun = options.dryrun
        self.opt = None
        opt = ''
        if options.umode:
            opt += 'f'
        if options.quiet:
            opt += 'q'
        if options.overwrite:
            opt += 'w'
        if len(opt) > 0:
            self.opt = '-' + opt

        self.logger = logging.getLogger('AutoUnpack')
        self.logger.info('-' * 50)
        self.logger.debug('Options: {0}'.format(options))
        self.data = self.init_data()


    ## __________________________________________________
    def __str__(self):
        data = self.data
        lines = []
        for d in data:
            line = '[show]:\t{0[orun]} | {0[idir]} {0[srun]} - {0[erun]} | FV:{0[fv]} DAQ:{0[dmode]} | {0[map]}'.format(d)
            lines.append(line)
        s = ('\n').join(lines)
        s+= '\n\t[B]:beam, [N]:noise, [C]:cosmic, [L]:LED, [Q]:QT'
        return s
    
    ## __________________________________________________
    def initBeamData(self):
        '''
        Database for beam data
        '''
        basedir = 'exp/beam/'
        data = []
        ##
        idir = os.path.join(basedir, 'MR41')
        detmap = 'detmapG15e5MR42v1'
        data.append(self.set_data(idir, 410018, 410075, 1, detmap, 1, 1, 100, 10)) ## DAQtrigger:1Hit, DAQsleep:200ms
        data.append(self.set_data(idir, 410077, 410127, 2, detmap, 1, 1, 100, 40)) ## DAQtrigger:3Hit, DAQsleep:200ms
        data.append(self.set_data(idir, 410129, 410129, 3, detmap, 1, 1, 100, 40)) ## DAQtrigger:3Hit, DAQsleep:500ms
        data.append(self.set_data(idir, 410131, 410241, 4, detmap, 1, 1, 100, 40)) ## DAQtrigger:3Hit, DAQsleep:500ms, +ATM15
        data.append(self.set_data(idir, 410242, 410269, 5, detmap, 1, 1, 100, 40)) ## DAQtrigger:3Hit, DAQsleep:500ms, +ATM15, +P2work
        data.append(self.set_data(idir, 410271, 410313, 6, detmap, 1, 1, 100, 40)) ## DAQtrigger:3Hit, DAQsleep:300ms, +ATM15, +P2work
        
        ##
        idir = os.path.join(basedir, 'MR42')
        data.append(self.set_data(idir, 420001, 420074, 1, 'detmapG15e5MR42v1', 1, 1, 100, 40))
        data.append(self.set_data(idir, 420075, 420094, 2, 'detmapG15e5MR42v1', 1, 1, 100, 40))
        data.append(self.set_data(idir, 420095, 420120, 3, 'detmapG15e5MR42v1', 1, 0, 100, 40)) ## increased noise, BAD run
        data.append(self.set_data(idir, 420121, 420123, 4, 'detmapG15e5MR42v2', 1, 0, 100, 40))
        data.append(self.set_data(idir, 420124, 420238, 5, 'detmapG15e5MR42v3', 1, 1, 100, 40))
        data.append(self.set_data(idir, 420239, 420277, 6, 'detmapG15e5MR42v4', 1, 1, 100, 40))
        detmap = 'detmapG15e5MR42v6'
        data.append(self.set_data(idir, 420278, 420377, 7, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420378, 420477, 8, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420478, 420577, 9, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420578, 420677, 10, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420678, 420777, 11, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420778, 420877, 12, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420878, 420977, 13, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 420978, 421062, 14, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 421063, 421194, 15, 'detmapG15e5MR42v7', 1, 2, 100, 40))
        data.append(self.set_data(idir, 421195, 421305, 16, 'detmapG15e5MR42v8', 1, 1, 100, 40))
        
        ##
        idir = os.path.join(basedir, 'MR43')
        detmap = 'detmapG15e5MR43v1'
        data.append(self.set_data(idir, 430001, 430100, 1, detmap, 0, 1, 100, 40))  ## FV empty
        data.append(self.set_data(idir, 430101, 430162, 2, detmap, 0, 1, 100, 40))  ## FV empty
        data.append(self.set_data(idir, 430163, 430200, 3, detmap, 1, 1, 100, 40))  ## FV full
        data.append(self.set_data(idir, 430201, 430339, 4, detmap, 1, 1, 100, 40))  ## FV full
        
        ##
        idir = os.path.join(basedir, 'MR45')
        detmap = 'MR45G15e5v1'
        data.append(self.set_data(idir, 450001, 450004, 1, detmap, 1, 1, 100, 40))
        detmap = 'MR45G15e5v2'    ## Add PMT masks
        data.append(self.set_data(idir, 450005, 450027, 2, detmap, 1, 1, 100, 40))
        data.append(self.set_data(idir, 450028, 450034, 3, detmap, 1, 1, 100, 40))
        
        ##
        idir = os.path.join(basedir, 'MR46')
        detmap = 'MR46G15e5v1'
        data.append(self.set_data(idir, 460001, 460100, 1, detmap, 1, 1, 100, 50))
        data.append(self.set_data(idir, 460101, 460200, 2, detmap, 1, 1, 100, 50))
        data.append(self.set_data(idir, 460201, 460300, 3, detmap, 1, 1, 100, 50))
        data.append(self.set_data(idir, 460301, 460400, 4, detmap, 1, 1, 100, 50))
        data.append(self.set_data(idir, 460401, 460500, 5, detmap, 1, 1, 100, 50))
        data.append(self.set_data(idir, 460501, 460554, 6, detmap, 1, 1, 100, 50))
        detmap = 'MR46G15e5v2'   ## PMT184 masked
        data.append(self.set_data(idir, 460555, 460700, 7, detmap, 2, 1, 100, 50))   ## FV half full
        data.append(self.set_data(idir, 460701, 460800, 8, detmap, 2, 1, 100, 50))   ## FV half full
        data.append(self.set_data(idir, 460801, 460917, 9, detmap, 2, 1, 100, 50))   ## FV half full
        ##
        idir = os.path.join(basedir, 'MR47')
        detmap = 'MR47G15e5v1'
        nped = 100
        nthr = 50
        data.append(self.set_data(idir, 470001, 470100, 1, detmap, 2, 1, nped, nthr))   ## FV half full
        data.append(self.set_data(idir, 470101, 470173, 2, detmap, 2, 1, nped, nthr))   ## FV half full
        detmap = 'MR47G15e5v2'
        data.append(self.set_data(idir, 470174, 470300, 3, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 470301, 470400, 4, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 470401, 470500, 5, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 470501, 470600, 6, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 470601, 470700, 7, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 470701, 470800, 8, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 470801, 470911, 9, detmap, 0, 1, nped, nthr))   ## FV empty
        ##
        idir = os.path.join(basedir, 'MR49')
        detmap = 'MR49G15e5v1'
        data.append(self.set_data(idir, 490001, 490100, 1, detmap, 0, 1, nped, nthr))   ## FV empty
        data.append(self.set_data(idir, 490101, 490156, 2, detmap, 0, 1, nped, nthr))   ## FV empty
        return data
    
    ## __________________________________________________
    def initNoiseData(self):
        '''
        Database for noise measurement
        '''
        data = []
        basedir = 'exp/noise/'
        ##_____ 2011/08/26 _____
        ##_____ 2011/09/02 _____
        ##_____ 2011/09/07 _____
        ##_____ 2011/12/13 _____
        ##_____ 2011/12/14 _____
        ##_____ 2011/12/16 _____
        ##_____ 2011/12/21 _____
        ##_____ 2011/12/22 _____
        ##_____ 2012/01/13 _____
        ##_____ 2012/03/02 _____
        ##_____ 2012/03/03 _____
        ##_____ 2012/03/04 _____
        ##_____ 2012/03/06 _____
        ##_____ 2012/03/07 _____
        ##_____ 2012/03/08 _____
        ##_____ 2012/03/12 _____
        ##_____ 2012/04/18 _____
        ##_____ 2012/04/20 _____
        ##_____ 2012/05/25, 27 _____
        idir = os.path.join(basedir, '120525')
        nped = 100
        thrd = 10
        detmap = 'detmapG15e5MR42v8'
        data.append(self.set_data(idir, 1, 5, 1, detmap, 1, 10, nped, thrd))
        idir = os.path.join(basedir, '120527')
        data.append(self.set_data(idir, 1, 5, 1, detmap, 1, 10, nped, thrd))
        ##_____ 2012/05/28, 30, 06/18 _____
        detmap = 'detmapG15e5MR43v1'
        idir = os.path.join(basedir, '120528')
        data.append(self.set_data(idir, 1, 9, 1, detmap, 1, 10, nped, thrd))
        idir = os.path.join(basedir, '120530')
        data.append(self.set_data(idir, 1, 30, 1, detmap, 1, 10, nped, thrd))
        idir = os.path.join(basedir, '120618')
        data.append(self.set_data(idir, 1, 279, 1, detmap, 1, 10, nped, thrd))
        ##_____ 2012/06/20 _____
        detmap = 'detmapN20120620HV1000v1'
        idir = os.path.join(basedir, '120620')
        data.append(self.set_data(idir, 1, 19, 1, detmap, 1, 10, nped, thrd))
        ##_____ 2012/06/20, 29 _____
        detmap = 'detmapN20120620G15e5v2'
        idir = os.path.join(basedir, '120620')
        data.append(self.set_data(idir, 20, 356, 2, detmap, 1, 10, nped, thrd))
        idir = os.path.join(basedir, '120629')
        data.append(self.set_data(idir, 1, 238, 1, detmap, 1, 10, nped, thrd))
        ##_____ 2012/08/08 _____
        ##_____ 2012/12/06 _____
        idir = os.path.join(basedir, '121206')
        nped = 10
        thrd = 40
        data.append(self.set_data(idir, 1, 12, 1, 'C20121205G15e5', 1, 10, nped, thrd))    ## same detmap as L20121204G15e5ALL
        data.append(self.set_data(idir, 13, 15, 2, 'C20121205G15e5', 1, 10, nped, thrd))
        ##_____ 2012/12/10 _____
        idir = os.path.join(basedir, '121210')
        data.append(self.set_data(idir, 1, 5, 1, 'C20121205G15e5', 1, 10, nped, thrd))    ## same detmap as L20121204G15e5ALL
        ##_____ 2012/12/12 _____
        idir = os.path.join(basedir, '121212')
        data.append(self.set_data(idir, 2, 2, 1, 'N20121212G15e5v1', 1, 10, nped, thrd))
        data.append(self.set_data(idir, 2, 2, 1, 'N20121212G15e5v2', 1, 10, nped, thrd))
        ##_____ 2012/12/19 _____
        idir = os.path.join(basedir, '121219')
        nped = 10
        thrd = 40  ##  > 3Hit
        detmap = 'N20121219G15e5v1'
        data.append(self.set_data(idir, 1, 10, 1, detmap, 1, 10, nped, thrd))    ## HV off
        thrd = 10  ##  >= 1Hit
        data.append(self.set_data(idir, 11, 20, 2, detmap, 1, 10, nped, thrd))    ## HV off
        data.append(self.set_data(idir, 21, 30, 3, detmap, 1, 10, nped, thrd))    ## HV on (HV=0)
        data.append(self.set_data(idir, 31, 100, 4, detmap, 1, 10, nped, thrd))    ## HV on (G=1.5e6)
        ##_____ 2012/12/20 _____
        idir = os.path.join(basedir, '121220')
        nped = 10
        thrd = 10  ## >= 1Hit
        detmap = 'N20121220G15e5v1'    ## Changed masks (unmasked some PMTs)
        data.append(self.set_data(idir, 1, 100, 1, detmap, 1, 10, nped, thrd))    ## HV on (G=1.5e6)
        ##_____ 2013/01/16 _____
        idir = os.path.join(basedir, '130116')
        nped = 10
        thrd = 10  ## >= 1Hit
        detmap = 'N20130116G15e5v1'
        data.append(self.set_data(idir, 1, 100, 1, detmap, 1, 10, nped, thrd))
        detmap = 'N20130116G15e5v2'
        data.append(self.set_data(idir, 101, 111, 2, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 112, 121, 3, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 122, 131, 4, detmap, 1, 10, nped, thrd))
        thrd = 50  ## >= 3Hit
        data.append(self.set_data(idir, 132, 149, 5, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 150, 157, 6, detmap, 1, 10, nped, thrd))
        ##_____ 2013/01/23 _____
        idir = os.path.join(basedir, '130123')
        nped = 100
        thrd = 40.2
        detmap = 'N20130123G15e5v1'
        data.append(self.set_data(idir, 1, 3, 1, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 4, 12, 2, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 13, 16, 3, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 17, 18, 4, detmap, 1, 10, nped, thrd))
        data.append(self.set_data(idir, 19, 21, 5, detmap, 1, 10, nped, thrd))
        ##_____ 2013/01/30 _____
        idir = os.path.join(basedir, '130130')
        nped = 100
        thrd = 50
        detmap = 'N20130130G15e5v1'
        data.append(self.set_data(idir, 1, 20, 1, detmap, 1, 10, nped, thrd))
        thrd = 35
        data.append(self.set_data(idir, 21, 40, 2, detmap, 1, 10, nped, thrd))
        ##_____ 2013/02/06 _____
        idir = os.path.join(basedir, '130206')
        nped = 100
        thrd = 50
        detmap = 'N20130206G15e5v1'  ### temp
        data.append(self.set_data(idir, 1, 11, 1, detmap, 1, 10, nped, thrd))  ## FV full
        data.append(self.set_data(idir, 12, 21, 2, detmap, 2, 10, nped, thrd))  ## FV half full
        data.append(self.set_data(idir, 22, 31, 3, detmap, 2, 10, nped, thrd))  ## FV half full
        detmap = 'N20130206G15e5v2'  ### temp
        data.append(self.set_data(idir, 32, 37, 4, detmap, 2, 10, nped, thrd))  ## FV half full
        ##_____ 2013/02/13 _____
        ##_____ 2013/02/17 _____
        idir = os.path.join(basedir, '130217')
        nped = 100
        thrd = 50
        detmap = 'N20130206G15e5v2'  ### temp --> NO HV applied
        data.append(self.set_data(idir, 1, 30, 1, detmap, 2, 10, nped, thrd))  ## FV half full
        ##_____ 2013/02/22
        idir = os.path.join(basedir, '130222')
        nped = 100
        thrd = 50
        detmap = 'N20130206G15e5v2'  ### temp --> NO HV applied
        data.append(self.set_data(idir, 1, 100, 1, detmap, 2, 10, nped, thrd))  ## FV half full
        ##_____ 2013/02/25 _____
        ##_____ 2013/02/26 _____
        ##_____ 2013/03/06 _____
        idir = os.path.join(basedir, '130306')
        nped = 100
        thrd = 50
        detmap = 'MR47G15e5v2'
        data.append(self.set_data(idir, 1, 7, 1, detmap, 0, 10, nped, thrd))  ## FV empty
        ##_____ 2013/03/07 _____
        idir = os.path.join(basedir, '130307')
        nped = 100
        thrd = 10
        detmap = 'MR47G15e5v2'
        data.append(self.set_data(idir, 1, 100, 1, detmap, 0, 10, nped, thrd))  ## FV empty
        data.append(self.set_data(idir, 101, 200, 2, detmap, 0, 10, nped, thrd))  ## FV empty
        data.append(self.set_data(idir, 201, 300, 3, detmap, 0, 10, nped, thrd))  ## FV empty
        ##_____ 2013/03/12 _____  no HV applied
        idir = os.path.join(basedir, '130312')
        nped = 100
        thrd = 10
        detmap = 'N20130312G00e5v1'   ## no HV applied
        data.append(self.set_data(idir, 1, 50, 1, detmap, 0, 10, nped, thrd))  ## FV empty
        data.append(self.set_data(idir, 51, 100, 2, detmap, 0, 10, nped, thrd))  ## FV empty
        data.append(self.set_data(idir, 101, 150, 3, detmap, 0, 10, nped, thrd))  ## FV empty
        ##_____ 2013/03/16 _____
        idir = os.path.join(basedir, '130316')
        nped = 100
        thrd = 10
        detmap = 'N20130316G15e5v1'   ## HV turned ON
        data.append(self.set_data(idir, 1, 50, 1, detmap, 0, 10, nped, thrd))  ## FV empty
        data.append(self.set_data(idir, 51, 100, 2, detmap, 0, 10, nped, thrd))  ## FV empty
        data.append(self.set_data(idir, 101, 150, 3, detmap, 0, 10, nped, thrd))  ## FV empty
        ##_____ 2013/05/02 _____
        idir = os.path.join(basedir, '130502')
        nped = 100
        thrd = 10  ## >= 1Hit
        detmap = 'N20130502G15e5v1'
        data.append(self.set_data(idir, 1, 4, 1, detmap, 0, 10, nped, thrd))  ## FV empty, 1Hz
        thrd = 50. ## > 3Hit
        data.append(self.set_data(idir, 5, 11, 2, detmap, 0, 10, nped, thrd))  ## FV empty, 2chFADC, 1Hz
        data.append(self.set_data(idir, 12, 13, 3, detmap, 0, 10, nped, thrd))  ## FV empty, 4chFADC, 1Hz
        data.append(self.set_data(idir, 14, 37, 4, detmap, 0, 10, nped, thrd))  ## FV empty, 4chFadc, BeamTrigger
    
        ##_____ 2013/05/08 _____
        idir = os.path.join(basedir, '130508')
        nped = 100
        thrd = 50  ## > 3Hit
        detmap = 'N20130508G15e5v1'
        data.append(self.set_data(idir, 1, 100, 1, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz
        data.append(self.set_data(idir, 101, 200, 2, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz
        data.append(self.set_data(idir, 201, 300, 3, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz
        data.append(self.set_data(idir, 301, 400, 4, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz
        data.append(self.set_data(idir, 401, 500, 5, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz, int30min
        data.append(self.set_data(idir, 501, 565, 6, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz, int30min
        ##_____ 2013/05/30 _____
        ### ATM threshold lowered to -200mV
        idir = os.path.join(basedir, '130530')
        nped = 100
        thrd = 50  ## > 3Hit
        detmap = 'N20130508G15e5v1'
        data.append(self.set_data(idir, 1, 33, 1, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz, int30min
        ##_____ 2013/05/31 _____
        ### ATM threshold lowered to -100mV
        idir = os.path.join(basedir, '130531')
        nped = 100
        thrd = 50  ## > 3Hit
        detmap = 'N20130508G15e5v1'
        data.append(self.set_data(idir, 1, 5, 1, detmap, 1, 10, nped, thrd))  ## FV full, 1Hz, int30min
        ##_____ 2013/07/17 _____
        idir = os.path.join(basedir, '130717')
        nped = 100
        thrd = 10  ## > 1Hit
        detmap = 'N20130717HV700'
        data.append(self.set_data(idir, 1, 1, 1, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130717HV800'
        data.append(self.set_data(idir, 2, 2, 2, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130717HV900'
        data.append(self.set_data(idir, 3, 3, 3, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130717HV1000'
        data.append(self.set_data(idir, 4, 4, 4, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130717G15e5'
        data.append(self.set_data(idir, 5, 5, 5, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        data.append(self.set_data(idir, 6, 9, 6, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 1000 event
        ##_____ 2013/07/18 _____
        idir = os.path.join(basedir, '130718')
        nped = 100
        thrd = 10  ## > 1Hit
        detmap = 'N20130718HV700'
        data.append(self.set_data(idir, 1, 1, 1, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718HV800'
        data.append(self.set_data(idir, 2, 2, 2, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718HV900'
        data.append(self.set_data(idir, 3, 3, 3, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718HV1000'
        data.append(self.set_data(idir, 4, 4, 4, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718HV1100'
        data.append(self.set_data(idir, 5, 5, 5, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718G10e5'
        data.append(self.set_data(idir, 6, 6, 6, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718G12e5'
        data.append(self.set_data(idir, 7, 7, 7, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130718G15e5'
        data.append(self.set_data(idir, 8, 8, 8, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        data.append(self.set_data(idir, 9, 30, 9, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        ##_____ 2013/07/23 _____
        idir = os.path.join(basedir, '130723')
        nped = 100
        thrd = 54.2  ## > 3Hit
        detmap = 'N20130723G15e5v2'
        data.append(self.set_data(idir, 1, 41, 1, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event
        detmap = 'N20130723G15e5v3'
        data.append(self.set_data(idir, 42, 49, 2, detmap, 1, 10, nped, thrd))  ## FV full, 0.3Hz, 100 event        
        return data

    ## __________________________________________________
    def initCosmicData(self):
        '''
        '''
        basedir = 'exp/cosmic/'
        data = []
        ##_____ 2012/06/22, 23, 27  _____
        idir = os.path.join(basedir, '120622')
        nped = 20
        thrd = 40
        detmap = 'detmap/detmapN20120620G15e5v2'
        data.append(self.set_data(idir, 1, 4, 1, detmap, 1, 100, nped, thrd))
        idir = os.path.join(basedir, '120623')
        data.append(self.set_data(idir, 1, 26, 1, detmap, 1, 100, nped, thrd))
        idir = os.path.join(basedir, '120627')
        data.append(self.set_data(idir, 1, 35, 1, detmap, 1, 100, nped, thrd))
        ##_____ 2012/11/19, 21 _____
        idir = os.path.join(basedir, '121119')
        detmap = 'detmap/detmapG15e5C20121119'
        nped = 50
        thrd = 10
        data.append(self.set_data(idir, 1, 7, 1, detmap, 1, 100, nped, thrd))
        detmap = 'detmap/detmapG15e5C20121121'
        data.append(self.set_data(idir, 1, 10, 1, detmap, 1, 100, nped, thrd))
        ##_____ 2012/12/05 _____
        idir = os.path.join(basedir, '121205')
        detmap = 'detmap/C20121205G15e5'    ## <-- same data as L20121204G15e5ALL
        nped = 50
        thrd = 40
        data.append(self.set_data(idir, 1, 4, 1, detmap, 1, 100, nped, thrd))
        data.append(self.set_data(idir, 5, 10, 2, detmap, 1, 100, nped, thrd))
        ##_____ 2012/12/06 _____
        idir = os.path.join(basedir, '121206')
        detmap = 'detmap/C20121205G15e5'    ## ? I'm not sure if this is correct detmap
        nped = 50
        thrd = 40
        data.append(self.set_data(idir, 1, 2, 1, detmap, 1, 100, nped, thrd))    ### a HV-MF went off at end of RUN3 and whole RUN4
        data.append(self.set_data(idir, 5, 22, 2, detmap, 1, 100, nped, thrd))    ### a HV-MF went off at end of RUN3 and whole RUN4
        ##_____ 2012/12/10 _____
        idir = os.path.join(basedir, '121210')
        detmap = 'detmap/C20121205G15e5'    ## ? I'm not sure if this is correct detmap
        nped = 50
        thrd = 40
        data.append(self.set_data(idir, 1, 8, 1, detmap, 1, 100, nped, thrd))
        ##_____ 2012/12/12 _____
        idir = os.path.join(basedir, '121212')
        detmap = 'detmap/MR43G15e5v2'    ## <-- same data as L20121204G15e5ALL
        nped = 50
        thrd = 40
        data.append(self.set_data(idir, 1, 4, 1, detmap, 1, 100, nped, thrd))
        ##_____ 2012/12/13 _____
        idir = os.path.join(basedir, '121213')
        detmap = 'detmap/MR43G15e5v2'    ## <-- same data as L20121204G15e5ALL
        nped = 50
        thrd = 40
        data.append(self.set_data(idir, 1, 4, 1, detmap, 1, 100, nped, thrd))

        return data

    ## __________________________________________________
    def initLedcalibData(self):
        '''
        '''
        basedir = 'exp/led/'
        data = []
        ##_____ 2012/05/25 _____
        idir = os.path.join(basedir, '120525')
        detmap = 'detmap/detmapG15e5MR43v1'
        nped = 100
        thrd = 10
        data.append(self.set_data(idir, 1, 1, 1, detmap, 1, 12500, nped, thrd))
        ##_____ 2012/08/08 _____
        idir = os.path.join(basedir, '120808')
        detmap = 'detmap/detmapG15e5MR43v1'
        nped = 100
        thrd = 10
        data.append(self.set_data(idir, 1, 2, 1, detmap, 1, 12500, nped, thrd))
        data.append(self.set_data(idir, 3, 6, 2, detmap, 1, 12450, nped, thrd))
        ##_____ 2012/11/26 _____
        idir = os.path.join(basedir, '121126')
        detmap = 'detmap/L20121126G15e5PMT009'  ## Applied HV to PMT009 only
        nped = 100
        thrd = 10
        ### fvstat  = pulse width [ns]
        ### daqmode = 10000 + LEDV[mV]
        data.append(self.set_data(idir, 1, 1, 1, detmap, 300, 13000, nped, thrd))  ## LED:300ns, 3.0V
        data.append(self.set_data(idir, 2, 2, 2, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V
        data.append(self.set_data(idir, 3, 3, 3, detmap, 280, 13000, nped, thrd))  ## LED:280ns, 3.0V
        data.append(self.set_data(idir, 4, 4, 4, detmap, 270, 13000, nped, thrd))  ## LED:270ns, 3.0V
        data.append(self.set_data(idir, 5, 5, 5, detmap, 300, 12900, nped, thrd))  ## LED:300ns, 2.9V
        data.append(self.set_data(idir, 6, 6, 6, detmap, 300, 12800, nped, thrd))  ## LED:300ns, 2.8V
        data.append(self.set_data(idir, 7, 7, 7, detmap, 300, 13100, nped, thrd))  ## LED:300ns, 3.1V
        data.append(self.set_data(idir, 8, 8, 8, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V
        data.append(self.set_data(idir, 9, 9, 9, detmap, 280, 13100, nped, thrd))  ## LED:280ns, 3.1V
        data.append(self.set_data(idir, 10, 10, 10, detmap, 270, 13100, nped, thrd))  ## LED:270ns, 3.1V
        ### Fixed LEDV to 3.0 V
        data.append(self.set_data(idir, 11, 11, 11, detmap, 300, 13000, nped, thrd))  ## LED:300ns, 3.0V
        data.append(self.set_data(idir, 12, 12, 12, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V
        data.append(self.set_data(idir, 13, 13, 13, detmap, 280, 13000, nped, thrd))  ## LED:280ns, 3.0V
        data.append(self.set_data(idir, 14, 14, 14, detmap, 310, 13000, nped, thrd))  ## LED:310ns, 3.0V
        data.append(self.set_data(idir, 15, 15, 15, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V
        data.append(self.set_data(idir, 16, 16, 16, detmap, 320, 13000, nped, thrd))  ## LED:320ns, 3.0V
        ### Fixed Pulse Width to 290 ns
        data.append(self.set_data(idir, 17, 17, 17, detmap, 290, 13050, nped, thrd))  ## LED:290ns, 3.05V
        data.append(self.set_data(idir, 18, 18, 18, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.10V
        data.append(self.set_data(idir, 19, 19, 19, detmap, 290, 12950, nped, thrd))  ## LED:290ns, 2.95V
        data.append(self.set_data(idir, 20, 20, 20, detmap, 290, 12900, nped, thrd))  ## LED:290ns, 2.90V
        ### Applied 1100V & 1200V to PMT177 (<-- changed PMT)
        detmap = 'detmap/L20121126HV1100PMT177'  ## Applied HV1100V to PMT177 only
        data.append(self.set_data(idir, 21, 21, 21, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V
        data.append(self.set_data(idir, 22, 22, 22, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V
        detmap = 'detmap/L20121126HV1200PMT177'  ## Applied HV1200V to PMT177 only
        data.append(self.set_data(idir, 23, 23, 23, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V
        ### Applied HV to ALL PMTs (but found out that HV Module 5 was masked)
        detmap = 'detmap/L20121126G15e5ALL'
        data.append(self.set_data(idir, 24, 24, 24, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V
        data.append(self.set_data(idir, 25, 25, 25, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V
        data.append(self.set_data(idir, 26, 26, 26, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V
        ##_____ 2012/12/04
        idir = os.path.join(basedir, '121204')
        detmap = 'L20121204G15e5PMT009'
        nped = 100
        thrd = 10
        ### use PMT009
        ### fvstat  = pulse width [ns]
        ### daqmode = 10000 + LEDV[mV] : w/  GATEbar
        ### daqmode = 20000 + LEDV[mV] : w/o GATEbar
        data.append(self.set_data(idir, 1, 1, 1, detmap, 290, 12900, nped, thrd))  ## LED:290ns, 2.9V, w/ GATEbar
        data.append(self.set_data(idir, 2, 2, 2, detmap, 290, 22900, nped, thrd))  ## LED:290ns, 2.9V, w/o GATEbar
        data.append(self.set_data(idir, 3, 3, 3, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V, w/ GATEbar
        data.append(self.set_data(idir, 4, 4, 4, detmap, 290, 23000, nped, thrd))  ## LED:290ns, 3.0V, w/o GATEbar
        data.append(self.set_data(idir, 5, 5, 5, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V, w/ GATEbar
        data.append(self.set_data(idir, 6, 6, 6, detmap, 290, 13000, nped, thrd))  ## LED:290ns, 3.0V, w/ GATEbar
        data.append(self.set_data(idir, 7, 7, 7, detmap, 290, 13100, nped, thrd))  ## LED:290ns, 3.1V, w/ GATEbar
        data.append(self.set_data(idir, 8, 8, 8, detmap, 290, 13200, nped, thrd))  ## LED:290ns, 3.2V, w/ GATEbar
        data.append(self.set_data(idir, 9, 9, 9, detmap, 290, 13200, nped, thrd))  ## LED:290ns, 3.2V, w/ GATEbar
        data.append(self.set_data(idir, 10, 10, 10, detmap, 290, 13200, nped, thrd))  ## LED:290ns, 3.2V, w/ GATEbar
        ### ATM(only board#7) --> LinearFin/Fout --> FADC
        srun, erun = 12, 16
        for irun in range(srun, erun+1):
            data.append(self.set_data(idir, irun, irun, irun, detmap, 290, 13200, nped, thrd))  ## LED:290ns, 3.2V, w/ GATEbar
        ### ATM(only board#7) --> directly --> FADC
        srun, erun = 17, 21
        for irun in range(srun, erun+1):
            data.append(self.set_data(idir, irun, irun, irun, detmap, 290, 13200, nped, thrd))  ## LED:290ns, 3.2V, w/ GATEbar
        ### ATM(all boards) --> LinearFin/Fout --> FADC
        srun, erun = 22, 26
        for irun in range(srun, erun+1):
            data.append(self.set_data(idir, irun, irun, irun, detmap, 290, 13200, nped, thrd))  ## LED:290ns, 3.2V, w/ GATEbar
        ### Applied HV to all PMTs
        detmap = 'L20121204G15e5ALL'
        nped = 100
        thrd = 40
        ## data taken with 1Hz clock
        data.append(self.set_data(idir, 27, 27, 27, detmap, 290, 12900, nped, thrd))  ## LED:290ns, 2.9V, w/ GATEbar, 1HzTrigger
        data.append(self.set_data(idir, 28, 28, 28, detmap, 290, 22900, nped, thrd))  ## LED:290ns, 2.9V, w/o GATEbar, 1HzTrigger
        ## data taken with 2.48s beam timing
        data.append(self.set_data(idir, 29, 29, 29, detmap, 290, 12900, nped, thrd))  ## LED:290ns, 2.9V, w/ GATEbar, BeamTrigger
        data.append(self.set_data(idir, 30, 30, 30, detmap, 290, 22900, nped, thrd))  ## LED:290ns, 2.9V, w/ GATEbar, BeamTrigger

        ##_____ 2013/07/23 _____
        ## Settings of Pulse Generator : [Pulse] Freq:10Hz, Hi:3.0V, Lo:0.0V, Width:270nsec, Edge:5.0ns
        ## Settings of External Trigger: [Burst] Source:Ext, Slope:rising edge
        ## FADC gate width = 2usec (changed from 14usec)
        ## HitTrig threshold = -102.7mV (changed from -54.4mV)
        idir = os.path.join(basedir, '130723')
        nped = 100
        thrd = 102.7  ##  require many Hits
        width = 270
        ledv = 13000
        ## FV full, flash ~0.3Hz, 100 events/run, add WIN160, 70, 4, 43 mask
        detmap = 'L20130723G15e5v1'
        data.append(self.set_data(idir, 1, 10, 1, detmap, width, ledv, nped, thrd))
        ## FV full, flash ~0.3Hz, 100 events/run, removed WIN160, 70, 4, 43 mask
        detmap = 'L20130723G15e5v2'
        data.append(self.set_data(idir, 11, 20, 2, detmap, width, ledv, nped, thrd))

        ##_____ 2013/07/24 _____
        ## PG, ET, FADCgate, HitTrgThrd settings are same as 2013/07/23
        idir = os.path.join(basedir, '130724')
        nped = 100
        thrd = 102.7  ##  require many Hits
        width = 270
        ## FV full, LED OFF, flash ~0.3Hz, 100 events/run
        ## WIN160, 70, 4, 43 are masked additionally
        detmap = 'L20130724G15e5v1'
        ledv = 10000
        data.append(self.set_data(idir, 1, 10, 1, detmap, width, ledv, nped, thrd))
        ## FV full, LED ON ~0.3Hz, 100 events/run, add WIN160, 70, 4, 43 masked
        ledv = 13000
        data.append(self.set_data(idir, 11, 20, 2, detmap, width, ledv, nped, thrd))
        data.append(self.set_data(idir, 21, 30, 3, detmap, width, ledv, nped, thrd))
        data.append(self.set_data(idir, 31, 40, 4, detmap, width, ledv, nped, thrd))
        ## FV full, LED ON ~0.3Hz, 100 events/run, add WIN160, 70, 4, 43 masked
        ledv = 13080
        data.append(self.set_data(idir, 41, 50, 5, detmap, width, ledv, nped, thrd))
        data.append(self.set_data(idir, 51, 60, 6, detmap, width, ledv, nped, thrd))
        data.append(self.set_data(idir, 61, 70, 7, detmap, width, ledv, nped, thrd))
        data.append(self.set_data(idir, 71, 80, 8, detmap, width, ledv, nped, thrd))
        data.append(self.set_data(idir, 81, 90, 9, detmap, width, ledv, nped, thrd))
        return data

    ## __________________________________________________
    def initQtcalibData(self):
        '''
        '''
        basedir = 'exp/qtcalib/'
        data = []
        ##_____ 2012/11/23 _____
        idir = os.path.join(basedir, '121123')
        detmap = 'detector_map'
        nped = 50
        thrd = 10
        ## daqmode = 1000 + QT [pC]
        data.append(self.set_data(idir, 1, 1, 1, detmap, -42, 1600, nped, thrd))  ## QT:600pC
        data.append(self.set_data(idir, 2, 2, 2, detmap, -42, 1300, nped, thrd))  ## QT:300pC
        data.append(self.set_data(idir, 3, 3, 3, detmap, -42, 1100, nped, thrd))  ## QT:100pC
        data.append(self.set_data(idir, 4, 4, 4, detmap, -42, 1050, nped, thrd))  ## QT: 50pC
        data.append(self.set_data(idir, 5, 5, 5, detmap, -42, 1030, nped, thrd))  ## QT: 30pC
        data.append(self.set_data(idir, 6, 6, 6, detmap, -42, 1020, nped, thrd))  ## QT: 20pC
        data.append(self.set_data(idir, 7, 7, 7, detmap, -42, 1010, nped, thrd))  ## QT: 10pC
        data.append(self.set_data(idir, 8, 8, 8, detmap, -42, 1005, nped, thrd))  ## QT:  5pC
        data.append(self.set_data(idir, 9, 9, 9, detmap, -42, 1003, nped, thrd))  ## QT:  3pC
        data.append(self.set_data(idir, 10, 10, 10, detmap, -42, 1002, nped, thrd))  ## QT:  2pC
        data.append(self.set_data(idir, 11, 11, 11, detmap, -42, 1001, nped, thrd))  ## QT:  1pC
        data.append(self.set_data(idir, 12, 12, 12, detmap, -42, 1000, nped, thrd))  ## QT:  0pC
        ## daqmode = 1000 + QT [pC], slightly change GATE timing
        data.append(self.set_data(idir, 13, 13, 13, detmap, -42, 1100, nped, thrd))  ## QT: 100pC
        data.append(self.set_data(idir, 14, 14, 14, detmap, -42, 1050, nped, thrd))  ## QT:  50pC
        data.append(self.set_data(idir, 15, 15, 15, detmap, -42, 1010, nped, thrd))  ## QT:  10pC
        data.append(self.set_data(idir, 16, 16, 16, detmap, -42, 1005, nped, thrd))  ## QT:   5pC
        ## daqmode = 1 + QT [pC], slightly change GATE timing (faster)
        data.append(self.set_data(idir, 17, 17, 17, detmap, -42, 1100, nped, thrd))  ## QT: 100pC
        data.append(self.set_data(idir, 18, 18, 18, detmap, -42, 1050, nped, thrd))  ## QT:  50pC
        data.append(self.set_data(idir, 19, 19, 19, detmap, -42, 1010, nped, thrd))  ## QT:  10pC
        data.append(self.set_data(idir, 20, 20, 20, detmap, -42, 1005, nped, thrd))  ## QT:   5pC
        ##_____
        ## QT output fixed to 100 [pC]
        ## fvstat  = 1000 + delay length af gate generator [ns]
        ## daqmode = 1000 + delay length between TRGout and TRGin [ns]
        trgdelay = 1000 + 500
        data.append(self.set_data(idir, 21, 21, 21, detmap, 1000, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:  0ns
        data.append(self.set_data(idir, 22, 22, 22, detmap, 1000, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:  0ns
        data.append(self.set_data(idir, 23, 23, 23, detmap, 1160, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:160ns
        data.append(self.set_data(idir, 24, 24, 24, detmap, 1240, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:240ns
        data.append(self.set_data(idir, 25, 25, 25, detmap, 1400, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:400ns
        data.append(self.set_data(idir, 26, 26, 26, detmap, 1560, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:560ns
        data.append(self.set_data(idir, 27, 27, 27, detmap, 1640, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:640ns
        data.append(self.set_data(idir, 28, 28, 28, detmap, 1720, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:720ns
        data.append(self.set_data(idir, 29, 29, 29, detmap, 1800, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:800ns
        data.append(self.set_data(idir, 30, 30, 30, detmap, 1880, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:880ns
        ## fvstat = 1000 + time difference between (TRGin - FADC gate)
        data.append(self.set_data(idir, 31, 31, 31, detmap, 1000, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate: 0ns
        data.append(self.set_data(idir, 32, 32, 32, detmap, 1020, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:20ns
        data.append(self.set_data(idir, 33, 33, 33, detmap, 1040, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:40ns
        data.append(self.set_data(idir, 34, 34, 34, detmap, 1060, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:60ns
        data.append(self.set_data(idir, 35, 35, 35, detmap, 1080, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:80ns
        data.append(self.set_data(idir, 36, 36, 36, detmap, 1048, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:48ns
        data.append(self.set_data(idir, 37, 37, 37, detmap, 1056, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:56ns
        data.append(self.set_data(idir, 38, 38, 38, detmap, 1064, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:64ns
        data.append(self.set_data(idir, 39, 39, 39, detmap, 1072, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:72ns
        data.append(self.set_data(idir, 40, 40, 40, detmap, 1032, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:32ns
        data.append(self.set_data(idir, 41, 41, 41, detmap, 1024, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:24ns
        data.append(self.set_data(idir, 42, 42, 42, detmap, 1016, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:16ns
        data.append(self.set_data(idir, 43, 43, 43, detmap, 1008, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate: 8ns
        data.append(self.set_data(idir, 44, 44, 44, detmap,  992, 1500, nped, thrd))  ## QT:100pC, dTRG:500ns, dGate:-8ns
        ##
        data.append(self.set_data(idir, 45, 45, 45, detmap, 1080, 1600, nped, thrd))  ## QT:100pC, dTRG:600ns, dGate: 80ns
        data.append(self.set_data(idir, 46, 46, 46, detmap,  980, 1600, nped, thrd))  ## QT:100pC, dTRG:600ns, dGate:-80ns
        data.append(self.set_data(idir, 47, 47, 47, detmap,  992, 1600, nped, thrd))  ## QT:100pC, dTRG:600ns, dGate: -8ns
        data.append(self.set_data(idir, 48, 48, 48, detmap, 1016, 1600, nped, thrd))  ## QT:100pC, dTRG:600ns, dGate: 16ns
        data.append(self.set_data(idir, 49, 49, 49, detmap, 1040, 1600, nped, thrd))  ## QT:100pC, dTRG:600ns, dGate: 40ns
        data.append(self.set_data(idir, 50, 50, 50, detmap, 1060, 1600, nped, thrd))  ## QT:100pC, dTRG:600ns, dGate: 60ns
        ##
        data.append(self.set_data(idir, 51, 51, 51, detmap, 1060, 1700, nped, thrd))  ## QT:100pC, dTRG:700ns, dGate: 60ns
        data.append(self.set_data(idir, 52, 52, 52, detmap, 1060, 1800, nped, thrd))  ## QT:100pC, dTRG:700ns, dGate: 60ns
        ### add additional 15m BNC, so actual gate timing will be 60ns - 75ns = -15ns
        data.append(self.set_data(idir, 53, 53, 53, detmap, 1060, 1800, nped, thrd))  ## QT:100pC, dTRG:700ns, dGate: 60ns
        ##_____ 2012/11/28 _____
        idir = os.path.join(basedir, '121128')
        # detmap = 
        ### Calibrate ATMch PP14-5 (PMT0009), no HV applied to the detector
        ### fvstat  = trgdelay = 1000 + (TRGout - TRGin)
        ### daqmode = qtcharge = 1000 + QT output [pC]
        data.append(self.set_data(idir, 1, 1, 1, detmap, 1500, 1600, nped, thrd))  ## dTRG:500ns, QT:600pC
        data.append(self.set_data(idir, 2, 2, 2, detmap, 1500, 1600, nped, thrd))  ## dTRG:500ns, QT:600pC
        data.append(self.set_data(idir, 3, 3, 3, detmap, 1500, 1600, nped, thrd))  ## dTRG:500ns, QT:600pC
        data.append(self.set_data(idir, 4, 4, 4, detmap, 1500, 1600, nped, thrd))  ## dTRG:500ns, QT:600pC
        data.append(self.set_data(idir, 5, 5, 5, detmap, 1500, 1300, nped, thrd))  ## dTRG:500ns, QT:300pC
        data.append(self.set_data(idir, 6, 6, 6, detmap, 1500, 1200, nped, thrd))  ## dTRG:500ns, QT:200pC
        data.append(self.set_data(idir, 7, 7, 8, detmap, 1500, 1100, nped, thrd))  ## dTRG:500ns, QT:100pC
        data.append(self.set_data(idir, 8, 8, 9, detmap, 1500, 1050, nped, thrd))  ## dTRG:500ns, QT: 50pC
        data.append(self.set_data(idir, 9, 9, 9, detmap, 1500, 1030, nped, thrd))  ## dTRG:500ns, QT: 30pC
        data.append(self.set_data(idir, 10, 10, 10, detmap, 1500, 1020, nped, thrd))  ## dTRG:500ns, QT: 20pC
        data.append(self.set_data(idir, 11, 11, 11, detmap, 1500, 1010, nped, thrd))  ## dTRG:500ns, QT: 10pC
        data.append(self.set_data(idir, 12, 12, 12, detmap, 1500, 1005, nped, thrd))  ## dTRG:500ns, QT:  5pC
        data.append(self.set_data(idir, 13, 13, 13, detmap, 1500, 1003, nped, thrd))  ## dTRG:500ns, QT:  3pC
        data.append(self.set_data(idir, 14, 14, 14, detmap, 1500, 1002, nped, thrd))  ## dTRG:500ns, QT:  2pC
        data.append(self.set_data(idir, 15, 15, 15, detmap, 1500, 1001, nped, thrd))  ## dTRG:500ns, QT:  1pC
        data.append(self.set_data(idir, 16, 16, 16, detmap, 1500, 1000, nped, thrd))  ## dTRG:500ns, QT:  0pC
        ##_____ 2012/12/03 _____
        idir = os.path.join(basedir, '121203')
        ### maybe a mistake ?
        ### fvstat  = trgdelay = 1000 + (TRGout - TRGin)
        ### daqmode = qtcharge = 1000 + QT output [pC]
        data.append(self.set_data(idir, 1, 1, 1, detmap, 1500, 1000, nped, thrd))  ## dTRG:500ns, QT:  0pC
        data.append(self.set_data(idir, 2, 2, 2, detmap, 1500, 1000, nped, thrd))  ## dTRG:500ns, QT:  0pC
        return data

    ## __________________________________________________
    def init_data(self):
        b = self.initBeamData()
        n = self.initNoiseData()
        c = self.initCosmicData()
        l = self.initLedcalibData()
        q = self.initQtcalibData()

        m = b
        m.extend(n)
        m.extend(c)
        m.extend(l)
        m.extend(q)

        self.logger.info('data initialized : %d', len(m))

        # for data in m:
        #     print data
        return m

    ## __________________________________________________
    def set_data(self, idir, srun, erun, subrun, imap, fvstat, daqmode, nped, thrd):
        d = {}
        d['idir'] = idir
        d['srun'] = int(srun)
        d['erun'] = int(erun)
        d['subrun'] = int(subrun)
        if imap.startswith('detmap/'):
            imap = imap.replace('detmap/', '')
        if imap.endswith('.txt'):
            imap = imap.replace('.txt', '')
        d['map'] = os.path.join(Config.DetDir, imap) + '.txt'
        d['fv'] = int(fvstat)
        d['dmode'] = int(daqmode)
        d['nped'] = int(nped)
        d['thrd'] = float(thrd)

        ### additional info
        top, sub = os.path.split(idir.rstrip('/'))
        d['itop'] = top  ## = 'exp/beam', 'exp/noise', ...
        d['isub'] = sub  ## = 'MR42', date
        arun = sub.replace('MR', '')
        orun = int('{0:03d}{1:04d}'.format(int(arun), int(subrun)))
        ofn = 'upk{0:07d}.root'.format(orun)
        odir = os.path.join(top, Config.UpkDir)
        ofn = os.path.join(odir, ofn)
        d['orun'] = int(orun)
        d['odir'] = odir
        d['ofn'] = ofn
        d['opt'] = ''

        log = '{0[idir]} {0[srun]:4d} {0[erun]:4d} ==> {0[ofn]}'.format(d)
        #self.logger.debug(log)
        return d

    ## __________________________________________________
    def get_data(self, key, value):
        data = self.data
        new_data = []
        for d in data:
            if key in ['srun', 'erun', 'irun', 'run']:
                if value in range(d['srun'], d['erun']+1):
                    self.logger.debug('key "%s" matched : %s', key, d)
                    new_data.append(d)
            elif d[key] == value:
                self.logger.debug('key "%s" matched : %s', key, d)
                new_data.append(d)
        if len(new_data) < 1:
            self.logger.error('No matching data for {0} = {1}'.format(key, value))
        self.data = new_data

        self.logger.debug('matched data : %d', len(new_data))
        return len(new_data)

    ## __________________________________________________
    def print_data(self):
        data = self.data
        for d in data:
            log = '[print_data:len={1}] | {0[idir]} {0[srun]:4d} - {0[erun]:4d} ==> {0[ofn]} | {0[orun]}'.format(d, len(data))
            self.logger.info(log)
            
    ## __________________________________________________
    def get_irun(self, ifn):
        '''
        extract 'topdir', 'subdir' and 'irun' from input filename.
        '''
        if not os.path.exists(ifn):
            self.logger.error('file "{0}" does not exist. Exit.'.format(ifn))
            sys.exit(-1)

        irun = {}
        idir, fn = os.path.split(ifn)
        top, sub = os.path.split(idir)
        arun = sub.replace('MR', '')
        run, ext = os.path.splitext(fn)
        run = run.replace('run', '')
        irun['itop'] = top
        irun['isub'] = sub
        irun['irun'] = int(run)
        self.logger.debug('irun : %s', irun)
        return irun

    ## ________________________________________
    def get_iruns(self, ifns):
        iruns = []
        for i, ifn in enumerate(ifns):
            self.logger.debug('ifns[%d] : %s', i, ifn)
            irun = self.get_irun(ifn)
            iruns.append(irun)
        return iruns

    ## __________________________________________________
    def set_options(self, data):
        if self.nped:
            data['nped'] = self.nped
        if self.thrd:
            data['thrd'] = self.thrd
        if self.opt:
            data['opt'] = self.opt
        return data

    ## __________________________________________________
    def unpack_wave(self, iruns):
        idata = self.data
        for i, run in enumerate(iruns):
            self.logger.debug('iruns[%d] : %s', i, run)
            ndata = self.get_data('isub', run['isub'])
            ndata = self.get_data('irun', run['irun'])
            ndata = self.get_data('itop', run['itop'])
            if not ndata == 1:
                self.logger.warning('ndata = %d ==>  number of matched data > 1', ndata)
            for d in self.data:
                isub = int(run['isub'].replace('MR', ''))
                irun = int(run['irun'])
                orun = int('{0:03d}{1:04d}'.format(isub, irun%10000))
                ofn = 'wave{0:07d}.root'.format(orun)
                odir = d['odir'].replace('upk', 'wave')

                if not os.path.exists(odir):
                    self.logger.error('directory "{0}" does not exist'.format(odir))
                    sys.exit(-1)

                d['ofn'] = os.path.join(odir, ofn)
                self.set_options(d)
                com = './unpack {0[opt]} -o {0[ofn]} -d {0[map]} -p {0[nped]} -t {0[thrd]} {0[idir]} {1} {1} {0[fv]} {0[dmode]} {1}'.format(d, irun)
                if self.dryrun:
                    self.logger.warning('[DRYRUN] ' + com)
                else:
                    self.logger.info(com)
                    os.system(com)
            self.data = idata
        return
                
    ## __________________________________________________
    def unpack_upk(self, iruns):
        oruns = []
        itops = []
        idata = self.data  ## keep initial data
        for i, run in enumerate(iruns):
            self.logger.debug('iruns[%d] : %s', i, run)
            ndata = self.get_data('isub', run['isub'])  ## get_data overwrites selt.data
            ndata = self.get_data('irun', run['irun'])
            ndata = self.get_data('itop', run['itop'])
            if not ndata == 1:
                self.logger.warning('ndata = %d ==>  number of matched data > 1', ndata)
            for d in self.data:
                orun = d['orun']
                itop = d['itop']
                oruns.append(orun)
                itops.append(itop)
            self.data = idata  ## reset with initial data

        self.logger.debug('before sorted(oruns) : %s', oruns)
        oruns = sorted(set(oruns))
        self.logger.debug('after  sorted(oruns) : %s', oruns)

        self.logger.debug('before sorted(itops) : %s', itops)
        itops = sorted(set(itops))
        self.logger.debug('after  sorted(itops) : %s', itops)

        if len(itops) > 1:
            self.logger.error('more than 1 data type specified : %s', itops)
            sys.exit(-1)

        self.data = idata
        for i, run in enumerate(oruns):
            self.logger.debug('orun[%d] : %s', i, run)
            ndata = self.get_data('orun', run)
            ndata = self.get_data('itop', itops[0])
            if not ndata == 1:
                self.logger.warning('ndata = %d ==>  number of matched data > 1', ndata)
            for d in self.data:
                self.set_options(d)
                com = './unpack {0[opt]} -o {0[ofn]} -d {0[map]} -p {0[nped]} -t {0[thrd]} {0[idir]} {0[srun]} {0[erun]} {0[fv]} {0[dmode]} {0[orun]}'.format(d)
                if self.dryrun:
                    self.logger.warning('[DRYRUN] ' + com)
                else:
                    self.logger.info(com)
                    os.system(com)
            self.data = idata
        return

## ______________________________________________________________________
if __name__ == '__main__':

    usage = '%prog [options] raw_root_files'
    usage += '\n'
    usage += '\n'
    usage += '  Unpack data\n'
    usage += '    Ex1: %prog -x exp/beam/MR42/run0420001.root\n'
    usage += '    Ex2: %prog -x exp/noise/120522/run0000001.root\n'
    usage += '\n'
    usage += '  Re-Unpack data\n'
    usage += '    Ex3: %prog --reprocess exp/beam/MR42/run04*.root\n'
    usage += '\n'
    usage += '  Unpack data with waveform\n'
    usage += '    Ex4: %prog -xf exp/beam/MR42/run0420001.root\n'
    usage += '    Ex5: %prog -xf exp/beam/MR49/run049*.root\n'
    usage += '\n'

    desc = 'Unpack data in exp/MODE/DIR.\n'
    desc += 'Several runs are put together into one upk file (~100 runs / upk).\n'
    desc += 'Unpacked files are saved in exp/MODE/upk/upk%07d.root.\n'
    desc += 'If "-f" option is specified, data are unpacked with FADC waveform data (1 run / wave).\n'
    desc += 'Unpacked files are saved in exp/MODE/wave/wave%07d.root.\n'
    desc += 'This program runs in "DRY RUN" mode by defalut for now.\n'
    desc += 'If you are reprocessing data, try using "--reprocess" option.\n'
    
    parser = OptionParser(usage)

    parser.set_description(desc)

    parser.add_option('-w', '--overwrite',
                      dest = 'overwrite',
                      action = 'store_true',
                      help = 'overwrite existing files')
    parser.add_option('-f', '--full',
                      dest = 'umode',
                      action = 'store_true',
                      help = 'unpack with FADC waveform')
    parser.add_option('-q', '--quiet',
                      dest = 'quiet',
                      action = 'store_true',
                      help = 'set quiet mode')
    parser.add_option('-n', '--dryrun',
                      dest = 'dryrun',
                      action = 'store_true',
                      help = 'dry run')
    parser.add_option('-x', '--no-dryrun',
                      dest = 'dryrun',
                      action = 'store_false',
                      help = 'no dry run')
    
    parser.add_option('-p', '--nped',
                      dest = 'nped',
                      help = 'set FADC pedestal points')
    parser.add_option('-t', '--thrd',
                      dest = 'thrd',
                      help = 'set FADC hit threshold')

    parser.add_option('--reprocess',
                      dest = 'reprocess',
                      action = 'store_true',
                      help = 'reprocess, (same as "-wqx" option)')

    parser.set_defaults(overwrite = False,
                        umode = False,
                        quiet = False,
                        dryrun = True,
                        nped = None,
                        thrd = None,
                        reprocess = False,
                        verbose = 2)
    
    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error('Wrong number of arguments ({0})'.format(len(args)));

    if options.reprocess:
        options.overwrite = True
        options.quiet = True
        options.dryrun = False

    au = AutoUnpack(options)
    iruns = au.get_iruns(args)
    if options.umode:
        au.unpack_wave(iruns)
    else:
        au.unpack_upk(iruns)
