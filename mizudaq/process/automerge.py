#! /usr/bin/env python
# -*- coding:utf-8 -*-

import os, sys
from optparse import OptionParser

## ________________________________________________________________________________
class AutoMerge:
    ## __________________________________________________
    def __init__(self, options=None):
        self.dryrun = options.dryrun
        self.opt = None
        opt = ''
        if options.quiet:
            opt += 'q'
        if options.overwrite:
            opt += 'w'
        if len(opt) > 0:
            self.opt = '-' + opt

        log = '[__init__]: Options: {0}\n'.format(options)
        
    ## __________________________________________________
    def get_irun(self, ifn):
        '''
        extract UPK run# from input filename.
        '''
        if not os.path.exists(ifn):
            sys.stderr.write('Error: No file named "{0}". Exit.\n'.format(ifn))
            sys.exit(-1)
        else:
            irun = {}
            idir, fn = os.path.split(ifn)
            run, ext = os.path.splitext(fn)
            run = run.replace('upk', '')
            irun['irun'] = int(run)
            return irun
            
    ## ________________________________________
    def get_iruns(self, ifns):
        iruns = []
        for ifn in ifns:
            irun = self.get_irun(ifn)
            iruns.append(irun)
        return iruns

    ## __________________________________________________
    def merge(self, iruns):
        for irun in iruns:
            print '-' * 50
            com = './merge {1} {0[irun]}'.format(irun, self.opt)
            if self.dryrun:
                print '[DRYRUN]\t{0}'.format(com)
            else:
                os.system(com)
        print '-' * 50

## ______________________________________________________________________
if __name__ == '__main__':

    usage = '%prog [options] upk_root_files'
    usage += '\n'
    usage += '\n'
    usage += '  Ex1: %prog -x exp/beam/upk/upk0420001.root\n'
    usage += '  Ex2: %prog -qx exp/beam/upk/upk04200*.root\n'
    usage += '  Ex3: %prog --reprocess exp/beam/upk/upk04*.root\n'
    usage += '\n'

    desc = 'Merge data in exp/beam/upk/ and BSD data.\n'
    desc += 'This script is a looping wrapper for "merge".\n'
    desc += 'Any BeamUpk files as argument.\n'
    desc += 'Merged data (=DST) is saved in exp/beam/dst/.\n'
    desc += 'This program runs in "DRY RUN" mode by defalut for now.\n'
    desc += 'If you are reprocessing data, try using "--reprocess" option.\n'
    
    parser = OptionParser(usage)

    parser.set_description(desc)

    parser.add_option('-w', '--overwrite',
                      dest = 'overwrite',
                      action = 'store_true',
                      help = 'overwrite existing files')
    parser.add_option('-q', '--quiet',
                      dest = 'quiet',
                      action = 'store_true',
                      help = 'set quiet mode')
    parser.add_option('-n', '--dryrun',
                      dest = 'dryrun',
                      action = 'store_true',
                      help = 'dry run')
    parser.add_option('-x', '--no-dryrun',
                      dest = 'dryrun',
                      action = 'store_false',
                      help = 'no dry run')

    parser.add_option('--reprocess',
                      dest = 'reprocess',
                      action = 'store_true',
                      help = 'reprocess, (same as "-wqx" option)')

    parser.add_option('-v',
                      dest = 'verbose',
                      help = 'verbosity level')
    
    parser.set_defaults(overwrite = False,
                        quiet = False,
                        dryrun = True,
                        reprocess = False,
                        verbose = 2)
    
    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error('Wrong number of arguments ({0})'.format(len(args)));

    if options.reprocess:
        options.overwrite = True
        options.quiet = True
        options.dryrun = False

    am = AutoMerge(options)
    iruns = am.get_iruns(args)
    am.merge(iruns)
