#include <iostream>
#include <string>

#include "TStopwatch.h"
#include "Unpacker.h"

// __________________________________________________
void Usage()
{
    TString usage, desc, option;
    usage = "Usage  : ./unpack [-hfodptqw] dir start#";
    usage += " (";
    usage += " [end#=start#]";
    usage += " [fvstat=-42]";
    usage += " [daqmode=-42]";
    usage += " [upkrun#=-42]";
    usage += ")";
    usage += "\n";
    usage += " Ex): ./unpack exp/beam/MR42 420005\n";
    usage += "      ./unpack exp/beam/MR42 420001 420009\n";
    usage += "      ./unpack -o hoge.root exp/beam/MR42 420001 420009\n";
    usage += "      ./unpack -o hoge.root -d detmap/detdet.txt exp/beam/MR42 420001 420009\n";
    usage += "\n";
    
    desc = "Desctiption:\n";
    desc += "    This program decodes raw data into analyzable ROOT format.\n";
    desc += "  Follow usage above.\n";
    desc += "  Arguments and options are described in below.\n";
    desc += "\n";
    desc += "Args:\n";
    desc += "  [1] dir     : path of the directory where raw data is stored.\n";
    desc += "  [2] start#  : start # of the run.\n";
    desc += "  [3] end#    : end # of the run. [default = start #]\n";
    desc += "  [4] fvstat  : status# of FV is as below.\n";
    desc += "       [default]: -42\n";
    desc += "        empty   :   0\n";
    desc += "        full    :   1\n";
    desc += "        half    :   2\n";
    desc += "  [5] daqmode : daq mode # is described in below.\n";
    desc += "       [default] : -42\n";
    desc += "        beam     : 1\n";
    desc += "        noise    : 10\n";
    desc += "        cosmic   : 100\n";
    desc += "        qtcalib  : 1000+ (1 + input QT charge [pC])\n";
    desc += "        ledcalib : 10000+ (1 + applied LED voltage [mV])\n";
    desc += "  [6] upkrun  : upk run #\n";
    desc += "\n";

    option = "Options:\n";
    option += "  -h         : show this help\n";
    option += "  -o FILENAME: set output filename    [unpacked_file.root]\n";
    option += "  -d DETMAP  : set detmap filename    [detector_map.txt]\n";
    option += "  -p NPED    : set points for FADC pedestal calculation     [100]\n";
    option += "  -t HITTHR  : set threshold (mV) for FADC hit calculation  [10]\n";
    option += "  -f         : unpack with waveforms    [false]\n";
    option += "  -q         : run in quiet mode        [false]\n";
    option += "  -w         : force overwrite          [false]\n";

    fprintf(stderr, "%s", usage.Data());
    fprintf(stderr, "%s", desc.Data());
    fprintf(stderr, "%s", option.Data());
    exit(0);
}

// __________________________________________________
int main(int argc,char *argv[])
{
    TStopwatch sw;
    sw.Start();
    
    TString dir;
    Int_t srun, erun;

    Int_t    fvstat  = -42;
    Int_t    daqmode = -42;
    Int_t    upkrun  = -42;
    TString  ofn     = "unpacked_file.root";
    TString  detmap  = "detector_map.txt";
    Int_t    umode   = 0;
    Int_t    nped    = 100;
    Double_t hitthrd = 10;
    Bool_t   quiet     = kFALSE;
    Bool_t   overwrite = kFALSE;
    Int_t    vlevel = 0;    // verbose level ; v
    Int_t    dlevel = 0;    // debug level   ; g
    Int_t    elevel = 0;    // error level   ; e

    Int_t opt;
    while ( (opt = getopt(argc, argv, "hfo:d:p:t:qwvge")) != -1 ) {
        switch(opt) {
        case 'h':
            Usage();
            exit(-1);
        case 'f':
            fprintf(stderr, "[getopt]  Option '-f' was selected\n");
            umode = 1;
            break;
        case 'o':
            fprintf(stderr, "[getopt]  Option '-o : %s' was selected\n", optarg);
            ofn = optarg;
            break;
        case 'd':
            fprintf(stderr, "[getopt]  Option '-d : %s' was selected\n", optarg);
            detmap = optarg;
            break;
        case 'p':
            nped = atoi(optarg);
            fprintf(stderr, "[getopt]  Option '-p : %d' was selected\n", nped);
            break;
        case 't':
            hitthrd = atof(optarg);
            fprintf(stderr, "[getopt]  Option '-t : %.2f' was selected\n", hitthrd);
            break;
        case 'q':
            quiet = kTRUE;
            fprintf(stderr, "[getopt]  Option '-q' was selected.\n");
            break;
        case 'w':
            overwrite = kTRUE;
            fprintf(stderr, "[getopt]  Option '-w' was selected.\n");
            break;
        case 'v':
            vlevel++;
            fprintf(stderr, "[getopt]  Option '-v' was selected.\n");
            break;
        case 'g':
            dlevel++;
            fprintf(stderr, "[getopt]  Option '-g' was selected.\n");
            break;
        case 'e':
            elevel++;
            fprintf(stderr, "[getopt]  Option '-e' was selected.\n");
            break;
        default:
            fprintf(stderr, "[getopt]  Error:\tWrong options\n\n");
            Usage();
        }
    }

    argc -= optind;
    argv += optind;
    
    if (argc < 2) {
        fprintf(stderr, "Wrong number of arguments. (%d)\n", argc);
        Usage();
        exit(-1);
    }
    dir.Form("%s", argv[0]);
    srun = atoi(argv[1]);
    erun = srun;

    if (argc > 2) erun    = atoi(argv[2]);
    if (argc > 3) fvstat  = atoi(argv[3]);
    if (argc > 4) daqmode = atoi(argv[4]);
    if (argc > 5) upkrun  = atoi(argv[5]);

    TString info, tmpinfo;
    info.Form("===== Unpack Information =====\n");
    tmpinfo.Form("  Input dirname   : %s\n", dir.Data());
    info += tmpinfo;
    tmpinfo.Form("  Upk run #       : %d\n", upkrun);
    info += tmpinfo;
    tmpinfo.Form("  Start #         : %d\n", srun);
    info += tmpinfo;
    tmpinfo.Form("  End   #         : %d\n", erun);
    info += tmpinfo;
    tmpinfo.Form("  FV status       : %d\n", fvstat);
    info += tmpinfo;
    tmpinfo.Form("  DAQ mode        : %d\n", daqmode);
    info += tmpinfo;
    tmpinfo.Form("  Output filename : %s\n", ofn.Data());
    info += tmpinfo;
    tmpinfo.Form("  DetMap filename : %s\n", detmap.Data());
    info += tmpinfo;
    tmpinfo.Form("  Unpack mode     : %d\n", umode);
    info += tmpinfo;
    tmpinfo.Form("  Pedestal points : %d\n", nped);
    info += tmpinfo;
    tmpinfo.Form("  Hit threshold   : %.2f\n", hitthrd);
    info += tmpinfo;
    fprintf(stderr, "%s", info.Data());
    
    if (srun > erun) {
        fprintf(stderr, "Error:\tStart#(%d) > End#(%d). Exit.\n", srun, erun);
        exit(-1);
    }

    // Confirm parameter if it is not quiet-mode
    if (!quiet) {
        fprintf(stderr, "Warning:\tIs parameter above correct? (y/[n]) > ");
        Int_t answer, readch;
        readch = getchar();
        answer = readch;
        while (readch != '\n' && readch != EOF) readch = getchar();
        if (answer == 'y' || answer == 'Y') {
            fprintf(stderr, "\tOK\n");
        }
        else if (answer == 'q' || answer == 'Q') {
            fprintf(stderr, "\tAbort. Bye Bye\n");
            gSystem->Abort();
        } else {
            fprintf(stderr, "\tQuit. Bye Bye\n");
            exit(-1);
        }
    } else {
        vlevel = 0;
        dlevel = 0;
        elevel = 0;
    }

    Unpacker *upk = new Unpacker(umode, hitthrd, nped);
    upk->SetVerboseLevel(vlevel);
    upk->SetDebugLevel(dlevel);
    upk->SetErrorLevel(elevel);
    upk->SetLogLevel(kINFO);
    
    upk->OpenUpkFile(ofn.Data(), overwrite);
    upk->SetFvstat(fvstat);
    upk->SetDaqmode(daqmode);
    upk->SetUpkRun(upkrun);
    
    TString ifn;
    TString log;
    for (Int_t irun = srun; irun <= erun; irun++) {
        if (irun == 430089) { ; } // pass
        else if (irun == 421222) {; }
        else if (irun == 420686) {; }
        else {
            ifn.Form("%s/run%07d.root", dir.Data(), irun);
            log.Form("[%s]:\tUnpack '%s' with '%s'.\n", __FUNCTION__, ifn.Data(), detmap.Data());
            upk->PrintVerbose(0, log.Data());
            fprintf(stderr, "--------------------------------------------------\n");
            fprintf(stderr, "%s", log.Data());
            upk->Unpack(ifn.Data(), detmap.Data());
        }
    }
    upk->Write();
    sw.Print();
    return 0;
}

