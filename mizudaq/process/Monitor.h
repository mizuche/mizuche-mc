// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-05 19:42:06+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
#ifndef ROOT_MONITOR
#define ROOT_MONITOR
#ifndef ROOT_TObject
#include "TObject.h"
#endif

class Monitor : public TObject
{
private:
    Int_t   fPort;
    TString fHostname;
    Bool_t  fPrintFlag;

    TCanvas *fCanvas0, *fCanvas1, *fCanvas2;
    T2KWCEvtDisp *fDisp;

    Unpacker fUnpacker;
    T2KWCUnpack *fUpk;
    T2KWCRaw *fRaw[2];

    TSocket  *fSocket;
    TMessage *fMsg;
    
public:
    Monitor();
    Monitor(const Int_t &port, const char* hostname, const Bool_t kPrint);
    virtual ~Monitor() {}

    void SetPort(const Int_t &p)       { fPort = p; }
    void SetHostname(const char* h)    { fHostname = h; }
    void SetPrintFlag(const Bool_t b)  { fPrintFlag = b; }

    Int_t  GetPort(const Int_t &p)      const { return fPort; }
    char*  GetHostname(const char* h)   const { return fHostname; }
    Bool_t GetPrintFlag(const Bool_t b)       { return fPrintFlag; }

    void InitCanvases();
    TH1D *InitHist(const char* name, const char* title,
                   const Double_t &xmin, const Double_t &xmax,
                   const Int_t &kColor, const Int_t &binwidth );
  

    ClassDef(Monitor,0) //DOCUMENT ME
};

#endif
//____________________________________________________________________ 
//  
// EOF
//


//____________________________________________________________________
//
// DOCUMENT ME
// 

//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-05 19:42:49+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
#ifndef ROOT_MONITOR
#include "Monitor.h"
#endif

//____________________________________________________________________
ClassImp(Monitor);

//____________________________________________________________________
Monitor::Monitor()
{
    // Default constructor
}

//____________________________________________________________________
Monitor::Monitor(const Int_t &port, const char* hostname, const Bool_t kPrint)
{
    SetPort(port);
    SetHostname(hostname);
    SetPrintFlag(kPrint);
}

//____________________________________________________________________
TH1D* Monitor::InitHist(const char* name, const char* title,
                        const Double_t &xmin, const Double_t &xmax,
                        const Int_t &kColor, const Int_t &binwidth = 1)
{
    Int_t xbin = (Int_t)xmax - (Int_t)xmin;
    xbin *= binwidth;
    TH1D *h = new TH1D(name, title, xbin, xmin, xmax);
    h->SetLineColor(kColor);
    h->SetFillColor(kColor);
    return h;
}

// ___________________________________________________________________
void Monitor::InitCanvases()
{
    fCanvas0 = new TCanvas("fCanvas0", "Event Display");
    fDisp    = new T2KWCEvtDisp();
    fDisp->Init(fCanvas0);
    fCanvas1 = new TCanvas("fCanvas1", "Updated on Every Spill");
    fCanvas2 = new TCanvas("fCanvas2", "Integration of Every Spill");
}

// ___________________________________________________________________
void Monitor::InitHists()
{

}

//____________________________________________________________________
void Monitor::GetEvents()
{
    Int_t runmode[2];
    Int_t nmsg = 0;

    fSocket = new TSocket(fHostname.Data(), fPort);
    if (fSocket->IsValid()) {
        Int_t isocket = 0;

        while (fSocket->Recv(fMsg)) {
            fRaw[isocket] = (T2KWCRaw*)fMsg->ReadObject(fMsg->GetClass());
            fprintf(stdout, "Receiving message from DAQ\n");
            fprintf(stdout, "Print socket# %d\n", isocket);
            fRaw[isocket]->Print();

            fUnpacker.unpack(fRaw[isocket]);
            fUpk = fUnpacker.GetUpk();

            runmode[isocket] = fUpk->GetRunmode();
            nmsg++;
            delete fMsg;
            isocket++;

            gSystem->ProcessEvents();

            if (isocket == 2) {
                FillHists(fUpk);
                DrawHists();
            }
        }
    }

}

// __________________________________________________
void Monitor::FillHists(T2KWCUnpack *upk)
{
    Int_t runnum    = upk->GetRunNum();
    Int_t spillnum  = upk->GetSpillNum();
    Int_t atm_nhits = upk->GetAtmNhits();
    Int_t atm_npes  = upk->GetAtmNpes();
    Int_t atm_trg   = upk->GetAtmNtrg();
    Int_t atm_gong  = upk->GetAtmGong();
    Int_t mode      = upk->GetRunmode();

}

    

//____________________________________________________________________ 
//  
// EOF
//


//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-05 19:43:16+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
//
#ifndef __CINT__
#ifndef ROOT_TApplication
#include "TApplication.h"
#endif
// PUT HEADERS HERE
#endif

int Monitor(int argc, char **argc)
{
    // DEFINE YOUR MAIN FUNCTION HERE
    Int_t   port      = 11000;
    TString hostname  = "10.105.55.226";
    Bool_t  printflag = kFALSE;

    Int_t opt;
    while ((opt = getopt(argc, argv, "p:h:P")) != -1 ) {
        switch(opt) {
        case 'p':
            port = atoi(optarg);
            break;
        case 'h':
            hostname.Form("%s", optarg);
            break;
        case 'P':
            printflag = kTRUE;
            break;
        default:
            Usage();
            exit(-1);
        }
    }
    argc -= optind;
    argv += optind;

    Monitor *mon = new Monitor(port, hostname, printflag);
    mon->InitCanvas();
    mon->InitHists();
    mon->InitSockets();
    mon->GetEvents();
    return 0;
}

#ifndef __CINT__
int main(int argc, char** argv)
{
    TApplication MonitorApp("MonitorApp", &argc, argv);
    int retVal = Monitor(argc, argv);
    MonitorApp.Run();
    return retVal;
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
