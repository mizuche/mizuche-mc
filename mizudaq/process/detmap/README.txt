Author: Shota TAkahashi
Created: 2012/04/19
Last Updated: 2012/05/28


********************************************************************************
        MapFile Description
********************************************************************************
detmapG15e5MR42v1	Masked=[4, 141, 11, 23, 83],            HV0=[177, 179, 219, 229]
detmapG15e5MR42v2	Masked=[4, 141, 11, 23, 83, 98, 30],    HV0=[177, 179, 219, 229]
detmapG15e5MR42v3	Masked=[4, 141, 11, 23, 83, 98, 30, 87, 171, 111, 93],   HV0=[177, 179, 219, 229]
detmapG15e5MR42v4	Masked=[4, 141, 11, 23, 83, 98, 30, 87, 105],            HV0=[177, 179, 219, 229]
detmapG15e5MR42v6	Masked=[4, 141, 11, 23, 83],            HV0=[177, 179, 219, 229] (<-- same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)
detmapG15e5MR42v7	Masked=[4, 141, 11, 23, 83],            HV0=[177, 179, 219, 229] (with LED correction for gain)
detmapG15e5MR42v8   Masked=[4, 141, 11, 23, 83,             HV0=[177, 179, 219, 229] (<-- revert LED correction, same as v6)
detmapG15e5MR43v1   Masked=[4, 141, 11, 23, 83],            HV0(177, 179, 219, 229) (<-- same as MR42v8)

++++++++++++++++++++++++++++++++++++++++++++++++++
        Beam Data
++++++++++++++++++++++++++++++++++++++++++++++++++
--------------------------------------------------------------------------------
Filename	Run#	DetMapFileName          Corresponding RUN#      Masked & HV=0 PMTs
--------------------------------------------------------------------------------
exp/2012Apr	#420001 - #420120	detmapG15e5MR42v1	Masked=[4, 141, 11, 23, 83], HV0=[177, 179, 219, 229]
exp/2012Apr	#420121 - #420123	detmapG15e5MR42v2	Masked=[4, 141, 11, 23, 83, 98, 30], HV0=[177, 179, 219, 229]
exp/2012Apr	#420124 - #420238	detmapG15e5MR42v3	Masked=[4, 141, 11, 23, 83, 98, 30, 87, 171, 111, 93], HV0=[177, 179, 219, 229]
								ノイジーなPMTを全てマスクして測定
exp/2012Apr	#420239 - #420277	detmapG15e5MR42v4	Masked=[4, 141, 11, 23, 83, 98, 30, 87, 105], HV0=[177, 179, 219, 229]
exp/2012Apr	#420278 - #421062	detmapG15e5MR42v6	Masked=[4, 141, 11, 23, 83], HV0=[177, 179, 219, 229]
                                                ノイズスタディの結果、全てをアンマスクすることに。マスクしてあるファイルはv1と同じ。ただ、PMT87とPMT11のPPをスワップしたので、ATMCHの番号の一部が異なる。
exp/2012Apr	#421063 - #421194	detmapG15e5MR42v7	Masked=[4, 141, 11, 23, 83], HV0=[177, 179, 219, 229]
		                                                LED較正の補正によりゲイン等の値が変わっている
exp/2012Apr	#421195 - #421305	detmapG15e5MR42v8       Masked=[4, 141, 11, 23, 83, HV0=[177, 179, 219, 229] (<-- same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)
		                                                LED較正の補正を元に戻した（v6と同じ）
exp/2012May	#430001 - #43xxxx	detmapG15e5MR43v1       Masked(4, 141, 11, 23, 83) HV0(177, 179, 219, 229) (<-- same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)
		                                                MR RUN43、FV水なし測定の開始、マップファイルとしては、MR42v6と同じ

++++++++++++++++++++++++++++++++++++++++++++++++++
        Increased noise rate study
++++++++++++++++++++++++++++++++++++++++++++++++++
***** ノイズレートが増加した際に、スタディしたときに使ったファイル *****
noise/120420    #01 - #03       detmapG15e5MR42v4       unmask 98, 30, 87, 105, 171, 111, 93, mask others.
noise/120420    #04 - #05       detmapG15e5MR42v5       unmask 98, 30, 87, 105, 171, 111, 93, mask others.
noise/120420    #06 - #18       detmapG15e5MR42v1       (no additional unmasked PMTs. See logbook(p.197-) for details)
noise/120420    #19 - #xx       detmapG15e5MR42v6       (Masked PMTs are same as v1. PP# for PMT87 and PMT11 are swapped. See logbook(p.200) for details.


++++++++++++++++++++++++++++++++++++++++++++++++++
        LED Calibrations
++++++++++++++++++++++++++++++++++++++++++++++++++
***** LED較正を行い、ゲインに補正をかけて電圧を印加 *****
led/120516    #08 - #20    detmapG15e5MR42v6    2.6V, 1Hz, -1000mV, OMなし（補正前）
                                                4, 141, 11, 23, 83, 177, 179, 219, 229 (<-- Masked PMTs are same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)
led/120516    #21 - #25    detmapG15e5MR42v6    2.5V, 1Hz, -1000mV, OMなし（補正前）
                                                4, 141, 11, 23, 83, 177, 179, 219, 229 (<-- Masked PMTs are same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)
led/120516    #26 - #30    detmapG15e5MR42v7    2.6V, 1Hz, -1000mV, OMなし（補正後）
                                                4, 141, 11, 23, 83, 177, 179, 219, 229 (<-- Masked PMTs are same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)
led/120516    #31 - #35    detmapG15e5MR42v7    2.5V, 1Hz, -1000mV, OMなし（補正後）
                                                4, 141, 11, 23, 83, 177, 179, 219, 229 (<-- Masked PMTs are same as v1, PMT87(PP#11-1) and PMT11(PP#03-4) are swapped)

led/120521    #01        detmapG15e5MR42v8    2.3V
led/120521    #02        detmapG15e5MR42v8    2.4V
led/120521    #03        detmapG15e5MR42v8    2.5V
led/120521    #04, 05    detmapG15e5MR42v8    2.6V

led/120525    #01, 02    detmapG15e5MR42v8    2.4V
led/120525    #03, 04    detmapG15e5MR42v8    2.5V
led/120525    #05, 06    detmapG15e5MR42v8    2.6V

