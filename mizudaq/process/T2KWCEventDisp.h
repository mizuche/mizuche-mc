
// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 22:26:33+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
#ifndef ROOT_T2KWCEVENTDISP
#define ROOT_T2KWCEVENTDISP
#ifndef ROOT_TObject
#include "TObject.h"
#endif

class T2KWCEventDisp : public TObject
{
private:
    TPad*           fPView;
    TH1D*           fHView;
    TBox*           fBarrel;
    TEllipse*       fEndCapUp;
    TEllipse*       fEndCapDown;
    TEllipse*       fPMT[npmts];

    TGraph *gpmtsum;
    TGraph *ghitsum;
  
    Int_t nphotons;
    Int_t npes;
    Double_t totalpe;
    Int_t nhitpmts;

public:
    T2KWCEventDisp();
    virtual ~T2KWCEventDisp();

    void Init( TCanvas* );
    void Clear();

    void InputData(T2KWCUnpack*);
    void InputData(UPK*);

    void SetHit(Int_t, Int_t);
    void SetHit(Int_t, Int_t, Double_t);
    
    void ConvertGeometry(Double_t,Double_t,Double_t,Double_t*);
    const char* GetModeName(Int_t);
    
    void Draw(TCanvas*);
    
    Int_t GetColor(Int_t);
    
    ClassDef(T2KWCEventDisp,0);    //DOCUMENT ME

};

#endif
//____________________________________________________________________ 
//  
// EOF
//


//____________________________________________________________________
//
// DOCUMENT ME
// 

//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 22:29:36+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
#ifndef ROOT_T2KWCEventDisp
#include "T2KWCEventDisp.h"
#endif

//____________________________________________________________________
ClassImp(T2KWCEventDisp);

//____________________________________________________________________
T2KWCEventDisp::T2KWCEventDisp()
{
    // Default constructor
}

//____________________________________________________________________
T2KWCEventDisp::~T2KWCEventDisp()
{
    // Default constructor
    if( fPView ) delete fPView;
    if( fHView ) delete fHView;
    if( fBarrel ) delete fBarrel;
    if( fEndCapUp ) delete fEndCapUp;
    if( fEndCapDown ) delete fEndCapDown;
    for(int i=0;i<npmts;i++) {
        if( fPMT[i] ) delete fPMT[i];
    }
}

//____________________________________________________________________
void T2KWCEvtDisp::Init(TCanvas* c)
{  
    char buff[300];
    Double_t circle = TMath::Pi() * 2 * r_tank; // [cm]
    //Int_t fon = 22;
    Int_t fon = 132;

    c->cd();
    //
    fPView = new TPad("fPView","Mizuche Event Display", 0.0, 0.0, 1.0, 1.0);
    //
    sprintf(buff,"View");
    fHView = new TH1D( "fHView", "Mizuche Event Display",
                       Int_t(10*(l_tank+r_tank*4+60)),
                       -1*(l_tank/2.+r_tank*2.+30),
                       (l_tank/2.+r_tank*2.+30) );
    fHView->SetBit( kCanDelete );
    fHView->SetMinimum( -1.*(circle/2.+30) );
    fHView->SetMaximum( (circle/2.+30) );

    fHView->SetLineColor(0);
    fHView->SetXTitle("Beam Direction (left->right) [cm]");
    fHView->SetYTitle("[cm]");
    fHView->SetTitleFont(fon,"");
    fHView->SetTitleFont(fon,"XY");
    fHView->SetTitleSize(0.045,"XY");
    fHView->GetXaxis()->CenterTitle();
    fHView->GetYaxis()->CenterTitle();
    fHView->SetLabelFont(fon,"XY");
    fHView->SetLabelSize(0.05,"XY");
    fHView->SetNdivisions(505, "XY");
    //fHView->SetNdivisions(0, "XY");
    fHView->SetStats(0);

    //
    TBox *fBarrel = new TBox( -1.*(l_tank/2.), -1.*(circle/2.), 
                              (l_tank/2.), (circle/2.));
    fBarrel->SetBit(kCanDelete);
    fBarrel->SetFillStyle(0);

    //
    TEllipse *fEndCapUp = new TEllipse( -1.*(l_tank/2.+r_tank), 0., r_tank);
    fEndCapUp->SetFillStyle(0);
    TEllipse *fEndCapDown = new TEllipse( 1.*(l_tank/2.+r_tank), 0., r_tank);
    fEndCapDown->SetFillStyle(0);

    //
    Double_t pmt[2];
    for(int i=0; i<npmts; i++) {

        ConvertGeometry(pmt_pot[i][0],pmt_pot[i][1],pmt_pot[i][2],pmt);

        fPMT[i] = new TEllipse( pmt[0], pmt[1], r_pmt);
        fPMT[i]->SetFillColor(0);
    }

    // FADC hist
    ghitsum = new TGraph(0);
    gpmtsum = new TGraph(0);

    // Show Display (Canavs setting)
    c->Divide(2,1);
    c->GetPad(2)->Divide(1,2);

    // Tank
    c->cd(1);

    fPView->Draw();
    fHView->Draw();
    fBarrel->Draw();
    fEndCapUp->Draw();
    fEndCapDown->Draw();

    for(int i=0;i<npmts;i++) fPMT[i]->Draw();

    c->Modified();
    for(int i=1;i<=2;i++) c->GetPad(i)->Modified();
    c->Update();
}


//____________________________________________________________________ 
//  
// EOF
//
