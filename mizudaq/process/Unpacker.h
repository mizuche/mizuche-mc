// -*- c++ -*-

#ifndef UNPACKER_H
#define UNPACKER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <math.h>
#include <time.h>

#include <TObject.h>
#include <TFile.h>
#include <TTree.h>

//######## Raw data class #########
#include "T2KWCRaw.h"
#include "TRawData.h"

//####### Output class ##########
#include "T2KWCUnpack.h"

// ATM info
const Int_t kNboards = 15;         // Number of ATM boards used
const Int_t kNchPerBoard = 12;  // Number of channels per ATM board
const Int_t kNbuffPerCh = 2;    // Number of buffers per channel
const Int_t kNatmCh = kNboards * kNchPerBoard;  // = 180
//const Int_t max_pmt_nb = kNatmCh;
const Int_t kNtype = 2;    // same as kNbuffPerCh, maybe

// FADC info
const Int_t kFadcOffset = 0x800; // [count], at fadc_dc_offset=0x8000
const Int_t kPedTDCOffset = 550; // ns, set by software
const Int_t kHeaderSize = 4;
const Int_t kFadcNchMax = 8;

// ATM HITSUM specifications
const Double_t kHitsumWidth  = 200.0;  // [mV]
const Double_t kHitsumHeight = 15.0;   // [mV]

static mizu_raw_data gMizuRawDataDummy;

// Flags for unpack mode
const Int_t kLIGHTMODE = 0;  // default
const Int_t kFULLMODE = 1;
const Int_t kMONIMODE = 2;

// Flags for LogLevel
const Int_t kNOTSET = 10;
const Int_t kDEBUG = 10;
const Int_t kINFO = 20;
const Int_t kWARNING = 30;
const Int_t kERROR = 40;
const Int_t kCRITICAL = 40;

// __________ The Debug Flags __________
// Firstly, uncomment these flags for debug, then try your own debugging
// brief [1 - 3] precise
//#define DEBUG1
//#define DEBUG2
//#define DEBUG3    

// #define DEBUG_UATM
// #define DEBUG_CATM
// #define DEBUG_UTRG
// #define DEBUG_NTRG
//#define DEBUG_FADCHEADER
// #define DEBUG_IFADC
//#define DEBUG_UFADC
//#define DEBUG_CFADC
// #define DEBUG_PEDESTAL
// #define DEBUG_DETMAP
// #define DEBUG_ONMON

//#define ENABLE_NTRG

// __________ Uncomment this if you unpack data before MR40 (includes MR40) __________
// Changes in ATM and GONG timing were made btw MR40 and MR41
//#define BeforeMR40

// __________________________________________________
class Unpacker {
private:
    TFile *fFin;
    TTree *fRaw;

    TFile *fOut;
    TTree *fUpk;
    TTree *fMap;
    TTree *fInfo;
    TTree *fUmode;
    TTree *fHistory;
    TTree *fWave;

    // Raw data structure
    mizu_raw_data fMizuRawData;

    Int_t fRawRun;
    Int_t fRawMode;
    UShort_t fRawSpill;
    //Int_t    fRawSpill;

    Int_t fRawTimeSec;
    Int_t fRawTimeUsec;

    T2KWCUnpack *fUnpack;

    Int_t fPedEvents;
    Double_t fPedTDC[kNatmCh][kNtype];
    Double_t fPedQDC[kNatmCh][kNtype];
    Double_t fPedTDCLast[kNatmCh][kNtype];
    Double_t fPedQDCLast[kNatmCh][kNtype];
    
    Int_t fAtmchPmt[kNatmCh];
    Int_t fAtmchWin[kNatmCh];
    Int_t fAtmchMask[kNatmCh];
    Int_t fAtmchHv[kNatmCh];
    Double_t fAtmchGain[kNatmCh];

    Double_t fFadcHitThrd;
    Int_t fFadcNped;
    Int_t fFadcNch;

    Bool_t IsTrgGood;
    Bool_t IsAtmGood;
    Bool_t IsFadcGood;
    Bool_t IsPedUpdated;
    Bool_t IsHistInitialized;
    Bool_t IsOverwritable;
    Bool_t IsFileOpened;
    Bool_t IsDetmapRead;
    Bool_t IsHistRenamed;

    Int_t fUnpackMode;
    
    std::vector<Int_t> fAtmNtrgnum;
    std::vector<Int_t> fFadcRaw[kFadcNchMax];    // for FADC CH[0-7]
    std::vector<Double_t> fFadcMv[kFadcNchMax];    // for FADC CH[0-7]
    std::vector<Int_t> fRawPmtsum;
    std::vector<Int_t> fRawHitsum;
    std::vector<Double_t> fFadcPmtsum;
    std::vector<Double_t> fFadcHitsum;
    std::vector<Int_t> fFadcNsec;
    std::vector<Int_t> fFadcHit;
    std::vector<Double_t> fFadcHeight;
    std::vector<Double_t> fFadcCharge;
    std::vector<Double_t> fFadcPe;

    // histograms for check (partially (every ~ 1 day) for logging)
    TH1D *hQdcPedPart[kNatmCh][kNtype];
    TH1D *hTdcPedPart[kNatmCh][kNtype];
    TH1D *hQdcPedRmsPart[kNatmCh][kNtype];
    TH1D *hTdcPedRmsPart[kNatmCh][kNtype];
    // -----
    TH1D *hAtmPePart[kNatmCh][kNtype];
    TH1D *hAtmChargePart[kNatmCh][kNtype];
    TH1D *hAtmTimePart[kNatmCh][kNtype];
    // -----
    TH1D *hAtmNhitsPart;
    TH1D *hAtmNchargesPart;
    TH1D *hAtmNpesPart;
    // -----
    TH1D *hFadcNhitsPart;
    TH1D *hFadcNchargesPart;
    TH1D *hFadcNpesPart;
    TH1D *hFadcNchargesBunchPart;
    TH1D *hFadcNpesBunchPart;
    // -----
    TH1D *hAtmMaxChargePart;
    TH1D *hAtmMaxPePart;
    
    // --------------------
    // Histograms for saving
    TH1D *hQdcPed[kNatmCh][kNtype];
    TH1D *hTdcPed[kNatmCh][kNtype];
    TH1D *hQdcPedRms[kNatmCh][kNtype];
    TH1D *hTdcPedRms[kNatmCh][kNtype];
    // -----
    TH1D *hAtmPe[kNatmCh][kNtype];
    TH1D *hAtmCharge[kNatmCh][kNtype];
    TH1D *hAtmTime[kNatmCh][kNtype];
    // -----
    TH1D *hAtmNhits;
    TH1D *hAtmNcharges;
    TH1D *hAtmNpes;
    // -----
    TH1D *hFadcNhits;
    TH1D *hFadcNcharges;
    TH1D *hFadcNpes;
    TH1D *hFadcNchargesBunch;
    TH1D *hFadcNpesBunch;
    // -----
    TH1D *hAtmMaxCharge;
    TH1D *hAtmMaxPe;
    
    // Temp variables
    Int_t fTmpRawTimeSec;
    Int_t fTmpRawTimeUsec;
    Int_t fRunNum;
    Int_t fSpillNum;
    Int_t fTime;
    // for ATM
    Double_t fAtmNhits;
    Double_t fAtmNhitsRms;
    Double_t fAtmNcharges;
    Double_t fAtmNchargesRms;
    Double_t fAtmNpes;
    Double_t fAtmNpesRms;
    // for FADC
    Double_t fFadcNhits;
    Double_t fFadcNhitsRms;
    Double_t fFadcNcharges;
    Double_t fFadcNchargesRms;
    Double_t fFadcNpes;
    Double_t fFadcNpesRms;
    // Maximum point
    Double_t fAtmMaxCharge;
    Double_t fAtmMaxChargeRms;
    Double_t fAtmMaxPe;
    Double_t fAtmMaxPeRms;
    // for pedestal calculation
    Double_t fQdc0Mean[kNatmCh][kNtype];
    Double_t fQdc0MeanRms[kNatmCh][kNtype];
    Double_t fTdc0Mean[kNatmCh][kNtype];
    Double_t fTdc0MeanRms[kNatmCh][kNtype];
    Double_t fQdc0Rms[kNatmCh][kNtype];
    Double_t fQdc0RmsRms[kNatmCh][kNtype];
    Double_t fTdc0Rms[kNatmCh][kNtype];
    Double_t fTdc0RmsRms[kNatmCh][kNtype];
    //
    Double_t fAtmPe[kNatmCh][kNtype];
    Double_t fAtmPeRms[kNatmCh][kNtype];
    Double_t fAtmCharge[kNatmCh][kNtype];
    Double_t fAtmChargeRms[kNatmCh][kNtype];
    Double_t fAtmTime[kNatmCh][kNtype];
    Double_t fAtmTimeRms[kNatmCh][kNtype];

    Double_t fAverageGain;
    
    TBranch *fBrSec;
    TBranch *fBrUsec;
    std::vector<Int_t> fQdcPedTmp[kNatmCh][kNtype];
    std::vector<Int_t> fTdcPedTmp[kNatmCh][kNtype];

    Int_t fStartTime;

    TString fLog, fLogTmp;    // output message
    Int_t fVerboseLevel;
    Int_t fDebugLevel;
    Int_t fErrorLevel;
    Int_t fLogLevel;

public:
    Unpacker();
    Unpacker(const Int_t &mode, const Double_t &hitthr, const Int_t &nped);
    Unpacker(const char *detmap);
    Unpacker(const char *ifn, const char *ofn, const char* detmap, const Int_t fvstat, const Int_t daqmode); // obsolete
    ~Unpacker();

    void Clear();
    void Write();

    T2KWCUnpack *GetUpk() const { return fUnpack; }    // used in online monitor
    char* ConvertUnixTime(const Int_t &utime);

    void ReadRawTree(const char* ifn);
    void OpenUpkFile(const char* ofn, Bool_t kOverwrite);
    void Unpack(const T2KWCRaw *raw); // used in online monitor
    void Unpack(const char* ifn, const char* detmap);    // used in unpack.cc
    void UnpackTree();
    void UnpackEntry();
    void UnpackTrg();
    void UnpackAtm(const Int_t &mode);
    void UnpackAtmOld(const Int_t &mode);    // Before MR40
    void UnpackFadc(const Int_t &nch);
    void UnpackFadc2();
    //void UnpackFadc(const Int_t &ich, const Int_t &nsamp);    // not used

    void SetUnpackMode(const Int_t &mode) { fUnpackMode = mode; }
    void SetFadcHitThrd(const Double_t &thrd) { fFadcHitThrd = thrd; }
    void SetFadcNped(const Int_t &np) { fFadcNped = np; }
    void SetFvstat(const Int_t &stat) { fUnpack->SetFvstat(stat); }
    void SetDaqmode(const Int_t &mode) { fUnpack->SetDaqmode(mode); }
    void SetUpkRun(const Int_t &run) { fUnpack->SetUpkRun(run); }

    Int_t    GetUnpackMode() const { return fUnpackMode; }
    Int_t    GetFadcNped() const { return fFadcNped; }
    Double_t GetFadcHitThrd() const { return fFadcHitThrd; }
    
    void     ConvertATM();
    void     InitAtmPedestal();
    void     StorePedestal();
    void     GetAtmPedestal();
    Double_t GetAtmCharge(const Int_t &atmch, const Double_t &adc);
    Double_t GetAtmPe(const Int_t &atmch, const Double_t &adc);
    Double_t GetAtmNsec(const Int_t &atmch, const Double_t &adc);

    void     ConvertFadc();
    void     AnalyzeFadc();
    void     ConvertFadcBunch();
    void     FillFadc();
    void     FillUpk();
    void     ConvertFADC_shota();
    void     InitFadc();
    // Int_t    GetFadcNsec(Int_t &isamp) const;   // compile error
    Int_t    GetFadcNsec(Int_t &isamp);
    Double_t CalcFadcMv(Double_t &adc);
    //Double_t GetFadcCharge(Double_t &adcsum);    // not used
    Double_t CalcFadcPe(Double_t &charge);
    Double_t CalcAverageGain();
    Double_t GetAverageGain() const { return fAverageGain; };

    Int_t    SetFadcPedestal();
    Int_t    CalcFadcPedestal();
    Double_t GetHitsumAverage(const Int_t &start, const Int_t &end);
    Double_t GetPmtsumIntegral(const Int_t &start, const Int_t &end);
    Int_t    CheckFadcHeaders();
    Int_t    GetAtmchWindow(const Int_t &atmch);
    Int_t    GetAtmchPmt(const Int_t &atmch);
    Double_t GetAtmchGain(const Int_t &atmch);
    Int_t    GetAtmchMask(const Int_t &atmch);
    Int_t    GetAtmchHv(const Int_t &atmch);
    Bool_t   IsAtmchBad(const Int_t &atmch);
    
    void ReadDetectorMap(const char*);
    //void PrintDebug(const char*);  // not used; use PrintDeubug(Int, char*) instead
    //void PrintError(const char*);  // not used; use PrintError(Int, char*) instead
    void PrintMap();
    void PrintHitInfo(const Int_t &ihit, const Int_t &nhits);
    void PrintHitSummary();
    Int_t PathExists(const char* ifn);
    Int_t PathExists(const char* ifn, const char* mode);
    Int_t PrintProgressBar(const Int_t &i, const Int_t &N, const Int_t &L);

    TH1D *InitHist(const char* name, const char* title,
                   const Double_t &xmin, const Double_t &xmax,
                   const Int_t &color, const Int_t &binwidth);
    void InitHists();
    void RenameHists();
    void AddHists();
    void WriteHists();
    void CheckHists(const char* ifn);
    Int_t Overwrite(const char* ofn);
    Int_t Stop();
    
    void SetVerboseLevel(const Int_t &level) { fVerboseLevel = level; }
    inline void PrintVerbose(const Int_t &level, const char* log);
    inline void PrintVerbose(const Int_t &level);
    
    inline void SetDebugLevel(const Int_t &level) { fDebugLevel = level; }
    inline void PrintDebug(const Int_t &level, const char* log);
    inline void PrintDebug(const Int_t &level);

    inline void SetErrorLevel(const Int_t &level) { fErrorLevel = level; }
    inline void PrintError(const Int_t &level, const char* log);
    inline void PrintError(const Int_t &level);

    void SetFadcNch(const Int_t &nch) { fFadcNch = nch; }
    Int_t GetFadcNch() { return fFadcNch; }

    // New implementation for logging
    void SetLogLevel(const Int_t &level) { fLogLevel = level; }
    void LogFormat(const char* log);
    
    inline void LogInfo(const char* log);
    inline void LogInfo();

    inline void LogDebug(const char* log);
    inline void LogDebug();

    inline void LogWarning(const char* log);
    inline void LogWarning();

    inline void LogError(const char* log);
    inline void LogError();

    inline void LogCritical(const char* log);
    inline void LogCritical();
    
        
};


// __________________________________________________
inline void Unpacker::LogInfo(const char* log) {
    if (fLogLevel <= kINFO) {
        fprintf(stdout, "INFO     %s\n", log);
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::LogInfo() {
    LogInfo(fLog.Data());
}

// __________________________________________________
inline void Unpacker::LogDebug(const char* log) {
    if (fLogLevel <= kDEBUG) {
        fprintf(stdout, "DEBUG    %s\n", log);
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::LogDebug() {
    LogDebug(fLog.Data());
}

// __________________________________________________
inline void Unpacker::LogWarning(const char* log) {
    if (fLogLevel <= kWARNING) {
        fprintf(stdout, "WARNING  %s\n", log);
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::LogWarning() {
    LogWarning(fLog.Data());
}

// __________________________________________________
inline void Unpacker::LogError(const char* log) {
    if (fLogLevel <= kERROR) {
        fprintf(stdout, "ERROR    %s\n", log);
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::LogError() {
    LogError(fLog.Data());
}

// __________________________________________________
inline void Unpacker::LogCritical(const char* log) {
    if (fLogLevel <= kCRITICAL) {
        fprintf(stdout, "CRITICAL %s\n", log);
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::LogCritical() {
    LogCritical(fLog.Data());
}

// __________________________________________________
inline void Unpacker::PrintVerbose(const Int_t &level, const char* log) {
    if (level < fVerboseLevel) {
        fprintf(stdout, "VERBO[%d<%d] | %s", level, fVerboseLevel, log);
        fprintf(stdout, Form("This function ('%s') will be deprecated. Use LogInfo instead.", __FUNCTION__));
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::PrintVerbose(const Int_t &level) {
    PrintVerbose(level, fLog.Data());
}

// __________________________________________________
inline void Unpacker::PrintDebug(const Int_t &level, const char* log) {
    TString space = "";
    for (Int_t ilevel = 0; ilevel < level; ilevel++) {
        space += "  ";
    }
        
    if (level < fDebugLevel) {
        fprintf(stdout, "DEBUG[%d<%d] | %s%s", level, fDebugLevel, space.Data(), log);
        fprintf(stdout, Form("This function ('%s') will be deprecated. Use LogDebug instead.", __FUNCTION__));
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::PrintDebug(const Int_t &level) {
    PrintDebug(level, fLog.Data());
}

// __________________________________________________
inline void Unpacker::PrintError(const Int_t &level, const char* log) {
    if (level < fErrorLevel) {
        fprintf(stdout, "ERROR[%d<%d] | %s",level, fErrorLevel, log);
        fprintf(stdout, Form("This function ('%s') will be deprecated. Use LogError instead.", __FUNCTION__));
        fflush(stdout);
    }
}

// __________________________________________________
inline void Unpacker::PrintError(const Int_t &level) {
    PrintError(level, fLog.Data());
}

// __________________________________________________
Int_t Unpacker::Stop()
{
    fprintf(stderr, "Press 'q' to quit > ");
    Int_t answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    if (answer == 'q' || answer == 'Q') {
        fprintf(stderr, "\tQuit. Bye Bye !!!\n");
        Write();
        exit(-1);
    }
    return 0;
}

// __________________________________________________
Int_t Unpacker::PrintProgressBar(const Int_t &i, const Int_t &N, const Int_t &L)
{
    if (i == 0) fprintf(stderr, "Progress:\t");
    if (i % L == 0) {
        fprintf(stderr, "*");
    }
    if (i == N-1) fprintf(stderr, "\n\n");

    if (fVerboseLevel > 2) {
        printf("**************************************************\n");
        printf("%4d th event processed\n", i);
        printf("**************************************************\n");
        if (i % L == 0) {
            if (i % 2 == 0) {
                printf("--------------------------------------------------\n");
            }
            else {
                printf("++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            }
        }
    }
    return 0;
}

// __________________________________________________
Int_t Unpacker::PathExists(const char* ifn, const char* mode)
{
#ifdef DEBUG3
    fprintf(stdout, "DEBUG3:\t[%s]\n", __PRETTY_FUNCTION__);
#endif
    TString m = mode;
    m.ToLower();

    FileStat_t info;
    Int_t r = gSystem->GetPathInfo(ifn, info);
    if (m == "r" && r !=0) fprintf(stderr, "Error:\t[%s]\tFile '%s' does not exist.\n", __FUNCTION__, ifn);
    if (m == "w" && r ==0) fprintf(stderr, "Error:\t[%s]\tFile '%s' already exist.\n", __FUNCTION__, ifn);
    return r;
}

// __________________________________________________
char* Unpacker::ConvertUnixTime(const Int_t &utime)
{
    time_t t = utime;
    struct tm* m;
    m = localtime(&t);
    char s[500];
    strftime(s, sizeof(s), "%Y/%m/%d %H:%M:%S", m);
    char *r = s;
    return r;
}

// __________________________________________________
Double_t Unpacker::GetAtmNsec(const Int_t &atmch, const Double_t &adc )
{
#ifdef DEBUG3
    fprintf(stdout, "DEBUG3:\t[%s]\n", __PRETTY_FUNCTION__);
#endif
    Double_t adc2T = 0.25;  // [nsec/count]
    Double_t calib = 0.623;  // recon-T/true-T (typical value)
    Double_t time = (adc * adc2T / calib) + kPedTDCOffset; // [nsec]
    return time;
}

// __________________________________________________
Double_t Unpacker::GetAtmCharge(const Int_t &atmch, const Double_t &adc )
{
#ifdef DEBUG3
    fprintf(stdout, "DEBUG3:\t[%s]\n", __PRETTY_FUNCTION__);
#endif
    Double_t adc2Q = 0.1; // [pC/count]
    Double_t calib = 0.623;  // recon-Q/true-Q (typical value)
    Double_t charge = (adc * adc2Q / calib); // [pC]
    return charge;
}

// __________________________________________________
Double_t Unpacker::GetAtmPe(const Int_t &atmch, const Double_t &adc )
{
#ifdef DEBUG3
    fprintf(stdout, "DEBUG3:\t[%s]\n", __PRETTY_FUNCTION__);
#endif

    Double_t charge = GetAtmCharge(atmch, adc); //[pC]
    Double_t e      = 1.6e-7;  // [pC/electron]
    Double_t gain   = GetAtmchGain(atmch);
    Int_t    mask   = GetAtmchMask(atmch);

    Double_t pe     = 0;
    if (gain == 0)      pe = 0;
    else if (mask != 0) pe = 0;
    else pe = charge / e / gain; // [p.e.]

    return pe;
}

// __________________________________________________
Int_t Unpacker::GetFadcNsec(Int_t &isamp)
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    return isamp * 4;    // [samp] --> [nsec]
}

// __________________________________________________
Double_t Unpacker::CalcFadcMv(Double_t &adc)
{
#ifdef DEBUG3
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug(3);
#endif
    return adc * 0.489; // [count] --> [mV]
}

// __________________________________________________
// Double_t Unpacker::GetFadcCharge(Double_t &adcsum)
// {
// #ifdef DEBUG1
//     fprintf(stdout, "DEBUG1:\t[%s]\n", __PRETTY_FUNCTION__);
// #endif
//     return adcsum * 0.489 * 4. / 50.;    // [count*samp] --> [pC]
// }

// __________________________________________________
Double_t Unpacker::CalcFadcPe(Double_t &charge)
{
    fLog.Form("[%s]", __FUNCTION__);
    LogDebug();
    const Double_t e = 1.6e-7;    // [pC/e]
    Double_t gain = GetAverageGain();
    Double_t pe   = charge / gain / e;    // [pC] --> [p.e.]
    return pe;
}

// __________________________________________________
Double_t Unpacker::CalcAverageGain()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Double_t average_gain = 0;
    Int_t n = 0;
    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        Int_t mask = GetAtmchMask(iatm);
        if (mask == 0) {
            Double_t gain = GetAtmchGain(iatm);
            if (gain != 0) {
                average_gain += gain;
                n++;
            }
        }
    }
    fLog.Form("[%s] average_gain: %e / %d = %e",
              __FUNCTION__, average_gain, n, average_gain / n);
    LogDebug();
    average_gain /= n;
    return average_gain;
}

// __________________________________________________
Int_t Unpacker::CheckFadcHeaders()
{
    // Check if FADC header is correct
    //    1. Check magic number is 10 (=0b1010)
    //    2. Calculate number of FADCch used and set
    //    3. Return event size
    // --------------------------------------------------
    // FADC header structure is shown in below
    // --------------------------------------------------
    // HEADER[kHeaderSize=4][width=32]
    // HEADER[0][31 - 28] : Magic number = 1010
    // HEADER[0][27 -  0] : Event size
    // HEADER[1][31 - 27] : Board ID
    // HEADER[1][26 - 25] : RES (?)
    // HEADER[1][24]      : Zero length encoding: 0:disabled (default), 1:enabled
    // HEADER[1][23 -  8] : Pattern
    // HEADER[1][ 7 -  0] : Channel mask
    // HEADER[2][31 - 24] : reserved
    // HEADER[2][23 -  0] : Event counter
    // HEADER[3][31 -  0] : Trigger time tag
    // --------------------------------------------------
    // Remarks:
    //     1. If magic number is not 10, there might be something wrong with the FADC board.
    //     2. Assume that channels are used in sequenc, i.e. for 4ch use [0, 1, 2, 3], not [0, 1, 3, 5].
    //        Otherwise, number of FADCch used is not caluclated properly.
    //     3. Event size is not equal to number of sampling points.
    //        Also, event size include FADC header size (=kHeaderSize)
    
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    if( fMizuRawData.fadc.nFADC <= 4 ) {
        IsFadcGood = kFALSE;
        fLog.Form("[%s] ==> No sampling data --> Skip this event", __FUNCTION__);
        LogError();
        return 0;
    }

    Int_t head0 = fMizuRawData.fadc.FADC[0];
    Int_t head1 = fMizuRawData.fadc.FADC[1];
    
    Int_t magic_num = (head0 >> 28) & 0xF;
    if( magic_num!=10 ) {
        fLog.Form("[%s] ==> Magic Num: %d --> Invalid magic number of FADC (correctt:10). Exits.\n",
                  __FUNCTION__, magic_num);
        LogError();
        exit(1);
    }

    Int_t event_size = head0 & 0x0FFFFFFF;
    Int_t nchmask = head1 & 0x000000FF;
    
    // Calculate number of channels used --> SetFadcNch
    Int_t nch = 0;
    for (Int_t ich = 0; ich < kFadcNchMax; ich++) {
        Int_t bit = (nchmask >> ich) & 0x1;
        nch += bit;
    }

    fLog.Form("[%s] SetFadcNch : %d", __FUNCTION__, nch);
    LogDebug();
    SetFadcNch(nch);
    
#ifdef DEBUG_FADCHEADER
    Int_t head2 = fMizuRawData.fadc.FADC[2];
    Int_t head3 = fMizuRawData.fadc.FADC[3];
    Int_t board_id = head1 >> 27;
    Int_t pattern = (head1 >> 8) & 0x0000FFFF;
    Int_t event_counter = head2 & 0x0FFFFFFF;
    Int_t trigger_time = head3;

    fLog.Form("DEBUG_FADCHEADER:[%s]:HEADER[0]", __FUNCTION__);
    fLogTmp.Form("\tMagic number: 0x%08x = %d", magic_num, magic_num);
    fLog += fLogTmp;
    fLogTmp.Form(" | Event size: 0x%08x = %d\n", event_size, event_size);
    fLog += fLogTmp;
    LogDebug();

    fLog.Form("DEBUG_FADCHEADER:[%s]:HEADER[1]", __FUNCTION__);
    fLogTmp.Form("\tBoard ID: 0x%08x = %d", board_id, board_id);
    fLog += fLogTmp;
    fLogTmp.Form(" | Pattern: 0x%08x = %d", pattern, pattern);
    fLog += fLogTmp;
    fLogTmp.Form(" | Ch Mask: 0x%08x = %d (%d)\n", nchmask, nchmask, nch);
    fLog += fLogTmp;
    LogDebug();
    
    fLog.Form("DEBUG_FADCHEADER:[%s]:HEADER[2]", __FUNCTION__);
    fLogTmp.Form("\tCount: 0x%08x = %d\n", event_counter, event_counter);
    fLog += fLogTmp;
    LogDebug();

    fLog.Form("DEBUG_FADCHEADER:[%s]:HEADER[3]", __FUNCTION__);
    fLogTmp.Form("\tTime: 0x%08x = %d\n", trigger_time, trigger_time);
    fLog += fLogTmp;
    LogDebug();
#endif
    return event_size;
}


// __________________________________________________
Int_t Unpacker::SetFadcPedestal()
{
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(0);

    const Int_t nped_pts = GetFadcNped();
    Double_t raw_pmtsum0 = 0, raw_hitsum0 = 0;
    //Int_t c = 0;
    for (Int_t isamp = 0; isamp < nped_pts; isamp++) {
        raw_pmtsum0 += fRawPmtsum.at(isamp);
        raw_hitsum0 += fRawHitsum.at(isamp);
        //c++;
    }
    raw_pmtsum0 /= nped_pts;
    raw_hitsum0 /= nped_pts;
    // raw_pmtsum0 /= c;
    // raw_hitsum0 /= c;

    Double_t fadc_pmtsum0 = CalcFadcMv(raw_pmtsum0);
    Double_t fadc_hitsum0 = CalcFadcMv(raw_hitsum0);

#ifdef DEBUG_CFADC
    fLog.Form("DEBUG_CFADC:[%s]\tPmtsum0: %8.2f (%8.2f mV)\n", __FUNCTION__, raw_pmtsum0, fadc_pmtsum0);
    PrintDebug(0);
    fLog.Form("DEBUG_CFADC:[%s]\tHitsum0: %8.2f (%8.2f mV)\n", __FUNCTION__, raw_hitsum0, fadc_hitsum0);
    PrintDebug(0);
#endif
    fUnpack->SetRawPmtsum0(raw_pmtsum0);
    fUnpack->SetRawHitsum0(raw_hitsum0);
    fUnpack->SetFadcPmtsum0(fadc_pmtsum0);
    fUnpack->SetFadcHitsum0(fadc_hitsum0);    
    
    return 0;
}

// __________________________________________________
Int_t Unpacker::CalcFadcPedestal()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    const Int_t nsamp = GetFadcNped();
    const Int_t nch = GetFadcNch();
    Double_t ped_raw[kFadcNchMax] = {};
    Double_t ped_mv[kFadcNchMax] = {};

    for (Int_t ich = 0; ich < nch; ich++) {
        ped_raw[ich] = 0;
        ped_mv[ich] = 0;
        for (Int_t isamp = 0; isamp < nsamp; isamp++) {
            ped_raw[ich] += fFadcRaw[ich].at(isamp);
        }
        ped_raw[ich] /= nsamp;
        ped_mv[ich] = CalcFadcMv(ped_raw[ich]);
    }
    
#ifdef DEBUG_CFADC
    for (Int_t ich = 0; ich < nch; ich++) {
        fLog.Form("[%s] DEBUG_CFADC\tfFadcRaw[%d/%d]: %8.2f (%8.2f mV)",
                  __FUNCTION__, ich, nch, ped_raw[ich], ped_mv[ich]);
        LogDebug();
    }
#endif
    
    fUnpack->SetRawPmtsum0(ped_raw[0]);
    fUnpack->SetRawHitsum0(ped_raw[1]);
    fUnpack->SetFadcPmtsum0(ped_mv[0]);
    fUnpack->SetFadcHitsum0(ped_mv[1]);    
    
    return 0;
}

// __________________________________________________
Double_t Unpacker::GetHitsumAverage(const Int_t &start, const Int_t &end)
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Double_t iheight = 0;
    Int_t intv = 0;
    for (Int_t isamp = start; isamp < end; isamp++) {
        Double_t ihitsum = fFadcHitsum.at(isamp);
        iheight += ihitsum;
        intv++;
#ifdef DEBUG_CFADC
        fLog.Form("[%s] DEBUG_CFADC\tiheight:[%4d]\t%10.3f  [%10.3f] mV",
                  __FUNCTION__, isamp, ihitsum, iheight);
        LogDebug();
#endif
    }
    iheight = - iheight;
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC\tAverage height:%10.3f / %d = %10.3f mV",
              __FUNCTION__, iheight, intv, iheight / intv);
    LogDebug();
#endif    
    iheight = iheight/intv;   // mV

    return iheight;
}

// __________________________________________________
Double_t Unpacker::GetPmtsumIntegral(const Int_t &start, const Int_t &end)
{
    fLog.Form("[%s]", __FUNCTION__);
    LogDebug();
    
    Double_t icharge = 0;
    for (Int_t isamp = start; isamp < end; isamp++) {
        Double_t ipmtsum = fFadcPmtsum.at(isamp);
        icharge += ipmtsum; //Fill all bunch value
        
#ifdef DEBUG_CFADC
        fLog.Form("[%s] DEBUG_CFADC\ticharge:[%4d]\t%10.3f  [%10.3f] mV*dsamp(=4ns)",
                  __FUNCTION__, isamp, ipmtsum, icharge);
        LogDebug();
#endif
    }
    icharge *= 4. / 50.;  // [mV*samp] --> [pC]
    icharge = -icharge;
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADCIntegrated charge:\t%10.3f pC", __FUNCTION__, icharge);
    LogDebug();
#endif    
    return icharge;
}

// __________________________________________________
Int_t Unpacker::GetAtmchWindow(const Int_t &atmch)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);    
#endif
    if (atmch >= kNatmCh) {
        return -1;
    }
    return fAtmchWin[atmch];
}

// __________________________________________________
Int_t Unpacker::GetAtmchPmt(const Int_t &atmch)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);    
#endif
    if (atmch >= kNatmCh) {
        return -1;
    }
    return fAtmchPmt[atmch];
}

// __________________________________________________
Double_t Unpacker::GetAtmchGain(const Int_t& atmch)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);        
#endif
    if (atmch >= kNatmCh) {
        return -1;
    }
    return fAtmchGain[atmch];
}

// __________________________________________________
Int_t Unpacker::GetAtmchMask(const Int_t &atmch)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);            
#endif
    if (atmch >= kNatmCh) {
        return -1;
    }
    return fAtmchMask[atmch];
}

// __________________________________________________
Int_t Unpacker::GetAtmchHv(const Int_t &atmch)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);            
#endif
    if (atmch >= kNatmCh) {
        return -1;
    }
    return fAtmchHv[atmch];
}

// __________________________________________________
Bool_t Unpacker::IsAtmchBad(const Int_t &atmch)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);                
#endif
    if (atmch == 125 || atmch == 77 ) return kTRUE;
    else return kFALSE;
}

// // __________________________________________________
// void Unpacker::PrintDebug(const char* funcname)
// {
//     printf("DEBUG3:\tPrintDEBUG***** Unpacker::%s ***** \n", funcname);
// }

// __________________________________________________
// void Unpacker::PrintError(const char* funcname)
// {
//     printf("Error:\t[Unpacker::%s]\n", funcname);
// }
    
// __________________________________________________
Unpacker::Unpacker()
    : fFin(0), fRaw(0), fOut(0), fUpk(0), fMap(0), fUmode(0), fWave(0)
{
    SetLogLevel(0);
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    //SetUnpackMode(1);
    fLog.Form("[%s] SetBranch for 'upk'", __FUNCTION__);
    LogDebug();
    
    fUnpack  = new T2KWCUnpack();
    fUpk = new TTree("upk","Unpacked Raw Data");
    fUpk->Branch("upk.", "T2KWCUnpack", &fUnpack, 64000, 99);
    IsHistInitialized = kFALSE;
    IsHistRenamed     = kFALSE;
    IsDetmapRead      = kFALSE;

    fLog.Form("[%s] Initialize histograms", __FUNCTION__);
    LogDebug();
    
    InitHists();
    Clear();
}

// __________________________________________________
Unpacker::Unpacker(const Int_t &mode, const Double_t &hitthr, const Int_t &nped)
    : fFin(0), fRaw(0), fOut(0), fUpk(0), fMap(0), fInfo(0), fUmode(0)
{
    Unpacker();
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    fLog.Form("[%s] SetUnpackMode  : %d", __FUNCTION__, mode);
    LogDebug();
    SetUnpackMode(mode);

    fLog.Form("[%s] SetFadcHitThrd : %.2f mV", __FUNCTION__, hitthr);
    LogDebug();
    SetFadcHitThrd(hitthr);

    fLog.Form("[%s] SetFadcNped    : %d pts", __FUNCTION__, nped);
    LogDebug();
    SetFadcNped(nped);
    
    fLog.Form("Create TTree('umode')");
    LogDebug();
    fUmode = new TTree("umode", "Unpacked Mode Information");
    fUmode->Branch("umode", &fUnpackMode, "umode/I");
    fUmode->Branch("hitthr", &fFadcHitThrd, "hitthr/D");
    fUmode->Branch("nped", &fFadcNped, "nped/I");
    fUmode->Fill();

    fLog.Form("Initialize");
    LogDebug();
    
    IsHistInitialized = kFALSE;
    IsHistRenamed = kFALSE;
    IsDetmapRead = kFALSE;
    fStartTime = 0;
}


// __________________________________________________
Unpacker::~Unpacker()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    
    if (fRaw) delete fRaw;
    if (fFin) fFin->Close();
    if (fOut) Write();
}

// __________________________________________________
void Unpacker::Clear()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    fUnpack->Clear("C");
    fMizuRawData = gMizuRawDataDummy;
    IsTrgGood = kFALSE;
    IsAtmGood = kFALSE;
    IsFadcGood = kFALSE;
}

// __________________________________________________
void Unpacker::Write()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    fOut->cd();

    if (IsHistInitialized && fUnpackMode == kLIGHTMODE )  WriteHists();
    if (fUmode) fUmode->Write();
    //if (fInfo) fInfo->Write();    // Crashes at this point since fFin is already closed.
    if (fMap) fMap->Write();
    if (fUpk) fUpk->Write();
    if (fHistory) fHistory->Write();
    if (fUnpackMode == kFULLMODE && fWave) fWave->Write();
    fOut->Close();
    fOut = 0;
}

// __________________________________________________
void Unpacker::ReadRawTree(const char* ifn)
{
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(0);        
    
    IsFileOpened = kFALSE;
    
    if (PathExists(ifn, "r") != 0) return;

    fLog.Form("[%s]\tRead 'info' and 'r_mizu' from '%s'.\n", __FUNCTION__, ifn);
    PrintVerbose(0);

    fFin = new TFile(ifn);
    if (!fFin->IsOpen()) {
        fprintf(stderr, "Error:\t[%s]\tFile '%s' cannot be opned.\n", __FUNCTION__, ifn);
        return;
    }
    
    TTree *temp = (TTree*)fFin->Get("info");
    if (!temp) {
        fprintf(stderr, "Error:\t[%s]\tNo input tree named 'info'.\n", __FUNCTION__);
        return;
    }
    fInfo = temp->CloneTree();
    
    fRaw = (TTree*)fFin->Get("r_mizu");
    if(!fRaw) {
        fprintf(stderr, "Error:\t[%s]\tNo input tree named 'r_mizu'.\n", __FUNCTION__);
        return;
    }

    IsFileOpened = kTRUE;    // everything succeeded !!!

    fLog.Form("SetBranchAddress to 'r_mizu'.\n");
    PrintDebug(1);
    fRaw->SetBranchAddress("run", &fRawRun);
    fRaw->SetBranchAddress("mode", &fRawMode);
    fRaw->SetBranchAddress("spill", &fRawSpill);

    // check if "r_mizu" has branches for event timing.
    // this branch was implemented from MR43 ?
    // if there is no branches (br==0),
    // it will use timestamp (creation time) of fFin.
    Int_t utime;
    fBrSec = fRaw->GetBranch("time_sec");
    fBrUsec = fRaw->GetBranch("time_nano");
    if (fBrSec != 0 && fBrUsec != 0) {
        fRaw->SetBranchAddress("time_sec", &fRawTimeSec);
        fRaw->SetBranchAddress("time_nano", &fRawTimeUsec);

        utime = fRawTimeSec;
        char *str = ConvertUnixTime(utime);
        fprintf(stderr, "\t[%s]\tfRawTimeSec: %d\tfRawTimeUsec: %d ==> %s\n", __FUNCTION__, fRawTimeSec, fRawTimeUsec, str);
    } else {
        FileStat_t info;
        gSystem->GetPathInfo(ifn, info);
        fTmpRawTimeSec  = info.fMtime;
        fTmpRawTimeUsec = 0;
        
        utime = fTmpRawTimeSec;
        char *str = ConvertUnixTime(utime);
        fprintf(stderr, "Warning:\t[%s]\tNO branches named 'time_sec' nor 'time_nano'\n", __FUNCTION__);
        fprintf(stderr, "Warning:\t[%s]\tUse raw data creation time as event timing\n", __FUNCTION__);
        fprintf(stderr, "\t[%s]\tfRawTimeSec: %d\tfRawTimeUsec: %d ==> %s\n", __FUNCTION__, fTmpRawTimeSec, fTmpRawTimeUsec, str);
    }

    if (fStartTime == 0) fStartTime = utime;
    fRaw->SetBranchAddress("TRG",   &(fMizuRawData.trg) );
    fRaw->SetBranchAddress("ATM",   &(fMizuRawData.atm) );
    fRaw->SetBranchAddress("FADC",  &(fMizuRawData.fadc) );

    Long64_t nevents = fRaw->GetEntries();
    fLog.Form("GetEntries(): %d\n", (int)nevents);
    PrintDebug(1);
    
    return;
}

// __________________________________________________
Int_t Unpacker::Overwrite(const char* ofn)
{
    // Ask if you want to overwrite existing file.
    // DEFAULT : No overwriting
    
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);

    IsOverwritable = kFALSE;
    fprintf(stderr, "Do you want to ovewrite? [y/n/q] > ");
    Int_t answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    Int_t r = 0;
    if (answer == 'q' || answer == 'Q') {
        fprintf(stderr, "\tQuit. Bye Bye\n");
        exit(-1);
    }
    else if (answer == 'y' || answer == 'Y') {
        fprintf(stderr, "\tOverwriting '%s'.\n", ofn);
        IsOverwritable = kTRUE;
    }
    else if (answer == '.') {
        fprintf(stderr, "\tQuit loop.\n");
        exit(-1);
        return -1;
    }
    else {
        fprintf(stderr, "\tSkipped '%s'.\n", ofn);
        exit(-1);
        return -1;
    }
    return r;
}

// __________________________________________________
void Unpacker::OpenUpkFile(const char* ofn, Bool_t kOverwrite = kFALSE)
{
    // Open output ROOT file (recreate)
    // This file will have:
    // 1. TTree : history (see below)
    // 2. TTree : upk (see branches in T2KWCUnpack.h)
    // 3. TH1Ds : (see InitHits() in this file)
    
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    
    if (!kOverwrite) {
        fLog.Form("[%s] Overwrite check", __FUNCTION__);
        LogWarning();
        if (PathExists(ofn, "w") == 0) {
            Overwrite(ofn);
            if (!IsOverwritable) return;
        }
    } else {
        fLog.Form("[%s] Force overwrite", __FUNCTION__);
        LogWarning();
    }
    
    fLog.Form("[%s] Recreate '%s'", __FUNCTION__, ofn);
    LogInfo();
    fOut = new TFile(ofn, "recreate");
    
    fLog.Form("[%s] Create TTree('history')", __FUNCTION__);
    LogDebug();
    fHistory = new TTree("history", "1 day history");
    fHistory->Branch("runnum", &fRunNum, "runnum/I");
    fHistory->Branch("spillnum", &fSpillNum, "spillnum/i");
    fHistory->Branch("time", &fTime, "time/I");
    fHistory->Branch("qdc0", fQdc0Mean, "qdc0[180][2]/D");
    fHistory->Branch("qdc0_rms", fQdc0MeanRms, "qdc0_rms[180][2]/D");
    fHistory->Branch("qdc0rms", fQdc0Rms, "qdc0rms[180][2]/D");
    fHistory->Branch("qdc0rms_rms", fQdc0RmsRms, "qdc0rms_rms[180][2]/D");
    fHistory->Branch("tdc0", fTdc0Mean, "tdc0[180][2]/D");
    fHistory->Branch("tdc0_rms", fTdc0MeanRms, "tdc0_rms[180][2]/D");
    fHistory->Branch("tdc0rms", fTdc0Rms, "tdc0rms[180][2]/D");
    fHistory->Branch("tdc0rms_rms", fTdc0RmsRms, "tdc0rms_rms[180][2]/D");
    fHistory->Branch("atm_pe", fAtmPe, "atm_pe[180][2]/D");
    fHistory->Branch("atm_pe_rms", fAtmPeRms, "atm_pe_rms[180][2]/D");
    fHistory->Branch("atm_charge", fAtmCharge, "atm_charge[180][2]/D");
    fHistory->Branch("atm_charge_rms", fAtmChargeRms, "atm_charge_rms[180][2]/D");
    fHistory->Branch("atm_time", fAtmTime, "atm_time[180][2]/D");
    fHistory->Branch("atm_time_rms", fAtmTimeRms, "atm_time_rms[180][2]/D");
    fHistory->Branch("atm_nhits", &fAtmNhits, "atm_nhits/D");
    fHistory->Branch("atm_nhits_rms", &fAtmNhitsRms, "atm_nhits_rms/D");
    fHistory->Branch("atm_ncharges", &fAtmNcharges, "atm_ncharges/D");
    fHistory->Branch("atm_ncharges_rms", &fAtmNchargesRms, "atm_ncharges_rms/D");
    fHistory->Branch("atm_npes", &fAtmNpes, "atm_npes/D");
    fHistory->Branch("atm_npes_rms", &fAtmNpesRms, "atm_npes_rms/D");
    fHistory->Branch("fadc_nhits", &fFadcNhits, "fadc_nhits/D");
    fHistory->Branch("fadc_nhits_rms", &fFadcNhitsRms, "fadc_nhits_rms/D");
    fHistory->Branch("fadc_ncharges", &fFadcNcharges, "fadc_ncharges/D");
    fHistory->Branch("fadc_ncharges_rms", &fFadcNchargesRms, "fadc_ncharges_rms/D");
    fHistory->Branch("fadc_npes", &fFadcNpes, "fadc_npes/D");
    fHistory->Branch("fadc_npes_rms", &fFadcNpesRms, "fadc_npes_rms/D");
    fHistory->Branch("atm_max_charge", &fAtmMaxCharge, "atm_max_charge/D");
    fHistory->Branch("atm_max_charge_rms", &fAtmMaxChargeRms, "atm_max_charge_rms/D");
    fHistory->Branch("atm_max_pe", &fAtmMaxPe, "atm_max_pe/D");
    fHistory->Branch("atm_max_pe_rms", &fAtmMaxPeRms, "atm_max_pe_rms/D");

    fLog.Form("[%s] Create TTree('fadc')", __FUNCTION__);
    LogDebug();
    fWave = new TTree("fadc", "FADC data");
    fWave->Branch("runnum", &fRunNum, "runnum/I");
    fWave->Branch("spillnum", &fSpillNum, "spillnum/i");
    fWave->Branch("time", &fTime, "time/I");
    fWave->Branch("nch", &fFadcNch, "nch/I");
    TString bname;
    for (Int_t ich = 0; ich < kFadcNchMax; ich++) {
        bname.Form("raw%d", ich);
        fWave->Branch(bname.Data(), &fFadcRaw[ich]);
    }

    fLog.Form("[%s] SetBranch for 'upk'", __FUNCTION__);
    LogDebug();
    fUnpack  = new T2KWCUnpack();
    fUpk = new TTree("upk","Unpacked Raw Data");
    fUpk->Branch("upk.", "T2KWCUnpack", &fUnpack, 64000, 99);

    fLog.Form("[%s] Initialize histograms", __FUNCTION__);
    LogDebug();
    InitHists();
    Clear();

    return;
}


// __________________________________________________
void Unpacker::UnpackTree()
{
    // Loop over entries in fRaw
    // if fRaw doesn't have event time information,
    // it will use timestamp when fFin was created.
    // ( f[Tmp]RawTimeSec is set at ReadRawTree() )

    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();                
    Long64_t nevents = fRaw->GetEntries();
    
    fLog.Form("[%s] Start Event Loop : %d\n", __FUNCTION__, (Int_t)nevents);
    LogDebug();
    
    for (Int_t ievent = 0; ievent < nevents; ievent++) {
        PrintProgressBar(ievent, nevents, 100);    // show progress bar on stderr.
        Clear();  // initialization
        fRaw->GetEntry(ievent);
        if (fBrSec != 0 && fBrUsec != 0) {
            fUnpack->SetTime(fRawTimeSec, fRawTimeUsec);
        }
        else {
            fUnpack->SetTime(fTmpRawTimeSec, fTmpRawTimeUsec);
        }
        UnpackEntry();
        LogDebug("--------------------------------------------------------------------------------");
    }
    return;
}

// __________________________________________________
void Unpacker::Unpack(const char* ifn, const char* detmap)
{
    // This is the main program called by unpack.cc
    // 1. ReadDetectorMap(detmap)
    // 2. ReadRawTree(ifn)
    // 3. RenameHists() if necessary
    // 4. UnpackTree()  <-- main program
    // 5. Close ROOT file
    //
    // Remarks:
    // 1. Skip files if it were not opend propely.
    // 2. Keep 1 hr-interval log (histogram) is for simple check.

    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    
    const Int_t interval = 60 * 60;// * 12;
    ReadDetectorMap(detmap);
    // if (!IsDetmapRead) {
    //     fprintf(stderr, "Error:\t'ReadDetectorMap' not called.\n");
    //     exit(-1);
    // }

    ReadRawTree(ifn);

    //char *t1 = ConvertUnixTime(fStartTime);
    time_t t = fStartTime;
    struct tm* m = localtime(&t);
    char t1[500];
    strftime(t1, sizeof(t1), "%Y/%m/%d %H:%M:%S", m);

    if (!IsHistRenamed) RenameHists();
    if (IsFileOpened) {
        UnpackTree();
        Int_t time = (Int_t)fUnpack->GetTime();
        //char *t2 = ConvertUnixTime(time);
        Int_t sec  = fUnpack->GetTimeSec();
        Int_t usec = fUnpack->GetTimeUsec();
        t = time;
        struct tm* m2 = localtime(&t);
        char t2[500];
        strftime(t2, sizeof(t2), "%Y/%m/%d %H:%M:%S", m2);
        if (time - fStartTime > interval) {
            fprintf(stderr, "[%s]:\tTime %d (%d [sec] + %d [usec])\n",
                    __FUNCTION__, time, sec, usec);
            fprintf(stderr, "[%s]:\tExceed 1 day (%s ==> %s : diff:%d sec)\n",
                    __FUNCTION__, t1, t2, time - fStartTime);
            AddHists();
            fStartTime = time;
        }
    }
    else {
        fprintf(stderr, "Error:\t[%s]\tSkip unpacking '%s'.\n", __FUNCTION__, ifn);
    }

    if (fFin) fFin->Close();

    return;
}

// __________________________________________________
void Unpacker::Unpack(const T2KWCRaw *rw)
{
    // this unpacker is used for online monitor.
    // MizuDAQ sends T2KWCRaw in TSocket.
    // Check mizu_online2.cc for details.
    
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Clear();
    
    fLog.Form("[%s] Unpack TRG", __FUNCTION__);
    LogInfo();
    Int_t ntrg = rw->nTRG;
    fMizuRawData.trg.nTRG = ntrg;
    for (Int_t itrg = 0; itrg < ntrg; itrg++) {
        for (Int_t jtrg = 0; jtrg < 5; jtrg++) {
            fMizuRawData.trg.TRG[itrg][jtrg] = rw->TRG[itrg][jtrg];
            fLog.Form("[%s] TRG[%d/%d][%d] : %d",
                      __FUNCTION__,
                      itrg, ntrg, jtrg, rw->TRG[itrg][jtrg]);
            LogDebug();
        }
    }

    fLog.Form("[%s] Unpack ATM", __FUNCTION__);
    LogInfo();
    Int_t natm = rw->nATM;
    fMizuRawData.atm.nATM = natm;
    for (Int_t iatm = 0; iatm < natm; iatm++) {
        if ( (iatm%1000 == 0 && rw->mode == 0) || rw->mode == 1 ) {
            fLog.Form("[%s] ATM[%5d/%5d]   : %d",
                      __FUNCTION__,
                      iatm, natm, rw->ATM[iatm]);
            LogDebug();
        }
        fMizuRawData.atm.ATM[iatm] = rw->ATM[iatm];
    }

    
    fLog.Form("[%s] Unpack FADC", __FUNCTION__);
    LogInfo();
    Int_t nfadc = rw->nFADC;
    fMizuRawData.fadc.nFADC = nfadc;
    for (Int_t ifadc = 0; ifadc < nfadc; ifadc++) {
        if (ifadc%1000 == 0) {
            fLog.Form("[%s] FADC[%5d/%5d]      : %d",
                      __FUNCTION__,
                      ifadc, nfadc, rw->FADC[ifadc]);
            LogDebug();
        }
        fMizuRawData.fadc.FADC[ifadc] = rw->FADC[ifadc];
    }

    fLog.Form("[%s] Unpack Run#, Spill#, Mode", __FUNCTION__);
    LogInfo();
    fRawRun  = rw->run;
    fRawSpill = rw->spill;
    fRawMode = rw->mode;
    //
    UnpackEntry();
}

// __________________________________________________
void Unpacker::UnpackEntry() 
{
    // unpack data in an entry and set values to fUnpack.
    // Set basic vallues : [runmode, runnum]
    // branch process by ATM mode (PEDESTAL or OPERATE)
    // if PEDESTAL:
    //    calculate ATM pedestal values
    //    fill histograms with pedestal for each channel for quick check.
    // if OPERATE:
    //    set spill number
    //    unpack FADC
    //    unpack TRG
    //    unpack ATM
    //    store histgrams for log
    //
    // Remarks:
    //   1. in fRaw, PEDESTAL and OPERATE is in different entry.
    //      but for fUnpack they are merged into 1 entry.
    
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    fLog.Form("[%s] SetRunmode: %d", __FUNCTION__, fRawMode);
    LogDebug();
    fLog.Form("[%s] SetRunNum : %d", __FUNCTION__, fRawRun);
    LogDebug();

    fUnpack->SetRunmode(fRawMode);
    fUnpack->SetRunNum(fRawRun);

    // Calculate ATM pedestals in PEDESTAL_MODE,
    // Convert ATM, TRG, FADC values in OPERATE_MODE
    
    if (fRawMode == PEDESTAL_MODE) {
        fLog.Form("[%s] Mode: PEDESTAL", __FUNCTION__);
        LogDebug();
        InitAtmPedestal();
        UnpackAtm(fRawMode);
#ifdef BeforeMR40
        UnpackAtmOld(fRawMode);
#endif
        GetAtmPedestal();
    }
    else if( fRawMode==OPERATE_MODE ) {
        fLog.Form("[%s] Mode: OPERATE", __FUNCTION__);
        LogDebug();

        fUnpack->SetSpillNum(fRawSpill);
        
        // FADC part
        InitFadc();
        //UnpackFadc(2);  // Decode FADC data
        //UnpackFadc(4);  // Decode FADC data
        UnpackFadc2();
        //Stop();
        //ConvertFadc();  // Calculate pedestals for HITSUM and PMTSUM,
        AnalyzeFadc();    // Copied ConvertFadc() and renamed.
        // then calculete #ofHits and Charges
        //ConvertFADC_shota();
        ConvertFadcBunch();
        
        // TRG part
        UnpackTrg();    // Decode TRG data
        
        // ATM part
        if (fUnpack->GetAtmNtrg() > 0) {
            UnpackAtm(fRawMode);    // Decode signal ATM data
#ifdef BeforeMR40
            UnpackAtmOld(fRawMode);
#endif
            //if( IsAtmGood )
            if( IsTrgGood && IsAtmGood) {
                fLog.Form("[%s] Spill# %4d. ATM converted.", __FUNCTION__, fRawSpill);
                LogDebug();
                ConvertATM(); // Convert 'QDC', 'TDC' to 'Charge', 'PE', 'Time'
                Int_t    atm_ntrg       = fUnpack->GetAtmNtrg();
                Int_t    atm_nhits      = fUnpack->GetAtmNhits();
                Double_t atm_ncharges   = fUnpack->GetAtmNcharges();
                Double_t atm_npes       = fUnpack->GetAtmNpes();
                Double_t atm_max_charge = fUnpack->GetAtmMaxCharge();
                Double_t atm_max_pe     = fUnpack->GetAtmMaxPe();
                // ----------
                Int_t    fadc_ntrg      = fUnpack->GetFadcNtrg();
                Int_t    fadc_nhits     = fUnpack->GetFadcNhits();
                Double_t fadc_ncharges  = fUnpack->GetFadcNcharges();
                Double_t fadc_npes      = fUnpack->GetFadcNpes();

                if (fadc_ntrg == 1 && atm_ntrg == 1) {
                    hAtmNhitsPart    ->Fill(atm_nhits);
                    hAtmNchargesPart ->Fill(atm_ncharges);
                    hAtmNpesPart     ->Fill(atm_npes);
                    hAtmMaxChargePart->Fill(atm_max_charge);
                    hAtmMaxPePart    ->Fill(atm_max_pe);
                    // ----------
                    hFadcNhitsPart   ->Fill(fadc_nhits);
                    hFadcNchargesPart->Fill(fadc_ncharges);
                    hFadcNpesPart    ->Fill(fadc_npes);
                }
            } //else fprintf(stdout, "[%s]\tEntry# %4d. ATM not converted.\n", __FUNCTION__, fRawSpill);
        } //else fprintf(stdout, "[%s]\tEntry# %4d. ATM not unpacked.\n", __FUNCTION__, fRawSpill);

        if (fUnpackMode == kLIGHTMODE) {
            // This is needed when merging with BSD.
            fLog.Form("[%s] SetFadcNsamp(0)", __FUNCTION__);
            LogDebug();
            fUnpack->SetFadcNsamp(0);
        }
        else {
            fLog.Form("[%s] Fill Fadc", __FUNCTION__);
            LogDebug();
            FillFadc();
            fLog.Form("[%s] Filled Fadc", __FUNCTION__);
            LogDebug();
        }
        
        fLog.Form("[%s] Fill Upk", __FUNCTION__);
        LogDebug();
        FillUpk();
        fLog.Form("[%s] Filled Upk", __FUNCTION__);
        LogDebug();
    }
    return;
}

// __________________________________________________
void Unpacker::UnpackTrg()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    IsTrgGood = kFALSE;

    Int_t ntrg = fMizuRawData.trg.nTRG;    // Total TRG event #
    fUnpack->SetAtmNtrg(ntrg);
#ifdef DEBUG_UTRG
    fLog.Form("[%s] DEBUG_UTRG:\tnTRG: %4d", __FUNCTION__, ntrg);
    LogDebug();
#endif
    
    if (ntrg == 0) {
        IsTrgGood = kFALSE;
#ifdef DEBUG_UTRG
        fLog.Form("[%s] DEBUG_UTRG:\t==> No TRG data", __FUNCTION__);
        LogDebug();
#endif
    } else {
        IsTrgGood = kTRUE;
        fUnpack->SetAtmTrg(fMizuRawData.trg.TRG[0][0]);
        if (ntrg > 1) {
            fLog.Form("[%s] nTRG: %2d ==> Found MORE than 1 event / spill",
                      __FUNCTION__, ntrg);
            LogWarning();
            //IsTrgGood = kFALSE;
        } else {
            fLog.Form("[%s] ==> Found 1 event / spill", __FUNCTION__);
            LogDebug();
        }
    }

#ifdef ENABLE_NTRG
    if ( NTRG > 0 ) {
#ifdef DEBUG_NTRG
        fLog.Form("[%s] DEBUG_NTRG:\tnTRG: %d\n", __FUNCTION__, ntrg);
        LogDebug();
#endif
        for (Int_t itrg = 0; itrg < ntrg; itrg++) {
            Int_t trg = fMizuRawData.trg.TRG[itrg][0];
            fAtmNtrgnum.push_back(trg);
#ifdef DEBUG_NTRG
            fLog.Form("[%s] DEBUG_NTRG:\tTRG[%d][0]: %d\tfAtmNtrgnum.size: %d\n",
                      __FUNCTION__, itrg, trg, (Int_t)fAtmNtrgnum.size());
            LogDebug();
#endif
        }
    }
#ifdef DEBUG_NTRG
    fLog.Form("[%s] DEBUG_NTRG:\tnTRG: %d\tfAtmNtrgnum.size: %d\n",
              __FUNCTION__, ntrg, (Int_t)fAtmNtrgnum.size());
    LogDebug();
#endif
#endif

#ifdef DEBUG_UTRG
    fLog.Form("[%s] DEBUG_UTRG:\t==>\tIsTrgGood: %d\n",
              __FUNCTION__, IsTrgGood);
    LogDebug();
#endif
    return;
}

 
// __________________________________________________
void Unpacker::UnpackAtm(const Int_t &mode)
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();                                    

    TString m;
    if (mode == PEDESTAL_MODE)     m.Form("UnpackAtm( PEDESTAL )");
    else if (mode == OPERATE_MODE) m.Form("UnpackAtm( OPERATE )");

#ifdef DEBUG_UATM
    fLog.Form("[%s] DEBUG_UATM:\t%s", __FUNCTION__, m.Data());
    LogDebug();
#endif

    UShort_t hi_bit;
    UShort_t lo_bit;
    Short_t ma_num;
    Short_t sa_num;
    Short_t gong_num = 1000;
    Short_t tag;
    Short_t channel = -1000;
    Short_t event_num;
    Short_t ch_num;
    Short_t type_num = -1000;
    Short_t adc_data;
    
    T2KWCHit ahit;
    atm_buff raw_atm = fMizuRawData.atm;

    Int_t natm = raw_atm.nATM;
    fLog.Form("Loop over nATM:%d", natm);
    LogDebug();
    
    if( natm == 0 ) {
        IsAtmGood = kFALSE;
        fLog.Form("[%s] nATM:%8d\tIsAtmGood:%d ==> No ATM data. Skipping.",
                  __FUNCTION__, natm, IsAtmGood);
        LogError();
        return;
    }
    
    for(Int_t iatm = 0; iatm < natm; iatm++) {
        hi_bit = ((raw_atm.ATM[iatm])>>16) & 0xFFFF;
        lo_bit = (raw_atm.ATM[iatm]) & 0xFFFF;
        ma_num = (hi_bit>>11) & 0x001F;
        sa_num = hi_bit & 0x07FF;
        tag    = (lo_bit>>14) & 0x0003;

#ifdef DEBUG_UATM
        fLog.Form("[%s] DEBUG_UATM1:\tATM[%5d]:0x%08x = H:0x%04x (m:%2d, s:%4d) L:0x%04x (t:%d)",
                  __FUNCTION__, iatm, raw_atm.ATM[iatm], hi_bit, ma_num, sa_num, lo_bit, tag);
        LogDebug();
#endif
        
        if (iatm == 0) {
#ifdef DEBUG_UATM
            fLog.Form("[%s] DEBUG_UATM2:\t==>\tData size of this entry : %d [bytes]\n",
                      __FUNCTION__, lo_bit);
            LogDebug();
#endif
        } else if (iatm == 1) {
            Int_t nwords_gong   = 2;
            Int_t nwords_per_ch = 3;

#ifdef DEBUG_UATM
            Int_t total_nhits = Int_t( (lo_bit-nwords_gong) / (nwords_per_ch) );
            fLog.Form("DEBUG_UATM2:\t==>\tSerial#: %d\tWord#: %d\n", hi_bit, lo_bit);
            PrintDebug(2);
            fLog.Form("DEBUG_UATM2:\t==>\tTotal # of hits: %d\n", total_nhits);
            PrintDebug(2);
#endif            

            if( mode==PEDESTAL_MODE ) {
                lo_bit  = (raw_atm.ATM[iatm]) & 0xFFFF;
                Int_t nume = lo_bit - nwords_gong;
                Int_t deno = nwords_per_ch * kNboards * kNchPerBoard * kNbuffPerCh;
                fPedEvents = Int_t(nume / deno);
#ifdef DEBUG_UATM
                fLog.Form("DEBUG_UATM2:\t==>\t# of pedestal events: %d\n", fPedEvents);
                PrintDebug(2);
#endif
            }
        }
        // Main part : the order of data is GONG -> ATM(ch info->TDC->QDC)
        else {
            // GONG part : Set GONG#
            if( hi_bit == 0 ) {
                gong_num = lo_bit;
                fUnpack->SetAtmGong( gong_num );
#ifdef DEBUG_UATM
                fLog.Form("DEBUG_UATM2:\t[%s]\t==>\tGONG #: %d\n", __FUNCTION__, gong_num);
                PrintDebug(2);
#endif
            }
            
            // ATM part
            // tag == 0 : event info
            // tag == 1 : TDC
            // tag == 2 : QDC
            // else     : ignore
            else if ( ma_num > 0 && hi_bit != 0xff00 ) {
                if (tag == 0) {
                    event_num = (lo_bit>>6) & 0xFF;       // ATM # = Event #
                    type_num  = (lo_bit>>5) & 0x1;        // ATM Ch Type
                    ch_num    = lo_bit & 0xF;             // CH #
                    channel   = (ma_num-1)*12 + ch_num;   // ATMCH #

#ifdef DEBUG_UATM
                    fLog.Form("DEBUG_UATM2:\t==>\tATM # : %4d\tType: %4d\tCh: %4d\tATMCH: %4d\n",
                              event_num, type_num, ch_num, channel);
                    PrintDebug(2);
#endif

                    // Check ATM# and TRG# matching : ([ATM# = TRG#] => good_atm_daq=true)
                    if( mode == OPERATE_MODE) {
#ifndef ENABLE_NTRG
                        if (event_num != (fUnpack->GetAtmTrg() & 0xffff)) {
                            fLog.Form("Warning @ ATM# != TRG# : ");
                            fLogTmp.Form("ATM#:TRG#:GONG# : %3d != %3d != %3d ",
                                         event_num, (fUnpack->GetAtmTrg() & 0xffff), (gong_num & 0xff));
                            fLog += fLogTmp;
                            fLogTmp.Form("(Atmch:%4d\tType:%2d)", channel, type_num);
                            fLog += fLogTmp;
                            LogWarning();
                            IsAtmGood = kFALSE;
                        }
                        else IsAtmGood = kTRUE;
#else
                        
#ifdef DEBUG_NTRG
                        fLog.Form("DEBUG_NTRG:\tnTRG : fAtmNtrgnum.size = %4d : %4d\n",
                                  fMizuRawData.trg.nTRG, (Int_t)fAtmNtrgnum.size());
                        PrintDebug(1);
#endif
                        for (Int_t itrg = 0; itrg < fMizuRawData.trg.nTRG; itrg++) {
                            Int_t trg    = fAtmNtrgnum.at(itrg);
                            Int_t trgevt = trg & 0xffff;
                            if (event_num != trgevt) {
#ifdef DEBUG_NTRG
                                Int_t trg0 = fMizuRawData.trg.TRG[itrg][0];
                                fLog.Form("DEBUG_NTRG:\tTRG0 : TRG = %4d : %4d\n", trg0, trg);
                                PrintDebug(1);
                                fLog.Form("DEBUG_NTRG:\tEve0 : Eve = %4d : %4d\n", event_num, trgevt);
                                PrintDebug(1);
#endif
                                fprintf(stdout, "Warning @ ATM - NTRG part.");
                                fprintf(stdout, "\tATM# is INCONSISTENT with TRG #.");
                                fprintf(stdout, "(ATM#:TRG#:GONG# : %3d != %3d != %3d)",
                                        event_num, trgevt, (gong_num & 0xff) );
                                fprintf(stdout, "\t(Atmch:%4d\tType:%2d)\n", channel, type_num);
                                IsAtmGood = kFALSE;
                            } else IsAtmGood = kTRUE;
                            
                        }   
#endif
                    }

                    ahit.SetTag(event_num);
                    ahit.SetCh(channel);
                    ahit.SetWindow( GetAtmchWindow(channel) );
                    ahit.SetPmt( GetAtmchPmt(channel) );
                    ahit.SetType(type_num);
                }
                // ----- Unpack ATM TDC ------------------------------
                else if(tag == 1) {
                    adc_data = lo_bit & 0x0FFF; 
                    ahit.SetTdc1(adc_data);
                    if(mode == PEDESTAL_MODE) {
                        fPedTDC[channel][type_num] += adc_data;
                        fTdcPedTmp[channel][type_num].push_back(adc_data);
                    }
                    
#ifdef DEBUG_UATM
                    fLog.Form("DEBUG_UATM2:Unpack ATM TDC | Mode  : %4d\tTDC: %4d\tfPedTDC: %8.3f\n",
                              mode, adc_data, fPedTDC[channel][type_num] );
                    PrintDebug(2);
#endif
                }

                // ----- Unpack ATM QDC ------------------------------
                else if(tag == 2) {
                    adc_data = lo_bit & 0x0FFF; 
                    ahit.SetQdc1( adc_data );
                    if( mode==PEDESTAL_MODE ) {
                        fPedQDC[channel][type_num] += adc_data;
                        fQdcPedTmp[channel][type_num].push_back(adc_data);
                    }
#ifdef DEBUG_UATM
                    fLog.Form("DEBUG_UATM2:Unpack ATM QDC | Mode  : %4d\tQDC: %4d\tfPedQDC: %8.3f\n",
                              mode, adc_data, fPedQDC[channel][type_num] );
                    PrintDebug(2);
#endif
                    // In raw data format, QDC is last
                    // Skip bad channel
                    if ( IsAtmchBad(channel) == kFALSE ) { fUnpack->AddHit( &ahit ); }
                    ahit.Clear();
                } else {
#ifdef DEBUG_UATM
                    fprintf(stdout, "DEBUG_UATM:\t[%s]\t==>\tTag:%d\n", __FUNCTION__, tag);
#endif
                }
            } // end of ATM part
            
#ifdef DEBUG_UATM
            // End of each event
            if ( hi_bit == 0xff00 && lo_bit==0x0000 ) {
                fprintf(stdout, "DEBUG_UATM:\t[%s]\t==>\tEnd of event\n", __FUNCTION__);
            }
            // End of entry
            if( hi_bit==0xffff && lo_bit==0xffff ) {
                fprintf(stdout, "DEBUG_UATM:\t[%s]\t==>\tEnd of this entry\n", __FUNCTION__); 
                fprintf(stdout, "DEBUG_UATM:\t[%s]\t==>\tIsAtmGood? : %d\n", __FUNCTION__, IsAtmGood);
                }
#endif
        } // end of nhits loop
    }
}


// __________________________________________________
void Unpacker::UnpackAtmOld(const Int_t &mode)
{

    fLog.Form("DEBUG1:\t[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(1);

    TString m;
    if (mode == PEDESTAL_MODE)     m.Form("UnpackAtm( PEDESTAL )");
    else if (mode == OPERATE_MODE) m.Form("UnpackAtm( OPERATE )");
    PrintDebug(0,  m.Data() );

    UShort_t high_bit,low_bit;
    Short_t ma_num, sa_num;
    Short_t gong_num = -1000, tag, channel = -1000;
    Short_t event_num, ch_num, type_num = -1000, adc_data;
    
    T2KWCHit ahit;
    atm_buff raw_atm = fMizuRawData.atm;

    Int_t natm = raw_atm.nATM;

    if( natm == 0 ) {
        IsAtmGood = kFALSE;
        fLog.Form("[%s]", __FUNCTION__);
        fLogTmp.Form("\t--> nATM:%8d", natm);
        fLog += fLogTmp;
        fLogTmp.Form("\t==> No ATM data. Skipping.");
        fLog += fLogTmp;
        fLogTmp.Form("\t(nATM:%8d\tIsAtmGood:%d", natm, IsAtmGood);
        fLog += fLogTmp;
        LogError();
        return;
    }
    
    PrintDebug(0,  m + " ==> Loop over nATM");

    for(Int_t i = 0; i < natm; i++) {
        high_bit = ( (raw_atm.ATM[i])>>16 ) & 0xFFFF;
        low_bit  = (raw_atm.ATM[i]) & 0xFFFF;
        ma_num   = (high_bit>>11) & 0x001F;
        sa_num   = high_bit & 0x07FF;
        tag      = (low_bit>>14) & 0x0003;

#ifdef DEBUG_UATM
        if ( i%3000 == 0 ) {
            printf("DEBUG_UATM:\t[%5d]\tATM:0x%.8x = H:0x%.4x (m:%2d, s:%4d) L:0x%.4x (t:%d)\n",
                   i, raw_atm.ATM[i], high_bit, ma_num, sa_num, low_bit, tag);
        }
#endif
        if (i == 0 ) {
#ifdef DEBUG_UATM
            printf("DEBUG_UATM:\t[%5d]\tData size of this entry : %d [bytes]\n", i, low_bit);
#endif
        } else if( i==1 ) {
            Int_t nwords_gong   = 2;
            Int_t nwords_per_ch = 3;
#ifdef DEBUG_UATM
            Int_t total_nhits = int( (low_bit-nwords_gong) / (nwords_per_ch) );
            printf("DEBUG_UATM:\t[%5d]\tSerial#: %d", i, high_bit);
            printf("\tWord#: %d\n", low_bit);
            printf("DEBUG_UATM:\t--> Total # of hits (total_nhits): %d\n",total_nhits);
#endif            
            if( mode==PEDESTAL_MODE ) {
                low_bit  = (raw_atm.ATM[i]) & 0xFFFF;
                fPedEvents =
                    Int_t((low_bit-nwords_gong)/(nwords_per_ch*kNboards*kNchPerBoard*kNbuffPerCh));
#ifdef DEBUG_UATM
                printf("DEBUG_UATM:\t--> # of pedestal events (fPedEvents) : %d\n", fPedEvents);
#endif
            }
        }

        // Main part : the order of data is GONG -> ATM(ch info->TDC->QDC)
        else {
            // GONG part
            if( high_bit==0 ) {           
                gong_num = low_bit;
                fUnpack->SetAtmGong( gong_num );

#ifdef BeforeMR40
                PrintDebug( m + " BeforeMR40" );
                // Check GONG# and TRG# matching
                // ([GONG# = TRG# + 1] => good_atm_daq=true)
                if( mode==OPERATE_MODE && (gong_num != (fUnpack->GetAtmTrg() & 0xffff) + 1) ) {
                    fprintf(stdout, "WARNING @ GONG-TRG part\n");
                    fprintf(stdout, "\tGONG # is INCONSISTENT with TRG #.");
                    fprintf(stdout, "(G:%d != T:%d)\n", gong_num, (fUnpack->GetAtmTrg() & 0xffff) +1 );
                    IsAtmGood = kFALSE;
                }
#endif

            } // end of GONG part

            // ATM part
            else if( ma_num>0 && high_bit!=0xff00 ) {
                if(tag==0) {
                    event_num = (low_bit>>6) & 0xFF;       // ATM # = Event #
                    type_num  = (low_bit>>5) & 0x1;        // ATM Ch Type
                    ch_num    = low_bit & 0xF;             // CH #
                    channel   = (ma_num-1)*12 + ch_num;    // ATMCH #
#ifdef DEBUG_UATM
                    printf("DEBUG_UATM:\t--> EVENT#:%7d\tType:%d\tCh:%4d\tATMCH:%4d\n",
                           event_num, type_num, ch_num, channel);
#endif
                    
#ifdef BeforeMR40
                    // Check GONG# and ATM# matching
                    // ([GONG# = ATM#] => good_atm_daq=true)
                    if( mode==OPERATE_MODE && event_num != (gong_num&0xff) ) {
                        fprintf(stdout, "Warning @ ATM-GONG part\n");
                        fprintf(stdout, "\tGONG# is INCONSISTENT with ATM #.");
                        fprintf(stdout, "(G:%d != A:%d)", gong_num & 0xff, event_num);
                        fprintf(stdout, "\t(Ch:%d\tType:%d)\n", channel, type_num);
                        IsAtmGood = kFALSE;
                    }
#else
                    // Check ATM# and TRG# matching
                    // ([ATM# = TRG#] => good_atm_daq=true)
                    if( mode==OPERATE_MODE) {
                        if (event_num != (fUnpack->GetAtmTrg() & 0xffff)) {
                            fprintf(stdout, "Warning @ ATM- TRG part.");
                            fprintf(stdout, "\tATM# is INCONSISTENT with TRG #.");
                            fprintf(stdout, "(A:%d != T:%d != G:%d)", event_num, (fUnpack->GetAtmTrg() & 0xffff), (gong_num & 0xff) );
                            fprintf(stdout, "\t(Ch:%d\tType:%d)\n", channel, type_num);
                            IsAtmGood = kFALSE;
                        }
#ifdef DEBUG_NTRG
                        // for (Int_t i = 0; i < fMizuRawData.trg.nTRG; i++) {
                        //     if (event_num != (fUnpack->NTRGNum(i) & 0xffff) ) {
                        //         fprintf(stdout, "Warning @ ATM-NTRG part.");
                        //         fprintf(stdout, "\tATM# is INCONSISTENT with TRG #.");
                        //         fprintf(stdout, "(A:%d != T:%d != G:%d)", event_num, (fUnpack->NTRGNum(i) & 0xffff), (gong_num & 0xff) );
                        //         fprintf(stdout, "\t(Ch:%d\tType:%d)\n", channel, type_num);
                        //         IsAtmGood = kFALSE;
                        //     }
                        // }   
#endif
                    }
#endif
                    ahit.SetTag( event_num );
                    ahit.SetCh( channel );
                    ahit.SetWindow( GetAtmchWindow(channel) );
                    ahit.SetPmt( GetAtmchPmt(channel) );
                    ahit.SetType( type_num );
                }

                // TDC
                else if(tag == 1) {
                    adc_data = low_bit & 0x0FFF; 
                    ahit.SetTdc1( adc_data );
                    if( mode==PEDESTAL_MODE ) fPedTDC[channel][type_num] += adc_data;
#ifdef DEBUG_UATM
                    if (i%500 == 0) {
                        printf("\t--> [%5d][t:%d]\tTDC:%4d\tfPedTDC: %5.1f\n",
                               i, tag, adc_data, fPedTDC[channel][type_num] ); }
#endif
                }
                // QDC
                else if(tag == 2) {
                    adc_data = low_bit & 0x0FFF; 
                    ahit.SetQdc1( adc_data );
                    if( mode==PEDESTAL_MODE ) fPedQDC[channel][type_num] += adc_data;
#ifdef DEBUG_UATM
                    if (i%500 == 0) {
                        printf("DEBUG_UATM:\t--> [%5d][t:%d]\tQDC:%4d\tfPedQDC: %5.1f\n",
                               i, tag, adc_data, fPedQDC[channel][type_num] ); }
#endif
                    // In raw data format, QDC is last
                    // Skip bad channel
                    if ( IsAtmchBad(channel) == kFALSE ) { fUnpack->AddHit( &ahit ); }
                    ahit.Clear();
                    
                } else {
#ifdef DEBUG_UATM
                    printf("DEBUG_UATM:\t[%5d]\tATM:0x%.8x = H:0x%.4x (m:%2d, s:%4d) L:0x%.4x (t:%d)",
                           i, raw_atm.ATM[i], high_bit, ma_num, sa_num, low_bit, tag);
                    printf("\t--> else (Tag:%d)\n", tag);
#endif
                }
            } // end of ATM part

#ifdef DEBUG_UATM
            // End of each event
            if( high_bit == 0xff00 && low_bit==0x0000 ) {
                printf("DEBUG_UATM:\t[%5d]\tATM:0x%.8x = H:0x%.4x (m:%2d, s:%4d) L:0x%.4x (t:%d)",
                       i, raw_atm.ATM[i], high_bit, ma_num, sa_num, low_bit, tag);
                printf("\t--> module down bit (end of event)\n"); }

            // End of entry
            if( high_bit==0xffff && low_bit==0xffff ) {
                printf("DEBUG_UATM:\t[%5d]\tATM:0x%.8x = H:0x%.4x (m:%2d, s:%4d) L:0x%.4x (t:%d)",
                       i, raw_atm.ATM[i], high_bit, ma_num, sa_num, low_bit, tag);
                printf("\t--> Footer (end of this entry)\n"); }
#endif
        } // end of nhits loop
    }
}

// __________________________________________________
TH1D *Unpacker::InitHist(const char* name, const char* title,
                         const Double_t &xmin, const Double_t &xmax,
                         const Int_t &color, const Int_t &binwidth = 1)
{
#ifdef DEBUG3
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(3);
#endif
    Int_t xbin = (Int_t)xmax - (Int_t)xmin;
    xbin *= binwidth;
    TH1D *h = new TH1D(name, title, xbin, xmin, xmax);
    h->SetLineColor(color);
    h->SetFillColor(color);
    h->SetLineWidth(2);
    return h;
}

// __________________________________________________
void Unpacker::InitHists()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug(fLog.Data());

    // if (!IsDetmapRead) {
    //     fprintf(stderr, "Error:\t'ReadDetectorMap' not called.\n");
    //     exit(-1);
    // }
    
    Double_t xmin = 0   , xmax = 5000;    // ATM dynamic range [0 - 4096]
    Double_t rmin = 0   , rmax = 50;      // range for RMS of pedestal, with 10 bins;
    Double_t pmin = -500, pmax = 1500;    // range for P.E.
    Double_t qmin = -500, qmax = 1500;    // range for Charge
    Double_t tmin = -500, tmax = 1500;    // range foe Timing

    Int_t color = 2;
    TString name, title, t;
    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        for (Int_t itype = 0; itype < kNtype; itype++) {
            color = itype + 2;
            // ------------------------------
            name.Form("hQdcPedPart%03dT%d", iatm, itype);
            title.Form("%s;raw QDC [count];Entries [#]", name.Data());
            hQdcPedPart[iatm][itype] = InitHist(name.Data(), title.Data(), xmin, xmax, color);
            // ----------
            name.Form("hTdcPedPart%03dT%d", iatm, itype);
            title.Form("%s;raw TDC [count];Entries [#]", name.Data());
            hTdcPedPart[iatm][itype] = InitHist(name.Data(), title.Data(), xmin, xmax, color);
            // ----------
            name.Form("hQdcPedRmsPart%03dT%d", iatm, itype);
            title.Form("%s;raw QDC RMS [count];Entries [#]", name.Data());
            hQdcPedRmsPart[iatm][itype] = InitHist(name.Data(), title.Data(), rmin, rmax, color, 10);
            // ----------
            name.Form("hTdcPedRmsPart%03dT%d", iatm, itype);
            title.Form("%s;raw TDC RMS [count];Entries [#]", name.Data());
            hTdcPedRmsPart[iatm][itype] = InitHist(name.Data(), title.Data(), rmin, rmax, color, 10);
            // ----------
            name.Form("hAtmPePart%03dT%d", iatm, itype);
            title.Form("%s;P.E. [#];Entries [#]", name.Data());
            hAtmPePart[iatm][itype] = InitHist(name.Data(), title.Data(), pmin, pmax, color);
            // ----------
            name.Form("hAtmChargePart%03dT%d", iatm, itype);
            title.Form("%s;Charge [pC];Entries [#]", name.Data());
            hAtmChargePart[iatm][itype] = InitHist(name.Data(), title.Data(), qmin, qmax, color, 10);
            // ----------
            name.Form("hAtmTimePart%03dT%d", iatm, itype);
            title.Form("%s;Time [nsec];Entries [#]", name.Data());
            hAtmTimePart[iatm][itype] = InitHist(name.Data(), title.Data(), tmin, tmax, color);
            // ------------------------------
            name.Form("hQdcPed%03dT%d", iatm, itype);
            title.Form("%s;raw QDC [count];Entries [#]", name.Data());
            hQdcPed[iatm][itype] = InitHist(name.Data(), title.Data(), xmin, xmax, color);
            // ----------
            name.Form("hTdcPed%03dT%d", iatm, itype);
            title.Form("%s;raw TDC [count];Entries [#]", name.Data());
            hTdcPed[iatm][itype] = InitHist(name.Data(), title.Data(), xmin, xmax, color);
            // ----------
            name.Form("hQdcPedRms%03dT%d", iatm, itype);
            title.Form("%s;raw QDC RMS [count];Entries [#]", name.Data());
            hQdcPedRms[iatm][itype] = InitHist(name.Data(), title.Data(), rmin, rmax, color, 10);
            // ----------
            name.Form("hTdcPedRms%03dT%d", iatm, itype);
            title.Form("%s;raw TDC RMS [count];Entries [#]", name.Data());
            hTdcPedRms[iatm][itype] = InitHist(name.Data(), title.Data(), rmin, rmax, color, 10);
            // ----------
            name.Form("hAtmPe%03dT%d", iatm, itype);
            title.Form("%s;P.E. [#];Entries [#]", name.Data());
            hAtmPe[iatm][itype] = InitHist(name.Data(), title.Data(), pmin, pmax, color);
            // ----------
            name.Form("hAtmCharge%03dT%d", iatm, itype);
            title.Form("%s;Charge [pC];Entries [#]", name.Data());
            hAtmCharge[iatm][itype] = InitHist(name.Data(), title.Data(), qmin, qmax, color, 10);
            // ----------
            name.Form("hAtmTime%03dT%d", iatm, itype);
            title.Form("%s;Time [nsec];Entries [#]", name.Data());
            hAtmTime[iatm][itype] = InitHist(name.Data(), title.Data(), tmin, tmax, color);
        }
    }

    color = 2;
    Double_t npmin = 0, npmax = 1000;   // range for Npes
    Double_t nqmin = 0, nqmax = 1000;   // range for Ncharges
    Double_t nhmin = 0, nhmax = 200;    // range for Nhits
    // ------------------------------
    name.Form("hAtmNhitsPart");
    title.Form("%s;Nhits [#];Entries [#]", name.Data());
    hAtmNhitsPart = InitHist(name.Data(), title.Data(), nhmin, nhmax, color);
    // --------------------
    name.Form("hAtmNchargesPart");
    title.Form("%s;Ncharges [pC];Entries [#]", name.Data());
    hAtmNchargesPart = InitHist(name.Data(), title.Data(), nqmin, nqmax, color);
    // --------------------
    name.Form("hAtmNpesPart");
    title.Form("%s;Npes [#];Entries [#]", name.Data());
    hAtmNpesPart = InitHist(name.Data(), title.Data(), npmin, npmax, color);
    // --------------------
    name.Form("hFadcNhitsPart");
    title.Form("%s;Nhits [#];Entries [#]", name.Data());
    hFadcNhitsPart = InitHist(name.Data(), title.Data(), nhmin, nhmax, color+2);
    // --------------------
    name.Form("hFadcNchargesPart");
    title.Form("%s;Ncharges [pC];Entries [#]", name.Data());
    hFadcNchargesPart = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+2);
    // --------------------
    name.Form("hFadcNpesPart");
    title.Form("%s;Npes [#];Entries [#]", name.Data());
    hFadcNpesPart = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+2);
    // --------------------
    name.Form("hFadcNchargesBunchPart");
    title.Form("%s;Ncharges [pC];Entries [#]", name.Data());
    hFadcNchargesBunchPart = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+3);
    // --------------------
    name.Form("hFadcNpesBunchPart");
    title.Form("%s;Npes [#];Entries [#]", name.Data());
    hFadcNpesBunchPart = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+3);
    // ------------------------------
    name.Form("hAtmMaxChargePart");
    title.Form("%s;Max Charge [pC];Entries [#]", name.Data());
    hAtmMaxChargePart = InitHist(name.Data(), title.Data(), nqmin, nqmax, color);
    // --------------------
    name.Form("hAtmMaxPePart");
    title.Form("%s;Max P.E. [p.e.];Entries [#]", name.Data());
    hAtmMaxPePart = InitHist(name.Data(), title.Data(), npmin, npmax, color);

    // ------------------------------
    name.Form("hAtmNhits");
    title.Form("%s;Nhits [#];Entries [#]", name.Data());
    hAtmNhits = InitHist(name.Data(), title.Data(), nhmin, nhmax, color);
    // --------------------
    name.Form("hAtmNcharges");
    title.Form("%s;Ncharges [pC];Entries [#]", name.Data());
    hAtmNcharges = InitHist(name.Data(), title.Data(), nqmin, nqmax, color);
    // --------------------
    name.Form("hAtmNpes");
    title.Form("%s;Npes [#];Entries [#]", name.Data());
    hAtmNpes = InitHist(name.Data(), title.Data(), npmin, npmax, color);
    // --------------------
    name.Form("hFadcNhits");
    title.Form("%s;Nhits [#];Entries [#]", name.Data());
    hFadcNhits = InitHist(name.Data(), title.Data(), nhmin, nhmax, color+2);
    // --------------------
    name.Form("hFadcNcharges");
    title.Form("%s;Ncharges [pC];Entries [#]", name.Data());
    hFadcNcharges = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+2);
    // --------------------
    name.Form("hFadcNpes");
    title.Form("%s;Npes [#];Entries [#]", name.Data());
    hFadcNpes = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+2);
    // --------------------
    name.Form("hFadcNchargesBunch");
    title.Form("%s;Ncharges [pC];Entries [#]", name.Data());
    hFadcNchargesBunch = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+3);
    // --------------------
    name.Form("hFadcNpesBunch");
    title.Form("%s;Npes [#];Entries [#]", name.Data());
    hFadcNpesBunch = InitHist(name.Data(), title.Data(), nqmin, nqmax, color+3);
    // ------------------------------
    name.Form("hAtmMaxCharge");
    title.Form("%s;Max Charge [pC];Entries [#]", name.Data());
    hAtmMaxCharge = InitHist(name.Data(), title.Data(), nqmin, nqmax, color);
    // --------------------
    name.Form("hAtmMaxPe");
    title.Form("%s;Max P.E. [p.e.];Entries [#]", name.Data());
    hAtmMaxPe = InitHist(name.Data(), title.Data(), npmin, npmax, color);
    
    IsHistInitialized = kTRUE;
    return ;
}

// __________________________________________________
void Unpacker::RenameHists()
{
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(0);

    TString title;
    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        Int_t pmt  = GetAtmchPmt(iatm);
        Int_t win  = GetAtmchWindow(iatm);
        Int_t mask = GetAtmchMask(iatm);
        title.Form("ATM:%4d PMT:%4d WIN:%4d (MASK:%d)", iatm, pmt, win, mask);
        for (Int_t itype = 0; itype < kNtype; itype++) {
            hQdcPed[iatm][itype]   ->SetTitle("[QdcPed] " + title);
            hTdcPed[iatm][itype]   ->SetTitle("[TdcPed] " + title);
            hQdcPedRms[iatm][itype]->SetTitle("[QdcRms] " + title);
            hTdcPedRms[iatm][itype]->SetTitle("[TdcRms] " + title);
            hAtmPe[iatm][itype]    ->SetTitle("[PE] "     + title);
            hAtmCharge[iatm][itype]->SetTitle("[Charge] " + title);
            hAtmTime[iatm][itype]  ->SetTitle("[Time] "   + title);
        }
    }
    IsHistRenamed = kTRUE;
    return;
}

// __________________________________________________
void Unpacker::AddHists()
{

    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(0);

    fprintf(stderr, "[%s]\tAdd Histograms\n", __FUNCTION__);
    
    fRunNum          = fUnpack->GetRunNum();
    fSpillNum        = fUnpack->GetSpillNum();
    fTime            = (Int_t)fUnpack->GetTime();

    fAtmNhits        = hAtmNhitsPart->GetMean();
    fAtmNhitsRms     = hAtmNhitsPart->GetRMS();
    fAtmNcharges     = hAtmNchargesPart->GetMean();
    fAtmNchargesRms  = hAtmNchargesPart->GetRMS();
    fAtmNpes         = hAtmNpesPart->GetMean();
    fAtmNpesRms      = hAtmNpesPart->GetRMS();

    fFadcNhits       = hFadcNhitsPart->GetMean();
    fFadcNhitsRms    = hFadcNhitsPart->GetRMS();
    fFadcNcharges    = hFadcNchargesPart->GetMean();
    fFadcNchargesRms = hFadcNchargesPart->GetRMS();
    fFadcNpes        = hFadcNpesPart->GetMean();
    fFadcNpesRms     = hFadcNpesPart->GetRMS();
    
    fAtmMaxCharge    = hAtmMaxCharge->GetMean();
    fAtmMaxChargeRms = hAtmMaxCharge->GetRMS();
    fAtmMaxPe        = hAtmMaxPe->GetMean();
    fAtmMaxPeRms     = hAtmMaxPe->GetRMS();
    
    hAtmNhits->Add(hAtmNhitsPart);
    hAtmNcharges->Add(hAtmNchargesPart);
    hAtmNpes->Add(hAtmNpesPart);

    hFadcNhits->Add(hFadcNhitsPart);
    hFadcNcharges->Add(hFadcNchargesPart);
    hFadcNpes->Add(hFadcNpesPart);
    hFadcNchargesBunch->Add(hFadcNchargesBunchPart);
    hFadcNpesBunch->Add(hFadcNpesBunchPart);

    hAtmMaxCharge->Add(hAtmMaxChargePart);
    hAtmMaxPe->Add(hAtmMaxPePart);

    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        for (Int_t ibuf = 0; ibuf < kNtype; ibuf++) {
            fQdc0Mean[iatm][ibuf]     = hQdcPedPart[iatm][ibuf]->GetMean();
            fQdc0MeanRms[iatm][ibuf]  = hQdcPedPart[iatm][ibuf]->GetRMS();
            fQdc0Rms[iatm][ibuf]      = hQdcPedRmsPart[iatm][ibuf]->GetMean();
            fQdc0RmsRms[iatm][ibuf]   = hQdcPedRmsPart[iatm][ibuf]->GetRMS();
            // -----
            fTdc0Mean[iatm][ibuf]     = hTdcPedPart[iatm][ibuf]->GetMean();
            fTdc0MeanRms[iatm][ibuf]  = hTdcPedPart[iatm][ibuf]->GetRMS();
            fTdc0Rms[iatm][ibuf]      = hTdcPedRmsPart[iatm][ibuf]->GetMean();
            fTdc0RmsRms[iatm][ibuf]   = hTdcPedRmsPart[iatm][ibuf]->GetRMS();
            // -----
            fAtmPe[iatm][ibuf]        = hAtmPe[iatm][ibuf]->GetMean();
            fAtmPeRms[iatm][ibuf]     = hAtmPe[iatm][ibuf]->GetRMS();
            fAtmCharge[iatm][ibuf]    = hAtmCharge[iatm][ibuf]->GetMean();
            fAtmChargeRms[iatm][ibuf] = hAtmCharge[iatm][ibuf]->GetRMS();
            fAtmTime[iatm][ibuf]      = hAtmTime[iatm][ibuf]->GetMean();
            fAtmTimeRms[iatm][ibuf]   = hAtmTime[iatm][ibuf]->GetRMS();
            // --------------------
            hQdcPed[iatm][ibuf]->Add(hQdcPedPart[iatm][ibuf]);
            hQdcPedRms[iatm][ibuf]->Add(hQdcPedRmsPart[iatm][ibuf]);
            hTdcPed[iatm][ibuf]->Add(hTdcPedPart[iatm][ibuf]);
            hTdcPedRms[iatm][ibuf]->Add(hTdcPedRmsPart[iatm][ibuf]);
            hAtmPe[iatm][ibuf]->Add(hAtmPePart[iatm][ibuf]);
            hAtmCharge[iatm][ibuf]->Add(hAtmChargePart[iatm][ibuf]);
            hAtmTime[iatm][ibuf]->Add(hAtmTimePart[iatm][ibuf]);
            // --------------------
            hQdcPedPart[iatm][ibuf]->Reset();
            hQdcPedRmsPart[iatm][ibuf]->Reset();
            hTdcPedPart[iatm][ibuf]->Reset();
            hTdcPedRmsPart[iatm][ibuf]->Reset();
            hAtmPePart[iatm][ibuf]->Reset();
            hAtmChargePart[iatm][ibuf]->Reset();
            hAtmTimePart[iatm][ibuf]->Reset();
        }
    }

    fHistory->Fill();
#ifdef DEBUG1
    Int_t n_hAtmNhitsPart     = (Int_t)hAtmNhitsPart->GetEntries();
    Int_t n_hAtmNchargesPart  = (Int_t)hAtmNchargesPart->GetEntries();
    Int_t n_hAtmNpesPart      = (Int_t)hAtmNpesPart->GetEntries();
    Int_t n_hFadcNhitsPart    = (Int_t)hFadcNhitsPart->GetEntries();
    Int_t n_hFadcNchargesPart = (Int_t)hFadcNchargesPart->GetEntries();
    Int_t n_hFadcNpesPart     = (Int_t)hFadcNpesPart->GetEntries();

    Int_t n_hAtmNhits     = (Int_t)hAtmNhits->GetEntries();
    Int_t n_hAtmNcharges  = (Int_t)hAtmNcharges->GetEntries();
    Int_t n_hAtmNpes      = (Int_t)hAtmNpes->GetEntries();
    Int_t n_hFadcNhits    = (Int_t)hFadcNhits->GetEntries();
    Int_t n_hFadcNcharges = (Int_t)hFadcNcharges->GetEntries();
    Int_t n_hFadcNpes     = (Int_t)hFadcNpes->GetEntries();

    
    fprintf(stdout, "DEBUG:\t[%s]\tAdd  :\thAtmNhits:     %4d ==> [total]: %4d\n",
            __FUNCTION__, n_hAtmNhitsPart, n_hAtmNhits);
    fprintf(stdout, "DEBUG:\t[%s]\tAdd  :\thAtmNcharges:  %4d ==> [total]: %4d\n",
            __FUNCTION__, n_hAtmNchargesPart, n_hAtmNcharges);
    fprintf(stdout, "DEBUG:\t[%s]\tAdd  :\thAtmNpes:      %4d ==> [total]: %4d\n",
            __FUNCTION__, n_hAtmNpesPart, n_hAtmNpes);
    fprintf(stdout, "DEBUG:\t[%s]\tAdd  :\thFadcNhits:    %4d ==> [total]: %4d\n",
            __FUNCTION__, n_hFadcNhitsPart, n_hFadcNhits);
    fprintf(stdout, "DEBUG:\t[%s]\tAdd  :\thFadcNcharges: %4d ==> [total]: %4d\n",
            __FUNCTION__, n_hFadcNchargesPart, n_hFadcNcharges);
    fprintf(stdout, "DEBUG:\t[%s]\tAdd  :\thFadcNpes:     %4d ==> [total]: %4d\n",
            __FUNCTION__, n_hFadcNpesPart, n_hFadcNpes);
#endif
    hAtmNhitsPart->Reset();
    hAtmNchargesPart->Reset();
    hAtmNpesPart->Reset();
    
    hFadcNhitsPart->Reset();
    hFadcNchargesPart->Reset();
    hFadcNpesPart->Reset();
    hFadcNchargesBunchPart->Reset();
    hFadcNpesBunchPart->Reset();

    hAtmMaxChargePart->Reset();
    hAtmMaxPePart->Reset();
    
#ifdef DEBUG1    
    n_hAtmNhitsPart     = (Int_t)hAtmNhitsPart->GetEntries();
    n_hAtmNchargesPart  = (Int_t)hAtmNchargesPart->GetEntries();
    n_hAtmNpesPart      = (Int_t)hAtmNpesPart->GetEntries();
    n_hFadcNhitsPart    = (Int_t)hFadcNhitsPart->GetEntries();
    n_hFadcNchargesPart = (Int_t)hFadcNchargesPart->GetEntries();
    n_hFadcNpesPart     = (Int_t)hFadcNpesPart->GetEntries();
    
    fprintf(stdout, "DEBUG:\t[%s]\tReset:\thAtmNhits:     %4d ==> %4d\n",
            __FUNCTION__, n_hAtmNhitsPart, n_hAtmNhits);
    fprintf(stdout, "DEBUG:\t[%s]\tReset:\thAtmNcharges:  %4d ==> %4d\n",
            __FUNCTION__, n_hAtmNchargesPart, n_hAtmNcharges);
    fprintf(stdout, "DEBUG:\t[%s]\tReset:\thAtmNpes:      %4d ==> %4d\n",
            __FUNCTION__, n_hAtmNpesPart, n_hAtmNpes);
    fprintf(stdout, "DEBUG:\t[%s]\tReset:\thFadcNhits:    %4d ==> %4d\n",
            __FUNCTION__, n_hFadcNhitsPart, n_hFadcNhits);
    fprintf(stdout, "DEBUG:\t[%s]\tReset:\thFadcNcharges: %4d ==> %4d\n",
            __FUNCTION__, n_hFadcNchargesPart, n_hFadcNcharges);
    fprintf(stdout, "DEBUG:\t[%s]\tReset:\thFadcNpes:     %4d ==> %4d\n",
            __FUNCTION__, n_hFadcNpesPart, n_hFadcNpes);
#endif
    
    return;
}


// __________________________________________________
void Unpacker::WriteHists()
{

    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(0);


    AddHists();
    
    for (Int_t iatm = 0; iatm < kNatmCh; iatm ++) {
        for (Int_t itype = 0; itype < kNtype; itype++) {
            hQdcPed[iatm][itype]->Write();
            hTdcPed[iatm][itype]->Write();
            hQdcPedRms[iatm][itype]->Write();
            hTdcPedRms[iatm][itype]->Write();
            hAtmPe[iatm][itype]->Write();
            hAtmCharge[iatm][itype]->Write();
            hAtmTime[iatm][itype]->Write();
        }
    }

    hAtmNhits->Write();
    hAtmNcharges->Write();
    hAtmNpes->Write();
    
    hFadcNhits->Write();
    hFadcNcharges->Write();
    hFadcNpes->Write();
    hFadcNchargesBunch->Write();
    hFadcNpesBunch->Write();

    hAtmMaxCharge->Write();
    hAtmMaxPe->Write();
    
    return;
}

// __________________________________________________
void Unpacker::InitFadc()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    // Clear all local variables for FADC
#ifdef DEBUG_IFADC    
    Int_t b1  = fAtmNtrgnum.size();
    Int_t b2  = fAtmNtrgnum.size();
    Int_t b3  = fRawPmtsum.size();
    Int_t b4  = fRawHitsum.size();
    Int_t b5  = fFadcPmtsum.size();
    Int_t b6  = fFadcHitsum.size();
    Int_t b7  = fFadcNsec.size();
    Int_t b8  = fFadcHit.size();
    Int_t b9  = fFadcHeight.size();
    Int_t b10 = fFadcCharge.size();
    Int_t b11 = fFadcPe.size();
#endif

    fAtmNtrgnum.clear();
    fRawPmtsum.clear();
    fRawHitsum.clear();
    fFadcPmtsum.clear();
    fFadcHitsum.clear();
    fFadcNsec.clear();
    fFadcHit.clear();
    fFadcHeight.clear();
    fFadcCharge.clear();
    fFadcPe.clear();

    for (Int_t ich = 0; ich < kFadcNchMax; ich++) {
        fFadcRaw[ich].clear();
        fFadcMv[ich].clear();
    }
    
#ifdef DEBUG_IFADC
    Int_t a1  = fAtmNtrgnum.size();
    Int_t a2  = fAtmNtrgnum.size();
    Int_t a3  = fRawPmtsum.size();
    Int_t a4  = fRawHitsum.size();
    Int_t a5  = fFadcPmtsum.size();
    Int_t a6  = fFadcHitsum.size();
    Int_t a7  = fFadcNsec.size();
    Int_t a8  = fFadcHit.size();
    Int_t a9  = fFadcHeight.size();
    Int_t a10 = fFadcCharge.size();
    Int_t a11 = fFadcPe.size();

    LogDebug(Form("[%s] DEBUG_IFADC:\tfAtmNtrgnum:%4d ==> %4d\n", __FUNCTION__, b1 , a1));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfAtmNtrgnum:%4d ==> %4d\n", __FUNCTION__, b2 , a2));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfRawHitsum :%4d ==> %4d\n", __FUNCTION__, b3 , a3));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfRawPmtsum :%4d ==> %4d\n", __FUNCTION__, b4 , a4));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcHitsum:%4d ==> %4d\n", __FUNCTION__, b5 , a5));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcPmtsum:%4d ==> %4d\n", __FUNCTION__, b6 , a6));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcNsec  :%4d ==> %4d\n", __FUNCTION__, b7 , a7));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcHit   :%4d ==> %4d\n", __FUNCTION__, b8 , a8));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcHeight:%4d ==> %4d\n", __FUNCTION__, b9 , a9));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcCharge:%4d ==> %4d\n", __FUNCTION__, b10, a10));
    LogDebug(Form("[%s] DEBUG_IFADC:\tfFadcPe    :%4d ==> %4d\n", __FUNCTION__, b11, a11));
#endif

    return;
}


// __________________________________________________
void Unpacker::UnpackFadc(const Int_t &nch) 
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    fLog.Form("FADC Nch:%d\n", nch);
    PrintDebug(1);

    const Int_t header_offset = 4;
    Int_t event_size = CheckFadcHeaders();  // event_size includes kHeaderSize
    Int_t nwords_per_ch = (Int_t)( (event_size-header_offset) / nch );
    Int_t word;
    Int_t adc;

    Int_t nsamp = 2 * nwords_per_ch;
    fUnpack->SetFadcNsamp(nsamp);
#ifdef DEBUG_UFADC
    printf("DEBUG_UFADC:\t[%s]\tEvent Size: %d\n",  __FUNCTION__, event_size);
    printf("DEBUG_UFADC:\t[%s]\t# of words / channel: %d\n",  __FUNCTION__, nwords_per_ch);
    printf("DEBUG_UFADC:\t[%s]\tnsamp     : %d\n", __FUNCTION__, nsamp);    
#endif
    
    // __________ PMTSUM (FADC CH:0) process part __________
    fLog.Form("Unpack FADC 0 (PMTSUM)\n");
    PrintDebug(1);
    for(int n = 0; n < nwords_per_ch; n++) {
        Int_t ibuffer = n + header_offset;
        word = fMizuRawData.fadc.FADC[ibuffer];
        adc = (word & 0x0FFF) - kFadcOffset;
        fRawPmtsum.push_back(adc);
#ifdef DEBUG_UFADC
        printf("DEBUG_UFADC:\t[%s]\tFADC[%4d]E\tPMTSUM[%4d/%4d]\tADC:%4d\n",  __FUNCTION__, ibuffer, 2*n, nsamp, adc);
#endif
        // ----------
        adc = ((word>>16) & 0x0FFF) - kFadcOffset;
        fRawPmtsum.push_back(adc);
#ifdef DEBUG_UFADC
        printf("DEBUG_UFADC:\t[%s]\tFADC[%4d]O\tPMTSUM[%4d/%4d]\tADC:%4d\n", __FUNCTION__, ibuffer, 2*n+1, nsamp, adc);
#endif
    }
    
    if( nch<2 ) return;

    // __________ HITSUM (FADC CH:1) process part __________
    fLog.Form("Unpack FADC 1 (HITSUM)\n");
    PrintDebug(1);
    for(int n=0; n<nwords_per_ch; n++) {
        Int_t ibuffer = n + header_offset + nwords_per_ch;  // add header_offset on 2012/09/18
        word = fMizuRawData.fadc.FADC[ibuffer];
        adc = (word & 0x0FFF) - kFadcOffset;
        fRawHitsum.push_back(adc);
#ifdef DEBUG_UFADC
        printf("DEBUG_UFADC:\t[%s]\tFADC[%4d]E\tHITSUM[%4d/%4d]\tADC:%4d\n", __FUNCTION__, ibuffer, 2*n, nsamp, adc);
#endif
        // ----------
        adc = ((word>>16) & 0x0FFF) - kFadcOffset;
        fRawHitsum.push_back(adc);
#ifdef DEBUG_UFADC
        printf("DEBUG_UFADC:\t[%s]\tFADC[%4d]O\tHITSUM[%4d/%4d]\tADC:%4d\n", __FUNCTION__, ibuffer, 2*n+1, nsamp, adc);
#endif
    }
    return;

}

// __________________________________________________
void Unpacker::UnpackFadc2()
{
    // FADC data
    // CH0 : FADC[HeaderSize + 0 * Nwords] --> Nwords --> FADC[HeaderSize + 1 * Nwords - 1]
    // CH1 : FADC[HeaderSize + 1 * Nwords] --> Nwords --> FADC[HeaderSize + 2 * Nwords - 1]
    // CH2 : FADC[HeaderSize + 2 * Nwords] --> Nwords --> FADC[HeaderSize + 3 * Nwords - 1]
    // CH3 : ...
    // Remarks:
    //    1. HeaderSize = 4, fixed, difined as golbal variable
    //    2. Nwords = number of word(=data) per FADC ch
    //    3. There are 2 sampling point data per word
    //    4. Number of FADCch used is calculated in CheckFadcHeaders()
    //    5. Last few words in each data seems to be not good ==> cut off last few points in ConvertFadc()
    
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Int_t event_size = CheckFadcHeaders();  // returns event size in header
    Int_t nch = GetFadcNch();  // fFadcNch is set in CheckFadcHeaders()
    Int_t nwords = (Int_t)( (event_size - kHeaderSize) / nch);    // number of words per FADCch
    Int_t nsamp = 2 * nwords;    // 2 sampling data / word
    fUnpack->SetFadcNsamp(nsamp);
    
#ifdef DEBUG_UFADC
    fLog.Form("[%s] DEBUG_UFADC", __FUNCTION__);
    fLogTmp.Form("\tEvent Size: %d", event_size);
    fLog += fLogTmp;
    fLogTmp.Form(" | Nch: %d",nch);
    fLog += fLogTmp;
    fLogTmp.Form(" | Nword: %d", nwords);
    fLog += fLogTmp;
    fLogTmp.Form(" | Nsamp : %d", nsamp);
    fLog += fLogTmp;
    LogDebug();
#endif
    
    Int_t word = 0, eve = 0, odd = 0;
    for (Int_t ich = 0; ich < nch; ich++) {
        fLog.Form("[%s] Unpack FADC[%d/%d]", __FUNCTION__, ich, nch);
        LogDebug();
        for (Int_t iword = 0; iword < nwords; iword++) {
            Int_t ibuffer = kHeaderSize + ich * nwords + iword;
            word = fMizuRawData.fadc.FADC[ibuffer];
            eve = (word & 0x0FFF) - kFadcOffset;
            odd = ((word >> 16) & 0x0FFF) - kFadcOffset;
            //fFadcRaw[ich].push_back(word);
            fFadcRaw[ich].push_back(eve);
            fFadcRaw[ich].push_back(odd);
            
#ifdef DEBUG_UFADC
            if (iword % 500 == 0 || iword > nwords - 5) {
                fLog.Form("[%s] DEBUG_UFADC", __FUNCTION__);
                fLogTmp.Form("\tfFadcRaw[%d/%d]", ich, nch);
                fLog += fLogTmp;
                fLogTmp.Form(" | F[%4d/%4d]", ibuffer, event_size);
                fLog += fLogTmp;
                fLogTmp.Form(" | W[%4d/%4d]:%d (0x%08x)", iword, nwords, word, word);
                fLog += fLogTmp;
                fLogTmp.Form(" | E:%4d (0x%04x)", eve, eve);
                fLog += fLogTmp;
                fLogTmp.Form(" | O:%4d (0x%04x)", odd, odd);
                fLog += fLogTmp;
                LogDebug();
            }
#endif
            if (ich == 0) {
                fRawPmtsum.push_back(eve);
                fRawPmtsum.push_back(odd);
            }
            else if (ich == 1) {
                fRawHitsum.push_back(eve);
                fRawHitsum.push_back(odd);
            }
            else {
                // pass
                ;
            }
        }
    }
    return;
}

// ________________________________________________________________________________
void Unpacker::InitAtmPedestal()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    // __________ Initialize Pedestal Buffer __________
    fPedEvents = 0;
    IsPedUpdated = kFALSE;

    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        for (Int_t ibuf = 0; ibuf < kNtype; ibuf++) {
            fPedQDC[iatm][ibuf]=0;
            fPedTDC[iatm][ibuf]=0;
            fQdcPedTmp[iatm][ibuf].clear();
            fTdcPedTmp[iatm][ibuf].clear();
        }
    }
    return;
}

// ________________________________________________________________________________
void Unpacker::StorePedestal()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    // __________ Obsolete ??? __________
    PrintDebug(0, "StorePedestal");
    for(Int_t i = 0; i < kNatmCh; i++) {
        for(Int_t j = 0; j < 2; j++) {
            fPedQDCLast[i][j] = fPedQDC[i][j];
            fPedTDCLast[i][j] = fPedTDCLast[i][j];
        }
    }
    return;
}


// ________________________________________________________________________________
void Unpacker::GetAtmPedestal()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    // __________ If pedestal data was not taken properly, use previous value __________
    if(fPedEvents == 0 ) {
        LogWarning(Form("[%s] Pedestal events not stored.", __FUNCTION__));
        LogWarning(Form("[%s] ==> Use previous pedestal values.", __FUNCTION__));

        for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
            for (Int_t ibuf = 0; ibuf < kNtype; ibuf++) {
#ifdef DEBUG_PEDESTAL
                // Debugged using RUN0420772 on 2012/05/11
                // RUN0420772 has (somehow) fPedEvents=0 entry (i.e. no pedestal data taken)
                // Checked that last stored pedestal value was called properly
                if (iatm == 0) {
                    // change "i" to check different channel if necessary
                    // (or comment it out to check whole channel)
                    LogDebug(Form("[%s] DEBUG_PED:\t[%3d][%d]\t%d\n", __FUNCTION__, iatm, ibuf, fPedEvents));
                    LogDebug(Form("[%s] DEBUG_PED:\tBefore\tPedQdc:%4.2f\t<== Last:%4.2f\n",
                                  __FUNCTION__, fPedQDC[iatm][ibuf], fPedQDCLast[iatm][ibuf]));
                    LogDebug(Form("[%s] DEBUG_PED:\tBefore\tPedTdc:%4.2f\t<== Last:%4.2f\n",
                                  __FUNCTION__, fPedTDC[iatm][ibuf], fPedTDCLast[iatm][ibuf]));
                }
#endif
                fPedQDC[iatm][ibuf] = fPedQDCLast[iatm][ibuf];
                fPedTDC[iatm][ibuf] = fPedTDCLast[iatm][ibuf];
#ifdef DEBUG_PEDESTAL
                
                if (iatm == 0) {
                    LogDebug(Form("[%s] DEBUG_PED:\t[%3d][%d]\t%d\n", __FUNCTION__, iatm, ibuf, fPedEvents));
                    LogDebug(Form("[%s] DEBUG_PED:\tAfter \tPedQdc:%4.2f\t<== Last:%4.2f\n",
                                  __FUNCTION__, fPedQDC[iatm][ibuf], fPedQDCLast[iatm][ibuf]));
                    LogDebug(Form("[%s] DEBUG_PED:\tAfter \tPedTdc:%4.2f\t<== Last:%4.2f\n",
                                  __FUNCTION__, fPedTDC[iatm][ibuf], fPedTDCLast[iatm][ibuf]));
                }
#endif
            }
        }
        IsPedUpdated = kFALSE;
    }
    
    else {
        for(Int_t iatm = 0; iatm < kNatmCh; iatm++) {
            for(Int_t ibuf = 0; ibuf < 2; ibuf++) {
                // __________ Calculate Average of Pedestal for fPedEvents __________
#ifdef DEBUG_PEDESTAL
                if (iatm == 0) {
                    LogDebug(Form("[%s] DEBUG_PED:\t[%3d][%d]\t%d\n",__FUNCTION__, iatm, ibuf, fPedEvents));
                    LogDebug(Form("[%s] DEBUG_PED:\tBefore\tPedQdc:%8.2f / %d\t| Stored:%8.2f\n",
                                  __FUNCTION__, fPedQDC[iatm][ibuf], fPedEvents, fPedQDCLast[iatm][ibuf]));
                    LogDebug(Form("[%s] DEBUG_PED:\tBefore\tPedTdc:%8.2f / %d\t| Stored:%8.2f\n",
                                  __FUNCTION__, fPedTDC[iatm][ibuf], fPedEvents, fPedTDCLast[iatm][ibuf]));
                }
#endif
                // (Double_t)/(Int_t) ==> (Double_t)
                fPedQDC[iatm][ibuf] = fPedQDC[iatm][ibuf]/fPedEvents;
                fPedTDC[iatm][ibuf] = fPedTDC[iatm][ibuf]/fPedEvents;
                // __________ Store this value in buffer __________
                fPedQDCLast[iatm][ibuf] = fPedQDC[iatm][ibuf];
                fPedTDCLast[iatm][ibuf] = fPedTDC[iatm][ibuf];
                
                // ---------- Fill Histograms --------------------
                Int_t nqdc0 = (Int_t)fQdcPedTmp[iatm][ibuf].size();
                Int_t ntdc0 = (Int_t)fTdcPedTmp[iatm][ibuf].size();

                Double_t qdc0 = 0, tdc0 = 0;
                for (Int_t iqdc = 0; iqdc < nqdc0; iqdc++) {
                    qdc0 += fQdcPedTmp[iatm][ibuf].at(iqdc);
                    tdc0 += fTdcPedTmp[iatm][ibuf].at(iqdc);;
                }
                qdc0 /= nqdc0;
                tdc0 /= ntdc0;

                Double_t qdc0_rms = 0, tdc0_rms = 0;
                for (Int_t iqdc = 0; iqdc < nqdc0; iqdc++) {
                    Double_t q = fQdcPedTmp[iatm][ibuf].at(iqdc);
                    Double_t t = fTdcPedTmp[iatm][ibuf].at(iqdc);
                    qdc0_rms += (q - qdc0) * (q - qdc0);
                    tdc0_rms += (t - tdc0) * (t - tdc0);
                }
                qdc0_rms /= nqdc0; qdc0_rms = sqrt(qdc0_rms);
                tdc0_rms /= ntdc0; tdc0_rms = sqrt(tdc0_rms);
#ifdef DEBUG_PEDESTAL
                LogDebug(Form("[%s] DEBUG_PED:\tNqdc0 : %4d\tNtdc0 : %4d\n", __FUNCTION__, nqdc0, ntdc0));
                LogDebug(Form("[%s] DEBUG_PED:\t<qdc0> : %8.3f +/- %8.3f\n", __FUNCTION__, qdc0, qdc0_rms));
                LogDebug(Form("[%s] DEBUG_PED:\t<tdc0> : %8.3f +/- %8.3f\n", __FUNCTION__, tdc0, tdc0_rms));
#endif
                hQdcPedPart[iatm][ibuf]->Fill(qdc0);
                hQdcPedRmsPart[iatm][ibuf]->Fill(qdc0_rms);
                hTdcPedPart[iatm][ibuf]->Fill(tdc0);
                hTdcPedRmsPart[iatm][ibuf]->Fill(tdc0_rms);
                
#ifdef DEBUG_PEDESTAL
                if (iatm == 0) {
                    LogDebug(Form("[%s] DEBUG_PED:\tAfter \tPedQdc:%8.2f\t==> Stored to Last:%8.2f\n",
                                  __FUNCTION__, fPedQDC[iatm][ibuf], fPedQDCLast[iatm][ibuf]));
                    LogDebug(Form("[%s] DEBUG_PED:\tAfter \tPedTdc:%8.2f\t==> Stored to Last:%8.2f\n",
                                  __FUNCTION__, fPedTDC[iatm][ibuf], fPedTDCLast[iatm][ibuf]));
                }
#endif
            }
        }
        IsPedUpdated = kTRUE;
    }
    
#ifdef DEBUG_PEDESTAL
    LogDebug(Form("[%s] DEBUG_PED:\tIsPedUpdated:%d", __FUNCTION__, IsPedUpdated));
#endif
    if ( !IsPedUpdated ) {
        LogError(Form("[%s] Pedestal data was not updated", __FUNCTION__));
    }
    
    return;
}

// __________________________________________________
void Unpacker::ConvertATM()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

#if defined(DEBUG_CATM)
    fLog.Form("[%s] DEBUG_CATM:\tIsPedUpdated:%4d", __FUNCTION__, IsPedUpdated);
    LogDebug();
    fLog.Form("[%s] DEBUG_CATM:\tfPedEvents  :%4d", __FUNCTION__, fPedEvents);
    LogDebug();
#endif
    
    if( fPedEvents == 0 || !IsPedUpdated) {
        fLog.Form("[%s] Pedestal mean not calculated --> Skip converting.", __FUNCTION__);
        return;
    }

    Double_t npes     = 0;
    Double_t nqdcs    = 0;
    Double_t ncharges = 0;

    Double_t max_pe     = -100;
    Double_t max_charge = -100;

    Int_t nhits = fUnpack->GetAtmNhits();

    T2KWCHit *hit;
    for(Int_t ihit = 0; ihit < nhits; ihit++) {
        hit = fUnpack->GetHit(ihit);

        Int_t ch   = hit->GetCh();
        Int_t type = hit->GetType();
        
        Double_t qdc0 = fPedQDC[ch][type];
        Double_t tdc0 = fPedTDC[ch][type];

        Double_t qdc1 = hit->GetQdc1();
        Double_t tdc1 = hit->GetTdc1();

        Double_t qdc = qdc1 - qdc0;
        Double_t tdc = tdc1 - tdc0;
        
        Double_t charge = GetAtmCharge(ch, qdc);
        Double_t pe     = GetAtmPe(ch, qdc);
        Double_t time   = GetAtmNsec(ch, tdc);

        Double_t gain = GetAtmchGain(ch);
        Int_t    mask = GetAtmchMask(ch);
        Int_t    hv   = GetAtmchHv(ch);

        if (mask == 0 && pe > max_pe) max_pe = pe;
        if (mask == 0 && charge > max_charge) max_charge = charge;
        
        // Calculate Total PEs (exclude Masked or Gain=0 PMTs)
        if (mask == 0) {
            nqdcs    += qdc;
            ncharges += charge;
            npes     += pe;
        }

        hit->SetQdc0(qdc0);
        hit->SetTdc0(tdc0);
        hit->SetCharge(charge);
        hit->SetPe(pe);
        hit->SetTime(time);
        hit->SetGain(gain);
        hit->SetMask(mask);
        hit->SetHv(hv);

        hAtmCharge[ch][type]->Fill(charge);
        hAtmPe[ch][type]->Fill(pe);
        hAtmTime[ch][type]->Fill(time);

        if (fVerboseLevel > 1) PrintHitInfo(ihit, nhits);
        
    } // end of hit loop

    fUnpack->SetAtmNcharges(ncharges);
    fUnpack->SetAtmNpes(npes);
    fUnpack->SetAtmMaxCharge(max_charge);
    fUnpack->SetAtmMaxPe(max_pe);

    if (fVerboseLevel > 0) PrintHitSummary();

    return;
}


// __________________________________________________
void Unpacker::ConvertFadc()
{
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    PrintDebug(0);

    // cut last 20 points when converting to mV
    Int_t nsamp = fUnpack->GetFadcNsamp() - 20;
    fUnpack->SetFadcNsamp(nsamp);    // Re-set Nsamp
    
    // Calculate pedestals for HITSUM and PMTSUM
    // pedestal = average of first 100 points (default)
    SetFadcPedestal();

    Double_t raw_pmtsum0 = fUnpack->GetRawPmtsum0();
    Double_t raw_hitsum0 = fUnpack->GetRawHitsum0();
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC RawPmtsum0 : %f\n", __FUNCTION__, raw_pmtsum0);
    LogDebug();
    fLog.Form("[%s] DEBUG_CFADC RawHitsum0 : %f\n", __FUNCTION__, raw_hitsum0);
    LogDebug();
#endif

    // Prepare data of HITSUM and PMTSUM after substacting pedestal
    for (Int_t isamp = 0; isamp < nsamp; isamp++) {
        Double_t ihitsum = fRawHitsum.at(isamp) - raw_hitsum0;  // [count]
        Double_t ipmtsum = fRawPmtsum.at(isamp) - raw_pmtsum0;  // [count]
        ihitsum = CalcFadcMv(ihitsum);  // [count] --> [mV]
        ipmtsum = CalcFadcMv(ipmtsum);  // [count] --> [mV]
        fFadcHitsum.push_back(ihitsum);
        fFadcPmtsum.push_back(ipmtsum);

#ifdef DEBUG_CFADC
        fLog.Form("[%s] DEBUG_CFADC [%4d/%4d]\tihit:%10.4f mV\tipmt:%10.4f mV\n",
                  __FUNCTION__, isamp, nsamp, ihitsum, ipmtsum);
        LogDebug();
#endif
    }
    
    // Search for HITSUM leading edge.
    // Method:
    // 1. Hit threshold > 3 Hits (=3*15-5 = 40mV)
    // 2. Check the HITSUM value of current sampling point
    // 3. If the value exceeds the threshold, convert that point into time.
    // 4. Record average of post 25pts (=100nsec) value as HITSUM pulse height.
    //    Divide that value by kOneHitPulseHeight to calculate # of hits in that event (=NHits)
    // 5. Integration timing window is set accorting to the above sampling point.
    //    Window set to [-18, 9] (= [-72nsec, 36 nsec])
    //    Record the integrated PMTSUM value as PMTSUM charge.
    // 6. Recored The size of "HITSUM pulse height" vector as # of hits in a spill (=NTRG)

    const Int_t skip = 49;         // 49 * 4ns = 196 ns
    const Int_t hitsumwidth = 40;  // 40 * 4ns = 160 ns
    //const Int_t skip = 100;           // 60 * 4ns = 240 ns
    //const Int_t hitsumwidth = 50;    // 50 * 4ns = 200 ns
    Int_t hitflag = -1;

    Int_t win_start = 0, win_end = 0;
    Int_t itime = 0;
    Double_t icharge = 0, iheight = 0;

#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC Search for HITSUM Leading Edge (ihitsum < %.2f mV)\n", __FUNCTION__, - fFadcHitThrd);
    LogDebug();
#endif
    for (Int_t isamp = 0 ; isamp < nsamp; isamp++) {
        Double_t currhit = fFadcHitsum.at(isamp);
        // Method 1:
        if (currhit < - fFadcHitThrd){
            for (Int_t jsamp = 0; jsamp < hitsumwidth; jsamp++){
                Int_t tsamp = isamp + jsamp + 1;
                if (tsamp >= nsamp) {
#ifdef DEBUG_CFADC
                    fLog.Form("[%s] DEBUG_CFADC [%4d/%4d][%4d]\ttsamp(%d) >= nsamp(%d)\thitflag:%+d\n",
                              __FUNCTION__, isamp, nsamp, tsamp, tsamp, nsamp, hitflag);
                    LogDebug();
#endif
                    return;//break;
                }
                Double_t thitsum = fFadcHitsum.at(tsamp);
                if (thitsum >= - fFadcHitThrd) { hitflag = -1; break; }
                if (jsamp == hitsumwidth - 1) { hitflag = 1; }

#ifdef DEBUG_CFADC
                fLog.Form("[%s] DEBUG_CFADC [%4d/%4d][%4d]\tthit:%8.2f mV\thitflag:%+d\n",
                          __FUNCTION__, isamp, nsamp, tsamp, thitsum, hitflag);
                LogDebug();
#endif
            }

            if (hitflag == 1) {
                // Method 2:
                itime = GetFadcNsec(isamp);     // returned in nsec
                // Method 3:
                win_start = isamp + 6;
                win_end   = isamp + 45;
                if (win_start < 0)   win_start = 0;
                if (win_end > nsamp) win_end = nsamp;
                iheight = GetHitsumAverage(win_start, win_end);  // returned in mV
                // Method 4:
                win_start = isamp - 18;    // = -72ns
                win_end   = isamp +  9;    // = +36 ns
                //win_end   = isamp +  32;    // = +128 ns
                //win_end   = isamp + 57;    //  = +228 ns
                if (win_start < 0)   win_start = 0;
                if (win_end > nsamp) win_end   = nsamp;
                icharge = GetPmtsumIntegral(win_start, win_end); // returned in pC
                
#ifdef DEBUG_CFADC
                Int_t spill = fUnpack->GetSpillNum();
                fprintf(stdout, "DEBUG_CFADC:\t[%s]", __FUNCTION__);
                fprintf(stdout, "\t[%4d]", isamp);
                fprintf(stdout, "\tSpill:%6d\tTiming:%6d nsec", spill, itime);
                fprintf(stdout, "\tHeight:%+8.3f mV", iheight);
                fprintf(stdout, "\tCharge:%+8.3f pC\n", icharge);
#endif
                // ----------
                fFadcNsec.push_back(itime);
                // ----------
                fFadcCharge.push_back(icharge);
                Double_t ipe = CalcFadcPe(icharge);
                fFadcPe.push_back(ipe);
                // ----------
                if (iheight < 0) iheight = 0;
                fFadcHeight.push_back(iheight);
                // Method 6:
                Int_t nhits = (Int_t)(iheight/kHitsumHeight);
                fFadcHit.push_back(nhits);
                isamp += skip; //Jump over this pulse
                hitflag = -1;  //Reset for next pulse
            }
        }
    }

    Int_t ntrg      = (Int_t)fFadcNsec.size();
    Int_t ntrg_tmp0 = (Int_t)fFadcHit.size();
    Int_t ntrg_tmp1 = (Int_t)fFadcHeight.size();
    Int_t ntrg_tmp2 = (Int_t)fFadcCharge.size();
    Int_t ntrg_tmp3 = (Int_t)fFadcPe.size();

    if (ntrg != ntrg_tmp0) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 0 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp1);
    if (ntrg != ntrg_tmp1) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 1 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp1);
    if (ntrg != ntrg_tmp2) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 2 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp2);
    if (ntrg != ntrg_tmp3) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 3 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp3);
    
    //fprintf(stderr, "[%s]:\tNTRG: %d / %d / %d / %d / %d\n", __FUNCTION__, ntrg, ntrg_tmp0, ntrg_tmp1, ntrg_tmp2, ntrg_tmp3);
    //Stop();

    Int_t    nhits    = 0;
    Double_t ncharges = 0;
    Double_t npes     = 0;
    for (Int_t isamp = 0; isamp < ntrg; isamp++) {
        nhits    += fFadcHit.at(isamp);
        ncharges += fFadcCharge.at(isamp);
        npes     += fFadcPe.at(isamp);
        //fprintf(stderr, "DEBUG_TMP:\t[%s]\t[%d/%d]\tH:%d [#]\tQ:%.2f [pC]\tN:%.2f [p.e.]\n", __FUNCTION__, isamp, ntrg, nhits, ncharges, npes);
    }

    fUnpack->SetFadcNtrg(ntrg);
    fUnpack->SetFadcNhits(nhits);
    fUnpack->SetFadcNcharges(ncharges);
    fUnpack->SetFadcNpes(npes);

    //Stop();
#ifdef DEBUG_CFADC
    for (Int_t itrg = 0; itrg < ntrg; itrg++) {
        Int_t    itime   = fFadcNsec.at(itrg);
        Double_t iheight = fFadcHeight.at(itrg);
        Double_t icharge = fFadcCharge.at(itrg);
        Int_t    ihit    = fFadcHit.at(itrg);
        Double_t ipe     = fFadcPe.at(itrg);
        fprintf(stdout, "[%s]", __FUNCTION__);
        fprintf(stdout, "\t[Check][%d/%d]", itrg, ntrg);
        fprintf(stdout, "\tTiming:%6d [ns]", itime);
        fprintf(stdout, "\tHeight:%8.3f [mV]\tHit :%4d [#]", iheight, ihit);
        fprintf(stdout, "\tCharge:%8.3f [pC]\tP.E.:%8.3f [p.e.]\n", icharge, ipe);
    }
#endif
    //Stop();
    return ;
}

// __________________________________________________
void Unpacker::AnalyzeFadc()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    // cut last 20 points when converting to mV
    Int_t nsamp = fUnpack->GetFadcNsamp() - 20;
    fUnpack->SetFadcNsamp(nsamp);    // Re-set Nsamp
    
    // Calculate pedestals for FADC
    // pedestal = average of first 100 points (default)
    // number of pedestal points is set by SetFadcNped(nped)
    CalcFadcPedestal();

    Int_t nch = GetFadcNch();
    Double_t raw0[kFadcNchMax] = {};
    
    raw0[0] = fUnpack->GetRawPmtsum0();
    raw0[1] = fUnpack->GetRawHitsum0();
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC\tRawPmtsum0 : %.2f", __FUNCTION__, raw0[0]);
    LogDebug();
    fLog.Form("[%s] DEBUG_CFADC\tRawHitsum0 : %.2f", __FUNCTION__, raw0[1]);
    LogDebug();
#endif

    // Prepare data of HITSUM and PMTSUM after substacting pedestal
    for (Int_t ich = 0; ich < nch; ich++) {
        for (Int_t isamp = 0; isamp < nsamp; isamp++) {
            Double_t ifadc = fFadcRaw[ich].at(isamp) - raw0[ich];    // [count]
            ifadc = CalcFadcMv(ifadc);  // convert [count] --> [mV]
            fFadcMv[ich].push_back(ifadc);
            if (ich == 0) {
                fFadcPmtsum.push_back(ifadc);
            }
            else if (ich == 1) {
                fFadcHitsum.push_back(ifadc);
            }
            else {
                // pass
                ;
            }
        }
    }
    
    // Search for HITSUM leading edge.
    // Method:
    // 1. Hit threshold > 3 Hits (=3*15-5 = 40mV)
    // 2. Check the HITSUM value of current sampling point
    // 3. If the value exceeds the threshold, convert that point into time.
    // 4. Record average of post 25pts (=100nsec) value as HITSUM pulse height.
    //    Divide that value by kOneHitPulseHeight to calculate # of hits in that event (=NHits)
    // 5. Integration timing window is set accorting to the above sampling point.
    //    Window set to [-18, 9] (= [-72nsec, 36 nsec])
    //    Record the integrated PMTSUM value as PMTSUM charge.
    // 6. Recored The size of "HITSUM pulse height" vector as # of hits in a spill (=NTRG)

    const Int_t skip = 49;         // 49 * 4ns = 196 ns
    const Int_t hitsumwidth = 40;  // 40 * 4ns = 160 ns
    Int_t hitflag = -1;

    Int_t win_start = 0, win_end = 0;
    Int_t itime = 0;
    Double_t icharge = 0, iheight = 0;
    
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC\tSearch for HITSUM Leading Edge (ihitsum < %.2f mV)", __FUNCTION__, - fFadcHitThrd);
    LogDebug();
#endif
    for (Int_t isamp = 0 ; isamp < nsamp; isamp++) {
        Double_t currhit = fFadcHitsum.at(isamp);
        // Method 1:
        if (currhit < - fFadcHitThrd){
            for (Int_t jsamp = 0; jsamp < hitsumwidth; jsamp++){
                Int_t tsamp = isamp + jsamp + 1;
                if (tsamp >= nsamp) {
#ifdef DEBUG_CFADC
                    fLog.Form("[%s] DEBUG_CFADC\t[%4d/%4d][%4d]\ttsamp(%d) >= nsamp(%d)\thitflag:%+d",
                              __FUNCTION__, isamp, nsamp, tsamp, tsamp, nsamp, hitflag);
                    LogDebug();
#endif
                    return;//break;
                }
                Double_t thitsum = fFadcHitsum.at(tsamp);
                if (thitsum >= - fFadcHitThrd) { hitflag = -1; break; }
                if (jsamp == hitsumwidth - 1) { hitflag = 1; }

#ifdef DEBUG_CFADC
                fLog.Form("[%s] DEBUG_CFADC\t[%4d/%4d][%4d]\tthit:%8.2f mV\thitflag:%+d",
                          __FUNCTION__, isamp, nsamp, tsamp, thitsum, hitflag);
                LogDebug();
#endif
            }
            
            if (hitflag == 1) {
                // Method 2:
                itime = GetFadcNsec(isamp);     // returned in nsec
                // Method 3:
                win_start = isamp + 6;
                win_end   = isamp + 45;
                if (win_start < 0)   win_start = 0;
                if (win_end > nsamp) win_end = nsamp;
                iheight = GetHitsumAverage(win_start, win_end);  // returned in mV
                // Method 4:
                win_start = isamp - 18;    // = -72ns
                win_end   = isamp +  9;    // = +36 ns
                //win_end   = isamp +  32;    // = +128 ns
                //win_end   = isamp + 57;    //  = +228 ns
                if (win_start < 0)   win_start = 0;
                if (win_end > nsamp) win_end   = nsamp;
                icharge = GetPmtsumIntegral(win_start, win_end); // returned in pC
                
#ifdef DEBUG_CFADC
                Int_t spill = fUnpack->GetSpillNum();
                fLog.Form("[%s] DEBUG_CFADC", __FUNCTION__);
                fLogTmp.Form("\t[%4d]", isamp);
                fLog += fLogTmp;
                fLogTmp.Form("\tSpill:%6d\tTiming:%6d nsec", spill, itime);
                fLog += fLogTmp;
                fLogTmp.Form("\tHeight:%+8.3f mV", iheight);
                fLog += fLogTmp;
                fLogTmp.Form("\tCharge:%+8.3f pC", icharge);
                fLog += fLogTmp;
                LogDebug();
#endif
                // ----------
                fFadcNsec.push_back(itime);
                // ----------
                fFadcCharge.push_back(icharge);
                Double_t ipe = CalcFadcPe(icharge);
                fFadcPe.push_back(ipe);
                // ----------
                if (iheight < 0) iheight = 0;
                fFadcHeight.push_back(iheight);
                // Method 6:
                Int_t nhits = (Int_t)(iheight/kHitsumHeight);
                fFadcHit.push_back(nhits);
                isamp += skip; //Jump over this pulse
                hitflag = -1;  //Reset for next pulse
            }
        }
    }

    Int_t ntrg      = (Int_t)fFadcNsec.size();
    Int_t ntrg_tmp0 = (Int_t)fFadcHit.size();
    Int_t ntrg_tmp1 = (Int_t)fFadcHeight.size();
    Int_t ntrg_tmp2 = (Int_t)fFadcCharge.size();
    Int_t ntrg_tmp3 = (Int_t)fFadcPe.size();

    if (ntrg != ntrg_tmp0) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 0 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp1);
    if (ntrg != ntrg_tmp1) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 1 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp1);
    if (ntrg != ntrg_tmp2) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 2 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp2);
    if (ntrg != ntrg_tmp3) fprintf(stderr, "Error:\t[%s]\tInequal FADC Ntrg 3 (%4d != %4d)\n", __FUNCTION__, ntrg, ntrg_tmp3);
    
    //fprintf(stderr, "[%s]:\tNTRG: %d / %d / %d / %d / %d\n", __FUNCTION__, ntrg, ntrg_tmp0, ntrg_tmp1, ntrg_tmp2, ntrg_tmp3);
    //Stop();

    Int_t    nhits    = 0;
    Double_t ncharges = 0;
    Double_t npes     = 0;
    for (Int_t isamp = 0; isamp < ntrg; isamp++) {
        nhits    += fFadcHit.at(isamp);
        ncharges += fFadcCharge.at(isamp);
        npes     += fFadcPe.at(isamp);
        //fprintf(stderr, "DEBUG_TMP:\t[%s]\t[%d/%d]\tH:%d [#]\tQ:%.2f [pC]\tN:%.2f [p.e.]\n", __FUNCTION__, isamp, ntrg, nhits, ncharges, npes);
    }

    fUnpack->SetFadcNtrg(ntrg);
    fUnpack->SetFadcNhits(nhits);
    fUnpack->SetFadcNcharges(ncharges);
    fUnpack->SetFadcNpes(npes);

    //Stop();
#ifdef DEBUG_CFADC
    for (Int_t itrg = 0; itrg < ntrg; itrg++) {
        Int_t    itime   = fFadcNsec.at(itrg);
        Double_t iheight = fFadcHeight.at(itrg);
        Double_t icharge = fFadcCharge.at(itrg);
        Int_t    ihit    = fFadcHit.at(itrg);
        Double_t ipe     = fFadcPe.at(itrg);
        fLog.Form("[%s]", __FUNCTION__);
        fLogTmp.Form("\t[Check][%d/%d]", itrg, ntrg);
        fLog += fLogTmp;
        fLogTmp.Form("\tTiming:%6d [ns]", itime);
        fLog += fLogTmp;
        fLogTmp.Form("\tHeight:%8.3f [mV]\tHit :%4d [#]", iheight, ihit);
        fLog += fLogTmp;
        fLogTmp.Form("\tCharge:%8.3f [pC]\tP.E.:%8.3f [p.e.]\n", icharge, ipe);
        fLog += fLogTmp;
        LogDebug();
    }
#endif
    //Stop();
    return ;
}

// __________________________________________________
void Unpacker::ConvertFadcBunch()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    // Convert FADC into charges and pes according to fixed bunch timing information.
    // Bunch # and its timing (unit in [samp. points])
    // Window set to  [-18, 9] (= [-72nsec, 36 nsec])
    const Int_t maxsamp = (Int_t)fFadcPmtsum.size();
    const Int_t Nbunches = 8;
    const Int_t bunch_timing[Nbunches] = { 401, 550, 696, 845, 985, 1136, 1286, 1420};
    Double_t icharge[Nbunches] = {};
    Double_t ipe[Nbunches] = {};

    Double_t ncharges = 0;
    Double_t npes = 0;
    Int_t win_start = 0, win_end = 0;
    for (Int_t ibunch = 0; ibunch < Nbunches; ibunch++) {
        win_start = bunch_timing[ibunch] - 50;
        win_end   = bunch_timing[ibunch] + 50;
        if (win_end > maxsamp) break;
        icharge[ibunch] = GetPmtsumIntegral(win_start, win_end);
        ipe[ibunch]     = CalcFadcPe(icharge[ibunch]);
#ifdef DEBUG_CFADC
        fLog.Form("[%s] DEBUG_CFADC:\tB%d:%8.3f += %8.3f [pC]\t%8.3f += %8.3f [p.e.]",
                  __FUNCTION__, ibunch+1, ncharges, icharge[ibunch], npes, ipe[ibunch]);
        LogDebug();
#endif
        ncharges += icharge[ibunch];
        npes     += ipe[ibunch];
        fUnpack->SetFadcNchargesBunch(ibunch+1, icharge[ibunch]);
        fUnpack->SetFadcNpesBunch(ibunch+1, ipe[ibunch]);
    }
    fLog.Form("[%s] B0:%8.3f [pC]\t%8.3f [p.e.]", __FUNCTION__, ncharges, npes);
    LogDebug();
    fUnpack->SetFadcNchargesBunch(0, ncharges);
    fUnpack->SetFadcNpesBunch(0, npes);

    hFadcNchargesBunchPart->Fill(ncharges);
    hFadcNpesBunchPart->Fill(npes);
    return;
}

// __________________________________________________
void Unpacker::FillFadc()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Int_t nsamp = 0;
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC\n", __FUNCTION__);
    nsamp = (Int_t)fAtmNtrgnum.size();
    fLogTmp.Form("\tfAtmNtrgnum: %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fRawPmtsum.size();
    fLogTmp.Form("\tfRawPmtsum : %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fRawHitsum.size();
    fLogTmp.Form("\tfRawHitsum : %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcPmtsum.size();
    fLogTmp.Form("\tfFadcPmtsum: %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcHitsum.size();
    fLogTmp.Form("\tfFadcHitsum: %4d\n", nsamp);
    fLog += fLogTmp;
    for (Int_t ich = 0; ich < kFadcNchMax; ich++) {
        nsamp = (Int_t)fFadcRaw[ich].size();
        fLogTmp.Form("\t\tfFadcRaw[%d]    : %4d\n", ich, nsamp);
        fLog += fLogTmp;
    }
    LogDebug();
#endif

    // Fill fUpk
    fLog.Form("[%s] Fill fUpk", __FUNCTION__);
    LogDebug();
    nsamp = (Int_t)fFadcPmtsum.size();
    for (Int_t isamp = 0; isamp < nsamp; isamp++) {
        fUnpack->SetFadcPmtsum(fFadcPmtsum.at(isamp));
        fUnpack->SetFadcHitsum(fFadcHitsum.at(isamp));
    }
    fUnpack->SetFadcNsamp(nsamp);

    // Fill fWave
    fLog.Form("[%s] Fill fWave", __FUNCTION__);
    LogDebug();
    fRunNum = fUnpack->GetRunNum();
    fSpillNum = fUnpack->GetSpillNum();
    fTime = (Int_t)fUnpack->GetTime();
    if (fUnpackMode == kFULLMODE) {
        fWave->Fill();
    }
    return;
}

// __________________________________________________
void Unpacker::FillUpk()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Int_t nsamp = 0;
#ifdef DEBUG_CFADC
    fLog.Form("[%s] DEBUG_CFADC\n", __FUNCTION__);
    nsamp = (Int_t)fAtmNtrgnum.size();
    fLogTmp.Form("\tfAtmNtrgnum: %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcNsec.size();
    fLogTmp.Form("\tfFadcNsec  : %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcHit.size();
    fLogTmp.Form("\tfFadcHit   : %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcHeight.size();
    fLogTmp.Form("\tfFadcHeight: %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcCharge.size();
    fLogTmp.Form("\tfFadcCharge: %4d\n", nsamp);
    fLog += fLogTmp;
    nsamp = (Int_t)fFadcPe.size();
    fLogTmp.Form("\tfFadcPe    : %4d\n", nsamp);
    fLog += fLogTmp;
    LogDebug();
#endif

    nsamp = (Int_t)fFadcHit.size();
    for (Int_t isamp = 0; isamp < nsamp; isamp++) {
        fUnpack->SetFadcNsec(fFadcNsec.at(isamp));
        fUnpack->SetFadcHeight(fFadcHeight.at(isamp));
        fUnpack->SetFadcHit(fFadcHit.at(isamp));
        fUnpack->SetFadcCharge(fFadcCharge.at(isamp));
        fUnpack->SetFadcPe(fFadcPe.at(isamp));
    }
    
    nsamp = (Int_t)fAtmNtrgnum.size();
    for (Int_t isamp = 0; isamp < nsamp; isamp++) {
        fUnpack->SetAtmNtrgnum(fAtmNtrgnum.at(isamp));
    }

    fUpk->Fill();
    return;
}

/*
// __________________________________________________
void Unpacker::ConvertFADC_shota()
{
    PrintDebug("ConvertFADC_shota");
    Int_t nsamp = fUnpack->GetFadcNsamp();
    // Calculate pedestals for HITSUM and PMTSUM
    // pedestal = average of first 100 points
    Int_t nsamp_ped = 100;
    Double_t pmtsum_sum = 0, hitsum_sum = 0;
    for (Int_t isamp = 0; isamp < nsamp_ped; isamp++) {
        pmtsum_sum += fUnpack->PMTSUM(isamp);
        hitsum_sum += fUnpack->HITSUM(isamp);
    }
    Double_t pmtsum_ped = pmtsum_sum / nsamp_ped;
    Double_t hitsum_ped = hitsum_sum / nsamp_ped;
#ifdef DEBUG_CFADC
    printf("DEBUG_CFADC:\tpmtsum0/hitsum0 : %f / %f\n", pmtsum_ped, hitsum_ped);
#endif
    fUnpack->SetFadcPMTSUM0(pmtsum_ped);
    fUnpack->SetFadcHITSUM0(hitsum_ped);

    // Prepare data of HITSUM and PMTSUM after substacting pedestal
    std::vector<Double_t> hitsum1, pmtsum1;
    for (Int_t isamp = 0; isamp < nsamp; isamp++) {
        Double_t hitsum_tmp = fUnpack->HITSUM(isamp) - hitsum_ped;
        Double_t pmtsum_tmp = fUnpack->PMTSUM(isamp) - pmtsum_ped;
        hitsum1.push_back(hitsum_tmp);
        pmtsum1.push_back(pmtsum_tmp);
    }
#ifdef DEBUG_CFADC

#endif
    
    // Search for HITSUM leading edge.
    // Method:
    // 1. Hit threshold > 3 Hits (=3*15-5 = 40mV)
    // 2. Check the differential btw current sampling point and the next
    // 3. If differential exceeds the threshold, skip next 10 sampling point (=40nsec).
    //    This is to avoid miscounting the fluctuation of HITSUM at leading edge.
    //    Record post 25pts (=100nsec) value as HITSUM pulse height.
    //    Divide that value by kOneHitPulseHeight to calculate # of hits in that event (=NHits)
    // 4. Integration timing window is set accorting to the above sampling point.
    //    Window set to [-18, 9] (= [-72nsec, 36 nsec])
    //    Record the integrated PMTSUM value as PMTSUM charge.
    // 5. Recored The size of "HITSUM pulse height" vector as # of hits in a spill (=NTRG)
    Double_t thr3hit = 3 * kOneHitPulseHeight - 5; // -40mV
    const Int_t next = 10; // = 10 * 4 [ns]
    const Int_t skip = 49; // = 20 * 4 [ns]
    const Int_t stab =  6; // = 10 * 4 [ns]
    const Int_t avrg = 40; // = 25 * 4 [ns]
    const Int_t int_start = 18; // = 18 * 4 [ns]
    const Int_t int_stop  = 9;  // =  9 * 4 [ns]
    for (Int_t isamp = 0; isamp < nsamp-next; isamp++) {
        Double_t currhit = hitsum1[isamp];
        Double_t nexthit = hitsum1[isamp+next];
        Double_t diff = nexthit - currhit;
        // Method 2. Check differential
        if (diff < -thr3hit) {
            isamp += next;
            Double_t itime = ConvertFadcTime(isamp);
            // Method 3. Get average of HITSUM pulse height
            Double_t iheight = 0;
            for (Int_t jsamp = 0; jsamp < avrg; jsamp++) {
                Int_t point = isamp + stab + jsamp;
                if (point > nsamp) break;
                iheight += hitsum1[point];
            }
            iheight = iheight / avrg;
            // Method 4. Integrate PMTSUM
            Double_t icharge = 0;
            Int_t win_start = isamp - int_start;
            if (win_start < 0) win_start = 0;
            Int_t win_stop  = isamp + int_stop;
            for (Int_t jsamp = win_start; jsamp < win_stop; jsamp++) {
                if (jsamp > nsamp) break;
                icharge += pmtsum1[jsamp];
            }
#ifdef DEBUG_CFADC
            Int_t spill = fUnpack->GetSpillNum();
            printf("DEBUG_CFADC:\t[%4d/%4d]", isamp, nsamp);
            printf("\tS:%6d\tT:%8f", spill, itime);
            printf("\tH:%f\tD:%f\tPH:%f",hitsum1[isamp], diff, iheight);
            printf("\tCharge:%f\n", icharge);
#endif
            fUnpack->SetFadcTime(itime);
            if (iheight < 0) iheight = -iheight;
            else iheight = 0;
            fUnpack->SetFadcHeight(iheight);
            Int_t nhits = (Int_t)iheight/(Int_t)kOneHitPulseHeight;
            fUnpack->SetFadcHit(nhits);
            fUnpack->SetFadcCharge(-icharge);
            isamp += skip;
        }
    }
    fUnpack->SetGetFadcNtrg();
    fUnpack->SetFadcNHits();
    fUnpack->SetFadcNCharges();
#if 0
//#ifdef DEBUG_CFADC
    Int_t ntrg = fUnpack->GetFadcNtrg();
    for (Int_t isize = 0; isize < ntrg; isize++) {
        Int_t    spill   = fUnpack->GetSpillNum();
        Double_t itime   = fUnpack->GetFadcNsec(isize);
        Double_t iheight = fUnpack->GetFadcHeight(isize);
        Double_t icharge = fUnpack->GetFadcCharge(isize);
        Int_t    ihit    = fUnpack->GetFadcHit(isize);
        printf("DEBUG_CFADC:\t[%d/%d][Get]", isize, ntrg);
        printf("\tS:%6d\tT:%8f [us]", spill, itime);
        printf("\tH:%d\tPH:%f [mV]", ihit, iheight);
        printf("\tC:%f [mV*us]\n", icharge);
    }
#endif
    
    return ;
}
*/
  

// __________________________________________________
void Unpacker::ReadDetectorMap(const char *ifn)
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    
    if (PathExists(ifn, "r") != 0) exit(-1);

    // Initialize channel map variables
    fLog.Form("[%s] Initialize channel map variables", __FUNCTION__);
    LogDebug();
    
    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        fAtmchWin[iatm]  = -1;
        fAtmchPmt[iatm]  = -1;
        fAtmchGain[iatm] = -1;
        fAtmchMask[iatm] = -1;
        fAtmchHv[iatm]   = -1;
    }

    Int_t atmch, pmt, win, mask, hv;
    Double_t gain;

    // Create Gain Map tree and append it to unpacked ROOT file.
    const char* basename = gSystem->BaseName(ifn);
    fLog.Form("[%s] Mapping data with detector", __FUNCTION__);
    LogDebug();
    fLog.Form("[%s] Read '%s'", __FUNCTION__, basename);
    LogDebug();
    
    fMap = new TTree("map", basename);
    fMap->ReadFile(ifn, "pmt/I:window/I:atmch/I:mask/I:hv/I:gain/D");
    fMap->SetBranchAddress("pmt"    , &pmt);
    fMap->SetBranchAddress("window" , &win);
    fMap->SetBranchAddress("atmch"  , &atmch);
    fMap->SetBranchAddress("mask"   , &mask);
    fMap->SetBranchAddress("hv"     , &hv);
    fMap->SetBranchAddress("gain"   , &gain);

    const Int_t N = fMap->GetEntries();

    for (Int_t i = 0; i < N; i++) {
        fMap->GetEntry(i);
        if ( atmch < 0 || atmch > kNatmCh) {
            LogError(Form("[%s] Invalid ATMCH#%d\n", __FUNCTION__, atmch));
        } else {
            if (win <= 0) {
                fAtmchWin[atmch]  = -1;
                fAtmchPmt[atmch]  = -1;
                fAtmchGain[atmch] = -1;
                fAtmchMask[atmch] = -1;
            } else {
                fAtmchWin[atmch]  = win;
                fAtmchPmt[atmch]  = pmt;
                fAtmchGain[atmch] = gain;
                fAtmchMask[atmch] = mask;
                fAtmchHv[atmch]   = hv;
            }
        }
    }
    IsDetmapRead = kTRUE;

    fAverageGain = CalcAverageGain();
    fUnpack->SetAverageGain(fAverageGain);
    
#ifdef DEBUG_DETMAP
    LogDebug(Form("[%s] DEBUG_DETMAP ***** TTree::SetScanField(0) *****", __FUNCTION__));
    fMap->SetScanField(0);
    LogDebug(Form("[%s] DEBUG_DETMAP ***** TTree::Scan *****", __FUNCTION__));
    fMap->Scan("*");
#endif
    return;
}

// __________________________________________________
void Unpacker::PrintMap()
{
    fLog.Form("[%s]\n", __PRETTY_FUNCTION__);
    LogDebug();

    fLog.Form("[%s]\t==================================================", __FUNCTION__);
    LogDebug();
    fLog.Form("[%s]\tATMCH\tPMT\tWIN\tMASK\tGain\n", __FUNCTION__);
    LogDebug();
    for (Int_t iatm = 0; iatm < kNatmCh; iatm++) {
        fLog.Form("[%s]\t%3d\t%3d\t%3d\t%3d\t%e",
                  __FUNCTION__,
                  iatm,
                  GetAtmchPmt(iatm),
                  GetAtmchWindow(iatm),
                  GetAtmchMask(iatm),
                  GetAtmchGain(iatm)
            );
        LogDebug();
    }
    fprintf(stdout, "[%s]\t==================================================\n", __FUNCTION__);
    return;
}


// __________________________________________________
void Unpacker::PrintHitInfo(const Int_t &ihit, const Int_t &nhits)
{
    T2KWCHit *hit_tmp = fUnpack->GetHit(ihit);

    Int_t    ch    = hit_tmp->GetCh();
    Int_t    type  = hit_tmp->GetType();
    Double_t qdc0  = hit_tmp->GetQdc0();
    Double_t tdc0  = hit_tmp->GetTdc0();
    Double_t qdc1  = hit_tmp->GetQdc1();
    Double_t tdc1  = hit_tmp->GetTdc1();
    
    Double_t qdc = qdc1 - qdc0;
    Double_t tdc = tdc1 - tdc0;
    
    Double_t charge = hit_tmp->GetCharge();
    Double_t pe     = hit_tmp->GetPe();
    Double_t time   = hit_tmp->GetTime();
    Double_t gain   = hit_tmp->GetGain();
    Int_t    mask   = hit_tmp->GetMask();

    if (ihit % 20 == 0) {
        printf("\n\t----------------------- HIT INFORMATION ------------------------------------------------------------\n");
        printf("\t%8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s\n",
               "[NHits]", "ATMCH", "TYPE", "QDC", "(QDC1-QDC0)", "TDC", "(TDC1-TDC0)", "GAIN", "CHARGE", "P.E.", "Time", "mask");
        printf("\t----------------------------------------------------------------------------------------------------\n");
    }
    printf("\t[%3d/%3d]%8d %8d %7.1f (%4.f-%4.1f) %7.1f (%4.f-%4.1f) %7.2e %7.2f %7.2f\t%7.2f\t%8d\n",
           ihit, nhits, ch, type, qdc, qdc1, qdc0, tdc, tdc1, tdc0, gain, charge, pe, time, mask);
    return;
}

// __________________________________________________
void Unpacker::PrintHitSummary()
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();

    Int_t    spill          = fUnpack->GetSpillNum();
    Int_t    atm_ntrg       = fUnpack->GetAtmNtrg();
    Int_t    atm_nhits      = fUnpack->GetAtmNhits();
    Double_t atm_ncharges   = fUnpack->GetAtmNcharges();
    Double_t atm_npes       = fUnpack->GetAtmNpes();
    Double_t atm_max_charge = fUnpack->GetAtmMaxCharge();
    Double_t atm_max_pe     = fUnpack->GetAtmMaxPe();
    
    Int_t    fadc_ntrg      = fUnpack->GetFadcNtrg();
    Int_t    fadc_nhits     = fUnpack->GetFadcNhits();
    Double_t fadc_ncharges  = fUnpack->GetFadcNcharges();
    Double_t fadc_npes      = fUnpack->GetFadcNpes();

    fprintf(stdout, "\n\t==================== HIT SUMMARY ==================================================\n");
    fprintf(stdout, "\t\t# %6d | %8s %8s %8s %8s | %8s %8s\n",
            spill, "Ntrg", "Nhits", "NQs", "Npes", "MaxQ", "MaxPe");
    fprintf(stdout, "\t\t%8s | %8d %8d %8.2f %8.2f | %8.2f %8.2f\n",
            "[ATM]", atm_ntrg, atm_nhits, atm_ncharges, atm_npes, atm_max_charge, atm_max_pe);
    fprintf(stdout, "\t\t%8s | %8d %8d %8.2f %8.2f | %8s %8s\n",
            "[FADC]", fadc_ntrg, fadc_nhits, fadc_ncharges, fadc_npes, "-", "-");
    fprintf(stdout, "\t===================================================================================\n\n");
    return;
}



// __________________________________________________
void Unpacker::CheckHists(const char *ifn)
{
    fLog.Form("[%s]", __PRETTY_FUNCTION__);
    LogDebug();
    
}
#endif


