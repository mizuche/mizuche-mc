//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 22:45:56+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
//
#ifndef __CINT__
#ifndef ROOT_TApplication
#include "TApplication.h"
#endif

// PUT HEADERS HERE
#include <iostream>

#include <TH1.h>
#include <TH2.h>
#include <TVector3.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TROOT.h>
#include <TSocket.h>
#include <TMessage.h>
#include <TSystem.h>
#include <TThread.h>

#include "Unpacker.h"
#include "T2KWCUnpack.h"
#include "T2KWCRaw.h"

#include "./T2KWCEvtDisp.h"

#endif

#define EVENTDISP
//#define DEBUG

typedef struct {
    Double_t min;
    Double_t max;
    Int_t bin;
} Range_t;

// __________________________________________________
Int_t PrintDebug(const char* fname)
{
#ifdef DEBUG
    printf("DEBUG:\t***** mizu_online::%s *****\n", fname);
#endif
    return 0;
}

// __________________________________________________
TH1D *InitHist(const char* name, const char* title,
               const Double_t &xmin, const Double_t &xmax,
               const Int_t &kColor, const Int_t &binwidth = 1)
{
    Int_t xbin = (Int_t)xmax - (Int_t)xmin;
    xbin *= binwidth;
    TH1D *h = new TH1D(name, title, xbin, xmin, xmax);
    h->SetLineColor(kColor);
    h->SetFillColor(kColor);
    return h;
}

// __________________________________________________
TH1D *InitHist(const char* name, Bool_t kAutoRebin = kTRUE)
{
    Double_t xmin = 0;
    Double_t xmax = 100;
    Int_t xbin = 100;
    TH1D *h = 0;
    TString hname, htitle;

    Color_t kColor = 3;
    const Color_t kNHITS   = 3;
    const Color_t kTOTNPES = 3;
    const Color_t kRUN     = 3;
    const Color_t kSPILL   = 3;
    const Color_t kTRG     = 3;
    const Color_t kGONG    = 3;
    const Color_t kRUNMODE = 3;
//    const Color_t kNSAMP   = 3;
    
    const Color_t kATMCH  = 9;
    const Color_t kWINDOW = 9;
    const Color_t kPMT    = 9;
    const Color_t kTYPE   = 5;
    const Color_t kTAG    = 5;
    const Color_t kQDC    = 3;
    const Color_t kTDC    = 4;
    const Color_t kCHARGE = 3;
    const Color_t kTIME   = 3;
    const Color_t kPE     = 4;

        
    TString n = name;
    n.ToLower();
    
    if (n == "run") {
        xmin = 0; xmax = 100;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kRUN;
    }
    else if (n == "spill") {
        xmin = 0; xmax = 5;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kSPILL;
    }
    else if (n == "nhits") {
        xmin = 0; xmax = 100;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kNHITS;
    }
    else if (n == "window") {
        xmin = 0; xmax = 200;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kWINDOW;
    }
    else if (n == "qdc") {
        xmin = 0; xmax = 1000;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kQDC;
    }
    else if (n == "type") {
        xmin = -1; xmax = 3;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kTYPE;
    }
    else if (n == "totnpes") {
        xmin = 0; xmax = 1000;
        xbin = xbin/10;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [p.e.];Entries [#]", name, name);
        kColor = kTOTNPES;
    }
    else if (n == "trg") {
        xmin = -10; xmax = 50;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kTRG;
    }
    else if (n == "gong") {
        xmin = -10; xmax = 50;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kGONG;
    }
    else if (n == "runmode") {
        xmin = -1; xmax = 3;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kRUNMODE;
    }
    else if (n == "atmch") {
        xmin = 0; xmax = 200;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kATMCH;
    }
    else if (n == "pmt") {
        xmin = -10; xmax = 50;
        n.ToUpper();
        hname.Form("h%s", n.Data() );
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kPMT;
    }
    else if (n == "tag") {
        xmin = 0; xmax = 1000;
        n.ToUpper();
        hname.Form("h%s", n.Data());        
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kTAG;
    }
    else if (n == "tdc") {
        xmin = -10; xmax = 50;
        n.ToUpper();
        hname.Form("h%s", n.Data());        
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kTDC;
    }
    else if (n == "charge") {
        xmin = -10; xmax = 50;
        n.ToUpper();
        hname.Form("h%s", n.Data());
        htitle.Form("%s;%s [pC];Entries [#]", name, name);
        kColor = kCHARGE;
    }
    else if (n == "time") {
        xmin = -10; xmax = 50;
        n.ToUpper();
        hname.Form("h%s", n.Data());
        htitle.Form("%s;%s [nsec];Entries [#]", name, name);
        kColor = kTIME;
    }
    else if (n == "pe") {
        xmin = -10; xmax = 300;
        n.ToUpper();
        hname.Form("h%s", n.Data());
        htitle.Form("%s;%s [p.e.];Entries [#]", name, name);
        kColor = kPE;
    }
    else if (n == "gong-trg") {
        xmin = -5; xmax = 10;
        hname.Form("hTRGGONG");
        htitle.Form("%s;%s [#];Entries [#]", name, name);
        kColor = kTRG;
    }
    
    xbin = (Int_t)xmax - (Int_t)xmin;
    h = new TH1D(hname.Data(), htitle.Data(), xbin, xmin, xmax);
    h->SetFillColor(kColor);
    h->SetLineColor(kColor);

    if (kAutoRebin)
        h->SetBit(TH1::kCanRebin);
    
    return h;
}

// __________________________________________________
TH1D *InitHist(const char* name, const Int_t &num, Bool_t kAutoRebin=kTRUE) {
    TH1D *h = InitHist(name, kAutoRebin);
    h->SetName(Form("%s%d", h->GetName(), num));
    return h;

}


// // __________________________________________________
// Int_t InitHists()
// {
//     TString name, title;

//     // Histograms for every spill
//     //TH1D *hWin0     = InitHist("hWindow", "hWindow", 0, 200, 2
//     TH1D *hWin0     = InitHist("Window", 0, kFALSE);   // Window
//     TH1D *hQdc0     = InitHist("QDC", 0);     // QDC
//     TH1D *hCharge0  = InitHist("Charge", 0);  // Charge
//     TH1D *hPe0      = InitHist("PE", 0);       // PE
//     TH1D *hTdc0     = InitHist("TDC", 0);     // TDC
//     TH1D *hTime0    = InitHist("Time", 0);    // Time
//     TH1D *hMode0    = InitHist("RUNMODE", 0);  // RUNMODE
//     TH1D *hGongtrg0 = InitHist("GONG-TRG", 0);     // GONT-TRG
//     TH1D *hType0    = InitHist("Type", 0);     // Type

//     // Accumulated
//     TH1D *hwin1     = InitHist("Window", 1, kFALSE);   // Window    
//     TH1D *hqdc1     = InitHist("QDC", 1);     // QDC
//     TH1D *hcharge1  = InitHist("Charge", 1);  // Charge
//     TH1D *hpe1      = InitHist("PE", 1);       // PE
//     TH1D *htdc1     = InitHist("TDC", 1);     // TDC
//     TH1D *htime1    = InitHist("Time", 1);    // Time
//     TH1D *hmode1    = InitHist("RUNMODE", 1);  // RUNMODE
//     TH1D *hgongtrg1 = InitHist("GONG-TRG", 1);     // GONT-TRG
//     TH1D *htype1    = InitHist("Type", 1);     // Type    
//     TH1D *hspill1   = InitHist("Spill", 1, kFALSE);     // Type    
// }

// __________________________________________________
void mizuom(Int_t argc, char** argv)
{
  // DEFINE YOUR MAIN FUNCTION HERE
    // PrintDebug("TApplication Construction");
    // TROOT root("GUI","GUI");

    // TApplication theApp("App",0,0);

    int port = 11000;
    char hostname[300] = "10.105.55.226";
    bool print_flag = false;

    int c=-1;
    while ((c = getopt(argc, argv, "p:h:P")) != -1) {
        switch(c){
        case 'p':
            port = atoi(optarg);
            break;
        case 'h':
            sprintf(hostname, "%s", optarg);
            break;
        case 'P':
            print_flag = true;
            break;
        default:
            std::cout << "-p <port>" << std::endl;
            std::cout << "-h <hostname>" << std::endl;
            std::cout << "-P : Save display at each event" << std::endl;
            exit(1);
        }
    }
    argc-=optind;
    argv+=optind;

    PrintDebug("TH1D Construction");
    
    TH1D *hwin0     = InitHist("Window", 0, kFALSE);   // Window
    TH1D *hqdc0     = InitHist("QDC", 0);     // QDC
    TH1D *hcharge0  = InitHist("Charge", 0);  // Charge
    TH1D *hpe0      = InitHist("PE", 0);       // PE
    TH1D *htdc0     = InitHist("TDC", 0);     // TDC
    TH1D *htime0    = InitHist("Time", 0);    // Time
    TH1D *hmode0    = InitHist("RUNMODE", 0);  // RUNMODE
    TH1D *hgongtrg0 = InitHist("GONG-TRG", 0);     // GONT-TRG
    TH1D *htype0    = InitHist("Type", 0);     // Type

    // Accumulated
    TH1D *hwin1     = InitHist("Window", 1, kFALSE);   // Window    
    TH1D *hqdc1     = InitHist("QDC", 1);     // QDC
    TH1D *hcharge1  = InitHist("Charge", 1);  // Charge
    TH1D *hpe1      = InitHist("PE", 1);       // PE
    TH1D *htdc1     = InitHist("TDC", 1);     // TDC
    TH1D *htime1    = InitHist("Time", 1);    // Time
    TH1D *hmode1    = InitHist("RUNMODE", 1);  // RUNMODE
    TH1D *hgongtrg1 = InitHist("GONG-TRG", 1);     // GONT-TRG
    TH1D *htype1    = InitHist("Type", 1);     // Type    
    TH1D *hspill1   = InitHist("Spill", 1, kFALSE);     // Type    
    

    TString hname, htitle;
    htitle.Form("TotNPEs vs NHits;Total PE [pe];Total Hits [#]");
    TH2D *h2d1 = new TH2D("h2d", htitle.Data(), 100, 0, 1000, 10, 0, 100);
    
    PrintDebug("TCanvas Construction");
    gROOT->Macro("./mizu_online_style.C");

    Int_t wx = 150, wy = 150;    // Default canvas size
    Int_t cw = 0, ch = 0;        // Starting point of the canvas

    // Spill Information Monitor
    TCanvas *c0 = new TCanvas("c0", "Every Spill Monitor", 0, 0, 1500, 150);
    c0->Divide(10, 1);
    // Accumulated Information
    TCanvas *c1 = new TCanvas("c1", "Integration So Far", 0, 250, 1500, 150);
    c1->Divide(10, 1);
    
#ifdef EVENTDISP
    PrintDebug("Main EVENTDISP Construction");
    TCanvas *canvas = new TCanvas("canvas","Event Display", 0, 500, 800,400);
    T2KWCEvtDisp *disp = new T2KWCEvtDisp();
    disp->Init( canvas );
#endif

    PrintDebug("T2KWCUnpack, T2KWCRaw  Construction");
    T2KWCUnpack* upk = NULL;
    Unpacker unpacker;
    T2KWCRaw *raw[2];
    //T2KWCHit *hit;

    // Open socket to recv raw data
    PrintDebug("TSocket, TMessage Construction");
    TSocket *socket;
    TMessage *msg;

    // start
    printf("**************************************************\n");
    printf("\tStart Event Display\n");
    printf("**************************************************\n");
    Long64_t fN=0;
    TString buff;
    int runnum, spillnum;
    int runmode[2];
    int nmsg = 0;

    unpacker.ReadDetectorMap("detector_map.txt");
    unpacker.SetUnpackMode(1);
    unpacker.SetNped(100);
    gSystem->ProcessEvents();

    static Int_t lastspillnum = 0;

    TString title;
    while(1) {
        printf("\tConnecting to %s:%d\n", hostname, port);
        socket = new TSocket(hostname, port);
        
        //// currently, daq send message two times (1st:pedestal, 2nd:operation)
        printf("\tWaiting message from DAQ\n");
        nmsg = 0;

        if (socket->IsValid() ) {
            int i = 0;

            
            while ( socket->Recv(msg) ) {
                raw[i] = (T2KWCRaw*)msg->ReadObject( msg->GetClass() );
                PrintDebug("Receiving massage form DAQ");
                PrintDebug(Form("Print raw[%d]", i));
#ifdef DEBUG
                raw[i]->Print();
#endif
                PrintDebug(Form("Unpack raw[%d]", i));
                unpacker.Unpack(raw[i]);
                PrintDebug(Form("Get upk raw[%d]", i));
                upk = unpacker.GetUpk();
//                upk->Print();
                runmode[i] = upk->GetRunmode();
                PrintDebug(Form("Runmode : %d", runmode[i]));
                nmsg++;
                PrintDebug(Form("nmsg : %d", nmsg));
                delete msg;
                i++;

                gSystem->ProcessEvents();                

                if ( i == 2 ) {
                    printf("\t--------------------------------------------------\n");
                    printf("\tChecking the order of coming messages.\n");
                    if( runmode[0] == PEDESTAL_MODE &&
                        runmode[1] == OPERATE_MODE )
                        printf("\t\t==> OK ! Comming messages are in correct order.\n");
                    else {
                        printf("\t\t==> NG ! Comming messages are NOT in correct order.\n");
                        continue;
                    }

                    PrintDebug("Filling Histograms");
                    
                    runnum           = upk->GetRunNum();
                    spillnum         = upk->GetSpillNum();
                    Int_t nhits      = upk->GetAtmNhits();
                    Double_t totnpes = upk->GetAtmNpes();
                    Int_t trg        = upk->GetAtmTrg();
                    Int_t gong       = upk->GetAtmGong();
                    Int_t mode       = upk->GetRunmode();                    

                    c1->SetTitle(Form("Integration So Far  : RUN #%07d : Spill #%d (#%d)", runnum, spillnum, (int)fN ));
#ifdef EVENTDISP
                    canvas->SetTitle(Form("Event Display : RUN #%07d : Spill #%d", runnum, spillnum));
#endif

                    Double_t totq = 0;
                    Double_t totpe = 0;
                        
                    if (nhits > 0) {
                        c0->SetTitle(Form("Last Event Monitor : RUN #%07d : Spill #%d", runnum, spillnum));
                        // Reset When Hit comes
                        hwin0->Reset();
                        hqdc0->Reset();
                        htdc0->Reset();
                        hcharge0->Reset();
                        htime0->Reset();
                        htype0->Reset();
                        //htag0->Reset();
                        hpe0->Reset();
                        hmode0->Reset();
                        hgongtrg0->Reset();

                        hgongtrg0->Fill(gong - trg);
                        hgongtrg1->Fill(gong - trg);

                        h2d1->Fill(totnpes, nhits);


//                        T2KWCHit *hit = new T2KWCHit();
                        for (Int_t j = 0; j < nhits; j++) {
                            T2KWCHit *hit = upk->GetHit(j);
                            Int_t    win  = hit->GetWindow();
                            Double_t qdc0 = hit->GetQdc0();
                            Double_t qdc1 = hit->GetQdc1();
                            Double_t qdc  = qdc1 - qdc0;
                            Double_t q    = hit->GetCharge();
                            Double_t pe   = hit->GetPe();
                            Double_t tdc0  = hit->GetTdc0();
                            Double_t tdc1  = hit->GetTdc1();
                            Double_t tdc  = tdc1 - tdc0;
                            Double_t time = hit->GetTime();
                            Int_t    type = hit->GetType();

                            if (pe > 0) totpe += pe;
                            if (q  > 0) totq += q;
                            
                            // Reset Every Spill
                            hwin0->Fill(win);
                            hqdc0->Fill(qdc);
                            hcharge0->Fill(q);
                            hpe0->Fill(pe);
                            htdc0->Fill(tdc);
                            htime0->Fill(time);
                            htype0->Fill(type);
                            hmode0->Fill(mode);
                            
                            hwin1->Fill(win);
                            hqdc1->Fill(qdc);
                            hcharge1->Fill(q);
                            hpe1->Fill(pe);
                            htdc1->Fill(tdc);
                            htime1->Fill(time);
                            htype1->Fill(type);
                            hmode1->Fill(mode);
                        }
//                        delete hit;
                    }
                    
                    printf("\t--------------------------------------------------\n");
                    printf("\tRUN#     : Spill# : NHits : TotQ : NPEs\n");
                    printf("\t%8d : %8d :", runnum, spillnum);
                    printf("\t%8d : %5.2f : %5.2f (%5.2f)\n", nhits, totq, totnpes, totpe);
                    //hspill1->Fill(spillnum%32);
                    hspill1->Fill(spillnum - lastspillnum);
                    lastspillnum = spillnum;
                    
                    PrintDebug("Drawing Every Spill Information");
                    c0->cd(1);    hwin0->Draw();
                    c0->cd(2);    hqdc0->Draw();
                    c0->cd(3);    hcharge0->Draw();
                    c0->cd(4);    hpe0->Draw();
                    c0->cd(5);    htdc0->Draw();
                    c0->cd(6);    htime0->Draw();
                    c0->cd(7);    htype0->Draw();
                    c0->cd(8);    hgongtrg0->Draw();
                    c0->cd();     c0->Modified();

                    PrintDebug("Drawing Accumulated Spill Information");
                    c1->cd(1);    hwin1->Draw();
                    c1->cd(2);    hqdc1->Draw();
                    c1->cd(3);    hcharge1->Draw();
                    c1->cd(4);    hpe1->Draw();
                    c1->cd(5);    htdc1->Draw();
                    c1->cd(6);    htime1->Draw();
                    c1->cd(7);    h2d1->Draw("colz");
                    c1->cd(8);    hgongtrg1->Draw();
                    c1->cd(9);    hspill1->Draw();
                    c1->cd(10);   hmode1->Draw();
                    c1->cd();     c1->Modified();
                          
#ifdef EVENTDISP
                    canvas->cd();
                    PrintDebug("T2KWCEvtDisp::InputData(T2KWCUnpack*)");
                    // Input for event display & draw
                    disp->Clear();
                    disp->InputData( upk );
                    PrintDebug("T2KWCEvtDisp::Draw(TCanvas*)");
                    disp->Draw( canvas );

                    PrintDebug("Cheking the flags for printing");
                    if( print_flag==true ) {
                        buff.Form("evtdisp_run%07d_spill%d.pdf", runnum, spillnum);
                        canvas->SaveAs(buff);
                    }
                    
                    PrintDebug("Updating TCanvas");
                    canvas->Modified();
                    canvas->Update();
#endif  
                    fN++;      
                    upk->Clear();

                    c0->Update();
                    c1->Update();



                    
                    printf("**************************************************\n");
                    printf("\tUpdating Canvas (%dth)\n", (int)fN);
                    printf("**************************************************\n");

                    i = 0;
                }
            }
        }
        else {    // socket->Isvalid()
            sleep(1);
        }
        // PrintDebug("TSocket is valid.");
        // PrintDebug("Open TFile");
        // TString ofn;
        // ofn.Form("omlog_%07d.root", runnum);
        // TFile *fout = new TFile(ofn, "recreate");
        // PrintDebug("Writing TFile.");
        // fout->Write();
        // PrintDebug("Closing TFile.");
        // fout->Close();
        // PrintDebug("Closing TSocket.");
        // socket->Close();
        // return 0;
        // gSystem->Exit(1);
        // TSocket *socket = new TSocket(hostname, port);
    }
    
    return;
}


#ifndef __CINT__
Int_t main(int argc, char** argv)
{
    TApplication omApp("omApp", &argc, argv);
    mizuom(argc, argv);
    //TThread thread("mizuom", &mizuom, 0, 0);
    //thread.Run();
    omApp.Run();
    return 0;
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
