#! /usr/bin/env python
# coding:utf-8

import sys, os
import time
from optparse import OptionParser
    
class LoopUnpack(object):
    # __________________________________________________
    def __init__(self, ifndir, ofndir, start, end, fvstat, daqmode, overwrite = False):
        self.ifndir    = ifndir
        self.ofndir    = ofndir
        self.start     = start
        self.end       = end
        self.fvstat    = fvstat
        self.daqmode   = daqmode
        self.overwrite = overwrite

    # __________________________________________________
    def __str__(self):
        p = 'Path : ifn="%s"\tofn="%s"' % (self.ifndir, self.ofndir)
        r = 'Run  : %d - %d' % (self.start, self.end)
        f = 'FV   : %d' % (self.fvstat)
        w = 'OW   : %d' % (self.overwrite)
        l = [p, r, f, w]
        s = ('\n').join(l)
        return s

    ## __________________________________________________
    def unpack(self, ifn, ofn, overwrite = 0):
        '''
        From given directory name[dir] and input filename[ifn],
        output filename is automatically created.
        '''
        upk     = './unpack'
        fvstat  = str(self.fvstat)
        daqmode = str(self.daqmode)
        list = [upk, ifn, ofn, fvstat, daqmode]
        com  = (' ').join(list)
        
        m = '%s:\tStart unpacking file "%s".' % (sys.argv[0], ifn)
        sys.stdout.flush()
        sys.stderr.write(m + '\n')
        if not os.path.exists(ifn):
            m = '%s:\tError:\tFile "%s" does not exist. Skipping.\t==> Check if the path is correct. (%s)' % (sys.argv[0], ifn, ifn)
            sys.stdout.flush()
            sys.stderr.write(m + '\n')
        elif os.path.exists(ofn):
            if (overwrite == 1):
                m = '%s:\tWarning:\tFile "%s" already unpacked. Overwriting.' % (sys.argv[0], ifn)
                print m
                print com
                sys.stdout.flush()
                sys.stderr.write(m + '\n')
                os.system(com)
            else:
                m = '%s:\tFile "%s" already unpacked. Skipping.\t==> Choose "-w" option if you want to overwrite.' % (sys.argv[0], ifn)
                sys.stdout.flush()
                sys.stderr.write(m + '\n')
        else:
            print com
            sys.stdout.flush()
            os.system(com)
            time.sleep(1)

    ## __________________________________________________
    def loop(self):
        '''
        Loop program from start run# to end run #
        '''
        start     = self.start
        end       = self.end
        overwrite = self.overwrite
        
        if start > end:
            print '%s:\tError:\tStart(#%d) > End(#%d). RUN# should be in increasing order.' % (sys.argv[0], start, end)
            exit(1)
        
        for i in range(start, end + 1):
            ifn = self.ifndir + 'run%07d.root' % i
            ofn = self.ofndir + 'upk_run%07d.root' % i
            self.unpack(ifn, ofn, overwrite)

## __________________________________________________
if __name__ == '__main__':
    '''
    Main function. You need 3 arguments. (directory, start and end # of run.)
    This program checks input & output filename path.
    If input filename path does NOT exist, it QUITs unpacking.
    If output filename path ALREADY exists, it SKIPs unpacking.
    Else it unpacks from and to the number you set.
    '''

    usage = 'usage: %s [-wh] IfnDir OfnDif FvStat DaqMode RunStart (RunEnd)' % sys.argv[0]

    parser = OptionParser(usage)
    parser.add_option('-w', '--overwrite',
                      action='store_true',
                      dest='overwrite',
                      default=False,
                      help='overwrites the output file')
    (options, args) = parser.parse_args()

    larg = len(args)

    if larg > 2:
        ifndir  = args[0]
        ofndir  = args[1]
        fvstat  = int(args[2]) if larg > 1 else 42
        daqmode = int(args[3]) if larg > 1 else 42
        start   = int(args[4]) if larg > 2 else int(args[-1])
        end     = int(args[5]) if larg > 3 else int(args[-1])
        lu      = LoopUnpack(ifndir, ofndir, start, end, fvstat, daqmode, options.overwrite)
        lu.loop()
    else:
        print usage

