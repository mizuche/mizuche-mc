// -*- c++ -*-
//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 22:45:56+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
//
#ifndef __CINT__
#ifndef ROOT_TApplication
#include "TApplication.h"
#endif

// PUT HEADERS HERE
#include <iostream>

#include <TGClient.h>
#include <TH1.h>
#include <TH2.h>
#include <TVector3.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TROOT.h>
#include <TSocket.h>
#include <TMessage.h>
#include <TSystem.h>
#include <TThread.h>
#include <TLine.h>

#include "Unpacker.h"
#include "T2KWCUnpack.h"
#include "T2KWCRaw.h"

#include "../../mizulib/T2KWCEventDisplay.h"

#endif

#define EVENTDISP

TGraph *gDiffSpill;
TGraph *gDiffGongTrg;

TH1D *hDiffSpill;
TH1D *hAtmWin;

TH1D *hAtmNhits;
TH1D *hAtmNcharges;
TH1D *hAtmNpes;
TH1D *hAtmNtrg;

TH1D *hFadcNhits;
TH1D *hFadcNcharges;
TH1D *hFadcNpes;
TH1D *hFadcNsec;
TH1D *hFadcNtrg;

TH1D *hAtmQdc0;
TH1D *hAtmTdc0;
TH1D *hAtmCh;
TH1D *hAtmGong;
TH1D *hAtmTrg;

TCanvas *cOnMoni;
TCanvas *cEventLast;
TCanvas *cEventStack;
TCanvas *cEventHitmap;
TCanvas *cEventDisp;

T2KWCEvtDisp *disp;

TLine *iDiffSpill[2];
TLine *iAtmWin[3];

TLegend *lDiffSpill;


//#define DEBUG

TH1D *hwin0;
TH1D *hqdc0;
TH1D *hcharge0;
TH1D *hpe0;
TH1D *htdc0;
TH1D *htime0;
TH1D *hmode0;
TH1D *hgongtrg0;
TH1D *htype0;

// Accumulated
TH1D *hwin1;
TH1D *hqdc1;
TH1D *hcharge1;
TH1D *hpe1;
TH1D *htdc1;
TH1D *htime1;
TH1D *hmode1;
TH1D *hgongtrg1;
TH1D *htype1;
TH1D *hspill1;


// __________________________________________________
Int_t InitIndicators()
{
    iDiffSpill[0] = new TLine(0, 1.5, 100, 1.5);
    iDiffSpill[1] = new TLine(0, 0.5, 100, 0.5);
    for (Int_t i = 0; i < 2; i++) {
        iDiffSpill[i]->SetLineWidth(2);
        iDiffSpill[i]->SetLineStyle(2);
        iDiffSpill[i]->SetLineColor(3);
    }
        
    iAtmWin[0] = new TLine(29, 0, 29, 100);
    iAtmWin[1] = new TLine(57, 0, 57, 100);
    iAtmWin[2] = new TLine(165, 0, 165, 100);
    for (Int_t i = 0; i < 3; i++) {
        iAtmWin[i]->SetLineWidth(2);
        iAtmWin[i]->SetLineStyle(2);
        iAtmWin[i]->SetLineColor(3);
    }

    return 0;
}

// __________________________________________________
TH1D *InitHist(const char* name, const char* title,
               const Double_t &xmin, const Double_t &xmax,
               const Int_t &kColor, const Double_t &binwidth = 1)
{
    Int_t xbin = (Int_t)xmax - (Int_t)xmin;
    xbin = (Int_t)(xbin * binwidth);
    TH1D *h = new TH1D(name, title, xbin, xmin, xmax);
    h->SetLineColor(kColor);
    h->SetFillColor(kColor);
    h->SetStats(0);
    return h;
}

// __________________________________________________
Int_t InitHists()
{
    printf("[%s]\tInitialize Histograms\n", __FUNCTION__);
    TString name, title;
    Color_t kAtm  = 2;
    Color_t kFadc = 4;

    // Upper left --------------------
    name.Form("hMoniDiffSpill");
    title.Form("%s;Spill # Difference [#];Entries [#]", name.Data());
    hDiffSpill = InitHist(name.Data(), title.Data(), -5, 5, 1);
    // -----
    gDiffSpill = new TGraph(0);
    name.Form("gMoniDiffSpill");
    title.Form("%s;Spill [#];Difference [curr - prev]", name.Data());
    gDiffSpill->SetName(name.Data());
    gDiffSpill->SetTitle(title.Data());
    // -----
    name.Form("hMoniAtmQdc0");
    title.Form("%s;Qdc [count];Entries [#]", name.Data());
    hAtmQdc0 = InitHist(name.Data(), title.Data(), 0, 5000, kAtm);
    // -----
    name.Form("hMoniAtmTdc0");
    title.Form("%s;Tdc [count];Entries [#]", name.Data());
    hAtmTdc0 = InitHist(name.Data(), title.Data(), 0, 5000, kAtm);
    // -----
    name.Form("hMoniAtmCh");
    title.Form("%s;ATM Buffer [#];Entries [#]", name.Data());
    hAtmCh = InitHist(name.Data(), title.Data(), 0, 3, kAtm);
    // -----
    gDiffGongTrg = new TGraph(0);
    name.Form("gMoniDiffGongTrg");
    title.Form("%s;Gong [#];Difference [GONG - TRG]", name.Data());
    gDiffGongTrg->SetName(name.Data());
    gDiffGongTrg->SetTitle(title.Data());
    // -----
    name.Form("hMoniAtmGong");
    title.Form("%s;Gong [#];Entries [#]", name.Data());
    hAtmGong = InitHist(name.Data(), title.Data(), 0, 200, kAtm);
    // -----
    name.Form("hMoniAtmTrg");
    title.Form("%s;TRG [#];Entries [#]", name.Data());
    hAtmTrg = InitHist(name.Data(), title.Data(), 0, 200, kAtm);
    
    // Lower right --------------------
    name.Form("hMoniFadcNsec");
    title.Form("%s;Time [nsec];Entries [#]", name.Data());
    hFadcNsec = InitHist(name.Data(), title.Data(), 0, 14000, kFadc, 0.25);
    // -----
    name.Form("hMoniWindow");
    title.Form("%s;Window [#];Entries", name.Data());
    hAtmWin = InitHist(name.Data(), title.Data(), 0, 200, kAtm);
    // Upper right --------------------
    name.Form("hMoniAtmNhits");
    title.Form("%s;Nhits [#];Entries", name.Data());
    hAtmNhits = InitHist(name.Data(), title.Data(), 0, 200, kAtm);
    // -----
    name.Form("hMoniAtmNcharges");
    title.Form("%s;Ncharges [pC];Entries", name.Data());
    hAtmNcharges = InitHist(name.Data(), title.Data(), 0, 200, kAtm);
    // -----
    name.Form("hMoniAtmNpes");
    title.Form("%s;Npes [p.e.];Entries", name.Data());
    hAtmNpes = InitHist(name.Data(), title.Data(), 0, 200, kAtm);
    // -----
    name.Form("hMoniAtmNtrg");
    title.Form("%s;Ntrg [#];Entries [#]", name.Data());
    hAtmNtrg = InitHist(name.Data(), title.Data(), 0, 10, kAtm);
    // Upper right --------------------
    name.Form("hMoniFadcNhits");
    title.Form("%s;Nhits [#];Entries", name.Data());
    hFadcNhits = InitHist(name.Data(), title.Data(), 0, 200, kFadc);
    // -----
    name.Form("hMoniFadcNcharges");
    title.Form("%s;Ncharges [pC];Entries", name.Data());
    hFadcNcharges = InitHist(name.Data(), title.Data(), 0, 200, kFadc);
    // -----
    name.Form("hMoniFadcNpes");
    title.Form("%s;Npes [p.e.];Entries", name.Data());
    hFadcNpes = InitHist(name.Data(), title.Data(), 0, 200, kFadc);
    // -----
    name.Form("hMoniFadcNtrg");
    title.Form("%s;Ntrg [#];Entries [#]", name.Data());
    hFadcNtrg = InitHist(name.Data(), title.Data(), 0, 10, kFadc);
    // --------------------
    
    return 0;
}

// __________________________________________________
Int_t FillHists(T2KWCUnpack *upk)
{
    Int_t    atm_ntrg  = upk->GetAtmNtrg();
    Int_t    fadc_ntrg = upk->GetFadcNtrg();

    if (atm_ntrg > 0 && fadc_ntrg > 0) {
        Int_t    atm_nhits = upk->GetAtmNhits();        
        Double_t atm_ncharges = upk->GetAtmNcharges();
        Double_t atm_npes  = upk->GetAtmNpes();

        hAtmNhits->Fill(atm_nhits);
        hAtmNcharges->Fill(atm_ncharges);
        hAtmNpes->Fill(atm_npes);
        hAtmNtrg->Fill(atm_ntrg);

        Int_t    fadc_nhits = upk->GetFadcNhits();
        Double_t fadc_ncharges = upk->GetFadcNcharges() / 0.088;
        Double_t fadc_npes  = upk->GetFadcNpes() / 0.088 / 1.6;

        hFadcNhits->Fill(fadc_nhits);
        hFadcNcharges->Fill(fadc_ncharges);
        hFadcNpes->Fill(fadc_npes);
        hFadcNtrg->Fill(fadc_ntrg);
        
        Int_t    atm_trg  = upk->GetAtmTrg();
        Int_t    atm_gong = upk->GetAtmGong();
        hAtmGong->Fill(atm_gong);
        hAtmTrg->Fill(atm_trg);
        if (atm_trg == 0) gDiffGongTrg->Set(0);
        gDiffGongTrg->SetPoint(gDiffGongTrg->GetN(), atm_gong, atm_gong - atm_trg);
        gDiffGongTrg->SetMarkerColor(atm_gong - atm_trg);

        //T2KWCHit *hit = new T2KWCHit();
        for (Int_t ihit = 0; ihit < atm_nhits; ihit++) {
            T2KWCHit *hit = upk->GetHit(ihit);
            Int_t window  = hit->GetWindow();
            hAtmWin->Fill(window);

            Int_t qdc0 = (Int_t)hit->GetQdc0();
            Int_t tdc0 = (Int_t)hit->GetTdc0();
            hAtmQdc0->Fill(qdc0);
            hAtmTdc0->Fill(tdc0);

            Int_t type = hit->GetType();
            hAtmCh->Fill(type);
        }

        for (Int_t itrg = 0; itrg < fadc_ntrg; itrg++) {
            Int_t fadc_nsec = upk->GetFadcNsec(itrg);
            hFadcNsec->Fill(fadc_nsec);
        }

        //cEventLast->SetTitle("Last Event Monitor " + title);
        hDiffSpill->SetLineColor(5);
        gDiffSpill->SetMarkerColor(5);
    } else {
        hDiffSpill->SetLineColor(1);
        gDiffSpill->SetMarkerColor(1);
        gDiffSpill->SetLineColor(1);
    }

    return 0;
}

// __________________________________________________
Int_t InitCanvases()
{
    Int_t displayh = (Int_t)(gClient->GetDisplayHeight()*0.9);
    Int_t displayw = (Int_t)(gClient->GetDisplayWidth()*0.9);
    cOnMoni = new TCanvas("cOnMoni", "Mizuche Online Monitor", displayw, displayh);
    cOnMoni->SetBorderSize(1);
    cOnMoni->SetLineStyle(2);
    cOnMoni->SetLineColor(2);
    cOnMoni->Divide(2, 2);

    // Last Event Monitor
    //cEventLast = new TCanvas("cEventLast", "Last Event Monitor", 0, 0, 1500, 150);
    cEventLast = (TCanvas*)cOnMoni->GetPad(1);
    cEventLast->SetBorderSize(1);
    cEventLast->SetLineStyle(2);
    cEventLast->SetLineColor(2);
    cEventLast->Modified();
    cEventLast->Divide(3, 2);

    // Accumulated Event Monitor
    //cEventStack = new TCanvas("cEventStack", "Stacked Event Monitor", 0, 250, 500, 500);
    cEventStack = (TCanvas*)cOnMoni->GetPad(2);
    cEventStack->Divide(3, 2);

    // Main display
    //cEventDisp = new TCanvas("cEventDisp","Event Display", 0, 500, 800,400);
    cEventDisp = (TCanvas*)cOnMoni->GetPad(3);
    disp = new T2KWCEvtDisp();
    disp->Init( cEventDisp );

    // Hitmap and Hittiming
    //cEventHitmap = new TCanvas("cEventHitmap", "Hit Map and Timing", 1000, 1000);
    cEventHitmap = (TCanvas*)cOnMoni->GetPad(4);
    cEventHitmap->Divide(1, 2);

    return 0;
}

// __________________________________________________
Int_t DrawEventDisp(T2KWCUnpack *upk)
{
    cEventDisp->Update();
    printf("[%s]\tUpdate Event Display\n", __FUNCTION__);
    cEventDisp->cd();
    // Input for event display & draw
    disp->Clear();
    disp->InputData( upk );
    disp->Draw( cEventDisp );

    // if( print_flag==true ) {
    //     buff.Form("evtdisp_run%07d_spill%d.pdf", runnum, spillnum);
    //     cEventDisp->SaveAs(buff);
    // }

    cEventDisp->Modified();
    cEventDisp->Update();
    return 0;
}


// __________________________________________________
Int_t DrawStack()
{
    printf("[%s]\tUpdate Stacked Histograms\n", __FUNCTION__);
    cEventStack->cd(1);
    hAtmNhits->Draw();
    hFadcNhits->Draw("same");
    cEventStack->cd(2);
    hFadcNcharges->Draw();
    hAtmNcharges->Draw("same");
    cEventStack->cd(3);
    hFadcNpes->Draw();
    hAtmNpes->Draw("same");

    cEventStack->cd(4)->SetLogy();
    hAtmNtrg->Draw();
    hFadcNtrg->Draw("same");
    cEventStack->cd(5);
    cEventStack->Update();

    return 0;
}

// __________________________________________________
Int_t DrawLast()
{
    printf("[%s]\tUpdate Last Event Histograms\n", __FUNCTION__);
    cEventLast->cd(1);
    //hDiffSpill->Draw();
    gDiffSpill->GetYaxis()->SetRangeUser(0, 2);
    gDiffSpill->SetMarkerStyle(8);
    gDiffSpill->Draw("apl");
    Int_t xmin = (Int_t)cEventLast->cd(1)->GetUxmin();
    Int_t xmax = (Int_t)cEventLast->cd(1)->GetUxmax();
    for (Int_t i = 0; i < 2; i++) {
        iDiffSpill[i]->SetX1(xmin);
        iDiffSpill[i]->SetX2(xmax);
        iDiffSpill[i]->Draw();
    }
    cEventLast->cd(5)->SetLogy();
    hAtmQdc0->GetXaxis()->SetRangeUser(0, 1500);
    hAtmQdc0->Draw();
    cEventLast->cd(6)->SetLogy();
    hAtmTdc0->GetXaxis()->SetRangeUser(1500, 3000);
    hAtmTdc0->Draw();
    cEventLast->cd(2);
    gDiffGongTrg->GetYaxis()->SetRangeUser(0, 5);
    if (gDiffGongTrg->GetN() != 0) gDiffGongTrg->Draw("apl");
    //hAtmGong->Draw();
    cEventLast->cd(4);
    //hAtmTrg->Draw();
    cEventLast->cd(3)->SetLogy();
    hAtmCh->Draw();
    cEventLast->Update();
    return 0;
}

// __________________________________________________
Int_t DrawHitmap()
{
    printf("[%s]\tUpdate Hitmap and Timing\n", __FUNCTION__);
    cEventHitmap->cd(1)->SetLogy(0);
    hAtmWin->Draw();
    Int_t ymax = (Int_t)cEventHitmap->cd(1)->GetUymax();
    for (Int_t i = 0; i < 3; i++) {
        iAtmWin[i]->SetY2(ymax);
        iAtmWin[i]->Draw();
    }
    cEventHitmap->cd(2)->SetLogy(0);
    hFadcNsec->Draw();
    return 0;
}

// __________________________________________________
int mizuom(Int_t argc, char** argv)
//int main(Int_t argc, char** argv)
{
    // DEFINE YOUR MAIN FUNCTION HERE
    // PrintDebug("TApplication Construction");
    // TROOT root("GUI","GUI");

    // TApplication theApp("App",0,0);

    int port = 11000;
    char hostname[300] = "10.105.55.226";
    bool print_flag = false;

    int c=-1;
    while ((c = getopt(argc, argv, "p:h:P")) != -1) {
        switch(c){
        case 'p':
            port = atoi(optarg);
            break;
        case 'h':
            sprintf(hostname, "%s", optarg);
            break;
        case 'P':
            print_flag = true;
            break;
        default:
            std::cout << "-p <port>" << std::endl;
            std::cout << "-h <hostname>" << std::endl;
            std::cout << "-P : Save display at each event" << std::endl;
            exit(1);
        }
    }
    argc-=optind;
    argv+=optind;


    gROOT->Macro("./mizu_online_style.C");
    InitHists();
    InitIndicators();
    InitCanvases();
    
    T2KWCUnpack* upk = NULL;
    Unpacker unpacker;
    T2KWCRaw *raw[2];
    //T2KWCHit *hit;

    // Open socket to recv raw data
    TSocket *socket;
    TMessage *msg;

    // start
    printf("**************************************************\n");
    printf("\tStart Event Display\n");
    printf("**************************************************\n");
    Long64_t fN=0;
    TString buff;
    int runnum, spillnum;
    int runmode[2];
    int nmsg = 0;

    unpacker.ReadDetectorMap("detector_map.txt");
    unpacker.SetUnpackMode(1);
    unpacker.SetFadcNped(100);
    unpacker.SetFadcHitThrd(10);
    unpacker.SetDebugLevel(0);
    unpacker.SetVerboseLevel(0);
    gSystem->ProcessEvents();

    static Int_t lastspillnum = 0;

    TString title;
    while(1) {
        printf("[%s]\tConnecting to '%s:%d'\n", __FUNCTION__, hostname, port);
        socket = new TSocket(hostname, port);
        nmsg = 0;
        //// currently, daq send message two times (1st:pedestal, 2nd:operation)
        printf("[%s]\tWaiting message from DAQ\n", __FUNCTION__);

        if (socket->IsValid() ) {
            int i = 0;
            while ( socket->Recv(msg) ) {
                raw[i] = (T2KWCRaw*)msg->ReadObject( msg->GetClass() );
#ifdef DEBUG
                PrintDebug("Receiving massage form DAQ");
                PrintDebug(Form("Print raw[%d]", i));
                raw[i]->Print();
                PrintDebug(Form("Unpack raw[%d]", i));
                PrintDebug(Form("Get upk raw[%d]", i));
#endif
                unpacker.Unpack(raw[i]);
                upk = unpacker.GetUpk();
                //upk->Print();
                runmode[i] = upk->GetRunmode();
                nmsg++;
                delete msg;
                i++;
                
                gSystem->ProcessEvents();                
                
                if ( i == 2 ) {
                    printf("\t--------------------------------------------------\n");
                    printf("[%s]\tChecking the order of coming messages.\n", __FUNCTION__);
                    if( runmode[0] == PEDESTAL_MODE &&
                        runmode[1] == OPERATE_MODE )
                        printf("\t\t==> OK ! Comming messages are in correct order.\n");
                    else {
                        printf("\t\t==> NG ! Comming messages are NOT in correct order.\n");
                        continue;
                    }
                    runnum             = upk->GetRunNum();
                    spillnum           = upk->GetSpillNum();
                    title.Form("RUN#%07d : Spill#%7d : Cycle#%5d", runnum, spillnum, (int)fN);
                    // cOnMoni->SetTitle("Mizuche Online Monitor : " + title);
                    cOnMoni->SetTitle(title);
                    cEventStack->SetTitle("Stacked Event Monitor " + title);
                    cEventDisp->SetTitle("Event Display " + title);
                    
                    FillHists(upk);
                    // printf("\t--------------------------------------------------\n");
                    // printf("\tRUN#     : Spill# : NHits : TotQ : NPEs\n");
                    // printf("\t%8d : %8d :", runnum, spillnum);
                    // printf("\t%8d : %5.2f : %5.2f (%5.2f)\n", nhits, totq, totnpes, totpe);
                    //if (fN==0) lastspillnum = spillnum -1;
                    if (gDiffSpill->GetN() > 100) gDiffSpill->Set(0);
                    gDiffSpill->SetPoint(gDiffSpill->GetN(), spillnum, spillnum-lastspillnum);
                    if (spillnum - lastspillnum !=1) gDiffSpill->SetLineColor(5);
                    hDiffSpill->Fill(spillnum - lastspillnum);
                    lastspillnum = spillnum;
                    
                    DrawLast();
                    DrawEventDisp(upk);
                    DrawStack();
                    DrawHitmap();
                    
                    printf("**************************************************\n");
                    printf("\tUpdating Canvas (%dth)\n", (int)fN);
                    printf("**************************************************\n");
                    //upk->Clear();
                    upk = 0;
                    raw[i] = 0;
                    i = 0;
                    fN++;      
                }
            }
        }
        else {    // socket->Isvalid()
            sleep(3);
        }
        // PrintDebug("TSocket is valid.");
        // PrintDebug("Open TFile");
        // TString ofn;
        // ofn.Form("omlog_%07d.root", runnum);
        // TFile *fout = new TFile(ofn, "recreate");
        // PrintDebug("Writing TFile.");
        // fout->Write();
        // PrintDebug("Closing TFile.");
        // fout->Close();
        // PrintDebug("Closing TSocket.");
        socket->Close();
        // return 0;
        // gSystem->Exit(1);
        // TSocket *socket = new TSocket(hostname, port);
        delete socket;
    }
    
    return 0;
}

// __________________________________________________
#ifndef __CINT__
Int_t main(int argc, char** argv)
{
    TApplication omApp("omApp", &argc, argv);
    mizuom(argc, argv);
    //TThread thread("mizuom", &mizuom, 0, 0);
    //thread.Run();
    omApp.Run();
    return 0;
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
