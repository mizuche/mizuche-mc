// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 01:06:59+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//

#ifndef ROOT_MERGER
#define ROOT_MERGER
#ifndef ROOT_TObject
#include "TObject.h"
#endif

//#define DEBUG

//#include <signal.h>

// #include <TString.h>
// #include <TStopwatch.h>
// #include <vector>
// #include <Fstream>

#include <TSystem.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TLegend.h>
#include <TChainElement.h>
#include <TGaxis.h>

#include "T2KWCUnpack.h"
//#include "Base.h"

class Merger : public TObject
{
private:

    // Input ROOT file (upk%07d.root)
    TFile *fin;
    TTree *fUmode;
    TTree *fUpk;
    TTree *fMap;
    TTree *fHistory;
    T2KWCUnpack *fUnpack;

    // Output ROOT file (dst%07d.root)
    TFile *fout;
    TTree *fDst;
    T2KWCHit    *fHit;
    T2KWCUnpack *fMerged;

    // Branches for BSDs
    TChain   *fBsd;
    Int_t     fBsdNurun;
    Int_t     fBsdMidasEvent;
    Int_t     fBsdMrrun;
    Int_t     fBsdMrshot;
    Int_t     fBsdSpillNum;
    Int_t     fBsdTrigSec[3];
    Int_t     fBsdTrigNano[3];
    Int_t     fBsdGpsstat[2];
    Double_t  fBsdCtnp[5][9];
    Double_t  fBsdBeamTime[5][9];
    Int_t     fBsdBeamFlag[5][9];
    Double_t  fBsdHct[3][5];
    Double_t  fBsdTpos[2];
    Double_t  fBsdTdir[2];
    Double_t  fBsdTsize[2];
    Double_t  fBsdMumon[12];
    Double_t  fBsdOtr[13];
    Int_t     fBsdGoodGpsFlag;
    Int_t     fBsdTriggerFlag;
    Int_t     fBsdSpillFlag;
    Int_t     fBsdGoodSpillFlag;
    Double_t  fBsdTargetEff[3];
    Int_t     fBsdRunType;
    Int_t     fBsdMagsetId;

    Int_t fUnpackMode;
    std::vector<Int_t>     fAtmNtrgnum;
    std::vector<Int_t>     fRawPmtsum;
    std::vector<Int_t>     fRawHitsum;
    std::vector<Double_t>  fFadcPmtsum;
    std::vector<Double_t>  fFadcHitsum;
    std::vector<Int_t>     fFadcNsec;
    std::vector<Int_t>     fFadcHit;
    std::vector<Double_t>  fFadcHeight;
    std::vector<Double_t>  fFadcCharge;
    std::vector<Double_t>  fFadcPe;

    Bool_t IsUpkRead;
    Bool_t IsBsdRead;
    Bool_t IsDstOpened;
    Bool_t IsOverwritable;

    TGraphErrors *greBsdSpill;
    TGraphErrors *greUpkSpill;
    TGraphErrors *greUpkRun;
    TGraphErrors *greBsdRun;
    TGraphErrors *greUpkEntry;
    TGraphErrors *greBsdEntry;

    TString fLog, fLogTmp;
    Int_t fVerboseLevel;
    Int_t fDebugLevel;
    Int_t fErrorLevel;
    
public:
    Merger();
    virtual ~Merger();

    Int_t ReadUpk(const char* ifn);
    Int_t ReadBsd(const char* ifn);
    Int_t ReadBsds(const Int_t &srun, const Int_t &erun, const char* t2krun, const char* version);
    Int_t ReadBsdsV01(const char* bsddir, const Int_t &irun);
    Int_t ReadBsdsV01_2(const char* bsddir, const Int_t &irun);
    Int_t OpenDst(const char* ofn, Bool_t kOverwrite);
    Int_t Merge();
    Int_t PreMerge();
    Int_t Write();
    Int_t Clear();
    Bool_t IsTimeOK(const Int_t &upktime, const Int_t &bsdtime, const Int_t &delta);
    Bool_t IsSpillOK(const Int_t &upkspill, const Int_t &bsdspill);
    char* ConvertUnixTime(const Int_t utime) const;
    Int_t Stop();
    Int_t Jump();
    Int_t FillDst();
    Int_t CloseDst();
    Int_t PrintProgress(const Int_t &i, const Int_t &N, const Int_t &L);
    Int_t Overwrite(const char* ofn);
    Int_t PathExists(const char* ifn, const char* mode);

    void SetVerboseLevel(const Int_t &level) { fVerboseLevel = level;}
    void PrintVerbose(const Int_t &level, const char* log);
    void PrintVerbose(const Int_t &level);

    void SetDebugLevel(const Int_t &level) { fDebugLevel = level;}
    void PrintDebug(const Int_t &level, const char* log);
    void PrintDebug(const Int_t &level);

    void SetErrorLevel(const Int_t &level) { fErrorLevel = level;}
    void PrintError(const Int_t &level, const char* log);
    void PrintError(const Int_t &level);
    
    //ClassDef(Merger,0) //DOCUMENT ME
};

#endif
//____________________________________________________________________ 
//  
// EOF
//


//____________________________________________________________________
//
// DOCUMENT ME
// 

//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-01 01:09:00+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
#ifndef ROOT_MERGER
#include "Merger.h"
#endif

//____________________________________________________________________
//ClassImp(Merger);

//____________________________________________________________________ 
//  
// EOF
//

// __________________________________________________
Merger::Merger()
{
    fLog.Form("[%s]", __FUNCTION__);
    PrintVerbose(0);

    IsUpkRead   = kFALSE;
    IsBsdRead   = kFALSE;
    IsDstOpened = kFALSE;
    greBsdSpill   = new TGraphErrors(0);
    greUpkSpill   = new TGraphErrors(0);
    greBsdRun     = new TGraphErrors(0);
    greUpkRun     = new TGraphErrors(0);
    greBsdEntry   = new TGraphErrors(0);
    greUpkEntry   = new TGraphErrors(0);
    
    //Clear();
}

// __________________________________________________
Merger::~Merger()
{
    
}

// __________________________________________________
Int_t Merger::PathExists(const char* ifn, const char* mode)
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(3);

    TString m = mode;
    m.ToLower();

    FileStat_t info;
    Int_t r = gSystem->GetPathInfo(ifn, info);
    if (m == "r" && r !=0) {
        fprintf(stderr, "Error:\tFile '%s' does not exist.\n", ifn);
    }
    if (m == "w" && r ==0) {
        fprintf(stderr, "Error:\tFile '%s' already exist.\n", ifn);
    }
    return r;
}

// __________________________________________________
void Merger::PrintVerbose(const Int_t &level, const char* log) {
    if (level < fVerboseLevel) {
        fprintf(stdout, "VERBO[%d<%d] | %s",level, fVerboseLevel, log);
        fflush(stdout);
    }
}

// __________________________________________________
void Merger::PrintVerbose(const Int_t &level) {
    PrintVerbose(level, fLog.Data());
}

// __________________________________________________
void Merger::PrintDebug(const Int_t &level, const char* log) {
    if (level < fDebugLevel) {
        fprintf(stdout, "DEBUG[%d<%d] | %s",level, fDebugLevel, log);
        fflush(stdout);
    }
}

// __________________________________________________
void Merger::PrintDebug(const Int_t &level) {
    PrintDebug(level, fLog.Data());
}

// __________________________________________________
void Merger::PrintError(const Int_t &level, const char* log) {
    if (level < fErrorLevel) {
        fprintf(stderr, "ERROR[%d<%d] | %s",level, fErrorLevel, log);
        fflush(stdout);
    }
}

// __________________________________________________
void Merger::PrintError(const Int_t &level) {
    PrintError(level, fLog.Data());
}

// // __________________________________________________
// void Merger::SetSignal(Int_t p_signame)
// {
//     if (signal(p_signame, SigHandler) == SIG_ERR) {
//         printf("Cannot set signals. Exit.\n");
//         exit(1);
//     }
//     return;
// }

// // __________________________________________________
// void Merger::SigHandler(Int_t p_signame)
// {
//     static int   sig_cnt = 0;
//     ++sig_cnt;
//     if (sig_cnt <= 2) {
//         printf("%d th interruption. Ignore it.\n", sig_cnt);
//     }
//     else {
//         printf("%d th interruption. Exit.\n",  sig_cnt);
//         exit(0);
//     }
    
//     SetSignal(p_signame);  // Set singal again.
    
//     return;
// }

// __________________________________________________
Int_t Merger::ReadUpk(const char* ifn)
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);

    if (PathExists(ifn, "r") != 0) exit(-1);

    fLog.Form("[%s]\tRead '%s'\n", __FUNCTION__, ifn);
    PrintVerbose(0);
    
    fin = new TFile(ifn, "read");
    fUpk = (TTree*)fin->Get("upk");
    TBranch *br = fUpk->GetBranch("upk.");
    fUnpack = new T2KWCUnpack();
    br->SetAddress(&fUnpack);
    fUpk->SetBranchAddress("upk.", &fUnpack);

    TTree *tmap = (TTree*)fin->Get("map");
    fMap = tmap->CloneTree();

    TTree *tumode = (TTree*)fin->Get("umode");
    fUmode = tumode->CloneTree();

    IsUpkRead = kTRUE;

    return 0;
}

// __________________________________________________
Int_t Merger::ReadBsd(const char* ifn)
{
    //fLog.Form("[%s]\n", __FUNCTION__);
    //PrintDebug(0);
    if (!IsBsdRead) {
        fLog.Form("[%s]\tCreate TChain named 'bsd'\n", __FUNCTION__);
        PrintDebug(1);
        fBsd = new TChain("bsd");
        fBsd->SetBranchAddress("nurun"           , &fBsdNurun);
        fBsd->SetBranchAddress("midas_event"     , &fBsdMidasEvent);
        fBsd->SetBranchAddress("mrrun"           , &fBsdMrrun);
        fBsd->SetBranchAddress("mrshot"          , &fBsdMrshot);
        fBsd->SetBranchAddress("spillnum"        , &fBsdSpillNum);
        fBsd->SetBranchAddress("trg_sec"         , fBsdTrigSec);
        fBsd->SetBranchAddress("trg_nano"        , fBsdTrigNano);
        fBsd->SetBranchAddress("gpsstat"         , fBsdGpsstat);
        fBsd->SetBranchAddress("ct_np"           , fBsdCtnp);
        fBsd->SetBranchAddress("beam_time"       , fBsdBeamTime);
        fBsd->SetBranchAddress("beam_flag"       , fBsdBeamFlag);
        fBsd->SetBranchAddress("hct"             , fBsdHct);
        fBsd->SetBranchAddress("tpos"            , fBsdTpos);
        fBsd->SetBranchAddress("tdir"            , fBsdTdir);
        fBsd->SetBranchAddress("tsize"           , fBsdTsize);
        fBsd->SetBranchAddress("mumon"           , fBsdMumon);
        fBsd->SetBranchAddress("otr"             , fBsdOtr);
        fBsd->SetBranchAddress("good_gps_flag"   , &fBsdGoodGpsFlag);
        fBsd->SetBranchAddress("trigger_flag"    , &fBsdTriggerFlag);
        fBsd->SetBranchAddress("spill_flag"      , &fBsdSpillFlag);
        fBsd->SetBranchAddress("good_spill_flag" , &fBsdGoodSpillFlag);
        fBsd->SetBranchAddress("target_eff"      , fBsdTargetEff);
        fBsd->SetBranchAddress("run_type"        , &fBsdRunType);
        fBsd->SetBranchAddress("magset_id"       , &fBsdMagsetId);
        IsBsdRead = kTRUE;
    }
    fLog.Form("[%s]\tAdd '%s'\n", __FUNCTION__, ifn);
    PrintDebug(1);
    fBsd->Add(ifn);
    return 0;
}

// __________________________________________________
Int_t Merger::ReadBsds(const Int_t &srun, const Int_t &erun, const char* t2krun, const char* version)
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    TString bsddir, bsdfmt, bsdver;
    bsdver = version;
    bsddir.Form("exp/bsd/%s/%s/", version, t2krun);
    fLog.Form("Read BSDs from '%s, %d - %d'\n", bsddir.Data(), srun, erun);
    PrintDebug(1);
    if (srun > erun) {
        fprintf(stderr, "Error:\tStart# %d > End# %d\n", srun, erun);
        exit(-1);
    }
    // --------------------
    // BSD Run# Memo
    // --------------------
    // t2krun3 / 410001 - 410244
    // t2krun3 / 420001 - 420333
    // t2krun3 / 430001 - 430069
    // t2krun4 / 440001 - 440235
    // t2krun4 / 450001 - 450103
    // t2krun4 / 460001 - 460141
    // t2krun4 / 470001 - 470182
    // t2krun4 / 480001 - 480020
    // t2krun4 / 490001 - 490038
    // --------------------
    // if irun is out of this range, skip it
    
    for (Int_t irun = srun; irun <= erun; irun++) {
        if ( (410001 <= irun && irun <= 410244) ||
             (420001 <= irun && irun <= 420333) ||
             (430001 <= irun && irun <= 430069) ||
             (440001 <= irun && irun <= 440235) ||
             (450001 <= irun && irun <= 450103) ||
             (460001 <= irun && irun <= 460141) ||
             (470001 <= irun && irun <= 470182) ||
             (480001 <= irun && irun <= 480020) ||
             (490001 <= irun && irun <= 490038)
            ) {
            if (bsdver == "v01") {
                ReadBsdsV01(bsddir.Data(), irun);
            }
            else if (bsdver == "v01_2") {
                ReadBsdsV01_2(bsddir.Data(), irun);
            }
            else {
                fprintf(stderr, "Error: Could not find the version specified : %s\n", version);
                exit(-1);
            }
        }
        else {
            // comment out line below when you want to debug.
            // fprintf(stderr, "Warning: BSD Run# %07d : out of range.\n", irun);
        }
    }
    
    Int_t ntrees = fBsd->GetNtrees();
    if (ntrees == 0) {
        fprintf(stderr, "Error:\tfBsd->GetNtrees = 0\n");
        exit(-1);
    }
    TObjArray *fileElements = fBsd->GetListOfFiles();
    fLog.Form("[%s]\tListOfFiles : %d\n", __FUNCTION__, ntrees);
    PrintVerbose(0);
    
    TIter next(fileElements);
    TChainElement *chEl = 0;
    while (( chEl=(TChainElement*)next() )) {
        fLog.Form("[%s]\tListOfFiles\t'%s'\n", __FUNCTION__, chEl->GetTitle() );
        PrintVerbose(1);
    }
    fflush(stdout);
    return 0;
}

// __________________________________________________
Int_t Merger::ReadBsdsV01(const char* bsddir, const Int_t &irun)
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    TString bsdfmt;
    // An ugly tips to sort filename (>_<)
    // But this will make iteration a little bit faster(2m15s ==> 44s)
    bsdfmt.Form("%sbsd_run%07dv01.root", bsddir, irun);
    ReadBsd(bsdfmt.Data());
    bsdfmt.Form("%sbsd_run%07d_?v01.root", bsddir, irun);
    ReadBsd(bsdfmt.Data());
    bsdfmt.Form("%sbsd_run%07d_??v01.root", bsddir, irun);
    ReadBsd(bsdfmt.Data());
    // bsdfmt.Form("bsd/%s/bsd_run%07d*%s.root", t2krun, irun, version);
    // ReadBsd(bsdfmt.Data());
    return 0;
}

// __________________________________________________
Int_t Merger::ReadBsdsV01_2(const char* bsddir, const Int_t &irun)
{
    TString bsdfmt;
    bsdfmt.Form("%sbsd_run%07d_*v01.root", bsddir,irun);
    ReadBsd(bsdfmt.Data());
    return 0;
}

// __________________________________________________
Int_t Merger::Overwrite(const char* ofn)
{
    // Ask if you want to overwrite existing file.
    // DEFAULT : No overwriting
    
    //fLog.Form("[%s]\n", __FUNCTION__);
    //PrintDebug(0);

    IsOverwritable = kFALSE;
    fprintf(stderr, "Do you want to ovewrite? [y/n/q] > ");
    Int_t answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    Int_t r = 0;
    if (answer == 'q' || answer == 'Q') {
        fprintf(stderr, "\tQuit. Bye Bye\n");
        exit(-1);
    }
    else if (answer == 'y' || answer == 'Y') {
        fprintf(stderr, "\tOverwriting '%s'.\n", ofn);
        IsOverwritable = kTRUE;
    }
    else if (answer == '.') {
        fprintf(stderr, "\tQuit loop.\n");
        exit(-1);
        return -1;
    }
    else {
        fprintf(stderr, "\tSkipped '%s'.\n", ofn);
        exit(-1);
        return -1;
    }
    return r;
}

// __________________________________________________
Int_t Merger::OpenDst(const char* ofn, Bool_t kOverwrite = kFALSE )
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);

    if (!kOverwrite) {
        fLog.Form("[%s]\tOverwrite check.\n", __FUNCTION__);
        PrintVerbose(0);
        if (PathExists(ofn, "w") == 0) {
            Overwrite(ofn);
            if (!IsOverwritable) return 0;
        }
    } else {
        fprintf(stderr, "Warning:\t[%s]\tForce overwrite.\n", __FUNCTION__);
    }

    fLog.Form("[%s]\tRecreate '%s'.\n", __FUNCTION__, ofn);
    PrintVerbose(0);
    fout = new TFile(ofn, "recreate");

    fLog.Form("[%s]\tSetBranch for 'dst'.\n", __FUNCTION__);
    PrintVerbose(0);
    fDst    = new TTree("dst","Unpacked Data merged with BSD");
    fMerged = new T2KWCUnpack();
    fDst->Branch("dst.", "T2KWCUnpack", &fMerged, 64000, 99);
    IsDstOpened = kTRUE;
    fflush(stdout);
    return 0;
}

//__________________________________________________
Int_t Merger::CloseDst()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    // if (fInfo) {
    //     printf("CloseDst:\tWriting TTree '%s' to '%s'.\n", fInfo->GetName(), name);
    //     fInfo->Write();
    // }
    if (fDst) {
        fLog.Form("[%s]:\tWriting TTree named '%s'.\n", __FUNCTION__, fDst->GetName());
        PrintDebug(0);
        fDst->Write();
    }
    if (fMap) {
        fLog.Form("[%s]:\tWriting TTree named '%s'.\n", __FUNCTION__, fMap->GetName());
        PrintDebug(0);
        fMap->Write();
    }
    if (fUmode) {
        fLog.Form("[%s]:\tWriting TTree named '%s'.\n", __FUNCTION__, fUmode->GetName());
        PrintDebug(0);
        fUmode->Write();
    }
    
    fLog.Form("[%s]:\tClosing '%s'.\n", __FUNCTION__, fout->GetName());
    PrintDebug(0);
    fout->Close();
    fflush(stdout);
    return 0;
}

// __________________________________________________
Int_t Merger::Clear()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    fAtmNtrgnum.clear();
    fRawPmtsum.clear();
    fRawHitsum.clear();
    fFadcPmtsum.clear();
    fFadcHitsum.clear();
    fFadcNsec.clear();
    fFadcHit.clear();
    fFadcHeight.clear();
    fFadcCharge.clear();
    fFadcPe.clear();
    return 0;
}

// __________________________________________________
Int_t Merger::Write()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    return 0;
}

//__________________________________________________
Int_t Merger::PrintProgress(const Int_t &i, const Int_t &N, const Int_t &L)
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    if ( i%L == 0 || i == N-1) fprintf(stderr, "\r*");
    //if ( i%(L*80) == 0 ) fprintf(stderr, "\n");
    if ( i == N-1) fprintf(stderr, "\n");
    fflush(stderr);
    return 0;
}

// __________________________________________________
char* Merger::ConvertUnixTime(const Int_t utime) const
{
    //fLog.Form("[%s]", __FUNCTION__);
    //PrintDebug(0);
    
    time_t t = utime;
    struct tm* m;
    m = localtime(&t);
    char s[500];
    strftime(s, sizeof(s), "%Y/%m/%d %H:%M:%S", m);
    char *r = s;
    return r;
}

// __________________________________________________
Bool_t Merger::IsSpillOK(const Int_t& upkspill, const Int_t& bsdspill)
{
    //fLog.Form("[%s]\n", __FUNCTION__);
    //PrintDebug(0);
    
    Int_t bsd16bit = (bsdspill+1) & 0xffff;
    Int_t dspill = upkspill - bsd16bit;
    if (dspill == 0) return kTRUE;
    else return kFALSE;
}

// __________________________________________________
Bool_t Merger::IsTimeOK(const Int_t &upktime, const Int_t &bsdtime, const Int_t &delta)
{
    //fLog.Form("[%s]\n", __FUNCTION__);
    //PrintDebug(0);
    
    Int_t dtime = upktime - bsdtime;
    //if ( (-delta < dtime) && (dtime < delta) ) return kTRUE;
    if ( abs(dtime) < delta ) return kTRUE;
    else return kFALSE;
}

// __________________________________________________
Int_t Merger::FillDst()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(1);

    fLog.Form("[%s]\tFill Run Information\n", __FUNCTION__);
    PrintDebug(1);
    // Fill Run Information
    fMerged->Clear();
    fMerged->SetRunNum(fUnpack->GetRunNum());
    fMerged->SetSpillNum(fUnpack->GetSpillNum());
    fMerged->SetRunmode(fUnpack->GetRunmode());
    fMerged->SetTimeSec(fUnpack->GetTimeSec());
    fMerged->SetTimeUsec(fUnpack->GetTimeUsec());
    fMerged->SetFvstat(fUnpack->GetFvstat());
    fMerged->SetDaqmode(fUnpack->GetDaqmode());
    fMerged->SetUpkRun(fUnpack->GetUpkRun());

    fLog.Form("[%s]\tFill Hit Data\n", __FUNCTION__);
    PrintDebug(1);
    // Hit data (T2KWCHit class)
    // hit data must be copied before ATM data (especially atm_nhits),
    // otherwise AddHit will fail due to memory overflow.
    Int_t nhits = fUnpack->GetAtmNhits();
    for(Int_t ihit = 0; ihit < nhits; ihit++) {
        fHit = fUnpack->GetHit(ihit);
#ifdef DEBUG
        if (ihit == 0) fHit->Print("i");
        else fHit->Print();
#endif
        fMerged->AddHit(fHit);
    }

    fLog.Form("[%s]\tFill ATM Data\n", __FUNCTION__);
    PrintDebug(1);
    // ATM data
    fMerged->SetAtmTrg(fUnpack->GetAtmTrg());
    fMerged->SetAtmGong(fUnpack->GetAtmGong());
    fMerged->SetAtmNhits(fUnpack->GetAtmNhits());
    fMerged->SetAtmNtrg(fUnpack->GetAtmNtrg());
    fMerged->SetAtmNcharges(fUnpack->GetAtmNcharges());
    fMerged->SetAtmNpes(fUnpack->GetAtmNpes());
    fMerged->SetAtmMaxCharge(fUnpack->GetAtmMaxCharge());
    fMerged->SetAtmMaxPe(fUnpack->GetAtmMaxPe());

    fLog.Form("[%s]\tFill FADC Data\n", __FUNCTION__);
    PrintDebug(1);
    // FADC data
    fMerged->SetFadcNsamp(fUnpack->GetFadcNsamp());
    fMerged->SetRawPmtsum0(fUnpack->GetRawPmtsum0());
    fMerged->SetRawHitsum0(fUnpack->GetRawHitsum0());
    fMerged->SetFadcPmtsum0(fUnpack->GetFadcPmtsum0());
    fMerged->SetFadcHitsum0(fUnpack->GetFadcHitsum0());
    fMerged->SetFadcNtrg(fUnpack->GetFadcNtrg());
    fMerged->SetFadcNhits(fUnpack->GetFadcNhits());
    fMerged->SetFadcNcharges(fUnpack->GetFadcNcharges());
    fMerged->SetFadcNpes(fUnpack->GetFadcNpes());
    fMerged->SetAverageGain(fUnpack->GetAverageGain());
    for (Int_t i = 0; i < 9; i++) {
        fMerged->SetFadcNchargesBunch(i, fUnpack->GetFadcNchargesBunch(i));
        fMerged->SetFadcNpesBunch(i, fUnpack->GetFadcNpesBunch(i));
    }
    Int_t fadc_nsamp = fUnpack->GetFadcNsamp();
    fLog.Form("[%s]\tFill FADC Nsamp : %d\n", __FUNCTION__, fadc_nsamp);
    PrintDebug(1);
    for (Int_t i = 0; i < fadc_nsamp; i++) {
        fMerged->SetFadcPmtsum(fUnpack->GetFadcPmtsum(i));
        fMerged->SetFadcHitsum(fUnpack->GetFadcHitsum(i));
    }
    Int_t fadc_ntrg = fUnpack->GetFadcNtrg();
    for (Int_t i = 0; i < fadc_ntrg; i++) {
        fMerged->SetFadcNsec(fUnpack->GetFadcNsec(i));
        fMerged->SetFadcHit(fUnpack->GetFadcHit(i));
        fMerged->SetFadcHeight(fUnpack->GetFadcHeight(i));
        fMerged->SetFadcCharge(fUnpack->GetFadcCharge(i));
        fMerged->SetFadcPe(fUnpack->GetFadcPe(i));
    }

    fLog.Form("[%s]\tFill BSD Data\n", __FUNCTION__);
    PrintDebug(1);
    // BSD data
    fMerged->SetBsdRun(fBsdNurun);
    fMerged->SetBsdMidasEvent(fBsdMidasEvent);
    fMerged->SetBsdMrRun(fBsdMrrun);
    fMerged->SetBsdMrShot(fBsdMrshot);
    fMerged->SetBsdSpillNum(fBsdSpillNum);
    
    for (Int_t i = 0; i < 3; i++) {
        fMerged->SetBsdSec(i, fBsdTrigSec[i]);
        fMerged->SetBsdNano(i, fBsdTrigNano[i]);
    }
    
    for (Int_t i = 0; i < 2; i++) {
        fMerged->SetBsdGpsStat(i, fBsdGpsstat[i]);
    }
    
    for (Int_t i = 0; i < 5; i++) {
        for (Int_t j = 0; j < 9; j++) {
            fMerged->SetBsdCt(i, j, fBsdCtnp[i][j]);
            fMerged->SetBsdBeamTime(i, j, fBsdBeamTime[i][j]);
            fMerged->SetBsdBeamFlag(i, j, fBsdBeamFlag[i][j]);
        }
    }
    
    for (Int_t i = 0; i < 3; i++) {
        for (Int_t j = 0; j < 5; j++) {
            fMerged->SetBsdHct(i, j, fBsdHct[i][j]);
        }
    }
    
    for (Int_t i = 0; i < 2; i++) {
        fMerged->SetBsdTpos(i, fBsdTpos[i]);
        fMerged->SetBsdTdir(i, fBsdTdir[i]);
        fMerged->SetBsdTsize(i, fBsdTsize[i]);
    }
    
    for (Int_t i = 0; i < 12; i++) {
        fMerged->SetBsdMumon(i, fBsdMumon[i]);
    }
    
    for (Int_t i = 0; i < 13; i++) {
        fMerged->SetBsdOtr(i, fBsdOtr[i]);
    }
    
    fMerged->SetGoodGpsFlag(fBsdGoodGpsFlag);
    fMerged->SetTriggerFlag(fBsdTriggerFlag);
    fMerged->SetSpillFlag(fBsdSpillFlag);
    fMerged->SetGoodSpillFlag(fBsdGoodSpillFlag);
    
    for (Int_t i = 0; i < 3; i++) {
        fMerged->SetBsdTargetEff(i, fBsdTargetEff[i]);
    }
    
    fMerged->SetBsdRunType(fBsdRunType);
    fMerged->SetBsdMagsetId(fBsdMagsetId);
    // *Br   71 :upk.atm_ntrgnum : vector<Int_t>

    fDst->Fill();
#ifdef DEBUG
    fprintf(stderr, "DEBUG:\t[%s]\tfMerged->Print()\n", __FUNCTION__);
    fMerged->Print();
    Stop();
#endif
    fMerged->Clear();
    return 0;
}

// __________________________________________________
Int_t Merger::Stop()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    fprintf(stderr, "\nStopped. Pres 'q' to quit. > ");
    Int_t answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    if (answer == 'q' || answer == 'Q') {
        if (IsDstOpened) CloseDst();
        fprintf(stderr, "\tQuit. Bye Bye !!!\n");
        exit(-1);
    }
    return 0;
}

// __________________________________________________
Int_t Merger::Jump()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    fprintf(stderr, "\nJump (Pres 'q' to quit) > ");
    char str[81];
    std::cin >> str;
    TString answer = str;
    answer.ToLower();
    if (answer == "q") {
        fprintf(stderr, "\tQuit. Bye Bye !!!\n");
        exit(-1);
    }
    else { return atoi(str); }
}

// __________________________________________________
Int_t Merger::PreMerge()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    if (!IsUpkRead) {
        fprintf(stderr, "Error:\t[%s]\tUPK file not read.\n", __FUNCTION__);
        exit(-1);
    }
    if (!IsBsdRead) {
        fprintf(stderr, "Error:\t[%s]\tBSD files not read.\n", __FUNCTION__);
        exit(-1);
    }
    //if (!IsDstOpened) { fprintf(stderr, "Error:\t[%s]\tDST file not opened.\n"); exit(-1); }
    
    fprintf(stdout, "[%s]\n", __FUNCTION__);
    const Int_t Nupk = fUpk->GetEntriesFast();
    const Int_t Nbsd = fBsd->GetEntries();
    fprintf(stdout, "[%s]:\tNupk: %d\tNbsd: %d\n", __FUNCTION__, Nupk, Nbsd);

    for (Int_t ientry = 0; ientry < Nupk; ientry++) {
        fUpk->GetEntry(ientry);
        Int_t upktime  = fUnpack->GetTimeSec();
        Int_t upkspill = fUnpack->GetSpillNum();
        Int_t upkrun   = fUnpack->GetRunNum();
        greUpkSpill->SetPoint(greUpkSpill->GetN(), upktime, upkspill);
        greUpkRun->SetPoint(greUpkRun->GetN(), upktime, (upkrun%10000)*50);
        greUpkEntry->SetPoint(greUpkEntry->GetN(), upktime, ientry);
        
    }

    for (Int_t ientry = 0; ientry < Nbsd; ientry++) {
        fBsd->GetEntry(ientry);
        Int_t bsdtime  = fBsdTrigSec[0];
        Int_t bsdspill = fBsdSpillNum;
        Int_t bsdrun   = fBsdNurun;
        greBsdSpill->SetPoint(greBsdSpill->GetN(), bsdtime, (bsdspill+1)&0xffff);
        greBsdRun->SetPoint(greBsdRun->GetN(), bsdtime, (bsdrun%10000)*50);
        greBsdEntry->SetPoint(greBsdEntry->GetN(), bsdtime, ientry);
    }

    Int_t kColorUpk = 1, kColorBsd = 2;
    Int_t kMarkerSpill = 4, kMarkerRun = 2;
    
    //TCanvas *c1 = new TCanvas("c1", "c1", 1200, 800);
    //
    greUpkSpill->GetXaxis()->SetTimeDisplay(1);
    greUpkSpill->GetXaxis()->SetTimeFormat("#splitline{%Y/%m/%d}{%H:%M:%S}");
    greUpkSpill->SetMarkerStyle(kMarkerSpill);
    greUpkSpill->SetMarkerColor(kColorUpk);
    greUpkSpill->SetLineColor(kColorUpk);
    //
    greBsdSpill->GetXaxis()->SetTimeDisplay(1);
    greBsdSpill->GetXaxis()->SetTimeFormat("#splitline{%Y/%m/%d}{%H:%M:%S}");
    greBsdSpill->SetMarkerStyle(kMarkerSpill);
    greBsdSpill->SetMarkerColor(kColorBsd);
    greBsdSpill->SetLineColor(kColorBsd);
    greBsdSpill->Draw("ap");
    greUpkSpill->Draw("psame");
    //
    greUpkRun->SetMarkerStyle(kMarkerRun);
    greUpkRun->SetMarkerColor(kColorUpk);
    greUpkRun->SetLineColor(kColorUpk);
    greUpkRun->Draw("psame");
    greBsdRun->SetMarkerStyle(kMarkerRun);
    greBsdRun->SetMarkerColor(kColorBsd);
    greBsdRun->SetLineColor(kColorBsd);
    greBsdRun->Draw("psame");
    //

    TLegend *leg1 = new TLegend(0.1, 0.7, 0.4, 0.9);
    leg1->SetFillColor(0);
    leg1->AddEntry(greUpkSpill, "Upk Spill #", "p");
    leg1->AddEntry(greUpkRun,   "Upk Run #", "p");
    leg1->AddEntry(greBsdSpill, "Bsd Spill #", "p");
    leg1->AddEntry(greBsdRun,   "Bsd Run #", "p");
    leg1->Draw();

    //Int_t xmin = gredSpill->GetXaxis()->GetXmin();
    Int_t xmax = (Int_t)greBsdSpill->GetXaxis()->GetXmax();
    TGaxis *axis_right = new TGaxis(xmax, 0, xmax, 70000, 0, 1400, 005, "+L");
    axis_right->SetTitle("Run [#]");
    //axis_right->SetLineColor(kColorUpk);
    //axis_right->SetLabelColor(kColorUpk);
    //axis_right->SetTitleColor(kColorUpk);
    axis_right->SetLabelFont(132);
    axis_right->SetTitleFont(132);
    axis_right->Draw();

    //TCanvas *c2 = new TCanvas("c2", "c2", 1000, 500);
    greUpkEntry->SetMarkerColor(kColorUpk);
    greUpkEntry->SetLineColor(kColorUpk);
    
    greBsdEntry->SetMarkerColor(kColorBsd);
    greBsdEntry->SetLineColor(kColorBsd);
    greBsdEntry->Draw("ap");
    greUpkEntry->Draw("psame");

    return 0;
}

// __________________________________________________
Int_t Merger::Merge()
{
    fLog.Form("[%s]\n", __FUNCTION__);
    PrintDebug(0);
    
    if (!IsUpkRead) {
        fprintf(stderr, "Error:\t[%s]\tUPK file not read.\n", __FUNCTION__);
        exit(-1);
    }
    if (!IsBsdRead) {
        fprintf(stderr, "Error:\t[%s]\tBSD files not read.\n", __FUNCTION__);
        exit(-1);
    }
    if (!IsDstOpened) {
        fprintf(stderr, "Error:\t[%s]\tDST file not opened.\n", __FUNCTION__);
        exit(-1);
    }

    TString log;  // local variable for log
    
    const Int_t kMargin = 60 * 60 * 24;
    const Int_t Nupk = fUpk->GetEntries();
    const Int_t Nbsd = fBsd->GetEntries();
    fUpk->SetEstimate(Nupk);
    fBsd->SetEstimate(Nbsd);

    fLog.Form("[%s] : [%8d / %8d] | dTime:%d [sec]\n", __FUNCTION__, Nupk, Nbsd, kMargin);
    fprintf(stderr, "%s", fLog.Data());
    PrintVerbose(0);

    Int_t iupk = 0, ibsd = 0;
    while(iupk < Nupk && ibsd < Nbsd) {

        //PrintProgress(iupk, Nupk, 100);

        fLog.Form("[%s]\tfUpk->GetEntry\n", __FUNCTION__);
        PrintDebug(1);
        fUpk->GetEntry(iupk);
        Int_t upktime = fUnpack->GetTimeSec();
        Int_t upkspill = fUnpack->GetSpillNum();
        Int_t upkrun = fUnpack->GetRunNum();
        fHit = new T2KWCHit();
        
        time_t t1 = upktime;
        struct tm *d1 = localtime(&t1);
        char s1[500];
        strftime(s1, sizeof(s1), "%Y/%m/%d %H:%M:%S", d1);

        fLog.Form("[%s]\tfBsd->GetEntry\n", __FUNCTION__);
        PrintDebug(1);
        fBsd->GetEntry(ibsd);
        //Int_t bsdtime  = fBsdTrigSec[0];// - 9*3600;// + fBsdTrigNano[0];
        Int_t bsdtime = fBsdTrigSec[0];
        Int_t bsdspill = fBsdSpillNum;
        Int_t bsdrun = fBsdNurun;
            
        time_t t2 = bsdtime;
        struct tm *d2 = localtime(&t2);
        char s2[500];
        strftime(s2, sizeof(s2), "%Y/%m/%d %H:%M:%S", d2);

        Int_t dtime  = upktime - bsdtime;
        Int_t dspill = upkspill - ((bsdspill+1)&0xffff);

        fLog.Form("[%s]", __FUNCTION__);
        log = fLog;
        fLogTmp.Form(" : [%8d / %8d]", Nupk-iupk, Nbsd-ibsd);
        log += fLogTmp;
        fLogTmp.Form(" | [R] %07d : %07d", upkrun, bsdrun);
        log += fLogTmp;
        fLogTmp.Form(" | [T] %s : %s (%8d)", s1, s2, dtime);
        log += fLogTmp;
        fLogTmp.Form(" | [S] %8d : %8d (%8d)", upkspill, (bsdspill+1)&0xffff, dspill);
        log += fLogTmp;

        if ( (iupk == 0 && ibsd == 0) || iupk == Nupk-1 || ibsd == Nbsd-1) {
            fprintf(stderr, "%s\n", log.Data());
            PrintVerbose(0, log + "\n");
        }
        
        fLog.Form("[%s]\tIsTimeOK && IsSpillOK\n", __FUNCTION__);
        PrintDebug(1);
        if (IsTimeOK(upktime, bsdtime, kMargin) && IsSpillOK(upkspill, bsdspill)) {
            FillDst();
            iupk++;
            ibsd++;
            fLogTmp.Form(" ==> Matched\n");
        }
        else {
            // [T] 2012/04/08 22:45:01 : 2012/04/08 20:58:31 (    6390)| [S]    62871 :    62855 (      16) --> ibsd++; until first matching
            // [T] 2012/04/09 04:15:20 : 2012/04/09 03:59:05 (     975)| [S]     4504 :     4503 (       1) --> ibsd++; lack of spill in rawdata
            // [T] 2012/04/09 01:53:05 : 2012/04/08 22:34:05 (   11940)| [S]     1677 :    63027 (  -61350) --> ibsd++; bsdspill# goes around
            // [T] 2012/04/12 14:35:03 : 2012/04/12 13:53:18 (    2505)| [S]    44101 :    44105 (      -4) --> iupk++; need to increase upkspill#

            // [T] 2012/04/20 09:33:01 : 2012/04/20 10:20:21 (   -2840)| [S]    62162 :    63363 (   -1201) --> iupk++;
            // [T] 2012/04/20 10:40:41 : 2012/04/20 13:10:25 (   -8984)| [S]    63379 :     1205 (   62174) --> iupk++;
            if (dtime >=0) {
                // -1000 < dspill < 0
                if (-1000 < dspill && dspill < 0) {
                    iupk++;
                    fLogTmp.Form(" iupk1++\n");
                }
                else {
                    ibsd++;
                    fLogTmp.Form(" ibsd1++\n");
                }
            }
            else {
                iupk++;
                fLogTmp.Form(" iupk2++\n");
                //if (0 < dspill) { ibsd++; fprintf(stdout, " ibsd2++\n"); }
                //else                             { iupk++; fprintf(stdout, " iupk2++\n"); }
            }
        }
        log += fLogTmp;
        PrintDebug(1, log.Data());
        
    }  // End (iupk < Nupk && ibsd < Nbsd)
    fflush(stdout);
    
    return 0;
}

