/**********************************************************************
 *  scaler.h                                                          *
 *          :header file for VME Visual Scaler                        *
 *                                                                    *
 *       (1) May-27-1994  First definition by M.Shiozawa (I.C.R.R)   *
 **********************************************************************/
#include <stdio.h>
#include <sys/types.h>

#ifndef SCALER_H_ALREADY_INCLUDED
#define SCALER_H_ALREADY_INCLUDED

#define SCALER_BASE 0xE000

#define SCLR_CH0        0
#define SCLR_CH1        1
#define SCLR_CH2        2
#define SCLR_CH3        3
#define SCLR_CH4        4
#define SCLR_CH5        5
#define SCLR_CH6        6
#define SCLR_CH7        7

#define SCLR_RESET   0xFF

#endif  //SCALER_H_ALREADY_INCLUDED

