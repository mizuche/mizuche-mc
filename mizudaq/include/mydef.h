/*****************************************************************
 *  mydef.h                                                      *
 *          :private standard header ANSI version                *
 *                                                               *
 *       (1) Sep.25.1992 First definition by M.Shiozawa          *
 *       (2) Feb.27.1992 Extend for K&R vesion by M.Shiozawa     *
 *       (2) Apr.29.1994 File name changed by M.Shiozawa         *
 *****************************************************************/


/*--- Include this file only if have not already included ---*/
#ifndef STDDEF_H_INCLUDED_

#define STDDEF_H_INCLUDED_


typedef unsigned char   byte;
typedef unsigned char   string;
typedef unsigned short  word;
typedef unsigned int    dword;
typedef          char   int8;
typedef          short  int16;
typedef          int    int32;

#ifndef NULL
#define NULL 0
#endif  //NULL

#define cNULL '\n'

#ifndef TRUE
#define TRUE  1
#endif
#define YES   1
#define ON    1
#ifndef FALSE
#define FALSE (!TRUE)
#endif
#define NO    0
#define OFF   0

#ifndef EOF
#define EOF   -1
#endif  //EOF

#ifndef ERROR
#define ERROR -1
#endif  //ERROR

#ifdef DEBUG
#define dprintf printf
#endif

#endif  //STDDEF_H_INCLUDED_




