#ifndef _TRawData_h_
#define _TRawData_h_

#include <iostream>
#include <unistd.h>

#include <TObject.h>
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>

#define PEDESTAL_MODE 0
#define OPERATE_MODE 1

#define MAX_nTRG 250
#define MAX_nATM 12000 // --> 1013 / 1event
#define MAX_nFADC 20000 //temporary


//#define OLDVER // for data before 8/26
#define DEBUG_TRAW

// __________________________________________________
typedef struct {
    int nTRG;
#ifdef OLDVER
    unsigned short TRG[MAX_nTRG]; 
#else
    unsigned short TRG[MAX_nTRG][5];
#endif
} trg_buff;

// __________________________________________________
typedef struct {
    int nATM;
    unsigned int ATM[MAX_nATM];
} atm_buff;

// __________________________________________________
typedef struct {
    int nFADC;
    unsigned int FADC[MAX_nFADC];
} fadc_buff;

// __________________________________________________
typedef struct{
    int run;
    int mode;
    int time_sec;
    int time_nano;
    //int spill;
    //unsigned int spill;
    unsigned short spill;
    trg_buff trg;
    atm_buff atm;
    fadc_buff fadc;
} mizu_raw_data;

// __________________________________________________
typedef struct{
    int atm_thr; 
    int fadc_off;
} mizu_info;

// __________________________________________________
class TRawData {
private:
	mizu_raw_data raw;
    mizu_info info;
    Bool_t Overwrite;
	TTree *traw;
	TTree *traw_send;
    TTree *tinf;
	TFile *fout;

public:
    TRawData(const char* file_name, Bool_t flag);
	~TRawData();
	void Init();
    void Fill(mizu_raw_data r_data);
    void SetInfo(mizu_info info0);
    void Write();
    TTree* GetRawTree() { return traw_send; }
    //TTree* GetRawTree() { return (TTree*)traw_send->CloneTree(); }

//    void GetRawTree(TTree *tree) {
//        tree = (TTree*)traw_send->Clone("r_mizu");
//    }

    void ResetSendTree() { traw_send->Reset(); }
};


// ////////////////////////////////////////////////////////////////////////////////

// __________________________________________________        
TRawData::TRawData(const char* file_name, Bool_t flag=kFALSE) 
{
#if defined(DEBUG) || defined(DEBUG_TRAW)
    printf("[%s]\tTRawData constructor\n", __FUNCTION__);
#endif
    FileStat_t info;
    if (gSystem->GetPathInfo(file_name, info)==0) {
        fprintf(stderr, "Error:\tFile '%s' already exists. Quit and avoid overwriting.\n", file_name);
        exit(1);
    }
    
    fout = new TFile(file_name,"recreate");
    traw = new TTree("r_mizu","Raw Mizuche Data");
    tinf = new TTree("info","Run Information");
    Overwrite = flag;
    Init();
}

// __________________________________________________
TRawData::~TRawData()
{
#if defined(DEBUG) || defined(DEBUG_TRAW)
    printf("[%s]\tTRawData destructor\n", __FUNCTION__);
#endif    
    if( (fout && traw && tinf) && (!Overwrite) ) Write();
}


// __________________________________________________
void TRawData::Init()
{
    // Tree for raw data
    traw->Branch("run",  &(raw.run),  "run/I");
    traw->Branch("mode",  &(raw.mode),  "mode/I");
    //traw->Branch("spill", &(raw.spill), "spill/I");
    traw->Branch("spill", &(raw.spill), "spill/s");
    traw->Branch("time_sec", &(raw.time_sec), "time_sec/I");
    traw->Branch("time_nano", &(raw.time_nano), "time_nano/I");
#ifdef OLDVER
    traw->Branch("TRG",   &(raw.trg),   "nTRG/I:TRG[nTRG]/s");
#else
    traw->Branch("TRG",   &(raw.trg),   "nTRG/I:TRG[nTRG][5]/s");
#endif
    traw->Branch("ATM",  &(raw.atm), "nATM/I:ATM[nATM]/i");
    traw->Branch("FADC",  &(raw.fadc), "nFADC/I:FADC[nFADC]/i");

    // dead copy for send
    traw_send = (TTree*)traw->Clone("r_mizu");
/*
  traw_send->Branch("run",  &(raw.run),  "run/I");
  traw_send->Branch("mode",  &(raw.mode),  "mode/I");
  traw_send->Branch("spill", &(raw.spill), "spill/s");
  #ifdef OLDVER
  traw_send->Branch("TRG",   &(raw.trg),   "nTRG/I:TRG[nTRG]/s");
  #else
  traw_send->Branch("TRG",   &(raw.trg),   "nTRG/I:TRG[nTRG][5]/s");
  #endif
  traw_send->Branch("ATM",  &(raw.atm), "nATM/I:ATM[nATM]/i");
  traw_send->Branch("FADC",  &(raw.fadc), "nFADC/I:FADC[nFADC]/i");
*/
    
    // Tree for run info
    //
    tinf->Branch("atm_thr" , &(info.atm_thr) , "atm_thr/I");
    tinf->Branch("fadc_off", &(info.fadc_off), "fadc_off/I");

#if defined(DEBUG) || defined(DEBUG_TRAW)
    printf("[%s]\tSet output tree branch\n", __FUNCTION__);
#endif    
}

// __________________________________________________
void TRawData::Fill(mizu_raw_data r_data)
{
    raw = r_data;
    traw->Fill();
//    traw_send->Fill();  
//  traw->Print();
//  traw_send->Print();

    // -> if "Overwrite==kTRUE", the more time in one event than 50msec
    if(Overwrite)
        traw->Write("",TObject::kOverwrite);
#if defined(DEBUG) || defined(DEBUG_TRAW)
    printf("[%s]\tFill raw data\n", __FUNCTION__);
#endif
}

// __________________________________________________
void TRawData::SetInfo(mizu_info info0)
{
    info = info0;
    tinf->Fill();
    // -> if "Overwrite==kTRUE", the more time in one event than 50msec
    if(Overwrite)
        tinf->Write("",TObject::kOverwrite);
}

// __________________________________________________
void TRawData::Write()
{
    fout->cd();
    if(!Overwrite) {
        traw->Write();
        tinf->Write();
    }
    fout->Close();
#if defined(DEBUG) || defined(DEBUG_TRAW)
    printf("[%s]\tWrite TTree\n", __FUNCTION__);
#endif    
}

#endif
