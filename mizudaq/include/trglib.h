/**********************************************************************
 *  trglib.h                                                          *
 *          :Header file for VME-TRG                                  *
 *                                                                    *
 *       (1) Aug.-10-1993  First definition by M.Shiozawa (I.C.R.R)   *
 *       (2) Nov.-12-1994  modified for new VME-TRG by M.Shiozawa     *
 **********************************************************************/
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>

#ifndef TRGLIB_H_ALREADY_INCLUDED
#define TRGLIB_H_ALREADY_INCLUDED

#define TRG_BASE       0x1000
#define TRG_MAP_SIZE   0x10

typedef struct TRGREG {
    u_short csr;
    u_short mask;
    u_short fifo_count;
    u_short fifo_a;
    u_short fifo_b;
    u_short fifo_c;
    u_short fifo_d;
    u_short fifo_e;
}TRGREG;  

typedef struct TRG {
    u_short fifo_a;
    u_short fifo_b;
    u_short fifo_c;
    u_short fifo_d;
    u_short fifo_e;
}TRG;  

#define TRG_DTMODE   0x0001
#define TRG_TRGENA   0x0002
#define TRG_TRGRST   0x0010
#define TRG_CNTCLR   0x0020
#define TRG_CLKCLR   0x0040
#define TRG_FIFCLR   0x0080
#define TRG_TRIG_0   0x0100
#define TRG_TRIG_1   0x0200
#define TRG_TRIG_2   0x0400
#define TRG_TRIG_3   0x0800
#define TRG_TRIG_4   0x1000
#define TRG_TRIG_5   0x2000
#define TRG_TRIG_6   0x4000
#define TRG_TRIG_7   0x8000

#define TRG_MEMFUL   0x0004
#define TRG_TRGVET   0x0008

#define TRG_MAX_EVENT 4096

#define TRG_TRGGER_ID(x)  (u_short)(x&0x000000FF)
#define TRG_COUNT_DATA(x) (u_short)(x&0x0000FFFF)

#endif //TRGLIB_H_ALREADY_INCLUDED
