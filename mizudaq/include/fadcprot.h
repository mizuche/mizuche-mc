#include <sys/types.h>

#ifndef FADCPROT_H_ALREADY_INCLUDE
#define FADCPROT_H_ALREADY_INCLUDE


void show_with_2bit(unsigned int val);
void write_mem(int fd, unsigned int address, unsigned int word);
void read_mem(int fd, unsigned int address, unsigned int *word, unsigned int n_array);
void set_run_mode(int fd, unsigned int run_ctrl_reg);
void wait_fadc_trigger();
void read_data_single32(int fd, unsigned int address, unsigned int *word, unsigned int read_n_array);
//void read_fadc_data(int fd, unsigned int *data_word);
int read_fadc_data(int fd, unsigned int *wdata);
//void clear_all_ch_config(int fd);

void FADC_init(unsigned int);
void FADC_daq_mode(int mode);
void FADC_daq_start();
void FADC_daq_stop();
int FADC_Read(unsigned int *wdata);
int FADC_close();

#endif //FADCPROT_H_ALREADY_INCLUDE
