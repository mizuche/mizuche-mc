#include <sys/types.h>

#ifndef MYUTILITY_H_ALREADY_INCLUDE
#define MYUTILITY_H_ALREADY_INCLUDE

void short_2bit(short val);
void int_2bit(int val);
int ss_sleep(int millisec);
double gettimeofday_usec();
double gettimeofday_sec();
double gettimeofday_sec_usec(int *sec, int *usec);

#endif 
