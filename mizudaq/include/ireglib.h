/*
 * ireglib.h,v 1.1 1994/11/23 Masato Shiozawa
 *          : header file for IREG library
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/uio.h>
#include <sys/time.h>

#ifndef IREGLIB_H_ALREADY_INCLUDED
#define IREGLIB_H_ALREADY_INCLUDED

#define INTREG_BASE 0x1000
#define INTREG_INT_VECTOR  0xfff0
#define INTREG_IRQ  5
#define INTREG_TIMEOUT  10
#define INTREG_USESIG   SIGUSR1

int       ireg_vme_fd;
u_short   ireg_signal_number = 0;
u_short   ireg_vector_received = 0;
u_int     ireg_int_receive_num = 0;
u_int     ireg_waitevent_timeout;
sigset_t  ireg_blockmask, ireg_waitmask;

#endif //IREGLIB_H_ALREADY_INCLUDED




