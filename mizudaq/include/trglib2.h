#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/page.h>

#ifndef TRGLIB_H_READY_INCLUDE
#define TRGLIB_H_READY_INCLUDE

#define TRG_BASE  0x1000
#define TRG_MAP_SIZE 10

#define a16_logical_name "/dev/vmedrv16d16"

typedef struct {
  unsigned short csr;
  unsigned short trig_mask;
  unsigned short fifo_ctr;
  unsigned short event_no;
  unsigned short trig_id;
  unsigned short clk48h;
  unsigned short clk48m;
  unsigned short clk48l;
} TRG_REGS_STR;


typedef struct {
  unsigned int event_no;
  unsigned int trig_id;
  unsigned long long clk48;
} TRG_INFO;

int btd;
TRG_REGS_STR *ptrg;
size_t map_size;
volatile caddr_t trg_vme_base;

#endif
