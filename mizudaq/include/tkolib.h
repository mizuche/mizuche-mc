/*
 * tkolib.h,v 1.1 1994/5/10 Masato Shiozawa
 *          : header file for TKO library
 *     v 1.1: 1994/5/10
 *            first definition by Masato Shiozawa(ICRR)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/uio.h>
#include <sys/time.h>
//#include <sys/btio.h>

#include "smpreg.h"
#include "mydef.h"

#ifndef TKOLIB_H_ALREADY_INCLUDED
#define TKOLIB_H_ALREADY_INCLUDED


/*
 * Change a number SMP_ENTRY to a number of the SMP you installed.
 * And set variable smp_base_address[#]'s to your selected base-address
 * of the SMP. 
 */

/*
//####### default setting (when get from hiraide-san) #########
#define SMP_MAX_ENTRY 20
extern u_int smp_base_address[SMP_MAX_ENTRY] 
  = { 0x70000000, 0x70200000, 0x70400000, 0x70600000, 0x70800000
	, 0x70a00000, 0x70c00000, 0x70e00000, 0x71000000, 0x71200000
	  , 0x71400000, 0x71600000, 0x71800000, 0x71a00000, 0x71c00000
	    , 0x71e00000, 0x72000000, 0x72200000, 0x72400000, 0x72600000};
u_int smp_page_number[SMP_MAX_ENTRY]; 
u_int smp_mask;
#define TKO_PAGE_SIZE (1<<20)
#define TKO_PAGE_MASK (TKO_PAGE_SIZE-1)
#define TKO_PAGE_NUM  0x7000
SMP *smp[SMP_MAX_ENTRY];
//##########################################################
*/

//########### mizuche setting #############

#define SMP_MAX_ENTRY 1
//extern u_int smp_base_address[SMP_MAX_ENTRY] = { 0x20000000 };
const u_int smp_base_address[SMP_MAX_ENTRY] = { 0x20000000 };
u_int smp_page_number[SMP_MAX_ENTRY]; 
u_int smp_mask;
#define TKO_PAGE_SIZE (1<<20)
#define TKO_PAGE_MASK (TKO_PAGE_SIZE-1)
#define TKO_PAGE_NUM  0x7000
SMP *smp[SMP_MAX_ENTRY];

//#########################################


#define DMA_THR_SIZE  (1<<13)
#define POLL_SIZE_THR (1)
/* for old sukon[1-9]
#define POLL_SIZE_THR (1<<21)
*/

#define TKO_USESIG  SIGUSR1
#define TKO_TIMEOUT 10 /* [sec] */
#define INTR_GENERATOR 0

/*
#define EVENT_NUMBER_THR  (1<<9)
#define DATA_SIZE_THR     0x1000
*/

#define EVENT_NUMBER_THR  (1<<2)
#define DATA_SIZE_THR     (1<<19)
/*
  #define EVENT_NUMBER_THR  (1<<6)
*/
/***************************************/
/*#define EVENT_NUMBER_THR  (1<<4)
  #define DATA_SIZE_THR     (1<<19)*/
/***************************************/

#define EVENT_NUMBER_MASK 0
#define DATA_SIZE_MASK    0
/*
#define EVENT_NUMBER_MASK SMP_EVENTNUM_SWITCH_MASK
#define DATA_SIZE_MASK    SMP_DATASIZE_SWITCH_MASK
*/


#define SMP_SELECTED(x) (SMP_mask&(1<<x))? 0 : 1

int       tko_vme_fd;
caddr_t   tko_vme_base;
int       SMP_Entry;
int       SMP_select_Entry;
int       SMP_mask;
u_short   tko_signal_number = 0;
u_short   tko_vector_received = 0;
u_int     tko_int_receive_num = 0;
u_int     tko_waitevent_timeout;
sigset_t  tko_blockmask, tko_waitmask;
u_int     event_number_thr, data_size_thr;
u_int     event_number_mask, data_size_mask;
int       intr_generator;

//####### added by akira.m #######
const int max_smp_data = 10000; // temporary
double atm_thr;
//################

#endif //TKOLIB_H_ALREADY_INCLUDED

