/*
 * dscrprot.h,v 1.1 1995/01/05 Masato Shiozawa
 *
 *     v 1.1: 1995/01/05
 *            first definition by Masato Shiozawa(ICRR)
 */
#include "discri.h"

#ifndef DSCRPROT_H_ALREADY_INCLUDED
#define DSCRPROT_H_ALREADY_INCLUDED

int DSCR_Open();
int DSCR_open();
int DSCR_Close();
int DSCR_close();
int DSCR_Set_Level( int channel, short level );
int DSCR_set_level( int channel, short level );
int DSCR_Set_Width( u_short width );
int DSCR_set_width( u_short width );

#endif //DSCRPROT_H_ALREADY_INCLUDED

