/*
 * smpreg.h,v 1.1 1994/2/14 Masato Shiozawa
 *          : header file for VME-SMP module
 *     v 1.1: 1994/2/14
 *            first definition by Masato SHiozawa(ICRR)
 */

#include <sys/types.h>

#ifndef SMPREG_H_ALREADY_INCLUDED
#define SMPREG_H_ALREADY_INCLUDED

#define SMP_MAP_SIZE (1<<21) /* 2 Mbyte */
#define SMP_MEM_SIZE (1<<20) /* 1 Mbyte */

typedef struct SMPREG{
    u_int csr;
#define SMP_SDS_GO      (1<<5)
#define SMP_BOARD_RESET (1<<6)
#define SMP_SQ_BUSY(x)  ((x>>5)&1)
#define SMP_TKO_TOUT(x) ((x>>15)&1)
#define SMP_TKO_Q(x)    ((x>>6)&1)
    u_int snccr;
#define SMP_SCOUNT_CLEAR (1<<1)
    u_int sncr;
    u_int dsr;
    u_int enr;
    u_int bcr;
#define SMP_DATASIZE_SWITCH_MASK (1<<0)
#define SMP_EVENTNUM_SWITCH_MASK (1<<1)
#define SMP_SWITCH_AUTO_CLR      (1<<4)
#define SMP_SWITCH_ENABLE        (1<<5)
#define SMP_SWITCH_FLG_AUTO_CLR  (1<<6)
#define SMP_SWITCH_FLAG          (1<<7)
#define SMP_SWITCH_EXEC          (1<<8)
#define SMP_CURRENT_BUFFER(x)    (x>>9)&1
    u_int scr;
#define SMP_MAX_EVENT_NUMBER_THR ((1<<10)-(1<<2))
#define SMP_MAX_DATA_SIZE_THR    ((1<<20)-(1<<12)) /* [byte] */
    u_int icr;
#define SMP_IRQ_LEVEL0        0x0000
#define SMP_IRQ_LEVEL1        0x0001
#define SMP_IRQ_LEVEL2        0x0002
#define SMP_IRQ_LEVEL3        0x0003
#define SMP_IRQ_LEVEL4        0x0004
#define SMP_IRQ_LEVEL5        0x0005
#define SMP_IRQ_LEVEL6        0x0006
#define SMP_IRQ_LEVEL7        0x0007
#define SMP_INT_AUTO_CLR      (1<<4)
#define SMP_INT_ENABLE        (1<<5)
#define SMP_INT_FLG_AUTO_CLR  (1<<6)
#define SMP_INT_FLAG          (1<<7)
    u_int ivr;
#define SMP_INT_VECTOR        0x78
}SMPREG;

// TKO adress space (SMP spec doc p.6)
// SA(Sub Address)[10:0]=2048
// MA(Module Address)[4:0]=32
// BC(Broad Cast) = 2
typedef struct TKO_SA{
    u_int sa[2048];
}TKO_SA;
typedef struct TKO_MA{
    TKO_SA ma[32];
}TKO_MA;
typedef struct TKOMAP{
    TKO_MA bc[2];
}TKOMAP;

typedef struct SMP{
    u_int  memory[(SMP_MEM_SIZE>>2)];
    TKOMAP tko;
    SMPREG reg;
}SMP;

/*
typedef struct SMP{
    u_int  memory[0x40000];
    u_int  tko[0x1C800];
    char   dummy[0xE000];
    SMPREG reg;
}SMP;
*/

#endif //SMPREG_H_ALREADY_INCLUDED
