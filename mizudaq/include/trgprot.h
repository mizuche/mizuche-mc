/*
 * trgprot.h,v 1.1 1994/11/23 Masato Shiozawa
 *
 *     v 1.1: 1994/11/23
 *            first definition by Masato Shiozawa(ICRR)
 */

//#include <sys/types.h>
#include "trglib.h"

#ifndef TRGPROT_H_ALREADY_INCLUDED
#define TRGPROT_H_ALREADY_INCLUDED

int TRG_open( void );
int TRG_Open( void );
int TRG_close( void );
int TRG_Close( void );

int TRG_read( int offset, u_short *sdata );
int TRG_write( int offset, u_short *sdata );

int TRG_command( u_short value );
int TRG_Command( u_short value );

u_short TRG_status( void );
u_short TRG_Status( void );

int TRG_mask( u_short value );
int TRG_Mask( u_short value );

u_short TRG_FIFO_Count( void );
u_short TRG_FIFO_Count( void );

u_int TRG_Read( TRG *trg, u_int max_event );

#endif //TRGPROT_H_ALREADY_INCLUDED

