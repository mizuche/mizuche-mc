#ifndef _DATA_HEADER_H__
#define _DATA_HEADER_H__

#include <iostream>
#include <unistd.h>

#define MAX_nTRG 100
#define MAX_nATM 3000
#define MAX_nFADC 1000

#define PEDESTAL_MODE 0
#define OPERATE_MODE 1

//
typedef struct {
  int nTRG;
  unsigned short TRG[MAX_nTRG];
} trg_buff;

typedef struct {
  int nATM;
  unsigned int ATM[MAX_nATM];
} atm_buff;

typedef struct {
  int nFADC;
  unsigned int FADC[MAX_nFADC];
} fadc_buff;

typedef struct{
  trg_buff trg;
  atm_buff s_atm;
  atm_buff p_atm;
  fadc_buff fadc;
  int mode;
} mizu_raw_data;

#endif
