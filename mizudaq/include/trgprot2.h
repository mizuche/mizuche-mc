#include <sys/types.h>

#ifndef TRGPROT_H_ALREADY_INCLUDE
#define TRGPROT_H_ALREADY_INCLUDE

unsigned short TRG_dump();
void TRG_reset();
void TRG_enable();
void TRG_open();
void TRG_close();

#endif
