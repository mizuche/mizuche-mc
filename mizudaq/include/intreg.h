/**********************************************************************
 *  intreg.h                                                          *
 *          :header file for VME Interrupt & I/O Register             *
 *                                                                    *
 *       (1) Oct.-15-1993  First definition by M.Shiozawa (I.C.R.R)   *
 **********************************************************************/
#include <stdio.h>
#include <sys/types.h>

#ifndef INTREG_H_ALREADY_INCLUDED
#define INTREG_H_ALREADY_INCLUDED

/*
 * define internal register of module.
 */

#define IREG_LATCH0     0
#define IREG_LATCH1     1
#define IREG_FLIP_FLOP  2
#define IREG_IN         3
#define IREG_PULSE      4
#define IREG_OUT        5
#define IREG_CSR1       6
#define IREG_CSR2       7

typedef struct INTREG{
    u_short latch0;
    u_short latch1;
    u_short flip_flop;
    u_short in;
    u_short pulse;
    u_short out;
#define IREG_BIT0           (1<<0)
#define IREG_BIT1           (1<<1)
#define IREG_BIT2           (1<<2)
#define IREG_BIT3           (1<<3)
#define IREG_BIT4           (1<<4)
#define IREG_BIT5           (1<<5)
#define IREG_BIT6           (1<<6)
#define IREG_BIT7           (1<<7)

    u_short csr0;
#define INTREG_CLR3    (1<<0)
#define INTREG_CLR1    (1<<1)
#define INTREG_MASK1   (1<<3)
#define INTREG_ENABLE1 (1<<4)
#define INTREG_BUSY1   (1<<5)
#define INTREG_ENABLE3 (1<<6)
#define INTREG_BUSY3   (1<<7)

    u_short csr1;
#define INTREG_CLR3    (1<<0)
#define INTREG_CLR2    (1<<1)
#define INTREG_MASK2   (1<<3)
#define INTREG_ENABLE2 (1<<4)
#define INTREG_BUSY2   (1<<5)
#define INTREG_ENABLE3 (1<<6)
#define INTREG_BUSY3   (1<<7)

}INTREG;

#endif  //INTREG_H_ALREADY_INCLUDED
