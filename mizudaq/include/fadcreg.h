/************************** 
 *     fadcreg.h   
 *    
 *        The structure arrays for MV8500
 *
 *  v1.0  2001/5/29
 *        First Definition by Hiroshi Nishihama (T.I.Tech) 
 *
 ****************************************************/ 

#include <sys/types.h>

#ifndef FADCREG_H_ALREADY_INCLUDED
#define FADCREG_H_ALREADY_INCLUDED

#define FADC_MAP_SIZE (1<<19)  /* 512kByte */
#define FADC_MEM_SIZE (1<<18)  /* 256k */

/*
#define FADC_SELECTED(x) (FADC_mask&(1<<x))? 0 : 1
*/

#define FADC_CSR 1
#define FADC_BCR 2
#define FADC_ICR 3
#define FADC_IVR 4

#define FADC_OVW 0
#define FADC_OR  1

typedef struct FADCREG{
  u_int csr;
#define FADC_MEASCTRL     (1<<0)
#define FADC_COMPMOD      (1<<1)
#define FADC_MEASMOD_00   (0<<0)
#define FADC_MEASMOD_01   (1<<2)
#define FADC_MEASMOD_10   (1<<3)
#define FADC_CLKSEL       (1<<4)
#define FADC_ECDSEL       (1<<6)
#define FADC_IECCLR       (1<<7)
#define FADC_RESET        (1<<15)
  u_int bcr;
#define FADC_RDBSEL       (1<<0)
#define FADC_ECOE         (1<<1)
#define FADC_WSOE         (1<<2)
#define FADC_EBCE         (1<<3)
#define FADC_BSEAC        (1<<4)
#define FADC_BSE          (1<<5)
#define FADC_TBFAC        (1<<6)
#define FADC_RDBS         (1<<11)
#define FADC_ECOF         (1<<12)
#define FADC_WSOF         (1<<13)
#define FADC_EBCF         (1<<14)
#define FADC_BRF          (1<<15)
  u_int icr;
#define FADC_IRQLVL0      0x0000
#define FADC_IRQLVL1      0x0001
#define FADC_IRQLVL2      0x0002
#define FADC_IRQLVL3      0x0003
#define FADC_IRQLVL4      0x0004
#define FADC_IRQLVL5      0x0005
#define FADC_IRQLVL6      0x0006
#define FADC_IRQLVL7      0x0007
#define FADC_IEAC         (1<<4)
#define FADC_IRQENB       (1<<5)
#define FADC_TIFAC        (1<<6)
#define FADC_TIF          (1<<7)
  u_int ivr;
  //#define FADC_INT_VECTOR         0xff78
#define FADC_INT_VECTOR         0xffff
  u_int npr;
#define FADC_MAX_PRESAMP_NUMBER_THR 8176
  u_int ecr;
#define FADC_MAX_EC_THR   0x2aab   /* 10923 */
  u_int wsr;
#define FADC_MAX_WORD_THR 0x1fffe  /* 128k-2 */
  u_int clksr;
#define FADC_CLK_M(x) ((x>>0)&0x01ff)
#define FADC_CLK_N(x) ((x>>9)&0x0007)
#define FADC_CLK_S(x) ((x>>13)&0x0007)
}FADCREG;

typedef struct DAC{
#define MAX_DCR_DAC 255
  u_int dcr[8];
#define MAX_TLR_DAC 255
  u_int tlr[8];
#define MAX_PDR_DAC 255
  u_int pdr[8];
}DAC;

typedef struct APPEND{
  u_int bir;
  u_int vr;
}APPEND;

typedef struct FADC_BUFFER{
  u_int memory[(FADC_MEM_SIZE>>2)];
  FADCREG reg;
  char dummy1[0xE0];
  DAC dac;
  char dummy2[0xA0];
  APPEND append;
  char dummy3[0xF8];
  u_int rwbuf[0x40];
}FADC_BUFFER;


#endif //FADCREG_H_ALREADY_INCLUDED
