/*
 * atmvar.h,v 1.1 1993/12/17 Masato Shiozawa
 *
 *     v 1.1: 1993/09/04
 *            first definition by Masato SHiozawa(ICRR)
 *            original is tkovar.h
 */



#ifndef ATMVAR_H_ALREADY_INCLUDED
#define ATMVAR_H_ALREADY_INCLUDED


/* --- definition for ATM --- */
#define ATM_SA             0

#define ATM_READ_F         0
#define ATM_READ_MODE_F    3
#define ATM_WRITE_F        8
#define ATM_WRITE_THRDAC_F 9
#define ATM_WRITE_MODE_F   11
#define ATM_FIFO_CLEAR_F   12
#define ATM_WRITE_CALCH_F  13
#define ATM_INITIALIZE_F   14

#define ATM_OPERATE_MODE   0
#define ATM_PEDESTAL_MODE  1
#define ATM_CAL_MODE       2
#define ATM_MEMTEST_MODE   3

#define ATM_FIRST_TAG(x)     (u_short)((((~x)&0x00008000)>>15)*(((~x)&0x00004000)>>14))
#define ATM_SECOND_TAG(x)    (u_short)((((~x)&0x00008000)>>15)*((  x &0x00004000)>>14))
#define ATM_THIRD_TAG(x)     (u_short)(((  x &0x00008000)>>15)*(((~x)&0x00004000)>>14))
#define ATM_TQ_DATA(x)       (u_short)( x&0x00000FFF)
#define ATM_COUNT_DATA(x)    (u_short)(((u_int)x&0x00003FC0)>>6)
#define ATM_CHANNEL_DATA(x)  (u_short)( (u_int)x&0x0000003F)
#define ATM_AB_DATA(x)       (u_short)(((u_int)x&0x00000020)>>5)
#define ATM_THRDAC(x)        (u_short)(((float)x+1250.0)*2047.0/1250.0) 
/* "x"[mV] must be negative number */

#endif //ATMVAR_H_ALREADY_INCLUDED

