/*
 * iregprot.h,v 1.1 1994/11/23 Masato Shiozawa
 *
 *     v 1.1: 1994/11/23
 *            first definition by Masato Shiozawa(ICRR)
 */
#include <sys/types.h>

#ifndef IREGPROT_H_ALREADY_INCLUDED
#define IREGPROT_H_ALREADY_INCLUDED

int IREG_write( int ireg_fd, int offset, u_short *sdata );
int IREG_read( int ireg_fd, int offset, u_short *sdata );
int IREG_close( int ireg_fd );
int IREG_open( u_int ireg_base );
int IREG_latch1_read( int ireg_fd, u_short *value );
int IREG_latch2_read( int ireg_fd, u_short *value );
int IREG_ff_read( int ireg_fd, u_short *value );
int IREG_through_read( int ireg_fd, u_short *value );
int IREG_pulse_out( int ireg_fd, u_short value );
int IREG_level_out( int ireg_fd, u_short value );
int IREG_waitevent( int ireg_fd, int enable, int timeout );
int IREG_reset( int ireg_fd );
void IREG_int_process( int sig );
void IREG_alarm_hdlr( int sig );
void IREG_process_end( int sig );

/* delete if the error has been fixed. */
int IREG_open_PIO( u_int ireg_base );
int IREG_close_PIO( u_int ireg_fd );
int IREG_set_A32D32( int ireg_fd );
int IREG_set_A16D16( int ireg_fd );
int IREG_latch1_read_PIO( int ireg_fd, u_short *value );
int IREG_latch2_read_PIO( int ireg_fd, u_short *value );
int IREG_ff_read_PIO( int ireg_fd, u_short *value );
int IREG_through_read_PIO( int ireg_fd, u_short *value );
int IREG_pulse_out_PIO( int ireg_fd, u_short value );
int IREG_level_out_PIO( int ireg_fd, u_short value );
int IREG_waitevent_PIO( int ireg_fd, int enable, int timeout );
#ifdef IREG_PIO
#define IREG_open         IREG_open_PIO
#define IREG_latch1_read  IREG_latch1_read_PIO
#define IREG_latch2_read  IREG_latch2_read_PIO
#define IREG_ff_read      IREG_ff_read_PIO
#define IREG_through_read IREG_through_read_PIO
#define IREG_pulse_out    IREG_pulse_out_PIO
#define IREG_level_out    IREG_level_out_PIO
#define IREG_waitevent    IREG_waitevent_PIO
#endif //IREG_PIO


#define INTREG_INTERRUPT1 (1<<0)
#define INTREG_INTERRUPT2 (1<<1)
#define INTREG_INTERRUPT3 (1<<2)

#endif //IREGPROT_H_ALREADY_INCLUDED

