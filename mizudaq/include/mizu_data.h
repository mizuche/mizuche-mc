#include <stdio.h>

#ifndef MIZUDATA_H_ALREADY_INCLUDE
#define MIZUDATA_H_ALREADY_INCLUDE

#define n_fadc_head 4 // word
#define max_fadc_data 10000 //temporary
#define max_atm_data 8000
#define max_atm_channel 350

typedef struct {
  int fadc_len;
  int atm_len;
  unsigned int fadc_raw[max_fadc_data];
  unsigned int atm_raw[max_atm_data];

  unsigned int fadc_magic;
  unsigned int fadc_board_info;
  unsigned int fadc_event_size;
  unsigned int fadc_event_count;
  unsigned int fadc_trg_time;
  int nsamp;
  short fadc[20000];  

  int spillnum;
  int nhits;
  short atm_id[max_atm_channel];
  short atm_ch[max_atm_channel];
  short atm_qdc[max_atm_channel];
  short atm_tdc[max_atm_channel];

} MIZURAW;

#endif
