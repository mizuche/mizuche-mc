/*
 * sclrprot.h,v 1.1 1994/11/23 Masato Shiozawa
 *
 *     v 1.1: 1994/11/23
 *            first definition by Masato Shiozawa(ICRR)
 */
#include <sys/types.h>
#include "scaler.h"

#ifndef SCLRPROT_H_ALREADY_INCLUDED
#define SCLRPROT_H_ALREADY_INCLUDED

int SCLR_write( int sclr_fd, int offset, u_short *sdata );
int SCLR_read( int sclr_fd, int offset, u_short *sdata );
int scaler( int sclr_fd, int channel, u_int *data );
int SCALER( int sclr_fd, int channel, u_int *data );
int SCLR_reset( int sclr_fd );
int SCLR_close( int sclr_fd );
int SCLR_open( u_int sclr_base );

/* delete if the error has been fixed. */
int set_A32D32( void );
int set_A16D16( void );
int SCLR_reset_PIO( int sclr_fd );
int scaler_PIO( int sclr_fd, int channel, u_int *data );

/* for PIO mode */
#ifdef SCLR_PIO
#define SCLR_reset SCLR_reset_PIO
#define scaler scaler_PIO
#define SCALER scaler_PIO
#endif //SCLR_PIO

#endif //SCLRPROT_H_ALREADY_INCLUDED

