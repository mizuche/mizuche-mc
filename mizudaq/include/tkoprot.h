/*
 * tkoprot.h,v 1.1 1994/11/23 Masato Shiozawa
 *
 *     v 1.1: 1994/11/23
 *            first definition by Masato Shiozawa(ICRR)
 */
#include <sys/types.h>
#include "smpreg.h"

#ifndef TKOPROT_H_ALREADY_INCLUDED
#define TKOPROT_H_ALREADY_INCLUDED

int bit3_set_page( int tko_num );
int TKO_close( void );
int smp_map( void );
void TKO_int_process(int sig);
void TKO_alarm_hdlr( int sig );
void TKO_process_end( int sig );
int TKO_signal_setup( void );
int TKO_bit3_initialize( void );
int TKO_open( int smp_entry );
int TKO_select_open( int smp_entry, u_int mask );
int TKO_single_act( int tko_num, int tko_bc, int tko_ma, int tko_sa, int tko_fn, u_short *tko_data ); 
int TKO_program_select( int tko_num, int program );
int TKO_program_select_all( int program );
int TKO_sds_go( int tko_num, int program );
#define SWITCH_AND_READ 1
#define READ_ONLY     0
int TKO_waitevent( int tko_timeout );
u_int TKO_buffer_size( void );
u_int TKO_read( u_int *ptr, u_int *total_length, int switch_option );
u_int TKO_test_read( u_int *ptr, u_int *total_length, int switch_option
		    , u_int smp_data_size );
u_int TKO_test_write(  int tko_num, u_int *ptr, u_int total_length );
int   TKO_param( int flag, u_int value );
#define TKO_NUMTHR 1
#define TKO_SIZTHR 2
#define TKO_NUMMSK 3
#define TKO_SIZMSK 4
u_int TKO_buffer_switch( void );

int SMP_reg_read( int tko_num, SMPREG *buffer);
int SMP_All_Reset( void );
int SMP_Reset( int tko_num );
int SMP_Switch_Mask( int tko_num, u_int en_mask, u_int ds_mask );
int SMP_TKO_memory_full( void );
int SMP_scount_Clear( int tko_num );
int SMP_scount_Clear_all( void );


//#####  added by akira.m #####

int TKO_init(int tko_num, double atm_threshold);
int set_tko_operate_mode(int tko_num);
int set_tko_pedestal_mode(int tko_num);
int TKO_waitaccept(int tko_num);
int start_tko_pedestal(int tko_num);
int pedestal_test(int tko_num);
u_int read_tko_data(u_int *wdata_out, int switch_option);
void decode_tko_data(u_int *wdata, u_int ndata);

//#############################

#define BROADCAST    1
#define BC           1
#define NO_BROADCAST 0
#define NO_BC        0

#endif //TKOPROT_H_ALREADY_INCLUDED

