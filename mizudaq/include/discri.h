/**********************************************************************
 *  discri.h                                                          *
 *          :Header file for VME-DISCRIMINATOR                        *
 *                                                                    *
 *       (1) Jan.-05-1995  First definition by M.Shiozawa (I.C.R.R)   *
 **********************************************************************/
//#include <stdio.h>
//#include <sys/types.h>
//#include <errno.h>

#ifndef DISCRI_H_ALREADY_INCLUDED
#define DISCRI_H_ALREADY_INCLUDED

#define DSCR_BASE_ADDRESS 0xf0000000

#define DSCR_MAX_CHANNEL 7
#define DSCR_MIN_CHANNEL 0

#define DSCR_MAX_LEVEL   255 // [mV]
#define DSCR_MIN_LEVEL     0 // [mV]

#define DSCR_CH0  0
#define DSCR_CH1  1
#define DSCR_CH2  2
#define DSCR_CH3  3
#define DSCR_CH4  4
#define DSCR_CH5  5
#define DSCR_CH6  6
#define DSCR_CH7  7

int dscr_fd;

#endif //DISCRI_H_ALREADY_INCLUDED

