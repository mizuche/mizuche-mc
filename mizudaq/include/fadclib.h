// -*- c++ -*- //

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#ifndef FADCLIB_H_ALREADY_INCLUDE
#define FADCLIB_H_ALREADY_INCLUDE

//
#define FADC_DEV_FILE "/dev/vmedrv24d32"
// at d32, word size should be 32 bit -> word = unsigned int
// at d16, word size should be 16 bit -> word = u_short

#define FADC_BASE_ADD     0x00300000  // for v24

//
int fadc_vme_fd;
//const int max_fadc_data = 2000; // 7 usec gate => 1750 sample/ch => 875 words/ch
const int max_fadc_data = 10000; // 7 usec gate => 1750 sample/ch => 875 words/ch
const double sampling_rate = 250.e6;
const unsigned int const_latency = 100;  // sample, temporary

// Address map (General)
#define MEMORY_HEAD_ADD           0x0000
//#define MAX_READ_BUFF_SIZE        0x0FFC
#define MAX_READ_BUFF_SIZE        0x0FFF
// EVENT READOUT BUFFER ADDRESS : 0x0000 ~ 0x0FFC

#define CH_CONFIG_ADD             0x8000
#define CH_CONFIG_CLR_ADD         0x8008
#define BUFF_ORGANIZATION_ADD     0x800C
#define ACQUISITION_CONTROL_ADD   0x8100
#define ACQUISITION_STATUS_ADD    0x8104
#define TRG_SOURCE_MASK_ADD       0x810C
#define POST_TRIGGER_ADD          0x8114
#define CH_ENABLE_MASK_ADD        0x8120
#define SET_MONITOR_DAC_ADD       0x8138
#define MONITOR_MODE_ADD          0x8144
#define FRONT_IO_CONTROL_ADD      0x811C
#define BLT_EVENT_NUMBER_ADD      0xEF1C
#define SOFTWARE_RESET_ADD        0xEF24
#define SOFTWARE_CLEAR_ADD        0xEF28

// Address map about each channel ("n" means channel#)
#define CH_ZS_THRE_ADD(n)         0x1024|(n<<8)
#define CH_THRESHOLD_ADD(n)       0x1080|(n<<8)
#define CH_STATUS_ADD(n)          0x1088|(n<<8)
#define CH_DAC_OFFSET_ADD(n)      0x1098|(n<<8)

// Channel mask
#define CH_NOT_MASK(n) (1<<n)
#define CH_MASK(n)     (0<<n)
const unsigned int ch_mask_reg =
  CH_NOT_MASK(0) | CH_NOT_MASK(1) | CH_MASK(2) | CH_MASK(3) |
  CH_MASK(4) | CH_MASK(5) | CH_MASK(6) | CH_MASK(7);
const unsigned int used_fadc_ch = 2;
// const unsigned int ch_mask_reg =
//      CH_NOT_MASK(0) | CH_NOT_MASK(1) | CH_NOT_MASK(2) | CH_NOT_MASK(3) |
//      CH_MASK(4) | CH_MASK(5) | CH_MASK(6) | CH_MASK(7);
// const unsigned int used_fadc_ch = 4; 

// channel config
#define FULL_SUPPRESS       (1<<16)|(1<<17)
#define MEM_SEQUENT_ACCESS  (1<<4)

// trigger source config
#define ENABLE_EX_TRG (1<<30)

const unsigned int fadc_dc_offset = 0x8000; // [bit]
// this offset adjusted for pedestal level
//
// 16bit DAC allows to add DC offset to input signal (+-1V range)
// FSR = Full Scale Range (16bit) correspond to 2V range
// default DC offset = FSR/2 = 0x8000 (15bit)
// negative uniporlar (-2V~0V): offset = 0 bit
// bipolar (-1V~1V) : FSR/2
// positibe uniporlar (0V~2V) : offset = FSR

#define CHN_CONFIG_CLR(n)       (1<<n)

// Channel Status
#define CH_MEMORY_EMPTY (1<<1)
#define CH_MEMORY_FULL  (1<<0)
#define CH_DAQ_BUSY     (1<<2)

// Acquisition Control
#define ACQUISITION_RUN     (1<<2)
#define ACQUISITION_STOP    (0<<2)
#define SIN_CONTROLLED_MODE (1<<0)
#define SIN_GATE_MODE       (1<<1)
#define REGISTER_CONTROLLED_MODE 0x0

#define BOARD_STAT          (1<<8)
#define EVENT_FULL          (1<<4)
#define EVENT_READY         (1<<3)
#define RUN_ON_OFF          (1<<2)

#endif //FADCLIB_H_ALREADY_INCLUDE
