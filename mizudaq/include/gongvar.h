/*
 * gongvar.h,v 1.1 1993/09/04 Masato Shiozawa
 *
 *     v 1.1: 1993/12/17
 *            first definition by Masato SHiozawa(ICRR)
 *            original is tkovar.h
 */



#ifndef GONGVAR_H_ALREADY_INCLUDED
#define GONGVAR_H_ALREADY_INCLUDED


/* --- definition for GONG --- */
#define GONG_MA            0
#define GONG_SA            0

#define GONG_READ_F        0
#define GONG_READ_DELAY_F  2
#define GONG_READ_MODE_F   3
#define GONG_PEDESTAL_F    8
#define GONG_WRITE_DELAY_F 10
#define GONG_WRITE_MODE_F  11
#define GONG_CNT_CLEAR_F   12
#define GONG_CNT_INC_F     13
#define GONG_INITIALIZE_F  14

#define GONG_OPERATE_MODE  0
#define GONG_PEDESTAL_MODE 1


#endif //GONGVAR_H_ALREADY_INCLUDED
