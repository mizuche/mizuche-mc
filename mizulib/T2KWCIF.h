#ifndef T2KWC_IF_H
#define T2KWC_IF_H

#include <iostream>

#include "TTree.h"
#include "TMath.h"


////////////////////
// MC variable
#define MAXNSIMHITS 400
#define MAXNSIMPARS 100
#define MAXNG 40

typedef struct 
{  
  // 
  Int_t tgenphs;      // total generated photon
  Int_t tgenpes;      // total generated p.e.
  Int_t tgenpes_pmt;  // total generated p.e. into pmt
  Long64_t seed;      // init seed
  Int_t fvwater;      // fvwater (1:w/ water , 0:w/o water)

  // neut info
  Double_t energy;          // neutrino energy [GeV]
  Int_t flav;               // numu:1, numu-bar:2, nue;3, nue-bar:4
  Int_t mode;               // interaction mode
  Double_t dir[3];          // neutrino diraction
  Double_t norm;            // normalization factor of Jnubeam
  Double_t totcrsne;        // cross-section
  Double_t tnorm;           // norm*totcrsne*(h2o_totatoms)

  // ancestor particle of neutrino
  Int_t ng;             // # of ancestor particles
  Double_t gpx[MAXNG];    // directional x-momentum 
  Double_t gpy[MAXNG];    // directional y-momentum 
  Double_t gpz[MAXNG];    // directional z-momentum 
  Double_t gcosbm[MAXNG];   // angle between ancestors and beam
  Double_t gvx[MAXNG];    // vertex x-position
  Double_t gvy[MAXNG];    // vertex y-position
  Double_t gvz[MAXNG];    // vertex z-position
  Int_t gpid[MAXNG];       // particle id of ancestors (GEANT3)
  Int_t gmec[MAXNG];       // production mechanism

  // simvertex
  Double_t simv[4]; // {x, y, z, flag}

  // simparticle
  Int_t nsp;                    // # of simparticle
  Int_t sppid[MAXNSIMPARS];      // pid
  Double_t sppx[MAXNSIMPARS];  // directional x-momentum [MeV/c]
  Double_t sppy[MAXNSIMPARS];  // directional y-momentum [MeV/c]
  Double_t sppz[MAXNSIMPARS];  // directional z-momentum [MeV/c]
  Double_t spivx[MAXNSIMPARS]; // init x-position
  Double_t spivy[MAXNSIMPARS]; // init y-position 
  Double_t spivz[MAXNSIMPARS]; // init z-position
  Double_t spivf[MAXNSIMPARS]; // init position flag
  Double_t spfvx[MAXNSIMPARS]; // finial x-position
  Double_t spfvy[MAXNSIMPARS]; // finial y-position
  Double_t spfvz[MAXNSIMPARS]; // finial z-position
  Double_t spfvf[MAXNSIMPARS]; // finial position flag

  // simhit
  Int_t nsh;
  Int_t shid[MAXNSIMHITS];          // hit id = window#
  Int_t shpe[MAXNSIMHITS];          // p.e.
  Double_t shtm[MAXNSIMHITS];       // time
} MCBranch;

////////////////////
// Data variable
#define MAXNHITS 400
#define MAXSAMPLE 10000 // temporary

typedef struct 
{
  //
  Int_t totpe;                // total # of p.e.

  //
  Int_t sm;                   // sampling #
  Short_t pmtsum[MAXSAMPLE];  // FADC of PMTSUM
  Short_t trgtime[MAXSAMPLE]; // FADC of TRGTIME

  //
  Int_t runnum;               // run#
  Int_t spillnum;             // spill#
  Int_t trgnum;               // trigger#
  Int_t gong;                 // gong count
  Int_t runmode;              // runmode

  //
  Int_t nhits;                // # of hits
  Int_t ch[MAXNHITS];         // ch#
  Int_t pmt[MAXNHITS];        // pmt#
  Int_t window[MAXNHITS];      // window#
  Int_t type[MAXNHITS];        // type (A or B)
  Int_t tag[MAXNHITS];         // tag#
  Int_t qdc[MAXNHITS];         // QDC
  Int_t tdc[MAXNHITS];         // TDC
  Double_t charge[MAXNHITS];   // recon charge from QDC
  Double_t time[MAXNHITS];     // recon time from TDC
  Double_t pe[MAXNHITS];       // recon p.e. from charge

} CalibBranch;


//
class T2KWCIF
{
private:

public:

  T2KWCIF(){};
  ~T2KWCIF(){};

  void SetMCBranch(TTree*, MCBranch&);
  void SetMCBranchAddress(TTree*, MCBranch&);
  void ClearMCBranch(MCBranch&);

  void SetCalibBranch(TTree*, CalibBranch&);
  void SetCalibBranchAddress(TTree*, CalibBranch&);
  void ClearCalibBranch(CalibBranch&);

  void PrintMCBranch(MCBranch);
  void PrintEventInfo(MCBranch);
  void PrintNeutInfo(MCBranch);
  void PrintSimParticle(MCBranch);
  void PrintSimVertex(MCBranch);
  void PrintSimHit(MCBranch);

  Double_t surf_Mizu() { return (TMath::Pi() * 70 * 70); } // [cm^2]
  Double_t vol_Mizu() { return (surf_Mizu() * 160); } // [cm^3]
  Double_t h2o_totatoms();

};


#endif
