#ifndef T2KWCRAW_H
#define T2KWCRAW_H

#include <iostream>

#include "TObject.h"

#define MAX_nTRG  250
#define MAX_nATM  12000         // --> 1013 / 1event
#define MAX_nFADC 20000         //temporary

// __________________________________________________
class T2KWCRaw : public TObject
{
public:
    //
	int	nTRG;				// channel#
	int	nATM;				// channel#
	int	nFADC;				// channel#
#ifdef OLDVER
    unsigned short  TRG[MAX_nTRG];
#else
    unsigned short  TRG[MAX_nTRG][5];
#endif
    unsigned int ATM[MAX_nATM];
    unsigned int FADC[MAX_nFADC];

    int run;
    int mode;
    unsigned short spill;
    int time_sec;
    int time_nano;

	T2KWCRaw();
	T2KWCRaw(T2KWCRaw* hit);
	virtual ~T2KWCRaw();

    void Clear(Option_t* option="");
    void Print(Option_t* option="") const;

	//
	ClassDef(T2KWCRaw, 1);
};

#endif
