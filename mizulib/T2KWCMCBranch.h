#ifndef T2KWC_MCBRANCH_H
#define T2KWC_MCBRANCH_H

#include <iostream>

#include "TTree.h"
#include "TLeaf.h"
#include "TMath.h"

//////////////////////////////////////////////////
// definition of variable
const int MAXNSIMHITS=400;
const int MAXNSIMPARS=100;

const Double_t totcrsne_unit = 1.e-38;   // [cm^2]
const Double_t surf_Mizu = TMath::Pi() * 70 * 70; // [cm^2]
const Double_t vol_Mizu = surf_Mizu * 160; // [cm^3]

////////////////////
// MC variable

typedef struct
{
  Int_t totnphotons;
  Int_t totnpes;
  Int_t totnpes_pmt;
  Long64_t seed;
  Int_t fvwater;
} EventInfo;

typedef struct
{  
  Double_t energy;
  Int_t flav; // numu:1, numu-bar:2, nue;3, nue-bar:4
  Int_t mode; // interaction mode
  Double_t dir[3]; // neutrino diraction
  Double_t norm;  // normalization factor of Jnubeam
  Double_t totcrsne;  // cross-section
  Double_t tnorm;  // norm*totcrsne*(h2o_totatoms)
} NeutInfo;

typedef struct
{
  Double_t vertex[4]; // {x, y, z, flag}
} SimVertex;

typedef struct
{
  Int_t nparts;
  Int_t pid[MAXNSIMPARS];
  Double_t absmom[MAXNSIMPARS];
  Double_t dir[MAXNSIMPARS][3];
  Double_t ipos[MAXNSIMPARS][4]; // {x,y,z,flag}
  Double_t fpos[MAXNSIMPARS][4];
} SimParticle;

typedef struct
{
  Int_t nhits;
  Int_t window[MAXNSIMHITS]; // windonw #
  Int_t npe[MAXNSIMHITS];
  Double_t time[MAXNSIMHITS];
} SimHit;

typedef struct 
{
  EventInfo evt;
  NeutInfo neut;
  SimVertex simver;
  SimParticle simpar;
  SimHit simhit;
} MCBranch;


/////////////////////////////////////////
// function
Double_t h2o_totatoms();
void SetMCBranch(TTree *t, MCBranch &m);
void SetMCBranchAddress(TTree *t, MCBranch &m);
void ClearMCBranch(MCBranch &m);

void PrintEventInfo(EventInfo);
void PrintNeutInfo(NeutInfo);
void PrintSimParticle(SimParticle);
void PrintSimVertex(SimVertex);
void PrintSimHit(SimHit);

#endif
