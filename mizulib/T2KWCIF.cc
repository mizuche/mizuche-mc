#include "T2KWCIF.h"

Double_t T2KWCIF::h2o_totatoms()
{
  Double_t h2o_mollmass = 18.02; // [g/mol]
  Double_t h2o_atom = 18;    // atomic number
  Double_t h2o_dens = 1.; // [g/cm^3]
  Double_t abogadro = 6.02e23;    // abogadro constant
  Double_t h2o_totmass = vol_Mizu() * h2o_dens; //[g]
  Double_t totcrsne_unit = 1.e-38;   // [cm^2]
  
  return ((h2o_totmass/h2o_mollmass) * h2o_atom * abogadro * totcrsne_unit); // [atom]
}

void T2KWCIF::SetMCBranch(TTree *t, MCBranch &m)
{
  std::cout << "SetMCBranch" << std::endl;

  //
  t->Branch("tgenphs", &(m.tgenphs), "tgenphs/I");
  t->Branch("tgenpes", &(m.tgenpes), "tgenpes/I");
  t->Branch("tgenpes_pmt", &(m.tgenpes_pmt), "tgenpes_pmt/I");
  t->Branch("seed", &(m.seed), "seed/L");
  t->Branch("fvwater", &(m.fvwater), "fvwater/I");

  //
  t->Branch("energy", &(m.energy), "energy/D");
  t->Branch("flav", &(m.flav), "flav/I");
  t->Branch("mode", &(m.mode), "mode/I");
  t->Branch("norm", &(m.norm), "norm/D");
  t->Branch("dir", (m.dir), "dir[3]/D");
  t->Branch("totcrsne", &(m.totcrsne), "totcrsne/D");
  t->Branch("tnorm", &(m.tnorm), "tnorm/D");

  t->Branch("ng", &(m.ng), "ng/I");
  t->Branch("gpx", (m.gpx), "gpx[ng]/D");
  t->Branch("gpy", (m.gpy), "gpy[ng]/D");
  t->Branch("gpz", (m.gpz), "gpz[ng]/D");
  t->Branch("gcosbm", (m.gcosbm), "gcosbm[ng]/D");
  t->Branch("gvx", (m.gvx), "gvx[ng]/D");
  t->Branch("gvy", (m.gvy), "gvy[ng]/D");
  t->Branch("gvz", (m.gvz), "gvz[ng]/D");
  t->Branch("gpid", (m.gpid), "gpid[ng]/I");
  t->Branch("gmec", (m.gmec), "gmec[ng]/I");

  //
  t->Branch("simv", (m.simv), "simv[4]/D");

  //
  t->Branch("nsp", &(m.nsp), "nsp/I");
  t->Branch("sppid", (m.sppid), "sppid[nsp]/I");
  t->Branch("sppx", (m.sppx), "sppx[nsp]/D");
  t->Branch("sppy", (m.sppy), "sppy[nsp]/D");
  t->Branch("sppz", (m.sppz), "sppz[nsp]/D");
  t->Branch("spivx", (m.spivx), "spivx[nsp]/D");
  t->Branch("spivy", (m.spivy), "spivy[nsp]/D");
  t->Branch("spivz", (m.spivz), "spivz[nsp]/D");
  t->Branch("spivf", (m.spivf), "spivf[nsp]/D");
  t->Branch("spfvx", (m.spfvx), "spfvx[nsp]/D");
  t->Branch("spfvy", (m.spfvy), "spfvy[nsp]/D");
  t->Branch("spfvz", (m.spfvz), "spfvz[nsp]/D");
  t->Branch("spfvf", (m.spfvf), "spfvf[nsp]/D");

  //
  t->Branch("nsh", &(m.nsh), "nsh/I");
  t->Branch("shid", (m.shid), "shid[nsh]/I");
  t->Branch("shpe", (m.shpe), "shpe[nsh]/I");
  t->Branch("shtm", (m.shtm), "shtm[nsh]/D");

  std::cout << "End of SetMCBranch" << std::endl;
}

void T2KWCIF::SetMCBranchAddress(TTree *t, MCBranch &m)
{
  std::cout << "SetMCBranchAddress" << std::endl;

  //
  t->SetBranchAddress("tgenphs", &(m.tgenphs));
  t->SetBranchAddress("tgenpes", &(m.tgenpes));
  t->SetBranchAddress("tgenpes_pmt", &(m.tgenpes_pmt));
  t->SetBranchAddress("seed", &(m.seed));
  t->SetBranchAddress("fvwater", &(m.fvwater));

  //
  t->SetBranchAddress("energy", &(m.energy));
  t->SetBranchAddress("flav", &(m.flav));
  t->SetBranchAddress("mode", &(m.mode));
  t->SetBranchAddress("norm", &(m.norm));
  t->SetBranchAddress("dir", (m.dir));
  t->SetBranchAddress("totcrsne", &(m.totcrsne));
  t->SetBranchAddress("tnorm", &(m.tnorm));

  t->SetBranchAddress("ng", &(m.ng));
  t->SetBranchAddress("gpx", (m.gpx));
  t->SetBranchAddress("gpy", (m.gpy));
  t->SetBranchAddress("gpz", (m.gpz));
  t->SetBranchAddress("gcosbm", (m.gcosbm));
  t->SetBranchAddress("gvx", (m.gvx));
  t->SetBranchAddress("gvy", (m.gvy));
  t->SetBranchAddress("gvz", (m.gvz));
  t->SetBranchAddress("gpid", (m.gpid));
  t->SetBranchAddress("gmec", (m.gmec));

  //
  t->SetBranchAddress("simv", &(m.simv));

  //
  t->SetBranchAddress("nsp", &(m.nsp));
  t->SetBranchAddress("sppid", (m.sppid));
  t->SetBranchAddress("sppx", &(m.sppx));
  t->SetBranchAddress("sppy", &(m.sppy));
  t->SetBranchAddress("sppz", &(m.sppz));
  t->SetBranchAddress("spivx", &(m.spivx));
  t->SetBranchAddress("spivy", &(m.spivy));
  t->SetBranchAddress("spivz", &(m.spivz));
  t->SetBranchAddress("spivf", &(m.spivf));
  t->SetBranchAddress("spfvx", &(m.spfvx));
  t->SetBranchAddress("spfvy", &(m.spfvy));
  t->SetBranchAddress("spfvz", &(m.spfvz));
  t->SetBranchAddress("spfvf", &(m.spfvf));

  //
  t->SetBranchAddress("nsh", &(m.nsh) );
  t->SetBranchAddress("shid", (m.shid) );
  t->SetBranchAddress("shpe", (m.shpe) );
  t->SetBranchAddress("shtm", (m.shtm) );

  std::cout << "End of SetMCBranchAddress" << std::endl;
}

void T2KWCIF::ClearMCBranch(MCBranch &m)
{
  std::cout << "ClearMCBranch" << std::endl;

  //
  m.tgenphs = 0;
  m.tgenpes = 0;
  m.tgenpes_pmt = 0;
  m.seed = -1;
  m.fvwater = -1111;

  //
  m.energy = -1.;
  m.flav = -1;
  m.mode = -1;
  for(int i=0; i<3; i++) m.dir[i] = -1111.;
  m.norm = -1.;
  m.totcrsne = -1.;
  m.tnorm = -1.;

  m.ng = 0;
  for(int i=0; i<MAXNG; i++) {
    for(int j=0; j<3; j++) {
      m.gpx[i] = -1111.;
      m.gpy[i] = -1111.;
      m.gpz[i] = -1111.;
      m.gvx[i] = -1111.;
      m.gvy[i] = -1111.;
      m.gvz[i] = -1111.;
    }
    m.gcosbm[i] = -1111.;
    m.gpid[i] = 0;
    m.gmec[i] = 0;
  }

  //
  for(int i=0; i<4; i++) m.simv[i] = -1111.;

  //
  m.nsp = 0;
  for(int i=0; i<MAXNSIMPARS; i++) {
    m.sppid[i] = -1;
    m.sppx[i] = -1111.;
    m.sppy[i] = -1111.;
    m.sppz[i] = -1111.;
    m.spivx[i] = -1111.;
    m.spivy[i] = -1111.;
    m.spivz[i] = -1111.;
    m.spivf[i] = -1111.;
    m.spfvx[i] = -1111.;
    m.spfvy[i] = -1111.;
    m.spfvz[i] = -1111.;
    m.spfvf[i] = -1111.;
  }

  //
  m.nsh = 0;
  for(int i=0; i<MAXNSIMHITS; i++) {
    m.shid[i] = -1;
    m.shpe[i] = -1;
    m.shtm[i] = -1.;
  }

  std::cout << "End of ClearMCBranch" << std::endl;
}

void T2KWCIF::SetCalibBranch(TTree *t, CalibBranch &c)
{
  std::cout << "SetCalibBranch" << std::endl;

  t->Branch("totpe", &(c.totpe), "totpe/I");
  
  t->Branch("sm", &(c.sm), "sm/I");
  t->Branch("pmtsum", (c.pmtsum), "pmtsum[sm]/S");
  t->Branch("trgtime", (c.trgtime), "trgtime[sm]/S");

  t->Branch("runnum", &(c.runnum), "runnum/I");
  t->Branch("spillnum", &(c.spillnum), "spillnum/I");
  t->Branch("trgnum", &(c.trgnum), "trgnum/I");
  t->Branch("gong", &(c.gong), "gong/I");
  t->Branch("runmode", &(c.runmode), "runmode/I");

  t->Branch("nhits", &(c.nhits), "nhits/I");
  t->Branch("ch", (c.ch), "ch[nhits]/I");
  t->Branch("pmt", (c.pmt), "pmt[nhits]/I");
  t->Branch("window", (c.window), "window[nhits]/I");
  t->Branch("type", (c.type), "type[nhits]/I");
  t->Branch("tag", (c.tag), "tag[nhits]/I");
  t->Branch("qdc", (c.qdc), "qdc[nhits]/I");
  t->Branch("tdc", (c.tdc), "tdc[nhits]/I");
  t->Branch("charge", (c.charge), "charge[nhits]/D");
  t->Branch("time", (c.time), "time[nhits]/D");
  t->Branch("pe", (c.pe), "pe[nhits]/D");

}

void T2KWCIF::SetCalibBranchAddress(TTree *t, CalibBranch &c)
{
  std::cout << "SetCalibBranchAddress" << std::endl;

  //
  t->SetBranchAddress("totpe", &(c.totpe));

  //
  t->SetBranchAddress("sm", &(c.sm));
  t->SetBranchAddress("pmtsum", (c.pmtsum));
  t->SetBranchAddress("trgnum", (c.trgtime));

  //
  t->SetBranchAddress("runnum", &(c.runnum));
  t->SetBranchAddress("spillnum", &(c.spillnum));
  t->SetBranchAddress("trgnum", &(c.trgnum));
  t->SetBranchAddress("gong", &(c.gong));
  t->SetBranchAddress("runmode", &(c.runmode));

  //
  t->SetBranchAddress("nhits", &(c.nhits));
  t->SetBranchAddress("ch", (c.ch));
  t->SetBranchAddress("pmt", (c.pmt));
  t->SetBranchAddress("window", (c.window));
  t->SetBranchAddress("type", (c.type));
  t->SetBranchAddress("tag", (c.tag));
  t->SetBranchAddress("qdc", (c.qdc));
  t->SetBranchAddress("tdc", (c.tdc));
  t->SetBranchAddress("charge", (c.charge));
  t->SetBranchAddress("time", (c.time));
  t->SetBranchAddress("pe", (c.pe));

}

void T2KWCIF::ClearCalibBranch(CalibBranch &c)
{
  //
  c.totpe = 0;
  
  //
  c.sm = 0;
  for(int i=0; i<MAXSAMPLE; i++) {
    c.pmtsum[i] = -1;
    c.trgtime[i] = -1;
  }

  //
  c.runnum = -1;
  c.spillnum = -1;
  c.trgnum = -1;
  c.gong = -1;
  c.runmode = -1;

  //
  c.nhits = 0;
  for(int i=0; i<MAXNHITS; i++) {
    c.ch[i] = -1;
    c.pmt[i] = -1;
    c.window[i] = -1;
    c.type[i] = -1;
    c.tag[i] = -1;
    c.qdc[i] = -1;
    c.tdc[i] = -1;
    c.charge[i] = -1.;
    c.time[i] = -1.;
    c.pe[i] = -1.;
  }

}



void T2KWCIF::PrintMCBranch(MCBranch m)
{
  PrintEventInfo(m);
  PrintNeutInfo(m);
  PrintSimParticle(m);
  PrintSimVertex(m);
  PrintSimHit(m);
}

void T2KWCIF::PrintEventInfo(MCBranch m)
{
  std::cout << "--- MC Event Info ---" << std::endl;
  std::cout << "Total # of generated photons : " << m.tgenphs << std::endl;
  std::cout << "Total # of generated p.e. " << m.tgenpes << std::endl;
  std::cout << "Total # of generated p.e. into PMT : " << m.tgenpes_pmt << std::endl;
  std::cout << "Init. seed :" << m.seed << std::endl;
  std::cout << "FV water : " << m.fvwater << std::endl;
}

void T2KWCIF::PrintNeutInfo(MCBranch m)
{
  std::cout << "--- Neut info ---" << std::endl;
  std::cout << "Energy: " << m.energy << std::endl;
  std::cout << "Flav: " << m.flav << std::endl;
  std::cout << "Interaction Mode: " << m.mode << std::endl;
  std::cout << "Norm: " << m.norm << std::endl;
  std::cout << "Total cross-section: " << m.totcrsne << std::endl;
  std::cout << std::endl;
}

void T2KWCIF::PrintSimParticle(MCBranch m)
{
  std::cout << "--- SimParticle info ---" << std::endl;
  for(int i=0; i<(m.nsp); i++) {
    Double_t amom = TMath::Sqrt(m.sppx[i]*m.sppx[i]+m.sppy[i]*m.sppy[i]+m.sppz[i]*m.sppz[i]);
    std::cout << "ID: " << i << std::endl;
    std::cout << " PID(PDG code): " << m.sppid[i] << std::endl;
    std::cout << " init mom: " << m.sppx[i] << "," 
                               << m.sppy[i] << "," 
                               << m.sppz[i]
              << "  theta: " << TMath::ACos(m.sppz[i])/amom/TMath::Pi()*180. 
              << std::endl;
    std::cout << " init pos: " << m.spivx[i] << ", " 
                               << m.spivy[i] << ", " 
                               << m.spivz[i]
                               << " : " << m.spivf[i] << std::endl;
    std::cout << " final pos: "<< m.spfvx[i] << ", " 
                               << m.spfvy[i] << ", " 
                               << m.spfvz[i]
                               << " : " << m.spfvf[i] << std::endl;
  }
}

void T2KWCIF::PrintSimVertex(MCBranch m)
{
  std::cout << "--- MC True vertex info ---" << std::endl;
  std::cout << "Pos: " << m.simv[0] << "," 
                       << m.simv[1] << "," 
                       << m.simv[2] << std::endl;
  Int_t vflag = (Int_t)m.simv[3];

  switch(vflag) {
    case 2 :
      std::cout << "vertex in FV" << std::endl;
      break;
    case 1 :
      std::cout << "vertex in Tank (out FV)" << std::endl;
      break;
    case 0 :
      std::cout << "vertex out Tank" << std::endl;
      break;
    default :
      exit(1);
  }
  std::cout << std::endl;
}

void T2KWCIF::PrintSimHit(MCBranch m)
{
  std::cout << "--- PMT MC Hit Info ---" << std::endl;
  for(int i=0; i<(m.nsh); i++) {
    std::cout << " Hit#: " << m.shid[i] << std::endl;
    std::cout << " P.E. : " << m.shpe[i] << std::endl;
    std::cout << " Time : " << m.shtm[i] << std::endl;
  }
}
