#ifndef T2KWCPMTHIT_H
#define T2KWCPMTHIT_H

#include <iostream>

#include "TObject.h"

//
class T2KWCPMTHit : public TObject
{
private:

	Int_t 		id;				// channel#
	Int_t 		npe;			// # of p.e.
  Double_t time;      // hit time [ns]

  Short_t  qdc;       // QDC count
  Short_t  tdc;       // TDC count

public:

	T2KWCPMTHit();
	T2KWCPMTHit(T2KWCPMTHit* hit);
	virtual ~T2KWCPMTHit();

	void Clear(Option_t* option="");
	void Print(Option_t* option="") const;

	//
	Int_t ID() 				{ return id; }
	Int_t NPE() 			{ return npe; }
  Double_t Time()   { return time; }

	//
	void SetID(int i) 					{ id = i; }
	void SetNPE(int n) 					{ npe = n; }
  void SetTime(double t)      { time = t; }

	//
	ClassDef(T2KWCPMTHit, 3)

};

#endif
