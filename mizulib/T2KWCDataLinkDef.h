#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class T2KWCEvent+;
#pragma link C++ class T2KWCPMTHit+;
#pragma link C++ class T2KWCSimVertex+;
#pragma link C++ class T2KWCSimParticle+;
#pragma link C++ class T2KWCNeutInfo+;

#endif
