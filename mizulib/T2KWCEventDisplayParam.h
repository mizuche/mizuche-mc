// constant value
const Int_t npmts = 164;
const Int_t nz = 6;
const Int_t nr = 18;
const Double_t r_tank = 70.;  // [cm]
const Double_t l_tank = 160.; // [cm]
const Double_t r_pmt  = 3.5;  // [cm]
const Int_t gNtrigger = 8;

// pmt_pot[window#][x,y,z]
// beam-direction is +z , right-hand cordinate (same as MC)
const Double_t pmt_pot[164][3] = 
{
    // upstream cap
    {  12, 60, -79.475 },
    { -12, 60, -79.475 },
    {  33.902, 50.188, -79.475 },
    { -33.902, 50.188, -79.475 },
    { 50.188, 33.902, -79.475 },
    {  14.545, 36, -79.475 },
    { -14.545, 36, -79.475 },
    { -50.188, 33.902, -79.475 },
    { 60, 12, -79.475 },
    { 36, 14.545, -79.475 },
    { 12, 12, -79.475 },
    { -12, 12, -79.475 },
    { -36, 14.545, -79.475 },
    { -60, 12, -79.475 },
    { 60, -12, -79.475 },
    { 36, -14.545, -79.475 },
    { 12, -12, -79.475 },
    { -12, -12, -79.475 },
    { -36, -14.545, -79.475 },
    { -60, -12, -79.475 },
    { 50.188, -33.902, -79.475 },
    { 14.545, -36, -79.475 },
    { -14.545, -36, -79.475 },
    { -50.188, -33.902, -79.475 },
    { 33.902, -50.188, -79.475 },
    { 12, -60, -79.475 },
    { -12, -60, -79.475 },
    { -33.902, -50.188, -79.475 },

    // downstream cap
    { 12, 60, 79.45 },
    { -12, 60, 79.45 },
    { 33.902, 50.188, 79.45 },
    { -33.902, 50.188, 79.45 },
    { 50.188, 33.902, 79.45 },
    { 14.545, 36, 79.45 },
    { -14.545, 36, 79.45 },
    { -50.188, 33.902, 79.45 },
    { 60, 12, 79.45 },
    { 36, 14.545, 79.45 },
    { 12, 12, 79.45 },
    { -12, 12, 79.45 },
    { -36, 14.545, 79.45 },
    { -60, 12, 79.45 },
    { 60, -12, 79.45 },
    { 36, -14.545, 79.45 },
    { 12, -12, 79.45 },
    { -12, -12, 79.45 },
    { -36, -14.545, 79.45 },
    { -60, -12, 79.45 },
    { 50.188, -33.902, 79.45 },
    { 33.902, -50.188, 79.45 },
    { -33.902, -50.188, 79.45 },
    { -50.188, -33.902, 79.45 },
    { 14.545, -36, 79.45 },
    { -14.545, -36, 79.45 },
    { 12, -60, 79.45 },
    { -12, -60, 79.45 },

    // side
    { -11.9644, 67.8533, -60 },
    { -34.45, 59.6692, -60 },
    { -52.7805, 44.2881, -60 },
    { -64.7448, 23.5652, -60 },
    { -68.9, 8.43782e-15, -60 },
    { -64.7448, -23.5652, -60 },
    { -52.7805, -44.2881, -60 },
    { -34.45, -59.6692, -60 },
    { -11.9644, -67.8533, -60 },
    { 11.9644, -67.8533, -60 },
    { 34.45, -59.6692, -60 },
    { 52.7805, -44.2881, -60 },
    { 64.7448, -23.5652, -60 },
    { 68.9, 0, -60 },
    { 64.7448, 23.5652, -60 },
    { 52.7805, 44.2881, -60 },
    { 34.45, 59.6692, -60 },
    { 11.9644, 67.8533, -60 },

    { -11.9644, 67.8533, -36 },
    { -34.45, 59.6692, -36 },
    { -52.7805, 44.2881, -36 },
    { -64.7448, 23.5652, -36 },
    { -68.9, 8.43782e-15, -36 },
    { -64.7448, -23.5652, -36 },
    { -52.7805, -44.2881, -36 },
    { -34.45, -59.6692, -36 },
    { -11.9644, -67.8533, -36 },
    { 11.9644, -67.8533, -36 },
    { 34.45, -59.6692, -36 },
    { 52.7805, -44.2881, -36 },
    { 64.7448, -23.5652, -36 },
    { 68.9, 0, -36 },
    { 64.7448, 23.5652, -36 },
    { 52.7805, 44.2881, -36 },
    { 34.45, 59.6692, -36 },
    { 11.9644, 67.8533, -36 },

    { -11.9644, 67.8533, -12 },
    { -34.45, 59.6692, -12 },
    { -52.7805, 44.2881, -12 },
    { -64.7448, 23.5652, -12 },
    { -68.9, 8.43782e-15, -12 },
    { -64.7448, -23.5652, -12 },
    { -52.7805, -44.2881, -12 },
    { -34.45, -59.6692, -12 },
    { -11.9644, -67.8533, -12 },
    { 11.9644, -67.8533, -12 },
    { 34.45, -59.6692, -12 },
    { 52.7805, -44.2881, -12 },
    { 64.7448, -23.5652, -12 },
    { 68.9, 0, -12 },
    { 64.7448, 23.5652, -12 },
    { 52.7805, 44.2881, -12 },
    { 34.45, 59.6692, -12 },
    { 11.9644, 67.8533, -12 },

    { -11.9644, 67.8533, 12 },
    { -34.45, 59.6692, 12 },
    { -52.7805, 44.2881, 12 },
    { -64.7448, 23.5652, 12 },
    { -68.9, 8.43782e-15, 12 },
    { -64.7448, -23.5652, 12 },
    { -52.7805, -44.2881, 12 },
    { -34.45, -59.6692, 12 },
    { -11.9644, -67.8533, 12 },
    { 11.9644, -67.8533, 12 },
    { 34.45, -59.6692, 12 },
    { 52.7805, -44.2881, 12 },
    { 64.7448, -23.5652, 12 },
    { 68.9, 0, 12 },
    { 64.7448, 23.5652, 12 },
    { 52.7805, 44.2881, 12 },
    { 34.45, 59.6692, 12 },
    { 11.9644, 67.8533, 12 },

    { -11.9644, 67.8533, 36 },
    { -34.45, 59.6692, 36 },
    { -52.7805, 44.2881, 36 },
    { -64.7448, 23.5652, 36 },
    { -68.9, 8.43782e-15, 36 },
    { -64.7448, -23.5652, 36 },
    { -52.7805, -44.2881, 36 },
    { -34.45, -59.6692, 36 },
    { -11.9644, -67.8533, 36 },
    { 11.9644, -67.8533, 36 },
    { 34.45, -59.6692, 36 },
    { 52.7805, -44.2881, 36 },
    { 64.7448, -23.5652, 36 },
    { 68.9, 0, 36 },
    { 64.7448, 23.5652, 36 },
    { 52.7805, 44.2881, 36 },
    { 34.45, 59.6692, 36 },
    { 11.9644, 67.8533, 36 },

    { -11.9644, 67.8533, 60 },
    { -34.45, 59.6692, 60 },
    { -52.7805, 44.2881, 60 },
    { -64.7448, 23.5652, 60 },
    { -68.9, 8.43782e-15, 60 },
    { -64.7448, -23.5652, 60 },
    { -52.7805, -44.2881, 60 },
    { -34.45, -59.6692, 60 },
    { -11.9644, -67.8533, 60 },
    { 11.9644, -67.8533, 60 },
    { 34.45, -59.6692, 60 },
    { 52.7805, -44.2881, 60 },
    { 64.7448, -23.5652, 60 },
    { 68.9, 0, 60 },
    { 64.7448, 23.5652, 60 },
    { 52.7805, 44.2881, 60 },
    { 34.45, 59.6692, 60 },
    { 11.9644, 67.8533, 60 },

};

const Double_t trg_pos[gNtrigger][4] =
{
    { -150, -130, -100, -140 },   // win:165
    { -200, -130, -150, -140 },  // win:166
    {  150, -130,  100, -140 },   // win:167
    {  200, -130,  150, -140 },   // win:168
    { -150,  130, -100,  140 },   // win:169
    { -200,  130, -150,  140 },   // win:170
    {  150,  130,  100,  140 },   // win:171
    {  200,  130,  150,  140 },   // win:172
};

const int ncol = 16;
const Float_t fRed[ncol] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.3,0.6,1.,1.,1.,1.,1.};
const Float_t fGreen[ncol] = {0.,0.25,0.5,0.75,1.,1.,1.,1.,1.,1.,1.,1.,0.75,0.5,0.25,0.};
const Float_t fBlue[ncol] = {1.,1.,1.,1.,1.,0.75,0.5,0.25,0.,0.,0.,0.,0.,0.,0.,0.};

const Int_t hit_threshold = 2;
