#include "T2KWCPMTHit.h"


//
T2KWCPMTHit::T2KWCPMTHit()
{
	id = -1;
	npe = -1;
  time = -1.;
}

T2KWCPMTHit::T2KWCPMTHit(T2KWCPMTHit* hit)
{
	id = hit->ID();
	npe = hit->NPE();
  time = hit->Time();
}

T2KWCPMTHit::~T2KWCPMTHit()
{
}


//
void T2KWCPMTHit::Clear(Option_t* option)
{
	id = -1;
	npe = -1;
  time = -1.;
}


//
void T2KWCPMTHit::Print(Option_t* option) const
{
	std::cout << "PMT Hit Info" << std::endl;
	std::cout << "PMT ID: " << id << "\t"
            << "P.E. : " << npe << "\t"
            << "Time : " << time << "\t"
            << std::endl;
}


//
ClassImp(T2KWCPMTHit)
