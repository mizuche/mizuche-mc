// -*- mode : c++ -*-

#ifndef T2KWCHIT_H
#define T2KWCHIT_H

#include <iostream>

#include "TObject.h"
#include "TString.h"

// __________________________________________________
class T2KWCHit : public TObject
{
private:
    Int_t    ch;     // Elec channel#
    Int_t    pmt;    // PMT#
    Int_t    window; // Tank window# of Tank
    Int_t    type;   // A:0, or B:1 type
    Int_t    tag;    // event tag
    Double_t qdc0;   // QDC pedestal
    Double_t tdc0;   // TDC pedestal
    Double_t qdc1;   // QDC raw (is Int_t more proper?)
    Double_t tdc1;   // TDC raw (is Int_t more proper?)
    Double_t charge; // [pC]
    Double_t time;   // [nsec]
    Double_t pe;     // [p.e.]
    Double_t gain;   // Gain
    Int_t    mask;   // PMT Mask
    Int_t    hv;     // PMT HV

public:
    T2KWCHit();
    T2KWCHit(T2KWCHit* hit);
    virtual ~T2KWCHit();
	void Clear(Option_t* option="");
	void Print(Option_t* option="") const;
	//
    Int_t    GetTag()    const { return tag; }
	Int_t    GetCh()     const { return ch; }
	Int_t    GetPmt()    const { return pmt; }
	Int_t    GetWindow() const { return window; }
    Int_t    GetType()   const { return type; }
    Double_t GetQdc0()   const { return qdc0; }
	Double_t GetTdc0()   const { return tdc0; }
	Double_t GetQdc1()   const { return qdc1; }
	Double_t GetTdc1()   const { return tdc1; }
    Double_t GetCharge() const { return charge; }
	Double_t GetTime()   const { return time; }
	Double_t GetPe()     const { return pe; }
    Double_t GetGain()   const { return gain;}
    Int_t    GetMask()   const { return mask;}
    Int_t    GetHv()     const { return hv;}
	//
    void SetTag(const Int_t &i)       { tag = i; }
	void SetCh(const Int_t &i)        { ch = i; }
	void SetPmt(const Int_t &i)       { pmt = i; }
	void SetWindow(const Int_t &i)    { window = i; }
	void SetType(const Int_t &i)      { type = i; }
    void SetQdc0(const Double_t &i)   { qdc0 = i; }
    void SetTdc0(const Double_t &i)   { tdc0 = i; }
    void SetQdc1(const Double_t &i)   { qdc1 = i; }
    void SetTdc1(const Double_t &i)   { tdc1 = i; }
	void SetCharge(const Double_t &i) { charge = i; }
	void SetTime(const Double_t &i)   { time = i; }
	void SetPe(const Double_t &i)        { pe = i; }
    void SetGain(const Double_t &i)   { gain = i;}
    void SetMask(const Int_t &i)      { mask = i;}
    void SetHv(const Int_t &i)        { hv = i;}
	//
	ClassDef(T2KWCHit, 6);
};

#endif
