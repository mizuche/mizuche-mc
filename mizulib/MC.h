//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Dec 28 13:11:19 2011 by ROOT version 5.30/02
// from TTree mc/Mizuche MC
// found on file: ../mizusim/T2KWC_output.root
//////////////////////////////////////////////////////////

#ifndef MC_h
#define MC_h

#include <vector>
using namespace std;

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
   const Int_t kMaxmc = 1;
   const Int_t kMaxmc_simpart = 100;
   const Int_t kMaxmc_simver = 1;
   const Int_t kMaxmc_neut = 1;
   const Int_t kMaxmc_hitpmt = 200;

class MC {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
 //T2KWCEvent      *mc_;
   UInt_t          mc_TObject_fUniqueID;
   UInt_t          mc_TObject_fBits;
   Int_t           mc_npmts;
   Int_t           mc_nsimparts;
   Int_t           mc_nsimvers;
   Int_t           mc_nneuts;
   Int_t           mc_totnphotons;
   Int_t           mc_totnpes;
   Int_t           mc_totnpes_pmt;
   ULong64_t       mc_seed;
   Int_t           mc_fvwater;
   Int_t           mc_simpart_;
   UInt_t          mc_simpart_fUniqueID[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_fBits[kMaxmc_simpart];   //[mc.simpart_]
   Int_t           mc_simpart_pid[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_absmom[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_dir_fUniqueID[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_dir_fBits[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_dir_fX[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_dir_fY[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_dir_fZ[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_ipos_fUniqueID[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_ipos_fBits[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_ipos_fX[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_ipos_fY[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_ipos_fZ[kMaxmc_simpart];   //[mc.simpart_]
   Int_t           mc_simpart_iflag[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_fpos_fUniqueID[kMaxmc_simpart];   //[mc.simpart_]
   UInt_t          mc_simpart_fpos_fBits[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_fpos_fX[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_fpos_fY[kMaxmc_simpart];   //[mc.simpart_]
   Double_t        mc_simpart_fpos_fZ[kMaxmc_simpart];   //[mc.simpart_]
   Int_t           mc_simpart_fflag[kMaxmc_simpart];   //[mc.simpart_]
   Int_t           mc_simver_;
   UInt_t          mc_simver_fUniqueID[kMaxmc_simver];   //[mc.simver_]
   UInt_t          mc_simver_fBits[kMaxmc_simver];   //[mc.simver_]
   UInt_t          mc_simver_vertex_fUniqueID[kMaxmc_simver];   //[mc.simver_]
   UInt_t          mc_simver_vertex_fBits[kMaxmc_simver];   //[mc.simver_]
   Double_t        mc_simver_vertex_fX[kMaxmc_simver];   //[mc.simver_]
   Double_t        mc_simver_vertex_fY[kMaxmc_simver];   //[mc.simver_]
   Double_t        mc_simver_vertex_fZ[kMaxmc_simver];   //[mc.simver_]
   Int_t           mc_simver_vflag[kMaxmc_simver];   //[mc.simver_]
   Int_t           mc_neut_;
   UInt_t          mc_neut_fUniqueID[kMaxmc_neut];   //[mc.neut_]
   UInt_t          mc_neut_fBits[kMaxmc_neut];   //[mc.neut_]
   Double_t        mc_neut_energy[kMaxmc_neut];   //[mc.neut_]
   Int_t           mc_neut_mode[kMaxmc_neut];   //[mc.neut_]
   UInt_t          mc_neut_dir_fUniqueID[kMaxmc_neut];   //[mc.neut_]
   UInt_t          mc_neut_dir_fBits[kMaxmc_neut];   //[mc.neut_]
   Double_t        mc_neut_dir_fX[kMaxmc_neut];   //[mc.neut_]
   Double_t        mc_neut_dir_fY[kMaxmc_neut];   //[mc.neut_]
   Double_t        mc_neut_dir_fZ[kMaxmc_neut];   //[mc.neut_]
   Double_t        mc_neut_norm[kMaxmc_neut];   //[mc.neut_]
   Double_t        mc_neut_totcrsne[kMaxmc_neut];   //[mc.neut_]
   Int_t           mc_neut_ng[kMaxmc_neut];   //[mc.neut_]
   vector<Int_t>   mc_neut_gpid[kMaxmc_neut];
   vector<Int_t>   mc_neut_gmec[kMaxmc_neut];
   vector<Double_t> mc_neut_gposx[kMaxmc_neut];
   vector<Double_t> mc_neut_gposy[kMaxmc_neut];
   vector<Double_t> mc_neut_gposz[kMaxmc_neut];
   vector<Double_t> mc_neut_gmomx[kMaxmc_neut];
   vector<Double_t> mc_neut_gmomy[kMaxmc_neut];
   vector<Double_t> mc_neut_gmomz[kMaxmc_neut];
   vector<Double_t> mc_neut_gcosbm[kMaxmc_neut];
   Int_t           mc_hitpmt_;
   UInt_t          mc_hitpmt_fUniqueID[kMaxmc_hitpmt];   //[mc.hitpmt_]
   UInt_t          mc_hitpmt_fBits[kMaxmc_hitpmt];   //[mc.hitpmt_]
   Int_t           mc_hitpmt_id[kMaxmc_hitpmt];   //[mc.hitpmt_]
   Int_t           mc_hitpmt_npe[kMaxmc_hitpmt];   //[mc.hitpmt_]
   Double_t        mc_hitpmt_time[kMaxmc_hitpmt];   //[mc.hitpmt_]
   Short_t         mc_hitpmt_qdc[kMaxmc_hitpmt];   //[mc.hitpmt_]
   Short_t         mc_hitpmt_tdc[kMaxmc_hitpmt];   //[mc.hitpmt_]

   // List of branches
   TBranch        *b_mc_TObject_fUniqueID;   //!
   TBranch        *b_mc_TObject_fBits;   //!
   TBranch        *b_mc_npmts;   //!
   TBranch        *b_mc_nsimparts;   //!
   TBranch        *b_mc_nsimvers;   //!
   TBranch        *b_mc_nneuts;   //!
   TBranch        *b_mc_totnphotons;   //!
   TBranch        *b_mc_totnpes;   //!
   TBranch        *b_mc_totnpes_pmt;   //!
   TBranch        *b_mc_seed;   //!
   TBranch        *b_mc_fvwater;   //!
   TBranch        *b_mc_simpart_;   //!
   TBranch        *b_mc_simpart_fUniqueID;   //!
   TBranch        *b_mc_simpart_fBits;   //!
   TBranch        *b_mc_simpart_pid;   //!
   TBranch        *b_mc_simpart_absmom;   //!
   TBranch        *b_mc_simpart_dir_fUniqueID;   //!
   TBranch        *b_mc_simpart_dir_fBits;   //!
   TBranch        *b_mc_simpart_dir_fX;   //!
   TBranch        *b_mc_simpart_dir_fY;   //!
   TBranch        *b_mc_simpart_dir_fZ;   //!
   TBranch        *b_mc_simpart_ipos_fUniqueID;   //!
   TBranch        *b_mc_simpart_ipos_fBits;   //!
   TBranch        *b_mc_simpart_ipos_fX;   //!
   TBranch        *b_mc_simpart_ipos_fY;   //!
   TBranch        *b_mc_simpart_ipos_fZ;   //!
   TBranch        *b_mc_simpart_iflag;   //!
   TBranch        *b_mc_simpart_fpos_fUniqueID;   //!
   TBranch        *b_mc_simpart_fpos_fBits;   //!
   TBranch        *b_mc_simpart_fpos_fX;   //!
   TBranch        *b_mc_simpart_fpos_fY;   //!
   TBranch        *b_mc_simpart_fpos_fZ;   //!
   TBranch        *b_mc_simpart_fflag;   //!
   TBranch        *b_mc_simver_;   //!
   TBranch        *b_mc_simver_fUniqueID;   //!
   TBranch        *b_mc_simver_fBits;   //!
   TBranch        *b_mc_simver_vertex_fUniqueID;   //!
   TBranch        *b_mc_simver_vertex_fBits;   //!
   TBranch        *b_mc_simver_vertex_fX;   //!
   TBranch        *b_mc_simver_vertex_fY;   //!
   TBranch        *b_mc_simver_vertex_fZ;   //!
   TBranch        *b_mc_simver_vflag;   //!
   TBranch        *b_mc_neut_;   //!
   TBranch        *b_mc_neut_fUniqueID;   //!
   TBranch        *b_mc_neut_fBits;   //!
   TBranch        *b_mc_neut_energy;   //!
   TBranch        *b_mc_neut_mode;   //!
   TBranch        *b_mc_neut_dir_fUniqueID;   //!
   TBranch        *b_mc_neut_dir_fBits;   //!
   TBranch        *b_mc_neut_dir_fX;   //!
   TBranch        *b_mc_neut_dir_fY;   //!
   TBranch        *b_mc_neut_dir_fZ;   //!
   TBranch        *b_mc_neut_norm;   //!
   TBranch        *b_mc_neut_totcrsne;   //!
   TBranch        *b_mc_neut_ng;   //!
   TBranch        *b_mc_neut_gpid;   //!
   TBranch        *b_mc_neut_gmec;   //!
   TBranch        *b_mc_neut_gposx;   //!
   TBranch        *b_mc_neut_gposy;   //!
   TBranch        *b_mc_neut_gposz;   //!
   TBranch        *b_mc_neut_gmomx;   //!
   TBranch        *b_mc_neut_gmomy;   //!
   TBranch        *b_mc_neut_gmomz;   //!
   TBranch        *b_mc_neut_gcosbm;   //!
   TBranch        *b_mc_hitpmt_;   //!
   TBranch        *b_mc_hitpmt_fUniqueID;   //!
   TBranch        *b_mc_hitpmt_fBits;   //!
   TBranch        *b_mc_hitpmt_id;   //!
   TBranch        *b_mc_hitpmt_npe;   //!
   TBranch        *b_mc_hitpmt_time;   //!
   TBranch        *b_mc_hitpmt_qdc;   //!
   TBranch        *b_mc_hitpmt_tdc;   //!

   MC(TTree *tree=0);
   virtual ~MC();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MC_cxx
MC::MC(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../mizusim/T2KWC_output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../mizusim/T2KWC_output.root");
      }
      f->GetObject("mc",tree);

   }
   Init(tree);
}

MC::~MC()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MC::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MC::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MC::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc.TObject.fUniqueID", &mc_TObject_fUniqueID, &b_mc_TObject_fUniqueID);
   fChain->SetBranchAddress("mc.TObject.fBits", &mc_TObject_fBits, &b_mc_TObject_fBits);
   fChain->SetBranchAddress("mc.npmts", &mc_npmts, &b_mc_npmts);
   fChain->SetBranchAddress("mc.nsimparts", &mc_nsimparts, &b_mc_nsimparts);
   fChain->SetBranchAddress("mc.nsimvers", &mc_nsimvers, &b_mc_nsimvers);
   fChain->SetBranchAddress("mc.nneuts", &mc_nneuts, &b_mc_nneuts);
   fChain->SetBranchAddress("mc.totnphotons", &mc_totnphotons, &b_mc_totnphotons);
   fChain->SetBranchAddress("mc.totnpes", &mc_totnpes, &b_mc_totnpes);
   fChain->SetBranchAddress("mc.totnpes_pmt", &mc_totnpes_pmt, &b_mc_totnpes_pmt);
   fChain->SetBranchAddress("mc.seed", &mc_seed, &b_mc_seed);
   fChain->SetBranchAddress("mc.fvwater", &mc_fvwater, &b_mc_fvwater);
   fChain->SetBranchAddress("mc.simpart", &mc_simpart_, &b_mc_simpart_);
   fChain->SetBranchAddress("mc.simpart.fUniqueID", mc_simpart_fUniqueID, &b_mc_simpart_fUniqueID);
   fChain->SetBranchAddress("mc.simpart.fBits", mc_simpart_fBits, &b_mc_simpart_fBits);
   fChain->SetBranchAddress("mc.simpart.pid", mc_simpart_pid, &b_mc_simpart_pid);
   fChain->SetBranchAddress("mc.simpart.absmom", mc_simpart_absmom, &b_mc_simpart_absmom);
   fChain->SetBranchAddress("mc.simpart.dir.fUniqueID", mc_simpart_dir_fUniqueID, &b_mc_simpart_dir_fUniqueID);
   fChain->SetBranchAddress("mc.simpart.dir.fBits", mc_simpart_dir_fBits, &b_mc_simpart_dir_fBits);
   fChain->SetBranchAddress("mc.simpart.dir.fX", mc_simpart_dir_fX, &b_mc_simpart_dir_fX);
   fChain->SetBranchAddress("mc.simpart.dir.fY", mc_simpart_dir_fY, &b_mc_simpart_dir_fY);
   fChain->SetBranchAddress("mc.simpart.dir.fZ", mc_simpart_dir_fZ, &b_mc_simpart_dir_fZ);
   fChain->SetBranchAddress("mc.simpart.ipos.fUniqueID", mc_simpart_ipos_fUniqueID, &b_mc_simpart_ipos_fUniqueID);
   fChain->SetBranchAddress("mc.simpart.ipos.fBits", mc_simpart_ipos_fBits, &b_mc_simpart_ipos_fBits);
   fChain->SetBranchAddress("mc.simpart.ipos.fX", mc_simpart_ipos_fX, &b_mc_simpart_ipos_fX);
   fChain->SetBranchAddress("mc.simpart.ipos.fY", mc_simpart_ipos_fY, &b_mc_simpart_ipos_fY);
   fChain->SetBranchAddress("mc.simpart.ipos.fZ", mc_simpart_ipos_fZ, &b_mc_simpart_ipos_fZ);
   fChain->SetBranchAddress("mc.simpart.iflag", mc_simpart_iflag, &b_mc_simpart_iflag);
   fChain->SetBranchAddress("mc.simpart.fpos.fUniqueID", mc_simpart_fpos_fUniqueID, &b_mc_simpart_fpos_fUniqueID);
   fChain->SetBranchAddress("mc.simpart.fpos.fBits", mc_simpart_fpos_fBits, &b_mc_simpart_fpos_fBits);
   fChain->SetBranchAddress("mc.simpart.fpos.fX", mc_simpart_fpos_fX, &b_mc_simpart_fpos_fX);
   fChain->SetBranchAddress("mc.simpart.fpos.fY", mc_simpart_fpos_fY, &b_mc_simpart_fpos_fY);
   fChain->SetBranchAddress("mc.simpart.fpos.fZ", mc_simpart_fpos_fZ, &b_mc_simpart_fpos_fZ);
   fChain->SetBranchAddress("mc.simpart.fflag", mc_simpart_fflag, &b_mc_simpart_fflag);
   fChain->SetBranchAddress("mc.simver", &mc_simver_, &b_mc_simver_);
   fChain->SetBranchAddress("mc.simver.fUniqueID", mc_simver_fUniqueID, &b_mc_simver_fUniqueID);
   fChain->SetBranchAddress("mc.simver.fBits", mc_simver_fBits, &b_mc_simver_fBits);
   fChain->SetBranchAddress("mc.simver.vertex.fUniqueID", mc_simver_vertex_fUniqueID, &b_mc_simver_vertex_fUniqueID);
   fChain->SetBranchAddress("mc.simver.vertex.fBits", mc_simver_vertex_fBits, &b_mc_simver_vertex_fBits);
   fChain->SetBranchAddress("mc.simver.vertex.fX", mc_simver_vertex_fX, &b_mc_simver_vertex_fX);
   fChain->SetBranchAddress("mc.simver.vertex.fY", mc_simver_vertex_fY, &b_mc_simver_vertex_fY);
   fChain->SetBranchAddress("mc.simver.vertex.fZ", mc_simver_vertex_fZ, &b_mc_simver_vertex_fZ);
   fChain->SetBranchAddress("mc.simver.vflag", mc_simver_vflag, &b_mc_simver_vflag);
   fChain->SetBranchAddress("mc.neut", &mc_neut_, &b_mc_neut_);
   fChain->SetBranchAddress("mc.neut.fUniqueID", &mc_neut_fUniqueID, &b_mc_neut_fUniqueID);
   fChain->SetBranchAddress("mc.neut.fBits", &mc_neut_fBits, &b_mc_neut_fBits);
   fChain->SetBranchAddress("mc.neut.energy", &mc_neut_energy, &b_mc_neut_energy);
   fChain->SetBranchAddress("mc.neut.mode", &mc_neut_mode, &b_mc_neut_mode);
   fChain->SetBranchAddress("mc.neut.dir.fUniqueID", &mc_neut_dir_fUniqueID, &b_mc_neut_dir_fUniqueID);
   fChain->SetBranchAddress("mc.neut.dir.fBits", &mc_neut_dir_fBits, &b_mc_neut_dir_fBits);
   fChain->SetBranchAddress("mc.neut.dir.fX", &mc_neut_dir_fX, &b_mc_neut_dir_fX);
   fChain->SetBranchAddress("mc.neut.dir.fY", &mc_neut_dir_fY, &b_mc_neut_dir_fY);
   fChain->SetBranchAddress("mc.neut.dir.fZ", &mc_neut_dir_fZ, &b_mc_neut_dir_fZ);
   fChain->SetBranchAddress("mc.neut.norm", &mc_neut_norm, &b_mc_neut_norm);
   fChain->SetBranchAddress("mc.neut.totcrsne", &mc_neut_totcrsne, &b_mc_neut_totcrsne);
   fChain->SetBranchAddress("mc.neut.ng", &mc_neut_ng, &b_mc_neut_ng);
   fChain->SetBranchAddress("mc.neut.gpid", &mc_neut_gpid, &b_mc_neut_gpid);
   fChain->SetBranchAddress("mc.neut.gmec", &mc_neut_gmec, &b_mc_neut_gmec);
   fChain->SetBranchAddress("mc.neut.gposx", &mc_neut_gposx, &b_mc_neut_gposx);
   fChain->SetBranchAddress("mc.neut.gposy", &mc_neut_gposy, &b_mc_neut_gposy);
   fChain->SetBranchAddress("mc.neut.gposz", &mc_neut_gposz, &b_mc_neut_gposz);
   fChain->SetBranchAddress("mc.neut.gmomx", &mc_neut_gmomx, &b_mc_neut_gmomx);
   fChain->SetBranchAddress("mc.neut.gmomy", &mc_neut_gmomy, &b_mc_neut_gmomy);
   fChain->SetBranchAddress("mc.neut.gmomz", &mc_neut_gmomz, &b_mc_neut_gmomz);
   fChain->SetBranchAddress("mc.neut.gcosbm", &mc_neut_gcosbm, &b_mc_neut_gcosbm);
   fChain->SetBranchAddress("mc.hitpmt", &mc_hitpmt_, &b_mc_hitpmt_);
   fChain->SetBranchAddress("mc.hitpmt.fUniqueID", mc_hitpmt_fUniqueID, &b_mc_hitpmt_fUniqueID);
   fChain->SetBranchAddress("mc.hitpmt.fBits", mc_hitpmt_fBits, &b_mc_hitpmt_fBits);
   fChain->SetBranchAddress("mc.hitpmt.id", mc_hitpmt_id, &b_mc_hitpmt_id);
   fChain->SetBranchAddress("mc.hitpmt.npe", mc_hitpmt_npe, &b_mc_hitpmt_npe);
   fChain->SetBranchAddress("mc.hitpmt.time", mc_hitpmt_time, &b_mc_hitpmt_time);
   fChain->SetBranchAddress("mc.hitpmt.qdc", mc_hitpmt_qdc, &b_mc_hitpmt_qdc);
   fChain->SetBranchAddress("mc.hitpmt.tdc", mc_hitpmt_tdc, &b_mc_hitpmt_tdc);
   Notify();
}

Bool_t MC::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MC::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MC::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MC_cxx
