#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class T2KWCUnpack+;
#pragma link C++ class T2KWCHit+;

#endif
