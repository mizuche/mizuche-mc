#include "T2KWCSimVertex.h"

//
T2KWCSimVertex::T2KWCSimVertex()
{
  vertex = TVector3(0,0,0);
  vflag = -1;
}

T2KWCSimVertex::T2KWCSimVertex(T2KWCSimVertex *simver)
{
	vertex = simver->Vertex();
  vflag = simver->VFlag();
}

T2KWCSimVertex::~T2KWCSimVertex()
{
}

//
void T2KWCSimVertex::Clear(Option_t *option)
{
  vertex = TVector3(0,0,0);
  vflag = -1;
}


//
void T2KWCSimVertex::Print(Option_t *option) const
{
	std::cout << "--- MC True vertex info ---" << std::endl;
	std::cout	<< "Pos: " << vertex(0) << "," << vertex(1) << "," << vertex(2) << std::endl;
  switch(vflag) {
    case 2 : 
      std::cout << "vertex in FV" << std::endl; 
      break;
    case 1 : 
      std::cout << "vertex in Tank (out FV)" << std::endl; 
      break;
    case 0 : 
      std::cout << "vertex out Tank" << std::endl; 
      break;
    default : 
      exit(1);
  }
	std::cout	<< std::endl;
}


//
ClassImp(T2KWCSimVertex)
