#include "T2KWCUnpack.h"

// __________________________________________________
T2KWCUnpack::T2KWCUnpack()
{
	NewTClonesArrays();
    Clear();
}

// __________________________________________________
T2KWCUnpack::T2KWCUnpack(const T2KWCUnpack& unpack) 
{ 
	NewTClonesArrays();
	//Clear("C");

    // Copy all arrays of detector summaries
    SetAtmNpes(unpack.GetAtmNpes());
    for(Int_t ihit = 0; ihit < unpack.atm_nhits; ++ihit ) {
		AddHit( unpack.GetHit(ihit) );
    }
    
    SetFadcNsamp(unpack.GetFadcNsamp());
    
    // for(Int_t isamp = 0; isamp < fadc_nsamp; isamp++) {
    //     pmtsum.push_back(unpack.PMTSUM(i));
    //     hitsum.push_back(unpack.HITSUM(i));
    // }

    // for (Int_t itrg = 0; itrg < (Int_t)ntrgnum.size(); i++ ) {
    //     ntrgnum.push_back(unpack.NTRGNum(i) );
    // }
}

// __________________________________________________
T2KWCUnpack::~T2KWCUnpack() 
{ 
    Clear("C");

    if( hit ) {
        delete hit;
        hit = 0;
    }
}

// __________________________________________________
void T2KWCUnpack::NewTClonesArrays()  
{ 
	hit = new TClonesArray("T2KWCHit", 1000);
}

// __________________________________________________
void T2KWCUnpack::Clear(Option_t* option) 
{
    runnum    = -1;
    spillnum  = -1;
    runmode   = -1;
    time_sec  = -1;
    time_usec = -1;
    //fvstat    = -1;
    //daqmode   = -1;
    
	atm_trg        = -1000;
    atm_gong       = -1000;
    atm_ntrg       = -1000;
    atm_nhits      =  0;    // TCloneArray gives error if this is set to negative number.
    atm_ncharges   = -1000;
    atm_npes       = -1000;
    atm_max_charge = -1000;
    atm_max_pe     = -1000;
    
    if( hit ) hit->Clear(option);
    
    fadc_nsamp    = -1000;
    raw_pmtsum0   = -1000;
    raw_hitsum0   = -1000;
    fadc_pmtsum0  = -1000;
    fadc_hitsum0  = -1000;
    fadc_ntrg     = -1000;
    fadc_nhits    = -1000;
    fadc_ncharges = -1000;
    fadc_npes     = -1000;
    for (Int_t ibunch = 0; ibunch < 9; ibunch++) {
        fadc_ncharges_bunch[ibunch] = 0;
        fadc_npes_bunch[ibunch] = 0;
    }
    //average_gain  = -1000;  // 
    
    atm_ntrgnum.clear();
    raw_pmtsum.clear();
    raw_hitsum.clear();
    fadc_pmtsum.clear();
    fadc_hitsum.clear();
    fadc_nsec.clear();
    fadc_hit.clear();
    fadc_height.clear();
    fadc_charge.clear();
    fadc_pe.clear();
}


// __________________________________________________
void T2KWCUnpack::Print(Option_t* option) const
{
    fprintf(stdout, "\n\t============= HIT SUMMARY [RUN#:%8d, SPILL#:%7d] =========================\n", runnum, spillnum);
    fprintf(stdout, "\t%10s | %8s %8s %8s %8s | %8s %8s\n",
            "", "Ntrg", "Nhits", "NQs", "Npes", "MaxQ", "MaxPe");
    fprintf(stdout, "\t%10s | %8d %8d %8.2f %8.2f | %8.2f %8.2f\n",
            "[ATM]", atm_ntrg, atm_nhits, atm_ncharges, atm_npes, atm_max_charge, atm_max_pe);
    fprintf(stdout, "\t%10s | %8d %8d %8.2f %8.2f | %8s %8s\n",
            "[FADC]", fadc_ntrg, fadc_nhits, fadc_ncharges, fadc_npes, "-", "-");
    fprintf(stdout, "\t----------------------------------------------------------------------------------\n");
    fprintf(stdout, "\t%10s | %8s %8s %8.2f %8.2f | %8s %8s\n",
            "[NO Thr]", "-", "-", fadc_ncharges_bunch[0], fadc_npes_bunch[0], "-", "-");
    fprintf(stdout, "\t===================================================================================\n\n");
}

// __________________________________________________
T2KWCHit* T2KWCUnpack::AddHit(T2KWCHit* hit0)
{
    TClonesArray &a_hit = *hit;
    new(a_hit[atm_nhits++]) T2KWCHit(*hit0);
    return (T2KWCHit*)(hit->At(atm_nhits-1));
}

// __________________________________________________
T2KWCHit* T2KWCUnpack::GetHit(int i) const
{
    if (i < atm_nhits && i>=0 )
        return (T2KWCHit*)(hit->At(i));
    return 0;
}

ClassImp(T2KWCUnpack)
