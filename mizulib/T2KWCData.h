#ifndef T2KWC_DATA_H
#define T2KWC_DATA_H

#include "T2KWCEvent.h"
#include "T2KWCPMTHit.h"
#include "T2KWCSimVertex.h"
#include "T2KWCSimParticle.h"
#include "T2KWCNeutInfo.h"

#endif
