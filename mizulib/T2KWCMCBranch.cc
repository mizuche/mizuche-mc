#include "T2KWCMCBranch.h"

Double_t h2o_totatoms()
{
  Double_t h2o_mollmass = 18.02; // [g/mol]
  Double_t h2o_atom = 18;    // atomic number
  Double_t h2o_dens = 1.; // [g/cm^3]
  Double_t abogadro = 6.02e23;    // abogadro constant
  Double_t h2o_totmass = vol_Mizu * h2o_dens; //[g]
  
  return ((h2o_totmass/h2o_mollmass) * h2o_atom * abogadro); // [atom]
}

void SetMCBranch(TTree *t, MCBranch &m)
{
  std::cout << "SetMCBranch" << std::endl;

  //
  t->Branch("totnphotons", &(m.evt.totnphotons), "totnphotons/I");
  t->Branch("totnpes", &(m.evt.totnpes), "totnpes/I");
  t->Branch("totnpes_pmt", &(m.evt.totnpes_pmt), "totnpes_pmt/I");
  t->Branch("seed", &(m.evt.seed), "seed/L");
  t->Branch("fvwater", &(m.evt.fvwater), "fvwater/I");

  //
  t->Branch("energy", &(m.neut.energy), "energy/D");
  t->Branch("flav", &(m.neut.flav), "flav/I");
  t->Branch("mode", &(m.neut.mode), "mode/I");
  t->Branch("norm", &(m.neut.norm), "norm/D");
  t->Branch("totcrsne", &(m.neut.totcrsne), "totcrsne/D");
  t->Branch("tnorm", &(m.neut.tnorm), "tnorm/D");

  //
  t->Branch("vertex", (m.simver.vertex), "vertex[4]/D");

  //
  t->Branch("nparts", &(m.simpar.nparts), "nparts/I");
  t->Branch("pid", (m.simpar.pid), "pid[nparts]/I");
  t->Branch("absmom", (m.simpar.absmom), "absmom[nparts]/D");
  t->Branch("dir", (m.simpar.dir), "dir[nparts][3]/D");
  t->Branch("ipos", (m.simpar.ipos), "ipos[nparts][4]/D");
  t->Branch("fpos", (m.simpar.fpos), "fpos[nparts][4]/D");

  //
  t->Branch("nhits", &(m.simhit.nhits), "nhits/I");
  t->Branch("window", (m.simhit.window), "window[nhits]/I");
  t->Branch("npe", (m.simhit.npe), "npe[nhits]/I");
  t->Branch("time", (m.simhit.time), "time[nhits]/D");

  std::cout << "End of SetMCBranch" << std::endl;
}

void SetMCBranchAddress(TTree *t, MCBranch &m)
{
  std::cout << "SetMCBranchAddress" << std::endl;

  //
  t->SetBranchAddress("totnphotons", &(m.evt.totnphotons));
  t->SetBranchAddress("totnpes", &(m.evt.totnpes));
  t->SetBranchAddress("totnpes_pmt", &(m.evt.totnpes_pmt));
  t->SetBranchAddress("seed", &(m.evt.seed));
  t->SetBranchAddress("fvwater", &(m.evt.fvwater));

  //
  t->SetBranchAddress("energy", &(m.neut.energy));
  t->SetBranchAddress("flav", &(m.neut.flav));
  t->SetBranchAddress("mode", &(m.neut.mode));
  t->SetBranchAddress("norm", &(m.neut.norm));
  t->SetBranchAddress("totcrsne", &(m.neut.totcrsne));
  t->SetBranchAddress("tnorm", &(m.neut.tnorm));

  //
  t->SetBranchAddress("vertex", &(m.simver.vertex));

  //
  t->SetBranchAddress("nparts", &(m.simpar.nparts));
  t->SetBranchAddress("pid", (m.simpar.pid));
  t->SetBranchAddress("absmom", (m.simpar.absmom));
  t->SetBranchAddress("dir", &(m.simpar.dir));
  t->SetBranchAddress("ipos", &(m.simpar.ipos));
  t->SetBranchAddress("fpos", &(m.simpar.fpos));

  //
  t->SetBranchAddress("nhits", &(m.simhit.nhits) );
  t->SetBranchAddress("window", (m.simhit.window) );
  t->SetBranchAddress("npe", (m.simhit.npe) );
  t->SetBranchAddress("time", (m.simhit.time) );

  std::cout << "End of SetMCBranchAddress" << std::endl;
}

void ClearMCBranch(MCBranch &m)
{
  std::cout << "ClearMCBranch" << std::endl;

  //
  m.evt.totnphotons = 0;
  m.evt.totnpes = 0;
  m.evt.totnpes_pmt = 0;
  m.evt.seed = -1;
  m.evt.fvwater = -1111;

  //
  m.neut.energy = -1.;
  m.neut.flav = -1;
  m.neut.mode = -1;
  for(int i=0; i<3; i++) m.neut.dir[i] = -1111.;
  m.neut.norm = -1.;
  m.neut.totcrsne = -1.;
  m.neut.tnorm = -1.;

  //
  for(int i=0; i<4; i++) m.simver.vertex[i] = -1111.;

  //
  m.simpar.nparts = 0;
  for(int i=0; i<MAXNSIMPARS; i++) {
    m.simpar.pid[i] = -1;
    m.simpar.absmom[i] = -1.;
    for(int j=0; j<3; j++) m.simpar.dir[i][j] = -1111.;
    for(int j=0; j<4; j++) {
      m.simpar.ipos[i][j] = -1111.;
      m.simpar.fpos[i][j] = -1111.;
    }
  }

  //
  m.simhit.nhits = 0;
  for(int i=0; i<MAXNSIMHITS; i++) {
    m.simhit.window[i] = -1;
    m.simhit.npe[i] = -1;
    m.simhit.time[i] = -1.;
  }

  std::cout << "End of ClearMCBranch" << std::endl;
}

void PrintEventInfo(EventInfo evt)
{

}

void PrintNeutInfo(NeutInfo neut)
{
  std::cout << "--- Neut info ---" << std::endl;
  std::cout << "Energy: " << neut.energy << std::endl;
  std::cout << "Flav: " << neut.flav << std::endl;
  std::cout << "Interaction Mode: " << neut.mode << std::endl;
  std::cout << "Norm: " << neut.norm << std::endl;
  std::cout << "Total cross-section: " << neut.totcrsne << std::endl;
  std::cout << std::endl;
}

void PrintSimParticle(SimParticle simpar)
{
  std::cout << "--- SimParticle info ---" << std::endl;
  for(int i=0; i<(simpar.nparts); i++) {
    std::cout << "ID: " << i << std::endl;
    std::cout << " PID(PDG code): " << simpar.pid[i] << std::endl;
    std::cout << " init mom: " << simpar.absmom[i] << std::endl;
    std::cout << " init dir: " << simpar.dir[i][0] << "," 
                               << simpar.dir[i][1] << "," 
                               << simpar.dir[i][2]
              << "  theta: " << TMath::ACos(simpar.dir[i][2])/TMath::Pi()*180. 
              << std::endl;
    std::cout << " init pos: " << simpar.ipos[i][0] << ", " 
                               << simpar.ipos[i][1] << ", " 
                               << simpar.ipos[i][2]
                               << " : " << simpar.ipos[i][3] << std::endl;
    std::cout << " final pos: "<< simpar.fpos[i][0] << ", " 
                               << simpar.fpos[i][1] << ", " 
                               << simpar.fpos[i][2]
                               << " : " << simpar.fpos[i][3] << std::endl;
  }
}

void PrintSimVertex(SimVertex simver)
{
  std::cout << "--- MC True vertex info ---" << std::endl;
  std::cout << "Pos: " << simver.vertex[0] << "," 
                       << simver.vertex[1] << "," 
                       << simver.vertex[2] << std::endl;
  Int_t vflag = (Int_t)simver.vertex[3];

  switch(vflag) {
    case 2 :
      std::cout << "vertex in FV" << std::endl;
      break;
    case 1 :
      std::cout << "vertex in Tank (out FV)" << std::endl;
      break;
    case 0 :
      std::cout << "vertex out Tank" << std::endl;
      break;
    default :
      exit(1);
  }
  std::cout << std::endl;
}

void PrintSimHit(SimHit simhit)
{
  std::cout << "--- PMT MC Hit Info ---" << std::endl;
  for(int i=0; i<(simhit.nhits); i++) {
    std::cout << " PMT Window: " << simhit.window[i] << std::endl;
    std::cout << " P.E. : " << simhit.npe[i] << std::endl;
    std::cout << " Time : " << simhit.time[i] << std::endl;
  }
}
