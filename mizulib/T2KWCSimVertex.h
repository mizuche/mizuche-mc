#ifndef T2KWCSIMVERTEX_H
#define T2KWCSIMVERTEX_H

#include <iostream>
#include "TObject.h"
#include "TVector3.h"

//
class T2KWCSimVertex : public TObject
{
private:

	TVector3 vertex;  	// vertex point [cm]
	Int_t vflag;  			// position flag (in FV:2, in Tank:1, out Tank:0)

public:

	T2KWCSimVertex();
	T2KWCSimVertex(T2KWCSimVertex *simver);
	virtual ~T2KWCSimVertex();

	void Clear(Option_t *option="");
	void Print(Option_t *option="") const;

public:

	TVector3 Vertex() { return vertex; }
	Int_t VFlag()     { return vflag; }

	void SetVertex(double x, double y, double z, int v)
  {
    vertex = TVector3(x,y,z);
    vflag = v;
  }

	void SetVertex(TVector3 vec, int v) 
  {
    vertex = vec;
    vflag = v;
  }

	//
	ClassDef(T2KWCSimVertex,1)

};
	
#endif
