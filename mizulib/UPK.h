//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Dec 27 14:47:22 2011 by ROOT version 5.30/02
// from TTree upk/unpacked raw data
// found on file: ../analysis/2011Dec/upk_260.root
//////////////////////////////////////////////////////////

#ifndef UPK_h
#define UPK_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
   const Int_t kMaxupk = 1;
   const Int_t kMaxupk_hit = 3560;

#include <vector>
using namespace std;

class UPK {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
 //T2KWCUnpack     *upk_;
   UInt_t          upk_TObject_fUniqueID;
   UInt_t          upk_TObject_fBits;
   Int_t           upk_nhits;
   Int_t           upk_totnpes;
   Int_t           upk_runnum;
   Int_t           upk_spillnum;
   Int_t           upk_trgnum;
   Int_t           upk_gong;
   Int_t           upk_runmode;
   Int_t           upk_hit_;
   UInt_t          upk_hit_fUniqueID[kMaxupk_hit];   //[upk.hit_]
   UInt_t          upk_hit_fBits[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_hit_ch[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_hit_pmt[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_hit_window[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_hit_type[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_hit_tag[kMaxupk_hit];   //[upk.hit_]
   Short_t         upk_hit_qdc[kMaxupk_hit];   //[upk.hit_]
   Short_t         upk_hit_tdc[kMaxupk_hit];   //[upk.hit_]
   Double_t        upk_hit_charge[kMaxupk_hit];   //[upk.hit_]
   Double_t        upk_hit_time[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_hit_pe[kMaxupk_hit];   //[upk.hit_]
   Int_t           upk_nsamp;
   vector<Double_t> upk_pmtsum;
   vector<Double_t> upk_hitsum;

   // List of branches
   TBranch        *b_upk_TObject_fUniqueID;   //!
   TBranch        *b_upk_TObject_fBits;   //!
   TBranch        *b_upk_nhits;   //!
   TBranch        *b_upk_totnpes;   //!
   TBranch        *b_upk_runnum;   //!
   TBranch        *b_upk_spillnum;   //!
   TBranch        *b_upk_trgnum;   //!
   TBranch        *b_upk_gong;   //!
   TBranch        *b_upk_runmode;   //!
   TBranch        *b_upk_hit_;   //!
   TBranch        *b_upk_hit_fUniqueID;   //!
   TBranch        *b_upk_hit_fBits;   //!
   TBranch        *b_upk_hit_ch;   //!
   TBranch        *b_upk_hit_pmt;   //!
   TBranch        *b_upk_hit_window;   //!
   TBranch        *b_upk_hit_type;   //!
   TBranch        *b_upk_hit_tag;   //!
   TBranch        *b_upk_hit_qdc;   //!
   TBranch        *b_upk_hit_tdc;   //!
   TBranch        *b_upk_hit_charge;   //!
   TBranch        *b_upk_hit_time;   //!
   TBranch        *b_upk_hit_pe;   //!
   TBranch        *b_upk_nsamp;   //!
   TBranch        *b_upk_pmtsum;   //!
   TBranch        *b_upk_hitsum;   //!

   UPK(TTree *tree=0);
   virtual ~UPK();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef UPK_cxx
UPK::UPK(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../analysis/2011Dec/upk_260.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../analysis/2011Dec/upk_260.root");
      }
      f->GetObject("upk",tree);

   }
   Init(tree);
}

UPK::~UPK()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t UPK::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t UPK::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void UPK::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("upk.TObject.fUniqueID", &upk_TObject_fUniqueID, &b_upk_TObject_fUniqueID);
   fChain->SetBranchAddress("upk.TObject.fBits", &upk_TObject_fBits, &b_upk_TObject_fBits);
   fChain->SetBranchAddress("upk.nhits", &upk_nhits, &b_upk_nhits);
   fChain->SetBranchAddress("upk.totnpes", &upk_totnpes, &b_upk_totnpes);
   fChain->SetBranchAddress("upk.runnum", &upk_runnum, &b_upk_runnum);
   fChain->SetBranchAddress("upk.spillnum", &upk_spillnum, &b_upk_spillnum);
   fChain->SetBranchAddress("upk.trgnum", &upk_trgnum, &b_upk_trgnum);
   fChain->SetBranchAddress("upk.gong", &upk_gong, &b_upk_gong);
   fChain->SetBranchAddress("upk.runmode", &upk_runmode, &b_upk_runmode);
   fChain->SetBranchAddress("upk.hit", &upk_hit_, &b_upk_hit_);
   fChain->SetBranchAddress("upk.hit.fUniqueID", upk_hit_fUniqueID, &b_upk_hit_fUniqueID);
   fChain->SetBranchAddress("upk.hit.fBits", upk_hit_fBits, &b_upk_hit_fBits);
   fChain->SetBranchAddress("upk.hit.ch", upk_hit_ch, &b_upk_hit_ch);
   fChain->SetBranchAddress("upk.hit.pmt", upk_hit_pmt, &b_upk_hit_pmt);
   fChain->SetBranchAddress("upk.hit.window", upk_hit_window, &b_upk_hit_window);
   fChain->SetBranchAddress("upk.hit.type", upk_hit_type, &b_upk_hit_type);
   fChain->SetBranchAddress("upk.hit.tag", upk_hit_tag, &b_upk_hit_tag);
   fChain->SetBranchAddress("upk.hit.qdc", upk_hit_qdc, &b_upk_hit_qdc);
   fChain->SetBranchAddress("upk.hit.tdc", upk_hit_tdc, &b_upk_hit_tdc);
   fChain->SetBranchAddress("upk.hit.charge", upk_hit_charge, &b_upk_hit_charge);
   fChain->SetBranchAddress("upk.hit.time", upk_hit_time, &b_upk_hit_time);
   fChain->SetBranchAddress("upk.hit.pe", upk_hit_pe, &b_upk_hit_pe);
   fChain->SetBranchAddress("upk.nsamp", &upk_nsamp, &b_upk_nsamp);
   fChain->SetBranchAddress("upk.pmtsum", &upk_pmtsum, &b_upk_pmtsum);
   fChain->SetBranchAddress("upk.hitsum", &upk_hitsum, &b_upk_hitsum);
   Notify();
}

Bool_t UPK::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void UPK::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t UPK::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef UPK_cxx
