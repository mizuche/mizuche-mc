#include "T2KWCSimParticle.h"

#include "TMath.h"

//
T2KWCSimParticle::T2KWCSimParticle()
{
	pid = 0;
	absmom = -1.;
  iflag = -1;
  fflag = -1;
  dir = TVector3(0,0,0);
  ipos = TVector3(0,0,0);
  fpos = TVector3(0,0,0);
}

//
T2KWCSimParticle::T2KWCSimParticle(T2KWCSimParticle *simpart)
{
	pid = simpart->PID();
	absmom = simpart->Absmom();
  iflag = simpart->Iflag();
  fflag = simpart->Fflag();
  dir = simpart->Dir();
  ipos = simpart->Ipos();
  fpos = simpart->Fpos();
}

T2KWCSimParticle::~T2KWCSimParticle()
{
}

//
void T2KWCSimParticle::Clear(Option_t *option)
{
	pid = 0;
	absmom = -1.;
  iflag = -1;
  fflag = -1;
  dir = TVector3(0,0,0);
  ipos = TVector3(0,0,0);
  fpos = TVector3(0,0,0);
}

//
void T2KWCSimParticle::Print(Option_t *option) const
{
	std::cout << "--- SimParticle info ---" << std::endl;
	std::cout	<< "PID(PDG code): " << pid << std::endl;
	std::cout	<< "init mom: " << absmom << std::endl;
	std::cout	<< "init dir: " << dir(0) << "," << dir(1) << "," << dir(2) 
            << "  theta: " << TMath::ACos(dir[2])/TMath::Pi()*180. << std::endl;
  std::cout << "init pos: " << ipos(0) << ", " << ipos(1) << ", " << ipos(2) 
                            << " : " << iflag << std::endl;
  std::cout << "final pos: "<< fpos(0) << ", " << fpos(1) << ", " << fpos(2) 
                            << " : " << fflag << std::endl;
	std::cout	<< std::endl;
}


//
ClassImp(T2KWCSimParticle)
