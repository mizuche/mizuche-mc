#ifndef T2KWCEVENT_H
#define T2KWCEVENT_H

#include <iostream>

#include "TObject.h"
#include "TClonesArray.h"
#include "TRef.h"

#include "T2KWCPMTHit.h"
#include "T2KWCSimVertex.h"
#include "T2KWCSimParticle.h"
#include "T2KWCNeutInfo.h"
 

//
class T2KWCEvent : public TObject
{
private:

    Int_t npmts; 		   	// Total # of hit PMTs	
    Int_t nsimparts;		// # of simparticle
    Int_t nsimvers;		 	// # of simvertex (=1)
    Int_t nneuts;				// # of neutinfo (=1)
		Int_t totnphotons;	// total # of optical photons produced in this event
    Int_t totnpes;			// total # of photo-electrons produced in this event
    Int_t totnpes_pmt;	// total # of photo-electrons into PMTs

    ULong64_t seed;     // use init random seed
    Int_t fvwater;      // w/ water(1), w/o water(0)

    TClonesArray* simpart;
    TClonesArray* simver;
    TClonesArray* neut;
    TClonesArray* hitpmt;

public:
    T2KWCEvent();
    T2KWCEvent(const T2KWCEvent& evt);
    virtual ~T2KWCEvent();

    void NewTClonesArrays();

    void Clear(Option_t* option="");
    void Print(Option_t* option="") const;

		//
		void SetTotNPhotons(int n) 	{ totnphotons = n; }
		void SetTotNPEs(int n) 			{ totnpes = n; }

		void SetSeed(long s)			{ seed = s; }
    void SetFVWater(int i)    { fvwater = i; }

    T2KWCPMTHit* AddPMTHit(T2KWCPMTHit*);
    T2KWCSimParticle* AddSimParticle(T2KWCSimParticle*);
    T2KWCSimVertex* AddSimVertex(T2KWCSimVertex*);
		T2KWCNeutInfo* AddNeutInfo(T2KWCNeutInfo*);

		//
    Int_t NPMTs() const 				{ return npmts;}  
    Int_t NSimParticles() const	{ return nsimparts;}  
    Int_t NSimVertexs() const	  { return nsimvers;}  
    Int_t NNeutInfos() const	  { return nneuts;}  

    Int_t TotNPhotons() const 			{ return totnphotons;}  
		Int_t TotNPEs() const 					{ return totnpes; }
		Int_t TotNPEs_PMT() const 			{ return totnpes_pmt; }

    Int_t Seed() const              { return seed; }
    Int_t FVWater() const           { return fvwater; }

    T2KWCPMTHit* GetPMTHit(int) const;
    T2KWCSimParticle* GetSimParticle(int) const;
    T2KWCSimVertex* GetSimVertex(int) const;
		T2KWCNeutInfo* GetNeutInfo(int) const;

		//
    ClassDef(T2KWCEvent, 7)
        
};

#endif 

