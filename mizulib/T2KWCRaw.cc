#include "T2KWCRaw.h"

// __________________________________________________
T2KWCRaw::T2KWCRaw()
{
}

// __________________________________________________
T2KWCRaw::T2KWCRaw(T2KWCRaw* raw)
{
  nTRG  = raw->nTRG;
  nATM  = raw->nTRG;
  nFADC = raw->nTRG;

/*
  for(int i=0; i<nTRG; i++) {
#ifdef OLDVER
    TRG[i] = raw->TRG[i];
#else
    for(int j=0; j<5; j++) TRG[i][j] = raw->TRG[i][j];
#endif
  }
  for(int i=0; i<nATM; i++) 
    ATM[i] = raw->ATM[i];

  for(int i=0; i<nFADC; i++)
    FADC[i] = raw->FADC[i];
*/

  run   = raw->run;
  mode  = raw->mode;
  spill = raw->spill;
}

// __________________________________________________
T2KWCRaw::~T2KWCRaw()
{

}


// __________________________________________________
void T2KWCRaw::Clear(Option_t* option)
{
  nTRG  = 0;
  nATM  = 0;
  nFADC = 0;

/*
  for(int i=0; i<MAX_nTRG; i++) {
#ifdef OLDVER
    TRG[i] = 0;
#else
    for(int j=0; j<5; j++) TRG[i][j] = 0;
#endif
  }
  for(int i=0; i<MAX_nATM; i++)
    ATM[i] = 0;
  
  for(int i=0; i<MAX_nFADC; i++)
    FADC[i] = 0;
*/

  run   = -1;
  mode  = -1;
  spill = 1;
}


// __________________________________________________
void T2KWCRaw::Print(Option_t* option) const
{
    printf("**************************************************\n");
    printf("\t%8s : %8s : %8s\n", "RUN #", "Spill #", "Runmode");
    printf("\t%8d : %8d : %8d\n", run, spill, mode);
    printf("  ----------------------------------------\n");
    printf("\t%8s : %8s : %8s\n", "nTRG", "nATM", "nFADC");
    printf("\t%8d : %8d : %8d\n", nTRG, nATM, nFADC);
    printf("**************************************************\n");
}

// __________________________________________________
ClassImp(T2KWCRaw)
