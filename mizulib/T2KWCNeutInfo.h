#ifndef T2KWCNEUTINFO_H
#define T2KWCNEUTINFO_H

#include <iostream>
#include <vector>
#include "TObject.h"
#include "TVector3.h"

//
class T2KWCNeutInfo : public TObject
{
private:

	Double_t energy; 		// Neutrino energy
  Int_t mode;         // Neutrino Interaction mode
	TVector3 dir;	      // neutrino direction
	Double_t norm;			// normalization factor of Jnubeam
	Double_t totcrsne;	// total cross-section [10^-38 cm^2]

  Int_t ng;                       // # of ancestor particles of neutrino
  std::vector<Int_t> gpid;        // PID of ancestors
  std::vector<Int_t> gmec;        // Generation mechanizm of ancestors
  std::vector<Double_t> gposx;    // Init x-position of ancectors
  std::vector<Double_t> gposy;    // Init y-position of ancectors
  std::vector<Double_t> gposz;    // Init z-position of ancectors
  std::vector<Double_t> gmomx;    // Init x-momentum of ancectors
  std::vector<Double_t> gmomy;    // Init y-momentum of ancectors
  std::vector<Double_t> gmomz;    // Init z-momentum of ancectors
  std::vector<Double_t> gcosbm;   // Init angle of ancestors

public:

	T2KWCNeutInfo();
	T2KWCNeutInfo(T2KWCNeutInfo *neutinfo);
	virtual ~T2KWCNeutInfo();

	void Clear(Option_t *option="");
	void Print(Option_t *option="") const;

	Double_t Energy() 	    { return energy; }
  Int_t Mode()            { return mode; } 
	TVector3 Dir()          { return dir; }
	Double_t Norm()         { return norm; }
	Double_t Totcrsne()     { return totcrsne; }

  Int_t NumG()            { return ng; }
  Int_t GPid(int i)       { return (i<ng) ? gpid[i] : 0; }
  Int_t GMec(int i)       { return (i<ng) ? gmec[i] : 0; }
  Double_t GCosbm(int i)  { return (i<ng) ? gcosbm[i] : 0.; }
  TVector3 GPos(int i)    { return (i<ng) ? TVector3(gposx[i],gposy[i],gposz[i]) : TVector3(0,0,0); }
  TVector3 GMom(int i)    { return (i<ng) ? TVector3(gmomx[i],gmomy[i],gmomz[i]) : TVector3(0,0,0); }

  //
	void SetEnergy(double e)  { energy = e; }
  void SetMode(int m)       { mode = m; }
	void SetDir(double x, double y, double z) { dir = TVector3(x,y,z); }
	void SetDir(TVector3 vec)   { dir = vec; }
	void SetNorm(double n)      { norm = n; }
	void SetTotcrsne(double x)  { totcrsne = x; }

  void SetNumG(int i)           { ng = i; }
  void SetGPid(int p)         { gpid.push_back(p); } 
  void SetGMec(int m)         { gmec.push_back(m); }
  void SetGCosbm(double c)   { gcosbm.push_back(c); }
  void SetGPos(double x, double y, double z) { 
    gposx.push_back( x ); 
    gposy.push_back( y ); 
    gposz.push_back( z ); 
  }
  void SetGPos(TVector3 vec) { 
    gposx.push_back( vec(0) ); 
    gposy.push_back( vec(1) ); 
    gposz.push_back( vec(2) ); 
  }
  void SetGMom(double x, double y, double z) { 
    gmomx.push_back( x ); 
    gmomy.push_back( y ); 
    gmomz.push_back( z ); 
  }
  void SetGMom(TVector3 vec) { 
    gmomx.push_back( vec(0) ); 
    gmomy.push_back( vec(1) ); 
    gmomz.push_back( vec(2) ); 
  }

	//
	ClassDef(T2KWCNeutInfo,8)

};
	
#endif
