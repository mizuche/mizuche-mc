// -*- mode : c++ -*-
#ifndef T2KWCEventDisplay_H
#define T2KWCEventDisplay_H

#include <iostream>

#include <TStyle.h>
#include <TROOT.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TString.h>
#include <TBox.h>
#include <TEllipse.h>
#include <TMath.h>
#include <TColor.h>
#include <TText.h>
#include <TVector3.h>
#include <TGraph.h>

#include "T2KWCEventDisplayParam.h"
#include "T2KWCUnpack.h"
#include "UPK.h"

const double fadc_volt = 0.488; // mV/count
const double fadc_time = 4.e-3; // us/sample

// __________________________________________________
class T2KWCEvtDisp {
private:
    TPad*           fPView;
    TH1D*           fHView;
    TBox*           fBarrel;
    TEllipse*       fEndCapUp;
    TEllipse*       fEndCapDown;
    TEllipse*       fPMT[npmts];
    TBox*           fTrigger[gNtrigger];

    TGraph *gPmtsum;
    TGraph *gHitsum;
  
    Int_t nphotons;
    Double_t npes;
    Double_t totalpe;
    Int_t nhitpmts;

public:
    T2KWCEvtDisp(){};
    ~T2KWCEvtDisp();

    
    void Init( TCanvas* );
    void Clear();

    Int_t InitTankDisplay();

    void InputData(T2KWCUnpack*);
    void InputData(UPK*);

    void SetHit(Int_t, Double_t);
    void SetHit(Int_t, Double_t, Double_t);

    void ConvertGeometry(Double_t,Double_t,Double_t,Double_t*);
    const char* GetModeName(Int_t);

    void Draw(TCanvas*);

    Int_t GetColor(Int_t);

};


// __________________________________________________
T2KWCEvtDisp::~T2KWCEvtDisp()
{ 
    if( fPView )      delete fPView;
    if( fHView )      delete fHView;
    if( fBarrel )     delete fBarrel;
    if( fEndCapUp )   delete fEndCapUp;
    if( fEndCapDown ) delete fEndCapDown;
    for (int i = 0;i < npmts; i++) {
        if( fPMT[i] ) delete fPMT[i];
    }
    for (Int_t i = 0; i < gNtrigger; i++) {
        if (fTrigger[i]) delete fTrigger[i];
    }
}

// __________________________________________________
Int_t T2KWCEvtDisp::InitTankDisplay()
{
    Double_t circle = TMath::Pi()*2*r_tank; // [cm]
    Int_t fon = 132;
    
    //
    Double_t xlo = 0.0;
    Double_t ylo = 0.0;
    Double_t xup = 1.0;
    Double_t yup = 1.0;
    fPView = new TPad("fPView","Mizuche Event Display", xlo, ylo, xup, yup);
    //
    Int_t nbinsx = Int_t(10*(l_tank+r_tank*4+60));
    xlo = -1*(l_tank/2.+r_tank*2.+30);
    xup = (l_tank/2.+r_tank*2.+30);
    fHView = new TH1D("fHView", "Mizuche Event Display", nbinsx, xlo, xup);
    fHView->SetBit( kCanDelete );
    fHView->SetMinimum( -1.*(circle/2.+30) );
    fHView->SetMaximum( (circle/2.+30) );

    fHView->SetLineColor(0);
    fHView->SetXTitle("Beam Direction (Left #rightarrow Right) [cm]");
    fHView->SetYTitle("[cm]");
    fHView->SetTitleFont(fon,"");
    fHView->SetTitleFont(fon,"XY");
    fHView->SetTitleSize(0.045,"XY");
    fHView->GetXaxis()->CenterTitle();
    fHView->GetYaxis()->CenterTitle();
    fHView->SetLabelFont(fon,"XY");
    fHView->SetLabelSize(0.05,"XY");
    fHView->SetNdivisions(505, "XY");
    fHView->SetStats(0);
    
    //
    double x1 = -1.*(l_tank/2.);
    double y1 = -1.*(circle/2.);
    double x2 = (l_tank/2.);
    double y2 = (circle/2.);
    TBox *fBarrel = new TBox(x1, y1, x2, y2);
    fBarrel->SetBit(kCanDelete);
    fBarrel->SetFillStyle(0);

    //
    TEllipse *fEndCapUp = new TEllipse( -1.*(l_tank/2.+r_tank), 0., r_tank);
    fEndCapUp->SetFillStyle(0);
    TEllipse *fEndCapDown = new TEllipse( 1.*(l_tank/2.+r_tank), 0., r_tank);
    fEndCapDown->SetFillStyle(0);

    //
    Double_t pmt[2];
    for(int i = 0; i < npmts; i++) {
        ConvertGeometry(pmt_pot[i][0], pmt_pot[i][1], pmt_pot[i][2], pmt);
        fPMT[i] = new TEllipse( pmt[0], pmt[1], r_pmt);
        fPMT[i]->SetFillColor(0);
    }

    for (int i = 0; i < gNtrigger; i++) {
        fTrigger[i] = new TBox( trg_pos[i][0], trg_pos[i][1], trg_pos[i][2], trg_pos[i][3]);
        fTrigger[i]->SetBit(kCanDelete);
        fTrigger[i]->SetFillStyle(0);
    }
    
    return 0;
}



// __________________________________________________
void T2KWCEvtDisp::Init(TCanvas* c)
{
    c->cd();
    //InitTankDisplay();
    
    //char buff[300];
    Double_t circle = TMath::Pi()*2*r_tank; // [cm]
    Int_t fon = 132;

    //
    Double_t xlo = 0.0;
    Double_t ylo = 0.0;
    Double_t xup = 1.0;
    Double_t yup = 1.0;
    fPView = new TPad("fPView","Mizuche Event Display", xlo, ylo, xup, yup);
    //
    Int_t nbinsx = Int_t(10*(l_tank+r_tank*4+60));
    xlo = -1*(l_tank/2.+r_tank*2.+30);
    xup = (l_tank/2.+r_tank*2.+30);
    //sprintf(buff,"View");
    fHView = new TH1D("fHView", "Mizuche Event Display", nbinsx, xlo, xup);
    fHView->SetBit( kCanDelete );
    fHView->SetMinimum( -1.*(circle/2.+30) );
    fHView->SetMaximum( (circle/2.+30) );

    fHView->SetLineColor(0);
    fHView->SetXTitle("Beam Direction (Left #rightarrow Right) [cm]");
    fHView->SetYTitle("[cm]");
    fHView->SetTitleFont(fon,"");
    fHView->SetTitleFont(fon,"XY");
    fHView->SetTitleSize(0.045,"XY");
    fHView->GetXaxis()->CenterTitle();
    fHView->GetYaxis()->CenterTitle();
    fHView->SetLabelFont(fon,"XY");
    fHView->SetLabelSize(0.05,"XY");
    fHView->SetNdivisions(505, "XY");
    //fHView->SetNdivisions(0, "XY");
    fHView->SetStats(0);
    
    //
    double x1 = -1.*(l_tank/2.);
    double y1 = -1.*(circle/2.);
    double x2 = (l_tank/2.);
    double y2 = (circle/2.);
    TBox *fBarrel = new TBox(x1, y1, x2, y2);
    fBarrel->SetBit(kCanDelete);
    fBarrel->SetFillStyle(0);

    //
    TEllipse *fEndCapUp = new TEllipse( -1.*(l_tank/2.+r_tank), 0., r_tank);
    fEndCapUp->SetFillStyle(0);
    TEllipse *fEndCapDown = new TEllipse( 1.*(l_tank/2.+r_tank), 0., r_tank);
    fEndCapDown->SetFillStyle(0);

    //
    Double_t pmt[2];
    for(int i = 0; i < npmts; i++) {
        ConvertGeometry(pmt_pot[i][0], pmt_pot[i][1], pmt_pot[i][2], pmt);
        fPMT[i] = new TEllipse( pmt[0], pmt[1], r_pmt);
        fPMT[i]->SetFillColor(0);
    }

    for (int i = 0; i < gNtrigger; i++) {
        fTrigger[i] = new TBox( trg_pos[i][0], trg_pos[i][1], trg_pos[i][2], trg_pos[i][3]);
        fTrigger[i]->SetBit(kCanDelete);
        fTrigger[i]->SetFillStyle(0);
    }
    
    // FADC hist
    gHitsum = new TGraph(0);
    gPmtsum = new TGraph(0);

    // Show Display (Canavs setting)
    c->Divide(2,1);
    c->GetPad(2)->Divide(1,2);

    // Tank
    c->cd(1);
    fPView->Draw();
    fHView->Draw();
    fBarrel->Draw();
    fEndCapUp->Draw();
    fEndCapDown->Draw();
    for(int i=0; i<npmts; i++) fPMT[i]->Draw();
    for(int i=0; i<gNtrigger; i++) fTrigger[i]->Draw();
    
    c->Modified();
    for(int i=1; i<=2; i++) { c->GetPad(i)->Modified(); }
    c->Update();
}

// __________________________________________________
void T2KWCEvtDisp::ConvertGeometry
(Double_t ix, Double_t iy, Double_t iz, Double_t *pmt)
{
    // End Cap Up
    if( iz < -78 ) {
        pmt[0] = ix - (l_tank/2.+r_tank);
        pmt[1] = iy;
    }
    // End Cap down
    else if( iz > 78 ) {
        pmt[0] = -1.*ix + (l_tank/2.+r_tank);
        pmt[1] = iy;
    }
    // Side Barrel
    else {
        Double_t angle = 0.;
        if( ix >= 0 && iy >= 0 )
            angle = TMath::ATan( iy/ix );
        else if( ix < 0 && iy < 0 )
            angle = TMath::ATan( iy/ix ) - TMath::Pi();
        else if( ix <= 0 && iy >= 0 )
            angle = TMath::ATan( iy/ix ) + TMath::Pi();
        else if( ix >0 && iy < 0 )
            angle = TMath::ATan( iy/ix );

        pmt[0] = iz;
        pmt[1] = r_tank*angle;
    }
}

// __________________________________________________
void T2KWCEvtDisp::InputData(T2KWCUnpack *upk)
{
    fHView->SetTitle("Event Display (size:p.e., color:time)");

    // PMT Hit info
    T2KWCHit* aHit;
    nphotons = -1;
    npes = upk->GetAtmNpes();

    Int_t nhits = upk->GetAtmNhits();
    if ( nhits > 0 ) {
        printf("Show %d hits\n", nhits);
        for( Int_t ihit = 0; ihit < nhits; ihit++) {
            aHit = upk->GetHit(ihit);
            if (ihit % 20 == 0) aHit->Print("i");
            else aHit->Print();
            //SetHit(aHit->GetWindow(), aHit->GetPe(), aHit->GetTime());
            SetHit(aHit->GetWindow(), aHit->GetCharge(), aHit->GetTime());
        }
    }
    upk->Print();
    
    // FADC info
    Int_t nsamp = upk->GetFadcNsamp();
    gHitsum->Set(0);
    gPmtsum->Set(0);
    
    Int_t    itime = 0;
    Double_t ihitsum = 0, ipmtsum = 0;
    for (Int_t isamp = 0; isamp < nsamp; isamp++) {
        itime   = isamp * 4;
        ihitsum = upk->GetFadcHitsum(isamp);
        ipmtsum = upk->GetFadcPmtsum(isamp);
        gHitsum->SetPoint(gHitsum->GetN(), itime, ihitsum);
        gPmtsum->SetPoint(gPmtsum->GetN(), itime, ipmtsum);
        // fprintf(stderr, "[%5d/%5d]\tH:%8.3f\tP:%8.3f\n",
        //         itime, nsamp, ihitsum, ipmtsum);
    }
    
}

// __________________________________________________
void T2KWCEvtDisp::SetHit(Int_t ipmt, Double_t npe)
{
    if( ipmt < 0 || ipmt >= npmts) {
        std::cerr << "invalid PMT window : " << ipmt  << std::endl;
        //exit(-1);
        return;
    }

    if( npe >= hit_threshold ) {
        Double_t size = r_pmt * TMath::Sqrt(TMath::Log(npe/hit_threshold));
        fPMT[ipmt]->SetR1(size);
    }
}

// __________________________________________________
void T2KWCEvtDisp::SetHit(Int_t ipmt, Double_t npe, Double_t time)
{

    if( ipmt < 0 || ipmt >= 173) {
        fprintf(stderr, "Invalid PMT window : %d\n", ipmt);
        //exit(-1);
        return;
    }
    
    Int_t t = Int_t((time-500.)/5) + 1;
    Int_t color = GetColor(t);

    if (164 <= ipmt && ipmt <= 172) {
        Int_t jpmt = ipmt - 165;
        //printf("[%s]\tjpmt:%d\t%d\n", __FUNCTION__, jpmt, color);
        fTrigger[jpmt]->SetFillStyle(1001);
        fTrigger[jpmt]->SetFillColor(color);
    } else {
        fPMT[ipmt]->SetFillColor(color);
        if( npe < 0) npe = 0;
        Double_t size = r_pmt * (TMath::Sqrt(npe/3.)+1.);
        fPMT[ipmt]->SetR1(size);
        fPMT[ipmt]->SetR2(size);
    }
}

// __________________________________________________
void T2KWCEvtDisp::Draw(TCanvas *c)
{
    // // Hit information
    // c->GetPad(1)->cd(1);
    // Int_t atm_nhits = unpack->GetAtmNhits();
    // TLegend *l = new TLegend(0.1, 0.8, 0.3, 0.9);
    // l.AddEntry("", Form("%d", atm_nhits), "");
    // l.Draw();
        
    // FADC
    c->GetPad(2)->cd(1);
    gHitsum->SetTitle("HITSUM");
    gHitsum->GetXaxis()->SetTitle("Time [ns]");
    gHitsum->GetYaxis()->SetTitle("Volt [mV]");
    if (gHitsum->GetN() != 0) { gHitsum->Draw("AL"); }

    c->GetPad(2)->cd(2);
    gPmtsum->SetTitle("PMTSUM");
    gPmtsum->GetXaxis()->SetTitle("Time [ns]");
    gPmtsum->GetYaxis()->SetTitle("Volt [mV]");
    if (gPmtsum->GetN() != 0) { gPmtsum->Draw("AL"); }
    
    //
    c->Modified();
    for(int i = 1; i<=2;i++) c->GetPad(i)->Modified();
    c->Update();
    gSystem->ProcessEvents();

}

// __________________________________________________
void T2KWCEvtDisp::Clear()
{
    nphotons = 0;
    npes = 0;
    nhitpmts = 0;
    totalpe = 0;

    for(int i=0; i<npmts; i++) {
        fPMT[i]->SetFillColor(0);
        fPMT[i]->SetR1(r_pmt);
        fPMT[i]->SetR2(r_pmt);
    }

    for (Int_t i = 0; i < 8; i++) {
        fTrigger[i]->SetFillColor(0);
        fTrigger[i]->SetFillStyle(0);
    }

    fHView->SetTitle("Mizuche Event Display");
  
}

// __________________________________________________
Int_t T2KWCEvtDisp::GetColor(Int_t n) 
{
    Float_t red = 0.;
    Float_t green = 0.;
    Float_t blue = 0.;

    if(n<64) {
        red = 0.;
        green = n*4.;
        blue = 255. - green;
    }
    else if( n>=64 && n<128 ) {
        red = (n-64)*4.;
        green = 255.;
        blue = 0.;
    }
    else if( n>=128 && n<256 ) {
        red = 255.;
        green = 255. - (n-128)*2.;
        blue = 0.;
    }
    else {
        red = 255.;
        green = 0.;
        blue = 0.;
    }

    red = red/255.;
    green = green/255.;
    blue = blue/255.;

    Int_t icol = TColor::GetColor(red, green, blue);

    return icol;
}


#endif
