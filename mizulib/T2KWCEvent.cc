#include "T2KWCEvent.h"


//
T2KWCEvent::T2KWCEvent():
	npmts(0),nsimparts(0),nsimvers(0),nneuts(0),
	totnphotons(0),totnpes(0),totnpes_pmt(0),
  simpart(0),simver(0),neut(0),hitpmt(0)
{ 
	NewTClonesArrays();
	//Clear("C");

  seed = 0;
  fvwater = -1;
}


T2KWCEvent::T2KWCEvent(const T2KWCEvent& evt) 
{ 
	NewTClonesArrays();
	//Clear("C");

  //
	totnphotons = evt.totnphotons;
	totnpes = evt.totnpes;

  seed = evt.seed;
  fvwater = evt.fvwater;

  // Copy all arrays
	for( int i=0; i < evt.npmts; i++ ) 
		AddPMTHit( evt.GetPMTHit(i) );

	for( int i=0; i < evt.nsimparts; i++ ) 
		AddSimParticle( evt.GetSimParticle(i) );

	for( int i=0; i < evt.nsimvers; i++ ) 
		AddSimVertex( evt.GetSimVertex(i) );

	for( int i=0; i < evt.nneuts; i++ ) 
		AddNeutInfo( evt.GetNeutInfo(i) );
}


T2KWCEvent::~T2KWCEvent() 
{ 
  Clear("C");

  if( hitpmt ) {
    delete hitpmt;
    hitpmt = 0;
  }

  if (simpart) {
    delete simpart;
    simpart = 0;
  }

  if (simver) {
    delete simver;
    simver = 0;
  }

	if (neut) {
		delete neut;
		neut = 0;
	}
}


//
void T2KWCEvent::NewTClonesArrays()  
{ 
	hitpmt = new TClonesArray("T2KWCPMTHit", 1000);
	simpart = new TClonesArray("T2KWCSimParticle", 1000);
	simver = new TClonesArray("T2KWCSimVertex", 1000);
	neut = new TClonesArray("T2KWCNeutInfo", 1000);
}


//
void T2KWCEvent::Clear(Option_t* option) 
{ 
	if( hitpmt ) hitpmt->Clear(option);
	if( simpart ) simpart->Clear(option);
	if( simver ) simver->Clear(option);
	if( neut ) neut->Clear(option);

	npmts = 0;
	nsimparts = 0;
  nsimvers = 0;
	nneuts = 0;
	totnphotons = 0;
	totnpes = 0;
	totnpes_pmt = 0;
}


//
void T2KWCEvent::Print(Option_t* option) const
{
  std::cout << "\n====================================" << std::endl;
	std::cout << "=== Event summary of output data ===" << std::endl;
  std::cout << "====================================" << std::endl;
	if(nsimparts>0) simpart->Print();
	if(nsimvers>0) simver->Print();
	if(nneuts>0) neut->Print();
  //if(hitpmt>0) hitpmt->Print();
  std::cout << std::endl;
}


//
T2KWCPMTHit* T2KWCEvent::AddPMTHit(T2KWCPMTHit* hitpmt0) 
{

	TClonesArray &a_pmt = *hitpmt;
	new(a_pmt[npmts++]) T2KWCPMTHit(*hitpmt0);

	totnpes_pmt += hitpmt0->NPE();

	return (T2KWCPMTHit*)(hitpmt->At(npmts-1));
}

T2KWCPMTHit* T2KWCEvent::GetPMTHit(int i) const
{ 
	if (i < npmts && i>=0 ) 
		return (T2KWCPMTHit*)(hitpmt->At(i));

	return 0;
}


//
T2KWCSimParticle* T2KWCEvent::AddSimParticle(T2KWCSimParticle* simpart0) 
{
	TClonesArray &a_spart = *simpart;
	new(a_spart[nsimparts++]) T2KWCSimParticle(*simpart0);

	return (T2KWCSimParticle*)(simpart->At(nsimparts-1));
}

T2KWCSimParticle* T2KWCEvent::GetSimParticle(int i) const
{ 
	if (i < nsimparts && i>=0 ) 
		return (T2KWCSimParticle*)(simpart->At(i));

	return 0;
}


//
T2KWCSimVertex* T2KWCEvent::AddSimVertex(T2KWCSimVertex* simver0) 
{
	TClonesArray &a_sver = *simver;
	new(a_sver[nsimvers++]) T2KWCSimVertex(*simver0);

	return (T2KWCSimVertex*)(simver->At(nsimvers-1));
}

T2KWCSimVertex* T2KWCEvent::GetSimVertex(int i) const
{ 
	if (i < nsimvers && i>=0 ) 
		return (T2KWCSimVertex*)(simver->At(i));

	return 0;
}


//
T2KWCNeutInfo* T2KWCEvent::AddNeutInfo(T2KWCNeutInfo* neut0) 
{
	TClonesArray &a_neut = *neut;
	new(a_neut[nneuts++]) T2KWCNeutInfo(*neut0);

	return (T2KWCNeutInfo*)(neut->At(nneuts-1));
}

T2KWCNeutInfo* T2KWCEvent::GetNeutInfo(int i) const
{ 
	if (i < nneuts && i>=0 ) 
		return (T2KWCNeutInfo*)(neut->At(i));

	return 0;
}


ClassImp(T2KWCEvent)
