#include "T2KWCHit.h"

// __________________________________________________
T2KWCHit::T2KWCHit()
{
    ch     = -1;
    pmt    = -1;
    window = -1;
    type   = -1;
    tag    = -1;
    qdc0   = -1;
    tdc0   = -1;
    qdc1   = -1;
    tdc1   = -1;
    charge = -1.;
    time   = -1.;
    pe     = -1;
}

// __________________________________________________
T2KWCHit::T2KWCHit(T2KWCHit* hit)
{
    ch     = hit->GetCh();
    pmt    = hit->GetPmt();
    window = hit->GetWindow();
    type   = hit->GetType();
    tag    = hit->GetTag();
    qdc0   = hit->GetQdc0();
    tdc0   = hit->GetTdc0();
    qdc1   = hit->GetQdc1();
    tdc1   = hit->GetTdc1();
    charge = hit->GetCharge();
	time   = hit->GetTime();
	pe     = hit->GetPe();
}

// __________________________________________________
T2KWCHit::~T2KWCHit()
{
}

// __________________________________________________
void T2KWCHit::Clear(Option_t* option)
{
    ch     = -1;
    pmt    = -1;
    window = -1;
    type   = -1;
    tag    = -1;
    qdc0   = -1;
    tdc0   = -1;
    qdc1   = -1;
    tdc1   = -1;    
    charge = -1;
    time   = -1;
    pe     = -1;
    gain   = -1;
    mask   = -1;
    hv     = -1;
}

// __________________________________________________
void T2KWCHit::Print(Option_t* option) const
{
    Double_t qdc = qdc1 - qdc0;
    Double_t tdc = tdc1 - tdc0;

    TString opt = option;
    opt.ToLower();

    if (opt == 'i') {
        printf("\n\t----------------------- HIT INFORMATION ------------------------------------------------------------\n");
        printf("\t%8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s\n",
               "ATMCH", "TYPE", "QDC", "(QDC1-QDC0)", "TDC", "(TDC1-TDC0)",
               "GAIN", "CHARGE", "P.E.", "Time", "mask");
        printf("\t----------------------------------------------------------------------------------------------------\n");
    }
    printf("\t%8d %8d %7.1f (%4.f-%4.1f) %7.1f (%4.f-%4.1f) %7.2e %7.2f %7.2f\t%7.2f\t%8d\n",
           ch, type, qdc, qdc1, qdc0, tdc, tdc1, tdc0, gain, charge, pe, time, mask);
}

//
ClassImp(T2KWCHit)
