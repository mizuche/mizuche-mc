#ifndef T2KWCSIMPARTICLE_H
#define T2KWCSIMPARTICLE_H

#include <iostream>
#include "TObject.h"
#include "TVector3.h"

//
class T2KWCSimParticle : public TObject
{
private:

  Int_t pid;				// particle data code (according to pdg code)
  Double_t absmom;  // initial absolute momentum [MeV/c]
  TVector3 dir;     // initial direction
  TVector3 ipos;    // initial position [cm]
  Int_t iflag;      // initial position flag
                    // out Tank:0, out FV & in Tank:1, in FV :2
  TVector3 fpos;    // initial position [cm]
  Int_t fflag;      // initial position flag
                    // out Tank:0, out FV & in Tank:1, in FV :2

public:

	T2KWCSimParticle();
	T2KWCSimParticle(T2KWCSimParticle *simpart);
	virtual ~T2KWCSimParticle();

	void Clear(Option_t *option="");
	void Print(Option_t *option="") const;

public:

	Int_t PID() 				{ return pid; }
	Double_t Absmom()	  { return absmom; }
  TVector3 Dir()      { return dir; }
  TVector3 Ipos()     { return ipos; }
  Int_t Iflag()       { return iflag; }
  TVector3 Fpos()     { return fpos; }
  Int_t Fflag()       { return fflag; }

	void SetPID(int p)                         { pid = p; }
	void SetAbsmom(double m)                   { absmom = m; }
  void SetDir(double x, double y, double z)  { dir = TVector3(x,y,z); }
  void SetIpos(double x, double y, double z) { ipos = TVector3(x,y,z); }
  void SetIflag(int i)                       { iflag = i; }
  void SetFpos(double x, double y, double z) { fpos = TVector3(x,y,z); }
  void SetFflag(int i)                       { fflag = i; }

	//
	ClassDef(T2KWCSimParticle,5)

};
	
#endif
