#include "T2KWCNeutInfo.h"

//static AncestorTemp ances_dummy;

//
T2KWCNeutInfo::T2KWCNeutInfo()
{
	energy = -1.;
  mode = 0;
  norm = -1.;
  totcrsne = -1.;
  dir = TVector3(0,0,0);

  ng = 0;
  gpid.clear();
  gmec.clear();
  gcosbm.clear();

  gposx.clear();
  gposy.clear();
  gposz.clear();
  gmomx.clear();
  gmomy.clear();
  gmomz.clear();
}

//
T2KWCNeutInfo::T2KWCNeutInfo(T2KWCNeutInfo *neut)
{
	energy = neut->energy;
  mode = neut->mode;
	norm = neut->norm;
	totcrsne = neut->totcrsne;
  dir = neut->dir;

  ng = neut->ng;
  for(int i=0;i<ng;i++) {
    gpid.push_back( neut->GPid(i) );
    gmec.push_back( neut->GMec(i) );
    gcosbm.push_back( neut->GCosbm(i) );

    gposx.push_back( neut->gposx[i] );
    gposy.push_back( neut->gposy[i] );
    gposz.push_back( neut->gposz[i] );
    gmomx.push_back( neut->gmomx[i] );
    gmomy.push_back( neut->gmomy[i] );
    gmomz.push_back( neut->gmomz[i] );
  }
}

T2KWCNeutInfo::~T2KWCNeutInfo()
{
}

//
void T2KWCNeutInfo::Clear(Option_t *option)
{
  energy = -1.;
  mode = 0;
  norm = -1.;
  totcrsne = -1.;
  dir = TVector3(0,0,0);

  ng = 0;
  gpid.clear();
  gmec.clear();
  gcosbm.clear();

  gposx.clear();
  gposy.clear();
  gposz.clear();
  gmomx.clear();
  gmomy.clear();
  gmomz.clear();
}

//
void T2KWCNeutInfo::Print(Option_t *option) const
{
	std::cout << "--- Neut info ---" << std::endl;
	std::cout	<< "Energy: " << energy << std::endl;
  std::cout << "Interaction Mode: " << mode << std::endl;
	std::cout << "Norm: " << norm << std::endl;
	std::cout << "Total cross-section: " << totcrsne << std::endl;
	std::cout	<< std::endl;
}

//
ClassImp(T2KWCNeutInfo)
