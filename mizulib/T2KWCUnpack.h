// -*- mode : c++ -*-

#ifndef T2KWCUNPACK_H
#define T2KWCUNPACK_H

#include <iostream>
#include <vector>

#include "TObject.h"
#include "TClonesArray.h"
#include "TRef.h"
#include "TH1.h"

#include "T2KWCHit.h"

#define MAXSAMPLE 10000
#define ENABLE_BSD

// __________________________________________________
class T2KWCUnpack : public TObject
{
private:
    Int_t            runnum;
    Int_t            spillnum;
    Int_t            runmode;
    Int_t            time_sec;
    Int_t            time_usec;
    Int_t            fvstat;        // Status of FV (1:Filled, 0:Not Filled)
    Int_t            daqmode;       // Status of DAQ status (Beam, Noise, Cosmic, etc...)
    Int_t            upkrun;        // Run number for upk and dst

    // Variables for ATM
    Int_t            atm_trg;
    Int_t            atm_gong;
    Int_t            atm_ntrg;
    Int_t            atm_nhits;    // Total # of hits
    Double_t         atm_ncharges; 
    Double_t         atm_npes;     // total # of photo-electrons into PMTs
    Double_t         atm_max_charge;
    Double_t         atm_max_pe;
    TClonesArray*    hit;

    // Variables for FADC
    Int_t            fadc_nsamp;
    Double_t         raw_pmtsum0;   // [FADC count]; PMTSUM pedestal
    Double_t         raw_hitsum0;   // [FADC count]; HITSUM pedestal
    Double_t         fadc_pmtsum0;  // [mV]; Pedestal of HITSUM
    Double_t         fadc_hitsum0;  // [mV]; Pedestal of PMTSUM
    Int_t            fadc_ntrg;     // Number of total events in SINGLE SPILL
    Int_t            fadc_nhits;    // Number of total HITSUM hits in SINGLE SPILL
    Double_t         fadc_ncharges; // Number of total PMTSUM integrals in SINGLE SPILL
    Double_t         fadc_npes;     // ncharges / average gain / elementary charge
    Double_t         fadc_ncharges_bunch[9];
    Double_t         fadc_npes_bunch[9];
    Double_t         average_gain;  // average gain

#ifdef ENABLE_BSD
    // Variables for BSD
    Int_t    bsd_nurun;           //
    Int_t    bsd_midas_event;     //
    Int_t    bsd_mrrun;           //
    Int_t    bsd_mrshot;          //
    Int_t    bsd_spillnum;        //
    Int_t    bsd_sec[3];          //
    Int_t    bsd_nano[3];         //
    Int_t    bsd_gpsstat[2];      //
    Double_t bsd_ct_np[5][9];     //
    Double_t bsd_beam_time[5][9]; //
    Int_t    bsd_beam_flag[5][9]; //
    Double_t bsd_hct[3][5];       //
    Double_t bsd_tpos[2];         //
    Double_t bsd_tdir[2];         //
    Double_t bsd_tsize[2];        //
    Double_t bsd_mumon[12];       //
    Double_t bsd_otr[13];         //
    Int_t    good_gps_flag;       //
    Int_t    trigger_flag;        //
    Int_t    spill_flag;          //
    Int_t    good_spill_flag;     //
    Double_t bsd_target_eff[3];   //
    Int_t    bsd_run_type;        //
    Int_t    bsd_magset_id;       //
#endif

    // std::vector for full unpacking
    std::vector<Int_t>    atm_ntrgnum;   //  
    std::vector<Int_t>    raw_pmtsum;    //!  [FADC count]; PMTSUM data points
    std::vector<Int_t>    raw_hitsum;    //!  [FADC count]; HITSUM data points
    std::vector<Double_t> fadc_pmtsum;   //  [mV]
    std::vector<Double_t> fadc_hitsum;   //  [mV]
    std::vector<Int_t>    fadc_nsec;     //  [ns]; Timing of SINGLE EVENT (= HITSUM's leading edge)
    std::vector<Int_t>    fadc_hit;      //  [#] ; Number of HITSUM hits in SINGLE EVENT (= fadc_height/kOnePulseHeight, unit [#])
    std::vector<Double_t> fadc_height;   //  [mV]; Height of HITSUM pulse (after sub. pedestal) of SINGLE EVENT, unit [mV]
    std::vector<Double_t> fadc_charge;   //  [pC]; Charge of PMTSUM integral (in designated time window) of SINGLE EVENT, unit [mV*usec]
    std::vector<Double_t> fadc_pe;       //  [p.e.]; fadc_pe is calculated using average gain

public:
    T2KWCUnpack();
    T2KWCUnpack(const T2KWCUnpack& unpack);
    virtual ~T2KWCUnpack();
    
    void NewTClonesArrays();

    void Clear(Option_t* option="");
    void Print(Option_t* option="") const;
    
    // Getters and Setters for basic RUN information
    Int_t    GetRunNum()   const { return runnum; }
    Int_t    GetSpillNum() const { return spillnum; }
    Int_t    GetRunmode()  const { return runmode; }
    Int_t    GetTimeSec()  const { return time_sec; }
    Int_t    GetTimeUsec() const { return time_usec; }
    Double_t GetTime()     const { return time_sec + time_usec*1e-6; } 
    Int_t    GetFvstat()   const { return fvstat; }
    Int_t    GetDaqmode()  const { return daqmode; }
    Int_t    GetUpkRun()   const { return upkrun; }
    
    void SetRunNum(const Int_t &run)     { runnum = run; }
    void SetSpillNum(const Int_t &spill) { spillnum = spill; }
    void SetRunmode(const Int_t &mode)   { runmode = mode; }
    void SetTimeSec(const Int_t &sec)    { time_sec  = sec; }
    void SetTimeUsec(const Int_t &usec)  { time_usec = usec; }
    void SetTime(const Int_t &sec, const Int_t &usec)
        { SetTimeSec(sec); SetTimeUsec(usec); }
    void SetFvstat(const Int_t &stat)  { fvstat  = stat; }
    void SetDaqmode(const Int_t &mode) { daqmode = mode; }
    void SetUpkRun(const Int_t &run)   { upkrun  = run; }

    // Getters and Setters for ATM variables
    Int_t     GetAtmTrg()       const { return atm_trg; }
    Int_t     GetAtmGong()      const { return atm_gong; }
    Int_t     GetAtmNtrg()      const { return atm_ntrg; }
    Int_t     GetAtmNhits()     const { return atm_nhits; }
    Double_t  GetAtmNcharges()  const { return atm_ncharges; }
    Double_t  GetAtmNpes()      const { return atm_npes; }
    Double_t  GetAtmMaxCharge() const { return atm_max_charge; }
    Double_t  GetAtmMaxPe()     const { return atm_max_pe; }
    T2KWCHit* GetHit(Int_t)     const;
    
    void SetAtmTrg(const Int_t &trg)              { atm_trg = trg; }
    void SetAtmGong(const Int_t &gong)            { atm_gong = gong; }
    void SetAtmNtrg(const Int_t &ntrg)            { atm_ntrg = ntrg; }
    void SetAtmNhits(const Int_t &nhits)          { atm_nhits = nhits; }
    void SetAtmNcharges(const Double_t &ncharges) { atm_ncharges = ncharges; }
    void SetAtmNpes(const Double_t &npes)         { atm_npes = npes; }
    void SetAtmMaxCharge(const Double_t &charge)  { atm_max_charge = charge; }
    void SetAtmMaxPe(const Double_t &pe)          { atm_max_pe = pe; }
    T2KWCHit* AddHit(T2KWCHit*);

    // Setters for FADC variables
    Int_t     GetFadcNsamp()    const { return fadc_nsamp; }
    Double_t  GetRawPmtsum0()   const { return raw_pmtsum0; }
    Double_t  GetRawHitsum0()   const { return raw_hitsum0; }
    Double_t  GetFadcPmtsum0()  const { return fadc_pmtsum0; }
    Double_t  GetFadcHitsum0()  const { return fadc_hitsum0; }
    Int_t     GetFadcNtrg()     const { return fadc_ntrg; }
    Int_t     GetFadcNhits()    const { return fadc_nhits; }
    Double_t  GetFadcNcharges() const { return fadc_ncharges; }
    Double_t  GetFadcNpes()     const { return fadc_npes; }
    Double_t  GetFadcNchargesBunch(Int_t &i) const
        {
            if (i > 8) {
                fprintf(stderr, "Error:\tIndex [%d] out of range.\n", i);
                exit(-1);
            } else return fadc_ncharges_bunch[i];
        }
    Double_t GetFadcNpesBunch(Int_t &i) const
        {
            if (i > 8) {
                fprintf(stderr, "Error:\tIndex [%d] out of range.\n", i);
                exit(-1);
            } else  return fadc_npes_bunch[i];
        }
    Double_t  GetAverageGain()  const { return average_gain; }

    void SetFadcNsamp(const Int_t &nsamp)          { fadc_nsamp   = nsamp; }
    void SetRawPmtsum0(const Double_t &pmtsum0)    { raw_pmtsum0  = pmtsum0; }
    void SetRawHitsum0(const Double_t &hitsum0)    { raw_hitsum0  = hitsum0; }
    void SetFadcPmtsum0(const Double_t &pmtsum0)   { fadc_pmtsum0 = pmtsum0;}
    void SetFadcHitsum0(const Double_t &hitsum0)   { fadc_hitsum0 = hitsum0;}
    void SetFadcNtrg(const Int_t &ntrg)            { fadc_ntrg = ntrg; }
    void SetFadcNhits(const Int_t &nhits)          { fadc_nhits = nhits; }
    void SetFadcNcharges(const Double_t &ncharges) { fadc_ncharges = ncharges; }
    void SetFadcNpes(const Double_t &npes)         { fadc_npes = npes; }
    void SetFadcNchargesBunch(const Int_t &i, const Double_t &ncharges)
        { fadc_ncharges_bunch[i] = ncharges; }
    void SetFadcNpesBunch(const Int_t &i, const Double_t &npes)
        { fadc_npes_bunch[i] = npes; }
    void SetAverageGain(const Double_t &gain)      { average_gain = gain; }

#ifdef ENABLE_BSD
    // Setters and Getters for BSD
    Int_t    GetBsdRun()                     const { return bsd_nurun; }
    Int_t    GetBsdMidasEvent()              const { return bsd_midas_event; }
    Int_t    GetBsdMrRun()                   const { return bsd_mrrun; }
    Int_t    GetBsdMrShot()                  const { return bsd_mrshot; }
    Int_t    GetBsdSpillNum()                const { return bsd_spillnum; }
    Int_t    GetBsdSec(const Int_t &i)       const { return bsd_sec[i]; }
    Int_t    GetBsdSec()                     const { return bsd_sec[0];}
    Int_t    GetBsdNano(const Int_t &i)      const { return bsd_nano[i]; }
    Int_t    GetBsdNano()                    const { return bsd_nano[0];}
    Int_t    GetBsdGpsStat(const Int_t &i)   const { return bsd_gpsstat[i]; }
    Double_t GetBsdCt5()                     const { return bsd_ct_np[4][0]; }
    Double_t GetBsdCt(const Int_t &ict, const Int_t &ibunch)
                                             const { return bsd_ct_np[ict][ibunch]; }
    Double_t GetBsdBeamTime(const Int_t &ict, const Int_t &ibunch)
                                             const { return bsd_beam_time[ict][ibunch]; }
    Int_t    GetBsdBeamFlag(const Int_t &ict, const Int_t &ibunch)
                                             const { return bsd_beam_flag[ict][ibunch]; }
    Double_t GetBsdHct(const Int_t &ihorn, const Int_t &ibass)
                                             const { return bsd_hct[ihorn][ibass]; }
    Double_t GetBsdTpos(const Int_t &i)      const { return bsd_tpos[i]; }
    Double_t GetBsdTdir(const Int_t &i)      const { return bsd_tdir[i]; }
    Double_t GetBsdTsize(const Int_t &i)     const { return bsd_tsize[i]; }
    Double_t GetBsdMumon(const Int_t &i)     const { return bsd_mumon[i]; }
    Double_t GetBsdOtr(const Int_t &i)       const { return bsd_otr[i]; }
    Int_t    GetGoodGpsFlag()                const { return good_gps_flag; }
    Int_t    GetTriggerFlag()                const { return trigger_flag; }
    Int_t    GetSpillFlag()                  const { return spill_flag; }
    Int_t    GetGoodSpillFlag()              const { return good_spill_flag; }
    Double_t GetBsdTargetEff(const Int_t &i) const { return bsd_target_eff[i]; }
    Int_t    GetBsdRunType()                 const { return bsd_run_type; }
    Int_t    GetBsdMagsetId()                const { return bsd_magset_id; }
    
    void SetBsdRun(const Int_t &run)                      { bsd_nurun = run; }
    void SetBsdMidasEvent(const Int_t &event)             { bsd_midas_event = event; }
    void SetBsdMrRun(const Int_t &run)                    { bsd_mrrun = run; }
    void SetBsdMrShot(const Int_t &shot)                  { bsd_mrshot = shot; }
    void SetBsdSpillNum(const Int_t &spill)               { bsd_spillnum = spill; }
    void SetBsdSec(const Int_t &i, const Int_t &sec)      { bsd_sec[i] = sec; }
    void SetBsdNano(const Int_t &i, const Int_t &nano)    { bsd_nano[i] = nano; }
    void SetBsdGpsStat(const Int_t &i, const Int_t &stat) { bsd_gpsstat[i] = stat; }
    void SetBsdCt(const Int_t &ict, const Int_t &ibunch, const Double_t &nprotons)    { bsd_ct_np[ict][ibunch] = nprotons; }
    void SetBsdBeamTime(const Int_t &ict, const Int_t &ibunch, const Double_t &time)  { bsd_beam_time[ict][ibunch] = time; }
    void SetBsdBeamFlag(const Int_t &ict, const Int_t &ibunch, const Int_t &flag)     { bsd_beam_flag[ict][ibunch] = flag; }
    void SetBsdHct(const Int_t &ihorn, const Int_t &ibass, const Double_t &current)   { bsd_hct[ihorn][ibass] = current; }
    void SetBsdTpos(const Int_t &i, const Double_t &pos)      { bsd_tpos[i] = pos; }
    void SetBsdTdir(const Int_t &i, const Double_t &dir)      { bsd_tdir[i] = dir; }
    void SetBsdTsize(const Int_t &i, const Double_t &size)    { bsd_tsize[i] = size; }
    void SetBsdMumon(const Int_t &i, const Double_t &mumon)   { bsd_mumon[i] = mumon; }
    void SetBsdOtr(const Int_t &i, const Double_t &otr)       { bsd_otr[i] = otr; }
    void SetGoodGpsFlag(const Int_t &flag)                    { good_gps_flag = flag; }
    void SetTriggerFlag(const Int_t &flag)                    { trigger_flag = flag; }
    void SetSpillFlag(const Int_t &flag)                      { spill_flag = flag; }
    void SetGoodSpillFlag(const Int_t &flag)                  { good_spill_flag = flag; }
    void SetBsdTargetEff(const Int_t &i, const Double_t &eff) { bsd_target_eff[i] = eff; }
    void SetBsdRunType(const Int_t &type)                     { bsd_run_type = type; }
    void SetBsdMagsetId(const Int_t &id)                      { bsd_magset_id = id; }

#endif
    // Setters for variables for FULL unpacking
    Int_t    GetAtmNtrgnum(const Int_t &i) const { return atm_ntrgnum.at(i); }
    Int_t    GetRawPmtsum(const Int_t &i)  const { return raw_pmtsum.at(i); }
    Int_t    GetRawHitsum(const Int_t &i)  const { return raw_hitsum.at(i); }
    Double_t GetFadcPmtsum(const Int_t &i) const { return fadc_pmtsum.at(i); }
    Double_t GetFadcHitsum(const Int_t &i) const { return fadc_hitsum.at(i); }
    Int_t    GetFadcNsec(const Int_t &i)   const { return fadc_nsec.at(i); }
    Double_t GetFadcHeight(const Int_t &i) const { return fadc_height.at(i); }
    Int_t    GetFadcHit(const Int_t &i)    const { return fadc_hit.at(i); }
    Double_t GetFadcCharge(const Int_t &i) const { return fadc_charge.at(i); }
    Double_t GetFadcPe(const Int_t &i)     const { return fadc_pe.at(i); }
    
    void SetAtmNtrgnum(const Int_t &itrgnum)    { atm_ntrgnum.push_back(itrgnum); }
    void SetRawPmtsum(const Int_t &ipmtsum)     { raw_pmtsum.push_back(ipmtsum); }
    void SetRawHitsum(const Int_t &ihitsum)     { raw_hitsum.push_back(ihitsum); }
    void SetFadcPmtsum(const Double_t &ipmtsum) { fadc_pmtsum.push_back(ipmtsum); }
    void SetFadcHitsum(const Double_t &ihitsum) { fadc_hitsum.push_back(ihitsum); }
    void SetFadcNsec(const Int_t &insec)        { fadc_nsec.push_back(insec); }
    void SetFadcHeight(const Double_t &iheight) { fadc_height.push_back(iheight); }
    void SetFadcHit(const Int_t &ihit)       { fadc_hit.push_back(ihit); }
    void SetFadcCharge(const Double_t &icharge) { fadc_charge.push_back(icharge); }
    void SetFadcPe(const Double_t &ipe)         { fadc_pe.push_back(ipe); }

    // // Automatic setters
    // void SetFadcNrtg() { fadc_ntrg = (Int_t)fadc_height.size(); }
    // void SetFadcNhits() {
    //     fadc_nhits = 0;
    //     Int_t size = (Int_t)fadc_hit.size();
    //     for (Int_t i = 0; i < size; i++) {
    //         fadc_nhits += GetFadcHit(i);
    //     }
    // }
    
    // void SetFadcNCharges() {
    //     fadc_ncharges = 0;
    //     Int_t size = (Int_t)fadc_charge.size();
    //     for (Int_t i = 0; i < size; i++) {
    //         fadc_ncharges += GetFadcCharge(i);
    //     }
    // }
    
    // void SetFadcNpes() {
    //     fadc_npes = 0;
    //     Int_t size = (Int_t)fadc_pe.size();
    //     for (Int_t i = 0; i < size; i++) {
    //         fadc_npes += GetFadcPe(i);
    //     }
    // }
    //
    ClassDef(T2KWCUnpack, 7);
};

#endif 

