#include "T2KWCEventAction.hh"
#include "T2KWCGeometry.hh"
#include "T2KWCOpticalHit.hh"
#include "T2KWCRunAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4GeneralParticleSource.hh"
#include "G4HCofThisEvent.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4VHitsCollection.hh"

#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4Square.hh"
#include "G4Trajectory.hh"
#include "G4TrajectoryContainer.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

#ifdef G4ANALYSIS_USE
#include "T2KWCAnalysisManager.hh"
#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCEventAction::T2KWCEventAction(GeneralCard *gcard0)
    : OpticalHitsCollID(-1)
{
  pmthit = new T2KWCPMTHit();
  gcard = gcard0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCEventAction::~T2KWCEventAction()
{
  if (pmthit)
    delete pmthit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCEventAction::BeginOfEventAction(const G4Event *evt)
{
  G4int printModulo = 1;

  evtNb = evt->GetEventID();

  G4SDManager *SDman = G4SDManager::GetSDMpointer();

  if (OpticalHitsCollID == -1)
  {
    OpticalHitsCollID = SDman->GetCollectionID("OpticalHitsCollection");
  }

  if (evtNb % printModulo == 0)
    G4cerr << "\n---> Begin of Event: " << evtNb << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCEventAction::EndOfEventAction(const G4Event *evt)
{
  //
  G4int nOptHits = 0;
  G4int nHitPmts = 0;
  G4HCofThisEvent *HCE = evt->GetHCofThisEvent();
  T2KWCOpticalHitsCollection *OpticalHitsColl = 0;

  //
  G4RunManager *runMgr = G4RunManager::GetRunManager();
  T2KWCRunAction *runAction = (T2KWCRunAction *)runMgr->GetUserRunAction();

  if ((runAction->GetEventWriteFlag()) < 0)
  {
    G4cout << "Eventwriteflag : " << runAction->GetEventWriteFlag() << G4endl;
    G4cout << " Abort Run process, skip this end of event action" << G4endl;
    goto ENDOFEVENT;
  }

  // Print vertex info
  if (gcard->dspvtx == 1)
    runAction->PrintAll();

  //
  if (HCE)
  {
    if (OpticalHitsCollID != -1)
      OpticalHitsColl =
          (T2KWCOpticalHitsCollection *)(HCE->GetHC(OpticalHitsCollID));
  }

  if (OpticalHitsColl)
  {

    nOptHits = OpticalHitsColl->entries();

    T2KWCOpticalHit *aHit = 0;

    if (gcard->dsphit == 1)
      G4cout << "\n### Print hit infomation in this Event ###" << G4endl;

    for (G4int iHit = 0; iHit < nOptHits; iHit++)
    {

      aHit = (*OpticalHitsColl)[iHit];

#ifdef G4ANALYSIS_USE
      G4double HitEnergy;
      HitEnergy = aHit->GetEnergy();
      G4ThreeVector HitPos = aHit->GetPosition() / cm;

      T2KWCAnalysisManager *analysis = T2KWCAnalysisManager::getInstance();
      analysis->FillHistogram(1, HitEnergy / eV);
#endif

      if (aHit->NHits() > 1)
        nHitPmts++;

      // Print Hit info
      if (gcard->dsphit == 1)
        aHit->Print();

      // Add Hit info to Tree
      pmthit->Clear("C");
      pmthit->SetID(aHit->PMTID());
      pmthit->SetNPE(aHit->NHits());
      pmthit->SetTime(aHit->HitTime());

      runAction->GetEventClass()->AddPMTHit(pmthit);
    }
  }

  // Print Hit summary
  if (gcard->dsphit == 1)
  {

    G4cout << "Total generated # of photon : " << runAction->GetEventClass()->TotNPhotons() << G4endl;
    G4cout << "Total generated # of p.e. : " << runAction->GetEventClass()->TotNPEs() << G4endl;
    G4cout << "Total measured # of p.e. : " << runAction->GetEventClass()->TotNPEs_PMT() << G4endl;
    G4cout << "# of Hit PMTs (p.e.>1) " << nHitPmts << G4endl;
    G4cout << "Acceptance(%): "
           << 100. * (G4double)(runAction->GetEventClass()->TotNPEs_PMT()) / (G4double)(runAction->GetEventClass()->TotNPEs())
           << G4endl;
  }

#ifdef G4ANALYSIS_USE
  T2KWCAnalysisManager *analysis = T2KWCAnalysisManager::getInstance();
  analysis->FillHistogram(2, nOptHits);
#endif

  // Define visualization atributes:
  if (G4VVisManager::GetConcreteInstance())
    DefineVis(evt);

  // Fill tree & refresh
  G4cout << "Fill Tree" << G4endl;
  runAction->FillTree();
  G4cout << "Clear Tree" << G4endl;
  runAction->ClearEvt();

// ####################################
// ###### End of Event process #######
// ####################################
ENDOFEVENT:

  G4cout << "End Event" << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

//
void T2KWCEventAction::DefineVis(const G4Event *evt)
{
  // G4String drawFlag = "all";
  G4String drawFlag = "primary";
  // G4String drawFlag = "not_opticalphoton";

  G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
  G4TrajectoryContainer *trajectoryContainer = evt->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if (trajectoryContainer)
  {
    n_trajectories = trajectoryContainer->entries();

    for (G4int i = 0; i < n_trajectories; i++)
    {
      G4Trajectory *trj = (G4Trajectory *)((*(evt->GetTrajectoryContainer()))[i]);

      if (drawFlag == "all")
        trj->DrawTrajectory();

      else if (drawFlag == "not_opticalphoton" &&
               trj->GetParticleName() != "opticalphoton")
      {
        trj->DrawTrajectory();
      }

      else if ((drawFlag == "primary") &&
               (trj->GetParentID() == 0))
      {
        trj->DrawTrajectory();
      }
    }
  }

  // about vertex
  if (evt->GetPrimaryVertex())
  {
    G4Square square(G4ThreeVector(evt->GetPrimaryVertex()->GetX0(),
                                  evt->GetPrimaryVertex()->GetY0(),
                                  evt->GetPrimaryVertex()->GetZ0()));

    square.SetScreenSize(12.);
    G4Colour colour(1., 0.8, 0.);
    G4VisAttributes attribs(colour);
    square.SetVisAttributes(attribs);
    pVVisManager->Draw(square);
  }
}
