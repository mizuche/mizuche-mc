#include "T2KWCDetectorConstruction.hh"
#include "T2KWCGeometry.hh"
#include "T2KWCPMTSD.hh"
// #include "T2KWCPMTParameterisation.hh"

#include "G4Box.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4OpticalSurface.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4Tubs.hh"
#include "G4VisAttributes.hh"

// #define PMTPOS

///////////////////////////////////////////////

const G4int nEntries = 35;

G4double PhotonEnergy[nEntries] =
    {2.034 * eV, 2.068 * eV, 2.103 * eV, 2.139 * eV,
     2.177 * eV, 2.216 * eV, 2.256 * eV, 2.298 * eV,
     2.341 * eV, 2.386 * eV, 2.433 * eV, 2.481 * eV,
     2.532 * eV, 2.585 * eV, 2.640 * eV, 2.697 * eV,
     2.757 * eV, 2.820 * eV, 2.885 * eV, 2.954 * eV,
     3.026 * eV, 3.102 * eV, 3.181 * eV, 3.265 * eV,
     3.353 * eV, 3.446 * eV, 3.545 * eV, 3.649 * eV,
     3.760 * eV, 3.877 * eV, 4.002 * eV, 4.136 * eV,
     4.275 * eV, 4.427 * eV, 4.591 * eV};

//////////////////////////////////////////////

//
T2KWCDetectorConstruction::T2KWCDetectorConstruction(T2KWCInputCard *aInputCard)
    : PMTParam(0), physiPMT(0), PMTSD(0), iPMT(0)
{
  gcard = aInputCard->GetGeneralCard();

  // Sensitive Detector Manager
  SDmanager = G4SDManager::GetSDMpointer();
}

//
T2KWCDetectorConstruction::~T2KWCDetectorConstruction()
{
  if (PMTParam)
    delete PMTParam;
}

//
G4VPhysicalVolume *T2KWCDetectorConstruction::Construct()
{

  //	------------- Materials -------------
  DefineMaterial();

  // ------------ Generate & Add Material Properties Table ------------
  SetWaterProperties();
  SetAcrylicProperties();

  // for FV without water
  SetAir2Properties();

  // for mini mizuche
  SetGlassProperties();

  //	------------- Volumes --------------

  // The experimental Hall
  G4Box *expHall_box = new G4Box("World", expHall_x, expHall_y, expHall_z);

  G4LogicalVolume *expHall_log = new G4LogicalVolume(expHall_box, Air, "World", 0, 0, 0);

  expHall_log->SetVisAttributes(G4VisAttributes::Invisible);

  G4VPhysicalVolume *expHall_phys = new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), expHall_log, "World", 0, false, 0);

  // The Tank
  G4LogicalVolume *waterTank_log;
  switch (gcard->detector)
  {

  case 0:
    waterTank_log = SetTank(expHall_log);
    SetPMT(waterTank_log);
    break;

  case 1:
    waterTank_log = SetTank_MINI(expHall_log);
    SetPMT_MINI(expHall_log);
    break;

  default:
    G4cerr << "Invalid Detector parameta (DETECTOR) !!!" << G4endl;
    exit(-1);
  }

  // -----  always return the physical World -----
  return expHall_phys;
}

//
void T2KWCDetectorConstruction::DefineMaterial()
{

  G4double a, z, density;
  G4int nelements;

  // Element
  G4Element *elN = new G4Element("Nitrogen", "N", z = 7., a = 14.01 * g / mole);
  G4Element *elO = new G4Element("Oxygen", "O", z = 8., a = 16.00 * g / mole);
  G4Element *elH = new G4Element("Hydrogen", "H", z = 1., a = 1.01 * g / mole);
  G4Element *elC = new G4Element("Carbon", "C", z = 6., a = 12.01 * g / mole);
  G4Element *elB = new G4Element("Boron", "B", z = 5., a = 10.81 * g / mole);
  G4Element *elNa = new G4Element("Sodium", "Na", z = 11., a = 22.99 * g / mole);
  G4Element *elAl = new G4Element("Aluminum", "Al", z = 13., a = 26.98 * g / mole);
  G4Element *elSi = new G4Element("Silicon", "Si", z = 14., a = 28.09 * g / mole);

  // Air
  //
  Air = new G4Material("Air", density = 1.29 * mg / cm3, nelements = 2);
  Air->AddElement(elN, 70. * perCent);
  Air->AddElement(elO, 30. * perCent);

  // Air2
  //
  Air2 = new G4Material("Air2", density = 1.29 * mg / cm3, nelements = 2);
  Air2->AddElement(elN, 70. * perCent);
  Air2->AddElement(elO, 30. * perCent);

  // Water
  //

  Water = new G4Material("Water", density = 1.0 * g / cm3, nelements = 2);
  Water->AddElement(elH, 2);
  Water->AddElement(elO, 1);

  // PMMA C5H8O2 ( Acrylic )
  // -------------

  Acrylic = new G4Material("Acrylic", density = 1.19 * g / cm3, nelements = 3);
  Acrylic->AddElement(elC, 5);
  Acrylic->AddElement(elH, 8);
  Acrylic->AddElement(elO, 2);

  // Glass ( borosilicate glass )
  //
  SiO2 = new G4Material("SiO2", density = 2.20 * g / cm3, 2);
  SiO2->AddElement(elSi, 1);
  SiO2->AddElement(elO, 2);

  B2O3 = new G4Material("B2O3", density = 2.46 * g / cm3, 2);
  B2O3->AddElement(elB, 2);
  B2O3->AddElement(elO, 3);

  Na2O = new G4Material("NaO2", density = 2.27 * g / cm3, 2);
  Na2O->AddElement(elNa, 2);
  Na2O->AddElement(elO, 1);

  Al2O3 = new G4Material("Al2O3", density = 4.00 * g / cm3, 2);
  Al2O3->AddElement(elAl, 2);
  Al2O3->AddElement(elO, 3);

  Glass = new G4Material("Glass", density = 2.23 * g / cm3, 4);
  Glass->AddMaterial(SiO2, 80.6 * perCent);
  Glass->AddMaterial(B2O3, 13.0 * perCent);
  Glass->AddMaterial(Na2O, 4.0 * perCent);
  Glass->AddMaterial(Al2O3, 2.4 * perCent);
}

//
void T2KWCDetectorConstruction::SetWaterProperties()
{
  G4double RefractiveIndex1[nEntries] =
      {1.3435, 1.344, 1.3445, 1.345, 1.3455,
       1.346, 1.3465, 1.347, 1.3475, 1.348,
       1.3485, 1.3492, 1.35, 1.3505, 1.351,
       1.3518, 1.3522, 1.3530, 1.3535, 1.354,
       1.3545, 1.355, 1.3555, 1.356, 1.3568,
       1.3572, 1.358, 1.3585, 1.359, 1.3595,
       1.36, 1.3608, 1.3608, 1.3608, 1.3608};

  G4double Absorption1[nEntries] =
      {3.448 * m, 4.082 * m, 6.329 * m, 9.174 * m, 12.346 * m, 13.889 * m,
       15.152 * m, 17.241 * m, 18.868 * m, 20.000 * m, 26.316 * m, 35.714 * m,
       45.455 * m, 47.619 * m, 52.632 * m, 52.632 * m, 55.556 * m, 52.632 * m,
       52.632 * m, 47.619 * m, 45.455 * m, 41.667 * m, 37.037 * m, 33.333 * m,
       30.000 * m, 28.500 * m, 27.000 * m, 24.500 * m, 22.000 * m, 19.500 * m,
       17.500 * m, 14.500 * m, 14.500 * m, 14.500 * m, 14.500 * m};
  // above 4.275 eV, I could not find data.
  // So just put 14.5m, which would be larger than actual value.
  //    A.K.Ichikawa

  G4MaterialPropertiesTable *myMPT1 = new G4MaterialPropertiesTable();
  myMPT1->AddProperty("RINDEX", PhotonEnergy, RefractiveIndex1, nEntries);
  myMPT1->AddProperty("ABSLENGTH", PhotonEnergy, Absorption1, nEntries);

  Water->SetMaterialPropertiesTable(myMPT1);
}

//
void T2KWCDetectorConstruction::SetAcrylicProperties()
{
  // use the length from 260nm to 400nm (14 collums)
  // At other ragion, the distribution is flat (or too large).
  // -> assumed to flat
  const G4int num = 17;

  G4double wave_photon[num] = // [nm]
      {200.,
       260., 270., 280., 290., 300.,
       310., 320., 330., 340., 350.,
       360., 370., 380., 390., 400.,
       620.};

  // Convert from wave length [nm] to energy [eV]
  G4double Ephoton[num];
  for (int i = 0; i < num; i++)
  {
    Ephoton[i] = (1239.84 / wave_photon[i]) * eV;
  }

  // estimate by measurement of 5mm acrylic

  G4double ABSLENG_ACRYLIC[num] =
      {0.867 * mm,
       0.867 * mm, 1.46 * mm, 3.94 * mm, 1.10 * cm, 3.48 * cm,
       7.71 * cm, 9.81 * cm, 11.58 * cm, 13.6 * cm, 15.7 * cm,
       21.2 * cm, 39.4 * cm, 81.1 * cm, 3.86 * m, 9.27 * m,
       9.27 * m};
  /*
  G4double ABSLENG_ACRYLIC[num] =
  { 867*mm,
    867*mm, 1460*mm, 3940*mm, 1100*cm, 3480*cm,
    77100*cm, 98100*cm, 115800*cm, 13600*cm, 15700*cm,
    21200*cm, 39400*cm, 81100*cm, 3860*m, 9270*m,
    9270*m};
  */

  // estimate by measurement of 8mm acrylic
  /*
    G4double ABSLENG_ACRYLIC[num] =
    { 1.31*mm, 1.33*mm, 3.09*mm, 1.03*cm, 2.60*cm,
      6.45*cm, 10.6cm, 15.4*cm, 25.9*cm, 39.7*cm,
      55.0*cm, 88.1*cm, 12.0*m, 11.8*m, 17.7*m };
  */

  G4double RINDEX_ACRYLIC[num];
  for (int i = 0; i < num; i++)
  {
    RINDEX_ACRYLIC[i] = 1.49;
  }

  G4MaterialPropertiesTable *MPT_Acrylic = new G4MaterialPropertiesTable();
  MPT_Acrylic->AddProperty("RINDEX", Ephoton, RINDEX_ACRYLIC, num);
  MPT_Acrylic->AddProperty("ABSLENGTH", Ephoton, ABSLENG_ACRYLIC, num);

  /*
    G4double RINDEX_ACRYLIC[nEntries];
    G4double ABSLENG_ACRYLIC[nEntries];
    for(int i=0;i<nEntries;i++) {
      RINDEX_ACRYLIC[i] = 1.49;
      ABSLENG_ACRYLIC[i] = 3*m;
    }

    G4MaterialPropertiesTable *MPT_Acrylic = new G4MaterialPropertiesTable();
    MPT_Acrylic->AddProperty("RINDEX", PhotonEnergy, RINDEX_ACRYLIC, nEntries);
    MPT_Acrylic->AddProperty("ABSLENGTH", PhotonEnergy, ABSLENG_ACRYLIC, nEntries);
  */

  Acrylic->SetMaterialPropertiesTable(MPT_Acrylic);

  /*
    const G4int    N_RINDEX_ACRYLIC = 3 ;
    G4double X_RINDEX_ACRYLIC[N_RINDEX_ACRYLIC] = {320.0, 400.0, 500.0};
    // Wavelength in nanometers
    G4double RINDEX_ACRYLIC[N_RINDEX_ACRYLIC] = {1.526, 1.507, 1.497};

    // Convert from nm to GeV

    for(G4int i=0;i<N_RINDEX_ACRYLIC; i++){
      X_RINDEX_ACRYLIC[i] = ((1239.84/X_RINDEX_ACRYLIC[i])*1E-9)*GeV;
    }

    G4MaterialPropertiesTable *MPT_Acrylic = new G4MaterialPropertiesTable();
    MPT_Acrylic->AddProperty("RINDEX", X_RINDEX_ACRYLIC, RINDEX_ACRYLIC, N_RINDEX_ACRYLIC);
    Acrylic->SetMaterialPropertiesTable(MPT_Acrylic);
  */
}

//
void T2KWCDetectorConstruction::SetAir2Properties()
{
  G4double RINDEX_AIR2[nEntries];
  G4double ABSLENG_AIR2[nEntries];
  for (int i = 0; i < nEntries; i++)
  {
    RINDEX_AIR2[i] = 1.0003;
    ABSLENG_AIR2[i] = 1000 * m;
  }

  G4MaterialPropertiesTable *MPT_Air2 = new G4MaterialPropertiesTable();
  MPT_Air2->AddProperty("RINDEX", PhotonEnergy, RINDEX_AIR2, nEntries);
  MPT_Air2->AddProperty("ABSLENGTH", PhotonEnergy, ABSLENG_AIR2, nEntries);
  Air2->SetMaterialPropertiesTable(MPT_Air2);
}

//
void T2KWCDetectorConstruction::SetGlassProperties()
{
  G4double RINDEX_GLASS[nEntries];
  for (int i = 0; i < nEntries; i++)
  {
    RINDEX_GLASS[i] = 1.5;
  }

  G4MaterialPropertiesTable *MPT_Glass = new G4MaterialPropertiesTable();
  MPT_Glass->AddProperty("RINDEX", PhotonEnergy, RINDEX_GLASS, nEntries);
  Glass->SetMaterialPropertiesTable(MPT_Glass);
}

//
G4LogicalVolume *T2KWCDetectorConstruction::SetTank(G4LogicalVolume *mother_log)
{
  G4double fv_radius = fv_r;
  G4double fv_length = fv_z;
  G4ThreeVector fv_center;
  G4Material *fv_material = 0;

  // select inner material of FV
  if (gcard->fvwater == 0)
    fv_material = Air2;
  else if (gcard->fvwater == 1)
    fv_material = Water;

  // select fiducial volume size
  if (gcard->detector == 2)
  {
    fv_radius = fv_r;
    fv_length = half_fv_z;
    fv_center = G4ThreeVector(0., 0., -25. * cm);
  }
  else if (gcard->detector == 0)
  {
    fv_radius = fv_r;
    fv_length = fv_z;
    fv_center = G4ThreeVector(0., 0., 0.);
  }

  // The Water Tank
  G4Tubs *waterTank_tube = new G4Tubs("Tank", 0., tank_r, tank_z / 2., 0., 360.);
  G4LogicalVolume *waterTank_log = new G4LogicalVolume(waterTank_tube, Water, "Tank", 0, 0, 0);

  G4VisAttributes *waterTank_VisAtt = new G4VisAttributes(1, G4Color(0., 0.5, 1.));
  waterTank_VisAtt->SetForceWireframe(true);
  waterTank_log->SetVisAttributes(waterTank_VisAtt);

  SetTankSurface(waterTank_log);

  new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), waterTank_log, "Tank", mother_log, false, 0);

  // FV acrylic tube
  G4Tubs *acrylic_tube = new G4Tubs("AcrylicTube", 0., fv_radius + fvbound_r, fv_length / 2. + fvbound_z, 0., 360.);
  G4LogicalVolume *acrylic_log = new G4LogicalVolume(acrylic_tube, Acrylic, "AcrylicTube", 0, 0, 0);

  G4VisAttributes *acrylic_VisAtt = new G4VisAttributes(1, G4Color(1., 0., 0.));
  acrylic_VisAtt->SetForceWireframe(true);
  acrylic_log->SetVisAttributes(acrylic_VisAtt);

  SetAcrylicSurface(acrylic_log);

  new G4PVPlacement(0, fv_center, acrylic_log, "AcrylicTube", waterTank_log, false, 0);

  // FV in Water Tank
  G4Tubs *fv_tube = new G4Tubs("FV", 0., fv_radius, fv_length / 2., 0., 360.);
  G4LogicalVolume *fv_log = new G4LogicalVolume(fv_tube, fv_material, "FV", 0, 0, 0);

  G4VisAttributes *fv_VisAtt = new G4VisAttributes(1, G4Color(0., 1., 0.2));
  fv_VisAtt->SetForceWireframe(true);
  fv_log->SetVisAttributes(fv_VisAtt);

  SetFVSurface(fv_log);

  new G4PVPlacement(0, fv_center, fv_log, "FV", acrylic_log, false, 0);

  return waterTank_log;
}

//
G4LogicalVolume *T2KWCDetectorConstruction::SetTank_MINI(G4LogicalVolume *mother_log)
{
  // Water area
  G4Tubs *water_area = new G4Tubs("Water", 0., tank_mini_r, tank_mini_z, 0., 360.);
  G4LogicalVolume *water_log = new G4LogicalVolume(water_area, Water, "Tank", 0, 0, 0);
  //= new G4LogicalVolume(water_area,Air2,"Tank",0,0,0); // for acrylic measurement

  G4VisAttributes *water_VisAtt = new G4VisAttributes(G4Color(0., 0.6, 1.));
  water_VisAtt->SetForceSolid(true);
  water_log->SetVisAttributes(water_VisAtt);

  new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), water_log, "Water area", mother_log, false, 0);

  // Acrylic area
  G4Tubs *acrylic_area = new G4Tubs("Acry", 0., tank_mini_r, acry_mini_z, 0., 360.);

  G4LogicalVolume *acrylic_log = new G4LogicalVolume(acrylic_area, Acrylic, "Acry", 0, 0, 0);

  G4VisAttributes *acrylic_VisAtt = new G4VisAttributes(G4Color(0.5, 0., 0.));
  acrylic_VisAtt->SetForceSolid(true);
  acrylic_log->SetVisAttributes(acrylic_VisAtt);

  new G4PVPlacement(0, G4ThreeVector(0., 0., tank_mini_z + acry_mini_z),
                    acrylic_log, "Acry area", mother_log, false, 0);

// air gap in tube ( for acrylic measurement, etc)
#if 0
	G4Tubs* air_tube = new G4Tubs("air",0.,tank_mini_r,air_mini_z,0.,360.);

	G4LogicalVolume* air_log
		= new G4LogicalVolume(air_tube,Air2,"air",0,0,0);

	G4VisAttributes* air_VisAtt = new G4VisAttributes(G4Color(0.,0.2,0.8));
	air_VisAtt->SetForceSolid(true);
	air_log->SetVisAttributes(air_VisAtt);

  new G4PVPlacement(0,G4ThreeVector(0.,0.,tank_mini_z+acry_mini_z*2+air_mini_z),
                        air_log,"air",mother_log,false,0);
#endif

  //
  DefineSurface_MINI(water_log, acrylic_log);

  return water_log;
}

void T2KWCDetectorConstruction::SetAcrylicSurface(G4LogicalVolume *acrylic_log)
{
  // Simple Surface model
  G4OpticalSurface *OpAcrylicSurface = new G4OpticalSurface("AcrylicSurface");
  OpAcrylicSurface->SetType(dielectric_dielectric);
  OpAcrylicSurface->SetFinish(polished);
  OpAcrylicSurface->SetModel(glisur);

  new G4LogicalSkinSurface("AcrycliSurface", acrylic_log, OpAcrylicSurface);
}

void T2KWCDetectorConstruction::SetFVSurface(G4LogicalVolume *fv_log)
{
  // Simple Surface model
  G4OpticalSurface *OpFVSurface = new G4OpticalSurface("FVSurface");
  OpFVSurface->SetType(dielectric_dielectric);
  OpFVSurface->SetFinish(polished);
  OpFVSurface->SetModel(glisur);

  new G4LogicalSkinSurface("FVSurface", fv_log, OpFVSurface);
}

//
void T2KWCDetectorConstruction::SetTankSurface(G4LogicalVolume *waterTank_log)
{
  // Simple Surface model
  G4OpticalSurface *OpTankSurface = new G4OpticalSurface("TankSurface");
  OpTankSurface->SetType(dielectric_metal);
  OpTankSurface->SetFinish(polished);
  OpTankSurface->SetModel(glisur);

  new G4LogicalSkinSurface("TankSurface", waterTank_log, OpTankSurface);

  //
  G4double reflectivity[nEntries];
  G4double efficiency[nEntries];
  for (int i = 0; i < nEntries; i++)
  {
    reflectivity[i] = 0.;
    efficiency[i] = 0.;
  }
  // reflectivity : the ratio of reflection
  // efficiency : the ratio of absorption & energy deposit on material
  // At dielectric-metal border, first determine reflect or not with reflectivity,
  // then determine deposit or not energy on material with efficiency.
  // reflectivity = 0 means no reflection
  // efficiency = 0 means no energy deposite on material
  // (at mizuche MC, efficinecy setting is not so important)

  G4MaterialPropertiesTable *TankPT = new G4MaterialPropertiesTable();
  TankPT->AddProperty("REFLECTIVITY", PhotonEnergy, reflectivity, nEntries);
  TankPT->AddProperty("EFFICIENCY", PhotonEnergy, efficiency, nEntries);

  OpTankSurface->SetMaterialPropertiesTable(TankPT);
}

void T2KWCDetectorConstruction::DefineSurface_MINI(G4LogicalVolume *water_log, G4LogicalVolume *acrylic_log)
{
  G4OpticalSurface *OpWaterSurface = new G4OpticalSurface("WaterSurface");
  OpWaterSurface->SetType(dielectric_dielectric);
  OpWaterSurface->SetFinish(polished);
  OpWaterSurface->SetModel(glisur);

  G4OpticalSurface *OpAcrySurface = new G4OpticalSurface("AcrySurface");
  OpAcrySurface->SetType(dielectric_dielectric);
  OpAcrySurface->SetFinish(polished);
  OpAcrySurface->SetModel(glisur);

  new G4LogicalSkinSurface("WaterSurface", water_log, OpWaterSurface);
  new G4LogicalSkinSurface("AcrySurface", acrylic_log, OpAcrySurface);

  /*
    G4LogicalBorderSurface* WaterSurface =
      new G4LogicalBorderSurface("WaterSurface",
                                   acrylic_phys,waterTank_phys,OpWaterSurface);
  */

  //
  // Generate & Add Material Properties Table attached to the optical surfaces
  // (copy from N06 code)
  /*
    G4double specularLobe[nEntries];
    G4double specularSpike[nEntries];
    G4double backscatter[nEntries];
    G4double reflectivity[nEntries];
    G4double efficiency[nEntries];

    G4double refractiveIndexW[nEntries];
    G4double refractiveIndexA[nEntries];

    for(int i=0;i<nEntries;i++) {
      specularLobe[i] = 0.3;
      specularSpike[i] = 0.2;
      backscatter[i] = 0.1;
      reflectivity[i] = 0.2;
      efficiency[i] = 0.9;

  //    refractiveIndexW[i] = 1.33;
  //    refractiveIndexA[i] = 1.49;
    }

    G4MaterialPropertiesTable* acrylicPT = new G4MaterialPropertiesTable();
  //  acrylicPT->AddProperty("RINDEX",                PhotonEnergy, refractiveIndexA,nEntries);
    acrylicPT->AddProperty("SPECULARLOBECONSTANT",  PhotonEnergy, specularLobe,    nEntries);
    acrylicPT->AddProperty("SPECULARSPIKECONSTANT", PhotonEnergy, specularSpike,   nEntries);
    acrylicPT->AddProperty("BACKSCATTERCONSTANT",   PhotonEnergy, backscatter,     nEntries);
    acrylicPT->AddProperty("REFLECTIVITY",          PhotonEnergy, reflectivity,    nEntries);
    acrylicPT->AddProperty("EFFICIENCY",            PhotonEnergy, efficiency,      nEntries);

    G4MaterialPropertiesTable* waterPT = new G4MaterialPropertiesTable();
  //  waterPT->AddProperty("RINDEX",                PhotonEnergy, refractiveIndexA,nEntries);
    waterPT->AddProperty("SPECULARLOBECONSTANT",  PhotonEnergy, specularLobe,    nEntries);
    waterPT->AddProperty("SPECULARSPIKECONSTANT", PhotonEnergy, specularSpike,   nEntries);
    waterPT->AddProperty("BACKSCATTERCONSTANT",   PhotonEnergy, backscatter,     nEntries);
    waterPT->AddProperty("REFLECTIVITY",          PhotonEnergy, reflectivity,    nEntries);
    waterPT->AddProperty("EFFICIENCY",            PhotonEnergy, efficiency,      nEntries);

    //
    OpAcrylicSurface->SetMaterialPropertiesTable(acrylicPT);
    OpWaterSurface->SetMaterialPropertiesTable(waterPT);
  */
}

//
void T2KWCDetectorConstruction::SetPMT(G4LogicalVolume *mother_log)
{
  // PMT windos
  //
  G4Tubs *PMTwindow_tube = new G4Tubs("pmtwindow", 0., PMTradius, PMTthickness / 2., 0., 360.);

  G4LogicalVolume *PMT_log = new G4LogicalVolume(PMTwindow_tube, Acrylic, "PMT", 0, 0, 0);

  // Surface condition
  SetPMTSurface(PMT_log);

  // visualize option
  G4VisAttributes *PMT_VisAtt = new G4VisAttributes(G4Color(0.8, 0.8, 0.8));
  PMT_VisAtt->SetForceSolid(true);
  PMT_log->SetVisAttributes(PMT_VisAtt);

  /*
    // for reduce MC memory, not yet ready
    PMTParam =
    new T2KWCPMTParameterisation( PMTGap,PMTradius,PMTthickness,tank_r,tank_z );

    //
    physiPMT =
    new G4PVParameterised( "PMTWINDOW",PMT_log,waterTank_log,kZAxis,TotalPMTs,PMTParam );
  */

  // set PMT on end cap
  SetEndCapPMT(mother_log, PMT_log);

  // set PMT on side burrel
  SetSidePMT(mother_log, PMT_log);

  G4cout << "Total set PMTs: " << iPMT << G4endl;

  // set sensicitivity to PMT
  PMTSD = new T2KWCPMTSD("PMTSD");
  SDmanager->AddNewDetector(PMTSD);
  PMT_log->SetSensitiveDetector(PMTSD);
}

//
void T2KWCDetectorConstruction::SetPMT_MINI(G4LogicalVolume *mother_log)
{
  // PMT windos
  //
  G4Tubs *PMTwindow_tube = new G4Tubs("pmtwindow", 0., PMTradius, PMTthickness_mini / 2., 0., 360.);

  G4LogicalVolume *PMT_log = new G4LogicalVolume(PMTwindow_tube, Acrylic, "PMT", 0, 0, 0);
  //= new G4LogicalVolume(PMTwindow_tube,Air2,"PMT",0,0,0); // for acrylic measurment

  // visualize option
  G4VisAttributes *PMT_VisAtt = new G4VisAttributes(G4Color(0.8, 0.8, 0.8));
  PMT_VisAtt->SetForceSolid(true);
  PMT_log->SetVisAttributes(PMT_VisAtt);

  // Surface condition of PMT
  SetPMTSurface(PMT_log);

  G4double zcenter = tank_mini_z + acry_mini_z * 2 + air_mini_z * 2 + PMTthickness_mini / 2.;
  new G4PVPlacement(0, G4ThreeVector(0., 0., zcenter),
                    PMT_log, "PMT", mother_log, false, iPMT);

  iPMT++;

  G4cout << "Total set PMTs: " << iPMT << G4endl;

  // set sensicitivity to PMT
  PMTSD = new T2KWCPMTSD("PMTSD");
  SDmanager->AddNewDetector(PMTSD);
  PMT_log->SetSensitiveDetector(PMTSD);
}

void T2KWCDetectorConstruction::SetPMTSurface(G4LogicalVolume *PMT_log)
{
  // Surface condition of PMT
  G4OpticalSurface *OpPMTSurface = new G4OpticalSurface("PMTSurface");
  OpPMTSurface->SetType(dielectric_dielectric);
  OpPMTSurface->SetFinish(polished);
  OpPMTSurface->SetModel(glisur);

  new G4LogicalSkinSurface("PMTSurface", PMT_log, OpPMTSurface);
}

//
void T2KWCDetectorConstruction::SetEndCapPMT(G4LogicalVolume *mother_log, G4LogicalVolume *PMT_log)
{
  G4ThreeVector cent(0, 0, 0);

  for (int j = 0; j < 2; j++)
  {
    if (j == 0)
      cent(2) = -1. * (tank_z - PMTthickness - 0.5 * mm) / 2.;
    else
      cent(2) = (tank_z - PMTthickness - 1. * mm) / 2.;

    for (int i = 0; i < 28; i++)
    {
      cent(0) = pos_cap[i][0];
      cent(1) = pos_cap[i][1];

      new G4PVPlacement(0, cent, PMT_log, "PMT", mother_log, false, iPMT);

#ifdef PMTPOS
      G4cout << "{ " << cent(0) / cm << ", "
             << cent(1) / cm << ", "
             << cent(2) / cm << " }," << G4endl;
#endif
      iPMT++;
    }
  }
}

//
void T2KWCDetectorConstruction::SetSidePMT(G4LogicalVolume *mother_log, G4LogicalVolume *PMT_log)
{
  G4double delta_angle = 360. * deg / (G4double)nr; // [radian]
  G4double Radius = tank_r - PMTthickness - 1. * mm;
  G4double startZ = -1. * tank_z / 2. + (tank_z - PMTGap * (nz - 1)) / 2.;
  G4double dangle, Z;

  for (int fz = 0; fz < nz; fz++)
  {
    for (int fr = 0; fr < nr; fr++)
    {

      G4RotationMatrix rotM = G4RotationMatrix();
      dangle = delta_angle * (G4double)fr; //[radian]

      rotM.rotateY(90. * deg);
      rotM.rotateZ(dangle);

      Z = PMTGap * (G4double)fz + startZ;
      G4ThreeVector posM(Radius, 0., Z);
      posM.rotateZ(dangle);

      new G4PVPlacement(G4Transform3D(rotM, posM), PMT_log,
                        "PMT", mother_log, false, iPMT);

#ifdef PMTPOS
      G4cout << "{ " << Radius * std::cos(dangle) / cm << ", "
             << Radius * std::sin(dangle) / cm << ", "
             << Z / cm << " }," << G4endl;
#endif

      iPMT++;
    }
  }
}
