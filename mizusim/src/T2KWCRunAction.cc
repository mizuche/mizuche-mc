#include "T2KWCRunAction.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include "Randomize.hh"

#include <sys/time.h>
#include <time.h>

#include "T2KWCRunActionMessenger.hh"

#ifdef G4ANALYSIS_USE
#include "T2KWCAnalysisManager.hh"
#endif

G4long gettimeofday_rand()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  G4cout << "tv_sec:" << tv.tv_sec << G4endl;
  G4cout << "tv_usec:" << tv.tv_usec << G4endl;

  return (G4long)(tv.tv_sec + tv.tv_usec);
}

// //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCRunAction::T2KWCRunAction(const char *filename, T2KWCInputCard *incard0)
{
  // init output root file
  file = new TFile(filename, "RECREATE");

  // init output histgram
  photon = new TH1F("photon", "photon", 500, 1, 6);
  pe = new TH1F("pe", "pe", 500, 1, 6);
  n_vertex = new TH1F("n_vertex", "primary interaction z-position", 200, -100, 100);
  n_vertex->SetXTitle("interaction z-position [cm]");

  std::cerr << __LINE__ << std::endl;
  // initialize data class & ROOT output
  evt = new T2KWCEvent();
  std::cerr << __LINE__ << std::endl;
  tree = new TTree("mc", "Mizuche MC");
  std::cerr << __LINE__ << std::endl;
  tree->Branch("mc.", "T2KWCEvent", &evt, 64000, 99);

  std::cerr << __LINE__ << std::endl;
  // init input card
  incard = incard0;

  // set random seed
  G4long seed;
  int randset = incard0->GetGeneralCard()->randset;
  if (randset == 0)
  {
    seed = gettimeofday_rand();
  }
  else if (randset == 1)
  {
    seed = incard0->GetGeneralCard()->seed;
  }
  else
  {
    G4cerr << "invalid random seed initialization!" << G4endl;
    exit(1);
  }
  G4cout << "input seed : " << seed << G4endl;

  // Default Random engine : HepJameRandom
  // The range of input seed : [0~900000000]
  G4int luxury = 3;
  CLHEP::HepRandom::setTheSeed(seed, luxury);
  CLHEP::HepRandom::showEngineStatus();

  std::cerr << __LINE__ << std::endl;
  theRunActMessenger = new T2KWCRunActionMessenger(this);

  std::cerr << __LINE__ << std::endl;
  evt->SetSeed(seed);

  //
  std::cerr << __LINE__ << std::endl;
  evt->SetFVWater(incard0->GetGeneralCard()->fvwater);

  //
  event_write_flag = 1;
}

// //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCRunAction::~T2KWCRunAction()
{
  delete theRunActMessenger;

  // Write object
  G4cout << "Write output root file ...." << G4endl;
  file->cd();

  photon->Write();
  pe->Write();
  n_vertex->Write();
  tree->Write();
  file->Close();
  G4cout << "... Complete to write output root file" << G4endl;

  G4cout << "Deleting object of RunAction ..." << G4endl;
  if (evt)
    delete evt;
  G4cout << "... Complete to deleting object of RunAction" << G4endl;
}

// //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCRunAction::BeginOfRunAction(const G4Run *aRun)
{

#ifdef G4ANALYSIS_USE
  T2KWCAnalysisManager *analysis = T2KWCAnalysisManager::getInstance();
  analysis->book();
#endif

  G4cout << "### Run " << aRun->GetRunID() << " start..." << G4endl;

  // save Rndm status
  if (saveRndm > 0)
  {
    // CLHEP::HepRandom::showEngineStatus();
    CLHEP::HepRandom::saveEngineStatus("beginOfRun.rndm");
  }
}

// //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCRunAction::EndOfRunAction(const G4Run *aRun)
{

  // save Rndm status
  if (saveRndm == 1)
  {
    CLHEP::HepRandom::showEngineStatus();
    CLHEP::HepRandom::saveEngineStatus("endOfRun.rndm");
  }

  // Write histograms to file
  G4cout << "Close and Save Histograms" << G4endl;
  G4cout << "### Run " << aRun->GetRunID() << " ended." << G4endl;

#ifdef G4ANALYSIS_USE
  T2KWCAnalysisManager *analysis = T2KWCAnalysisManager::getInstance();
  analysis->finish();
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCRunAction::MySetRunID(G4int myrun)
{
  runID = myrun;
}
