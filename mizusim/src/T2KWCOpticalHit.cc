//    ****************************************************
//    *      T2KWCOpticalHit.cc
//    ****************************************************
//
//    Classe defining an optical photon hit in the photomultiplier
//    Data members are the photon energy and incidence position
//
#include "T2KWCOpticalHit.hh"

#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

#define HIT_THRESHOLD 2

G4Allocator<T2KWCOpticalHit> T2KWCOpticalHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCOpticalHit::T2KWCOpticalHit()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCOpticalHit::T2KWCOpticalHit(G4int id, G4ThreeVector position, G4double time)
{
	//
	pmtid = id;
	pmtpos = position;
	hittime = time;

	nhits = 1;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCOpticalHit::~T2KWCOpticalHit()
{
	;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCOpticalHit::T2KWCOpticalHit(const T2KWCOpticalHit &right) : G4VHit()
{
	//
	pmtid = right.pmtid;
	pmtpos = right.pmtpos;
	hittime = right.hittime;
	nhits = right.nhits;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

const T2KWCOpticalHit &T2KWCOpticalHit::operator=(const T2KWCOpticalHit &right)
{
	pmtid = right.pmtid;
	pmtpos = right.pmtpos;
	hittime = right.hittime;
	nhits = right.nhits;

	return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

int T2KWCOpticalHit::operator==(const T2KWCOpticalHit &right) const
{
	return (this == &right) ? 1 : 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

int T2KWCOpticalHit::CompareID(const T2KWCOpticalHit right)
{
	return (pmtid == right.pmtid) ? 1 : 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCOpticalHit::AddHit(const T2KWCOpticalHit right)
{
	//  nhits++;
	nhits = nhits + right.nhits;

	if (nhits >= HIT_THRESHOLD)
	{
		hittime = right.hittime;
	}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void T2KWCOpticalHit::Draw()
{

	G4double size = 20.;

	G4double red[16] = {0., 0., 0., 0., 0., 0., 0., 0., 0., 0.3, 0.6, 1., 1., 1., 1., 1.};
	G4double green[16] = {0., 0.25, 0.5, 0.75, 1., 1., 1., 1., 1., 1., 1., 1., 0.75, 0.5, 0.25, 0.};
	G4double blue[16] = {1., 1., 1., 1., 1., 0.75, 0.5, 0.25, 0., 0., 0., 0., 0., 0., 0., 0.};
	G4int colorid = 0;

	if (0 < nhits && nhits < 16)
		colorid = nhits;
	else
		colorid = 15;

	G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
	if (pVVisManager)
	{
		G4ThreeVector pos;
		for (int i = 0; i < 3; i++)
			pos[i] = pmtpos[i] * mm;
		G4Circle circle(pos);

		circle.SetScreenSize(size);
		circle.SetFillStyle(G4Circle::filled);

		G4VisAttributes attribs(G4Colour(red[colorid], green[colorid], blue[colorid]));
		circle.SetVisAttributes(attribs);
		pVVisManager->Draw(circle);
	}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCOpticalHit::Print()
{
	G4cout.precision(4);

	G4cout << "PMT#: " << pmtid << "\t"
		   << "P.E.: " << nhits << "\t"
		   << "pos [cm]: {"
		   << pmtpos[0] / cm << ","
		   << pmtpos[1] / cm << ","
		   << pmtpos[2] / cm << "}\t"
		   << "Time [ns]: " << hittime
		   << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
