#include "T2KWCPrimaryGeneratorAction.hh"
#include "T2KWCGeometry.hh"
#include "T2KWCPrimaryGeneratorMessenger.hh"
#include "T2KWCRunAction.hh"

#include "Randomize.hh"

#include "G4Event.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4RunManager.hh"

#include <math.h>

#define NUMU_FLAV 1
#define NUMUBAR_FLAV 2

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPrimaryGeneratorAction::
    T2KWCPrimaryGeneratorAction(GeneralCard *gcard0)
    : particleGun(0), gunMessenger(0), simver(0), neutinfo(0)
{
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);

  gcard = gcard0;

  // create a messenger for this class
  gunMessenger = new T2KWCPrimaryGeneratorMessenger(this);

  simver = new T2KWCSimVertex();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPrimaryGeneratorAction::
    T2KWCPrimaryGeneratorAction(GeneralCard *gcard0, T2KWCNeut *a_neut)
    : particleGun(0), gunMessenger(0), simver(0), neutinfo(0)
{
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);

  neut = a_neut;
  gcard = gcard0;

  // create a messenger for this class
  gunMessenger = new T2KWCPrimaryGeneratorMessenger(this);

  simver = new T2KWCSimVertex();
  neutinfo = new T2KWCNeutInfo();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPrimaryGeneratorAction::~T2KWCPrimaryGeneratorAction()
{
  if (particleGun)
    delete particleGun;
  if (gunMessenger)
    delete gunMessenger;
  if (simver)
    delete simver;
  if (neutinfo)
    delete neutinfo;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void T2KWCPrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent)
{
  switch (gcard->runtype)
  {

  case 0:
    SetPrimaryParticles_Single(anEvent);
    break;

  case 1:
    SetPrimaryParticles_Neut(anEvent);
    break;

  default:
    G4cerr << "Invalid Run type !!! " << G4endl;
    exit(-1);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::SetPrimaryParticles_Single(G4Event *anEvent)
{
  // set inject particle
  G4ParticleDefinition *particle;
  G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();

  if (gcard->pid == 0)
    particle = particleTable->FindParticle("opticalphoton");
  else
    particle = particleTable->FindParticle(gcard->pid);

  //  G4cout << "**********" << particle->GetParticleName() << G4endl;
  particleGun->SetParticleDefinition(particle);

  // set init timing
  particleGun->SetParticleTime(0. * ns);

  // set init vertex
  G4ThreeVector vertex0;
  DefineVertex(vertex0);
  G4int pos_flag = GetPosFlag(vertex0);

  // #########################
  /*
    G4double z0 = -80*cm - 1*cm;
    G4double r0 = sqrt( G4UniformRand() );
    G4double angle0 = G4UniformRand() * twopi;
    G4double x0 = cos( angle0 ) * r0 * tank_r;
    G4double y0 = sin( angle0 ) * r0 * tank_r;

    vertex0 = G4ThreeVector(x0, y0, z0);
    pos_flag=0;
  */
  // ########################
  particleGun->SetParticlePosition(vertex0);

  // set init direction
  G4ThreeVector direct0;
  if (gcard->isodir == 1)
  {
    IsotropicDirect(direct0);
  }
  else
  {
    direct0(0) = gcard->direction[0];
    direct0(1) = gcard->direction[1];
    direct0(2) = gcard->direction[2];
  }
  particleGun->SetParticleMomentumDirection(direct0);

  // set init kinetic energy of particle
  G4double mass = particle->GetPDGMass();
  G4double absmom = (gcard->momentum) * MeV;

  // When input optical photon, momentum more than 1MeV means "wave length".
  // Convert from wave length [nm] to Energy [eV]
  if (gcard->pid == 0 && absmom > (1 * MeV))
  {
    absmom = (1239.84 / (absmom / MeV)) * eV;
  }

  // absmom = 120*MeV + 1*GeV*G4UniformRand();

  G4double energy = sqrt(absmom * absmom + mass * mass);
  G4double Ekin = energy - mass;
  particleGun->SetParticleEnergy(Ekin); // set Kinetic Energy

  //
  particleGun->GeneratePrimaryVertex(anEvent);

  // Fill MC true vertex
  simver->Clear("C");
  simver->SetVertex(vertex0(0) / cm, vertex0(1) / cm, vertex0(2) / cm,
                    pos_flag);
  G4RunManager *runMgr = G4RunManager::GetRunManager();
  T2KWCRunAction *runAction = (T2KWCRunAction *)runMgr->GetUserRunAction();
  runAction->GetEventClass()->AddSimVertex(simver);
}

//
void T2KWCPrimaryGeneratorAction::SetPrimaryParticles_Neut(G4Event *anEvent)
{
  G4cout << "\n**** Set Primary Particle ****" << G4endl;

  // read one event from neut file
  ReadOneEvent();

  // Definte vertex of interaction
  G4int pos_flag = GetPosFlag(vertex_neutint);
  particleGun->SetParticlePosition(vertex_neutint);

  // select generated particle in neutrino interaction
  SelectGenParticle(anEvent);

  // fill MC true vertex information
  simver->Clear("C");
  simver->SetVertex(vertex_neutint(0) / cm,
                    vertex_neutint(1) / cm,
                    vertex_neutint(2) / cm,
                    pos_flag);

  G4RunManager *runMgr = G4RunManager::GetRunManager();
  T2KWCRunAction *runAction = (T2KWCRunAction *)runMgr->GetUserRunAction();

  runAction->GetEventClass()->AddSimVertex(simver);

  // fill neutrino vector information
  neutinfo->Clear("C");
  neutinfo->SetEnergy(neut->neutvec.energy);
  neutinfo->SetMode(neut->primary.mode);
  neutinfo->SetDir(neut->neutvec.dir[0],
                   neut->neutvec.dir[1],
                   neut->neutvec.dir[2]);
  neutinfo->SetNorm(neut->neutvec.norm);
  neutinfo->SetTotcrsne(neut->neutint.totcrsne);

  neutinfo->SetNumG(neut->ances.ng);
  for (int i = 0; i < (neut->ances.ng); i++)
  {
    neutinfo->SetGPid(neut->ances.gpid[i]);
    neutinfo->SetGMec(neut->ances.gmec[i]);
    neutinfo->SetGPos(neut->ances.gvx[i],
                      neut->ances.gvy[i],
                      neut->ances.gvz[i]);
    neutinfo->SetGMom(neut->ances.gpx[i],
                      neut->ances.gpy[i],
                      neut->ances.gpz[i]);
    neutinfo->SetGCosbm(neut->ances.gcosbm[i]);
  }

  runAction->GetEventClass()->AddNeutInfo(neutinfo);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::SetOptPhotonPolar()
{
  G4double angle = G4UniformRand() * 360.0 * deg;
  SetOptPhotonPolar(angle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::SetOptPhotonPolar(G4double angle)
{
  if (particleGun->GetParticleDefinition()->GetParticleName() != "opticalphoton")
  {
    G4cout << "--> warning from PrimaryGeneratorAction::SetOptPhotonPolar() :"
              "the particleGun is not an opticalphoton"
           << G4endl;
    return;
  }

  G4ThreeVector normal(1., 0., 0.);
  G4ThreeVector kphoton = particleGun->GetParticleMomentumDirection();
  G4ThreeVector product = normal.cross(kphoton);
  G4double modul2 = product * product;

  G4ThreeVector e_perpend(0., 0., 1.);
  if (modul2 > 0.)
    e_perpend = (1. / std::sqrt(modul2)) * product;
  G4ThreeVector e_paralle = e_perpend.cross(kphoton);

  G4ThreeVector polar = std::cos(angle) * e_paralle + std::sin(angle) * e_perpend;
  particleGun->SetParticlePolarization(polar);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::UniformVertex_Tank(G4ThreeVector &vertex0)
{
  G4double z0 = (G4UniformRand() - 0.5) * tank_z;
  G4double r0 = sqrt(G4UniformRand());
  G4double angle0 = G4UniformRand() * twopi;
  G4double x0 = cos(angle0) * r0 * tank_r;
  G4double y0 = sin(angle0) * r0 * tank_r;

  vertex0 = G4ThreeVector(x0, y0, z0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::UniformVertex_FV(G4ThreeVector &vertex0)
{
  G4double z0 = (G4UniformRand() - 0.5) * fv_z;
  G4double r0 = sqrt(G4UniformRand());
  G4double angle0 = G4UniformRand() * twopi;
  G4double x0 = cos(angle0) * r0 * fv_r;
  G4double y0 = sin(angle0) * r0 * fv_r;

  vertex0 = G4ThreeVector(x0, y0, z0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::UniformVertex_OV(G4ThreeVector &vertex0)
{
  G4double z0 = -1.;
  G4double r0 = -1.;
  G4double angle0 = 0.;

  while (1)
  {
    r0 = sqrt(G4UniformRand()) * tank_r;
    angle0 = (G4UniformRand()) * twopi;
    z0 = (G4UniformRand() - 0.5) * tank_z;

    if (r0 <= fv_r && fabs(z0) <= fv_z / 2.)
    {
      G4cout << "vertex in FV (no water) -> re-select!" << G4endl;
    }
    else
    {
      break;
    }
  }
  G4double x0 = cos(angle0) * r0;
  G4double y0 = sin(angle0) * r0;

  vertex0 = G4ThreeVector(x0, y0, z0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::IsotropicDirect(G4ThreeVector &direct0)
{
  G4double theta0 = G4UniformRand() * pi;
  G4double phi0 = G4UniformRand() * twopi;

  G4double dx0 = cos(phi0) * sin(theta0);
  G4double dy0 = sin(phi0) * sin(theta0);
  G4double dz0 = cos(theta0);

  direct0 = G4ThreeVector(dx0, dy0, dz0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int T2KWCPrimaryGeneratorAction::IsNeutInt()
{
  // Select interaction vertex in Tank
  // Mizuche end cap geometry at JNUBEAM is the 70cm x 70cm square,
  // but actual geometry is the 70cm-radius circle.
  if ((neut->neutvec.rnu) * cm > tank_r)
  {
    G4cout << " --> This vertex is out of Tank (rnu="
           << neut->neutvec.rnu << ")" << G4endl;
    G4cout << " --> Skip this entry" << G4endl;

    return 0;
  }

  // Select interaction neutrino with H2O nucleous
  // for mu neutrino, primary.mode means below mode.
  //  Abs(primary.mode) > 0      : all neutrino interaction
  //  0 < Abs(primary.mode) < 30 : CC interaction
  //  30 < Abs(primary.mode)     : NC interacion
  if ((neut->primary.mode) != 0)
  {

    // At the case there is no water in FV, the entry whose
    // vertex is in FV is skipped.

    // Define vertex position
    if (DefineVertex(vertex_neutint) == 0)
    {
      G4cout << "+++ vertex in FV (no water) -> skip this entry! +++" << G4endl;
      return 0; // skip this event
    }

    G4cout << " --> Read numu entry from neut file" << G4endl;

    return 1;
  }

  // "interaction mode = 0" means "not creat inteaction"
  // Too low neutrino energy to find kinematics combination
  // of generated particles from neutrino interaction.
  // -> Fill only neutrino vector information and skip this event
  else if ((neut->primary.mode) == 0)
  {

    G4cout << " --> This neutrino not interact" << G4endl;

    neutinfo->Clear("C");
    neutinfo->SetEnergy(neut->neutvec.energy);
    neutinfo->SetMode(neut->primary.mode);
    neutinfo->SetDir(neut->neutvec.dir[0],
                     neut->neutvec.dir[1],
                     neut->neutvec.dir[2]);
    neutinfo->SetNorm(neut->neutvec.norm);
    neutinfo->SetTotcrsne(0.);

    neutinfo->SetNumG(neut->ances.ng);
    for (int i = 0; i < (neut->ances.ng); i++)
    {
      neutinfo->SetGPid(neut->ances.gpid[i]);
      neutinfo->SetGMec(neut->ances.gmec[i]);
      neutinfo->SetGPos(neut->ances.gvx[i],
                        neut->ances.gvy[i],
                        neut->ances.gvz[i]);
      neutinfo->SetGMom(neut->ances.gpx[i],
                        neut->ances.gpy[i],
                        neut->ances.gpz[i]);
      neutinfo->SetGCosbm(neut->ances.gcosbm[i]);
    }

    //
    G4RunManager *runMgr = G4RunManager::GetRunManager();
    T2KWCRunAction *runAction = (T2KWCRunAction *)runMgr->GetUserRunAction();
    runAction->GetEventClass()->AddNeutInfo(neutinfo);
    runAction->FillTree();
    runAction->ClearEvt();

    return 0;
  }

  return -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::SelectGenParticle(G4Event *anEvent)
{
  G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition *particle;

  for (int ipart = 0; ipart < (neut->final).npart; ipart++)
  {

    // select generated particles not interacted in neucleus
    // NEUT file include the history of particles which interacted and
    // absorption, etc.
    if ((neut->final).icrnv[ipart] == 1)
    {

      particle = particleTable->FindParticle((neut->final).pid[ipart]);
      particleGun->SetParticleDefinition(particle);

      G4ThreeVector nvec;
      for (int i = 0; i < 3; i++)
        nvec(i) = (neut->final).mom[ipart][i] / (neut->final).absmom[ipart];

      particleGun->SetParticleMomentumDirection(nvec);

      G4double mass = particle->GetPDGMass();
      G4double absmom = (neut->final).absmom[ipart] * MeV;
      G4double energy = sqrt(mass * mass + absmom * absmom) - mass;
      particleGun->SetParticleEnergy(energy);

      particleGun->SetParticleTime(0.0 * ns);

      //
      particleGun->GeneratePrimaryVertex(anEvent);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorAction::ReadOneEvent()
{
  G4int read_flag, flav, int_flag;

  while (1)
  {

    read_flag = neut->ReadEvent(gcard->dspneut);

    if (read_flag > 0)
    {
      G4cout << "\n=== Success to read entry from neut file ===" << G4endl;
    }

    else
    {
      G4cout << "!!! Fail to read any entries from neut file !!!" << G4endl;
      G4cout << "!!! End This Run !!!" << G4endl;
      G4RunManager *runManager = G4RunManager::GetRunManager();
      runManager->AbortRun(1);
      T2KWCRunAction *runAction = (T2KWCRunAction *)runManager->GetUserRunAction();
      runAction->SetEventWriteFlag(-1111);

      return;
    }

    // Currently, neut file (for numu) has all neufrino flavor.
    // (due to the bug of NEUT).
    // Need to select only numu. -> break while loop.
    flav = (neut->neutvec).production_mode;

    if ((int)(flav / 10) == NUMU_FLAV)
    {

      int_flag = IsNeutInt();

      if (int_flag == 1)
      {
        G4cout << "Select neutrino interaction" << G4endl;
        break;
      }

      else if (int_flag < 0)
      {
        G4cout << "!!! Invalid entry !!!" << G4endl;
        G4cout << "!!! End This Run !!!" << G4endl;
        G4RunManager *runManager = G4RunManager::GetRunManager();
        runManager->AbortRun(1);
        T2KWCRunAction *runAction = (T2KWCRunAction *)runManager->GetUserRunAction();
        runAction->SetEventWriteFlag(-1111);

        return;
      }
    }
    else
    {
      G4cout << "This neutrino flav : " << flav << ", not wanted" << G4endl;
      G4cout << "--> Skip this entry" << G4endl;
    }

  } // end of while loop
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int T2KWCPrimaryGeneratorAction::DefineVertex(G4ThreeVector &vertex0)
{
  //// Case of single particle MC ////
  if (gcard->runtype == 0)
  {

    // Uniform distribution in Tank
    if (gcard->univer == 1)
      UniformVertex_Tank(vertex0);

    // Uniform distribution in FV
    else if (gcard->univer == 2)
      UniformVertex_FV(vertex0);

    // Uniform distribution in OV
    else if (gcard->univer == 3)
      UniformVertex_OV(vertex0);

    // Use input vertex position
    else if (gcard->univer == 0)
    {
      vertex0(0) = (gcard->vertex[0]) * cm;
      vertex0(1) = (gcard->vertex[1]) * cm;
      vertex0(2) = (gcard->vertex[2]) * cm;
    }

    else
    {
      G4cout << "invalid vertex setting" << G4endl;
      exit(1);
    }

  } // end of single particle MC

  //// Case of neutrino interaction MC ////
  else if (gcard->runtype == 1)
  {

    vertex0(0) = (neut->neutvec.xnu) * cm;
    vertex0(1) = (neut->neutvec.ynu) * cm;
    vertex0(2) = (G4UniformRand() - 0.5) * tank_z;

    // without water in FV
    if (gcard->fvwater == 0)
    {
      if (GetPosFlag(vertex0) == INFV)
      {
        return 0;
      }
    }

  } // end of neutrino interaction MC

  else
  {
    G4cout << "invalid runtype" << G4endl;
    exit(1);
  }

  return 1;
}
