//////////////////////////////////////////////////////////////////////////////
// This physics list set is almost copy of QGSP.hh, which is sample set of GEANT4.
// The part of optical-photon physics (cerenkov) is added myself.
// But I don't know whether the QGSP physics list consider the process about
// optical photon (EmExtraPhysics seems to have, so is removed).
/////////////////////////////////////////////////////////////////////////////

//// web referene for Geant4 physics list
// http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch05s02.html

#include "T2KWCPhysicsListCustom.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "globals.hh"

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4ios.hh"
#include <iomanip>

#include "G4DecayPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4IonPhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4QStoppingPhysics.hh"

#include "G4DataQuestionaire.hh"

// Select hadron physics list
#define USEQGSP_BERT_MODEL

#ifdef USEQGSP_MODEL
#include "G4HadronElasticPhysics.hh"
#include "HadronPhysicsQGSP.hh"
#endif

#ifdef USEQGSP_BERT_MODEL
#include "G4HadronElasticPhysics.hh"
#include "HadronPhysicsQGSP_BERT.hh"
#endif

#ifdef USEQGSP_BERT_HP_MODEL
#include "G4HadronElasticPhysicsHP.hh"
#include "HadronPhysicsQGSP_BERT_HP.hh"
#endif

//
#include "T2KWCOpticalPhysics.hh"

T2KWCPhysicsListCustom::T2KWCPhysicsListCustom(G4int ver) : G4VModularPhysicsList()
{

  ///// The follwoing is copied from QGSP Physics list ///////
  // QGSP is the basic physics list applying the quark gluon string model for high energy
  // interactions of protons, neutrons, pions, and Kaons and nuclei. The high energy
  // interaction creates an exited nucleus, which is passed to the precompound model
  // modeling the nuclear de-excitation.
#ifdef USEQGSP_MODEL

  G4DataQuestionaire it(photon);
  G4cout << "<<< Geant4 Physics List simulation engine: QGSP 3.3" << G4endl;
  G4cout << G4endl;

  G4cout << "Physics List Verbose level : " << ver << G4endl;

  this->defaultCutValue = 0.7 * mm; // this is default of QGSP 3.3
  this->SetVerboseLevel(ver);

  // EM Physics
  this->RegisterPhysics(new G4EmStandardPhysics(ver));

  // Synchroton Radiation & GN Physics
  this->RegisterPhysics(new G4EmExtraPhysics("extra EM"));

  // Decays
  this->RegisterPhysics(new G4DecayPhysics("decay", ver));

  // Hadron Elastic scattering
  this->RegisterPhysics(new G4HadronElasticPhysics("elastic", ver, false));

  // Hadron Physics
  G4bool quasiElastic;
  this->RegisterPhysics(new HadronPhysicsQGSP("hadron", quasiElastic = true));

  // Stopping Physics
  this->RegisterPhysics(new G4QStoppingPhysics("stopping"));

  // Ion Physics
  this->RegisterPhysics(new G4IonPhysics("ion"));

  // Neutron tracking cut
  this->RegisterPhysics(new G4NeutronTrackingCut("Neutron tracking cut", ver));

#endif

#ifdef USEQGSP_BERT_MODEL
  ///// The follwoing is copied from QGSP_BERT Physics list ///////
  // Like QGSP, but using Geant4 Bertini cascade for primary protons, neutrons, pions
  // and Kaons below ~10GeV. In comparison to experimental data we find improved agreement
  // to data compared to QGSP which uses the low energy parameterised (LEP) model for all
  // particles at these energies. The Bertini model produces more secondary neutrons and
  // protons than the LEP model, yielding a better agreement to experimental data.

  // Main difference from QGSP is Hadron Physics.

  G4DataQuestionaire it(photon);
  G4cout << "<<< Geant4 Physics List simulation engine: QGSP_BERT 3.4" << G4endl;
  G4cout << G4endl;

  this->defaultCutValue = 0.7 * mm;
  this->SetVerboseLevel(ver);

  // EM Physics
  this->RegisterPhysics(new G4EmStandardPhysics(ver));

  // Synchroton Radiation & GN Physics
  this->RegisterPhysics(new G4EmExtraPhysics(ver));

  // Decays
  this->RegisterPhysics(new G4DecayPhysics(ver));

  // Hadron Elastic scattering
  this->RegisterPhysics(new G4HadronElasticPhysics(ver));

  // Hadron Physics
  this->RegisterPhysics(new HadronPhysicsQGSP_BERT(ver));

  // Stopping Physics
  this->RegisterPhysics(new G4QStoppingPhysics(ver));

  // Ion Physics
  this->RegisterPhysics(new G4IonPhysics(ver));

  // Neutron tracking cut
  this->RegisterPhysics(new G4NeutronTrackingCut(ver));

#endif

#ifdef USEQGSP_BERT_HP_MODEL
  ///// The follwoing is copied from QGSP_BERT_HP Physics list ///////
  // This list is similar to QGSP_BERT and in addition uses the data driven high
  // precision neutron package (NeutronHP) to transport neutrons below 20 MeV down
  // to thermal energies.

  G4DataQuestionaire it(photon, neutron);
  G4cout << "<<< Geant4 Physics List simulation engine: QGSP_BERT_HP 2.4" << G4endl;
  G4cout << G4endl << G4endl;

  this->defaultCutValue = 0.7 * mm;
  this->SetVerboseLevel(ver);

  // EM Physics
  this->RegisterPhysics(new G4EmStandardPhysics(ver));

  // Synchroton Radiation & GN Physics
  this->RegisterPhysics(new G4EmExtraPhysics(ver));

  // Decays
  this->RegisterPhysics(new G4DecayPhysics(ver));

  // Hadron Elastic scattering
  this->RegisterPhysics(new G4HadronElasticPhysicsHP(ver));

  // Hadron Physics
  this->RegisterPhysics(new HadronPhysicsQGSP_BERT_HP(ver));

  // Stopping Physics
  this->RegisterPhysics(new G4QStoppingPhysics(ver));

  // Ion Physics
  this->RegisterPhysics(new G4IonPhysics(ver));

#endif

  ////////// Cerenkov Physics list //////////
  this->RegisterPhysics(new T2KWCOpticalPhysics("OpticalPhoton", ver));
}

T2KWCPhysicsListCustom::~T2KWCPhysicsListCustom()
{
}

void T2KWCPhysicsListCustom::SetCuts()
{
  if (this->verboseLevel > 1)
  {
    G4cout << "QGSP_BERT::SetCuts:";
  }
  //  " G4VUserPhysicsList::SetCutsWithDefault" method sets
  //   the default cut value for all particle types

  this->SetCutsWithDefault();

  //  if (this->verboseLevel >0)
  //    G4VUserPhysicsList::DumpCutValuesTable();
}
