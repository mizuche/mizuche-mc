#include "T2KWCPhysicsListMessenger.hh"

#include "G4UIcmdWithAnInteger.hh"
#include "G4UIdirectory.hh"
#include "T2KWCPhysicsList.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPhysicsListMessenger::T2KWCPhysicsListMessenger(T2KWCPhysicsList *pPhys)
    : pPhysicsList(pPhys)
{
  N06Dir = new G4UIdirectory("/N06/");
  N06Dir->SetGuidance("UI commands of this example");

  physDir = new G4UIdirectory("/N06/phys/");
  physDir->SetGuidance("PhysicsList control");

  verboseCmd = new G4UIcmdWithAnInteger("/N06/phys/verbose", this);
  verboseCmd->SetGuidance("set verbose for physics processes");
  verboseCmd->SetParameterName("verbose", true);
  verboseCmd->SetDefaultValue(1);
  verboseCmd->SetRange("verbose>=0");
  verboseCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  cerenkovCmd = new G4UIcmdWithAnInteger("/N06/phys/cerenkovMaxPhotons", this);
  cerenkovCmd->SetGuidance("set max nb of photons per step");
  cerenkovCmd->SetParameterName("MaxNumber", false);
  cerenkovCmd->SetRange("MaxNumber>=0");
  cerenkovCmd->AvailableForStates(G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPhysicsListMessenger::~T2KWCPhysicsListMessenger()
{
  delete verboseCmd;
  delete cerenkovCmd;
  delete physDir;
  delete N06Dir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPhysicsListMessenger::SetNewValue(G4UIcommand *command,
                                            G4String newValue)
{
  if (command == verboseCmd)
  {
    pPhysicsList->SetVerbose(verboseCmd->GetNewIntValue(newValue));
  }

  if (command == cerenkovCmd)
  {
    pPhysicsList->SetNbOfPhotonsCerenkov(cerenkovCmd->GetNewIntValue(newValue));
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
