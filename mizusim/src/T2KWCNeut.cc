#include <iostream>
#include <stdio.h>

#include "T2KWCNeut.hh"

//
static PrimaryState primary0;	// dummy
static FinalState final0;		// dummy
static NeutVector neutvec0;		// dummy
static AncestorParticle ances0; // dummy

//
T2KWCNeut::T2KWCNeut(const char *input_neutfile)
	: nevents(0), ievent(0), read_event(0),
	  primary(primary0), final(final0), neutvec(neutvec0), ances(ances0)
{
	neutfile = new TFile(input_neutfile);
	if (neutfile->IsZombie())
	{
		G4cerr << "Invalid input neut file !!!" << G4endl;
		exit(1);
	}

	tree = (TTree *)neutfile->Get("h10");
	if (!tree)
	{
		G4cerr << "There is not tree(h10) in input neut file" << G4endl;
		exit(1);
	}

	nevents = tree->GetEntries();
	G4cout << "Read total number of events : " << nevents << G4endl;

	InitTree();
}

//
T2KWCNeut::~T2KWCNeut()
{
	if (neutfile)
		delete neutfile;
	if (tree)
		delete tree;
}

//
void T2KWCNeut::InitTree()
{
	tree->SetBranchAddress("Nev", &ievent);

	// neutrino interaction info
	tree->SetBranchAddress("Mode", &(primary.mode));
	tree->SetBranchAddress("Totcrsne", &(neutint.totcrsne));

	// generated particle info of primary status (before neucleus effect)
	tree->SetBranchAddress("Numnu", &(primary.npart));
	tree->SetBranchAddress("Ipnu", (primary.pid));
	tree->SetBranchAddress("Abspnu", (primary.absmom));
	tree->SetBranchAddress("Pnu", (primary.mom));

	// generated particle info of final status (after neucleus effect)
	tree->SetBranchAddress("Npar", &(final.npart));
	tree->SetBranchAddress("Ipv", (final.pid));
	tree->SetBranchAddress("Iorgv", (final.ppid));
	tree->SetBranchAddress("Icrnv", (final.icrnv));
	tree->SetBranchAddress("Iflgv", (final.iflgv));
	tree->SetBranchAddress("Abspv", (final.absmom));
	tree->SetBranchAddress("Pmomv", (final.mom));
	tree->SetBranchAddress("Pos", (final.vertex));

	// neutrino flux info
	tree->SetBranchAddress("Enu", &(neutvec.energy));
	tree->SetBranchAddress("ppid", &(neutvec.pid));
	tree->SetBranchAddress("modef", &(neutvec.production_mode));
	tree->SetBranchAddress("norm", &(neutvec.norm));
	tree->SetBranchAddress("rnu", &(neutvec.rnu));
	tree->SetBranchAddress("xnu", &(neutvec.xnu));
	tree->SetBranchAddress("ynu", &(neutvec.ynu));
	tree->SetBranchAddress("nnu", (neutvec.dir));
	tree->SetBranchAddress("idfd", &(neutvec.idfd));

	// ancestor particle of neutrino
	tree->SetBranchAddress("ng", &(ances.ng));
	tree->SetBranchAddress("gpx", (ances.gpx));
	tree->SetBranchAddress("gpy", (ances.gpy));
	tree->SetBranchAddress("gpz", (ances.gpz));
	tree->SetBranchAddress("gcosbm", (ances.gcosbm));
	tree->SetBranchAddress("gvx", (ances.gvx));
	tree->SetBranchAddress("gvy", (ances.gvy));
	tree->SetBranchAddress("gvz", (ances.gvz));
	tree->SetBranchAddress("gpid", (ances.gpid));
	tree->SetBranchAddress("gmec", (ances.gmec));
}

//
long T2KWCNeut::GetTotalEvents()
{
	return nevents;
}

//
int T2KWCNeut::ReadEvent(G4int verbose)
{
	G4int read_flag = tree->GetEntry(read_event);
	G4cout << "\n Event#:" << read_event
		   << ", Tree read flag : " << read_flag
		   << G4endl;
	read_event++;

	if (verbose > 0)
		PrintAll();

	return read_flag;
}

//
void T2KWCNeut::PrintAll()
{
	G4cout << "\n===== Information read from Neut file =====" << G4endl;

	G4cout << "# of event : " << ievent << G4endl;
	G4cout << "interaction mode : " << primary.mode << G4endl;

	G4cout << "=== Primary State Particle ===" << G4endl;
	for (int i = 0; i < primary.npart; i++)
	{
		G4cout << "  PID : " << primary.pid[i] << G4endl;
		G4cout << "  Absolute momentum : " << primary.absmom[i] << G4endl;
		G4cout << "  momentum : " << (primary.mom)[i][0] << ", " << (primary.mom)[i][1] << ", " << (primary.mom)[i][2] << G4endl;
	}

	G4cout << "=== Final State Particle ===" << G4endl;
	for (int i = 0; i < final.npart; i++)
	{
		G4cout << "  PID : " << final.pid[i] << G4endl;
		G4cout << "  Absolute momentum : " << final.absmom[i] << G4endl;
		G4cout << "  momentum : " << (final.mom)[i][0] << ", " << (final.mom)[i][1] << ", " << (final.mom)[i][2] << G4endl;
		G4cout << "  Tracking flag : " << final.icrnv[i] << G4endl;
	}

	G4cout << "=== Neutrino vector ===" << G4endl;
	G4cout << "Energy : " << neutvec.energy << G4endl;
	G4cout << "IDFD : " << neutvec.idfd << G4endl;
	G4cout << "Intearction vertex : " << neutvec.xnu << ", " << neutvec.ynu << G4endl;
	G4cout << "Production mode : " << neutvec.production_mode << G4endl;
	G4cout << "Norm : " << neutvec.norm << G4endl;
	G4cout << "Total cross-seciton : " << neutint.totcrsne << G4endl;

	G4cout << "=== Ancestor of Neutrino ===" << G4endl;
	G4cout << "NG : " << ances.ng << G4endl;
	for (int i = 0; i < ances.ng; i++)
	{
		G4cout << "  ng# : " << i << G4endl;
		G4cout << "  pid : " << ances.gpid[i] << G4endl;
		G4cout << "  mechanism : " << ances.gmec[i] << G4endl;
		G4cout << "  pos : " << ances.gpx[i] << ","
			   << ances.gpy[i] << ","
			   << ances.gpz[i] << G4endl;
		G4cout << "  mom : " << ances.gvx[i] << ","
			   << ances.gvy[i] << ","
			   << ances.gvz[i] << G4endl;
	}
}
