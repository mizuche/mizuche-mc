//    ****************************************************
//    *      T2KWCPMTSD.cc
//    ****************************************************
//
//    Class used to define the Ultra photomultiplier as a sensitive detector.
//    Hits in this sensitive detector are defined in the T2KWCOpticalHit class
//
#include "T2KWCPMTSD.hh"
#include "T2KWCDetectorResponse.hh"

#include "G4Material.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4Track.hh"
#include "G4VTouchable.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCPMTSD::T2KWCPMTSD(G4String name) : G4VSensitiveDetector(name)
{

  collectionName.insert("OpticalHitsCollection");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

T2KWCPMTSD::~T2KWCPMTSD()
{
  ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCPMTSD::Initialize(G4HCofThisEvent *HCE)
{

  static int HCID1 = -1;

  // SensitiveDetectorName and collectionName are data members of G4VSensitiveDetector

  OpticalHitsCollection =
      new T2KWCOpticalHitsCollection(SensitiveDetectorName, collectionName[0]);

  if (HCID1 < 0)
  {
    HCID1 = GetCollectionID(0);
  }
  HCE->AddHitsCollection(HCID1, OpticalHitsCollection);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4bool T2KWCPMTSD::ProcessHits(G4Step *aStep, G4TouchableHistory *)
{
  // G4cout << "*** HIT! ***" << G4endl;

  //
  G4Track *aTrack = aStep->GetTrack();
  const G4VTouchable *touchable = aStep->GetPreStepPoint()->GetTouchable();

  // Get Material
  G4String thisVolume = aTrack->GetVolume()->GetName();
  G4String particleName = aTrack->GetDefinition()->GetParticleName();

  // Other (charged) particles from optical photon are set not
  // to deposite enerty.
  if (particleName != "opticalphoton")
    return false;
  //  if (thisVolume != "PMT1" && thisVolume != "PMT2") return false;

  /*
  G4cout << "opticalphoton's Kinetic Energy [eV] : "
         << aTrack->GetKineticEnergy()/eV << G4endl; // for debug
  */

  // Define optical photon goes through acrylic window
  // not use currently (2011.3.23)
  /*
    if( G4UniformRand() > AcrylicTrans(aTrack->GetKineticEnergy(),ACRYLIC_WOR) )
      return false;
  */

  // The track of optical-photon stops when create hit in the PMT window.
  if (particleName == "opticalphoton")
    aTrack->SetTrackStatus(fStopAndKill);

  // G4cout << particleName << " killed" << G4endl; // for debug

  // Get HIT info
  G4int pmtid = touchable->GetVolume(0)->GetCopyNo();                // PMT ID#
  G4ThreeVector pmtpos = touchable->GetVolume(0)->GetTranslation();  // center position of PMT
  G4double hittime = aStep->GetPreStepPoint()->GetGlobalTime() / ns; // hit time [ns]

  T2KWCOpticalHit *OpticalHit = new T2KWCOpticalHit(pmtid, pmtpos, hittime);

  //
  T2KWCOpticalHit *aHit;
  for (int iHit = 0; iHit < OpticalHitsCollection->entries(); iHit++)
  {

    aHit = (*OpticalHitsCollection)[iHit];

    // If there are other hits on this PMT, this hit is merged to
    // the earlier one in the same PMT.
    if (aHit->CompareID(*OpticalHit))
    {
      aHit->AddHit(*OpticalHit);
      return true;
    }
  }

  OpticalHitsCollection->insert(OpticalHit);

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCPMTSD::EndOfEvent(G4HCofThisEvent *HCE)
{
  static G4int HCID = -1;
  if (HCID < 0)
  {
    HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  }
  HCE->AddHitsCollection(HCID, OpticalHitsCollection);

  //
  DrawAll();
  //	PrintAll();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCPMTSD::clear()
{
  ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCPMTSD::DrawAll()
{
  for (int iHit = 0; iHit < OpticalHitsCollection->entries(); iHit++)
    (*OpticalHitsCollection)[iHit]->Draw();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void T2KWCPMTSD::PrintAll()
{
  G4cout << "\n### Print hit infomation in this Event ###" << G4endl;

  for (int iHit = 0; iHit < OpticalHitsCollection->entries(); iHit++)
    (*OpticalHitsCollection)[iHit]->Print();

  G4cout << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
