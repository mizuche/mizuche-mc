//    ****************************************************
//    *      T2KWCRunActionMessenger.cc
//    ****************************************************
//
//    Messenger Class for T2KWCRunAction
//    Allows to set the run ID
//
#include "T2KWCRunActionMessenger.hh"
#include "G4UIcommand.hh"
#include "G4UIdirectory.hh"
#include "G4UImanager.hh"
#include "G4UIparameter.hh"
#include "G4ios.hh"
#include "T2KWCRunAction.hh"
#include <sstream>

T2KWCRunActionMessenger::T2KWCRunActionMessenger(T2KWCRunAction *aRunAction)
    : theRunAction(aRunAction)
{
  runDirectory = new G4UIdirectory("/mysetrun/");
  runDirectory->SetGuidance("My set commands.");

  runIDCmd = new G4UIcommand("/mysetrun/SetRunID", this);
  runIDCmd->SetGuidance("Set run ID");
  runIDCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  G4UIparameter *p1 = new G4UIparameter("runID", 'i', true);
  p1->SetDefaultValue(1000);
  runIDCmd->SetParameter(p1);
}

T2KWCRunActionMessenger::~T2KWCRunActionMessenger()
{
  delete runIDCmd;
  delete runDirectory;
}

void T2KWCRunActionMessenger::SetNewValue(G4UIcommand *command, G4String newValue)
{
  const char *nv = (const char *)newValue;
  if (command == runIDCmd)
  {
    G4int id;
    std::istringstream is(nv);
    is >> id;

    theRunAction->MySetRunID(id);
  }
}
