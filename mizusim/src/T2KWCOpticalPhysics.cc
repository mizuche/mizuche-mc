#include "T2KWCOpticalPhysics.hh"

#include "G4ios.hh"
#include <iomanip>

#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "globals.hh"

#include "G4Material.hh"
#include "G4MaterialTable.hh"

#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"

#include "G4Cerenkov.hh"
#include "G4OpAbsorption.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4OpRayleigh.hh"

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCOpticalPhysics::T2KWCOpticalPhysics(const G4String &name, G4int ver)
    : G4VPhysicsConstructor(name), verbose_level(ver)
{
  theCerenkovProcess = 0;
  theAbsorptionProcess = 0;
  theRayleighScatteringProcess = 0;
  theBoundaryProcess = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCOpticalPhysics::~T2KWCOpticalPhysics() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCOpticalPhysics::ConstructParticle()
{
  // pseudo-particles
  G4Geantino::GeantinoDefinition();
  G4ChargedGeantino::ChargedGeantinoDefinition();

  // gamma
  G4Gamma::GammaDefinition();

  // optical photon
  G4OpticalPhoton::OpticalPhotonDefinition();
}

void T2KWCOpticalPhysics::ConstructProcess()
{
  theCerenkovProcess = new G4Cerenkov("Cerenkov");
  theAbsorptionProcess = new G4OpAbsorption();
  theRayleighScatteringProcess = new G4OpRayleigh();
  theBoundaryProcess = new G4OpBoundaryProcess();

  theCerenkovProcess->SetVerboseLevel(verbose_level);
  theAbsorptionProcess->SetVerboseLevel(verbose_level);
  theRayleighScatteringProcess->SetVerboseLevel(verbose_level);
  theBoundaryProcess->SetVerboseLevel(verbose_level);

  if (verbose_level > 0)
  {
    G4cout << "Dump the table of Cerenkov Process" << G4endl;
    theCerenkovProcess->DumpPhysicsTable();
    G4cout << "Dump the table of RayleighScattering Process" << G4endl;
    theRayleighScatteringProcess->DumpPhysicsTable();
  }

  //// manage memory of cherenkov production
  // -> http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch05s02.html#sect.PhysProc.Photo
  // theCerenkovProcess->SetMaxNumPhotonsPerStep(300); -> Geant4 default: turn on
  // theCerenkovProcess->SetMaxBetaChangePerStep(10.0); -> Geant4 default: turn on
  theCerenkovProcess->SetTrackSecondariesFirst(true);

  //// Optical surface setting
  // G4OpticalSurfaceModel themodel = unified;
  G4OpticalSurfaceModel themodel = glisur;
  theBoundaryProcess->SetModel(themodel);

  // registration
  theParticleIterator->reset();
  while ((*theParticleIterator)())
  {
    G4ParticleDefinition *particle = theParticleIterator->value();
    G4ProcessManager *pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();

    if (theCerenkovProcess->IsApplicable(*particle))
    {

      // for ~ geant4.9.0
      // pmanager->AddContinuousProcess(theCerenkovProcess);

      // for geant4.9.2 ~ (latest)
      pmanager->AddProcess(theCerenkovProcess);
      pmanager->SetProcessOrdering(theCerenkovProcess, idxPostStep);
    }

    if (particleName == "opticalphoton")
    {
      G4cout << " AddDiscreteProcess to OpticalPhoton " << G4endl;
      pmanager->AddDiscreteProcess(theAbsorptionProcess);
      pmanager->AddDiscreteProcess(theRayleighScatteringProcess);
      pmanager->AddDiscreteProcess(theBoundaryProcess);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void T2KWCOpticalPhysics::SetNbOfPhotonsCerenkov(G4int MaxNumber)
{
  theCerenkovProcess->SetMaxNumPhotonsPerStep(MaxNumber);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCOpticalPhysics::SetCuts()
{
  /*
    if( verbose_level>0 ) {
      G4cout << "T2KWCOpticalPhysics::SetCuts:";
    }
    SetCutsWithDefault();
  */
}
