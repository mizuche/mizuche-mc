#include "T2KWCInputCard.hh"

#include <fstream>
#include <iostream>
#include <sstream>

// __________________________________________________
T2KWCInputCard::T2KWCInputCard(const char *fname)
{
    printf("[%s]\tRead Input Card File '%s'\n", __FUNCTION__, fname);
    std::ifstream ifs(fname);
    if (ifs.fail())
    {
        fprintf(stderr, "Error:\t[%s]\tCannot open '%s'\n", __FUNCTION__, fname);
        exit(-1);
    }

    char buf[256];
    std::string name;

    while (!ifs.eof())
    {
        ifs.getline(buf, 256);
        std::istringstream iss(buf);
        iss >> name;

        // General
        if (name == "NEVENT")
            iss >> gcard.nevent;
        else if (name == "FVWATER")
            iss >> gcard.fvwater;
        else if (name == "DETECTOR")
            iss >> gcard.detector;
        else if (name == "RUNTYP")
            iss >> gcard.runtype;
        else if (name == "RANDSET")
            iss >> gcard.randset;
        else if (name == "PHYSLIST")
            iss >> gcard.phylist;

        else if (name == "PARTICLE")
            iss >> gcard.pid;
        else if (name == "VERTEX")
            iss >> gcard.vertex[0] >> gcard.vertex[1] >> gcard.vertex[2];
        else if (name == "UNIVER")
            iss >> gcard.univer;
        else if (name == "DIRECTION")
            iss >> gcard.direction[0] >> gcard.direction[1] >> gcard.direction[2];
        else if (name == "ISODIR")
            iss >> gcard.isodir;
        else if (name == "MOMENTUM")
            iss >> gcard.momentum;

        else if (name == "DSPVTX")
            iss >> gcard.dspvtx;
        else if (name == "DSPNEUT")
            iss >> gcard.dspneut;
        else if (name == "DSPHIT")
            iss >> gcard.dsphit;
        else if (name == "DSPSTCK")
            iss >> gcard.dspstck;
        else if (name == "PHYVER")
            iss >> gcard.phyver;
    }

    ifs.close();

    gcard.seed = 0;
}

// __________________________________________________
T2KWCInputCard::~T2KWCInputCard()
{
}

// __________________________________________________
void T2KWCInputCard::SetSeed(long inseed)
{
    gcard.seed = inseed;
}

// __________________________________________________
void T2KWCInputCard::PrintAll()
{
    printf("[%s]\tInput Card\n", __FUNCTION__);
    PrintGeneral();
}

// __________________________________________________
void T2KWCInputCard::PrintGeneral()
{
    printf("[%s]\tInput Card File Setting\n", __FUNCTION__);

    G4cout << "NEVENT    : " << gcard.nevent << G4endl;
    G4cout << "DETECTOR  : " << gcard.detector << G4endl;
    G4cout << "FVWATER   : " << gcard.fvwater << G4endl;
    G4cout << "RUNTYP    : " << gcard.runtype << G4endl;
    G4cout << "RANDSET   : " << gcard.randset << G4endl;
    G4cout << "PARTICLE  : " << gcard.pid << G4endl;
    G4cout << "UNIVER    : " << gcard.univer << G4endl;
    G4cout << "VERTEX    : " << gcard.vertex[0] << ",\t"
           << gcard.vertex[1] << ",\t"
           << gcard.vertex[2] << G4endl;
    G4cout << "ISODIR    : " << gcard.isodir << G4endl;
    G4cout << "DIRECTION : " << gcard.direction[0] << ",\t"
           << gcard.direction[1] << ",\t"
           << gcard.direction[2] << G4endl;
    G4cout << "MOMENTUM  : " << gcard.momentum << G4endl;
    G4cout << "DSPVTX    : " << gcard.dspvtx << G4endl;
    G4cout << "DSPNEUT   : " << gcard.dspneut << G4endl;
    G4cout << "DSPHIT    : " << gcard.dsphit << G4endl;
    G4cout << "DSPSTCK   : " << gcard.dspstck << G4endl;
    G4cout << "PHYVER    : " << gcard.phyver << G4endl;
    G4cout << "PHYSLIST  : " << gcard.phylist << G4endl;
}
