#include "T2KWCGeometry.hh"

G4int GetPosFlag(G4ThreeVector pos)
{
  G4double pos_r = sqrt(pos(0) * pos(0) + pos(1) * pos(1));

  G4int flag_tmp = -1;

  // in FV
  if (pos_r <= fv_r && fabs(pos(2)) <= fv_z / 2.)
    flag_tmp = INFV;

  // out Tank
  else if (pos_r > tank_r || fabs(pos(2)) > tank_z / 2.)
    flag_tmp = OUTTANK;

  // in Tank, out FV (including acrylic area)
  else
    flag_tmp = INOV;

  return flag_tmp;
}
