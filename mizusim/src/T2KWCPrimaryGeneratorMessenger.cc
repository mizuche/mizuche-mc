#include "T2KWCPrimaryGeneratorMessenger.hh"

#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIdirectory.hh"
#include "T2KWCPrimaryGeneratorAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPrimaryGeneratorMessenger::T2KWCPrimaryGeneratorMessenger(
    T2KWCPrimaryGeneratorAction *T2KWCGun)
    : T2KWCAction(T2KWCGun)
{
  gunDir = new G4UIdirectory("/N06/gun/");
  gunDir->SetGuidance("PrimaryGenerator control");

  polarCmd = new G4UIcmdWithADoubleAndUnit("/N06/gun/optPhotonPolar", this);
  polarCmd->SetGuidance("Set linear polarization");
  polarCmd->SetGuidance("  angle w.r.t. (k,n) plane");
  polarCmd->SetParameterName("angle", true);
  polarCmd->SetUnitCategory("Angle");
  polarCmd->SetDefaultValue(-360.0);
  polarCmd->SetDefaultUnit("deg");
  polarCmd->AvailableForStates(G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

T2KWCPrimaryGeneratorMessenger::~T2KWCPrimaryGeneratorMessenger()
{
  delete polarCmd;
  delete gunDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void T2KWCPrimaryGeneratorMessenger::SetNewValue(
    G4UIcommand *command, G4String newValue)
{
  G4cout << "##############tesko###########" << G4endl;
  if (command == polarCmd)
  {
    G4double angle = polarCmd->GetNewDoubleValue(newValue);
    if (angle == -360.0 * deg)
    {
      T2KWCAction->SetOptPhotonPolar();
    }
    else
    {
      T2KWCAction->SetOptPhotonPolar(angle);
    }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
