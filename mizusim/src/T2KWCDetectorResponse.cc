#include "T2KWCDetectorResponse.hh"

G4double PMTQE(G4double energy)
{
  G4double qe = 0.;

  // QE of PMT
  if (energy < PhotonEnergyQE[0] ||
      energy > PhotonEnergyQE[nEntriesQE - 1])
    qe = 0.;

  else
  {
    for (int i = 0; i < (nEntriesQE - 1); i++)
    {
      if (energy >= PhotonEnergyQE[i] &&
          energy < PhotonEnergyQE[i + 1])
      {
        qe = QE[i];
        break;
      }
    }
  }
  // G4cout << "QE:" << qe << G4endl;

  // normalize QE (according to PMT spec sheet)
  //// QE peak = 27.76% due to HAMAMATSU handbook
  //// QE peak = 19% due to PMT spec sheet
  qe = (19. / 27.76) * qe;

  // Change the QE to manage the p.e. of MC is same as one of DATA.
  // Current DATA/MC ratio from cosmic test is about 0.7
  //
  // qe = qe * 0.7;

#if 0
// Transmittion at PMT window (Glass)
  // -> not consider here. Currectly, need to set the transmission 
  // as refractive index & absorption length of the glass.
  G4double trans = 0.;
  if( energy<PhotonEnergyTPMTWindow[0] )
    trans = 1.;

  else if( energy>PhotonEnergyTPMTWindow[nEntriesTPMTWindow-1] )
    trans = 0.;

  else {
    for(int i=0; i<(nEntriesTPMTWindow-1); i++) {
      if( energy>=PhotonEnergyTPMTWindow[i] &&
          energy<PhotonEnergyTPMTWindow[i+1] ) {
        trans = TransPMTWindow[i];
        break;
      }
    }
  }
  //G4cout << "Trans:" << trans << G4endl;

  qe = qe*trans;
#endif

  return qe;
}
