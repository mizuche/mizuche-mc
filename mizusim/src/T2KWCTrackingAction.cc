#include "T2KWCTrackingAction.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4RunManager.hh"
#include "G4Track.hh"
#include "G4TrackingManager.hh"

#include "T2KWCGeometry.hh"
#include "T2KWCRunAction.hh"

#include <iostream>
#include <math.h>

//
T2KWCTrackingAction::T2KWCTrackingAction(GeneralCard *gcard0)
{
  simpart = new T2KWCSimParticle();
  gcard = gcard0;

  n_added_primary = 0;
}

T2KWCTrackingAction::~T2KWCTrackingAction()
{
  if (simpart)
    delete simpart;
}

//
void T2KWCTrackingAction::PreUserTrackingAction(const G4Track *aTrack)
{
  G4int track_id_tmp = aTrack->GetTrackID();
  G4int parent_id = aTrack->GetParentID();

  // Store only primary generated particle
  if (parent_id == 0)
  /*
    // Store all generated electron from muon (for cosmic test)
    if( parent_id==1 &&
        aTrack->GetDefinition()!=G4OpticalPhoton::OpticalPhotonDefinition() )
  */
  {

    G4int vec_size = track_id.size();
    G4int flag = 0;

    for (int i = 0; i < vec_size; i++)
    {
      if (track_id_tmp == track_id[i])
        flag = 1;
    }

    if (flag == 0)
    {
      track_id.push_back(track_id_tmp);
      init_pos.push_back(aTrack->GetPosition());
      init_mom.push_back(aTrack->GetMomentum());

      n_added_primary++;
    }
  }
}

//
void T2KWCTrackingAction::PostUserTrackingAction(const G4Track *aTrack)
{
  G4int parent_id = aTrack->GetParentID();

  // Store only primary generated particle
  if (parent_id == 0)
  /*
    // Store all generated electron from muon (for cosmic test)
    if( parent_id==1 &&
        aTrack->GetDefinition()!=G4OpticalPhoton::OpticalPhotonDefinition() )
  */
  {

    // select only process-ended track
    if (aTrack->GetTrackStatus() == fStopAndKill)
    {

      G4int vec_size = track_id.size();
      for (int i = 0; i < vec_size; i++)
      {
        if (track_id[i] == aTrack->GetTrackID())
        {
          AddSimParticle(i, aTrack);
          n_added_primary--;
        }
      }
    }

    if (n_added_primary == 0)
    {
      track_id.clear();
      init_pos.clear();
      init_mom.clear();
    }
  }
}

void T2KWCTrackingAction::AddSimParticle(G4int index, const G4Track *aTrack)
{
  //
  G4int pid = aTrack->GetDefinition()->GetPDGEncoding();
  G4ThreeVector init_pos_tmp = init_pos[index];
  G4ThreeVector init_mom_tmp = init_mom[index];
  G4ThreeVector final_pos_tmp = aTrack->GetPosition();

  G4double absmom = sqrt(init_mom_tmp(0) * init_mom_tmp(0) +
                         init_mom_tmp(1) * init_mom_tmp(1) +
                         init_mom_tmp(2) * init_mom_tmp(2)) /
                    MeV;

  //
  simpart->Clear("C");
  simpart->SetPID(pid);
  simpart->SetAbsmom(absmom);
  simpart->SetDir(init_mom_tmp(0) / absmom, init_mom_tmp(1) / absmom, init_mom_tmp(2) / absmom);
  simpart->SetIpos(init_pos_tmp(0) / cm, init_pos_tmp(1) / cm, init_pos_tmp(2) / cm);
  simpart->SetFpos(final_pos_tmp(0) / cm, final_pos_tmp(1) / cm, final_pos_tmp(2) / cm);
  simpart->SetIflag(GetPosFlag(init_pos_tmp));
  simpart->SetFflag(GetPosFlag(final_pos_tmp));

  G4RunManager *runMgr = G4RunManager::GetRunManager();
  T2KWCRunAction *runAction = (T2KWCRunAction *)runMgr->GetUserRunAction();
  runAction->GetEventClass()->AddSimParticle(simpart);
}
