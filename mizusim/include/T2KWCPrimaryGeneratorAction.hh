#ifndef T2KWCPrimaryGeneratorAction_h
#define T2KWCPrimaryGeneratorAction_h 1

#include "G4ThreeVector.hh"
#include "G4VUserPrimaryGeneratorAction.hh"

#include "T2KWCInputCard.hh"
#include "T2KWCNeut.hh"

#include "T2KWCData.h"

#include "globals.hh"

class G4ParticleGun;
class G4Event;
class T2KWCPrimaryGeneratorMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class T2KWCPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  T2KWCPrimaryGeneratorAction(GeneralCard *);
  T2KWCPrimaryGeneratorAction(GeneralCard *, T2KWCNeut *);
  ~T2KWCPrimaryGeneratorAction();

public:
  void SetPrimaryParticles_Single(G4Event *);
  void SetPrimaryParticles_Neut(G4Event *);
  void GeneratePrimaries(G4Event *);

  void SetOptPhotonPolar();
  void SetOptPhotonPolar(G4double);

private:
  G4ParticleGun *particleGun;
  T2KWCPrimaryGeneratorMessenger *gunMessenger;

  T2KWCNeut *neut;
  GeneralCard *gcard;

  T2KWCSimVertex *simver;
  T2KWCNeutInfo *neutinfo;

  G4ThreeVector vertex_neutint;

  G4int IsNeutInt();
  void UniformVertex_Tank(G4ThreeVector &);
  void UniformVertex_FV(G4ThreeVector &);
  void UniformVertex_OV(G4ThreeVector &);
  void IsotropicDirect(G4ThreeVector &);
  void SelectGenParticle(G4Event *);
  void ReadOneEvent();
  G4int DefineVertex(G4ThreeVector &);
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif /*T2KWCPrimaryGeneratorAction_h*/
