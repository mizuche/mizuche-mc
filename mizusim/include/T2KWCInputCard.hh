#ifndef T2KWC_INPUT_CARD_H
#define T2KWC_INPUT_CARD_H 1

#include "globals.hh"

typedef struct
{
    int nevent;   // number of events
    int randset;  // random seed setting
    long seed;    // init random seed
    int phylist;  // Physics List
    int fvwater;  // 0:without water in FV / 1:with water in FV
    int detector; // 0:full set up / 1:mini MC
    int runtype;  // Input File Type

    int pid;          // particle (PDG code)
    double vertex[3]; // vertex (cm)
    int univer;
    double direction[3]; // momentum direction
    int isodir;
    double momentum; // absolute momentum (MeV/c)

    int dspvtx;
    int dspneut;
    int dsphit;
    int dspstck;
    int phyver;

} GeneralCard;

class T2KWCInputCard
{
public:
    T2KWCInputCard(const char *fname);
    ~T2KWCInputCard();

    inline GeneralCard *GetGeneralCard() { return &gcard; };
    void PrintAll();
    void SetSeed(long);

private:
    GeneralCard gcard;
    void PrintGeneral();
};

#endif
