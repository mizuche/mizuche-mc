#ifndef T2KWCNEUT_h
#define T2KWCNEUT_h 1

#include "globals.hh"

#include "TFile.h"
#include "TTree.h"

#define NVEC 100
#define NG 20

//
typedef struct
{
	int mode;
	int npart;
	int pid[NVEC];
	float absmom[NVEC];
	float mom[NVEC][3];
} PrimaryState;

//
typedef struct
{
	int npart;
	int pid[NVEC];	 // paritcle id
	int ppid[NVEC];	 // parent particle id
	int icrnv[NVEC]; // Tracking flag (set to 0 if the particle was interacted,absorbed or dissappeared)
	int iflgv[NVEC]; // Interaction code (if the particle already interacted with the others, this flag set)
	float absmom[NVEC];
	float mom[NVEC][3];
	float vertex[3];
} FinalState;

//
typedef struct
{
	int ipart;
	float pos[3];
	float vec[3];
	float absmom;
} PrimaryProton;

//
typedef struct
{
	int ng;			  // # of ancestor particles
	float gpx[NG];	  // directional x-momentum
	float gpy[NG];	  // directional y-momentum
	float gpz[NG];	  // directional z-momentum
	float gcosbm[NG]; // angle between ancestors and beam
	float gvx[NG];	  // vertex x-position
	float gvy[NG];	  // vertex y-position
	float gvz[NG];	  // vertex z-position
	int gpid[NG];	  // particle id of ancestors (GEANT3)
	int gmec[NG];	  // production mechanism
} AncestorParticle;

//
typedef struct
{
	float absmom;
	float vertex[3];
	float dir[3];
	float CosBeam;
} ParentVector;

//
typedef struct
{
	float energy;
	int pid;
	int production_mode;
	ParentVector parent_decay;
	float norm;
	int nvtx0;
	ParentVector parent_production;
	float rnu;
	float xnu;
	float ynu;
	float dir[3];
	int idfd;
	PrimaryProton proton;
	AncestorParticle ancestor;
} NeutVector;

//
typedef struct
{
	float crsnenergy;
	float totcrsne; // total cross section
} NeutInteraction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
class T2KWCNeut
{
public:
	T2KWCNeut(const char *input_neutfile);
	~T2KWCNeut();

	void InitTree();
	long GetTotalEvents();
	int ReadEvent(G4int verbose = 0);
	void PrintAll();

public:
	long nevents;	 // total number of event
	int ievent;		 // event number from neut file
	long read_event; // # of read event from neut file
	PrimaryState primary;
	FinalState final;
	NeutVector neutvec;
	NeutInteraction neutint;
	AncestorParticle ances;

private:
	TFile *neutfile;
	TTree *tree;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
