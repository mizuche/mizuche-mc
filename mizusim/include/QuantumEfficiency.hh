#ifndef QuantumEfficiency_h
#define QuantumEfficiency_h 1

#include "globals.hh"

G4double QuantumEfficiency(G4double energy);

#endif /*QuantumEfficiency_h*/
