#ifndef T2KWCTrackingAction_h
#define T2KWCTrackingAction_h 1

#include <vector>

#include "G4ThreeVector.hh"
#include "G4UserTrackingAction.hh"

#include "T2KWCData.h"
#include "T2KWCInputCard.hh"

class T2KWCTrackingAction : public G4UserTrackingAction
{

public:
  T2KWCTrackingAction(GeneralCard *);
  virtual ~T2KWCTrackingAction();

  virtual void PreUserTrackingAction(const G4Track *);
  virtual void PostUserTrackingAction(const G4Track *);

private:
  T2KWCSimParticle *simpart;
  GeneralCard *gcard;

  std::vector<G4int> track_id;
  std::vector<G4ThreeVector> init_pos;
  std::vector<G4ThreeVector> init_mom;

  void AddSimParticle(G4int, const G4Track *);

  G4int n_added_primary;
};

#endif
