//   **********************************************
//   *        T2KWCOpticalHit.hh
//   **********************************************
//
//    Classe defining an optical photon hit in the photomultiplier
//
#ifndef T2KWCOpticalHit_h
#define T2KWCOpticalHit_h 1

#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "G4VHit.hh"

class T2KWCOpticalHit : public G4VHit
{
public:
  T2KWCOpticalHit();
  T2KWCOpticalHit(G4int, G4ThreeVector, G4double);
  T2KWCOpticalHit(const T2KWCOpticalHit &);
  ~T2KWCOpticalHit();

  const T2KWCOpticalHit &operator=(const T2KWCOpticalHit &);
  int operator==(const T2KWCOpticalHit &) const;
  int CompareID(const T2KWCOpticalHit right);

  inline void *operator new(size_t);
  inline void operator delete(void *);

  inline G4int PMTID() { return pmtid; }
  inline G4int NHits() { return nhits; }
  inline G4ThreeVector PMTPos() { return pmtpos; }
  inline G4double HitTime() { return hittime; }

  void AddHit(const T2KWCOpticalHit right);

  void Draw();
  void Print();

private:
  G4int pmtid;
  G4int nhits;
  G4ThreeVector pmtpos; //[mm]
  G4double hittime;     // [nsec]

public:
  // Set functions to store information on hits
  //  inline void SetEnergy(G4double fEn)   {fPhotEne= fEn;}
  // inline void SetPosition(G4ThreeVector xyz) {fPhotPos = xyz;}

  // Get functions to acess information on hits
  //  inline G4double      GetEnergy()    {return fPhotEne; }
  //  inline G4ThreeVector GetPosition()  {return fPhotPos; }
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

typedef G4THitsCollection<T2KWCOpticalHit> T2KWCOpticalHitsCollection;

extern G4Allocator<T2KWCOpticalHit> T2KWCOpticalHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void *T2KWCOpticalHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *)T2KWCOpticalHitAllocator.MallocSingle();
  return aHit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

inline void T2KWCOpticalHit::operator delete(void *aHit)
{
  T2KWCOpticalHitAllocator.FreeSingle((T2KWCOpticalHit *)aHit);
}

#endif
