#ifndef T2KWCOpticalPhysics_h
#define T2KWCOpticalPhysics_h 1

#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

class G4Cerenkov;
class G4OpAbsorption;
class G4OpRayleigh;
class G4OpBoundaryProcess;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class T2KWCOpticalPhysics : public G4VPhysicsConstructor
{
public:
  T2KWCOpticalPhysics(const G4String &name = "OpticalPhoton", G4int ver = 0);
  ~T2KWCOpticalPhysics();

public:
  void ConstructParticle();
  void ConstructProcess();

  void SetCuts();

  void ConstructBosons();

  void ConstructOp();

  void SetNbOfPhotonsCerenkov(G4int);

private:
  // optical photon
  G4Cerenkov *theCerenkovProcess;
  G4OpAbsorption *theAbsorptionProcess;
  G4OpRayleigh *theRayleighScatteringProcess;
  G4OpBoundaryProcess *theBoundaryProcess;

  G4int verbose_level;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
