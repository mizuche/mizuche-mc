#ifndef T2KWCGEOMETRY_H
#define T2KWCGEOMETRY_H 1

#include "G4ThreeVector.hh"
#include "globals.hh"

#include <math.h>

const G4double expHall_x = 1. * m;
const G4double expHall_y = 1. * m;
const G4double expHall_z = 1. * m;

const G4double x_offset = 0. * m;
const G4double y_offset = 0. * m;

///////////////////////////////////////
// Full MC Geometray
const G4double tank_r = 0.70 * m;
const G4double tank_z = 1.6 * m;
const G4double fv_r = 0.40 * m;
const G4double fv_z = 1. * m;
const G4double half_fv_z = 0.50 * m; // for half-FV
const G4double PMTthickness = 1. * cm;
const G4double PMTradius = 35 * mm;
const G4double PMTGap = 24. * cm;
const G4double fvbound_r = 5 * mm;
const G4double fvbound_z = 8 * mm;

const G4int nz = 6;
const G4int nr = 18;

const G4double pos_cap[28][2] = {
    {-120. * mm, -600. * mm},
    {120. * mm, -600. * mm},
    {-339.02 * mm, -501.88 * mm},
    {339.02 * mm, -501.88 * mm},
    {-501.88 * mm, -339.02 * mm},
    {-145.45 * mm, -360. * mm},
    {145.45 * mm, -360. * mm},
    {501.88 * mm, -339.02 * mm},
    {-360. * mm, -145.45 * mm},
    {360. * mm, -145.45 * mm},
    {-600. * mm, -120. * mm},
    {-120. * mm, -120. * mm},
    {120. * mm, -120. * mm},
    {600. * mm, -120. * mm},
    {-600. * mm, 120. * mm},
    {-120. * mm, 120. * mm},
    {120. * mm, 120. * mm},
    {600. * mm, 120. * mm},
    {-360. * mm, 145.45 * mm},
    {360. * mm, 145.45 * mm},
    {-501.88 * mm, 339.02 * mm},
    {-145.45 * mm, 360. * mm},
    {145.45 * mm, 360. * mm},
    {501.88 * mm, 339.02 * mm},
    {-339.02 * mm, 501.88 * mm},
    {339.02 * mm, 501.88 * mm},
    {-120. * mm, 600. * mm},
    {120. * mm, 600. * mm},
};

////////////////////////////////////////////
// MINI MC Geometry
const G4double tank_mini_r = 5. * cm;
const G4double tank_mini_z = 2. * cm; // half length of tank(water region)
// const G4double acry_mini_z = 0.5*cm; // half length
const G4double acry_mini_z = 0.25 * cm; // half length
const G4double air_mini_z = 0. * cm;
// const G4double air_mini_z = 0.5*cm; // half length, for acrylic measurement
const G4double PMTthickness_mini = 0.1 * cm; // total length
///////////////////////////////////////////

//
#define INFV 2
#define OUTTANK 0
#define INOV 1

G4int GetPosFlag(G4ThreeVector pos);

#endif
