#ifndef T2KWCPhysicsListMessenger_h
#define T2KWCPhysicsListMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class T2KWCPhysicsList;
class G4UIdirectory;
class G4UIcmdWithAnInteger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class T2KWCPhysicsListMessenger : public G4UImessenger
{
public:
  T2KWCPhysicsListMessenger(T2KWCPhysicsList *);
  ~T2KWCPhysicsListMessenger();

  void SetNewValue(G4UIcommand *, G4String);

private:
  T2KWCPhysicsList *pPhysicsList;

  G4UIdirectory *N06Dir;
  G4UIdirectory *physDir;
  G4UIcmdWithAnInteger *verboseCmd;
  G4UIcmdWithAnInteger *cerenkovCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
