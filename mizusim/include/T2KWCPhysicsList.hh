#ifndef T2KWCPhysicsList_h
#define T2KWCPhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

class G4Cerenkov;
class G4OpAbsorption;
class G4OpRayleigh;
class G4OpBoundaryProcess;

class T2KWCPhysicsListMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class T2KWCPhysicsList : public G4VUserPhysicsList
{
public:
  T2KWCPhysicsList(G4int ver = 0);
  ~T2KWCPhysicsList();

public:
  void ConstructParticle();
  void ConstructProcess();

  void SetCuts();

  // these methods Construct particles
  void ConstructBosons();
  void ConstructLeptons();
  void ConstructMesons();
  void ConstructBaryons();

  // these methods Construct physics processes and register them
  void ConstructGeneral();
  void ConstructEM();
  void ConstructOp();

  // for the Messenger
  void SetVerbose(G4int);
  void SetNbOfPhotonsCerenkov(G4int);

private:
  // optical photon
  G4Cerenkov *theCerenkovProcess;
  G4OpAbsorption *theAbsorptionProcess;
  G4OpRayleigh *theRayleighScatteringProcess;
  G4OpBoundaryProcess *theBoundaryProcess;

  T2KWCPhysicsListMessenger *pMessenger;

  G4int verbose_level;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif /* T2KWCPhysicsList_h */
