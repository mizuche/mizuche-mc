#ifndef T2KWCEventAction_h
#define T2KWCEventAction_h 1

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "G4ThreeVector.hh"
#include "G4UserEventAction.hh"
#include "globals.hh"

#include "T2KWCData.h"
#include "T2KWCInputCard.hh"

//
class G4HCofThisEvent;

//
class T2KWCEventAction : public G4UserEventAction
{
public:
  T2KWCEventAction(GeneralCard *);
  ~T2KWCEventAction();

public:
  void BeginOfEventAction(const G4Event *);
  void EndOfEventAction(const G4Event *);

  G4int GetEvtNumb() { return evtNb; };

private:
  G4int evtNb;
  G4int OpticalHitsCollID;

  T2KWCPMTHit *pmthit;
  GeneralCard *gcard;

private:
  void DefineVis(const G4Event *);
};

#endif
