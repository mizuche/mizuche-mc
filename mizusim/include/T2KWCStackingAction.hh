#ifndef T2KWCStackingAction_H
#define T2KWCStackingAction_H 1

#include <fstream>
#include <iostream>
#include <vector>

#include "G4UserStackingAction.hh"
#include "T2KWCInputCard.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class T2KWCStackingAction : public G4UserStackingAction
{
public:
  T2KWCStackingAction(GeneralCard *);
  ~T2KWCStackingAction();

public:
  G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track *aTrack);
  void NewStage();
  void PrepareNewEvent();

private:
  G4int photonCounter;
  G4int peCounter;

  std::vector<int> ppid;
  G4double z_vertex;

  G4int dspstck;
  std::ofstream ofs;
  std::ofstream ofs2;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
