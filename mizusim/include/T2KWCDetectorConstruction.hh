#ifndef T2KWCDetectorConstruction_h
#define T2KWCDetectorConstruction_h 1

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

#include "T2KWCInputCard.hh"

class G4SDManager;
class G4VPVParameterisation;
class T2KWCPMTSD;

class T2KWCDetectorConstruction : public G4VUserDetectorConstruction
{
public:
	T2KWCDetectorConstruction(T2KWCInputCard *);
	~T2KWCDetectorConstruction();

public:
	G4VPhysicalVolume *Construct();

private:
	GeneralCard *gcard;

	// Matirial
	G4Material *Air;
	G4Material *Air2; // for mini mizuche
	G4Material *Water;
	G4Material *Acrylic;
	G4Material *SiO2;
	G4Material *B2O3;
	G4Material *Na2O;
	G4Material *Al2O3;
	G4Material *Glass;

	// for pamateta
	G4VPVParameterisation *PMTParam; // pointer to PMT parameterisation
	G4VPhysicalVolume *physiPMT;	 // pointer to the physical Chamber

	// SD
	T2KWCPMTSD *PMTSD;		// pointer to the photomultiplier sensitive detector
	G4SDManager *SDmanager; // Sensitive Detector Manager

	//
	void DefineMaterial();
	void SetTankSurface(G4LogicalVolume *);
	void SetAcrylicSurface(G4LogicalVolume *);
	void SetFVSurface(G4LogicalVolume *);
	void DefineSurface_MINI(G4LogicalVolume *, G4LogicalVolume *);
	void SetWaterProperties();
	void SetAcrylicProperties();
	void SetGlassProperties();
	void SetAir2Properties(); // for mini mizuche
	G4LogicalVolume *SetTank(G4LogicalVolume *);
	G4LogicalVolume *SetTank_MINI(G4LogicalVolume *);
	void SetPMT(G4LogicalVolume *);
	void SetPMT_MINI(G4LogicalVolume *);
	void SetPMTSurface(G4LogicalVolume *);
	void SetEndCapPMT(G4LogicalVolume *, G4LogicalVolume *);
	void SetSidePMT(G4LogicalVolume *, G4LogicalVolume *);

	int iPMT;
};

#endif
