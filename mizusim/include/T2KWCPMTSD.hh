//   **********************************************
//   *        T2KWCPMTSD.hh
//   **********************************************
//
//    Class used to define the Ultra photomultiplier as a sensitive detector.
//    Hits in this sensitive detector are defined in the T2KWCOpticalHit class
//
#ifndef T2KWCPMTSD_h
#define T2KWCPMTSD_h 1

#include "G4VSensitiveDetector.hh"
#include "globals.hh"

class G4Material;
class G4HCofThisEvent;
class G4Step;

#include "T2KWCOpticalHit.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class T2KWCPMTSD : public G4VSensitiveDetector
{
public:
  T2KWCPMTSD(G4String);
  ~T2KWCPMTSD();

  void Initialize(G4HCofThisEvent *);
  G4bool ProcessHits(G4Step *astep, G4TouchableHistory *ROHist);
  void EndOfEvent(G4HCofThisEvent *);

  void clear();
  void DrawAll();
  void PrintAll();

private:
  T2KWCOpticalHitsCollection *OpticalHitsCollection;
};

#endif
