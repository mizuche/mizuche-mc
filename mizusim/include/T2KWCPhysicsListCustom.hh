#ifndef T2KWCPhysicsListCustom_h
#define T2KWCPhysicsListCustom_h 1

#include "CompileTimeConstraints.hh"
#include "G4VModularPhysicsList.hh"
#include "globals.hh"

class T2KWCPhysicsListCustom : public G4VModularPhysicsList
{
public:
  T2KWCPhysicsListCustom(G4int ver = 0);
  virtual ~T2KWCPhysicsListCustom();

public:
  virtual void SetCuts();
};

#endif
