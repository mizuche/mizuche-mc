#ifndef T2KWCRunAction_h
#define T2KWCRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"

#include "T2KWCData.h"
#include "T2KWCInputCard.hh"

// forward declarations
class G4Run;
class T2KWCAnalysisManager;
class T2KWCRunActionMessenger;

class T2KWCRunAction : public G4UserRunAction
{
public:
  T2KWCRunAction(const char *, T2KWCInputCard *);
  ~T2KWCRunAction();

public:
  void BeginOfRunAction(const G4Run *aRun);
  void EndOfRunAction(const G4Run *aRun);

  G4int GetRunNumb() { return runID; };
  G4int GetEventWriteFlag() { return event_write_flag; }

  void MySetRunID(G4int);
  void PrintAll() { evt->Print(); }
  void FillTree() { tree->Fill(); }
  void ClearEvt() { evt->Clear(); }
  void SetEventWriteFlag(G4int i) { event_write_flag = i; }

  inline GeneralCard *GetGeneralCard() { return incard->GetGeneralCard(); }
  inline T2KWCEvent *GetEventClass() { return evt; }

  TH1F *photon;
  TH1F *pe;
  TH1F *n_vertex;

private:
  T2KWCRunActionMessenger *theRunActMessenger;

  G4int saveRndm;
  G4int runID;
  G4int event_write_flag;

  TFile *file;
  TTree *tree;

  T2KWCEvent *evt;
  T2KWCInputCard *incard;
};

#endif
