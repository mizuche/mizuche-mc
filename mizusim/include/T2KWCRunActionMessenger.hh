//   **********************************************
//   *        T2KWCRunActionMessenger.hh
//   **********************************************
//
//    Messenger Class for T2KWCRunAction
//    Allows to set the run ID
//
#ifndef T2KWCRunActionMessenger_h
#define T2KWCRunActionMessenger_h 1

class G4UIdirectory;
class T2KWCRunAction;
#include "G4UImessenger.hh"
#include "globals.hh"

class T2KWCRunActionMessenger : public G4UImessenger
{
public:
  T2KWCRunActionMessenger(T2KWCRunAction *);
  ~T2KWCRunActionMessenger();

public:
  void SetNewValue(G4UIcommand *command, G4String newValues);

private:
  T2KWCRunAction *theRunAction;

private: // commands
  G4UIdirectory *runDirectory;
  G4UIcommand *runIDCmd;
};

#endif
