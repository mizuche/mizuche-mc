#ifndef T2KWCPrimaryGeneratorMessenger_h
#define T2KWCPrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class T2KWCPrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithADoubleAndUnit;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class T2KWCPrimaryGeneratorMessenger : public G4UImessenger
{
public:
  T2KWCPrimaryGeneratorMessenger(T2KWCPrimaryGeneratorAction *);
  ~T2KWCPrimaryGeneratorMessenger();

  void SetNewValue(G4UIcommand *, G4String);

private:
  T2KWCPrimaryGeneratorAction *T2KWCAction;
  G4UIdirectory *gunDir;
  G4UIcmdWithADoubleAndUnit *polarCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
