#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UItcsh.hh"
#include "G4UIterminal.hh"

#include "G4ios.hh"

#include "QGSP_BERT.hh"
#include "T2KWCDetectorConstruction.hh"
#include "T2KWCEventAction.hh"
#include "T2KWCInputCard.hh"
#include "T2KWCPhysicsList.hh"
#include "T2KWCPhysicsListCustom.hh"
#include "T2KWCPrimaryGeneratorAction.hh"
#include "T2KWCRunAction.hh"
#include "T2KWCStackingAction.hh"
#include "T2KWCTrackingAction.hh"

#include "Randomize.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

// __________________________________________________
void PrintUsage()
{
    G4cerr << "Usage:\tT2KWC [hcoirm] inputcard" << G4endl;
    G4cerr << "Options:" << G4endl;
    G4cerr << "\t-c\tset input card file (must)" << G4endl;
    G4cerr << "\t-o\tset output filename (default: T2KWC_output.root)" << G4endl;
    G4cerr << "\t-m\tset visual macro (default: vis.mac)" << G4endl;
    G4cerr << "\t-i\tset input neut file" << G4endl;
    G4cerr << "\t-r\tset input random seed (in 0-900000000)" << G4endl;
    exit(1);
}

// __________________________________________________
int main(int argc, char **argv)
{
    int NumOfEvents = 0;

    G4String input_card = "";
    G4String output_file = "T2KWC_output.root";
    G4String input_neut = "";
    long input_rand_seed = -1111;
    G4String macro = "vis.mac";

    int tmp = -1;
    while ((tmp = getopt(argc, argv, "hc:o:i:r:m:")) != -1)
    {
        switch (tmp)
        {

        case 'c':
            input_card = optarg;
            break;

        case 'o':
            output_file = optarg;
            break;

        case 'i':
            input_neut = optarg;
            break;

        case 'r':
            input_rand_seed = atol(optarg);
            break;

        case 'm':
            macro = optarg;
            break;

        case 'h':
        default:
            PrintUsage();
        }
    }

    if (input_card == "")
    {
        G4cerr << "Error:\tNeed input card file!" << G4endl;
        PrintUsage();
    }

    // Read Input card
    T2KWCInputCard *InputCard = new T2KWCInputCard(input_card.c_str());
    InputCard->PrintAll();

    GeneralCard *gcard = InputCard->GetGeneralCard();
    if (gcard->randset == 1)
    {
        if (input_rand_seed == -1111)
            PrintUsage();

        InputCard->SetSeed(input_rand_seed);
    }

    // User Verbose output class
    /*
      G4VSteppingVerbose* verbosity = new ExN06SteppingVerbose;
      G4VSteppingVerbose::SetInstance(verbosity);
    */

    // Run manager
    G4RunManager *runManager = new G4RunManager;

    // UserInitialization classes - mandatory
    G4VUserDetectorConstruction *detector = new T2KWCDetectorConstruction(InputCard);
    runManager->SetUserInitialization(detector);

    // Physics process
    G4VUserPhysicsList *physics = 0;
    switch (gcard->phylist)
    {

    case 0:
        physics = new T2KWCPhysicsList(gcard->phyver);
        G4cout << "Physics process : only Cerenkov process" << G4endl;
        break;

    case 1:
        physics = new T2KWCPhysicsListCustom(gcard->phyver);
        G4cout << "Physics process : Cerenkov + hadron process" << G4endl;
        break;

    case 2:
        physics = new QGSP_BERT();
        G4cout << "Physics process : QGSP_BERT" << G4endl;
        break;

    default:
        G4cerr << "Error:\tInvalid Physics List !!!" << G4endl;
        exit(-1);
    }
    // Set physics process initialization
    runManager->SetUserInitialization(physics);

#ifdef G4VIS_USE
    // visualization manager
    G4VisManager *visManager = new G4VisExecutive;
    visManager->Initialize();
#endif

    fprintf(stderr, "[%s]\tL:%d\n", __FILE__, __LINE__);
    // UserAction classes - optional
    T2KWCRunAction *RunAct = new T2KWCRunAction(output_file.c_str(), InputCard);
    runManager->SetUserAction(RunAct);

    fprintf(stderr, "[%s]\tL:%d\n", __FILE__, __LINE__);
    // Select Run type
    T2KWCPrimaryGeneratorAction *PrimGenAct = 0;
    T2KWCNeut *neut = 0;
    switch (gcard->runtype)
    {

    case 0: // Single particle
        PrimGenAct = new T2KWCPrimaryGeneratorAction(gcard);
        break;

    case 1: // NEUT file
        if (input_neut == "")
            PrintUsage();
        neut = new T2KWCNeut(input_neut.c_str());
        PrimGenAct = new T2KWCPrimaryGeneratorAction(gcard, neut);
        break;

    default:
        G4cerr << "Invalid Run Type !!!" << G4endl;
        exit(-1);
    }
    runManager->SetUserAction(PrimGenAct);
    // Set user action class
    //  Event Action
    fprintf(stderr, "[%s]\tL:%d\tSet Event Action\n", __FILE__, __LINE__);
    runManager->SetUserAction(new T2KWCEventAction(gcard));

    // Tracking Action
    fprintf(stderr, "[%s]\tL:%d\tSet Tracking Action\n", __FILE__, __LINE__);
    runManager->SetUserAction(new T2KWCTrackingAction(gcard));

    // Stacking Action
    fprintf(stderr, "[%s]\tL:%d\tSet Stacking Action\n", __FILE__, __LINE__);
    runManager->SetUserAction(new T2KWCStackingAction(gcard));

    // Initialize G4 kernel
    fprintf(stderr, "[%s]\tL:%d\tInitialize G4 Kernel\n", __FILE__, __LINE__);
    runManager->Initialize();

    // Get the pointer to the User Interface manager
    fprintf(stderr, "[%s]\tL:%d\tGet UI pointer\n", __FILE__, __LINE__);
    G4UImanager *UI = G4UImanager::GetUIpointer();

    fprintf(stderr, "[%s]\tL:%d\tCheck UI session\n", __FILE__, __LINE__);
    //
    NumOfEvents = gcard->nevent;
    if (NumOfEvents == -1)
    { // Define UI session for interactive mode
        G4UIsession *session = 0;

#ifdef G4UI_USE_TCSH
        session = new G4UIterminal(new G4UItcsh);
#else
        session = new G4UIterminal();
#endif
        G4String command = "/control/execute " + macro;
        UI->ApplyCommand(command);
        session->SessionStart();
        delete session;
    }
    else if (NumOfEvents == 0)
    {
        NumOfEvents = neut->GetTotalEvents();
        runManager->BeamOn(NumOfEvents);
    }
    else
    {
        runManager->BeamOn(NumOfEvents);
    }

    // Job termination
    // Free the store: user actions, physics_list and detector_description are
    //                 owned and deleted by the run manager, so they should not
    //                 be deleted in the main() program !
    // delete visualization system
#ifdef G4VIS_USE
    delete visManager;
#endif
    delete runManager;
    //  delete verbosity;
    return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
