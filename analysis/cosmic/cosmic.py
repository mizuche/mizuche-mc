#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TCanvas
from ROOT import TCanvas, TBox, TGaxis, TLegend
from ROOT import TH1D, TH2D, TGraph, TGraphErrors, TF1
from ROOT import TVectorD, Double

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

## __________________________________________________
class Cosmic(object):
    ## __________________________________________________
    def __init__(self, date, runs, options = None):
        self.dir   = '../exp/cosmic/upk/'
        self.date  = date
        self.runs  = runs
        self.ifns  = []
        for irun in runs:
            ifn = 'upk%d%04d.root' % (int(date), int(irun))
            ifn = os.path.join(self.dir, ifn)
            self.ifns.append(ifn)
        self.cut = options.cut

    ## __________________________________________________
    def stop(self):
        gSystem.ProcessEvents()
        ans = raw_input('Stopped. Press "q" to quit > ')
        if ans in ['q', 'Q']:
            sys.exit(-1)
        elif ans in ['.', '.q', '.Q']:
            return -1

    ## __________________________________________________
    def control(self, ientry):
        gSystem.ProcessEvents()
        ans = raw_input('Entry # %5d    Input entry# or [n]/p/q > ' % ientry)
        if ans in ['q', 'Q']  : sys.exit(-1)
        elif ans in ['p', 'P']: return ientry-1
        elif ans in ['n', 'N', '']: return ientry+1
        else: return int(ans)    
    
    ## __________________________________________________
    def read(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.exit(-1)

        print '[%s]\tRead "%s"' % ('read', ifn)
        fin = TFile(ifn, 'read')
        return fin
    
    ## __________________________________________________
    def waveform(self, ifn):
        print '[%s]\tDraw waveforms in "%s".' % ('waveform', ifn)
        fin = self.read(ifn)

        umode = fin.Get('umode')
        umode.GetEntry(0)
        mode = umode.umode

        print '[%s]\tChecking unpacked mode : %d' % ('waveform', mode)
        if not mode == 1:
            sys.stderr.write('Error:\t"%s" does not have waveform data.\n' % ifn)
            sys.stderr.write('\tQuit. Bye Bye!!!\n')
            sys.exit(-1)
        
        upk = fin.Get('upk')
        unpack = T2KWCUnpack()
        br = upk.GetBranch('upk.')
        br.SetAddress(unpack)
        upk.SetBranchAddress('upk.', unpack)

        c = TCanvas('cWaveform', 'check waveforms', 800, 800)
        c.Divide(1, 2)

        nentries = upk.GetEntries()
        ientry = 0
        while(ientry < nentries):
            ## PMTSUM
            upk.Project('', 'fadc_pmtsum:Iteration$', 'Entry$ == %d' % ientry)
            xp = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yp = TVectorD(upk.GetSelectedRows(), upk.GetV1())
            
            gp = TGraph(xp, yp)
            gp.SetTitle('PMTSUM (%4d/%4d)' % (ientry, nentries))
            gp.GetXaxis().SetTitle('Point [4ns/pt]')
            gp.GetYaxis().SetTitle('PMTSUM [mV]')

            ## HITSUM
            upk.Project('', 'fadc_hitsum:Iteration$', 'Entry$ == %d' % ientry)
            xh = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yh = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gh = TGraph(xh, yh)
            gh.SetTitle('HITSUM (%4d/%4d)' % (ientry, nentries))
            gh.GetXaxis().SetTitle('Point [4ns/pt]')
            gh.GetYaxis().SetTitle('HITSUM [mV]')

            ## Bunch timing and HITSUM height
            upk.Project('', '-fadc_height:fadc_nsec/4', 'Entry$ == %d' % ientry)
            xb = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yb = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gb = TGraph(xb, yb)
            gb.SetTitle('Hit Timing (%4d/%4d)' % (ientry, nentries))
            gb.GetXaxis().SetTitle('Timing [nsec]')
            gb.GetYaxis().SetTitle('Height [mV]')
            gb.SetLineColor(2)
            gb.SetMarkerColor(2)
            gb.SetMarkerStyle(34)
            gb.SetMarkerSize(4)

            ### To draw integral region for PMTSUM
            nb = gb.GetN()
            int_p = []
            win_p = []
            ymin = gp.GetYaxis().GetXmin()
            ymax = gp.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = int(x) - 18
                xmax = int(x) + 9
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3004)
                win_p.append(box)
                integ = 0.
                # for i in range(xmin, xmax+1):
                #     integ += upk.upk.GetFadcPmtsum(i)
                # int_p.append(integ)

            int_p2 = []
            win_p2 = []
            ymin = gp.GetYaxis().GetXmin()
            ymax = gp.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = int(x) - 18
                xmax = int(x) + 57
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(3)
                box.SetLineColor(3)
                box.SetLineWidth(3)
                box.SetLineStyle(2)
                box.SetFillStyle(3005)
                win_p2.append(box)
                integ = 0.
                # for i in range(xmin, xmax+1):
                #     integ += upk.upk.GetFadcPmtsum(i)
                # int_p2.append(integ)

            for integ in int_p:
                print 'int_p  : %.2f' % (- integ * 4 / 50 / 0.088)
            for integ in int_p2:
                print 'int_p2 : %.2f' % (- integ * 4 / 50 / 0.088)

            ### To draw integral region for HITSUM
            win_h = []
            ymin = gh.GetYaxis().GetXmin()
            ymax = gh.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = x + 6
                xmax = x + 45
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3004)
                win_h.append(box)                

            ### Print hit summary
            print '\tEntry# %4d' % ientry
            upk.GetEntry(ientry)
            upk.upk.Print()            

            run   = upk.upk.GetRunNum()
            spill = upk.upk.GetSpillNum()
            fadc_ntrg     = upk.upk.GetFadcNtrg()
            fadc_nhits    = upk.upk.GetFadcNhits()
            fadc_ncharges = upk.upk.GetFadcNcharges()
            fadc_npes     = upk.upk.GetFadcNpes()
            atm_ntrg     = upk.upk.GetAtmNtrg()
            atm_nhits    = upk.upk.GetAtmNhits()
            atm_ncharges = upk.upk.GetAtmNcharges()
            atm_npes     = upk.upk.GetAtmNpes()

            title = 'Run#%07d Spill#%6d' % (run, spill)
            lp = TLegend(0.6, 0.1, 0.9, 0.3, title)
            lp.SetFillColor(0)
            label = 'FADC:%8.2f [pC] / %8.2f [p.e.]' % (fadc_ncharges, fadc_npes)
            lp.AddEntry(gp, label, 'lp')
            label = 'ATM :%8.2f [pC] / %8.2f [p.e.]' % (atm_ncharges, atm_npes)
            lp.AddEntry(gp, label, 'lp')
            
            lh = TLegend(0.6, 0.1, 0.9, 0.3, title)
            lh.SetFillColor(0)
            label = 'FADC:%8.2f [hits] / %8.2f [trg]' % (fadc_nhits, fadc_ntrg)
            lh.AddEntry(gp, label, 'lp')
            label = 'ATM :%8.2f [hits] / %8.2f [trg]' % (atm_nhits, atm_ntrg)
            lh.AddEntry(gp, label, 'lp')
            
            c.cd(1)
            gp.Draw('apl')
            for box in win_p:
                box.Draw()
            for box in win_p2:
                box.Draw()
            lp.Draw()
            c.cd(2)
            gh.Draw('apl')
            if gb.GetN() > 0:
                gb.Draw('psame')
            for box in win_h:
                box.Draw()
            lh.Draw()
            c.Update()

            ientry = self.control(ientry)
            if ientry < 0 or ientry > nentries - 1:
                sys.stderr.write('Error:\tEntry #%d out of range. ' % ientry)
                c.Clear()
                c.Divide(1, 2)
                c.Update()
                self.stop()
                ientry = 0
        return fin

    ## __________________________________________________
    def histNcharges(self, ifn, draw = False):
        fin = self.read(ifn)
        
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()
        
        cut = self.cut

        xmin, xmax, xbin = 0, 1000, 1000
        name = 'hAtmNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hAtm = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_ncharges', cut)

        name = 'hFadcNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_ncharges/0.09', cut)

        name = 'hDiffNcharges'
        title = '%s;DiffNcharges (ATM - FADC) [pC];Entries [#]' % name
        hDiff = TH1D(name, title, 200, -100, 100)
        upk.Project(hDiff.GetName(), "atm_ncharges - fadc_ncharges/0.088", cut);

        if draw:
            name = 'cHistNcharges'
            c = TCanvas(name, name, 1200, 600)
            c.Divide(2, 1)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()
        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNpes(self, ifn, draw = False):
        fin = self.read(ifn)
        
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()

        cut = self.cut

        xmin, xmax, xbin = 0, 1000, 1000
        name = 'hAtmNpes'
        title = '%s;Npes [p.e.];Entries [#]' % name
        hAtm = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_npes', cut)

        name = 'hFadcNpes'
        title = '%s;Npes [p.e.];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_npes/0.088', cut)

        name = 'hDiffNpes'
        title = '%s;DiffNpes (ATM - FADC) [p.e.];Entries [#]' % name
        hDiff = TH1D(name, title, 1000, -500, 500)
        upk.Project(hDiff.GetName(), "atm_npes - fadc_npes/0.088", cut);

        if draw:
            name = 'cHistNpes'
            c = TCanvas(name, name, 1200, 600)
            c.Divide(2, 1)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()

        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNhits(self, ifn, draw = False):
        fin = self.read(ifn)
        
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()
        
        cut = self.cut

        xmin, xmax, xbin = 0, 200, 200
        name = 'hAtmNhits'
        title = '%s;Nhits [#];Entries [#]' % name
        hAtm = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_nhits', cut)

        name = 'hFadcNhits'
        title = '%s;Nhits [#];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_nhits', cut)

        name = 'hDiffNhits'
        title = '%s;DiffNhits (ATM - FADC) [#];Entries [#]' % name
        hDiff = TH1D(name, title, 200, -100, 100)
        upk.Project(hDiff.GetName(), "atm_nhits - fadc_nhits", cut);

        if draw:
            name = 'cHistNhits'
            c = TCanvas(name, name, 1200, 600)
            c.Divide(2, 1)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()
        return fin, upk, hAtm, hFadc, hDiff
    
    ## __________________________________________________
    def histNtrg(self, ifn, draw = False):
        fin = self.read(ifn)

        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()

        name = 'hAtmNtrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hAtm = TH1D(name, title, 10, 0, 10)
        upk.Project(hAtm.GetName(), "atm_ntrg");
        
        name = 'hFadcNtrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hFadc = TH1D(name, title, 10, 0, 10)
        upk.Project(hFadc.GetName(), "fadc_ntrg");

        name = 'hDiffNtrg'
        title = '%s;DiffNtrg (ATM - FADC) [#];Entries [#]' % name
        hDiff = TH1D(name, title, 20, -10, 10)
        upk.Project(hDiff.GetName(), "atm_ntrg - fadc_ntrg");
        
        if draw:
            name = 'cHistNtrg'
            c = TCanvas(name, name, 800, 400)
            c.Divide(2, 1)
            c.cd(1).SetLogy()
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw();
            hFadc.SetLineColor(4)
            hFadc.SetFillColor(4)
            hFadc.Draw('same');

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM : %d Entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC: %d Entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2).SetLogy()
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop();
        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def hist(self, item, draw = True):
        items = ['ncharges', 'npes', 'ntrg', 'waveform', 'nhits']
        if not item in items:
            sys.stderr.write('Error:\tGiven item (%s) is not in the list' % item)
            print items
            sys.exit()
        
        ifns = self.ifns
        for ifn in ifns:
            if item == items[0]:
                self.histNcharges(ifn, draw)
            if item == items[1]:
                self.histNpes(ifn, draw)
            if item == items[2]:
                self.histNtrg(ifn, draw)
            if item == items[3]:
                self.waveform(ifn)
            if item == items[4]:
                self.histNhits(ifn, draw)
        return
    
    ## __________________________________________________
    def graphNcharges(self):
        ifns = self.ifns
        gLedvAtm   = TGraphErrors(0)
        gLedvFadc  = TGraphErrors(0)
        gLedvDiff  = TGraphErrors(0)
        gWidthAtm  = TGraphErrors(0)
        gWidthFadc = TGraphErrors(0)
        gWidthDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNcharges(ifn)
            upk.GetEntry(0)
            run    = upk.upk.GetRunNum()
            ledv   = (upk.upk.GetDaqmode() - 10000) / 1000.
            width  = upk.upk.GetFvstat()
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gLedvAtm.GetN()
            gLedvAtm.SetPoint(n, ledv, m_atm)
            gLedvAtm.SetPointError(n, 0, r_atm)
            n = gLedvFadc.GetN()
            gLedvFadc.SetPoint(n, ledv, m_fadc)
            gLedvFadc.SetPointError(n, 0, r_fadc)
            n = gLedvDiff.GetN()
            gLedvDiff.SetPoint(n, ledv, m_diff)
            gLedvDiff.SetPointError(n, 0, r_diff)
            n = gWidthAtm.GetN()
            gWidthAtm.SetPoint(n, width, m_atm)
            gWidthAtm.SetPointError(n, 0, r_atm)
            n = gWidthFadc.GetN()
            gWidthFadc.SetPoint(n, width, m_fadc)
            gWidthFadc.SetPointError(n, 0, r_fadc)
            n = gWidthDiff.GetN()
            gWidthDiff.SetPoint(n, width, m_diff)
            gWidthDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gLedvAtm.SetTitle('LEDV vs Ncharges;LEDV [V];Ncharges [pC]')
        gLedvAtm.SetMarkerColor(2)
        gLedvAtm.SetLineColor(2)
        gLedvAtm.Draw('ap')
        gLedvFadc.SetMarkerColor(4)
        gLedvFadc.SetLineColor(4)
        gLedvFadc.Draw('psame')
        lLedv = TLegend(0.1, 0.7, 0.5, 0.9, 'LEDV vs Ncharges')
        lLedv.SetFillColor(0)
        lLedv.AddEntry(gLedvAtm, 'ATM', 'lp')
        lLedv.AddEntry(gLedvFadc, 'FADC', 'lp')
        lLedv.Draw()
        c.cd(2)
        gLedvDiff.SetTitle('LEDV vs DiffNcharges;LEDV [V];DiffNcharges (ATM - FADC/0.088) [pC]')
        gLedvDiff.SetMarkerColor(3)
        gLedvDiff.SetLineColor(3)
        gLedvDiff.Draw('ap')
        c.cd(3)
        gWidthAtm.SetTitle('Pulse width vs Ncharges;Pulse Width [ns];Ncharges [pC]')
        gWidthAtm.SetMarkerColor(2)
        gWidthAtm.SetLineColor(2)
        gWidthAtm.Draw('ap')
        gWidthFadc.SetMarkerColor(4)
        gWidthFadc.SetLineColor(4)
        gWidthFadc.Draw('psame')
        lWidth = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse width vs Ncharges')
        lWidth.SetFillColor(0)
        lWidth.AddEntry(gWidthAtm, 'ATM', 'lp')
        lWidth.AddEntry(gWidthFadc, 'FADC', 'lp')
        lWidth.Draw()
        c.cd(4)
        gWidthDiff.SetTitle('Pulse width vs DiffNcharges;Pulse Width [ns];DiffNcharges (ATM - FADC/0.088) [pC]')
        gWidthDiff.SetMarkerColor(3)
        gWidthDiff.SetLineColor(3)
        gWidthDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graphNpes(self):
        ifns = self.ifns
        gLedvAtm   = TGraphErrors(0)
        gLedvFadc  = TGraphErrors(0)
        gLedvDiff  = TGraphErrors(0)
        gWidthAtm  = TGraphErrors(0)
        gWidthFadc = TGraphErrors(0)
        gWidthDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNpes(ifn, False)
            upk.GetEntry(0)
            
            run    = upk.upk.GetRunNum()
            ledv   = (upk.upk.GetDaqmode() - 10000) / 1000.
            width  = upk.upk.GetFvstat()
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gLedvAtm.GetN()
            gLedvAtm.SetPoint(n, ledv, m_atm)
            gLedvAtm.SetPointError(n, 0, r_atm)
            n = gLedvFadc.GetN()
            gLedvFadc.SetPoint(n, ledv, m_fadc)
            gLedvFadc.SetPointError(n, 0, r_fadc)
            n = gLedvDiff.GetN()
            gLedvDiff.SetPoint(n, ledv, m_diff)
            gLedvDiff.SetPointError(n, 0, r_diff)
            n = gWidthAtm.GetN()
            gWidthAtm.SetPoint(n, width, m_atm)
            gWidthAtm.SetPointError(n, 0, r_atm)
            n = gWidthFadc.GetN()
            gWidthFadc.SetPoint(n, width, m_fadc)
            gWidthFadc.SetPointError(n, 0, r_fadc)
            n = gWidthDiff.GetN()
            gWidthDiff.SetPoint(n, width, m_diff)
            gWidthDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gLedvAtm.SetTitle('LEDV vs Npes;LEDV [V];Npes [p.e.]')
        gLedvAtm.SetMarkerColor(2)
        gLedvAtm.SetLineColor(2)
        gLedvAtm.Draw('ap')
        gLedvFadc.SetMarkerColor(4)
        gLedvFadc.SetLineColor(4)
        gLedvFadc.Draw('psame')
        lLedv = TLegend(0.1, 0.7, 0.5, 0.9, 'LEDV vs Npes')
        lLedv.SetFillColor(0)
        lLedv.AddEntry(gLedvAtm, 'ATM', 'lp')
        lLedv.AddEntry(gLedvFadc, 'FADC', 'lp')
        lLedv.Draw()
        c.cd(2)
        gLedvDiff.SetTitle('LEDV vs DiffNpes;LEDV [V];DiffNpes (ATM - FADC/0.088) [p.e.]')
        gLedvDiff.SetMarkerColor(3)
        gLedvDiff.SetLineColor(3)
        gLedvDiff.Draw('ap')
        c.cd(3)
        gWidthAtm.SetTitle('Pulse width vs Npes;Pulse Width [ns];Npes [p.e.]')
        gWidthAtm.SetMarkerColor(2)
        gWidthAtm.SetLineColor(2)
        gWidthAtm.Draw('ap')
        gWidthFadc.SetMarkerColor(4)
        gWidthFadc.SetLineColor(4)
        gWidthFadc.Draw('psame')
        lWidth = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse width vs Npes')
        lWidth.SetFillColor(0)
        lWidth.AddEntry(gWidthAtm, 'ATM', 'lp')
        lWidth.AddEntry(gWidthFadc, 'FADC', 'lp')
        lWidth.Draw()
        c.cd(4)
        gWidthDiff.SetTitle('Pulse width vs DiffNpes;Pulse Width [ns];DiffNpes (ATM - FADC/0.088) [p.e.]')
        gWidthDiff.SetMarkerColor(3)
        gWidthDiff.SetLineColor(3)
        gWidthDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graphNtrg(self):
        ifns = self.ifns
        gLedvAtm   = TGraphErrors(0)
        gLedvFadc  = TGraphErrors(0)
        gLedvDiff  = TGraphErrors(0)
        gWidthAtm  = TGraphErrors(0)
        gWidthFadc = TGraphErrors(0)
        gWidthDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNtrg(ifn)
            upk.GetEntry(0)
            run    = upk.upk.GetRunNum()
            ledv   = (upk.upk.GetDaqmode() - 10000) / 1000.
            width  = upk.upk.GetFvstat()
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gLedvAtm.GetN()
            gLedvAtm.SetPoint(n, ledv, m_atm)
            gLedvAtm.SetPointError(n, 0, r_atm)
            n = gLedvFadc.GetN()
            gLedvFadc.SetPoint(n, ledv, m_fadc)
            gLedvFadc.SetPointError(n, 0, r_fadc)
            n = gLedvDiff.GetN()
            gLedvDiff.SetPoint(n, ledv, m_diff)
            gLedvDiff.SetPointError(n, 0, r_diff)
            n = gWidthAtm.GetN()
            gWidthAtm.SetPoint(n, width, m_atm)
            gWidthAtm.SetPointError(n, 0, r_atm)
            n = gWidthFadc.GetN()
            gWidthFadc.SetPoint(n, width, m_fadc)
            gWidthFadc.SetPointError(n, 0, r_fadc)
            n = gWidthDiff.GetN()
            gWidthDiff.SetPoint(n, width, m_diff)
            gWidthDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gLedvAtm.SetTitle('LEDV vs Ntrg;LEDV [V];Ntrg [#]')
        gLedvAtm.SetMarkerColor(2)
        gLedvAtm.SetLineColor(2)
        gLedvAtm.Draw('ap')
        gLedvFadc.SetMarkerColor(4)
        gLedvFadc.SetLineColor(4)
        gLedvFadc.Draw('psame')
        lLedv = TLegend(0.1, 0.7, 0.5, 0.9, 'LEDV vs Ntrg')
        lLedv.SetFillColor(0)
        lLedv.AddEntry(gLedvAtm, 'ATM', 'lp')
        lLedv.AddEntry(gLedvFadc, 'FADC', 'lp')
        lLedv.Draw()
        c.cd(2)
        gLedvDiff.SetTitle('LEDV vs DiffNtrg;LEDV [V];DiffNtrg [#]')
        gLedvDiff.SetMarkerColor(3)
        gLedvDiff.SetLineColor(3)
        gLedvDiff.Draw('ap')
        c.cd(3)
        gWidthAtm.SetTitle('Pulse width vs Ntrg;Pulse Width [ns];Ntrg [#]')
        gWidthAtm.SetMarkerColor(2)
        gWidthAtm.SetLineColor(2)
        gWidthAtm.Draw('ap')
        gWidthFadc.SetMarkerColor(4)
        gWidthFadc.SetLineColor(4)
        gWidthFadc.Draw('psame')
        lWidth = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse width vs Ntrg')
        lWidth.SetFillColor(0)
        lWidth.AddEntry(gWidthAtm, 'ATM', 'lp')
        lWidth.AddEntry(gWidthFadc, 'FADC', 'lp')
        lWidth.Draw()
        c.cd(4)
        gWidthDiff.SetTitle('Pulse width vs DiffNtrg;Pulse Width [ns];DiffNtrg [#]')
        gWidthDiff.SetMarkerColor(3)
        gWidthDiff.SetLineColor(3)
        gWidthDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graph(self, item):
        items = ['ncharges', 'npes', 'ntrg']
        if not item in items:
            sys.stderr.write('Error:\tGiven item (%s) is not in the list' % item)
            print items
            sys.exit()
        
        if item == items[0]:
            self.graphNcharges()
        if item == items[1]:
            self.graphNpes()
        if item == items[2]:
            self.graphNtrg()
        return

    ## __________________________________________________
    def trigger(self, ifn):
        fin = self.read(ifn)

        upk = fin.Get('upk')
        unpack = T2KWCUnpack()
        br = upk.GetBranch('upk.')
        br.SetAddress(unpack)
        upk.SetBranchAddress('upk.', unpack)

        n = upk.GetEntries()

        for i in range(0, n):
            upk.GetEntry(i)
            atm_ntrg = unpack.GetAtmNtrg()
            hit = T2KWCHit()

            if atm_ntrg > 0:
                for j in range(0, atm_ntrg):
                    hit = unpack.GetHit(j)
                    pmt = hit.GetPmt()
                    print '%d' % pmt
    
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [options] dir run_start# run_end#' % sys.argv[0]

    defdir = '../exp/cosmic/upk/'
    defcut = 'atm_ntrg ==1 && fadc_ntrg==1'
    
    parser = OptionParser(usage)
    parser.add_option('-l', '--list',
                      action = 'store_true',
                      dest   = 'ls',
                      help   = 'ls "%s"' % defdir)
    parser.add_option('-i', '--individual',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('-q', '--ncharges',
                      action = 'store_true',
                      dest   = 'ncharges',
                      help   = 'draw ncharges distribution')
    parser.add_option('-p', '--npes',
                      action = 'store_true',
                      dest   = 'npes',
                      help   = 'draw npes distribution')
    parser.add_option('-w', '--waveform',
                      action = 'store_true',
                      dest = 'waveform',
                      help = 'draw waveforms')
    parser.add_option('-t', '--trg',
                      action = 'store_true',
                      dest = 'ntrg',
                      help = 'draw trg')
    parser.add_option('-g', '--graph',
                      action = 'store_true',
                      dest = 'graph',
                      help = 'draw graph')
    parser.add_option('-c', '--cut',
                      dest = 'cut',
                      help = 'set cut [default = "%s"]' % defcut)
    parser.set_defaults(ls         = False,
                        individual = False,
                        cut        = defcut,
                        ncharges   = False,
                        npes       = False,
                        waveform   = False,
                        ntrg       = False,
                        graph      = False)
                        
    
    (options, args) = parser.parse_args()
    
    #print options
    #print len(args), args

    if len(args) < 1:
        if options.ls:
            print 'List files in "%s"' % defdir
            com = 'ls %s' % defdir
            os.system(com)
        else:
            sys.stderr.write('Error:\tWrong number of arguments. (%d)\n' % len(args))
        print usage
        sys.exit(-1)

    date = args[0]
    if options.individual:
        runs = args[1:]
    else:
        srun = int(args[1])
        erun = srun
        if len(args) > 2:
            erun = int(args[2])
        runs = [run for run in range(srun, erun+1)]
    print 'Runs:\t', runs

    c = Cosmic(date, runs, options)

    items = []
    if options.waveform : items.append('waveform')
    if options.ncharges : items.append('ncharges')
    if options.npes     : items.append('npes')
    if options.ntrg     : items.append('ntrg')

    for item in items:
        if options.graph:
            c.graph(item)
        else:
            c.hist(item, True)
        sys.exit(-1)
        
    c.hist('nhits', True)
