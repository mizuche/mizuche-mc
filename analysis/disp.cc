// -*- mode : c++ -*-

//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-11-23 01:55:00+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
//
#ifndef __CINT__
#ifndef ROOT_TApplication
#include "TApplication.h"
#endif

// PUT HEADERS HERE
#include "TSystem.h"
#include "T2KWCEventDisplay.h"
#include "T2KWCUnpack.h"
#include "T2KWCData.h"

#endif

// __________________________________________________
Int_t Usage()
{
    fprintf(stderr, "Usage   : ./disp [htpmr] ifn\n");
    fprintf(stderr, "Options\n");
    fprintf(stderr, "\t-h    : show help\n");
    fprintf(stderr, "\t-t    : specify input tree name [default = 'upk']\n");
    //fprintf(stderr, "\t-p    : print event display     [default = False]\n");
    //fprintf(stderr, "\t-m    : read MC files           [default = False]\n");
    //fprintf(stderr, "\t-r    : specify run#\n");
    return 0;
}

// __________________________________________________
Int_t Control()
{
    gSystem->ProcessEvents();
    fprintf(stderr, "Input entry# or [n]/p/q > ");
    Int_t answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    if (answer == 'q' || answer == 'Q') {
        fprintf(stderr, "\tQuit. Bye Bye !!!\n");
        exit(-1);
    } else if (answer == 'p' || answer == 'P') return -1;
    else return 1;
}

// __________________________________________________
int disp(int argc, char** argv)
{
    // DEFINE YOUR MAIN FUNCTION HERE
    TString tname = "upk";
    
    Int_t opt = 0;
    if ((opt = getopt(argc, argv, "hpt:r:")) != -1) {
        switch(opt) {
        case 'h':
            Usage();
            exit(-1);
        case 't':
            fprintf(stderr, "[getopt]\tSelect -t option : '%s'\n", optarg);
            tname = optarg;
            break;
        default:
            fprintf(stderr, "Error:\tWrong options\n");
            Usage();
            exit(-1);
        }
    }
    argc -= optind;
    argv += optind;
    
    if (argc < 1) {
        fprintf(stderr, "Error:\tYou need at least 1 input file.\n");
        Usage();
        exit(-1);
    }
    TCanvas *fCanvas    = new TCanvas("cEventDisplay", "cEventDisplay", 1000, 500);
    T2KWCEvtDisp *fDisp = new T2KWCEvtDisp();
    fDisp->Init(fCanvas);

    TChain *fChain = new TChain(tname.Data());
    TString ifn;
    FileStat_t info;
    for (Int_t ifile = 0; ifile < argc; ifile++) {
        ifn = argv[ifile];
        if (gSystem->GetPathInfo(ifn.Data(), info) != 0) {
            fprintf(stderr, "Error:\tFile '%s' does not exist.\n", ifn.Data());
            exit(1);
        } else {
            fChain->Add(ifn.Data());
        }
    }

    if (fChain->GetNtrees() == 0) {
        fprintf(stderr, "NO trees in this chain. Quit.\n");
        exit(1);
    }

    TBranch     *fBr    = NULL;
    //T2KWCEvent  *fEvent = NULL;
    T2KWCUnpack *fUpk   = NULL;

    fBr = fChain->GetBranch("upk.");
    fUpk = new T2KWCUnpack();
    fBr->SetAddress(&fUpk);
    fChain->SetBranchAddress("upk.", &fUpk);

    Int_t nentries = fChain->GetEntries();
    fprintf(stderr, "Total # of events : %d\n", nentries);
    if (nentries == 0) {
        fprintf(stderr, "Error:\tNo entries found in this chain.\n");
        exit(1);
    }

    TString title;
    Int_t ientry  = 0;
    Int_t control = 1;
    while(ientry < nentries) {
        fUpk->Clear();
        fDisp->Clear();
        
        title.Form("Entry# %6d / %6d", ientry, nentries);
        fCanvas->SetTitle(title.Data());
        fChain->GetEntry(ientry);

        
        Int_t runnum   = fUpk->GetRunNum();
        Int_t spillnum = fUpk->GetSpillNum();
        Int_t nhits    = fUpk->GetAtmNhits();

        if (nhits > 0) {
            fDisp->InputData(fUpk);
            fDisp->Draw(fCanvas);
            fprintf(stderr, "Entry# %d drawn [R:%7d S:%7d]\t", ientry, runnum, spillnum);
            control = Control();
        } else {
            //fprintf(stderr, "No event in entry# %d [R:%7d S:%7d]\t", ientry, runnum, spillnum);
            //control = Control();
        }
        
        if      (control ==  1) ientry++;
        else if (control == -1) ientry--;
        else {
            ientry = Control();
            control = 1;
        }

        if (ientry < 0) {
            fprintf(stderr, "Error:\tEntry# %d is below 0\n", ientry);
            ientry  = 0;
            control = Control();
        }

        if (ientry >= nentries) {
            fprintf(stderr, "Error:\tEntry# %d is over maximum entry#\n", ientry);
            break;
        }
    }
    
    return 0;
}

// __________________________________________________
#ifndef __CINT__
int main(int argc, char** argv)
{
    TString style = "~/.rootlogon.C";
    gROOT->Macro(style.Data());
    TApplication dispApp("dispApp", &argc, argv);
    int retVal = disp(dispApp.Argc(), dispApp.Argv());
    dispApp.Run();
    return retVal;
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
