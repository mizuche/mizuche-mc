#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import time
import datetime

import logging
logging.basicConfig(level=logging.INFO,
                    datefmt='%Y/%m/%d %H:%M:%S',
                    format='%(levelname)s - %(name)s - %(funcName)s - %(message)s')

import argparse
#from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gROOT, gClient, gStyle
from ROOT import TFile, TTree, TChain
from ROOT import TH1D, TH2D
from ROOT import TGraph, TMultiGraph
from ROOT import TCanvas

gStyle.SetNdivisions(520)

## __________________________________________________
class Config(object):
    ClassName = 'DataLogger'
    TreeName = 'log'
    BranchDescriptor = 'utime/I:ch11/D:ch12/D:ch19/D:ch20/D'
    ## range
    Flow = (0, 5.5, 55)
    Temp = (17, 22, 50)
    Nch  = 2    ## OV, FV
    Duration = 3600
    dispw = gClient.GetDisplayWidth()
    disph = gClient.GetDisplayHeight()
    Disp  = (dispw, disph)
    Timeformat = '#splitline{%m/%d}{%H:%M}'
    PlotDir = 'data/plots'

## __________________________________________________
class DataLogger(object):
    '''
    Create ROOT file from DATALOGGER.CSV.
    Draw plots 
    '''
    
    ## __________________________________________________
    def __init__(self):
        self.chain = TChain(Config.TreeName)
        self.canvases = []
        
    ## __________________________________________________
    def __str__(self):
        '''
        print information of files added to TChain.
        '''
        ch = self.chain
        s = '[print]\tTreeName : {0}\n'.format(ch.GetName())
        s += '[print]\tNtrees   : {0}\n'.format(ch.GetNtrees())
        s += '[print]\tEntries  : {0}\n'.format(ch.GetEntries())
        return s

    ## __________________________________________________
    def init_th1d(self, name, title, xtuple, nloop, timedisplay=0):
        '''
        Initializer for N TH1D histgrams.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        logging.debug('Initialize TH1D')
        xmin, xmax, xbin = xtuple
        hists = []
        for i in range(nloop):
            color = i % 8 + 2
            h = TH1D(name % i, title % i, xbin, xmin, xmax)
            h.SetStats(0)
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            h.SetFillColor(color)
            h.GetXaxis().SetTimeFormat(Config.Timeformat)
            h.GetXaxis().SetTimeOffset(0, 'gmt')
            h.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                h.GetXaxis().SetLabelOffset(0.015)
            hists.append(h)
        return hists

    ## __________________________________________________
    def init_th2d(self, name, title, xtuple, ytuple, nloop, timedisplay=0):
        '''
        Initializer for N TH2D histgrams.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        logging.debug('Initialize TH2D')
        xmin, xmax, xbin = xtuple
        ymin, ymax, ybin = ytuple
        hists = []
        for i in range(nloop):
            color = i % 8 + 2
            h = TH2D(name % i, title % i, xbin, xmin, xmax, ybin, ymin, ymax)
            h.SetStats(0)
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            h.SetFillColor(color)
            h.GetXaxis().SetTimeFormat(Config.Timeformat)
            h.GetXaxis().SetTimeOffset(0, 'gmt')
            h.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                h.GetXaxis().SetLabelOffset(0.015)
            hists.append(h)
        return hists

    ## __________________________________________________
    def init_tgraph(self, name, title, nloop, timedisplay=0):
        '''
        Initializer for N TGraph graphs.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        logging.debug('Initialize TGraph')
        graphs = []
        for i in range(nloop):
            color = i % 8 + 2
            g = TGraph(0)
            g.SetName(name)
            g.SetTitle(title)
            g.SetLineColor(color)
            g.SetMarkerColor(color)
            g.SetFillStyle(0)
            g.GetXaxis().SetTimeFormat(Config.Timeformat)
            g.GetXaxis().SetTimeOffset(0, 'gmt')
            g.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                g.GetXaxis().SetLabelOffset(0.015)
            graphs.append(g)
        return graphs
    
    ## __________________________________________________
    def read(self, ifn):
        '''
        if input file is a CSV format : Convert it ROOT file.
        if input file is a ROOT format : Add TTree to TChain
        '''
        if not os.path.exists(ifn):
            logging.error('File "%s" does not exist.\n' % ifn)
            sys.exit(-1)

        head, tail = os.path.split(ifn)
        root, ext = os.path.splitext(tail)
        size = os.path.getsize(ifn) / 1e6
        start = datetime.datetime.now()
        if ext in ['.csv', '.CSV']:
            logging.info('Check filename\t: File "{0} ({1:5.2f} MB)" is a CSV file.'.format(ifn, size))
            self.create_rootfile(ifn)
        elif ext in ['.root']:
            logging.info('Check filename\t: File "{0} ({1:5.2f} MB)" is a ROOT file.'.format(ifn, size))
            self.read_rootfile(ifn)
        end = datetime.datetime.now()
        logging.debug('Ellapled time\t: {0} [sec]'.format(end - start))
        return
    
    ## __________________________________________________
    def create_rootfile(self, ifn):
        '''
        input filename : data/yymmdd/yymmdd-hhmmss_UG.CSV
        output filename : data/rootfile/yymmdd-hhmmss.root

        Read CSV file and create ROOT file.
        If output ROOT file exists, skip conversion.
        '''
        head, tail = os.path.split(ifn)
        ofn = os.path.join('data/rootfile', tail.lower().replace('_ug.csv', '.root'))

        if os.path.exists(ofn):
            logging.warn('Skip\t: "%s" already exist.' % ofn)
            return 'Skip conversion'
        
        logging.info('Read\t: "{0}"\n'.format(ifn))
        with open(ifn, 'r') as fin:
            lines = fin.readlines()

        logging.debug('Create "temp.txt" with formatted data')
        with open('temp.txt', 'w') as fout:
            for line in lines[34:]:
                line = line.rstrip().split(',')
                date = time.strptime(line[1], '%Y/%m/%d %H:%M:%S')
                utime = int(time.mktime(date))
                ch11 = line[12]
                ch12 = line[13]
                ch19 = line[20] if not len(line) == 19 else line[14]
                ch20 = line[21] if not len(line) == 19 else line[15]
                logging.debug('{0}\t{1}\t{2}\t{3}\t{4}'.format(utime, ch11, ch12, ch19, ch20))
                fout.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(utime, ch11, ch12, ch19, ch20))
                
        tname = Config.TreeName
        bd = Config.BranchDescriptor
        logging.info('Construct TTree\t: "%s", "%s"' % (tail, bd))
        t = TTree(tname, tail)
        t.ReadFile('temp.txt', bd)
        logging.info('Recreate : "%s"' % ofn)
        fout = TFile(ofn, 'recreate')
        logging.info('Write "%s" to "%s"' % (tname, ofn))
        t.Write()
        fout.Close()
        logging.info('Close : "%s"' % ofn)
        logging.debug('Remove "temp.txt"')
        os.remove('temp.txt')
        return 'Conversion Finished'

    ## __________________________________________________
    def read_rootfile(self, ifn):
        '''
        Read ROOT file with TChain
        When reading a file:
        1. Check if the input file exists.
        2. Check if it is a ROOT file, (*.root).
        3. Assume plot-saving directory and check if it exists.
        4. Open ROOT file and hold it inside.
        '''
        ### Open ROOT file and get TTree
        logging.info('Add\t: %s' % ifn)
        self.chain.Add(ifn)
        return self.chain

    ## __________________________________________________
    def process(self):
        ch = self.chain
        nentries = ch.GetEntries()
        if not nentries == 0:
            self.process_rootfile()
        else:
            sys.exit(-1)
        return

    ## __________________________________________________
    def get_utime(self, chain):
        logging.info('Get start/end of unixtime')
        nentries = chain.GetEntries()
        chain.GetEntry(0)
        s_utime = e_utime = chain.utime
        for ientry in range(nentries):
            chain.GetEntry(ientry)
            t = chain.utime
            s_utime = min(s_utime, t)
            e_utime = max(e_utime, t)
            
        s_date = '{0:%Y%m%d}'.format(datetime.datetime.fromtimestamp(s_utime))
        e_date = '{0:%Y%m%d}'.format(datetime.datetime.fromtimestamp(e_utime))
        self.prefix = '{0}_{1}'.format(s_date, e_date)
        logging.debug('Start : {0} ==> {1}'.format(s_utime, s_date))
        logging.debug('End   : {0} ==> {1}'.format(e_utime, e_date))
        return s_utime, e_utime
            
    ## __________________________________________________
    def process_rootfile(self):
        logging.info('Process ROOT file')
        ch = self.chain
        nentries = ch.GetEntries()

        ### get start / end of unixtime
        s_time, e_time = self.get_utime(ch)
        interval = Config.Duration
        s_time = (s_time / interval) * interval
        e_time = (e_time / interval + 1 ) * interval
        n_time = (e_time - s_time) / interval
        time = (s_time, e_time, n_time)
        
        ### Flow
        hname = 'hFlow%d'
        htitle = 'hFlow%d;Date/Time;Flow [V]'
        hFlows = self.init_th2d(hname, htitle, time, Config.Flow, Config.Nch, timedisplay=1)
        ### Temperature
        ymin, ymax, ybin = Config.Temp
        hname = 'hTemp%d'
        htitle = 'hTemp%d;Date/Time;Temprature [degC]' 
        hTemps = self.init_th2d(hname, htitle, time, Config.Temp, Config.Nch, timedisplay=1)

        logging.debug('Project ch11/12, ch19/20')
        ch.Project(hFlows[0].GetName(), 'ch11:utime')
        ch.Project(hFlows[1].GetName(), 'ch12:utime')
        ch.Project(hTemps[0].GetName(), 'ch19:utime')
        ch.Project(hTemps[1].GetName(), 'ch20:utime')

        ### Flow
        gname = 'gFlow%d'
        gtitle = 'gFlow%d;Date/Time;Flow [V]'
        gFlows = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)
        ### Temperature
        gname = 'gTemp%d'
        gtitle = 'gTemp%d;Date/Time;Temperature [degC]'
        gTemps = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)

        logging.debug('Filling TGraphs')
        for ientry in range(nentries):
            ch.GetEntry(ientry)
            utime = ch.utime    # unixtime
            ch11 = ch.ch11  # FV flow
            ch12 = ch.ch12  # OV flow
            ch19 = ch.ch19  # FV temp
            ch20 = ch.ch20  # OV temp

            
            n1 = gFlows[0].GetN()
            gFlows[0].SetPoint(n1, utime, ch11)

            n2 = gFlows[1].GetN()
            gFlows[1].SetPoint(n2, utime, ch12)

            n3 = gTemps[0].GetN()
            gTemps[0].SetPoint(n3, utime, ch19)

            n4 = gTemps[1].GetN()
            gTemps[1].SetPoint(n4, utime, ch20)

        self.hTemps = hTemps
        self.hFlows = hFlows
        self.gTemps = gTemps
        self.gFlows = gFlows

        return hTemps, hFlows, gTemps, gFlows

    ## __________________________________________________
    def draw(self):
        #c1 = self.draw_hist()
        c2 = self.draw_graph()
        self.stop()

    ## __________________________________________________
    def draw_hist(self):
        logging.info('Draw histograms')
        hTemps = self.hTemps
        hFlows = self.hFlows
        
        wx, wy = Config.Disp
        cname = 'cDataLogger{0}'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'DataLogger{0}'.format(self.prefix))
        c = TCanvas(cname, title, wx, wy)
        c.Divide(1, 2)
        c.cd(1)
        for i in range(len(hTemps)):
            gopt = 'box' if i == 0 else 'boxsame'
            hTemps[i].Draw(gopt)
        c.cd(2)
        for i in range(len(hFlows)):
            gopt = 'box' if i == 0 else 'boxsame'
            hFlows[i].Draw(gopt)
        c.Update()
        self.stop()

        self.canvases += c
            
        return c

    ## __________________________________________________
    def draw_graph(self, save):
        logging.info('Draw graphs')
        gTemps = self.gTemps
        gFlows = self.gFlows

        mgTemp = TMultiGraph('mgTemp', 'mgTemp;Date;Water Temperature [degC]')
        for g in gTemps:
            if g.GetN() > 0:
                mgTemp.Add(g, 'pl')
        mgFlow = TMultiGraph('mgFlow', 'mgFlow;Date;Water Flow [V]')
        for g in gFlows:
            if g.GetN() > 0:
                mgFlow.Add(g, 'pl')
        
        wx, wy = Config.Disp
        cname = 'cDataLogger{0}'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'DataLogger{0}'.format(self.prefix))
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 2)

        c.cd(1)
        mgTemp.Draw('apl')
        c.cd(1).Update()
        mgTemp.GetXaxis().SetTimeDisplay(1)
        mgTemp.GetXaxis().SetTimeOffset(0, 'gmt')
        mgTemp.GetXaxis().SetTimeFormat(Config.Timeformat)

        c.cd(2)
        mgFlow.Draw('apl')
        c.cd(2).Update()
        mgFlow.GetXaxis().SetTimeDisplay(1)
        mgFlow.GetXaxis().SetTimeOffset(0, 'gmt')
        mgFlow.GetXaxis().SetTimeFormat(Config.Timeformat)

        c.Update()
        logging.warn('Save canvas ?')
        self.stop()
        c.SaveAs('{0}.png'.format(c.GetTitle()))
        return c

    ## __________________________________________________
    def stop(self):
        logging.warn('Press "q" or ".q" to Quit.')
        ans = raw_input(' > ')
        if ans in ['q', '.q']:
            sys.stderr.write('\t"%s" pressed. Bye Bye.\n' % ans)
            sys.exit(-1)
            
## __________________________________________________
if __name__ == '__main__':
    
    desc = '(1) Create ROOT file if input file is a TEXT data\n'
    desc += '(2) Create plots if input file is a ROOT file'

    epi = 'examples:\n'
    epi += '(1) Create ROOT file:\n'
    epi += '    Ex1) %(prog)s data/130320/130320-132322_UG.CSV\n'
    epi += '    Ex2) %(prog)s data/130320/130320-*.CSV\n'
    epi += '(2) Create plots:\n'
    epi += '    Ex3) %(prog)s data/rootfile/130320-132322.root\n'
    epi += '    Ex4) %(prog)s -s data/rootfile/130508-1*.root\n'
    epi += '\n'

    epi += 'about logger format:\n'
    epi += '\n'

    epi += 'others:\n'
    epi += '\n'
    

    parser = argparse.ArgumentParser(description=desc,
                                     epilog=epi,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('ifn',
                        type=str,
                        nargs='+',
                        help='input filename(s)')
    parser.add_argument('-b',
                        dest='batch',
                        action='store_true',
                        help='set ROOT batch mode')
    parser.add_argument('-s',
                        dest='save',
                        action='store_true',
                        help='save canvases in png')
    parser.set_defaults(batch=False,
                       save=False)

    args = parser.parse_args()
    
    logging.debug('options : {0}'.format(options))
    logging.debug('args    : {0}'.format(args))

    if len(args) < 1:
        parse.error('Wrong number of arguments ({0}).'.format(len(args)))
        sys.exit(-1)
        
    if options.batch:
        logging.info('Set ROOT batch mode.\n')
        gROOT.SetBatch(1)

    log = DataLogger()
    
    for ifn in args:
        log.read(ifn)
    log.process()
    log.draw_graph(options.save)
    logging.info('-' * 50)
    
