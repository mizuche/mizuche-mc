#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
from ROOT import TTree, TCanvas, TLegend, TH2D
from ROOT import TVectorD, TGraph, TF1, TMath

## __________________________________________________
if __name__ == '__main__':
    ifn = 'gate_trgin_atmncharges.txt'
    t = TTree('t', 't')
    t.ReadFile(ifn, 'gate_trgin:atm_ncharges:trgin_trgout')

    t.Project('', 'atm_ncharges:gate_trgin', 'trgin_trgout == 600')
    x600 = TVectorD(t.GetSelectedRows(), t.GetV2())
    y600 = TVectorD(t.GetSelectedRows(), t.GetV1())
    g600 = TGraph(x600, y600)
    g600.SetMarkerColor(2)

    t.Project('', 'atm_ncharges:gate_trgin', 'trgin_trgout == 500')
    x500 = TVectorD(t.GetSelectedRows(), t.GetV2())
    y500 = TVectorD(t.GetSelectedRows(), t.GetV1())
    g500 = TGraph(x500, y500)

    t.Project('', 'atm_ncharges:gate_trgin', 'trgin_trgout == 700')
    x700 = TVectorD(t.GetSelectedRows(), t.GetV2())
    y700 = TVectorD(t.GetSelectedRows(), t.GetV1())
    g700 = TGraph(x700, y700)
    g700.SetMarkerColor(3)

    t.Project('', 'atm_ncharges:gate_trgin', 'trgin_trgout == 800')
    x800 = TVectorD(t.GetSelectedRows(), t.GetV2())
    y800 = TVectorD(t.GetSelectedRows(), t.GetV1())
    g800 = TGraph(x800, y800)
    g800.SetMarkerColor(4)

    c = TCanvas('cGate', 'cGate', 1000, 500)
    title = 'ATM charge decrease with Gate overlap length;Gate - TRGin [ns];ATM Charge [pC]'
    frame = TH2D('fame', title, 130, -30, 100, 120, -10, 110)
    frame.SetStats(0)
    frame.Draw()
    g500.SetMarkerSize(2)
    g500.Draw('psame')
    g600.Draw('psame')
    g700.SetMarkerSize(2)
    g700.Draw('psame')
    g800.Draw('psame')

    # f = TF1('f', 'TMath::Exp([0]+1./[1])')
    # f.SetParameters(1, 10)
    # g500.Fit(f, '', '', 30, 60)

    l = TLegend(0.2, 0.1, 0.5, 0.3)
    l.SetFillColor(0)
    l.AddEntry(g500, 'TRGout - TRGin = 500ns', 'p')
    l.AddEntry(g600, 'TRGout - TRGin = 600ns', 'p')
    l.AddEntry(g700, 'TRGout - TRGin = 700ns', 'p')
    l.AddEntry(g800, 'TRGout - TRGin = 800ns', 'p')
    l.Draw()

    ans = raw_input('Press "q" to quit > ')
    if len(ans) > 0: ans = ans[0]
    if ans in ['q', 'Q']:
        sys.exit(-1)
    
    pass
