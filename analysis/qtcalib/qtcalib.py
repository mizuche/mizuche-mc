#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TCanvas
from ROOT import TCanvas, TBox, TGaxis, TLegend, TLine
from ROOT import TH1D, TH2D, TGraph, TGraphErrors, TF1
from ROOT import TVectorD, Double

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

## __________________________________________________
class QtCalib(object):
    ## __________________________________________________
    def __init__(self, date, runs, options = None):
        self.dir   = '../exp/qtcalib/upk/'
        self.date  = date
        self.runs  = runs
        self.ifns  = []
        for irun in runs:
            ifn = 'upk%d%04d.root' % (int(date), int(irun))
            ifn = os.path.join(self.dir, ifn)
            self.ifns.append(ifn)
        self.cut = options.cut
        self.calibFadc = 1 #0.08

    ## __________________________________________________
    def stop(self):
        gSystem.ProcessEvents()
        ans = raw_input('Stopped. Press "q" to quit > ')
        if ans in ['q', 'Q']:
            sys.exit(-1)
        elif ans in ['.', '.q', '.Q']:
            return -1

    ## __________________________________________________
    def control(self, ientry):
        gSystem.ProcessEvents()
        ans = raw_input('Entry # %5d    Input entry# or [n]/p/q > ' % ientry)
        if ans in ['q', 'Q']  : sys.exit(-1)
        elif ans in ['p', 'P']: return ientry-1
        elif ans in ['n', 'N', '']: return ientry+1
        else: return int(ans)    
    
    ## __________________________________________________
    def read(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.exit(-1)

        print '[%s]\tRead "%s"' % ('read', ifn)
        fin = TFile(ifn, 'read')
        return fin
    
    ## __________________________________________________
    def waveform(self, ifn):
        print '[%s]\tDraw waveforms in "%s".' % ('waveform', ifn)
        fin = self.read(ifn)

        umode = fin.Get('umode')
        umode.GetEntry(0)
        mode   = umode.umode
        hitthr = umode.hitthr

        print '[%s]\tChecking unpacked mode : %d' % ('waveform', mode)
        if not mode == 1:
            sys.stderr.write('Error:\t"%s" does not have waveform data.\n' % ifn)
            sys.stderr.write('\tQuit. Bye Bye!!!\n')
            sys.exit(-1)
        
        upk = fin.Get('upk')
        unpack = T2KWCUnpack()
        br = upk.GetBranch('upk.')
        br.SetAddress(unpack)
        upk.SetBranchAddress('upk.', unpack)

        c = TCanvas('cWaveform', 'check waveforms', 800, 800)
        c.Divide(1, 2)

        nentries = upk.GetEntries()
        ientry = 0
        while(ientry < nentries):
            ## PMTSUM
            upk.Project('', 'fadc_pmtsum:Iteration$', 'Entry$ == %d' % ientry)
            xp = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yp = TVectorD(upk.GetSelectedRows(), upk.GetV1())
            
            gp = TGraph(xp, yp)
            gp.SetTitle('PMTSUM (%4d/%4d)' % (ientry, nentries))
            gp.GetXaxis().SetTitle('Point [4ns/pt]')
            gp.GetYaxis().SetTitle('PMTSUM [mV]')

            ## HITSUM
            upk.Project('', 'fadc_hitsum:Iteration$', 'Entry$ == %d' % ientry)
            xh = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yh = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gh = TGraph(xh, yh)
            gh.SetTitle('HITSUM (%4d/%4d)' % (ientry, nentries))
            gh.GetXaxis().SetTitle('Point [4ns/pt]')
            gh.GetYaxis().SetTitle('HITSUM [mV]')

            ## Bunch timing and HITSUM height
            upk.Project('', '-fadc_height:fadc_nsec/4', 'Entry$ == %d' % ientry)
            xb = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yb = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gb = TGraph(xb, yb)
            gb.SetTitle('Hit Timing (%4d/%4d)' % (ientry, nentries))
            gb.GetXaxis().SetTitle('Timing [nsec]')
            gb.GetYaxis().SetTitle('Height [mV]')

            ### To draw integral region for PMTSUM and HITSUM
            nb = gb.GetN()
            gp_ymin = gp.GetYaxis().GetXmin()
            gp_ymax = gp.GetYaxis().GetXmax()
            gh_ymin = gh.GetYaxis().GetXmin()
            gh_ymax = gh.GetYaxis().GetXmax()
            fadc_nsamp = upk.upk.GetFadcNsamp()
            
            charges_p = []
            windows_p = []
            windows_h = []
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = int(x) - 18
                xmax = int(x) + 9
                box_p = TBox(xmin, gp_ymin, xmax, gp_ymax)
                windows_p.append(box_p)
                ###
                xmin = int(x) + 6
                xmax = int(x) + 45
                box_h = TBox(xmin, gh_ymin, xmax, gh_ymax)
                windows_h.append(box_h)

            ### To draw daq threshold line
            xmin = gh.GetXaxis().GetXmin()
            xmax = gh.GetXaxis().GetXmax()
            lthr = TLine(xmin, -hitthr, xmax, -hitthr)
            lthr.SetLineColor(2)
            lthr.SetLineStyle(2)
                
            ### Print hit summary
            print '\tEntry# %4d' % ientry
            upk.GetEntry(ientry)
            upk.upk.Print()            

            run           = upk.upk.GetRunNum()
            qt            = upk.upk.GetDaqmode() - 1000
            delay         = upk.upk.GetFvstat()  - 1000

            fadc_ntrg     = upk.upk.GetFadcNtrg()
            fadc_nhits    = upk.upk.GetFadcNhits()
            fadc_ncharges = upk.upk.GetFadcNcharges()
            fadc_npes     = upk.upk.GetFadcNpes()
            atm_ntrg      = upk.upk.GetAtmNtrg()
            atm_nhits     = upk.upk.GetAtmNhits()
            atm_ncharges  = upk.upk.GetAtmNcharges()
            atm_npes      = upk.upk.GetAtmNpes()

            title = 'Run:%d QT:%d pC Delay:%d ns' % (run, qt, delay)
            lp = TLegend(0.6, 0.1, 0.9, 0.3, title)
            lp.SetFillColor(0)
            label = 'FADC:%8.2f [pC] / %8.2f [p.e.]' % (fadc_ncharges, fadc_npes)
            lp.AddEntry(gp, label, 'lp')
            label = 'ATM :%8.2f [pC] / %8.2f [p.e.]' % (atm_ncharges, atm_npes)
            lp.AddEntry(gp, label, 'lp')
            
            lh = TLegend(0.6, 0.1, 0.9, 0.3, title)
            lh.SetFillColor(0)
            label = 'FADC:%8.2f [hits] / %8.2f [trg]' % (fadc_nhits, fadc_ntrg)
            lh.AddEntry(gp, label, 'lp')
            label = 'ATM :%8.2f [hits] / %8.2f [trg]' % (atm_nhits, atm_ntrg)
            lh.AddEntry(gp, label, 'lp')
            
            c.cd(1)
            gp.Draw('apl')
            for box in windows_p:
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3004)
                box.Draw()
            lp.Draw()

            c.cd(2)
            gh.Draw('apl')
            if gb.GetN() > 0:
                gb.SetLineColor(2)
                gb.SetMarkerColor(2)
                gb.SetMarkerStyle(34)
                gb.SetMarkerSize(4)
                gb.Draw('psame')
            for box in windows_h:
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3004)
                box.Draw()
            lthr.Draw()
            lh.Draw()
            c.Update()

            ientry = self.control(ientry)
            if ientry < 0 or ientry > nentries - 1:
                sys.stderr.write('Error:\tEntry #%d out of range. ' % ientry)
                c.Clear()
                c.Divide(1, 2)
                c.Update()
                self.stop()
                ientry = 0
        return fin

    ## __________________________________________________
    def histNcharges(self, ifn, draw = False):
        fin = self.read(ifn)
        
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()
        qt    = upk.upk.GetDaqmode() - 1000
        delay = upk.upk.GetFvstat()  - 1000
        
        cut = self.cut
        calibFadc = self.calibFadc

        xmin, xmax, xbin = 0, 1000, 1000
        name = 'hAtmNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hAtm = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_ncharges', cut)

        name = 'hFadcNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_ncharges/%f' % calibFadc, cut)

        name = 'hDiffNcharges'
        title = '%s;DiffNcharges (ATM - FADC) [pC];Entries [#]' % name
        hDiff = TH1D(name, title, 200, -100, 100)
        upk.Project(hDiff.GetName(), 'atm_ncharges - fadc_ncharges/%f' % calibFadc, cut);

        if draw:
            name = 'cHistNcharges'
            c = TCanvas(name, name, 800, 400)
            c.Divide(2, 1)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN:%d QT:%d pC' % (run, qt)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()
        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNpes(self, ifn, draw = False):
        fin = self.read(ifn)
        
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()
        qt    = upk.upk.GetDaqmode() - 1000
        delay = upk.upk.GetFvstat()  - 1000

        cut = self.cut
        calibFadc = self.calibFadc

        xmin, xmax, xbin = 0, 1000, 1000
        name = 'hAtmNpes'
        title = '%s;Npes [p.e.];Entries [#]' % name
        hAtm = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_npes', cut)

        name = 'hFadcNpes'
        title = '%s;Npes [p.e.];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_npes/%f' % calibFadc, cut)

        name = 'hDiffNpes'
        title = '%s;DiffNpes (ATM - FADC) [p.e.];Entries [#]' % name
        hDiff = TH1D(name, title, 1000, -500, 500)
        upk.Project(hDiff.GetName(), 'atm_npes - fadc_npes/%f' % calibFadc, cut);

        if draw:
            name = 'cHistNpes'
            c = TCanvas(name, name, 800, 400)
            c.Divide(2, 1)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN:%d QT:%d pC' % (run, qt)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()

        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNhits(self, ifn, draw = False):
        fin = self.read(ifn)
        
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()
        qt    = upk.upk.GetDaqmode() - 1000
        delay = upk.upk.GetFvstat()  - 1000
        
        cut = self.cut

        xmin, xmax, xbin = 0, 1000, 1000
        name = 'hAtmNhits'
        title = '%s;Nhits [#];Entries [#]' % name
        hAtm = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_nhits', cut)

        name = 'hFadcNhits'
        title = '%s;Nhits [#];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_nhits', cut)

        name = 'hDiffNhits'
        title = '%s;DiffNhits (ATM - FADC) [#];Entries [#]' % name
        hDiff = TH1D(name, title, 1000, -500, 500)
        upk.Project(hDiff.GetName(), "atm_nhits - fadc_nhits", cut);

        if draw:
            name = 'cHistNhits'
            c = TCanvas(name, name, 800, 400)
            c.Divide(2, 1)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN:%d QT:%d pC' % (run, qt)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()

        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNtrg(self, ifn, draw = False):
        fin = self.read(ifn)

        upk = fin.Get('upk')
        upk.GetEntry(0)
        run   = upk.upk.GetRunNum()
        qt    = upk.upk.GetDaqmode() - 1000
        delay = upk.upk.GetFvstat()  - 1000
        
        name = 'hAtmNtrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hAtm = TH1D(name, title, 10, 0, 10)
        upk.Project(hAtm.GetName(), "atm_ntrg");
        
        name = 'hFadcNtrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hFadc = TH1D(name, title, 10, 0, 10)
        upk.Project(hFadc.GetName(), "fadc_ntrg");

        name = 'hDiffNtrg'
        title = '%s;DiffNtrg (ATM - FADC) [#];Entries [#]' % name
        hDiff = TH1D(name, title, 20, -10, 10)
        upk.Project(hDiff.GetName(), "atm_ntrg - fadc_ntrg");
        
        if draw:
            name = 'cHistNtrg'
            c = TCanvas(name, name, 800, 400)
            c.Divide(2, 1)
            c.cd(1).SetLogy()
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw();
            hFadc.SetLineColor(4)
            hFadc.SetFillColor(4)
            hFadc.Draw('same');

            title = 'RUN:%d QT:%d pC' % (run, qt)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM : %d Entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC: %d Entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2).SetLogy()
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop();
        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def hist(self, item, draw = True):
        items = ['ncharges', 'npes', 'ntrg', 'waveform']
        if not item in items:
            sys.stderr.write('Error:\tGiven item (%s) is not in the list' % item)
            print items
            sys.exit()
        
        ifns = self.ifns
        for ifn in ifns:
            if item == items[0]:
                self.histNcharges(ifn, draw)
            if item == items[1]:
                self.histNpes(ifn, draw)
            if item == items[2]:
                self.histNtrg(ifn, draw)
            if item == items[3]:
                self.waveform(ifn)
        return
    
    ## __________________________________________________
    def graphNcharges(self):
        ifns       = self.ifns
        calibFadc  = self.calibFadc
        gQtAtm     = TGraphErrors(0)
        gQtFadc    = TGraphErrors(0)
        gQtDiff    = TGraphErrors(0)
        gDelayAtm  = TGraphErrors(0)
        gDelayFadc = TGraphErrors(0)
        gDelayDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNcharges(ifn)
            upk.GetEntry(0)
            run    = upk.upk.GetRunNum()
            delay  = upk.upk.GetFvstat()  - 1000
            qt     = upk.upk.GetDaqmode() % 1000
            ###
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gQtAtm.GetN()
            gQtAtm.SetPoint(n, qt, m_atm)
            gQtAtm.SetPointError(n, 0, r_atm)
            n = gQtFadc.GetN()
            gQtFadc.SetPoint(n, qt, m_fadc)
            gQtFadc.SetPointError(n, 0, r_fadc)
            n = gQtDiff.GetN()
            gQtDiff.SetPoint(n, qt, m_diff)
            gQtDiff.SetPointError(n, 0, r_diff)
            n = gDelayAtm.GetN()
            gDelayAtm.SetPoint(n, delay, m_atm)
            gDelayAtm.SetPointError(n, 0, r_atm)
            n = gDelayFadc.GetN()
            gDelayFadc.SetPoint(n, delay, m_fadc)
            gDelayFadc.SetPointError(n, 0, r_fadc)
            n = gDelayDiff.GetN()
            gDelayDiff.SetPoint(n, delay, m_diff)
            gDelayDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        #c.Divide(2, 2)
        c.cd(1)
        xmin, xmax = 0, 300
        gQtAtm.GetXaxis().SetRangeUser(xmin, xmax)
        gQtAtm.SetTitle('QT vs Ncharges;QT Input [pC];Ncharges Response [pC]')
        gQtAtm.SetMarkerColor(2)
        gQtAtm.SetLineColor(2)
        gQtAtm.Draw('ap')
        gQtFadc.SetMarkerColor(4)
        gQtFadc.SetLineColor(4)
        gQtFadc.Draw('psame')
        ###
        lQt = TLegend(0.1, 0.8, 0.5, 0.9, 'QT vs Ncharges')
        lQt.SetFillColor(0)
        lQt.AddEntry(gQtAtm, 'ATM', 'lp')
        lQt.AddEntry(gQtFadc, 'FADC', 'lp')
        lQt.Draw()

        ## fit QT vs FADCresponse in 2-ways
        lFit = TLegend(0.1, 0.6, 0.5, 0.8, 'Fit results')
        lFit.SetFillColor(0)


        
        fFitAtm1 = TF1('fFitAtm1', '[0]*x')
        fFitAtm2 = TF1('fFitAtm2', '[0]*x + [1]')
        fFitAtm1.SetLineColor(3)
        fFitAtm2.SetLineColor(5)
        fFitAtm1.SetParameter(0, 1)
        fFitAtm2.SetParameter(0, 1)
        fFitAtm2.SetParameter(1, 0)
        gQtAtm.Fit(fFitAtm1, 'emq', 'same', xmin, xmax)
        pAtm0 = fFitAtm1.GetParameter(0)
        label = 'ATM = %.3f #times QT' % (pAtm0)
        lFit.AddEntry(fFitAtm1, label, 'l')
        gQtAtm.Fit(fFitAtm2, 'emq+', 'same', xmin, xmax)
        pAtm1 = fFitAtm2.GetParameter(0)
        pAtm2 = fFitAtm2.GetParameter(1)
        label = 'ATM = %.3f #times QT + %.3f' % (pAtm1, pAtm2)
        lFit.AddEntry(fFitAtm2, label, 'l')

        fFitFadc1 = TF1('fFitFadc1', '[0]*x')
        fFitFadc2 = TF1('fFitFadc2', '[0]*x + [1]')
        fFitFadc1.SetLineColor(6)
        fFitFadc2.SetLineColor(7)
        fFitFadc1.SetParameter(0, 0.1)
        fFitFadc2.SetParameter(0, 0.1)
        fFitFadc2.SetParameter(1, 0)
        gQtFadc.Fit(fFitFadc1, 'emq', 'same', xmin, xmax)
        pFadc0 = fFitFadc1.GetParameter(0)
        label = 'FADC = %.3f #times QT' % (pFadc0)
        lFit.AddEntry(fFitFadc1, label, 'l')
        gQtFadc.Fit(fFitFadc2, 'emq+', 'same', xmin, xmax)
        pFadc1 = fFitFadc2.GetParameter(0)
        pFadc2 = fFitFadc2.GetParameter(1)
        label = 'FADC = %.3f #times QT + %.3f' % (pFadc1, pFadc2)
        lFit.AddEntry(fFitFadc2, label, 'l')
        
        lFit.Draw()
        
        c.cd(2)
        gQtDiff.SetTitle('QT vs DiffNcharges')
        gQtDiff.GetXaxis().SetTitle('QT Input [pC]')
        gQtDiff.GetYaxis().SetTitle('DiffNcharges (ATM - FADC/%.3f) [pC]' % calibFadc)
        gQtDiff.SetMarkerColor(3)
        gQtDiff.SetLineColor(3)
        #gQtDiff.Draw('ap')
        c.cd(3)
        gDelayAtm.SetTitle('Delay (TRGout - TRGin) vs Ncharges')
        gDelayAtm.GetXaxis().SetTitle('Delay (TRGout - TRGin) [ns]')
        gDelayAtm.GetYaxis().SetTitle('Ncharges [pC]')
        gDelayAtm.SetMarkerColor(2)
        gDelayAtm.SetLineColor(2)
        #gDelayAtm.Draw('ap')
        gDelayFadc.SetMarkerColor(4)
        gDelayFadc.SetLineColor(4)
        #gDelayFadc.Draw('psame')
        lDelay = TLegend(0.1, 0.7, 0.5, 0.9, 'Delay vs Ncharges')
        lDelay.SetFillColor(0)
        lDelay.AddEntry(gDelayAtm, 'ATM', 'lp')
        lDelay.AddEntry(gDelayFadc, 'FADC', 'lp')
        #lDelay.Draw()
        c.cd(4)
        gDelayDiff.SetTitle('Delay (TRGout - TRGin) vs DiffNcharges')
        gDelayDiff.GetXaxis().SetTitle('Delay (TRGout - TRGin) [ns]')
        gDelayDiff.GetYaxis().SetTitle('DiffNcharges (ATM - FADC/%.3f) [pC]' % calibFadc)
        gDelayDiff.SetMarkerColor(3)
        gDelayDiff.SetLineColor(3)
        #gDelayDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graphNpes(self):
        ifns = self.ifns
        gQtAtm   = TGraphErrors(0)
        gQtFadc  = TGraphErrors(0)
        gQtDiff  = TGraphErrors(0)
        gDelayAtm  = TGraphErrors(0)
        gDelayFadc = TGraphErrors(0)
        gDelayDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNpes(ifn, False)
            upk.GetEntry(0)
            
            run    = upk.upk.GetRunNum()
            qt     = upk.upk.GetDaqmode() - 1000
            delay  = upk.upk.GetFvstat()  - 1000
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gQtAtm.GetN()
            gQtAtm.SetPoint(n, qt, m_atm)
            gQtAtm.SetPointError(n, 0, r_atm)
            n = gQtFadc.GetN()
            gQtFadc.SetPoint(n, qt, m_fadc)
            gQtFadc.SetPointError(n, 0, r_fadc)
            n = gQtDiff.GetN()
            gQtDiff.SetPoint(n, qt, m_diff)
            gQtDiff.SetPointError(n, 0, r_diff)
            n = gDelayAtm.GetN()
            gDelayAtm.SetPoint(n, delay, m_atm)
            gDelayAtm.SetPointError(n, 0, r_atm)
            n = gDelayFadc.GetN()
            gDelayFadc.SetPoint(n, delay, m_fadc)
            gDelayFadc.SetPointError(n, 0, r_fadc)
            n = gDelayDiff.GetN()
            gDelayDiff.SetPoint(n, delay, m_diff)
            gDelayDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gQtAtm.SetTitle('QT vs Npes;QT Input [pC];Npes [p.e.]')
        gQtAtm.SetMarkerColor(2)
        gQtAtm.SetLineColor(2)
        gQtAtm.Draw('ap')
        gQtFadc.SetMarkerColor(4)
        gQtFadc.SetLineColor(4)
        gQtFadc.Draw('psame')
        lQt = TLegend(0.1, 0.7, 0.5, 0.9, 'QT vs Npes')
        lQt.SetFillColor(0)
        lQt.AddEntry(gQtAtm, 'ATM', 'lp')
        lQt.AddEntry(gQtFadc, 'FADC', 'lp')
        lQt.Draw()
        c.cd(2)
        gQtDiff.SetTitle('QT vs DiffNpes;QT Input [pC];DiffNpes (ATM - FADC/%.3f) [p.e.]' % self.calibFadc)
        gQtDiff.SetMarkerColor(3)
        gQtDiff.SetLineColor(3)
        gQtDiff.Draw('ap')
        c.cd(3)
        gDelayAtm.SetTitle('Delay vs Npes;Delay (TRGout - TRGin) [ns];Npes [p.e.]')
        gDelayAtm.SetMarkerColor(2)
        gDelayAtm.SetLineColor(2)
        gDelayAtm.Draw('ap')
        gDelayFadc.SetMarkerColor(4)
        gDelayFadc.SetLineColor(4)
        gDelayFadc.Draw('psame')
        lDelay = TLegend(0.1, 0.7, 0.5, 0.9, 'Delay vs Npes')
        lDelay.SetFillColor(0)
        lDelay.AddEntry(gDelayAtm, 'ATM', 'lp')
        lDelay.AddEntry(gDelayFadc, 'FADC', 'lp')
        lDelay.Draw()
        c.cd(4)
        gDelayDiff.SetTitle('Delay vs DiffNpes;Delay (TRGout - TRGin) [ns];DiffNpes (ATM - FADC/%.3f) [p.e.]' % calibFadc)
        gDelayDiff.SetMarkerColor(3)
        gDelayDiff.SetLineColor(3)
        gDelayDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graphNtrg(self):
        ifns = self.ifns
        gQtAtm   = TGraphErrors(0)
        gQtFadc  = TGraphErrors(0)
        gQtDiff  = TGraphErrors(0)
        gDelayAtm  = TGraphErrors(0)
        gDelayFadc = TGraphErrors(0)
        gDelayDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNtrg(ifn)
            upk.GetEntry(0)
            run    = upk.upk.GetRunNum()
            qt     = upk.upk.GetDaqmode() - 1000
            delay  = upk.upk.GetFvstat()  - 1000
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gQtAtm.GetN()
            gQtAtm.SetPoint(n, qt, m_atm)
            gQtAtm.SetPointError(n, 0, r_atm)
            n = gQtFadc.GetN()
            gQtFadc.SetPoint(n, qt, m_fadc)
            gQtFadc.SetPointError(n, 0, r_fadc)
            n = gQtDiff.GetN()
            gQtDiff.SetPoint(n, qt, m_diff)
            gQtDiff.SetPointError(n, 0, r_diff)
            n = gDelayAtm.GetN()
            gDelayAtm.SetPoint(n, delay, m_atm)
            gDelayAtm.SetPointError(n, 0, r_atm)
            n = gDelayFadc.GetN()
            gDelayFadc.SetPoint(n, delay, m_fadc)
            gDelayFadc.SetPointError(n, 0, r_fadc)
            n = gDelayDiff.GetN()
            gDelayDiff.SetPoint(n, delay, m_diff)
            gDelayDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gQtAtm.SetTitle('QT vs Ntrg;QT [V];Ntrg [#]')
        gQtAtm.SetMarkerColor(2)
        gQtAtm.SetLineColor(2)
        gQtAtm.Draw('ap')
        gQtFadc.SetMarkerColor(4)
        gQtFadc.SetLineColor(4)
        gQtFadc.Draw('psame')
        lQt = TLegend(0.1, 0.7, 0.5, 0.9, 'QT vs Ntrg')
        lQt.SetFillColor(0)
        lQt.AddEntry(gQtAtm, 'ATM', 'lp')
        lQt.AddEntry(gQtFadc, 'FADC', 'lp')
        lQt.Draw()
        c.cd(2)
        gQtDiff.SetTitle('QT vs DiffNtrg;QT [V];DiffNtrg [#]')
        gQtDiff.SetMarkerColor(3)
        gQtDiff.SetLineColor(3)
        gQtDiff.Draw('ap')
        c.cd(3)
        gDelayAtm.SetTitle('Pulse delay vs Ntrg;Pulse Delay [ns];Ntrg [#]')
        gDelayAtm.SetMarkerColor(2)
        gDelayAtm.SetLineColor(2)
        gDelayAtm.Draw('ap')
        gDelayFadc.SetMarkerColor(4)
        gDelayFadc.SetLineColor(4)
        gDelayFadc.Draw('psame')
        lDelay = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse delay vs Ntrg')
        lDelay.SetFillColor(0)
        lDelay.AddEntry(gDelayAtm, 'ATM', 'lp')
        lDelay.AddEntry(gDelayFadc, 'FADC', 'lp')
        lDelay.Draw()
        c.cd(4)
        gDelayDiff.SetTitle('Pulse delay vs DiffNtrg;Pulse Delay [ns];DiffNtrg [#]')
        gDelayDiff.SetMarkerColor(3)
        gDelayDiff.SetLineColor(3)
        gDelayDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graph(self, item):
        items = ['ncharges', 'npes', 'ntrg']
        if not item in items:
            sys.stderr.write('Error:\tGiven item (%s) is not in the list' % item)
            print items
            sys.exit()
        
        if item == items[0]:
            self.graphNcharges()
        if item == items[1]:
            self.graphNpes()
        if item == items[2]:
            self.graphNtrg()
        return
    
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [options] dir run_start# run_end#' % sys.argv[0]

    defdir = '../exp/qtcalib/upk/'
    defcut = 'atm_ntrg ==1 && fadc_ntrg==1'
    
    parser = OptionParser(usage)
    parser.add_option('-l', '--list',
                      action = 'store_true',
                      dest   = 'ls',
                      help   = 'ls "%s"' % defdir)
    parser.add_option('-i', '--individual',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('-q', '--ncharges',
                      action = 'store_true',
                      dest   = 'ncharges',
                      help   = 'draw ncharges distribution')
    parser.add_option('-p', '--npes',
                      action = 'store_true',
                      dest   = 'npes',
                      help   = 'draw npes distribution')
    parser.add_option('-w', '--waveform',
                      action = 'store_true',
                      dest = 'waveform',
                      help = 'draw waveforms')
    parser.add_option('-t', '--trg',
                      action = 'store_true',
                      dest = 'ntrg',
                      help = 'draw trg')
    parser.add_option('-g', '--graph',
                      action = 'store_true',
                      dest = 'graph',
                      help = 'draw graph')
    parser.add_option('-c', '--cut',
                      dest = 'cut',
                      help = 'set cut [default = "%s"]' % defcut)

    parser.set_defaults(ls         = False,
                        individual = False,
                        canvas     = False,
                        ncharges   = False,
                        npes       = False,
                        waveform   = False,
                        ntrg       = False,
                        graph      = False,
                        cut        = defcut)
    
    (options, args) = parser.parse_args()
    
    #print options
    #print len(args), args

    if len(args) < 1:
        if options.ls:
            print 'List files in "%s"' % os.path.realpath(defdir)
            com = 'ls %s' % defdir
            os.system(com)
        else:
            sys.stderr.write('Error:\tWrong number of arguments. (%d)\n' % len(args))
        print usage
        sys.exit(-1)

    date = args[0]
    if options.individual:
        runs = args[1:]
    else:
        srun = int(args[1])
        erun = srun
        if len(args) > 2:
            erun = int(args[2])
        runs = [run for run in range(srun, erun+1)]
    print 'Runs:\t', runs

    q = QtCalib(date, runs, options)

    items = []
    if options.waveform: items.append('waveform')
    if options.ncharges: items.append('ncharges')
    if options.npes    : items.append('npes')
    if options.ntrg    : items.append('ntrg')

    if len(items) == 0:
        q.graph('ncharges')
    else:
        for item in items:
            if options.graph:
                q.graph(item)
            else:
                q.hist(item, True)
    sys.exit(-1)
