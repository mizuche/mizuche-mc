#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TVectorD, TGraph, TCanvas, TH1D, TGraphErrors, TBox
from ROOT import TF1

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

class Graph(object):
    ## __________________________________________________
    def __init__(self, start = 80, end = 100):
        self.setIntegrationRange(start, end)
        self.debug = False
        self.graph = False
        self.histo = False

    ## __________________________________________________
    def setIntegrationRange(self, start, end):
        if start > end:
            print 'Error:\tstart# (%d) > end# (%d)' % (start, end)
            sys.exit(-1)
        self.start = start
        self.end   = end

    ## __________________________________________________
    def checkWaveforms(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.exit(-1)

        print 'Draw waveforms in "%s".' % ifn
        fin = TFile(ifn, 'read')
        upk = fin.Get('upk')

        nentries = upk.GetEntries()

        c = TCanvas('cWaveform', 'check waveforms', 1000, 1000)
        c.Divide(1, 2)
        
        for ientry in range(0, nentries):
            upk.Project('', 'fadc_pmtsum:Iteration$', 'Entry$ == %d' % ientry)
            xp = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yp = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            upk.Project('', 'fadc_hitsum:Iteration$', 'Entry$ == %d' % ientry)
            xh = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yh = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gp = TGraph(xp, yp)
            gp.SetTitle('PMTSUM (%4d);sampling point [4ns/pt];PMTSUM [mV]' % ientry)
            gh = TGraph(xh, yh)
            gh.SetTitle('HITSUM (%4d);sampling point [4ns/pt];HITSUM [mV]' % ientry)

            c.cd(1)
            gp.Draw("apl")
            c.cd(2)
            gh.Draw("apl")
            c.Update()
            self.ask()

        return
    
    ## __________________________________________________
    def draw(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.exit(-1)

        print 'Draw "%s".' % ifn
        fin = TFile(ifn, 'read')
        upk = fin.Get('upk')

        upk.GetEntry(0)
        iq = upk.upk.GetDaqmode() % 10000    # input charge
        
        entries = upk.GetEntries()

        if self.debug:
            print entries

        h     = TH1D('h', 'h', 600, 0, 600)
        hdiff = TH1D('hdiff', 'hdiff', 2000, -1, 1)

        if self.graph:
            c1 = TCanvas('c1', 'c1', 1000, 500)

        for ientry in range(0, entries):
            upk.Project('', 'fadc_pmtsum:Iteration$', 'Entry$ == %d' % ientry)
            x = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            y = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            totq = 0    # integrated charge
            start = self.start
            end   = self.end
            for i in range(start, end+1):
                totq = totq + y[i]

            totq = -totq * 4 / 50    # convert into [pC]
            h.Fill(totq)
            hdiff.Fill(1-totq/iq)

            if self.graph:
                print 'Area : %8.2f [pC] / %f' % (totq, iq)
                g = TGraph(x, y)
                g.SetFillStyle(1001)
                g.SetTitle('QT output %3d [pC] (Entry# %4d)' % (iq, ientry))
                g.GetXaxis().SetTitle('sampling point')
                g.GetYaxis().SetTitle('FADC amplitude [mV]')
                g.GetXaxis().SetRangeUser(start - 5, end + 5)
                
                if ientry == 0:
                    g.Draw('apl')
                else:
                    g.Draw('apl')

                ymin = g.GetYaxis().GetXmin()
                ymax = g.GetYaxis().GetXmax()
                lbox = TBox(start - 5, ymin, start, ymax)
                rbox = TBox(end, ymin, end + 5, ymax)

                lbox.SetFillColor(2)
                rbox.SetFillColor(2)                
                lbox.SetFillStyle(3004)
                rbox.SetFillStyle(3004)
                lbox.Draw()
                rbox.Draw()
                c1.Update()
                self.ask()

        m1, m2 = h.GetMean(), hdiff.GetMean()
        r1, r2 = h.GetRMS() , hdiff.GetRMS()
        
        if self.histo:
            c2 = TCanvas('c2', 'c2', 1000, 500)
            c2.Divide(2, 1)
            c2.cd(1)
            h.SetTitle('QT output %4d [pC];FADC charge [pC];Entries [#/1pC]' % iq)
            h.Draw()
            c2.cd(2)
            hdiff.SetTitle('QT output %4d [pC];(1 - FADC/QT);Entries [#/1pC]' % iq)
            hdiff.Draw()
            print 'h    :\tI:%8.3f\tM:%8.3f\tR:%8.3f (%5.2f %%)' % (iq, m1, r1, r1/m1*100)
            print 'hdiff:\tI:%8.3f\tM:%8.3f\tR:%8.3f (%5.2f %%)' % (iq, m2, r2, r2/m2*100)

        if self.histo or self.graph:
            self.ask()

        return iq, m1, r1, m2, r2

    ## __________________________________________________
    def read(self, date, run, dir = '../exp/qtcalib/upk'):
        ifn = 'upk%d%04d.root' % (date, run)
        ifn = os.path.join(dir, ifn)
        return self.draw(ifn)

    ## __________________________________________________
    def hist(self, date, srun, erun, canvas = 0):
        print 'hist(%d, %d, %d)' % (date, srun, erun)
        hm = TH1D('hm', 'hm;FADC charge [pC];Entries [#/0.1pC]', 1000, 0.5, 100.5)
        hr = TH1D('hr', 'hr;FADC charge [pC];Entries [#/0.1pC]', 200, -9.5, 10.5)
        iq = 0    ## QT output
        for irun in range(srun, erun+1):
            q, m1, r1, m2, r2 = self.read(date, irun)
            if irun == srun:
                iq = q
            hm.Fill(m1)
            hr.Fill(r1)
        m1, m2 = hm.GetMean(), hr.GetMean()
        r1, r2 = hm.GetRMS() , hr.GetRMS()

        if not canvas == 0:
            hm.SetTitle('QT output %3d [pC] (Run# %d - %d)' % (iq, srun, erun))
            hr.SetTitle('QT output %3d [pC] (Run# %d - %d)' % (iq, srun, erun))
            canvas.Divide(2, 1)
            canvas.cd(1)
            hm.Draw();
            canvas.cd(2)
            hr.Draw();
            canvas.Update()
            print 'QT output\t%d [pC]' % iq
            print 'hm:\tM:%8.3f\tR:%8.3f (%5.2f %%)' % (m1, r1, r1/m1*100)
            print 'hr:\tM:%8.3f\tR:%8.3f (%5.2f %%)' % (m2, r2, r2/m2*100)
            print 'hr/hm:\t%5.2f %%' % (m2/m1*100)
            self.ask()
        return iq, m1, m2

    ## __________________________________________________    
    def hists(self, date, srun, erun, canvas = 0):
        print 'hists(%d, %d, %d)' % (date, srun, erun)
        g = TGraphErrors(0)
        for irun in range(srun, erun, 5):
            n = g.GetN()
            q, y, yerr = self.hist(date, irun, irun+4)
            g.SetPoint(n, q, y)
            g.SetPointError(n, 0, yerr)

        g.GetXaxis().SetTitle('QT output [pC]')
        g.GetYaxis().SetTitle('FADC response [pC]')
        g.GetXaxis().SetRangeUser(0, 650)
        g.GetYaxis().SetRangeUser(0, 100)
        
        if not canvas == 0:
            g.Draw('ap')
            canvas.Update()
            self.ask()

        return g
        
    ## __________________________________________________
    def graphs(self, date, srun, erun, canvas = 0):
        print 'graphs(%d, %d, %d)' % (date, srun, erun)
        g = TGraphErrors(0)
        for irun in range(srun, erun+1):
            q, m1, r1, m2, r2 = self.read(date, irun)
            n = g.GetN()
            g.SetPoint(n, q, m1)
            g.SetPointError(n, 0, r1)

        g.SetTitle('FADC calibration using QT')
        g.GetXaxis().SetTitle('Input Charge [pC]')
        g.GetYaxis().SetTitle('FADC response [pC]')
        g.GetXaxis().SetRangeUser(0, 650)
        g.GetYaxis().SetRangeUser(0, 100)
        if not canvas == 0:
            g.Draw("ap")
            canvas.Update()
            self.ask()
        return g

    ## __________________________________________________
    def correct(self, date, srun, erun, canvas = 0):
        print 'correct(%d, %d, %d)' % (date, srun, erun)
        g = TGraphErrors(0)
        for irun in range(srun, erun+1):
            q, m1, r1, m2, r2 = self.read(date, irun)
            n = g.GetN()
            cq = 0.97 * float(q) + 1.59
            g.SetPoint(n, cq, m1)
            g.SetPointError(n, 0, r1)

        g.SetTitle('FADC calibration using QT')
        g.GetXaxis().SetTitle('Input Charge (corrected) [pC]')
        g.GetYaxis().SetTitle('FADC response [pC]')
        g.GetXaxis().SetRangeUser(0, 650)
        g.GetYaxis().SetRangeUser(0, 100)
        if not canvas == 0:
            g.Draw("ap")
            canvas.Update()
            self.ask()
        return g

    ## __________________________________________________
    def ask(self):
        gSystem.ProcessEvents()
        ans = raw_input('Press "q" to quit > ')
        if ans in ['q' or 'Q']:
            sys.exit(-1)


## __________________________________________________
class Tektronix(object):
    ## __________________________________________________
    def __init__(self):
        pass
    
    ## __________________________________________________
    def ask(self):
        gSystem.ProcessEvents()
        ans = raw_input('Press "q" to quit > ')
        if ans in ['q' or 'Q']:
            sys.exit(-1)

    ## __________________________________________________
    def getQtOutput(self, run):
        qt = -1
        if   run in range( 6, 10+1): qt = 100
        elif run in range(11, 15+1): qt = 200
        elif run in range(16, 20+1): qt = 300
        elif run in range(21, 25+1): qt = 600
        return qt
            
    ## __________________________________________________
    def draw(self, ifn, qt, canvas = 0):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t File "%s" does not exist.\n' % ifn)
            sys.exit(-1)

        with open(ifn, 'r') as fin:
            lines = fin.readlines()
        h = lines[:15]    ## header
        d = lines[15:]    ## data

        ## calculate pedestal
        ped  = 0
        c    = 0
        for line in d:
            line = line.rstrip().split(',')
            if len(line) == 2:
                x = float(line[0]) * 1e9
                y = float(line[1]) * 1e3
                if -201 < x < -100:
                    print '[%3d]\t%8.3f\t%8.3f\t%8.3f' % (c, x, y, ped)
                    ped = ped + y
                    c   = c + 1
        ped /= c
        print 'Pedestal : %8.4f [mV]' % ped
            
        totq = 0
        g = TGraph(0)
        for line in d:
            line = line.rstrip().split(',')
            if len(line) == 2:
                x = float(line[0]) * 1e9
                y = float(line[1]) * 1e3
                y = y - ped
                #y =- ped
                
                if -100 <= x <= 500:
                    n = g.GetN()
                    g.SetPoint(n, x, y)
                    totq = totq + y

        totq = -totq * 0.4 / 50.

        print 'Integrated charge : %8.5f [pC]' % totq
        if not canvas == 0:
            c = TCanvas('c1', 'c1')
            g.SetTitle('QT output %3d [pC]' % qt)
            g.GetXaxis().SetTitle('Time [nsec]')
            g.GetYaxis().SetTitle('Amplitude [mV]')
            g.Draw('apl')
            c.Update()
        return qt, totq

    ## __________________________________________________
    def read(self, run, ch = 1, dir = '../exp/qtcalib/tektronix'):
        qt = self.getQtOutput(run)
        ifn = 'tek%04dCH%d.csv' % (run, ch)
        ifn = os.path.join(dir, ifn)
        return self.draw(ifn, qt)

    ## __________________________________________________
    def hist(self, srun, erun, canvas = 0):
        h = TH1D('h', 'h', 700, 0, 700)
        for irun in range(srun, erun+1):
            qt, totq = self.read(irun)
            h.Fill(totq)

        h.SetTitle('QT output %3d [pC] (Run %d - %d)' % (qt, srun, erun))
        h.GetXaxis().SetTitle('Oscilloscope response [pC]')
        h.GetYaxis().SetTitle('Entries [#/pC]')
        m = h.GetMean()
        r = h.GetRMS()
        print 'Mean: %8.3f\tRMS : %8.3f (%5.3f %%)' % (m, r, r/m*100)
        if not canvas == 0:
            h.Draw()
            canvas.Update()
            self.ask()
        return qt, m, r

    ## __________________________________________________    
    def hists(self, srun, erun, canvas = 0):
        print 'hists(%d, %d)' % (srun, erun)
        g = TGraphErrors(0)
        for irun in range(srun, erun, 5):
            n = g.GetN()
            q, y, yerr = self.hist(irun, irun+4)
            g.SetPoint(n, q, y)
            g.SetPointError(n, 0, yerr)

        g.SetTitle('QT calibration using Oscilloscope')            
        g.GetXaxis().SetTitle('QT output [pC]')
        g.GetYaxis().SetTitle('Oscilloscope response [pC]')
        g.GetXaxis().SetRangeUser(0, 650)
        g.GetYaxis().SetRangeUser(0, 650)
        
        if not canvas == 0:
            g.Draw('ap')
            canvas.Update()
            self.ask()

        return g

    ## __________________________________________________
    def graphs(self, srun, erun, canvas = 0):
        print 'graphs(%d, %d)' % (srun, erun)
        g = TGraphErrors(0)
        for irun in range(srun, erun+1):
            q, totq = self.read(irun)
            n = g.GetN()
            g.SetPoint(n, q, totq)

        g.SetTitle('QT calibration using Oscilloscope')
        g.GetXaxis().SetTitle('Input Charge [pC]')
        g.GetYaxis().SetTitle('Oscilloscope response [pC]')
        g.GetXaxis().SetRangeUser(0, 650)
        g.GetYaxis().SetRangeUser(0, 650)
        if not canvas == 0:
            g.Draw("ap")
            canvas.Update()
            self.ask()
        return g
    
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [t] ifn start end' % sys.argv[0]

    parser = OptionParser(usage)
    parser.add_option('-t', '--test',
                      action = 'store_true',
                      dest = 'test',
                      help = 'for test; [ifn] ([win start] [win end])')
    parser.add_option('--hist',
                      action = 'store_true',
                      dest = 'hist',
                      help = 'draw hist; [dir] [start] [end] ([win start] [win end])')
    parser.add_option('--hists',
                      action = 'store_true',
                      dest = 'hists',
                      help = 'draw hists; [dir] [start] [end] ([win start] [win end])')
    parser.add_option('--graphs',
                      action = 'store_true',
                      dest = 'graphs',
                      help = 'draw graphs; [dir] [start] [erun] ([win start] [win end])')
    parser.add_option('-c', '--compare',
                      action = 'store_true',
                      dest = 'compare',
                      help = 'compare graphs; [dir] [start] [end] ([win start] [win end])')
    parser.add_option('-o', '--tek',
                      action = 'store_true',
                      dest = 'tektronix',
                      help = 'draw tektronix; [dir] [start] [end] ([win start] [win end])')    
    parser.set_defaults(test    = False,
                        hist    = False,
                        hists   = False,
                        graphs  = False,
                        compare = False)
    (options, args) = parser.parse_args()

    start, end = 80, 100
    if options.test:
        if 0 < len(args) < 4:
            ifn = args[0]
            if len(args) > 1: start = int(args[1])
            if len(args) > 2: end   = int(args[2])
            g = Graph(start, end)
            g.graph = True
            g.histo = True
            #g.draw(ifn)
            g.checkWaveforms(ifn)
        else:
            print usage
    elif options.tektronix:
        gStyle.SetOptFit(1111)
        srun = int(args[0])
        erun = int(args[1])
        t = Tektronix()
        c = TCanvas('c', 'c')
        t.hist(srun, erun, c)
        # th = t.hists(srun, erun)
        # tg = t.graphs(srun, erun)
        # th.Draw('ap')
        # tg.SetMarkerColor(2)
        # tg.Draw('psame')
        # f1 = TF1('f1', '[0]*x + [1]', 0, 300)
        # f1.SetLineColor(4)
        # f1.SetParameters(1, 0)
        # tg.Fit(f1, '', '', 0, 600)
        # tg.Fit(f1, '', '', 0, 600)
        # c.Update()
        # t.ask()
        
    else:
        if 2 < len(args) < 6:
            date = int(args[0])
            srun = int(args[1])
            erun = int(args[2])
            if len(args) > 3: start = int(args[3])
            if len(args) > 4: end   = int(args[4])
            g = Graph(start, end)
            if options.hist:
                c = TCanvas('c', 'c', 1000, 500)
                g.hist(date, srun, erun, c)
            elif options.hists:
                c = TCanvas('c', 'c', 500, 500)
                g.hists(date, srun, erun, c)
            elif options.graphs:
                c = TCanvas('c', 'c', 500, 500)
                g.graphs(date, srun, erun, c)
            elif options.compare:
                gStyle.SetOptFit(1111)
                g = Graph(start, end)
                gg = g.graphs(date, srun, erun)
                gh = g.hists(date, srun, erun)
                gh.SetMarkerColor(2)
                gh.SetLineColor(2)
                c = TCanvas('c1', 'c1')
                gg.Draw('ap')
                gh.Draw('psame')

                f1 = TF1('f1', '[0]*x + [1]', 0, 300)
                f1.SetLineColor(4)
                f1.SetParameters(1, 0)
                gg.Fit(f1, '', '', 0, 600)
                gg.Fit(f1, '', '', 0, 600)
                c.Update()
                g.ask()
        else:
            print 'Usage: %s --hist date srun erun (start end)' % sys.argv[0]
    
    # ## ----------
    # g1 = Graph(100, 140)
    # c1 = TCanvas('c1', 'c1', 500, 500)
    # g1.graphs(121108,  1, 25, c1)

    # ## ----------
    # g3 = Graph(100, 200)
    # c3 = TCanvas('c3', 'c3', 500, 500)
    # g3.graphs(121108,  1, 25, c3)

    # ## ----------
    # g2 = Graph(80, 100)
    # c2 = TCanvas('c2', 'c2', 500, 500)
    # g2.graphs(121108, 26, 50, c2)
