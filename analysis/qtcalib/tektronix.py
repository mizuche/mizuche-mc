#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TVectorD, TGraph, TCanvas, TH1D, TGraphErrors, TBox
from ROOT import TF1, TLegend, TH2D

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit


## __________________________________________________
class Tektronix(object):
    ## __________________________________________________
    def __init__(self, date, runs, options):
        self.dir   = '../exp/qtcalib/'
        self.date  = date
        self.runs  = runs
        self.ifns  = []
        ch = options.ch
        for irun in runs:
            ifn = '%d/tek%04d%s.csv' % (int(date), int(irun), ch)
            ifn = os.path.join(self.dir, ifn)
            self.ifns.append(ifn)
        start = options.start
        stop  = options.stop
        self.setIntegrationRange(start, stop)
        self.debug = False
        self.graph = False
        self.histo = False
        self.nch   = None
        
    ## __________________________________________________
    def setIntegrationRange(self, start, stop):
        if start > stop:
            print 'Error:\tstart# (%d) > end# (%d)' % (start, stop)
            sys.exit(-1)
        self.start = start
        self.stop   = stop

    ## __________________________________________________
    def stop(self):
        gSystem.ProcessEvents()
        ans = raw_input('[stop]: \tPress "q" to quit > ')
        if ans in ['q' or 'Q']:
            sys.exit(-1)
            
    ## __________________________________________________
    def control(self, ientry):
        gSystem.ProcessEvents()
        ans = raw_input('Entry # %5d    Input entry# or [n]/p/q > ' % ientry)
        if   ans in ['q', 'Q']    : sys.exit(-1)
        elif ans in ['p', 'P']    : return ientry-1
        elif ans in ['n', 'N', '']: return ientry+1
        else: return int(ans)    

    ## __________________________________________________
    def read(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\tFile "%s" does not exist.\n' % ifn)
            sys.exit(-1)

        print '[read]\tRead "%s"' % ifn
        with open(ifn, 'r') as fin:
            lines = fin.readlines()

        labels = lines[13].rstrip().split(',')
        nch = len(labels) - 1
        self.nch = nch

        data = []
        datum = {}
        datum['time'] = 'time'
        for ich in range(0, nch):
            ilabel = 'ch%d' % (ich+1)
            datum[ilabel] = labels[ich+1]
        data.append(datum)
        #print data

        for line in lines[15:-1]:
            datum = {}
            line = line.rstrip().split(',')
            datum['time'] = line[0]
            for ich in range(0, nch):
                ilabel = 'ch%d' % (ich+1)
                datum[ilabel] = line[ich+1]
            data.append(datum)
        #print data
        self.data = data
        return data

    ## __________________________________________________
    def readfiles(self):
        ifns = self.ifns
        for ifn in ifns:
            self.read(ifn)
        return

    ## __________________________________________________
    def waveform(self, data, ich, color = 1, canvas = None, legend = None):
        sys.stderr.write('[waveform]:\tDraw waveform for CH%d\n' % ich)

        nch = self.nch
        if ich > nch:
            sys.stderr.write('Error:\tSpecified CH#(%d) > NCH#(%d).\n' % (ich, nch))
            sys.exit(-1)

        g = TGraph(0)
        labels = data[0]
        for datum in data[1:]:
            v = 0
            t = float(datum['time']) * 1e9
            ilabel = 'ch%d' % (ich)
            v = float(datum[ilabel]) * 1e3
            g.SetPoint(g.GetN(), t, v)
                #print datum
        g.GetXaxis().SetTitle(labels['time'] + ' [nsec]')
        g.GetYaxis().SetTitle('Amplitude [mV]')
        title = labels[ilabel]
        g.SetTitle(title)
        g.SetMarkerColor(color)
        g.SetLineColor(color)

        if canvas is None:
            c = TCanvas('cWaveform', 'cWaveform CH%d' % ich, 1000, 500)
            g.Draw('apl')
        else:
            if ich == 1 : g.Draw('apl')
            else : g.Draw('plsame')
            gSystem.ProcessEvents()

        if legend is None:
            l = TLegend(0.7, 0.1, 0.9, 0.3)
            l.AddEntry(g, '%s' % title, 'lp')
            l.Draw()
        else:
            legend.AddEntry(g, '%s' % title, 'lp')
                #legend.Draw()
            self.stop()
            return g
except AttributeError:
            sys.stderr.write('Error:\tNo data. Try "read(ifn)" first.\n')
            sys.exit(-1)

    ## __________________________________________________
    def waveforms(self):
        nch = self.nch
        c = TCanvas('cWaveform', 'cWaveform', 1000, 500)
        #frame = TH2D('h2', 'h2', 500, 0, 5000, 2000, -1000, 1000)
        #frame.Draw()
        l = TLegend(0.1, 0.7, 0.9, 0.9)
        l.SetFillColor(0)
        l.SetFillStyle(0)
        waves = []
        for ich in range(0, nch):
            g = self.waveform(ich+1, ich+1, c, l)
            waves.append(g)

        waves[0].Draw('ap')
        for g in waves[1:]:
            g.Draw('psame')
        l.Draw()
        self.stop()
        
    

## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [t] ifn (ch#)' % sys.argv[0]

    defdir = '../exp/qtcalib/'
    defch  = 'ALL'
    defstart = 80
    defstop  = 100
    
    parser = OptionParser(usage)
    parser.add_option('-l', '--list',
                      action = 'store_true',
                      dest   = 'ls',
                      help   = 'ls "%s"' % defdir)
    parser.add_option('-i', '--individual',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('--ch',
                      dest   = 'ch',
                      help   = 'set filename suffix [default = "%s"]' % defch)
    parser.add_option('--start',
                      dest = 'start',
                      help = 'set start point for integration [default = %d]' % defstart)
    parser.add_option('--stop',
                      dest = 'end',
                      help = 'set stop point for integration [default = %d]' % defstop)
    parser.set_defaults(ls         = False,
                        individual = False,
                        ch         = defch,
                        start      = defstart,
                        stop       = defstop)
    (options, args) = parser.parse_args()

    print options
    print len(args), args
    if options.ls:
        if len(args) > 0:
            date = args[0]
            defdir = os.path.join(defdir, date)
        com = 'ls %s' % defdir
        os.system(com)
        sys.exit(-1)
        
    if len(args) < 1:
        sys.stderr.write('Error:\tWrong number of arguments.\n')
        print usage
        sys.exit(-1)

    date = args[0]
    if options.individual:
        runs = args[1:]
    else:
        srun = int(args[1])
        erun = srun
        if len(args) > 2:
            erun = int(args[2])
        runs = [run for run in range(srun, erun+1)]
    print 'Runs:\t', runs

    t = Tektronix(date, runs, options)
    t.readfiles()
