#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TCanvas
from ROOT import TCanvas, TBox, TGaxis, TLegend
from ROOT import TH1D, TH2D, TGraph, TGraphErrors, TF1
from ROOT import TVectorD, Double

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from wave import Wave

## __________________________________________________
class Hist(Wave):
    ## __________________________________________________
    def __init__(self, ifns, cut = ""):
        self.file = __file__.split('/')[-1]
        Wave.__init__(self, ifns, False)
        self.cut = cut
        
    ## __________________________________________________
    def histNcharges(self, fin, draw = False):
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run = upk.upk.GetRunNum()
        cut = self.cut

        xmin, xmax, xbin = 0, 1000, 1000
        name  = 'hAtmNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hAtm  = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_ncharges', cut)

        name  = 'hFadcNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_ncharges/0.09', cut)

        name  = 'hDiffNcharges'
        title = '%s;DiffNcharges (ATM - FADC) [pC];Entries [#]' % name
        hDiff = TH1D(name, title, 200, -100, 100)
        upk.Project(hDiff.GetName(), "atm_ncharges - fadc_ncharges/0.088", cut);

        if draw:
            name = 'cHistNcharges'
            c = TCanvas(name, name, 350, 700)
            c.Divide(1, 2)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()
        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNpes(self, fin, draw = False):
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run = upk.upk.GetRunNum()
        cut = self.cut

        xmin, xmax, xbin = 0, 1000, 1000
        name  = 'hAtmNpes'
        title = '%s;Npes [p.e.];Entries [#]' % name
        hAtm  = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_npes', cut)

        name  = 'hFadcNpes'
        title = '%s;Npes [p.e.];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_npes/0.088', cut)

        name  = 'hDiffNpes'
        title = '%s;DiffNpes (ATM - FADC) [p.e.];Entries [#]' % name
        hDiff = TH1D(name, title, 1000, -500, 500)
        upk.Project(hDiff.GetName(), "atm_npes - fadc_npes/0.088", cut);

        if draw:
            name = 'cHistNpes'
            c = TCanvas(name, name, 350, 700)
            c.Divide(1, 2)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()

        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histNhits(self, fin, draw = False):
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run = upk.upk.GetRunNum()
        cut = self.cut

        xmin, xmax, xbin = 0, 200, 200
        name  = 'hAtmNhits'
        title = '%s;Nhits [#];Entries [#]' % name
        hAtm  = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hAtm.GetName(), 'atm_nhits', cut)

        name  = 'hFadcNhits'
        title = '%s;Nhits [#];Entries [#]' % name
        hFadc = TH1D(name, title, xbin, xmin, xmax)
        upk.Project(hFadc.GetName(), 'fadc_nhits', cut)

        name  = 'hDiffNhits'
        title = '%s;DiffNhits (ATM - FADC) [#];Entries [#]' % name
        hDiff = TH1D(name, title, 200, -100, 100)
        upk.Project(hDiff.GetName(), "atm_nhits - fadc_nhits", cut);

        if draw:
            name = 'cHistNhits'
            c = TCanvas(name, name, 350, 700)
            c.Divide(1, 2)
            c.cd(1)
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw()
            hFadc.SetLineColor(3)
            hFadc.SetFillColor(3)
            hFadc.Draw('same')

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM :  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC:  %d entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2)
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop()
        return fin, upk, hAtm, hFadc, hDiff
    
    ## __________________________________________________
    def histNtrg(self, fin, draw = False):
        upk = fin.Get('upk')
        upk.GetEntry(0)
        run = upk.upk.GetRunNum()

        name  = 'hAtmNtrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hAtm  = TH1D(name, title, 10, 0, 10)
        upk.Project(hAtm.GetName(), "atm_ntrg");
        
        name  = 'hFadcNtrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hFadc = TH1D(name, title, 10, 0, 10)
        upk.Project(hFadc.GetName(), "fadc_ntrg");

        name  = 'hDiffNtrg'
        title = '%s;DiffNtrg (ATM - FADC) [#];Entries [#]' % name
        hDiff = TH1D(name, title, 20, -10, 10)
        upk.Project(hDiff.GetName(), "atm_ntrg - fadc_ntrg");
        
        if draw:
            name = 'cHistNtrg'
            c = TCanvas(name, name, 350, 700)
            c.Divide(1, 2)
            c.cd(1).SetLogy()
            hAtm.SetLineColor(2)
            hAtm.SetFillColor(2)
            hAtm.SetFillStyle(1001)
            hAtm.Draw();
            hFadc.SetLineColor(4)
            hFadc.SetFillColor(4)
            hFadc.Draw('same');

            title = 'RUN#:%07d' % (run)
            l = TLegend(0.1, 0.8, 0.9, 0.9, title)
            l.SetFillColor(0)
            n = hAtm.GetEntries()
            m = hAtm.GetMean()
            r = hAtm.GetRMS()
            label = 'ATM : %d Entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hAtm, label, 'lf')
            n = hFadc.GetEntries()
            m = hFadc.GetMean()
            r = hFadc.GetRMS()
            label = 'FADC: %d Entries | %.2f #pm %.2f' % (n, m, r)
            l.AddEntry(hFadc, label, 'lf')
            l.Draw()

            c.cd(2).SetLogy()
            hDiff.SetLineColor(3)
            hDiff.SetFillColor(3)
            hDiff.SetFillStyle(1001)
            hDiff.Draw()
            
            self.stop();
        return fin, upk, hAtm, hFadc, hDiff

    ## __________________________________________________
    def histQTdc(self, ifn, draw = False):
        
        pass

    ## __________________________________________________
    def hist(self, item, draw = True):
        items = ['ncharges', 'npes', 'ntrg', 'waveform', 'nhits']
        if not item in items:
            sys.stderr.write('Error:\tGiven item (%s) is not in the list' % item)
            print items
            sys.exit()
        fins = self.reads()
        for fin in fins:
            if item == items[0]:
                self.histNcharges(fin, draw)
            if item == items[1]:
                self.histNpes(fin, draw)
            if item == items[2]:
                self.histNtrg(fin, draw)
            if item == items[3]:
                self.waveform(fin)
            if item == items[4]:
                self.histNhits(fin, draw)
        return
    
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [hliqptc] type date run_start# run_end#' % sys.argv[0]

    defdir = '../exp/cosmic/upk/'
    defcut = 'atm_ntrg ==1 && fadc_ntrg==1'
    
    parser = OptionParser(usage)
    parser.add_option('-l',
                      action = 'store_true',
                      dest   = 'lsdir',
                      help   = 'ls "%s"' % defdir)
    parser.add_option('-d',
                      action = 'store_true',
                      dest   = 'debug',
                      help   = 'set debug flag')
    parser.add_option('-f',
                      action = 'store_true',
                      dest   = 'filename',
                      help   = 'set filename')
    parser.add_option('-i',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('-q',
                      action = 'store_true',
                      dest   = 'ncharges',
                      help   = 'draw ncharges distribution')
    parser.add_option('-p',
                      action = 'store_true',
                      dest   = 'npes',
                      help   = 'draw npes distribution')
    parser.add_option('-t',
                      action = 'store_true',
                      dest = 'ntrg',
                      help = 'draw trg')
    parser.add_option('-c',
                      dest = 'cut',
                      help = 'set cut [default = "%s"]' % defcut)
    parser.set_defaults(lsdir      = False,
                        debug      = False,
                        filename   = False,
                        individual = False,
                        cut        = defcut,
                        ncharges   = False,
                        npes       = False,
                        waveform   = False,
                        ntrg       = False,
                        graph      = False)
    
    (options, args) = parser.parse_args()

    ifns = []
    if options.filename:
        if len(args) > 0:
            ifns = args

    else:
        if len(args) > 2:
            datadir  = '../exp/%s/upk' % args[0]
            date     = args[1]
            if options.individual:
                runs = args[2:]
            else:
                srun = int(args[2])
                erun = srun
                if len(args) > 3:
                    erun = int(args[3])
            runs = [run for run in range(srun, erun+1)]
            print 'Runs:\t', runs
            
            for run in runs:
                ifn = 'upk%03d%04d.root' % (int(date), int(run))
                ifn = os.path.join(datadir, ifn)
                ifns.append(ifn)
        else:
            print usage
                
    h = Hist(ifns)
    if options.lsdir:
        h.lists()        
    else:
        items = []
        if options.ncharges : items.append('ncharges')
        if options.npes     : items.append('npes')
        if options.ntrg     : items.append('ntrg')
        
        for item in items:
            h.hist(item, True)
            sys.exit(-1)
        h.hist('nhits', True)

