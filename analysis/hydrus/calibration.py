#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import datetime
import math

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='calibration.log',
                    filemode='w')
console = logging.StreamHandler()
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(module)s.%(funcName)-20s %(message)s')
console.setLevel(logging.INFO)
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle, gClient, gROOT
gStyle.SetTimeOffset(-788918400)
from ROOT import TFile, TChain, TCut, TTree
from ROOT import TH1D, TGraphErrors, TH2D
from ROOT import TPad, TCanvas

mizulib = '../../mizulib/'
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from read2 import Config, Read

## __________________________________________________
class Config(Config):
    pass

## __________________________________________________
class Calibration(Read):

    ## __________________________________________________
    def __init__(self, hprefix='calib'):
        self.logger = logging.getLogger('Calibation')
        self.logger.info('-' * 50)
        self.hprefix = hprefix
        Read.__init__(self, 'upk', 'upk')
        self.calib = TChain('calib')
        
    ## __________________________________________________
    def init_histograms(self, params, nwindow=Config.nch):
        self.logger.info('initialize %d histograms', nwindow)
        xmin = params['xmin']
        xmax = params['xmax']
        xbin = params['xbin']
        hnamefmt = params['hname']
        htitlefmt = params['htitle']
        hs = []
        for iwindow in range(nwindow):
            hname = self.hprefix + '.' + hnamefmt % iwindow
            htitle = htitlefmt % hname
            h = TH1D(hname, htitle, xbin, xmin, xmax)
            hs.append(h)
        return hs

    ## __________________________________________________
    def delete_histograms(self, hs):
        self.logger.info('delete histograms')
        for h in hs:
            h.Delete()
        return

    ## __________________________________________________
    def init_hqdcs(self, nwindow=Config.nch):
        self.logger.info('initialize %d histograms', nwindow)
        xmin, xmax, xbin = Config.atm_qdc
        hs = []
        for iwindow in range(nwindow):
            hname = 'hWindow%03d' % (iwindow)
            htitle = '%s;qdc [count];Entries [#]' % hname
            h = TH1D(hname, htitle, xbin, xmin, xmax)
            hs.append(h)
        return hs
            
    ## __________________________________________________
    def fill_hqdcs(self, hit, hqdcs):
        win  = hit.GetWindow()
        qdc0 = hit.GetQdc0()
        qdc1 = hit.GetQdc1()
        #self.logger.debug('Window : %3d | QDC0 : %.2f | QDC1 : %.2f | QDC : %.2f', win, qdc0, qdc1, qdc1 - qdc0)
        hqdcs[win].Fill(qdc1 - qdc0)
        return

    ## __________________________________________________
    def draw_hqdcs(self):
        wx, wy = Config.disp
        hqdcs = self.hqdcs

        cname = 'c{0:d}'.format(self.upkrun)
        c = TCanvas(cname, cname, wx/2, wy)

        ofn = os.path.join('../exp/led/calib', 'calib%d.txt' % self.upkrun)
        if os.path.exists(ofn):
            self.logger.warning('file "%s" already exists', ofn)
            self.logger.warning('do you want to overwrite ? (y/[n]) >')
            ans = raw_input('')
            if not ans in ['y', 'Y']:
                self.logger.error('skip')
                return
            
        self.logger.info('write result to "%s"', ofn)
        with open(ofn, 'w') as fout:
            for i, h in enumerate(hqdcs):
                h.GetXaxis().SetRangeUser(0, 1000);
                h.Draw()
                mean = 0
                rms = 0
                pe = 0
                gain = 0
                hentries = int(h.GetEntries())
                if hentries > 0:
                    mean = h.GetMean()
                    rms = h.GetRMS()
                    if mean > 0 and rms > 0:
                        pe = (mean / rms) ** 2
                        gain = (rms ** 2) / mean * (0.25e-12 / 1.6e-19)
                        line = ['{0:3d}\t{1}\t{2:d}'.format(i, h.GetTitle(), hentries),
                                '{0:f}\t{1:f}'.format(mean, rms),
                                '{0:f}\t{1:e}'.format(pe, gain),
                                '{0:d}\t{1:d}\t{2:d}'.format(self.upkrun, self.fvstat, self.daqmode)]
                        line = ('\t').join(line)
                        self.logger.info(line)
                        fout.write(line + '\n')
                c.Modified()
                c.Update()
                gSystem.ProcessEvents()
                #self.stop()
        self.delete_histograms(hqdcs)

    ## __________________________________________________
    def process(self):
        self.logger.info('start')
        ch = self.chain
        nentries = ch.GetEntries()
        self.logger.info('nentries : %d', nentries)

        self.hqdcs = self.init_hqdcs()

        for ientry in range(nentries):
            if ientry % 100 == 0:
                self.logger.info('processed : %d / %d', ientry, nentries)
            ch.GetEntry(ientry)
            upkrun = ch.upk.GetUpkRun()
            daqmode = ch.upk.GetDaqmode()
            fvstat = ch.upk.GetFvstat()
            atm_ntrg = ch.upk.GetAtmNtrg()
            if atm_ntrg == 1:
                atm_nhits = ch.upk.GetAtmNhits()
                for ihit in range(atm_nhits):
                    hit = ch.upk.GetHit(ihit)
                    self.fill_hqdcs(hit, self.hqdcs)
        
        self.upkrun = upkrun
        self.fvstat = fvstat
        self.daqmode = daqmode
        ch.Reset()

    ## __________________________________________________
    def create_rootfile(self, ifn):
        if not os.path.exists(ifn):
            self.logger.error('file "%s" does not exist. quit.', ifn)
            sys.exit(-1)

        head, tail = os.path.split(ifn)
        
        t = TTree('calib', tail)
        branches = ['win/I',
                    'name/C',
                    'nentry/I',
                    'mean/D',
                    'rms/D',
                    'pe/D',
                    'gain/D',
                    'upkrun/I',
                    'fvstat/I',
                    'daqmode/I']
        bd = (':').join(branches)
        t.ReadFile(ifn, bd)
        
        ofn = ifn.replace('.txt', '.root')
        self.logger.info('recreate "%s"', ofn)
        f = TFile(ofn, 'recreate')
        t.Write()
        f.Close()
        
    ## __________________________________________________
    def read_calibfile(self, ifn):
        if not os.path.exists(ifn):
            self.logger.error('file "%s" does not exist. quit.', ifn)
            sys.exit(-1)

        self.logger.info('add "%s"', ifn)
        self.calib.Add(ifn)
        return

    ## __________________________________________________
    def init_hpes(self, nwindow=Config.nch):
        self.logger.info('initialize %d hpes', nwindow)
        params = {'xmin':0,
                  'xmax':300,
                  'xbin':300,
                  'hname':'hPeWin%03d',
                  'htitle':'%s;P.E. [#];Entries [#]'
                  }
        return self.init_histograms(params, nwindow)

    ## __________________________________________________
    def init_hgains(self, nwindow=Config.nch):
        self.logger.info('initialize %d hgains', nwindow)
        params = {'xmin':0,
                  'xmax':300,
                  'xbin':3000,
                  'hname':'hGainWin%03d',
                  'htitle':'%s;Gain [/1e6];Entries [#]'
                  }
        return self.init_histograms(params, nwindow)

    ## __________________________________________________
    def fill_hpes(self, calib, hpes):
        win = calib.win
        pe = calib.pe
        hpes[0].Fill(pe)
        hpes[win].Fill(pe)
        return

    ## __________________________________________________
    def fill_hgains(self, calib, hgains):
        win = calib.win
        gain = calib.gain/1e6
        hgains[0].Fill(gain)
        hgains[win].Fill(gain)
        return

    ## __________________________________________________
    def process_calibfile(self):
        ch = self.calib
        nentries = ch.GetEntries()
        self.logger.info('nentries : %d', nentries)

        self.hpes = self.init_hpes()
        self.hgains = self.init_hgains()

        self.gwinpe = TGraphErrors(0)
        self.gwinpe.SetTitle(';Window [#];P.E. [#]')
        
        self.gwingain = TGraphErrors(0)
        self.gwingain.SetTitle(';Window [#];Gain [/1e6]')

        for ientry in range(nentries):
            ch.GetEntry(ientry)
            win = ch.win
            pe = ch.pe
            if 0 < win < 165 and pe > 0:
                self.fill_hpes(ch, self.hpes)
                self.fill_hgains(ch, self.hgains)

                n = self.gwinpe.GetN()
                self.gwinpe.SetPoint(n, win, ch.pe)
                self.gwinpe.SetPointError(n, 0, math.sqrt(ch.pe))

                n = self.gwingain.GetN()
                self.gwingain.SetPoint(n, win, ch.gain/1e6)
                self.gwingain.SetPointError(n, 0, ch.gain * 0.15 / 1e6)
                
        
        return
    
    ## __________________________________________________
    def draw_hpes(self, canvas=None):
        if not canvas:
            canvas = TCanvas('cPes', 'cPpes')

        for h in self.hpes:
            canvas.cd()
            h.Draw()
            canvas.Update()
            self.stop()
            canvas.Clear()

        self.delete_histograms(self.hpes)

    ## __________________________________________________
    def draw_hgain(self, canvas=None):
        if not canvas:
            canvas = TCanvas('cGains', 'cGains')
            
        for h in self.hgains:
            canvas.cd()
            h.Draw()
            canvas.Update()
            self.stop()
            canvas.Clear()
            
        self.delete_histograms(self.hgains)

    ## __________________________________________________
    def draw_calibfile(self):

        wx, wy = Config.disp
        c1 = TCanvas('cCalibfileSummary', 'cCalibfileSummary', wx, wy)
        c1.Divide(2, 2)

        c1.cd(1)
        self.gwinpe.Draw('ap')
        c1.cd(2)
        self.gwingain.Draw('ap')
        
        c1.cd(3)
        self.hpes[0].Draw()
        c1.cd(4)
        self.hgains[0].Draw()

        c1.Update()

        c2 = TCanvas('cWindow', 'cWindow', wx, wy/2)
        c2.Divide(2, 1)
        for i, h in enumerate(self.hpes):
            c2.SetTitle('cWindow%03d' % i)
            c2.cd(1)
            h.Draw()
            c2.cd(2)
            self.hgains[i].Draw()
            c2.Update()
            self.stop()

        return
        
## __________________________________________________
if __name__ == '__main__':

    usage = '%prog [options] upk_files'
    usage += '\n'
    usage += '\n'
    usage += '  Example to create CALIBFILE.txt from UPKFILE.root.'
    usage += '  You can use -b option\n'
    usage += '    $ ./%prog ../exp/led/upk/upk1307240001.root\n'
    usage += '    $ ./%prog ../exp/led/upk/upk130724000*.root (wildcard can be used)\n'
    usage += '    $ ./%prog -b ../exp/led/upk/upk130724000*.root (batch-mode; no TCanvas)\n'
    usage += '\n'
    usage += '  Example to create CALIBFILE.root from CALIBFILE.txt.'
    usage += '  No options needed\n'
    usage += '    $ ./%prog ../exp/led/calib/calib1307240001.txt\n'
    usage += '    $ ./%prog ../exp/led/calib/calib130724000*.txt\n'
    usage += '\n'
    
    
    parser = OptionParser(usage)
    parser.set_description('Script for LED calibration')
    parser.add_option('-b', '--batch',
                      dest='batch',
                      action='store_true',
                      help='run as batch mode')

    parser.set_defaults(batch=False,
                        each=False,
                        ofn='tmpupk.root')
    (options, args) = parser.parse_args()

    gROOT.SetBatch(options.batch)

    if len(args) < 1:
        parser.error('Wrong number of arguments. (%d)' % len(args))

    calib = Calibration()
    for ifn in args:
        root, ext = os.path.splitext(ifn)
        head, tail = os.path.split(ifn)
        if ext in ['.txt']:
            calib.create_rootfile(ifn)
        elif head in ['../exp/led/upk']:
            calib.add(ifn)
            calib.process()
            calib.draw_hqdcs()
        elif head in ['../exp/led/calib']:
            calib.read_calibfile(ifn)
        else:
            calib.stop()

    if head in ['../exp/led/calib']:
        calib.process_calibfile()
        calib.draw_calibfile()
