#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import time

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle, gROOT
gStyle.SetTimeOffset(-788918400)
from ROOT import THStack
from ROOT import TGraph, TMultiGraph
from ROOT import TCanvas, TLegend

mizulib = '../../mizulib/'
#gSystem.Load(mizulib+'libT2KWCUnpack.so')
gSystem.Load(mizulib+'libT2KWCData.so')
from ROOT import T2KWCEvent
from ROOT import T2KWCSimParticle
from ROOT import T2KWCSimVertex
from ROOT import T2KWCNeutInfo
from ROOT import T2KWCPMTHit

from read2 import Read, Config

## __________________________________________________
class Mc(Read):
    ## __________________________________________________
    def __init__(self, name, tname):
        '''
        Initializaiton
        '''
        Read.__init__(self, name, tname)

    ## __________________________________________________
    def process_beambk(self):
        ch = self.chain
        nentries = ch.GetEntries()
        print ch.GetName(), nentries

        ## search values of absmoms in this chain
        absmoms = []
        for ientry in range(nentries):
            ch.GetEntry(ientry)
            nparticles = ch.mc.NSimParticles()
            for iparticle in range(nparticles):
                particle = ch.mc.GetSimParticle(iparticle)
                absmom = particle.Absmom()
                absmoms.append(round(absmom))
        moms = sorted(set(absmoms))
        print moms

        name = self.get_object_name()
        hname = '{0}.hId%02d'.format(name)
        htitle = 'hId%02d;Id [#];Entries [#]'
        hIds = self.init_th1d(hname, htitle,
                              Config.window, len(moms))

        hs = THStack('hs', 'hs')
        for ihist in range(len(moms)):
            cut = 'abs(absmom - {0}) < 1'.format(moms[ihist])
            htitle = '{0} P={1:.1f} MeV'.format(hIds[ihist].GetTitle(), moms[ihist])
            hIds[ihist].SetTitle(htitle)
            ch.Project(hIds[ihist].GetName(), 'id', cut)
            color = ihist % 7 + 2
            hIds[ihist].SetLineColor(color)
            hIds[ihist].SetMarkerColor(color)
            hIds[ihist].SetFillColor(color)
            hs.Add(hIds[ihist])

        c = TCanvas('c', 'c')
        hs.Draw()
        c.BuildLegend()
        self.stop()

        
        


    


## __________________________________________________
if __name__ == '__main__':

    usage = '%prog mc_root_files'
    usage += '\n'
    usage += '  Ex1: %prog ../mc/beambk/withFV/muon_bk*00.root\n'
    usage += '  Ex2: %prog ../mc/beambk/withFV/neutron_bk*00.root\n'
    usage += '  Ex3: %prog ../mc/beambk/woFV/gamma_bk*00.root\n'
    usage += '\n'

    parser = OptionParser(usage)

    parser.add_option('-b', '--batch',
                      action='store_true',
                      dest='batch',
                      help='run ROOT in batch mode')
    parser.set_defaults(batch=False)

    (options, args) = parser.parse_args()

    gROOT.SetBatch(options.batch)
    
    if len(args) < 1:
        parser.error('Wrong number of arguments. (%d)\n' % len(args))
    
    m = Mc('mc', 'mc')
    for ifn in args:
        m.add(ifn)

    print m
    m.process_beambk()
    # m.draw_waveform()
