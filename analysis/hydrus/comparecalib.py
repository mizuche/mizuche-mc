#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import datetime
import math

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='calibration.log',
                    filemode='w')
console = logging.StreamHandler()
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(module)s.%(funcName)-20s %(message)s')
console.setLevel(logging.INFO)
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle, gClient, gROOT
gStyle.SetTimeOffset(-788918400)
from ROOT import TFile, TChain, TCut, TTree
from ROOT import TH1D, TGraphErrors, TH2D, TMultiGraph, THStack
from ROOT import TPad, TCanvas, TLegend

mizulib = '../../mizulib/'
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

#from read2 import Config, Read
from read2 import Config
from calibration import Calibration

## __________________________________________________
class CompareCalibration(Calibration):

    ## __________________________________________________
    def __init__(self):
        self.logger = logging.getLogger('CompareCalibation')
        self.logger.info('-' * 50)
        
    ## __________________________________________________
    def compare(self, calibs):

        mgWinPes = TMultiGraph('mgWinPes', 'mgWinPes')
        mgWinGains = TMultiGraph('mgWinGains', 'mgWinGains')

        hsPes = []
        hsGains = []

        lwinpes = TLegend(0.1, 0.7, 0.5, 0.9)
        lwingains = TLegend(0.1, 0.7, 0.5, 0.9)
        lpes = TLegend(0.1, 0.7, 0.5, 0.9)
        lgains = TLegend(0.1, 0.7, 0.5, 0.9)

        ls = [lwinpes, lwingains, lpes, lgains]
        for l in ls:
            l.SetFillStyle(0)
            l.SetBorderSize(0)

        for i, calib in enumerate(calibs):
            calib.gwinpe.SetMarkerColor(i+2)
            calib.gwinpe.SetLineColor(i+2)
            mgWinPes.Add(calib.gwinpe, 'p')
            lwinpes.AddEntry(calib.gwinpe, calib.hprefix, 'p')

            calib.gwingain.SetMarkerColor(i+2)
            calib.gwingain.SetLineColor(i+2)
            mgWinGains.Add(calib.gwingain, 'p')
            lwingains.AddEntry(calib.gwingain, calib.hprefix, 'p')

            calib.hpes[0].SetLineColor(i+2)
            hsPes.append(calib.hpes[0])
            lpes.AddEntry(calib.hpes[0], calib.hprefix, 'l')
            
            calib.hgains[0].SetLineColor(i+2)
            hsGains.append(calib.hgains[0])
            lgains.AddEntry(calib.hgains[0], calib.hprefix, 'l')
        
        wx, wy = Config.disp
        c = TCanvas('cCompare', 'cCompare', wx, wy)
        c.Divide(2, 2)
        c.cd(1)
        mgWinPes.Draw('a')
        c.cd(2)
        mgWinGains.Draw('a')
        c.cd(3).SetLogy(1)
        for i, h in enumerate(hsPes):
            h.GetXaxis().SetRangeUser(0, 100)
            gopt = '' if i == 0 else 'same'
            h.DrawNormalized(gopt)
        c.cd(4).SetLogy(1)
        for i, h in enumerate(hsGains):
            h.GetXaxis().SetRangeUser(0, 10)
            gopt = '' if i == 0 else 'same'
            h.DrawNormalized(gopt)

        for i, l in enumerate(ls):
            c.cd(i+1)
            l.Draw()
            
        self.stop()
        c.Update()
        
        
## __________________________________________________
if __name__ == '__main__':
    usage = '%prog [options]'

    calibdir = '../exp/led/calib/'
    
    calib1 = Calibration('calib20130723LED3000mV')
    ifn = os.path.join(calibdir, 'calib1307230001.root')
    calib1.read_calibfile(ifn)
    calib1.process_calibfile()

    calib2 = Calibration('calib20130724LED3000mV')
    for run in range(2, 5):
        ifn = os.path.join(calibdir, 'calib130724%04d.root' % run)
        calib2.read_calibfile(ifn)
    calib2.process_calibfile()

    calib3 = Calibration('calib20130724')
    for run in range(5, 10):
        ifn = os.path.join(calibdir, 'calib130724%04d.root' % run)
        calib3.read_calibfile(ifn)
    calib3.process_calibfile()

    calib4 = Calibration('calib20121204')
    for run in range(27, 31):
        ifn = os.path.join(calibdir, 'calib121204%04d.root' % run)
        calib4.read_calibfile(ifn)
    calib4.process_calibfile()

    calib5 = Calibration('calib20121126')
    for run in range(24, 27):
        ifn = os.path.join(calibdir, 'calib121126%04d.root' % run)
        calib5.read_calibfile(ifn)
    calib5.process_calibfile()


    # comp1 = CompareCalibration()
    # comp1.compare([calib1, calib2, calib3])
    
    comp2 = CompareCalibration()
    comp2.compare([calib3, calib4, calib5])
    
