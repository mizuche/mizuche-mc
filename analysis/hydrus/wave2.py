#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import time

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='debug_wave.log',
                    filemode='w')
console = logging.StreamHandler()
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(module)s.%(funcName)-20s %(message)s')
console.setLevel(logging.INFO)
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

from ROOT import gSystem, gStyle, gROOT
gStyle.SetTimeOffset(-788918400)
from ROOT import TGraph, TMultiGraph
from ROOT import TCanvas, TLegend

mizulib = '../../mizulib/'
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from read2 import Read, Config

## __________________________________________________
class Wave(Read):
    ## __________________________________________________
    def __init__(self, name, tname):
        '''
        Initialization
        '''
        Read.__init__(self, name, tname)
        self.logger = logging.getLogger('Wave')
        self.logger.debug('-' * 50)

    ## __________________________________________________
    def convert_fadc_mv(self, count, inverse=False):
        '''
        convert FADC count into voltage [mV] using default value.
        '''
        if not inverse:
            return count * 0.489
        else:
            return count / 0.489

    ## __________________________________________________
    def convert_fadc_nsec(self, count, inverse=False):
        '''
        convert FADC sampling point into time [nsec]
        250MS = 4ns/count
        '''
        if not inverse:
            return count * 4.
        else:
            return count / 4.

    ## __________________________________________________
    def convert_fadc_pc(self, area, inverse=False):
        '''
        convert integrated FADC area into charge [pC]
        '''
        if not inverse:
            return area * 0.489 * 4. / 50.
        else:
            raise NotImplemented()

    ## __________________________________________________
    def process_waveform(self):
        ch = self.chain
        nentries = ch.GetEntries()

        ## Create TCanvas
        wx, wy = Config.disp
        c = TCanvas('cWaveforms', 'cWaveforms', wx, wy)
        c.Divide(2, 2)

        ## Event Loop
        for ientry in range(nentries):
            ch.GetEntry(ientry)
            ## Get run# and spill# of the entry
            runnum = ch.runnum
            spillnum = ch.spillnum

            htitle = 'RUN{0:07d} Spill{1:05d};nsamp [count];FADC [count]'.format(runnum, spillnum)
            data0 = ch.raw0
            data1 = ch.raw1
            data2 = ch.raw2
            data3 = ch.raw3
            
            ## fix number of samplings to raw0 data
            nsamp = len(data0) - 100
            
            ## calculate pedestal for each FADC data
            ped0 = self.calc_pedestal(data0, nsamp, nped=100)
            ped1 = self.calc_pedestal(data1, nsamp, nped=100)
            ped2 = self.calc_pedestal(data2, nsamp, nped=100)
            ped3 = self.calc_pedestal(data3, nsamp, nped=100)

            ## search HIT edges
            edges1 = self.search_edges(data1, nsamp, ped1, thrd=50, width=50)
            edges2 = self.search_edges(data2, nsamp, ped2, thrd=10, width=50)
            edges3 = self.search_edges(data3, nsamp, ped3, thrd=100, width=300)

            ## integrate FADC data using edges
            sums0 = self.integrate_fadc(data0, nsamp, ped0, edges1, -18, 9)
            sums2 = self.integrate_fadc(data2, nsamp, ped2, edges2, -18, 9)
            sums3 = self.integrate_fadc(data3, nsamp, ped3, edges3, -18, 9)

            ## average FADC data using edges
            avgs1 = self.average_fadc(data1, nsamp, ped1, edges1, 6, 45)

            if self.convert:
                htitle = 'RUN{0:07d} Spill{1:05d};time [nsec];FADC [mV]'.format(runnum, spillnum)
                data0 = [self.convert_fadc_mv(d) for d in ch.raw0]
                data1 = [self.convert_fadc_mv(d) for d in ch.raw1]
                data2 = [self.convert_fadc_mv(d) for d in ch.raw2]
                data3 = [self.convert_fadc_mv(d) for d in ch.raw3]
                ped0 = self.convert_fadc_mv(ped0)
                ped1 = self.convert_fadc_mv(ped1)
                ped2 = self.convert_fadc_mv(ped2)
                ped3 = self.convert_fadc_mv(ped3)
                sums0 = [self.convert_fadc_pc(s) for s in sums0]
                sums2 = [self.convert_fadc_pc(s) for s in sums2]
                sums3 = [self.convert_fadc_pc(s) for s in sums3]

                
            ## fill TGraph
            mg0, leg0 = self.fill_waveform('mg0', 'PMTSUM:{0}'.format(htitle), data0, nsamp, ped0, edges1)
            mg1, leg1 = self.fill_waveform('mg1', 'HITSUM:{0}'.format(htitle), data1, nsamp, ped1, edges1)
            mg2, leg2 = self.fill_waveform('mg2', 'RAW2:{0}'.format(htitle), data2, nsamp, ped2, edges2)
            mg3, leg3 = self.fill_waveform('mg3', 'RAW3:{0}'.format(htitle), data3, nsamp, ped3, edges3)

            for num in sums0:
                leg0.AddEntry('', '{0:5.2f}'.format(num), 'p')
            for num in sums2:
                leg2.AddEntry('', '{0:5.2f}'.format(num), 'p')
            for num in sums3:
                leg3.AddEntry('', '{0:5.2f}'.format(num), 'p')
            for num in avgs1:
                leg1.AddEntry('', '{0:5.2f}'.format(num), 'p')
                
            title = os.path.join(Config.plot_dir, 'cWave{0:07d}_s{1:05d}.png'.format(runnum, spillnum))
            c.SetTitle(title)
            c.cd(1)
            mg0.Draw('apl')
            leg0.Draw()
            c.cd(2)
            mg1.Draw('apl')
            leg1.Draw()
            c.cd(3)
            mg2.Draw('apl')
            leg2.Draw()
            c.cd(4)
            mg3.Draw('apl')
            leg3.Draw()
            c.cd()
            c.Modified()
            c.Update()

            self.logger.info('draw : {0}'.format(c.GetTitle()))
            if self.save:
                c.SaveAs(c.GetTitle())
            if self.interactive:
                self.stop()
            else:
                time.sleep(1)
            #self.stop()
            #self.graws = graws
        return graws

    ## __________________________________________________
    def calc_pedestal(self, fadc_data, nsamp, nped):
        '''
        Calculate FADC pedestal value using first NPED points of FADC_DATA.
        Returns pedestal.
        No unit convertion.
        '''
        if nsamp < nped:
            self.logger.error('Length of data is not enough. (%d < %d)'.format(nsamp, nped))
            raise NotImplemented()
        
        data = [fadc_data[ip] for ip in range(nped)]
        average = float(sum(data))/len(data)
        return average

    ## __________________________________________________
    def search_edges(self, fadc_data, nsamp, ped, thrd, width):
        '''
        Get edge points that exceeds THRD.
        Set pedestal value if FADC_DATA is raw value.
        Set THRD in positive number.
        Returns edges in list.
        No unit conversion.
        '''
        fall_edges = []
        rise_edges = []

        if not self.convert:
            thrd = self.convert_fadc_mv(thrd, inverse=True)

        ## search the point that is larger than threshold
        for ip in range(nsamp):
            data = fadc_data[ip] - ped
            if data < -thrd:
                edge = ip
                if fadc_data[ip-1] > fadc_data[ip+1]:
                    fall_edges.append(edge)
                else:
                    rise_edges.append(edge)
        ## sort edges, duration btw edges > 50 points
        sorted_edges = []
        if len(fall_edges) > 0:
            edge0 = fall_edges[0]
            sorted_edges.append(edge0)
            for edge in fall_edges:
                if not edge in range(edge0, edge0 + width):
                    edge0 = edge
                    sorted_edges.append(edge0)
        return sorted_edges

    ## __________________________________________________
    def integrate_fadc(self, fadc_data, nsamp, ped, edges, lower, upper):
        '''
        calculate integrated area in [lower, upper].
        returns areas in list.
        No unit conversion.
        '''
        integrals = []
        for edge in edges:
            tmp = []
            for i in range(edge + lower, edge + upper):
                data = fadc_data[i] - ped
                tmp.append(data)
            integrals.append(sum(tmp))
        return integrals

    ## __________________________________________________
    def average_fadc(self, fadc_data, nsamp, ped, edges, lower, upper):
        '''
        calculate average in [lower, upper].
        returns averages in list.
        No unit conversion.
        '''
        averages = []
        for edge in edges:
            tmp = []
            for i in range(edge + lower, edge + upper):
                data = fadc_data[i] - ped
                tmp.append(data)
            average = float(sum(tmp)) / len(tmp)
            averages.append(average)
        return averages

    ## __________________________________________________
    def fill_waveform(self, name, title, fadc_data, nsamp, ped, edges):
        '''
        Return TGraphs filled with FADC_DATA
        '''
        graw = TGraph(0)
        gped = TGraph(0)
        gfadc = TGraph(0)

        for isamp in range(nsamp):
            time = isamp
            volt = fadc_data[isamp]
            graw.SetPoint(graw.GetN(), time, volt)
            gped.SetPoint(gped.GetN(), time, ped)
            gfadc.SetPoint(gfadc.GetN(), time, volt - ped)
        
        gedge = TGraph(0)
        for iedge in edges:
            time = iedge
            volt = fadc_data[iedge]
            gedge.SetPoint(gedge.GetN(), time, volt - ped)
        gedge.SetMarkerStyle(23)
        # gedge.SetMarkerSize(3)
        nedges = len(edges)
        ltitle = 'FADC2MV : {0}'.format(self.convert)
        leg = TLegend(0.1, 0.3, 0.5, 0.7, ltitle)
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)
        labels = ['raw',
                  'ped : {0:5.2f}'.format(ped),
                  'fadc',
                  'edge : {0}'.format(nedges)
                  ]
        mg = TMultiGraph(name, title)
        graphs = [graw, gped, gfadc, gedge]
        ng = len(graphs)
        for i, g in enumerate(graphs):
            color = i + 1
            g.SetLineColor(color)
            g.SetMarkerColor(color)
            if g.GetN() > 0:
                #gopt = 'p' if i >=3 else 'pl'
                mg.Add(g, 'pl')
                leg.AddEntry(g, labels[i], 'lp')
        return mg, leg
    
## __________________________________________________
if __name__ == '__main__':

    usage = '%prog wave_root_files'
    usage += '\n'
    usage += '  Ex1: %prog ../exp/beam/wave/wave0490001.root'
    usage += '\n'

    parser = OptionParser(usage)
    
    parser.add_option('-b', '--batch',
                      action='store_true',
                      dest='batch',
                      help='run ROOT in batch mode')
    parser.add_option('-c', '--convert',
                      action='store_true',
                      dest='convert',
                      help='draw plot in nsec-mV')
    parser.add_option('-s', '--save',
                      action='store_true',
                      dest='save',
                      help='save canvas')
    parser.add_option('-i',
                      action='store_true',
                      dest='interactive',
                      help='interactive mode')
    parser.add_option('-a',
                      action='store_false',
                      dest='interactive',
                      help='non-interactive mode')
    parser.set_defaults(batch=False,
                        convert=False,
                        save=False,
                        interactive=False)

    (options, args) = parser.parse_args()

    gROOT.SetBatch(options.batch)
    
    if len(args) < 1:
        parser.error('Wrong number of arguments. (%d)\n' % len(args))
    
    w = Wave('fadc', 'fadc')
    for ifn in args:
        w.add(ifn)

    w.convert = options.convert
    w.save = options.save
    w.interactive = options.interactive
    print w
    w.process_waveform()
    w.draw_waveform()
