#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os
from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem
from ROOT import TFile

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

## __________________________________________________
class Read(object):
    ## __________________________________________________
    def __init__(self, ifns, debug = False):
        self.file = __file__.split('/')[-1]
        self.ifns  = ifns
        self.debug = debug

    ## __________________________________________________
    def read(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.exit(-1)
        func = '%s:%s' % (self.file, 'read')
        print '[%s]\tRead "%s"' % (func, ifn)
        fin = TFile(ifn, 'read')
        return fin

    ## __________________________________________________
    def reads(self):
        ifns = self.ifns
        fins = []
        for ifn in ifns:
            fin = self.read(ifn)
            fins.append(fin)
        self.fins = fins
        # if self.debug:
        #     for fin in fins:
        #         print fin.GetName()
        return fins

    ## __________________________________________________
    def stop(self):
        func = '%s:%s' % (self.file, 'stop')
        gSystem.ProcessEvents()
        ans = raw_input('[%s]\tStopped. Press "q" to quit > ' % (func))
        if ans in ['q', 'Q']:
            sys.exit(-1)
        elif ans in ['.', '.q', 'Q']:
            return -1

    ## __________________________________________________
    def control(self, ientry):
        gSystem.ProcessEvents()
        ans = raw_input('Entry # %5d    Input entry# or [n]/p/q > ' % ientry)
        if ans in ['q', 'Q']  : sys.exit(-1)
        elif ans in ['p', 'P']: return ientry-1
        elif ans in ['n', 'N', '']: return ientry+1
        else: return int(ans)

    ## __________________________________________________
    def list(self, lsdir):
        func = '%s:%s' % (self.file, 'list')
        sys.stderr.write('[%s]\tList files in "%s"\n' % (func, lsdir))
        com = 'ls %s' % lsdir
        os.system(com)

    ## __________________________________________________
    def lists(self):
        lsdirs = self.ifns
        for lsdir in lsdirs:
            self.list(lsdir)
        
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [options]' % sys.argv[0]
    parser = OptionParser(usage)
    parser.add_option('-l',
                      action = 'store_true',
                      dest   = 'lsdir',
                      help   = 'list files and directories')
    parser.add_option('-d',
                      action = 'store_true',
                      dest   = 'debug',
                      help   = 'set debug flag')
    parser.set_defaults(lsdir = False,
                        debug = False)
    (options, args) = parser.parse_args()

    ifns = []
    if len(args) > 0:
        ifns = args
    else:
        ifns = ['.']

    r = Read(ifns, debug=options.debug)
    if options.lsdir:
        r.lists()
    else:
        r.reads()
