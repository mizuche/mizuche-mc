#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TVectorD, TGraph, TCanvas, TH1D, TGraphErrors, TBox
from ROOT import TF1, TLegend, TH2D

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from wave import Wave

## __________________________________________________
class Tektronix(Wave):
    ## __________________________________________________
    #kdef __init__(self, date, runs, options):
    def __init__(self):
        pass
        # self.dir   = '../exp/qtcalib/'
        # self.date  = date
        # self.runs  = runs
        # self.ifns  = []
        # ch = options.ch
        # for irun in runs:
        #     ifn = '%d/tek%04d%s.csv' % (int(date), int(irun), ch)
        #     ifn = os.path.join(self.dir, ifn)
        #     self.ifns.append(ifn)
        # start = options.start
        # stop  = options.stop
        # self.setIntegrationRange(start, stop)
        # self.debug = False
        # self.graph = False
        # self.histo = False
        # self.nch   = None
        
    ## __________________________________________________
    def setIntegrationRange(self, start, stop):
        if start > stop:
            print 'Error:\tstart# (%d) > end# (%d)' % (start, stop)
            sys.exit(-1)
        self.start = start
        self.stop   = stop
        
    ## __________________________________________________
    def read(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\tFile "%s" does not exist.\n' % ifn)
            sys.exit(-1)

        print '[read]\tRead "%s"' % ifn
        with open(ifn, 'r') as fin:
            lines = fin.readlines()

        self.dsamp   = float(lines[6].rstrip().split(',')[1])
        vscales = lines[12].rstrip().split(',')[1:]
        labels  = lines[13].rstrip().split(',')[1:]
        nch     = len(labels)
        self.nch = nch

        data = []
        datum = {}
        datum['time'] = 'time'
        for ich in range(0, nch):
            ilabel = 'ch%d' % (ich+1)
            datum[ilabel] = labels[ich]
        data.append(datum)
        

        datum = {}
        datum['time'] = float(min(vscales))
        for ich in range(0, nch):
            ilabel = 'ch%d' % (ich+1)
            datum[ilabel] = float(vscales[ich])
        data.append(datum)
        
        for line in lines[15:-1]:
            datum = {}
            line = line.rstrip().split(',')
            datum['time'] = float(line[0])
            for ich in range(0, nch):
                ilabel = 'ch%d' % (ich+1)
                datum[ilabel] = float(line[ich+1])
            data.append(datum)
        self.data = data
        return data
    
    ## __________________________________________________
    def waveform(self, data, ich, draw = False):
        sys.stderr.write('[waveform]:\tDraw waveform for CH%d\n' % ich)

        nch = self.nch
        if ich > nch:
            sys.stderr.write('Error:\tSpecified CH#(%d) > NCH#(%d).\n' % (ich, nch))
            sys.exit(-1)

        g = TGraph(0)
        labels  = data[0]
        vscales = data[1]

        start = self.start
        stop  = self.stop
        dsamp = self.dsamp * 1e9
        
        ikey   = 'ch%d' % (ich)
        ilabel = labels[ikey]
        iscale = vscales['time'] / vscales[ikey]

        ### calculate pedestal
        ped, nped = 0, 0
        for datum in data[2:50]:
            t = float(datum['time']) * 1e9  ## [s] --> [ns]
            v = float(datum[ikey])   * 1e3  ## [V] --> [mV]
            ped  += v
            nped += 1
        #print 'Pedestal: %.3f [mV] (%.3f / %.3f)' % (ped/nped, ped, nped)
        ped = ped / nped

        ### plot waveform and caluculate charges
        charge = 0
        for datum in data[2:]:
            t = float(datum['time']) * 1e9
            v = (float(datum[ikey]) * 1e3) - ped
            v = v * iscale
            g.SetPoint(g.GetN(), t, v)
            if start <= t <= stop:
                charge += v
        #print 'Charge  : %.3f [pC] (%.3f [mV] * %.3f [ns/point] / 50 [ohm]' % (-charge * dsamp / 50, -charge, dsamp)
        charge = -charge * dsamp / 50
        
        title = '%s [#times %.2f] Q:%.2f [pC] (ped:%.2f [mV])' % (ilabel, iscale, charge, ped)
        g.SetTitle(title)
        g.GetXaxis().SetTitle(labels['time'] + ' [nsec]')
        g.GetYaxis().SetTitle('Amplitude [mV]')

        if draw:
            canvas = TCanvas('cWaveform', 'cWaveform CH%d' % ich, 1000, 500)
            g.Draw('apl')
            legend = TLegend(0.7, 0.1, 0.9, 0.3)
            legend.SetFillColor(0)
            legend.AddEntry(g, '%s' % title, 'lp')
            legend.Draw()
            self.stop()

        return g, charge, ped

    ## __________________________________________________
    def waveforms(self):
        data = self.data
        nch  = self.nch

        l = TLegend(0.5, 0.1, 0.9, 0.3)
        l.SetFillColor(0)

        waves   = []
        charges = []
        peds    = []
        ymaxs   = []
        ymins   = []
        for ich in range(nch):
            g, charge, ped = self.waveform(data, ich+1, False)
            g.SetLineColor(ich+1)
            g.SetMarkerColor(ich+1)
            l.AddEntry(g, '%s' % g.GetTitle(), 'lp')
            ymax = g.GetYaxis().GetXmax()
            ymin = g.GetYaxis().GetXmin()
            waves.append(g)
            charges.append(charge)
            peds.append(ped)
            ymins.append(ymin)
            ymaxs.append(ymax)

        ymax = max(ymaxs)
        ymin = min(ymins)
        
        c = TCanvas('cWaveform', 'cWaveform', 1000, 500)
        waves[0].GetYaxis().SetRangeUser(ymin, ymax)
        waves[0].Draw('apl')
        for g in waves[1:]:
            g.Draw('plsame')
        l.Draw()
        return waves, charges, peds, c, l

    ## __________________________________________________
    def loop(self, datadir, runs):
        irun, nrun = 0, len(runs)
        while(irun < nrun):
            run = runs[irun]
            ifn = 'tek%04dALL.csv' % run
            ifn = os.path.join(datadir, ifn)
            if not os.path.exists(ifn):
                sys.stderr.write('Error:\tFile "%s" does not exist.\n' % ifn)
                ifn = 'tek%04dCH4.csv' % run
                ifn = os.path.join(datadir, ifn)
            self.read(ifn)
            waves, charges, peds, c, l = self.waveforms()
            irun = self.control(irun)
            if irun < 0: irun = 0
            if irun > nrun: break
        return 
    
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [hliw] dir run#' % sys.argv[0]
    
    defstart = -10
    defstop  = 50
    
    parser = OptionParser(usage)
    parser.add_option('-l', '--ls',
                      dest   = 'type',
                      help   = 'list files')
    parser.add_option('-i', '--individual',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('-w', '--waveform',
                      dest   = 'ifn',
                      help   = 'set input filename')
    # parser.add_option('--ch',
    #                   dest   = 'ch',
    #                   help   = 'set filename suffix [default = "%s"]' % defch)
    parser.add_option('--start',
                      dest = 'start',
                      help = 'set start point for integration [default = %d]' % defstart)
    parser.add_option('--stop',
                      dest = 'end',
                      help = 'set stop point for integration [default = %d]' % defstop)
    parser.set_defaults(type       = False,
                        individual = False,
                        ifn        = None,
                        start      = defstart,
                        stop       = defstop)

    (options, args) = parser.parse_args()
    
    #print options
    #print len(args), args

    if options.type:
        name = options.type
        if name == 'beam':
            com = 'ls ../exp/upk'
        else:
            com = 'ls ../exp/%s' % name
        os.system(com)
        sys.exit(-1)

    if options.ifn:
        t = Tektronix()
        ifn = options.ifn
        t.read(ifn)
        t.waveforms()

    if len(args) < 2:
        sys.stderr.write('Error:\tWrong number of arguments.\n')
        print usage
        sys.exit(-1)

    datadir = args[0]
    if options.individual:
        runs    = [int(run) for run in args[1:]]
    else:
        srun = int(args[1])
        erun = srun
        if len(args) > 2:
            erun = int(args[2])
        runs = [run for run in range(srun, erun+1)]
    print runs

    t = Tektronix()
    t.setIntegrationRange(options.start, options.stop)
    t.loop(datadir, runs)
