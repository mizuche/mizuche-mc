#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import datetime
from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle, gClient, gROOT
gStyle.SetTimeOffset(-788918400)
from ROOT import TFile, TChain, TCut
from ROOT import TH1D, TGraphErrors, TH2D
from ROOT import TPad, TCanvas

mizulib = '../../mizulib/'
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from read2 import Config, Read

## __________________________________________________
if __name__ == '__main__':

    usage = '%prog [options] dst_files'
    parser = OptionParser(usage)
    parser.set_description('Script for checking DST trees')
    parser.add_option('-b', '--batch',
                      dest='batch',
                      action='store_true',
                      help='run as batch mode')
    parser.add_option('-e', '--each',
                      dest='each',
                      action='store_true',
                      help='read filenames separetely')
    parser.add_option('-o', '--out',
                      dest='ofn',
                      help='set output filename')
    parser.set_defaults(batch=False,
                        each=False,
                        ofn='tmpdst.root')
    (options, args) = parser.parse_args()

    gROOT.SetBatch(options.batch)

    if len(args) < 1:
        parser.error('Wrong number of arguments. (%d)\n' % len(args))

    if len(args) == 1:
        path, ifn = os.path.split(args[0])
        ofn = 'r' + ifn.capitalize()
        options.ofn = ofn

    if options.each:
        for arg in args:
            dst = Read('dst', 'dst')
            dst.add(arg)
            dst.process()
            dst.draw()
            #dst.stop()
            path, ifn = os.path.split(arg)
            ofn = 'r' + ifn.capitalize()
            dst.save(ofn)
    else:
        dst = Read('dst', 'dst')
        for arg in args:
            dst.add(arg)
        dst.process()
        dst.draw()
        dst.save(options.ofn)
