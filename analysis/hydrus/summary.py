#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import math

import logging
# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='debug.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(module)s.%(funcName)-20s %(message)s')
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import TFile, TTree, TChain
from ROOT import TH1D
from ROOT import TGraph, TGraphErrors, TMultiGraph
from ROOT import TCanvas

from read2 import Config, Read

## __________________________________________________
class Summary(Read):
    ## __________________________________________________
    def __init__(self):
        '''
        Initialization
        '''
        self.ch = TChain('sum')
        self.logger = logging.getLogger('Summary')
        self.logger.info('initialize')

    ## __________________________________________________
    def read(self, ifn):
        '''
        read tmporal summary file.
        '''
        self.logger.info('read "%s"', ifn)
        with open(ifn, 'r') as fin:
            lines = fin.readlines()

        data = []
        for line in lines:
            line = line.rstrip()
            #self.logger.debug('%s', line)
            read = line.startswith('[Read]')
            legend = line.startswith(' TLegendEntry')
            if read or legend:
                data.append(line)
        return data

    ## __________________________________________________
    def replace(self, data):
        self.logger.info('replacing data')
        new_data = []
        for d in data:
            self.logger.debug('pre  : %s', d)
            d = d.replace('[', '')
            d = d.replace(']', '')
            #d = d.replace('add', '')
            d = d.replace('"', '')
            #d = d.replace('NULL', '')
            d = d.replace('upk.hUpkRu', '')
            d = d.replace('dst.hUpkRun', '')
            d = d.replace('h', '')
            d = d.replace('TLegendEntry', '')
            d = d.replace('Object', '')
            d = d.replace('ALL', '')
            d = d.replace('Label', '')
            d = d.replace('Option', '')
            d = d.replace('lpf', '')

            d = d.replace('(', '')
            d = d.replace(')', '')
            d = d.replace('%', '')
            d = d.replace(':', '')
            d = d.split()
            d.pop(1)
            self.logger.debug('post : %s', d)
            new_data.append(d)
        return new_data

    ## __________________________________________________
    def sort(self, data):
        self.logger.info('sorting data')
        new_data = []
        for d in data:
            self.logger.debug('pre  : %s', d)
            dic = {}
            if 'Read' in d:
                d = d[-1]
                d = os.path.split(d)[-1]
                d = os.path.splitext(d)[0]
                d = d.replace('upk', '')
                d = d.replace('dst', '')
                run = d
            elif 'P.O.T' in d:
                pot = d[1]
            else:
                dic['run'] = run
                dic['pot'] = pot
                dic.update({'cut': d[0], 'nevents': d[1], 'ratio': d[2]})
                self.logger.debug('post : %s', dic)
                new_data.append(dic)
        return new_data

    ## __________________________________________________
    def write(self, data, ofn):
        self.logger.info('write data to "%s"', ofn)
        tmp = ofn.replace('.root', '.txt')
        with open(tmp, 'w') as fout:
            for d in data:
                line = '{0[run]}\t{0[pot]}\t{0[cut]}\t{0[nevents]}\t{0[ratio]}'.format(d)
                self.logger.debug('%s', line)
                fout.write(line + '\n')
        self.logger.info('recreate "%s"', ofn)
        fout = TFile(ofn, 'recreate')
        t = TTree('sum', 'run summary')
        br = 'run/I:pot/D:cuttype/I:nevents/I:ratio/D'
        t.ReadFile(tmp, br)
        t.Write()
        fout.Close()
        return

    ## __________________________________________________
    def process(self, ifn, ofn):
        self.logger.info('process "%s" to "%s"', ifn, ofn)
        ext = os.path.splitext(ifn)[-1]
        if ext in ['.txt']:
            #self.logger.info('Process text file : "{0}"'.format(ifn))
            data = self.read(ifn)
            data = self.replace(data)
            data = self.sort(data)
            self.write(data, ofn)
        elif ext in ['.root']:
            #self.logger.info('Process ROOT file : "{0}"'.format(ifn))
            self.add(ifn)
            self.draw()
        else:
            raise NotImplemented()

    ## __________________________________________________
    def add(self, ifn):
        '''
        Add TChain
        '''
        return self.ch.Add(ifn)

    ## __________________________________________________
    def draw(self):
        ch = self.ch
        nentries = ch.GetEntries()
        if nentries == 0:
            sys.stderr.write('No entries in this TChain. Quit.\n')
            sys.exit(-1)

            
        gNevents = [TGraphErrors(0) for i in range(Config.ncut)]
        for ientry in range(nentries):
            ch.GetEntry(ientry)
            run = ch.run % 10000
            nevents = ch.nevents
            cut = ch.cuttype
            nNevents = gNevents[cut].GetN()
            gNevents[cut].SetPoint(nNevents, run, nevents)
            gNevents[cut].SetPointError(nNevents, 0, math.sqrt(nevents))
            
        mgNevents = TMultiGraph('mgNevents', 'mgNevents')
        c = TCanvas('c', 'c')
        for i in range(len(gNevents)):
            #gNevents[i].GetYaxis().SetRangeUser(0, 3e4)
            gNevents[i].SetMarkerColor(i%8+1)
            gNevents[i].SetLineColor(i%8+1)
            gNevents[i].SetFillStyle(0)
            #gNevents[i].Draw('ap') if i == 1 else gNevents[i].Draw('psame')
            mgNevents.Add(gNevents[i], 'p')
        mgNevents.Draw('a')
        c.BuildLegend()
        self.stop()

## __________________________________________________
if __name__ == '__main__':

    usage = '%prog [summary_txtfiles or summary_rootfiles]'
    usage += '\n\n'
    usage += 'Run the script in procedure below:\n'
    usage += '    (Example for MR42 case)\n'
    usage += '  $ ./upk.py ../exp/beam/upk/upk042*.root > tmp.txt\n'
    usage += '  $ ./summary.py tmp.txt -o sumUpk042.root\n'
    usage += '  $ ./summary.py sumUpk042.root\n'

    parser = OptionParser(usage)
    parser.add_option('-o', '--ofn',
                      dest='ofn',
                      help='set output ROOT filename')
    parser.set_defaults(ofn='tmpsummary.root')

    (options, args) = parser.parse_args()

    if len(args) > 1:
        parser.error('Too many arguments : {0}'.format(len(args)))

    s = Summary()
    s.process(args[0], options.ofn)
    # for arg in args:
    #     s.process(arg, options.ofn)
