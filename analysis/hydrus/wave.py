#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TCanvas
from ROOT import TCanvas, TBox, TGaxis, TLegend
from ROOT import TH1D, TH2D, TGraph, TGraphErrors, TF1
from ROOT import TVectorD, Double

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from read import Read

## __________________________________________________
class Wave(Read):
    ## __________________________________________________
    def __init__(self, ifns, debug = False):
        Read.__init__(self, ifns, debug)
        self.file = __file__.split('/')[-1]

    ## __________________________________________________
    def waveform(self, fin):
        func  = '%s:%s' % (self.file, 'waveform')
        umode = fin.Get('umode')
        umode.GetEntry(0)
        mode  = umode.umode

        print '[%s]\tChecking unpacked mode : %d' % (func, mode)
        if not mode == 1:
            sys.stderr.write('Error:\t"%s" does not have waveform data.\n' % ifn)
            sys.stderr.write('\tQuit. Bye Bye!!!\n')
            sys.exit(-1)
        
        upk    = fin.Get('upk')
        unpack = T2KWCUnpack()
        br     = upk.GetBranch('upk.')
        br.SetAddress(unpack)
        upk.SetBranchAddress('upk.', unpack)

        c = TCanvas('cWaveform', 'check waveforms', 800, 800)
        c.Divide(1, 2)

        nentries = upk.GetEntries()
        ientry = 0
        while(ientry < nentries):
            ## PMTSUM
            upk.Project('', 'fadc_pmtsum:Iteration$', 'Entry$ == %d' % ientry)
            xp = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yp = TVectorD(upk.GetSelectedRows(), upk.GetV1())
            
            gp = TGraph(xp, yp)
            gp.SetTitle('PMTSUM (%4d/%4d)' % (ientry, nentries))
            gp.GetXaxis().SetTitle('Point [4ns/pt]')
            gp.GetYaxis().SetTitle('PMTSUM [mV]')

            ## HITSUM
            upk.Project('', 'fadc_hitsum:Iteration$', 'Entry$ == %d' % ientry)
            xh = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yh = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gh = TGraph(xh, yh)
            gh.SetTitle('HITSUM (%4d/%4d)' % (ientry, nentries))
            gh.GetXaxis().SetTitle('Point [4ns/pt]')
            gh.GetYaxis().SetTitle('HITSUM [mV]')

            ## Bunch timing and HITSUM height
            upk.Project('', '-fadc_height:fadc_nsec/4', 'Entry$ == %d' % ientry)
            xb = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yb = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gb = TGraph(xb, yb)
            gb.SetTitle('Hit Timing (%4d/%4d)' % (ientry, nentries))
            gb.GetXaxis().SetTitle('Timing [nsec]')
            gb.GetYaxis().SetTitle('Height [mV]')
            gb.SetLineColor(2)
            gb.SetMarkerColor(2)
            gb.SetMarkerStyle(34)
            gb.SetMarkerSize(4)

            ### To draw integral region for PMTSUM
            nb = gb.GetN()
            int_p = []
            win_p = []
            ymin = gp.GetYaxis().GetXmin()
            ymax = gp.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = int(x) - 18
                xmax = int(x) + 9
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3004)
                win_p.append(box)
                integ = 0.
                # for i in range(xmin, xmax+1):
                #     integ += upk.upk.GetFadcPmtsum(i)
                # int_p.append(integ)

            int_p2 = []
            win_p2 = []
            ymin = gp.GetYaxis().GetXmin()
            ymax = gp.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = int(x) - 18
                xmax = int(x) + 57
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(3)
                box.SetLineColor(3)
                box.SetLineWidth(3)
                box.SetLineStyle(2)
                box.SetFillStyle(3005)
                win_p2.append(box)
                integ = 0.
                # for i in range(xmin, xmax+1):
                #     integ += upk.upk.GetFadcPmtsum(i)
                # int_p2.append(integ)

            for integ in int_p:
                print 'int_p  : %.2f' % (- integ * 4 / 50 / 0.088)
            for integ in int_p2:
                print 'int_p2 : %.2f' % (- integ * 4 / 50 / 0.088)

            ### To draw integral region for HITSUM
            win_h = []
            ymin = gh.GetYaxis().GetXmin()
            ymax = gh.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = x + 6
                xmax = x + 45
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3004)
                win_h.append(box)                

            ### Print hit summary
            print '\tEntry# %4d' % ientry
            upk.GetEntry(ientry)
            upk.upk.Print()            

            run           = upk.upk.GetRunNum()
            spill         = upk.upk.GetSpillNum()
            fadc_ntrg     = upk.upk.GetFadcNtrg()
            fadc_nhits    = upk.upk.GetFadcNhits()
            fadc_ncharges = upk.upk.GetFadcNcharges()
            fadc_npes     = upk.upk.GetFadcNpes()
            atm_ntrg      = upk.upk.GetAtmNtrg()
            atm_nhits     = upk.upk.GetAtmNhits()
            atm_ncharges  = upk.upk.GetAtmNcharges()
            atm_npes      = upk.upk.GetAtmNpes()

            title = 'Run#%07d Spill#%6d' % (run, spill)
            lp    = TLegend(0.6, 0.1, 0.9, 0.3, title)
            lp.SetFillColor(0)
            label = 'FADC:%8.2f [pC] / %8.2f [p.e.]' % (fadc_ncharges, fadc_npes)
            lp.AddEntry(gp, label, 'lp')
            label = 'ATM :%8.2f [pC] / %8.2f [p.e.]' % (atm_ncharges, atm_npes)
            lp.AddEntry(gp, label, 'lp')
            
            lh    = TLegend(0.6, 0.1, 0.9, 0.3, title)
            lh.SetFillColor(0)
            label = 'FADC:%8.2f [hits] / %8.2f [trg]' % (fadc_nhits, fadc_ntrg)
            lh.AddEntry(gp, label, 'lp')
            label = 'ATM :%8.2f [hits] / %8.2f [trg]' % (atm_nhits, atm_ntrg)
            lh.AddEntry(gp, label, 'lp')
            
            c.cd(1)
            gp.Draw('apl')
            for box in win_p:
                box.Draw()
            for box in win_p2:
                box.Draw()
            lp.Draw()
            c.cd(2)
            gh.Draw('apl')
            if gb.GetN() > 0:
                gb.Draw('psame')
            for box in win_h:
                box.Draw()
            lh.Draw()
            c.Update()

            ientry = self.control(ientry)
            if ientry < 0 or ientry > nentries - 1:
                sys.stderr.write('Error:\tEntry #%d out of range. ' % ientry)
                c.Clear()
                c.Divide(1, 2)
                c.Update()
                self.stop()
                ientry = 0
        return fin

    ## __________________________________________________
    def waveforms(self):
        fins = self.reads()
        for fin in fins:
            self.waveform(fin)
        return
    
## __________________________________________________
if __name__ == '__main__':

    usage = '%s ifn' % sys.argv[0]

    parser = OptionParser(usage)
    parser.add_option('-l',
                      action = 'store_true',
                      dest   = 'lsdir',
                      help   = 'list files and directories')
    parser.add_option('-d',
                      action = 'store_true',
                      dest   = 'debug',
                      help   = 'set debug flag')
    parser.set_defaults(lsdir = False,
                        debug = False)

    (options, args) = parser.parse_args()

    ifns = []
    if len(args) > 0:
        ifns = args
    else:
        ifns = ['.']

    w = Wave(ifns, debug=options.debug)
    
    if options.lsdir:
        w.lists()
    else:
        w.waveforms()
