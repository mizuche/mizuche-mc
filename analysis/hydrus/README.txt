-*- mode : org -*-

* スクリプトの覚書

** upk, dst ファイル解析のメインスクリプト

   |----------+---------------------------------------|
   | read2.py | 根幹のスクリプト；                    |
   | upk.py   | $ upk.py ../exp/beam/upk/upk042*.root |
   | dst.py   | $ dst.py ../exp/beam/dst/dst042*.root |
   |----------+---------------------------------------|

** 解析のサマリを編集するスクリプト
   |------------+-------------------------------------------------|
   | summary.py | $ upk.py ../exp/beam/upk/upk042*.root > tmp.txt |
   |            | $ summary.py tmp.txt -o sumUpk042.root          |
   |            | $ summary.py sumUpk042.root                     |
   |------------+-------------------------------------------------|

** その他のスクリプトの思い出し書き
   + calibration.py
     - LED較正のデータを処理するスクリプト
     - '-h'でヘルプを確認する
     - どんな内容か忘れてしまったので、もう一度中身を読み返す必要あり
     - このCalibrationクラスがあるので、comparecalibを使うことができる
       
   + compare.py
     - 読み込むファイルは中にベタ書きしているので、そこを変更する
       
   + comparecalib.py
     - LED較正のデータを比較するスクリプト
     - 読み込むファイルはベタ書き
     - 一瞬で描画される
       
   + dst.py
     - DSTファイルをチェックするスクリプトらしい
      
   + fvstat.py
     - RUN42, 43, 46, 47を水あり、水なし、水半分に分割して、ヒットレートのヒストグラムを生成する
     - 引数要らない
     - Read.hHitrateTimeというヒストグラムがないせいで怒られるので修正が必要

   + mc.py
     - BeamMCのデータを読み込み、ヒット分布を作成する
     - 使いづらいので、改善が必要
     - バーテックス分布なども付け加えたいo
       
       

* 解析クラスの構造についてつらつらと・・・
  + データディレクトリの構造
    + exp
      + DataLogger
        + yymmdd
        + rootfile
        + plots
      + HVLog
        + detmap
        + plots
        + rlog
        + rootfile
        + slog
        + wlog
      + Ondotori
        + 
      + beam
        + MR* : 生データ; 1000 events / file
        + dst : BSDとUPKを結合したファイル; ~ 1 upkfile / dstfile
        + plots
          + upk%07d : 各UPKファイルから作成したプロットを保存
        + upk : アンパックしたファイル; 100 rawfiles / upkfile
        + wave : FADCの波形データを残したアンパックファイル; 1 rawfile / wavefile
      + bsd
        + v01
          + t2krun3
          + t2krun4
      + cosmic
        + yymmdd
        + plots
          + yymmdd
        + upk
      + led
        + yymmdd
        + calib
        + plots
        + upk
      + noise
        + yymmdd
        + macro
        + plots
        + upk
      + ped
        + plots
      + qtcalib
        + plots
        + tektronix
        + upk
  + mc
    + beambk
      + withFV
      + woFV

** データ読み込みに関して
   * 実験データに関しては、UPKファイルを読み込めるようにしておく。
     ビームデータに関しては、DST読み込めるようにしておく。
     TreeNameはそれぞれ、upk, dst
   * FADCの波形を確認、解析したい場合は、WAVEファイルを読み込む。
     TreeNameは'fadc'
   * MCのTreeNameは'mc'

** 作成するヒストグラム
   * ヒット分布
   * データとMCで、作りたいヒストグラムが異なるかもしれないので、
     init_hist_mc, init_hist_data, などで別々に初期化できるようにしておく？
     共通に使えるものは, init_hist_commonでまとめとく？
     めんどくさい場合は、init_histにまとめてしまう。 (メモリリークとの兼ね合い？)
   * ヒストグラムのオブジェクト名は自動生成できるといいな
     * h%04dでインクリメントしていく
     * 時刻からランダムで生成する
       
** ヒストグラムの描画
   * 上記の様に、存在しないヒストグラムを描画 (またはFill) しようとするとエラーがでるので、
     ちゃんと回避 (or エラーメッセージが出るように) したい

** プロットの保存
   * ある一定のルールでキャンバスを保存させる。
   * 試行錯誤時は、overwriteで良いが、久しぶりに使う場合
     (=動作内容を忘れている場合)はoverwriteさせたくない。
     オプションで管理する (デフォルトは un-overwritable)

** TTree (or TChain) のエントリのループとヒストグラムのFill
   * 1回のループ内で、複数のヒストグラムをFillする。
     ヒストグラムを作成するたびに、ループを回すのはアホ。
   * 作成したいヒストグラムは選択できるとありがたい。
     ひとつだけ確認したいときに、全部のヒストグラムを作成するのはもったいない気がする。
     かといって、process_all, process_hitなどというようにプロセスした
     ヒストグラムごとにメソッドを用意するのもなんか違う気がする。

** 以下のクラス構造にする
   1. Init
      + init_hist
        - init_hist_common        
        - init_hist_data
        - init_hist_mc
   2. Read
      - read_data
      - read_mc
   3. Process
   4. Draw
   5. Save
      
      
* 改善項目
  1. optparse -> argparseに置き換える
