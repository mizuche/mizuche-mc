#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TCanvas
from ROOT import TCanvas, TBox, TGaxis, TLegend
from ROOT import TH1D, TH2D, TGraph, TGraphErrors, TF1
from ROOT import TVectorD, Double

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from hist import Hist

## __________________________________________________
class Graph(Hist):
    ## __________________________________________________
    def __init__(self, ifns, cut = ""):
        Hist.__init__(self, ifns, cut)

    ## __________________________________________________
    def graphNcharges(self):
        ifns = self.ifns
        gLedvAtm   = TGraphErrors(0)
        gLedvFadc  = TGraphErrors(0)
        gLedvDiff  = TGraphErrors(0)
        gWidthAtm  = TGraphErrors(0)
        gWidthFadc = TGraphErrors(0)
        gWidthDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNcharges(ifn)
            upk.GetEntry(0)
            run    = upk.upk.GetRunNum()
            ledv   = (upk.upk.GetDaqmode() - 10000) / 1000.
            width  = upk.upk.GetFvstat()
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gLedvAtm.GetN()
            gLedvAtm.SetPoint(n, ledv, m_atm)
            gLedvAtm.SetPointError(n, 0, r_atm)
            n = gLedvFadc.GetN()
            gLedvFadc.SetPoint(n, ledv, m_fadc)
            gLedvFadc.SetPointError(n, 0, r_fadc)
            n = gLedvDiff.GetN()
            gLedvDiff.SetPoint(n, ledv, m_diff)
            gLedvDiff.SetPointError(n, 0, r_diff)
            n = gWidthAtm.GetN()
            gWidthAtm.SetPoint(n, width, m_atm)
            gWidthAtm.SetPointError(n, 0, r_atm)
            n = gWidthFadc.GetN()
            gWidthFadc.SetPoint(n, width, m_fadc)
            gWidthFadc.SetPointError(n, 0, r_fadc)
            n = gWidthDiff.GetN()
            gWidthDiff.SetPoint(n, width, m_diff)
            gWidthDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gLedvAtm.SetTitle('LEDV vs Ncharges;LEDV [V];Ncharges [pC]')
        gLedvAtm.SetMarkerColor(2)
        gLedvAtm.SetLineColor(2)
        gLedvAtm.Draw('ap')
        gLedvFadc.SetMarkerColor(4)
        gLedvFadc.SetLineColor(4)
        gLedvFadc.Draw('psame')
        lLedv = TLegend(0.1, 0.7, 0.5, 0.9, 'LEDV vs Ncharges')
        lLedv.SetFillColor(0)
        lLedv.AddEntry(gLedvAtm, 'ATM', 'lp')
        lLedv.AddEntry(gLedvFadc, 'FADC', 'lp')
        lLedv.Draw()
        c.cd(2)
        gLedvDiff.SetTitle('LEDV vs DiffNcharges;LEDV [V];DiffNcharges (ATM - FADC/0.088) [pC]')
        gLedvDiff.SetMarkerColor(3)
        gLedvDiff.SetLineColor(3)
        gLedvDiff.Draw('ap')
        c.cd(3)
        gWidthAtm.SetTitle('Pulse width vs Ncharges;Pulse Width [ns];Ncharges [pC]')
        gWidthAtm.SetMarkerColor(2)
        gWidthAtm.SetLineColor(2)
        gWidthAtm.Draw('ap')
        gWidthFadc.SetMarkerColor(4)
        gWidthFadc.SetLineColor(4)
        gWidthFadc.Draw('psame')
        lWidth = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse width vs Ncharges')
        lWidth.SetFillColor(0)
        lWidth.AddEntry(gWidthAtm, 'ATM', 'lp')
        lWidth.AddEntry(gWidthFadc, 'FADC', 'lp')
        lWidth.Draw()
        c.cd(4)
        gWidthDiff.SetTitle('Pulse width vs DiffNcharges;Pulse Width [ns];DiffNcharges (ATM - FADC/0.088) [pC]')
        gWidthDiff.SetMarkerColor(3)
        gWidthDiff.SetLineColor(3)
        gWidthDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graphNpes(self):
        ifns = self.ifns
        gLedvAtm   = TGraphErrors(0)
        gLedvFadc  = TGraphErrors(0)
        gLedvDiff  = TGraphErrors(0)
        gWidthAtm  = TGraphErrors(0)
        gWidthFadc = TGraphErrors(0)
        gWidthDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNpes(ifn, False)
            upk.GetEntry(0)
            
            run    = upk.upk.GetRunNum()
            ledv   = (upk.upk.GetDaqmode() - 10000) / 1000.
            width  = upk.upk.GetFvstat()
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gLedvAtm.GetN()
            gLedvAtm.SetPoint(n, ledv, m_atm)
            gLedvAtm.SetPointError(n, 0, r_atm)
            n = gLedvFadc.GetN()
            gLedvFadc.SetPoint(n, ledv, m_fadc)
            gLedvFadc.SetPointError(n, 0, r_fadc)
            n = gLedvDiff.GetN()
            gLedvDiff.SetPoint(n, ledv, m_diff)
            gLedvDiff.SetPointError(n, 0, r_diff)
            n = gWidthAtm.GetN()
            gWidthAtm.SetPoint(n, width, m_atm)
            gWidthAtm.SetPointError(n, 0, r_atm)
            n = gWidthFadc.GetN()
            gWidthFadc.SetPoint(n, width, m_fadc)
            gWidthFadc.SetPointError(n, 0, r_fadc)
            n = gWidthDiff.GetN()
            gWidthDiff.SetPoint(n, width, m_diff)
            gWidthDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gLedvAtm.SetTitle('LEDV vs Npes;LEDV [V];Npes [p.e.]')
        gLedvAtm.SetMarkerColor(2)
        gLedvAtm.SetLineColor(2)
        gLedvAtm.Draw('ap')
        gLedvFadc.SetMarkerColor(4)
        gLedvFadc.SetLineColor(4)
        gLedvFadc.Draw('psame')
        lLedv = TLegend(0.1, 0.7, 0.5, 0.9, 'LEDV vs Npes')
        lLedv.SetFillColor(0)
        lLedv.AddEntry(gLedvAtm, 'ATM', 'lp')
        lLedv.AddEntry(gLedvFadc, 'FADC', 'lp')
        lLedv.Draw()
        c.cd(2)
        gLedvDiff.SetTitle('LEDV vs DiffNpes;LEDV [V];DiffNpes (ATM - FADC/0.088) [p.e.]')
        gLedvDiff.SetMarkerColor(3)
        gLedvDiff.SetLineColor(3)
        gLedvDiff.Draw('ap')
        c.cd(3)
        gWidthAtm.SetTitle('Pulse width vs Npes;Pulse Width [ns];Npes [p.e.]')
        gWidthAtm.SetMarkerColor(2)
        gWidthAtm.SetLineColor(2)
        gWidthAtm.Draw('ap')
        gWidthFadc.SetMarkerColor(4)
        gWidthFadc.SetLineColor(4)
        gWidthFadc.Draw('psame')
        lWidth = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse width vs Npes')
        lWidth.SetFillColor(0)
        lWidth.AddEntry(gWidthAtm, 'ATM', 'lp')
        lWidth.AddEntry(gWidthFadc, 'FADC', 'lp')
        lWidth.Draw()
        c.cd(4)
        gWidthDiff.SetTitle('Pulse width vs DiffNpes;Pulse Width [ns];DiffNpes (ATM - FADC/0.088) [p.e.]')
        gWidthDiff.SetMarkerColor(3)
        gWidthDiff.SetLineColor(3)
        gWidthDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graphNtrg(self):
        ifns = self.ifns
        gLedvAtm   = TGraphErrors(0)
        gLedvFadc  = TGraphErrors(0)
        gLedvDiff  = TGraphErrors(0)
        gWidthAtm  = TGraphErrors(0)
        gWidthFadc = TGraphErrors(0)
        gWidthDiff = TGraphErrors(0)

        for ifn in ifns:
            fin, upk, hAtm, hFadc, hDiff = self.histNtrg(ifn)
            upk.GetEntry(0)
            run    = upk.upk.GetRunNum()
            ledv   = (upk.upk.GetDaqmode() - 10000) / 1000.
            width  = upk.upk.GetFvstat()
            m_atm  = hAtm.GetMean()
            r_atm  = hAtm.GetRMS()
            m_fadc = hFadc.GetMean()
            r_fadc = hFadc.GetRMS()
            m_diff = hDiff.GetMean()
            r_diff = hDiff.GetRMS()
            ###
            n = gLedvAtm.GetN()
            gLedvAtm.SetPoint(n, ledv, m_atm)
            gLedvAtm.SetPointError(n, 0, r_atm)
            n = gLedvFadc.GetN()
            gLedvFadc.SetPoint(n, ledv, m_fadc)
            gLedvFadc.SetPointError(n, 0, r_fadc)
            n = gLedvDiff.GetN()
            gLedvDiff.SetPoint(n, ledv, m_diff)
            gLedvDiff.SetPointError(n, 0, r_diff)
            n = gWidthAtm.GetN()
            gWidthAtm.SetPoint(n, width, m_atm)
            gWidthAtm.SetPointError(n, 0, r_atm)
            n = gWidthFadc.GetN()
            gWidthFadc.SetPoint(n, width, m_fadc)
            gWidthFadc.SetPointError(n, 0, r_fadc)
            n = gWidthDiff.GetN()
            gWidthDiff.SetPoint(n, width, m_diff)
            gWidthDiff.SetPointError(n, 0, r_diff)

        c = TCanvas('c', 'c', 800, 800)
        c.Divide(2, 2)
        c.cd(1)
        gLedvAtm.SetTitle('LEDV vs Ntrg;LEDV [V];Ntrg [#]')
        gLedvAtm.SetMarkerColor(2)
        gLedvAtm.SetLineColor(2)
        gLedvAtm.Draw('ap')
        gLedvFadc.SetMarkerColor(4)
        gLedvFadc.SetLineColor(4)
        gLedvFadc.Draw('psame')
        lLedv = TLegend(0.1, 0.7, 0.5, 0.9, 'LEDV vs Ntrg')
        lLedv.SetFillColor(0)
        lLedv.AddEntry(gLedvAtm, 'ATM', 'lp')
        lLedv.AddEntry(gLedvFadc, 'FADC', 'lp')
        lLedv.Draw()
        c.cd(2)
        gLedvDiff.SetTitle('LEDV vs DiffNtrg;LEDV [V];DiffNtrg [#]')
        gLedvDiff.SetMarkerColor(3)
        gLedvDiff.SetLineColor(3)
        gLedvDiff.Draw('ap')
        c.cd(3)
        gWidthAtm.SetTitle('Pulse width vs Ntrg;Pulse Width [ns];Ntrg [#]')
        gWidthAtm.SetMarkerColor(2)
        gWidthAtm.SetLineColor(2)
        gWidthAtm.Draw('ap')
        gWidthFadc.SetMarkerColor(4)
        gWidthFadc.SetLineColor(4)
        gWidthFadc.Draw('psame')
        lWidth = TLegend(0.1, 0.7, 0.5, 0.9, 'Pulse width vs Ntrg')
        lWidth.SetFillColor(0)
        lWidth.AddEntry(gWidthAtm, 'ATM', 'lp')
        lWidth.AddEntry(gWidthFadc, 'FADC', 'lp')
        lWidth.Draw()
        c.cd(4)
        gWidthDiff.SetTitle('Pulse width vs DiffNtrg;Pulse Width [ns];DiffNtrg [#]')
        gWidthDiff.SetMarkerColor(3)
        gWidthDiff.SetLineColor(3)
        gWidthDiff.Draw('ap')
        
        self.stop()
        return

    ## __________________________________________________
    def graph(self, item):
        items = ['ncharges', 'npes', 'ntrg']
        if not item in items:
            sys.stderr.write('Error:\tGiven item (%s) is not in the list' % item)
            print items
            sys.exit()
        
        if item == items[0]:
            self.graphNcharges()
        if item == items[1]:
            self.graphNpes()
        if item == items[2]:
            self.graphNtrg()
        return

    ## __________________________________________________
    def trigger(self, ifn):
        fin = self.read(ifn)

        upk = fin.Get('upk')
        unpack = T2KWCUnpack()
        br = upk.GetBranch('upk.')
        br.SetAddress(unpack)
        upk.SetBranchAddress('upk.', unpack)

        n = upk.GetEntries()

        for i in range(0, n):
            upk.GetEntry(i)
            atm_ntrg = unpack.GetAtmNtrg()
            hit = T2KWCHit()

            if atm_ntrg > 0:
                for j in range(0, atm_ntrg):
                    hit = unpack.GetHit(j)
                    pmt = hit.GetPmt()
                    print '%d' % pmt
    
## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [options] dir run_start# run_end#' % sys.argv[0]

    defdir = '../exp/cosmic/upk/'
    defcut = 'atm_ntrg ==1 && fadc_ntrg==1'
    
    parser = OptionParser(usage)
    parser.add_option('-l', '--list',
                      action = 'store_true',
                      dest   = 'lsdir',
                      help   = 'ls "%s"' % defdir)
    parser.add_option('-d',
                      action = 'store_true',
                      dest   = 'debug',
                      help   = 'set debug flag')
    parser.add_option('-f',
                      action = 'store_true',
                      dest   = 'filename',
                      help   = 'set filename')
    parser.add_option('-i', '--individual',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('-q', '--ncharges',
                      action = 'store_true',
                      dest   = 'ncharges',
                      help   = 'draw ncharges distribution')
    parser.add_option('-p', '--npes',
                      action = 'store_true',
                      dest   = 'npes',
                      help   = 'draw npes distribution')
    parser.add_option('-w', '--waveform',
                      action = 'store_true',
                      dest = 'waveform',
                      help = 'draw waveforms')
    parser.add_option('-t', '--trg',
                      action = 'store_true',
                      dest = 'ntrg',
                      help = 'draw trg')
    parser.add_option('-c', '--cut',
                      dest = 'cut',
                      help = 'set cut [default = "%s"]' % defcut)
    parser.set_defaults(lsdir      = False,
                        debug      = False,
                        filename   = False,
                        individual = False,
                        cut        = defcut,
                        ncharges   = False,
                        npes       = False,
                        waveform   = False,
                        ntrg       = False,
                        graph      = False)
    
    (options, args) = parser.parse_args()

    ifns = []
    if options.filename:
        if len(args) > 0:
            ifns = args
    else:
        if len(args) > 2:
            datadir = '../exp/%s/upk' % args[0]
            date    = args[1]
            if options.individual:
                runs = args[2:]
            else:
                srun = int(args[2])
                erun = srun
                if len(args) > 3:
                    erun = int(args[3])
            runs = [run for run in range(srun, erun+1)]
            print 'Runs:\t', runs
        else:
            print usage

    g = Graph(ifns)
    if options.lsdir:
        g.lists()
    else:
        items = []
        if options.ncharges : items.append('ncharges')
        if options.npes     : items.append('npes')
        if options.ntrg     : items.append('ntrg')
        
        for item in items:
            g.graph(item)
            sys.exit(-1)
