#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import os, sys
from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from read2 import Config, Read

from ROOT import gSystem, gStyle, gClient, gROOT
from ROOT import TFile
from ROOT import TCanvas, TLegend

## __________________________________________________
class Compare(Read):
    ## __________________________________________________
    def __init__(self, *read):
        self.read = [r for r in read]

    ## __________________________________________________
    def save(self, ofn = 'tmp.root'):
        sys.stdout.write('-' * 50 + '\n')
        sys.stdout.write('[save]\tsave "{0}"\n'.format(ofn))
        fout = TFile(ofn, 'recreate')
        for r in self.read:
            tname = r.get_tree_name()
            if tname == 'upk':
                r.save_upk()
            elif tname == 'dst':
                r.save_dst()
        fout.Close()
        sys.stdout.write('[save]\tclose "{0}"\n'.format(ofn))
        sys.stdout.write('-' * 50 + '\n')
        return

    ## __________________________________________________
    def draw_nhits(self, icut):
        ltitle = 'Cut{0:02d}'.format(icut)
        lAtm  = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        lFadc = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        cname  = 'cCompDrawNhitsCut{0:02d}'.format(icut)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        wx, wy = Config.disp
        wy = wx/3
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 1)
        for i in range(len(self.read)):
            c.cd(1)    ## hNhitsAtm
            hAtm = self.read[i].hNhitsAtm[icut]
            hAtm.SetStats(0)
            hAtm.SetLineColor(i+2)
            hAtm.SetFillColor(i+2)
            hAtm.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hAtm.GetName())[0], int(hAtm.GetEntries()))
            lAtm.AddEntry(hAtm, label, 'lpf')
            if i == 0 : hAtm.Draw()
            else      : hAtm.Draw('same')
            c.cd(2)    ## hNhitsFadc
            hFadc = self.read[i].hNhitsFadc[icut]
            hFadc.SetStats(0)
            hFadc.SetLineColor(i+2)
            hFadc.SetFillColor(i+2)
            hFadc.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hFadc.GetName())[0], int(hFadc.GetEntries()))
            lFadc.AddEntry(hFadc, label, 'lpf')
            if i == 0 : hFadc.Draw()
            else      : hFadc.Draw('same')
            c.cd(3)    ## hNhitsAtmFadc
            hAtmFadc = self.read[i].hNhitsAtmFadc[icut]
            hAtmFadc.SetMarkerColor(i+1)
            if i == 0 : hAtmFadc.Draw()
            else      : hAtmFadc.Draw('same')
        c.cd(1)
        lAtm.SetFillColor(0)
        lAtm.Draw()
        c.cd(2)
        lFadc.SetFillColor(0)
        lFadc.Draw()
        c.Update()
        c.SaveAs(ctitle)
        self.read[0].stop()
        return c

    ## __________________________________________________
    def draw_ncharges(self, icut):
        ltitle = 'Cut{0:02d}'.format(icut)
        lAtm  = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        lFadc = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        cname  = 'cCompDrawNchargesCut{0:02d}'.format(icut)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        wx, wy = Config.disp
        wy = wx/3
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 1)
        for i in range(len(self.read)):
            c.cd(1)    ## hNchargesAtm
            hAtm = self.read[i].hNchargesAtm[icut]
            hAtm.SetStats(0)
            hAtm.SetLineColor(i+2)
            hAtm.SetFillColor(i+2)
            hAtm.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hAtm.GetName())[0], int(hAtm.GetEntries()))
            lAtm.AddEntry(hAtm, label, 'lpf')
            if i == 0 : hAtm.Draw()
            else      : hAtm.Draw('same')
            c.cd(2)    ## hNchargesFadc
            hFadc = self.read[i].hNchargesFadc[icut]
            hFadc.SetStats(0)
            hFadc.SetLineColor(i+2)
            hFadc.SetFillColor(i+2)
            hFadc.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hFadc.GetName())[0], int(hFadc.GetEntries()))
            lFadc.AddEntry(hFadc, label, 'lpf')
            if i == 0 : hFadc.Draw()
            else      : hFadc.Draw('same')
            c.cd(3)
            hAtmFadc = self.read[i].hNchargesAtmFadc[icut]
            hAtmFadc.SetMarkerColor(i+2)
            if i == 0 : hAtmFadc.Draw()
            else      : hAtmFadc.Draw('same')
        c.cd(1)
        lAtm.SetFillColor(0)
        lAtm.Draw()
        c.cd(2)
        lFadc.SetFillColor(0)
        lFadc.Draw()
        c.Update()
        c.SaveAs(ctitle)
        self.read[0].stop()
        return c

    ## __________________________________________________
    def draw_npes(self, icut):
        ltitle = 'Cut{0:02d}'.format(icut)
        lAtm  = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        lFadc = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        cname  = 'cCompDrawNpesCut{0:02d}'.format(icut)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        wx, wy = Config.disp
        wy = wx/3
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 1)
        for i in range(len(self.read)):
            c.cd(1)    ## hNpesAtm
            hAtm = self.read[i].hNpesAtm[icut]
            hAtm.SetLineColor(i+2)
            hAtm.SetFillColor(i+2)
            hAtm.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hAtm.GetName())[0], int(hAtm.GetEntries()))
            lAtm.AddEntry(hAtm, label, 'lpf')
            if i == 0 : hAtm.Draw()
            else      : hAtm.Draw('same')
            c.cd(2)    ## hNpesFadc
            hFadc = self.read[i].hNpesFadc[icut]
            hFadc.SetLineColor(i+2)
            hFadc.SetFillColor(i+2)
            hFadc.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hFadc.GetName())[0], int(hFadc.GetEntries()))
            lFadc.AddEntry(hFadc, label, 'lpf')
            if i == 0 : hFadc.Draw()
            else      : hFadc.Draw('same')
            c.cd(3)
            hAtmFadc = self.read[i].hNpesAtmFadc[icut]
            hAtmFadc.SetMarkerColor(i+2)
            if i == 0 : hAtmFadc.Draw()
            else      : hAtmFadc.Draw('same')
        c.cd(1)
        lAtm.SetFillColor(0)
        lAtm.Draw()
        c.cd(2)
        lFadc.SetFillColor(0)
        lFadc.Draw()
        c.Update()
        c.SaveAs(ctitle)        
        self.read[0].stop()
        return c

    ## __________________________________________________
    def draw_ntrg(self, icut):
        ltitle = 'Cut{0:02d}'.format(icut)
        lAtm  = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        lFadc = TLegend(0.5, 0.5, 0.9, 0.9, ltitle)
        cname  = 'cCompDrawNtrgCut{0:02d}'.format(icut)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        wx, wy = Config.disp
        wy = wx/3
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 1)
        for i in range(len(self.read)):
            c.cd(1)    ## hNtrgAtm
            hAtm = self.read[i].hNtrgAtm[icut]
            hAtm.SetStats(0)
            hAtm.SetLineColor(i+2)
            hAtm.SetFillColor(i+2)
            hAtm.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hAtm.GetName())[0], int(hAtm.GetEntries()))
            lAtm.AddEntry(hAtm, label, 'lpf')
            if i == 0 : hAtm.Draw()
            else      : hAtm.Draw('same')
            c.cd(2)    ## hNtrgFadc
            hFadc = self.read[i].hNtrgFadc[icut]
            hFadc.SetStats(0)
            hFadc.SetLineColor(i+2)
            hFadc.SetFillColor(i+2)
            hFadc.SetMarkerColor(i+2)
            label = '{0} : {1:10d}'.format(os.path.splitext(hFadc.GetName())[0], int(hFadc.GetEntries()))
            lFadc.AddEntry(hFadc, label, 'lpf')
            if i == 0 : hFadc.Draw()
            else      : hFadc.Draw('same')
            c.cd(3)
            hAtmFadc = self.read[i].hNtrgAtmFadc[icut]
            hAtmFadc.SetMarkerColor(i+2)
            if i == 0 : hAtmFadc.Draw('colz')
            else      : hAtmFadc.Draw('colzsame')
        c.cd(1)
        lAtm.SetFillColor(0)
        lAtm.Draw()
        c.cd(2)
        lFadc.SetFillColor(0)
        lFadc.Draw()
        c.Update()
        c.SaveAs(ctitle)        
        self.read[0].stop()
        return c

## __________________________________________________
if __name__ == '__main__':

    usage = '%s [options]' % sys.argv[0]
    usage += '\n'
    usage += '\n'
    usage += 'DESCRIPTION\n'
    usage += '  Read BeamData UPK files. Modify the UPK filename if needes.\n'
    usage += '  Draw hNhits histograms on same canvas, with several cut options.\n'
    
    parser = OptionParser(usage)
    parser.add_option('-b', '--batch',
                      action = 'store_true',
                      dest   = 'batch',
                      help   = 'run ROOT in batch mode')
    parser.add_option('-m', '--mode',
                      choices = ('upk', 'history', 'bsd', 'dst'),
                      dest   = 'mode',
                      help   = 'choose mode from ([upk], history, bsd, dst).')
    parser.set_defaults(batch = False,
                        mode   = 'dst')
    (options, args) = parser.parse_args()

    gROOT.SetBatch(options.batch)
    
    r1 = Read('upk0460001', 'upk')
    r1.add('../exp/beam/upk/upk0460001.root')
    r1.process()

    r3 = Read('upk0460003', 'upk')
    r3.add('../exp/beam/upk/upk0460003.root')
    r3.process()

    r4 = Read('upk0460004', 'upk')
    r4.add('../exp/beam/upk/upk0460004.root')
    r4.process()

    c = Compare(r1, r3, r4)
    for i in range(Config.ncut):
        sys.stdout.write('[Compare]\tcut{0:d} : {1}\n'.format(i, Config.cuts[i]))
        c.draw_nhits(i)
        #c.draw_ncharges(i)
        #c.draw_npes(i)
        #c.draw_ntrg(i)
    c.save()
