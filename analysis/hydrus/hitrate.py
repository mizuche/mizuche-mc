#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TCanvas
from ROOT import TCanvas, TBox, TGaxis, TLegend
from ROOT import TH1D, TH2D, TGraph, TGraphErrors, TF1
from ROOT import TVectorD, Double

mizulib = '../../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

from hist import Hist

## __________________________________________________
class HitRate(Hist):
    ## __________________________________________________
    def __init__(self, ifns, cut = ''):
        self.file = __file__.split('/')[-1]
        Hist.__init__(self, ifns, cut)
        fins = self.reads()

    ## __________________________________________________
    def hitRate(self, fin, run, hitcut = 'atm_nhits>0'):
        upk = fin.Get('upk')
        cut = 'runnum==%d' % run
        n = upk.GetEntries(cut)
        cut = cut + ' && ' + hitcut
        i0 = upk.GetEntries(cut)
        return n, i0

    ## __________________________________________________
    def hitRates(self, hitcut = 'atm_nhits>0'):
        fins = self.fins
        g = TGraph(0)
        for fin in fins:
            upk     = fin.Get('upk')
            entries = upk.GetEntries()
            upk.GetEntry(0)
            srun    = upk.upk.GetRunNum()
            upk.GetEntry(entries-1)
            erun    = upk.upk.GetRunNum()
            for irun in range(srun, erun+1):
                n, i = self.hitRate(fin, irun, hitcut)
                rate = float(i)/float(n)
                g.SetPoint(g.GetN(), irun%10000, rate)
        g.SetTitle('gHitRate;Run [#];Hit Rate [# /spill]')
        return g

## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [hliqptc] type date run_start# run_end#' % sys.argv[0]

    defdir = '../exp/cosmic/upk/'
    defcut = 'atm_ntrg ==1 && fadc_ntrg==1'
    
    parser = OptionParser(usage)
    parser.add_option('-l',
                      action = 'store_true',
                      dest   = 'lsdir',
                      help   = 'ls "%s"' % defdir)
    parser.add_option('-d',
                      action = 'store_true',
                      dest   = 'debug',
                      help   = 'set debug flag')
    parser.add_option('-f',
                      action = 'store_true',
                      dest   = 'filename',
                      help   = 'set filename')
    parser.add_option('-i',
                      action = 'store_true',
                      dest   = 'individual',
                      help   = 'set individual run# (-i dir run1 run2 run3 ...)')
    parser.add_option('-q',
                      action = 'store_true',
                      dest   = 'ncharges',
                      help   = 'draw ncharges distribution')
    parser.add_option('-p',
                      action = 'store_true',
                      dest   = 'npes',
                      help   = 'draw npes distribution')
    parser.add_option('-t',
                      action = 'store_true',
                      dest = 'ntrg',
                      help = 'draw trg')
    parser.add_option('-r',
                      dest   = 'ymax',
                      help   = 'set ymax')
    parser.add_option('-c',
                      dest = 'cut',
                      help = 'set cut [default = "%s"]' % defcut)
    parser.set_defaults(lsdir      = False,
                        debug      = False,
                        filename   = False,
                        individual = False,
                        cut        = defcut,
                        ncharges   = False,
                        npes       = False,
                        waveform   = False,
                        ntrg       = False,
                        graph      = False,
                        ymax       = 0.2)
    
    (options, args) = parser.parse_args()

    ifns = []
    if options.filename:
        if len(args) > 0:
            ifns = args
    else:
        if len(args) > 2:
            datadir  = '../exp/%s/upk' % args[0]
            date     = args[1]
            if options.individual:
                runs = args[2:]
            else:
                srun = int(args[2])
                erun = srun
                if len(args) > 3:
                    erun = int(args[3])
            runs = [run for run in range(srun, erun+1)]
            print 'Runs:\t', runs
            
            for run in runs:
                ifn = 'upk%03d%04d.root' % (int(date), int(run))
                ifn = os.path.join(datadir, ifn)
                ifns.append(ifn)
        else:
            print usage
                
    h = HitRate(ifns)
    if options.lsdir:
        h.lists()        
    else:
        ymax = options.ymax
        gAtm00  = h.hitRates('atm_nhits>0')
        gAtm03  = h.hitRates('atm_nhits>3')
        gAtm10  = h.hitRates('atm_nhits>10')
        gFadc00 = h.hitRates('fadc_nhits>0')
        gFadc03 = h.hitRates('fadc_nhits>3')
        gFadc10 = h.hitRates('fadc_nhits>10')
        ###
        c = TCanvas('c1', 'c1', 1000, 1000)
        c.Divide(1, 2)
        ###
        c.cd(1)
        gAtm00.SetMarkerColor(2)
        gAtm03.SetMarkerColor(3)
        gAtm10.SetMarkerColor(4)
        gAtm00.GetYaxis().SetRangeUser(0, ymax)
        gAtm00.Draw('ap')
        gAtm03.Draw('psame')
        gAtm10.Draw('psame')
        ###
        c.cd(2)
        gFadc00.SetMarkerColor(2)
        gFadc03.SetMarkerColor(3)
        gFadc10.SetMarkerColor(4)
        gFadc00.GetYaxis().SetRangeUser(0, ymax)
        gFadc00.Draw('ap')
        gFadc03.Draw('psame')
        gFadc10.Draw('psame')
        ###
        h.stop()
