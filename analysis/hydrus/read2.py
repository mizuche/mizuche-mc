#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import datetime
import inspect
import time

import logging
# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='debug.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(module)s.%(funcName)-20s %(message)s')
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle, gClient, gROOT
gStyle.SetTimeOffset(-788918400)
from ROOT import TFile, TChain, TCut
from ROOT import TH1D, TGraphErrors, TH2D
from ROOT import TCanvas, TLegend

mizulib = '../../mizulib/'
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

## __________________________________________________
class Config(object):
    name = 'Config'
    tnames=('upk', 'history', 'bsd', 'dst'),
    plot_dir = 'plots/'
    root_dir = 'rootfile/'
    bin_time = 600 #3600            # [sec]
    nsec = (0, 14000, 3500)    # (min, max, nbin)
    nhits = (0, 200, 200)
    ncharges = (0, 1000, 200)  # (0, 200, 200)
    npes = (0, 1000, 200)      # (0, 500, 500)
    window = (0, 200, 200)
    ntrg = (0,  10, 10)
    atm_qdc = (0, 5000, 5000)
    atm_tdc = (0, 5000, 5000)
    atm_rms = (0, 10, 10)
    atm_pe = (-100, 200, 300)
    atm_charge = (0, 1000, 1000)
    atm_time = (0, 1000, 1000)
    fadc = (-1000, 1000, 2000)
    nbuff = 2
    nch = 180
    fcorr = (0.088, 1.6)       # (qcorr, gcorr) for FADC corection factor
    timeformat = '#splitline{%m/%d}{%H:%M}'
    disph = gClient.GetDisplayHeight()
    dispw = gClient.GetDisplayWidth()
    disp = (dispw, disph)
    prefix = 'Tmp'

    ### range
    range_nsec = (2000, 8000)
    range_hitrate = (0, 0.15)
    range_window = (0.5, 5e4)

    ### analysis threshold
    thr_ntrg = (0, 1)
    thr_nhits = (0, 10, 20)
    thr_npes = (0, 20, 40, 60, 80, 100, 120)

    #cut0 = TCut('cut00', 'daqmode==10')
    cut0 = TCut('cut00', 'daqmode>0')
    cut1 = TCut('cut01', 'atm_ntrg  > {0:3d} && fadc_ntrg  > {0:3d}'.format(thr_ntrg[0]))
    cut2 = TCut('cut02', 'atm_ntrg == {0:3d} && fadc_ntrg == {0:3d}'.format(thr_ntrg[1]))
    cut3 = TCut('cut03', 'atm_nhits > {0:3d} && fadc_nhits > {0:3d}'.format(thr_nhits[0]))
    cut4 = TCut('cut04', 'atm_nhits > {0:3d} && fadc_nhits > {0:3d}'.format(thr_nhits[1]))
    cut5 = TCut('cut05', 'atm_nhits > {0:3d} && fadc_nhits > {0:3d}'.format(thr_nhits[2]))
    cut6 = TCut('cut06', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[0], fcorr[0], fcorr[1]))
    cut7 = TCut('cut07', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[1], fcorr[0], fcorr[1]))
    cut8 = TCut('cut08', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[2], fcorr[0], fcorr[1]))
    cut9 = TCut('cut09', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[3], fcorr[0], fcorr[1]))
    cut10 = TCut('cut10', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[4], fcorr[0], fcorr[1]))
    cut11 = TCut('cut11', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[5], fcorr[0], fcorr[1]))
    cut12 = TCut('cut12', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[6], fcorr[0], fcorr[1]))

    cuts = [cut0,
            cut1 + cut0,
            cut2 + cut0,
            cut3 + cut2 + cut0,
            cut4 + cut2 + cut0,
            cut5 + cut2 + cut0,
            cut6 + cut2 + cut0,
            cut7 + cut2 + cut0,
            cut8 + cut2 + cut0,
            cut9 + cut2 + cut0,
            cut10 + cut2 + cut0,
            cut11 + cut2 + cut0,
            cut12 + cut2 + cut0
            ]
    ncut = len(cuts)


## __________________________________________________
class Options(object):

    ## __________________________________________________
    def __init__(self, usage):
        pass

    ## __________________________________________________
    def parse_args(self):
        pass


## __________________________________________________
class Read(object):
    ## __________________________________________________
    def __init__(self, name, tname):
        '''Initialization'''
        self.set_object_name(name)
        self.set_class_name('Read')
        self.set_debug_mode(False)
        self.set_tree_name(tname)
        self.chain = TChain(tname)

        ## set logging.Logger
        self.logger = logging.getLogger('Read')
        self.logger.debug('-' * 50)
        self.logger.debug('initialized')

    ## __________________________________________________
    def __str__(self):
        '''
        print information of files added to TChain.
        '''
        ch = self.chain
        s = '[print]\tTree name : {0}\n'.format(ch.GetName())
        s += '[print]\tNtrees    : {0}\n'.format(ch.GetNtrees())
        s += '[print]\tEntries   : {0}\n'.format(ch.GetEntries())
        return s

    ## __________________________________________________
    def set_object_name(self, name):
        self.__name = name

    def get_object_name(self):
        return self.__name

    ## __________________________________________________
    def set_class_name(self, cname):
        '''Set class name.'''
        self.__class_name = cname

    def get_class_name(self):
        '''Get class name.'''
        return self.__class_name

    ## __________________________________________________
    def set_global_cut(self, cut):
        self.__gcut = cut

    def get_global_cut(self):
        return self.__gcut

    ## __________________________________________________
    def set_debug_mode(self, mode):
        '''Set debug mode.'''
        self.__debug_mode = mode

    def get_debug_mode(self):
        '''Get debug mode.'''
        return self.__debug_mode

    ## __________________________________________________
    def set_tree_name(self, tname):
        '''Set tree name.'''
        self.__tree_name = tname

    def get_tree_name(self):
        '''Get tree name.'''
        return self.__tree_name

    ## __________________________________________________
    name = property(get_object_name, set_object_name)
    tname = property(get_tree_name, set_tree_name)
    cname = property(get_class_name, set_class_name)
    debug = property(get_debug_mode, set_debug_mode)
    gcut = property(get_global_cut, set_global_cut)

    ## __________________________________________________
    def add(self, ifn):
        '''
        Add selected file to TChain
        '''
        if not os.path.exists(ifn):
            self.logger.error('File "%s" does not exist.' % ifn)
            sys.exit(-1)
        self.logger.info('Add "%s"', ifn)
        self.chain.Add(ifn)
        return self.chain

    ## __________________________________________________
    def stop(self):
        if gROOT.IsBatch():
            self.logger.warn('ROOT in batch mode')
            time.sleep(1)
        else:
            self.logger.warn('stop. Press "q" to quit >')
            gSystem.ProcessEvents()
            ans = raw_input('')
            if ans in ['q', 'Q']:
                sys.exit(-1)
            elif ans in ['.', '.q', '.Q']:
                return -1

    ## __________________________________________________
    def control(self, ientry):
        gSystem.ProcessEvents()
        ans = raw_input('Entry # %5d    Input entry# or [n]/p/q > ' % ientry)
        if ans in ['q', 'Q']:
            sys.exit(-1)
        elif ans in ['p', 'P']:
            return ientry-1
        elif ans in ['n', 'N', '']:
            return ientry+1
        else:
            return int(ans)

    ## __________________________________________________
    def list(self, lsdir):
        func = '%s:%s' % (self.file, 'list')
        sys.stderr.write('[%s]\tList files in "%s"\n' % (func, lsdir))
        com = 'ls %s' % lsdir
        os.system(com)

    # ## __________________________________________________
    # def lists(self):
    #     lsdirs = self.ifns
    #     for lsdir in lsdirs:
    #         self.list(lsdir)
    
    ## __________________________________________________
    def init_histograms(self, params, nhist):
        self.logger.info('initialize %d histograms', nhist)
        xmin = params['xmin']
        xmax = params['xmax']
        xbin = params['xbin']
        hnamefmt = params['hname']
        htitlefmt = params['htitle']
        hs = []
        for ihist in range(nhist):
            hname = hnamefmt % ihist
            htitle = htitlefmt % hname
            h = TH1D(hname, htitle, xbin, xmin, xmax)
            hs.append(h)
        return hs

    ## __________________________________________________
    def delete_histograms(self, hs):
        self.logger.info('delete histograms')
        for h in hs:
            h.Delete()

    ## __________________________________________________
    def init_th1d(self, hname, htitle, xtuple, num, timedisplay=0):
        self.logger.debug('init_th1d(%s, %s, %s)', htitle, num, timedisplay)
        xmin, xmax, xbin = xtuple
        hists = [TH1D(hname % i, htitle % i, xbin, xmin, xmax) for i in range(num)]
        for i in range(len(hists)):
            hists[i].SetStats(0)
            color = i % 9 + 1
            hists[i].SetLineColor(color)
            hists[i].SetMarkerColor(color)
            hists[i].SetFillColor(color)
            hists[i].GetXaxis().SetTimeFormat(Config.timeformat)
            hists[i].GetXaxis().SetTimeOffset(0, 'gmt')
            hists[i].GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                hists[i].GetXaxis().SetLabelOffset(0.015)
        return hists

    ## __________________________________________________
    def init_th2d(self, hname, htitle, xtuple, ytuple, num, timedisplay=0):
        self.logger.debug('init_th2d(%s, %s, %s)', htitle, num, timedisplay)
        xmin, xmax, xbin = xtuple
        ymin, ymax, ybin = ytuple
        hists = [TH2D(hname % i, htitle % i, xbin, xmin, xmax, ybin, ymin, ymax) for i in range(num)]
        for i in range(len(hists)):
            color = i % 8 + 1
            hists[i].SetStats(0)
            hists[i].SetLineColor(color)
            hists[i].SetMarkerColor(color)
            hists[i].GetXaxis().SetTimeFormat(Config.timeformat)
            hists[i].GetXaxis().SetTimeOffset(0, 'gmt')
            hists[i].GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                hists[i].GetXaxis().SetLabelOffset(0.015)
        return hists

    ## __________________________________________________
    def init_hist(self):
        self.logger.debug('initialize histograms')
        name = self.get_object_name()
        hists = {}
        ### init hRun, hRunRate
        hname = '{0}.hRun%d'.format(name)
        htitle = 'hRun%d;Run [#];Entries [#]'
        hRun = self.init_th1d(hname, htitle,
                              Config.run, Config.ncut)
        hists.update({'hRun': hRun})
        hname = '{0}.hRunRate%d'.format(name)
        htitle = 'hRunRate%d;Run [#];Hitrate [/RUN]'
        hRunRate = self.init_th1d(hname, htitle,
                                  Config.run, Config.ncut)
        hists.update({'hRunRate': hRunRate})
        hname = '{0}.hRunPot%d'.format(name)
        htitle = 'hRunPot%d;Run [#];Beam Power [POT]'
        hRunPot = self.init_th1d(hname, htitle,
                                 Config.run, num=1)
        hists.update({'hRunPot': hRunPot})
        ### init hTime, hTimeRate
        hname = '{0}.hTime%d'.format(name)
        htitle = 'hTime%d;Date/Time;Entries [#]'
        hTime = self.init_th1d(hname, htitle,
                               Config.utime, Config.ncut,
                               timedisplay=0)
        hists.update({'hTime': hTime})
        hname = '{0}.hTimeRate%d'.format(name)
        htitle = 'hTimeRate%d;Date/Time;Hitrate [/ {0} sec]'.format(Config.bin_time)
        hTimeRate = self.init_th1d(hname, htitle,
                                   Config.utime, Config.ncut,
                                   timedisplay=0)
        hists.update({'hTimeRate': hTimeRate})
        hname = '{0}.hTimePot%d'.format(name)
        htitle = 'hTimePot%d;Date/Time;Beam Power [POT]'
        hTimePot = self.init_th1d(hname, htitle,
                                  Config.utime, num=1)
        hists.update({'hTimePot': hTimePot})
        ### init hMrRun, hMrRunRate
        hname = '{0}.hMrRun%d'.format(name)
        htitle = 'hMrRun%d;MR [#];Entries [#]'
        hMrRun = self.init_th1d(hname, htitle,
                                Config.mrrun, Config.ncut)
        hists.update({'hMrRun': hMrRun})
        hname = '{0}.hMrRunRate%d'.format(name)
        htitle = 'hMrRunRate%d;MR [#];Hitrate [/MR RUN]'
        hMrRunRate = self.init_th1d(hname, htitle,
                                    Config.mrrun, Config.ncut)
        hists.update({'hMrRunRate': hMrRunRate})
        hname = '{0}.hMrRunPot%d'.format(name)
        htitle = 'hMrRunPot%d;MR [#];Beam Power [POT]'
        hMrRunPot = self.init_th1d(hname, htitle,
                                   Config.mrrun, num=1)
        hists.update({'hMrRunPot': hMrRunPot})
        ### init hUpkRun, hUpkRunRate
        hname = '{0}.hUpkRun%d'.format(name)
        htitle = 'hUpkRun%d;UpkRun [#];Entries [#]'
        hUpkRun = self.init_th1d(hname, htitle,
                                 Config.upkrun, Config.ncut)
        hists.update({'hUpkRun': hUpkRun})
        hname = '{0}.hUpkRunRate%d'.format(name)
        htitle = 'hUpkRunRate%d;UpkRun [#];Hitrate [/UpkRun]'
        hUpkRunRate = self.init_th1d(hname, htitle,
                                     Config.upkrun, Config.ncut)
        hists.update({'hUpkRunRate': hUpkRunRate})
        hname = '{0}.hUpkRunPot%d'.format(name)
        htitle = 'hUpkRunPot%d;UpkRun [#];Beam Power [POT]'
        hUpkRunPot = self.init_th1d(hname, htitle,
                                    Config.upkrun, num=1)
        hists.update({'hUpkRunPot': hUpkRunPot})
        ### init hNsecFadc
        hname = '{0}.hNsecFadc%d'.format(name)
        htitle = 'hNsecFadc%d;Timing [nsec];Entries [#]'
        hNsecFadc = self.init_th1d(hname, htitle,
                                   Config.nsec, Config.ncut)
        hists.update({'hNsecFadc': hNsecFadc})
        ### init hNhits*
        hname = '{0}.hNhitsAtm%d'.format(name)
        htitle = 'hNhitsAtm%d;NhitsAtm [#];Entries [#]'
        hNhitsAtm = self.init_th1d(hname, htitle,
                                   Config.nhits, Config.ncut)
        hists.update({'hNhitsAtm': hNhitsAtm})
        hname = '{0}.hNhitsFadc%d'.format(name)
        htitle = 'hNhitsFadc%d;NhitsFadc [#];Entries [#]'
        hNhitsFadc = self.init_th1d(hname, htitle,
                                    Config.nhits, Config.ncut)
        hists.update({'hNhitsFadc': hNhitsFadc})
        hname = '{0}.hNhitsAtmFadc%d'.format(name)
        htitle = 'hNhitsAtmFadc%d;NhitsAtm [#];NhitsFadc [#]'
        hNhitsAtmFadc = self.init_th2d(hname, htitle,
                                       Config.nhits, Config.nhits, Config.ncut)
        hists.update({'hNhitsAtmFadc': hNhitsAtmFadc})
        ### init hNcharges*
        hname = '{0}.hNchargesAtm%d'.format(name)
        htitle = 'hNchargesAtm%d;NchargesAtm [pC];Entries [#]'
        hNchargesAtm = self.init_th1d(hname, htitle,
                                      Config.ncharges, Config.ncut)
        hists.update({'hNchargesAtm': hNchargesAtm})
        hname = '{0}.hNchargesFadc%d'.format(name)
        htitle = 'hNchargesFadc%d;NchargesFadc [pC];Entries [#]'
        hNchargesFadc = self.init_th1d(hname, htitle,
                                       Config.ncharges, Config.ncut)
        hists.update({'hNchargesFadc': hNchargesFadc})
        hname = '{0}.hNchargesAtmFadc%d'.format(name)
        htitle = 'hNchargesAtmFadc%d;NchargesAtm [pC];NchargesFadc [pC]'
        hNchargesAtmFadc = self.init_th2d(hname, htitle,
                                          Config.ncharges, Config.ncharges, Config.ncut)
        hists.update({'hNchargesAtmFadc': hNchargesAtmFadc})
        ### init hNpes*
        hname = '{0}.hNpesAtm%d'.format(name)
        htitle = 'hNpesAtm%d;NpesAtm [p.e.];Entries [#]'
        hNpesAtm = self.init_th1d(hname, htitle,
                                  Config.npes, Config.ncut)
        hists.update({'hNpesAtm': hNpesAtm})
        hname = '{0}.hNpesFadc%d'.format(name)
        htitle = 'hNpesFadc%d;NpesFadc [p.e.];Entries [#]'
        hNpesFadc = self.init_th1d(hname, htitle,
                                   Config.npes, Config.ncut)
        hists.update({'hNpesFadc': hNpesFadc})
        hname = '{0}.hNpesAtmFadc%d'.format(name)
        htitle = 'hNpesAtmFadc%d;NpesAtm [p.e.];NpesFadc [p.e.]'
        hNpesAtmFadc = self.init_th2d(hname, htitle,
                                      Config.npes, Config.npes, Config.ncut)
        hists.update({'hNpesAtmFadc': hNpesAtmFadc})
        ### init hNtrg*
        hname = '{0}.hNtrgAtm%d'.format(name)
        htitle = 'hNtrgAtm%d;Ntrg [p.e.];Entries [#]'
        hNtrgAtm = self.init_th1d(hname, htitle,
                                  Config.ntrg, Config.ncut)
        hists.update({'hNtrgAtm': hNtrgAtm})
        hname = '{0}.hNtrgFadc%d'.format(name)
        htitle = 'hNtrgFadc%d;Ntrg [p.e.];Entries [#]'
        hNtrgFadc = self.init_th1d(hname, htitle,
                                   Config.ntrg, Config.ncut)
        hists.update({'hNtrgFadc': hNtrgFadc})
        hname = '{0}.hNtrgAtmFadc%d'.format(name)
        htitle = 'hNtrgAtmFadc%d;NtrgAtm [#];NtrgFadc [#]'
        hNtrgAtmFadc = self.init_th2d(hname, htitle,
                                      Config.ntrg, Config.ntrg, Config.ncut)
        hists.update({'hNtrgAtmFadc': hNtrgAtmFadc})
        ### init hWindow
        hname = '{0}.hWindowMap%d'.format(name)
        htitle = 'hWindowMap%d;Window [#];Entries [#]'
        hWindow = self.init_th1d(hname, htitle,
                                 Config.window, Config.ncut)
        hists.update({'hWindow': hWindow})
        ### init hTimeQdc*
        hname = '{0}.hTimeQdc0%d'.format(name)
        htitle = 'hTimeQdc0%d;Date/Time;Qdc0 [#]'
        hTimeQdc0 = self.init_th2d(hname, htitle,
                                   Config.utime, Config.atm_qdc, Config.nbuff,
                                   timedisplay=1)
        hists.update({'hTimeQdc0': hTimeQdc0})
        hname = '{0}.hTimeQdc1%d'.format(name)
        htitle = 'hTimeQdc1%d;Date/Time;Qdc1 [#]'
        hTimeQdc1 = self.init_th2d(hname, htitle,
                                   Config.utime, Config.atm_qdc, Config.nbuff,
                                   timedisplay=1)
        hists.update({'hTimeQdc1': hTimeQdc1})
        ### init hTimeTdc*
        hname = '{0}.hTimeTdc0%d'.format(name)
        htitle = 'hTimeTdc0%d;Date/Time;Tdc0 [#]'
        hTimeTdc0 = self.init_th2d(hname, htitle,
                                   Config.utime, Config.atm_tdc, Config.nbuff,
                                   timedisplay=1)
        hists.update({'hTimeTdc0': hTimeTdc0})
        hname = '{0}.hTimeTdc1%d'.format(name)
        htitle = 'hTimeTdc1%d;Date/Time;Tdc1 [#]'
        hTimeTdc1 = self.init_th2d(hname, htitle,
                                   Config.utime, Config.atm_tdc, Config.nbuff,
                                   timedisplay=1)
        hists.update({'hTimeTdc1': hTimeTdc1})
        ### init hTimeRaw*sum0
        hname = '{0}.hTimeRawPmtsum0%d'.format(name)
        htitle = 'hTimeRawPmtsum0%d;Date/Time;RawPmtsum0 [mV]'
        hTimeRawPmtsum0 = self.init_th2d(hname, htitle,
                                         Config.utime, Config.fadc, num=1,
                                         timedisplay=1)
        hists.update({'hTimeRawPmtsum0': hTimeRawPmtsum0})
        hname = '{0}.hTimeRawHitsum0%d'.format(name)
        htitle = 'hTimeRawHitsum0%d;Date/Time;RawHitsum0 [mV]'
        hTimeRawHitsum0 = self.init_th2d(hname, htitle,
                                         Config.utime, Config.fadc, num=1,
                                         timedisplay=1)
        hists.update({'hTimeRawHitsum0': hTimeRawHitsum0})
        ### init hTimeFadc*sum0
        hname = '{0}.hTimeFadcPmtsum0%d'.format(name)
        htitle = 'hTimeFadcPmtsum0%d;Date/Time;FadcPmtsum0 [mV]'
        hTimeFadcPmtsum0 = self.init_th2d(hname, htitle,
                                          Config.utime, Config.fadc, num=1,
                                          timedisplay=1)
        hists.update({'hTimeFadcPmtsum0': hTimeFadcPmtsum0})
        hname = '{0}.hTimeFadcHitsum0%d'.format(name)
        htitle = 'hTimeFadcHitsum0%d;Date/Time;FadcHitsum0 [mV]'
        hTimeFadcHitsum0 = self.init_th2d(hname, htitle,
                                          Config.utime, Config.fadc, num=1,
                                          timedisplay=1)
        hists.update({'hTimeFadcHitsum0': hTimeFadcHitsum0})
        return hists

    ## __________________________________________________
    def get_xmin_xmax(self):
        self.logger.info('get min/max for x-axis')
        ch = self.chain
        tname = ch.GetName()
        nentries = ch.GetEntries()
        
        ### Get Start Value
        ch.GetEntry(0)
        if tname == 'dst':
            s_run = e_run = ch.dst.GetRunNum()
            s_time = e_time = ch.dst.GetTimeSec()
            s_mrrun = e_mrrun = ch.dst.GetBsdMrRun()
            s_upkrun = e_upkrun = ch.dst.GetUpkRun()
        else:
            s_run = e_run = ch.upk.GetRunNum()
            s_time = e_time = ch.upk.GetTimeSec()
            s_mrrun = e_mrrun = ch.upk.GetBsdMrRun()
            s_upkrun = e_upkrun = ch.upk.GetUpkRun()
        ### Get Min or Max Value
        for i in range(nentries):
            if i % 10000 == 0:
                percent = float(i)/float(nentries) * 100
                sys.stderr.write('... {0:5.2f}%\r'.format(percent))
            ch.GetEntry(i)

            if tname == 'dst':
                t_run = ch.dst.GetRunNum()
                t_time = ch.dst.GetTimeSec()
                t_mrrun = ch.dst.GetBsdMrRun()
                t_upkrun = ch.dst.GetUpkRun()
            else:
                t_run = ch.upk.GetRunNum()
                t_time = ch.upk.GetTimeSec()
                t_mrrun = ch.upk.GetBsdMrRun()
                t_upkrun = ch.upk.GetUpkRun()

            s_run, e_run = min(s_run, t_run), max(e_run, t_run)
            s_time, e_time = min(s_time, t_time), max(e_time, t_time)
            s_mrrun, e_mrrun = min(s_mrrun, t_mrrun), max(e_mrrun, t_mrrun)
            s_upkrun, e_upkrun = min(s_upkrun, t_upkrun), max(e_upkrun, t_upkrun)

        ### RUN
        n_run = e_run % 10000 - s_run % 10000
        run = (s_run % 10000, e_run % 10000 + 1, n_run + 1)
        ### Time
        bin_time = Config.bin_time
        s_time = (s_time / bin_time) * bin_time
        e_time = (e_time / bin_time + 1) * bin_time
        n_time = (e_time - s_time) / bin_time
        utime = (s_time, e_time, n_time)
        ### MR RUN
        n_mrrun = e_mrrun - s_mrrun
        mrrun = (s_mrrun, e_mrrun + 1, n_mrrun + 1)
        ### UpkRun
        n_upkrun = e_upkrun % 10000 - s_upkrun % 10000
        upkrun = (s_upkrun % 10000, e_upkrun % 10000 + 1, n_upkrun + 1)

        Config.run = run
        Config.utime = utime
        Config.mrrun = mrrun
        Config.upkrun = upkrun

        prefix = None
        upkruns = sorted(set([s_upkrun, e_upkrun]))
        if len(upkruns) == 1:
            prefix = '{0}{1:07d}'.format(self.name.capitalize(),
                                         upkruns[0])
        else:
            prefix = '{0}{1:07d}{0}{2:07d}'.format(self.name.capitalize(),
                                                   upkruns[0],
                                                   upkruns[1])
        if prefix:
            Config.prefix = prefix
        self.logger.info('set filename prefix : {0}'.format(Config.prefix))

        return

    ## __________________________________________________
    def process(self):
        tname = self.get_tree_name()
        start = datetime.datetime.now()
        self.logger.info('-' * 50)
        self.logger.info('process "{0}"'.format(tname))
        if tname == 'upk':
            self.process_upk()
                #self.process_upk2()
        elif tname == 'history':
            self.process_history()
        elif tname == 'bsd':
            self.process_bsd()
        elif tname == 'dst':
            self.process_dst()
        else:
            raise NotImplemented()
        # try:
        #     if tname == 'upk':
        #         self.process_upk()
        #         #self.process_upk2()
        #     elif tname == 'history':
        #         self.process_history()
        #     elif tname == 'bsd':
        #         self.process_bsd()
        #     elif tname == 'dst':
        #         self.process_dst()

        #         raise NotImplemented()
        # except AttributeError:
        #     sys.stderr.write('[process]\tAttributeError : ')
        #     sys.stderr.write('This TChain does not have tree named "{0}"\n'.format(tname))
        #     sys.exit(-1)

        end = datetime.datetime.now()
        self.logger.info('finished ({0} [sec])'.format(end-start))
        self.logger.info('-' * 50)
        return

    ## __________________________________________________
    def draw(self):
        tname = self.get_tree_name()
        start = datetime.datetime.now()
        self.logger.info('-' * 50)
        self.logger.info('draw "{0}"'.format(tname))
        if tname == 'upk':
            self.draw_upk()
        elif tname == 'dst':
            self.draw_dst()
        elif tname == 'history':
            self.draw_history()
        elif tname == 'bsd':
            self.draw_bsd()
        else:
            raise NotImplemented()
        end = datetime.datetime.now()
        self.logger.info('finished ({0} [sec])'.format(end-start))
        self.logger.info('-' * 50)
        return

    ## __________________________________________________
    def process_upk(self):
        self.logger.info('started')
        # name = self.get_object_name()
        ch = self.chain
        nentries = ch.GetEntries()
        if nentries == 0:
            self.logger.error('No entry. Exit.')
            sys.exit(-1)
        ### Get Min or Max Value
        self.get_xmin_xmax()
        ### initialize histograms ###
        h = self.init_hist()
        ###
        hRun = h['hRun']
        hTime = h['hTime']
        hUpkRun = h['hUpkRun']
        ###
        hRunRate = h['hRunRate']
        hTimeRate = h['hTimeRate']
        hUpkRunRate = h['hUpkRunRate']
        ###
        hNsecFadc = h['hNsecFadc']
        ###
        hNhitsAtm = h['hNhitsAtm']
        hNhitsFadc = h['hNhitsFadc']
        hNhitsAtmFadc = h['hNhitsAtmFadc']
        ###
        hNchargesAtm = h['hNchargesAtm']
        hNchargesFadc = h['hNchargesFadc']
        hNchargesAtmFadc = h['hNchargesAtmFadc']
        ###
        hNpesAtm = h['hNpesAtm']
        hNpesFadc = h['hNpesFadc']
        hNpesAtmFadc = h['hNpesAtmFadc']
        ###
        hNtrgAtm = h['hNtrgAtm']
        hNtrgFadc = h['hNtrgFadc']
        hNtrgAtmFadc = h['hNtrgAtmFadc']
        ###
        hWindow = h['hWindow']
        ###
        hTimeQdc0 = h['hTimeQdc0']
        hTimeQdc1 = h['hTimeQdc1']
        hTimeTdc0 = h['hTimeTdc0']
        hTimeTdc1 = h['hTimeTdc1']
        ###
        hTimeRawPmtsum0 = h['hTimeRawPmtsum0']
        hTimeRawHitsum0 = h['hTimeRawHitsum0']
        hTimeFadcPmtsum0 = h['hTimeFadcPmtsum0']
        hTimeFadcHitsum0 = h['hTimeFadcHitsum0']

        (qcorr, gcorr) = Config.fcorr
        cuts = Config.cuts

        self.logger.info('project branches')
        for i in range(Config.ncut):
            self.logger.debug('cut{0} : {1}'.format(i, cuts[i].GetTitle()))
            ch.Project(hRun[i].GetName(), 'runnum%10000', cuts[i].GetTitle())
            ch.Project(hRunRate[i].GetName(), 'runnum%10000', cuts[i].GetTitle())
            ch.Project(hTime[i].GetName(), 'time_sec', cuts[i].GetTitle())
            ch.Project(hTimeRate[i].GetName(), 'time_sec', cuts[i].GetTitle())
            ch.Project(hUpkRun[i].GetName(), 'upkrun%10000', cuts[i].GetTitle())
            ch.Project(hUpkRunRate[i].GetName(), 'upkrun%10000', cuts[i].GetTitle())
            ch.Project(hNtrgAtm[i].GetName(), 'atm_ntrg', cuts[i].GetTitle())
            ch.Project(hNtrgFadc[i].GetName(), 'fadc_ntrg', cuts[i].GetTitle())
            ch.Project(hNtrgAtmFadc[i].GetName(), 'fadc_ntrg:atm_ntrg', cuts[i].GetTitle())
            ch.Project(hNhitsAtm[i].GetName(), 'atm_nhits', cuts[i].GetTitle())
            ch.Project(hNhitsFadc[i].GetName(), 'fadc_nhits', cuts[i].GetTitle())
            ch.Project(hNhitsAtmFadc[i].GetName(), 'fadc_nhits:atm_nhits', cuts[i].GetTitle())
            ch.Project(hNchargesAtm[i].GetName(), 'atm_ncharges', cuts[i].GetTitle())
            ch.Project(hNchargesFadc[i].GetName(), 'fadc_ncharges/{0}'.format(qcorr), cuts[i].GetTitle())
            ch.Project(hNchargesAtmFadc[i].GetName(), 'fadc_ncharges/{0}:atm_ncharges'.format(qcorr), cuts[i].GetTitle())
            ch.Project(hNpesAtm[i].GetName(), 'atm_npes', cuts[i].GetTitle())
            ch.Project(hNpesFadc[i].GetName(), 'fadc_npes/{0}/{1}'.format(qcorr, gcorr), cuts[i].GetTitle())
            ch.Project(hNpesAtmFadc[i].GetName(), 'fadc_npes/{0}/{1}:atm_npes'.format(qcorr, gcorr), cuts[i].GetTitle())
            ch.Project(hNsecFadc[i].GetName(), 'fadc_nsec', cuts[i].GetTitle())
            ch.Project(hWindow[i].GetName(), 'window', cuts[i].GetTitle())

        self.logger.info('project QDC, TDC pedestals')
        for i in range(Config.nbuff):
            ch.Project(hTimeQdc0[i].GetName(), 'qdc0[][{0}]:time_sec'.format(i))
            ch.Project(hTimeQdc1[i].GetName(), 'qdc1[][{0}]:time_sec'.format(i))
            ch.Project(hTimeTdc0[i].GetName(), 'tdc0[][{0}]:time_sec'.format(i))
            ch.Project(hTimeTdc1[i].GetName(), 'tdc1[][{0}]:time_sec'.format(i))

        self.logger.info('project HITSUM, PMTSUM pedestals')
        ch.Project(hTimeRawPmtsum0[0].GetName(), 'raw_pmtsum0:time_sec')
        ch.Project(hTimeRawHitsum0[0].GetName(), 'raw_hitsum0:time_sec')
        ch.Project(hTimeFadcPmtsum0[0].GetName(), 'fadc_pmtsum0:time_sec')
        ch.Project(hTimeFadcHitsum0[0].GetName(), 'fadc_hitsum0:time_sec')

        for h in hRunRate:
            h.Divide(hRun[0])
            h.SetStats(0)
        for h in hUpkRunRate:
            h.Divide(hUpkRun[0])
            h.SetStats(0)
        for h in hTimeRate:
            h.Divide(hTime[0])
            h.GetXaxis().SetTimeDisplay(1)
            h.GetXaxis().SetLabelOffset(0.015)
            h.SetStats(0)

        ###
        self.hRun = hRun
        self.hTime = hTime
        self.hUpkRun = hUpkRun
        ###
        self.hRunRate = hRunRate
        self.hTimeRate = hTimeRate
        self.hUpkRunRate = hUpkRunRate
        ###
        self.hNsecFadc = hNsecFadc
        ###
        self.hWindow = hWindow
        ###
        self.hNtrgAtm = hNtrgAtm
        self.hNtrgFadc = hNtrgFadc
        self.hNtrgAtmFadc = hNtrgAtmFadc
        ###
        self.hNhitsAtm = hNhitsAtm
        self.hNhitsFadc = hNhitsFadc
        self.hNhitsAtmFadc = hNhitsAtmFadc
        ###
        self.hNchargesAtm = hNchargesAtm
        self.hNchargesFadc = hNchargesFadc
        self.hNchargesAtmFadc = hNchargesAtmFadc
        ###
        self.hNpesAtm = hNpesAtm
        self.hNpesFadc = hNpesFadc
        self.hNpesAtmFadc = hNpesAtmFadc
        ###
        self.hTimeQdc0 = hTimeQdc0
        self.hTimeQdc1 = hTimeQdc1
        self.hTimeTdc0 = hTimeTdc0
        self.hTimeTdc1 = hTimeTdc1
        ###
        self.hTimeRawPmtsum0 = hTimeRawPmtsum0
        self.hTimeRawHitsum0 = hTimeRawHitsum0
        self.hTimeFadcPmtsum0 = hTimeFadcPmtsum0
        self.hTimeFadcHitsum0 = hTimeFadcHitsum0
        self.logger.info('finished')
        return

    ## __________________________________________________
    def process_upk2(self):
        self.logger.info('started')
        ch = self.chain
        nentries = ch.GetEntries()
        if nentries == 0:
            self.logger.error('No entry. Exit.\n')
            sys.exit(-1)
        ### Get Min or Max Value
        self.get_xmin_xmax()
        ### initialize histograms ###
        h = self.init_hist()
        ###
        hRun = h['hRun']
        hTime = h['hTime']
        hUpkRun = h['hUpkRun']
        ###
        hRunRate = h['hRunRate']
        hTimeRate = h['hTimeRate']
        hUpkRunRate = h['hUpkRunRate']
        ###
        hNsecFadc = h['hNsecFadc']
        ###
        hNhitsAtm = h['hNhitsAtm']
        hNhitsFadc = h['hNhitsFadc']
        hNhitsAtmFadc = h['hNhitsAtmFadc']
        ###
        hNchargesAtm = h['hNchargesAtm']
        hNchargesFadc = h['hNchargesFadc']
        hNchargesAtmFadc = h['hNchargesAtmFadc']
        ###
        hNpesAtm = h['hNpesAtm']
        hNpesFadc = h['hNpesFadc']
        hNpesAtmFadc = h['hNpesAtmFadc']
        ###
        hNtrgAtm = h['hNtrgAtm']
        hNtrgFadc = h['hNtrgFadc']
        hNtrgAtmFadc = h['hNtrgAtmFadc']
        ###
        hWindow = h['hWindow']
        ###
        hTimeQdc0 = h['hTimeQdc0']
        hTimeQdc1 = h['hTimeQdc1']
        hTimeTdc0 = h['hTimeTdc0']
        hTimeTdc1 = h['hTimeTdc1']
        ###
        hTimeRawPmtsum0 = h['hTimeRawPmtsum0']
        hTimeRawHitsum0 = h['hTimeRawHitsum0']
        hTimeFadcPmtsum0 = h['hTimeFadcPmtsum0']
        hTimeFadcHitsum0 = h['hTimeFadcHitsum0']

        # cut1 = TCut('cut1', 'atm_ntrg  > {0:3d} && fadc_ntrg  > {0:3d}'.format(thr_ntrg[0]))
        # cut2 = TCut('cut2', 'atm_ntrg == {0:3d} && fadc_ntrg == {0:3d}'.format(thr_ntrg[1]))
        # cut3 = TCut('cut3', 'atm_nhits > {0:3d} && fadc_nhits > {0:3d}'.format(thr_nhits[0]))
        # cut4 = TCut('cut4', 'atm_nhits > {0:3d} && fadc_nhits > {0:3d}'.format(thr_nhits[1]))
        # cut5 = TCut('cut5', 'atm_nhits > {0:3d} && fadc_nhits > {0:3d}'.format(thr_nhits[2]))
        # cut6 = TCut('cut6', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[0], fcorr[0], fcorr[1]))
        # cut7 = TCut('cut7', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[1], fcorr[0], fcorr[1]))
        # cut8 = TCut('cut8', 'atm_npes  > {0:3d} && fadc_npes/{1}/{2}  > {0:3d}'.format(thr_npes[2], fcorr[0], fcorr[1]))

        for i in range(nentries):
            ch.GetEntry(i)
            ntrg = self.ntrg_cut(ch.upk, cut=2)
            nhits = self.nhits_cut(ch.upk, cut=0)
            npes = self.npes_cut(ch.upk, cut=0)
            print i, ntrg, nhits, npes

            # cut0 = TCut('cut0', '')
            self.fill_runnum(h, ch.upk, 0)
            self.fill_time(h, ch.upk, 0)
            self.fill_ntrg(h, ch.upk, 0)
            self.fill_nhits(h, ch.upk, 0)
            self.fill_ncharges(h, ch.upk, 0)
            self.fill_npes(h, ch.upk, 0)

        h['hRun'][0].Draw()
        self.stop()
            #hRunRate[0].Fill(runnum % )
        # (qcorr, gcorr) = Config.fcorr
        # cuts = Config.cuts

        # sys.stderr.write('[process_upk]\tproject branches\n')
        # for i in range(Config.ncut):
        #     sys.stderr.write('[{0}]\tcut{1} : {2}\n'.format(Config.name, i, cuts[i].GetTitle()))
        #     sys.stderr.flush
        #     ch.Project(hNsecFadc[i].GetName(), 'fadc_nsec', cuts[i].GetTitle())
        #     ch.Project(hWindow[i].GetName(), 'window', cuts[i].GetTitle())

        # sys.stderr.write('[process_upk]\tproject QDC, TDC pedestals\n')
        # for i in range(Config.nbuff):
        #     ch.Project(hTimeQdc0[i].GetName(), 'qdc0[][{0}]:time_sec'.format(i))
        #     ch.Project(hTimeQdc1[i].GetName(), 'qdc1[][{0}]:time_sec'.format(i))
        #     ch.Project(hTimeTdc0[i].GetName(), 'tdc0[][{0}]:time_sec'.format(i))
        #     ch.Project(hTimeTdc1[i].GetName(), 'tdc1[][{0}]:time_sec'.format(i))

        # sys.stderr.write('[process_upk]\tproject HITSUM, PMTSUM pedestals\n')
        # ch.Project(hTimeRawPmtsum0[0].GetName(), 'raw_pmtsum0:time_sec')
        # ch.Project(hTimeRawHitsum0[0].GetName(), 'raw_hitsum0:time_sec')
        # ch.Project(hTimeFadcPmtsum0[0].GetName(), 'fadc_pmtsum0:time_sec')
        # ch.Project(hTimeFadcHitsum0[0].GetName(), 'fadc_hitsum0:time_sec')

        # for h in hRunRate:
        #     h.Divide(hRun[0])
        #     h.SetStats(0)
        # for h in hUpkRunRate:
        #     h.Divide(hUpkRun[0])
        #     h.SetStats(0)
        # for h in hTimeRate:
        #     h.Divide(hTime[0])
        #     h.GetXaxis().SetTimeDisplay(1)
        #     h.GetXaxis().SetLabelOffset(0.015)
        #     h.SetStats(0)

        # ###
        # self.hRun = hRun
        # self.hTime = hTime
        # self.hUpkRun = hUpkRun
        # ###
        # self.hRunRate = hRunRate
        # self.hTimeRate = hTimeRate
        # self.hUpkRunRate = hUpkRunRate
        # ###
        # self.hNsecFadc = hNsecFadc
        # ###
        # self.hWindow = hWindow
        # ###
        # self.hNtrgAtm = hNtrgAtm
        # self.hNtrgFadc = hNtrgFadc
        # self.hNtrgAtmFadc = hNtrgAtmFadc
        # ###
        # self.hNhitsAtm = hNhitsAtm
        # self.hNhitsFadc = hNhitsFadc
        # self.hNhitsAtmFadc = hNhitsAtmFadc
        # ###
        # self.hNchargesAtm = hNchargesAtm
        # self.hNchargesFadc = hNchargesFadc
        # self.hNchargesAtmFadc = hNchargesAtmFadc
        # ###
        # self.hNpesAtm = hNpesAtm
        # self.hNpesFadc = hNpesFadc
        # self.hNpesAtmFadc = hNpesAtmFadc
        # ###
        # self.hTimeQdc0 = hTimeQdc0
        # self.hTimeQdc1 = hTimeQdc1
        # self.hTimeTdc0 = hTimeTdc0
        # self.hTimeTdc1 = hTimeTdc1
        # ###
        # self.hTimeRawPmtsum0 = hTimeRawPmtsum0
        # self.hTimeRawHitsum0 = hTimeRawHitsum0
        # self.hTimeFadcPmtsum0 = hTimeFadcPmtsum0
        # self.hTimeFadcHitsum0 = hTimeFadcHitsum0

        self.logger.info('finished')
        return

    ## __________________________________________________
    def process_dst(self):
        self.logger.info('started')
        ch = self.chain
        nentries = ch.GetEntries()
        if nentries == 0:
            self.logger.error('No entry. Exit.')
            sys.exit(-1)
        ### Get Min or Max Value
        self.get_xmin_xmax()
        ### initialize histograms ###
        h = self.init_hist()
        hRun = h['hRun']
        hTime = h['hTime']
        hMrRun = h['hMrRun']
        hUpkRun = h['hUpkRun']
        ###
        hRunRate = h['hRunRate']
        hTimeRate = h['hTimeRate']
        hMrRunRate = h['hMrRunRate']
        hUpkRunRate = h['hUpkRunRate']
        ###
        hNsecFadc = h['hNsecFadc']
        ###
        hNhitsAtm = h['hNhitsAtm']
        hNhitsFadc = h['hNhitsFadc']
        hNhitsAtmFadc = h['hNhitsAtmFadc']
        ###
        hNchargesAtm = h['hNchargesAtm']
        hNchargesFadc = h['hNchargesFadc']
        hNchargesAtmFadc = h['hNchargesAtmFadc']
        ###
        hNpesAtm = h['hNpesAtm']
        hNpesFadc = h['hNpesFadc']
        hNpesAtmFadc = h['hNpesAtmFadc']
        ###
        hNtrgAtm = h['hNtrgAtm']
        hNtrgFadc = h['hNtrgFadc']
        hNtrgAtmFadc = h['hNtrgAtmFadc']
        ###
        hWindow = h['hWindow']
        ###
        hTimeQdc0 = h['hTimeQdc0']
        hTimeQdc1 = h['hTimeQdc1']
        hTimeTdc0 = h['hTimeTdc0']
        hTimeTdc1 = h['hTimeTdc1']
        ###
        hTimeRawPmtsum0 = h['hTimeRawPmtsum0']
        hTimeRawHitsum0 = h['hTimeRawHitsum0']
        hTimeFadcPmtsum0 = h['hTimeFadcPmtsum0']
        hTimeFadcHitsum0 = h['hTimeFadcHitsum0']
        ###
        hRunPot = h['hRunPot']
        hTimePot = h['hTimePot']
        hMrRunPot = h['hMrRunPot']
        hUpkRunPot = h['hUpkRunPot']

        (qcorr, gcorr) = Config.fcorr
        cuts = Config.cuts
        good = TCut('good', 'good_spill_flag==1')

        self.logger.info('fill histograms')
        for i in range(len(cuts)):
            cuts[i] = cuts[i] + good
            self.logger.debug('cut{0} : {1}\n'.format(i, cuts[i].GetTitle()))
            ch.Project(hRun[i].GetName(), 'runnum%10000', cuts[i].GetTitle())
            ch.Project(hRunRate[i].GetName(), 'runnum%10000', cuts[i].GetTitle())
            ch.Project(hTime[i].GetName(), 'time_sec', cuts[i].GetTitle())
            ch.Project(hTimeRate[i].GetName(), 'time_sec', cuts[i].GetTitle())
            ch.Project(hNtrgAtm[i].GetName(), 'atm_ntrg', cuts[i].GetTitle())
            ch.Project(hNtrgFadc[i].GetName(), 'fadc_ntrg', cuts[i].GetTitle())
            ch.Project(hNtrgAtmFadc[i].GetName(), 'fadc_ntrg:atm_ntrg', cuts[i].GetTitle())
            ch.Project(hNhitsAtm[i].GetName(), 'atm_nhits', cuts[i].GetTitle())
            ch.Project(hNhitsFadc[i].GetName(), 'fadc_nhits', cuts[i].GetTitle())
            ch.Project(hNhitsAtmFadc[i].GetName(), 'fadc_nhits:atm_nhits', cuts[i].GetTitle())
            ch.Project(hNchargesAtm[i].GetName(), 'atm_ncharges', cuts[i].GetTitle())
            ch.Project(hNchargesFadc[i].GetName(), 'fadc_ncharges/{0}'.format(qcorr), cuts[i].GetTitle())
            ch.Project(hNchargesAtmFadc[i].GetName(), 'fadc_ncharges/{0}:atm_ncharges'.format(qcorr), cuts[i].GetTitle())
            ch.Project(hNpesAtm[i].GetName(), 'atm_npes', cuts[i].GetTitle())
            ch.Project(hNpesFadc[i].GetName(), 'fadc_npes/{0}/{1}'.format(qcorr, gcorr), cuts[i].GetTitle())
            ch.Project(hNpesAtmFadc[i].GetName(), 'fadc_npes/{0}/{1}:atm_npes'.format(qcorr, gcorr), cuts[i].GetTitle())
            ch.Project(hNsecFadc[i].GetName(), 'fadc_nsec', cuts[i].GetTitle())
            ch.Project(hWindow[i].GetName(), 'window', cuts[i].GetTitle())
            ch.Project(hMrRun[i].GetName(), 'bsd_mrrun', cuts[i].GetTitle())
            ch.Project(hMrRunRate[i].GetName(), 'bsd_mrrun', cuts[i].GetTitle())
            ch.Project(hUpkRun[i].GetName(), 'upkrun%10000', cuts[i].GetTitle())
            ch.Project(hUpkRunRate[i].GetName(), 'upkrun%10000', cuts[i].GetTitle())

        self.logger.info('fill hTimeQdc0, hTimeTdc0')
        for i in range(Config.nbuff):
            ch.Project(hTimeQdc0[i].GetName(), 'qdc0[][{0}]:time_sec'.format(i))
            ch.Project(hTimeQdc1[i].GetName(), 'qdc1[][{0}]:time_sec'.format(i))
            ch.Project(hTimeTdc0[i].GetName(), 'tdc0[][{0}]:time_sec'.format(i))
            ch.Project(hTimeTdc1[i].GetName(), 'tdc1[][{0}]:time_sec'.format(i))

        self.logger.info('fill hTimeRawPmtsum0, hTimeFadcPmtsum0')
        ch.Project(hTimeRawPmtsum0[0].GetName(), 'raw_pmtsum0:time_sec')
        ch.Project(hTimeRawHitsum0[0].GetName(), 'raw_hitsum0:time_sec')
        ch.Project(hTimeFadcPmtsum0[0].GetName(), 'fadc_pmtsum0:time_sec')
        ch.Project(hTimeFadcHitsum0[0].GetName(), 'fadc_hitsum0:time_sec')

        self.logger.info('fill POT')
        for i in range(nentries):
            if i % 10000 == 0:
                percent = float(i)/float(nentries) * 100
                sys.stderr.write('... {0:5.2f}%\r'.format(percent))
            ch.GetEntry(i)
            sec = ch.dst.GetTimeSec()
            run = ch.dst.GetRunNum() % 10000
            upkrun = ch.dst.GetUpkRun() % 10000
            mrrun = ch.dst.GetBsdMrRun()
            pot = ch.dst.GetBsdCt5()

            hRunPot[0].Fill(run, pot)
            hTimePot[0].Fill(sec, pot)
            hMrRunPot[0].Fill(mrrun, pot)
            hUpkRunPot[0].Fill(upkrun, pot)

        fPotRun = hRunPot[0].Integral()
        fPotTime = hTimePot[0].Integral()
        fPotUpk = hUpkRunPot[0].Integral()
        fPotMr = hMrRunPot[0].Integral()

        self.logger.info('{0:.5e} POT / Run'.format(fPotRun))
        self.logger.info('{0:.5e} POT / Time'.format(fPotTime))
        self.logger.info('{0:.5e} POT / UpkRun'.format(fPotUpk))
        self.logger.info('{0:.5e} POT / MrRun'.format(fPotMr))

        hTimePot[0].GetXaxis().SetTimeDisplay(1)

        for h in hRunRate:
            h.Divide(hRun[0])
            h.SetStats(0)
        for h in hTimeRate:
            h.Divide(hTime[0])
            h.GetXaxis().SetTimeDisplay(1)
            h.GetXaxis().SetLabelOffset(0.02)
            h.SetStats(0)
        for h in hMrRunRate:
            h.Divide(hMrRun[0])
            h.SetStats(0)
        for h in hUpkRunRate:
            h.Divide(hUpkRun[0])
            h.SetStats(0)

        ###
        self.hRun = hRun
        self.hTime = hTime
        self.hMrRun = hMrRun
        self.hUpkRun = hUpkRun
        ###
        self.hRunRate = hRunRate
        self.hTimeRate = hTimeRate
        self.hMrRunRate = hMrRunRate
        self.hUpkRunRate = hUpkRunRate
        ###
        self.hNsecFadc = hNsecFadc
        ###
        self.hWindow = hWindow
        ###
        self.hNtrgAtm = hNtrgAtm
        self.hNtrgFadc = hNtrgFadc
        self.hNtrgAtmFadc = hNtrgAtmFadc
        ###
        self.hNhitsAtm = hNhitsAtm
        self.hNhitsFadc = hNhitsFadc
        self.hNhitsAtmFadc = hNhitsAtmFadc
        ###
        self.hNchargesAtm = hNchargesAtm
        self.hNchargesFadc = hNchargesFadc
        self.hNchargesAtmFadc = hNchargesAtmFadc
        ###
        self.hNpesAtm = hNpesAtm
        self.hNpesFadc = hNpesFadc
        self.hNpesAtmFadc = hNpesAtmFadc
        ###
        self.hTimeQdc0 = hTimeQdc0
        self.hTimeQdc1 = hTimeQdc1
        ###
        self.hTimeTdc0 = hTimeTdc0
        self.hTimeTdc1 = hTimeTdc1
        ###
        self.hTimeRawPmtsum0 = hTimeRawPmtsum0
        self.hTimeRawHitsum0 = hTimeRawHitsum0
        ###
        self.hTimeFadcPmtsum0 = hTimeFadcPmtsum0
        self.hTimeFadcHitsum0 = hTimeFadcHitsum0
        ###
        self.hRunPot = hRunPot
        self.hTimePot = hTimePot
        self.hMrRunPot = hMrRunPot
        self.hUpkRunPot = hUpkRunPot
        ###
        self.logger.info('finished')
        return

    ## __________________________________________________
    def process_history(self):
        self.logger.info('started\n')
        name = self.get_object_name()
        ch = self.chain
        nentries = ch.GetEntries()
        if nentries == 0:
            self.logger.error('No entry. Exit.\n')
            sys.exit(-1)
        ### Get Start Value
        ch.GetEntry(0)
        s_run = e_run = ch.runnum % 10000
        s_time = e_time = ch.time
        ### Get Min or Max Value
        for i in range(nentries):
            ch.GetEntry(i)
            t_run = ch.runnum % 10000
            s_run = min(s_run, t_run)
            e_run = max(e_run, t_run)
            t_time = ch.time
            s_time = min(s_time, t_time)
            e_time = max(e_time, t_time)

        ### initialize histograms ###
        n_buff = 2
        n_run = e_run - s_run
        run = (s_run, e_run, n_run)
        ### qdc0, qdc0rms
        hname = '{0}.hRunQdc%d'.format(name)
        htitle = 'hRunQdc%d;Run [#];Qdc [#]'
        hRunQdc = self.init_th2d(hname, htitle, run, Config.atm_qdc, n_buff)
        hname = '{0}.hRunQdc%dRms'.format(name)
        htitle = 'hRunQdc%dRms;Run [#];QdcRms [#]'
        hRunQdcRms = self.init_th2d(hname, htitle, run, Config.atm_rms, n_buff)
        ### tdc0, tdc0rms
        hname = '{0}.hRunTdc%d'.format(name)
        htitle = 'hRunTdc%d;Run [#];Tdc [#]'
        hRunTdc = self.init_th2d(hname, htitle, run, Config.atm_tdc, n_buff)
        hname = '{0}.hRunTdc%dRms'.format(name)
        htitle = 'hRunTdc%dRms;Run [#];TdcRms [#]'
        hRunTdcRms = self.init_th2d(hname, htitle, run, Config.atm_rms, n_buff)
        ### atm_pe, atm_pe_rms
        hname = '{0}.hRunPe%d'.format(name)
        htitle = 'hRunPe%d;Run [#];Pe [p.e.]'
        hRunPe = self.init_th2d(hname, htitle, run, Config.atm_pe, n_buff)
        hname = '{0}.hRunPe%dRms'.format(name)
        htitle = 'hRunPe%dRms;Run [#];PeRms [p.e.]'
        hRunPeRms = self.init_th2d(hname, htitle, run, Config.atm_pe, n_buff)
        ### atm_charge, atm_charge_rms
        hname = '{0}.hRunCharge%d'.format(name)
        htitle = 'hRunCharge%d;Run [#];Charge [pC]'
        hRunCharge = self.init_th2d(hname, htitle, run, Config.atm_pe, n_buff)
        hname = '{0}.hRunCharge%dRms'.format(name)
        htitle = 'hRunCharge%dRms;Run [#];ChargeRms [pC]'
        hRunChargeRms = self.init_th2d(hname, htitle, run, Config.atm_pe, n_buff)
        ### atm_time, atm_time_rms
        hname = '{0}.hRunTime%d'.format(name)
        htitle = 'hRunTime%d;Run [#];Time [pC]'
        hRunTime = self.init_th2d(hname, htitle, run, Config.atm_time, n_buff)
        hname = '{0}.hRunTime%dRms'.format(name)
        htitle = 'hRunTime%dRms;Run [#];TimeRms [pC]'
        hRunTimeRms = self.init_th2d(hname, htitle, run, Config.atm_time, n_buff)

        bin_time = Config.bin_time
        s_time = (s_time / bin_time) * bin_time
        e_time = (e_time / bin_time + 1) * bin_time
        n_time = (e_time - s_time) / bin_time
        utime = (s_time, e_time, n_time)
        ### qdc0, qdc0rms
        hname = '{0}.hTimeQdc%d'.format(name)
        htitle = 'hTimeQdc%d;Date/Time;Qdc [#]'
        hTimeQdc = self.init_th2d(hname, htitle, utime, Config.atm_qdc, n_buff, timedisplay=1)
        ymin, ymax, ybin = Config.atm_rms
        hname = '{0}.hTimeQdc%dRms'.format(name)
        htitle = 'hTimeQdc%dRms;Date/Time;QdcRms [#]'
        hTimeQdcRms = self.init_th2d(hname, htitle, utime, Config.atm_rms, n_buff, timedisplay=1)
        ### tdc0, tdc0rms
        hname = '{0}.hTimeTdc%d'.format(name)
        htitle = 'hTimeTdc%d;Date/Time;Tdc [#]'
        hTimeTdc = self.init_th2d(hname, htitle, utime, Config.atm_tdc, n_buff, timedisplay=1)
        ymin, ymax, ybin = Config.atm_rms
        hname = '{0}.hTimeTdc%dRms'.format(name)
        htitle = 'hTimeTdc%dRms;Date/Time;TdcRms [#]'
        hTimeTdcRms = self.init_th2d(hname, htitle, utime, Config.atm_rms, n_buff, timedisplay=1)
        ### atm_pe, atm_pe_rms
        hname = '{0}.hTimePe%d'.format(name)
        htitle = 'hTimePe%d;Date/Time;Pe [p.e.]'
        hTimePe = self.init_th2d(hname, htitle, utime, Config.atm_pe, n_buff, timedisplay=1)
        hname = '{0}.hTimePe%dRms'.format(name)
        htitle = 'hTimePe%dRms;Date/Time;PeRms [p.e.]'
        hTimePeRms = self.init_th2d(hname, htitle, utime, Config.atm_pe, n_buff, timedisplay=1)
        ### atm_charge, atm_charge_rms
        hname = '{0}.hTimeCharge%d'.format(name)
        htitle = 'hTimeCharge%d;Date/Time;Charge [pC]'
        hTimeCharge = self.init_th2d(hname, htitle, utime, Config.atm_charge, n_buff, timedisplay=1)
        hname = '{0}.hTimeCharge%dRms'.format(name)
        htitle = 'hTimeCharge%dRms;Date/Time;ChargeRms [pC]'
        hTimeChargeRms = self.init_th2d(hname, htitle, utime, Config.atm_charge, n_buff, timedisplay=1)
        # ### atm_time, atm_time_rms
        hname = '{0}.hTimeTime%d'.format(name)
        htitle = 'hTimeTime%d;Date/Time;Time [pC]'
        hTimeTime = self.init_th2d(hname, htitle, utime, Config.atm_time, n_buff, timedisplay=1)
        hname = '{0}.hTimeTime%dRms'.format(name)
        htitle = 'hTimeTime%dRms;Date/Time;TimeRms [pC]'
        hTimeTimeRms = self.init_th2d(hname, htitle, utime, Config.atm_time, n_buff, timedisplay=1)

        gNhits = [TGraphErrors(0) for i in range(2)]
        gNcharges = [TGraphErrors(0) for i in range(2)]
        gNpes = [TGraphErrors(0) for i in range(2)]
        gMaxCharge = TGraphErrors(0)
        gMaxPe = TGraphErrors(0)

        qcorr, gcorr = Config.fcorr

        for i in range(n_buff):
            ch.Project(hRunQdc[i].GetName(), 'qdc0[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunQdcRms[i].GetName(), 'qdc0rms[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunTdc[i].GetName(), 'tdc0[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunTdcRms[i].GetName(), 'tdc0rms[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunPe[i].GetName(), 'atm_pe[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunPeRms[i].GetName(), 'atm_pe_rms[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunCharge[i].GetName(), 'atm_charge[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunChargeRms[i].GetName(), 'atm_charge_rms[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunTime[i].GetName(), 'atm_time[][{0}]:runnum%10000'.format(i))
            ch.Project(hRunTimeRms[i].GetName(), 'atm_time_rms[][{0}]:runnum%10000'.format(i))
            ###
            ch.Project(hTimeQdc[i].GetName(), 'qdc0[][{0}]:time'.format(i))
            ch.Project(hTimeQdcRms[i].GetName(), 'qdc0rms[][{0}]:time'.format(i))
            ch.Project(hTimeTdc[i].GetName(), 'tdc0[][{0}]:time'.format(i))
            ch.Project(hTimeTdcRms[i].GetName(), 'tdc0rms[][{0}]:time'.format(i))
            ch.Project(hTimePe[i].GetName(), 'atm_pe[][{0}]:time'.format(i))
            ch.Project(hTimePeRms[i].GetName(), 'atm_pe_rms[][{0}]:time'.format(i))
            ch.Project(hTimeCharge[i].GetName(), 'atm_charge[][{0}]:time'.format(i))
            ch.Project(hTimeChargeRms[i].GetName(), 'atm_charge_rms[][{0}]:time'.format(i))
            ch.Project(hTimeTime[i].GetName(), 'atm_time[][{0}]:time'.format(i))
            ch.Project(hTimeTimeRms[i].GetName(), 'atm_time_rms[][{0}]:time'.format(i))

        for ientry in range(nentries):
            ch.GetEntry(ientry)
            time = ch.time
            #####
            a_nhits = ch.atm_nhits
            a_nhits_r = ch.atm_nhits_rms
            f_nhits = ch.fadc_nhits
            f_nhits_r = ch.fadc_nhits_rms
            n_a_nhits = gNhits[0].GetN()
            ###
            gNhits[0].SetPoint(n_a_nhits, time, a_nhits)
            gNhits[0].SetPointError(n_a_nhits, 0, a_nhits_r)
            n_f_nhits = gNhits[1].GetN()
            gNhits[1].SetPoint(n_f_nhits, time, f_nhits)
            gNhits[1].SetPointError(n_f_nhits, 0, f_nhits_r)
            #####
            a_ncharges = ch.atm_ncharges
            a_ncharges_r = ch.atm_ncharges_rms
            f_ncharges = ch.fadc_ncharges / qcorr
            f_ncharges_r = ch.fadc_ncharges_rms / qcorr
            ###
            n_a_ncharges = gNcharges[0].GetN()
            gNcharges[0].SetPoint(n_a_ncharges, time, a_ncharges)
            gNcharges[0].SetPointError(n_a_ncharges, 0, a_ncharges_r)
            n_f_ncharges = gNcharges[1].GetN()
            gNcharges[1].SetPoint(n_f_ncharges, time, f_ncharges)
            gNcharges[1].SetPointError(n_f_ncharges, 0, f_ncharges_r)
            #####
            a_npes = ch.atm_npes
            a_npes_r = ch.atm_npes_rms
            f_npes = ch.fadc_npes / qcorr / gcorr
            f_npes_r = ch.fadc_npes_rms / qcorr / gcorr
            ###
            n_a_npes = gNpes[0].GetN()
            gNpes[0].SetPoint(n_a_npes, time, a_npes)
            gNpes[0].SetPointError(n_a_npes, 0, a_npes_r)
            n_f_npes = gNpes[1].GetN()
            gNpes[1].SetPoint(n_f_npes, time, f_npes)
            gNpes[1].SetPointError(n_f_npes, 0, f_npes_r)
            #####
            a_max_charge = ch.atm_max_charge
            a_max_charge_r = ch.atm_max_charge_rms
            a_max_pe = ch.atm_max_pe
            a_max_pe_r = ch.atm_max_pe_rms
            ###
            n_a_max_q = gMaxCharge.GetN()
            gMaxCharge.SetPoint(n_a_max_q, time, a_max_charge)
            gMaxCharge.SetPointError(n_a_max_q, 0, a_max_charge_r)
            n_a_max_pe = gMaxPe.GetN()
            gMaxPe.SetPoint(n_a_max_pe, time, a_max_pe)
            gMaxPe.SetPointError(n_a_max_pe, 0, a_max_pe_r)

        for i in range(len(gNhits)):
            color = i % 8 + 1
            gNhits[i].SetLineColor(color)
            gNhits[i].SetMarkerColor(color)
            gNhits[i].GetXaxis().SetTimeDisplay(1)
            gNhits[i].GetXaxis().SetTimeFormat(Config.timeformat)
            gNhits[i].GetXaxis().SetTimeOffset(0, 'gmt')
            gNhits[i].GetXaxis().SetTitle('Date/Time')
            gNhits[i].GetYaxis().SetTitle('Nhits [#]')

        for i in range(len(gNcharges)):
            color = i % 8 + 1
            gNcharges[i].SetLineColor(color)
            gNcharges[i].SetMarkerColor(color)
            gNcharges[i].GetXaxis().SetTimeDisplay(1)
            gNcharges[i].GetXaxis().SetTimeFormat(Config.timeformat)
            gNcharges[i].GetXaxis().SetTimeOffset(0, 'gmt')
            gNcharges[i].GetXaxis().SetTitle('Date/Time')
            gNcharges[i].GetYaxis().SetTitle('Ncharges [pC]')

        for i in range(len(gNpes)):
            color = i % 8 + 1
            gNpes[i].SetLineColor(color)
            gNpes[i].SetMarkerColor(color)
            gNpes[i].GetXaxis().SetTimeDisplay(1)
            gNpes[i].GetXaxis().SetTimeFormat(Config.timeformat)
            gNpes[i].GetXaxis().SetTimeOffset(0, 'gmt')
            gNpes[i].GetXaxis().SetTitle('Date/Time')
            gNpes[i].GetYaxis().SetTitle('Npes [p.e.]')

        gMaxCharge.GetXaxis().SetTimeDisplay(1)
        gMaxCharge.GetXaxis().SetTimeFormat(Config.timeformat)
        gMaxCharge.GetXaxis().SetTimeOffset(0, 'gmt')
        gMaxCharge.GetXaxis().SetTitle('Date/Time')
        gMaxCharge.GetYaxis().SetTitle('MaxCharge [pC]')

        gMaxPe.GetXaxis().SetTimeDisplay(1)
        gMaxPe.GetXaxis().SetTimeFormat(Config.timeformat)
        gMaxPe.GetXaxis().SetTimeOffset(0, 'gmt')
        gMaxPe.GetXaxis().SetTitle('Date/Time')
        gMaxPe.GetYaxis().SetTitle('MaxPe [p.e.]')

        self.gNhits = gNhits
        self.gNcharges = gNcharges
        self.gNpes = gNpes
        self.gMaxCharge = gMaxCharge
        self.gMaxPe = gMaxPe

        self.hRunQdc = hRunQdc
        self.hRunQdcRms = hRunQdcRms
        self.hRunTdc = hRunTdc
        self.hRunTdcRms = hRunTdcRms
        self.hRunPe = hRunPe
        self.hRunPeRms = hRunPeRms
        self.hRunCharge = hRunCharge
        self.hRunChargeRms = hRunChargeRms
        self.hRunTime = hRunTime
        self.hRunTimeRms = hRunTimeRms
        self.hTimeQdc = hTimeQdc
        self.hTimeQdcRms = hTimeQdcRms
        self.hTimeTdc = hTimeTdc
        self.hTimeTdcRms = hTimeTdcRms
        self.hTimePe = hTimePe
        self.hTimePeRms = hTimePeRms
        self.hTimeCharge = hTimeCharge
        self.hTimeChargeRms = hTimeChargeRms
        self.hTimeTime = hTimeTime
        self.hTimeTimeRms = hTimeTimeRms
        self.logger.info('finished')
        return

    ## __________________________________________________
    def process_bsd(self):
        self.logger.info('started')
        name = self.get_object_name()
        ch = self.chain
        nentries = ch.GetEntries()
        if nentries == 0:
            self.logger.error('No entry. Exit.')
            sys.exit(-1)
        ### Get Start Value
        ch.GetEntry(0)
        s_run = e_run = ch.nurun
        s_time = e_time = ch.trig_sec[0]
        s_spill = e_spill = ch.spillnum
        ### Get Min of Max value
        for i in range(nentries):
            if i % 100000 == 0:
                percent = float(i)/float(nentries) * 100
                sys.stderr.write('[process_bsd]\t... processed {0:2.2f}%\n'.format(percent))
            ch.GetEntry(i)
            t_run = ch.nurun
            t_time = ch.trig_sec[0]
            t_spill = ch.spillnum
            s_run, e_run = min(s_run, t_run), max(e_run, t_run)
            s_time, e_time = min(s_time, t_time), max(e_time, t_time)
            s_spill, e_spill = min(s_spill, t_spill), max(e_spill, t_spill)

        ### initialize histograms ###
        n_run = e_run - s_run
        run = (s_run, e_run, n_run)

        bin_time = Config.bin_time
        s_time = (s_time / bin_time) * bin_time
        e_time = (e_time / bin_time + 1) * bin_time
        n_time = (e_time - s_time) / bin_time
        utime = (s_time, e_time, n_time)

        n_spill = e_spill - s_spill
        spill = (s_spill, e_spill, n_spill)

        ###
        hname = '{0}.hTimeRun%d'.format(name)
        htitle = 'hTimeRun%d;Time/Date;Run[#]'
        hTimeRun = self.init_th2d(hname, htitle, utime, run, num=1, timedisplay=1)

        hname = '{0}.hTimeSpill%d'.format(name)
        htitle = 'hTimeRun%d;Time/Date;Spill [#]'
        hTimeSpill = self.init_th2d(hname, htitle, utime, spill, num=1, timedisplay=1)

        #hname, htitle = '{0}.hTimePot%d'.format(name), 'hTimePot;Time/Date;POT'
        #hTimePot      = self.init_th2d(hname, htitle, utime, pot

        ch.Project(hTimeRun[0].GetName(), 'nurun%10000:trig_sec[0]')
        ch.Project(hTimeSpill[0].GetName(), 'spillnum:trig_sec[0]')

        self.hTimeRun = hTimeRun
        self.hTimeSpill = hTimeSpill

        return

    ## __________________________________________________
    def draw_pedestal(self):
        hTimeQdc0 = self.hTimeQdc0
        hTimeQdc1 = self.hTimeQdc1
        hTimeTdc0 = self.hTimeTdc0
        hTimeTdc1 = self.hTimeTdc1
        hTimeRawPmtsum0 = self.hTimeRawPmtsum0
        hTimeRawHitsum0 = self.hTimeRawHitsum0
        hTimeFadcPmtsum0 = self.hTimeFadcPmtsum0
        hTimeFadcHitsum0 = self.hTimeFadcHitsum0

        lQdc0 = TLegend(0.5, 0.5, 0.9, 0.9, 'QDC0')
        lQdc1 = TLegend(0.5, 0.5, 0.9, 0.9, 'QDC1')
        lTdc0 = TLegend(0.5, 0.5, 0.9, 0.9, 'TDC0')
        lTdc1 = TLegend(0.5, 0.5, 0.9, 0.9, 'TDC1')
        lHsum0 = TLegend(0.5, 0.7, 0.9, 0.9, 'RawHitsum0')
        lPsum0 = TLegend(0.5, 0.7, 0.9, 0.9, 'RawPmtsum0')
        legs = [lQdc0, lQdc1, lTdc0, lTdc1, lHsum0, lPsum0]
        for l in legs:
            l.SetFillStyle(0)
            
        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Pedestal'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(2, 3)
        c.cd(1)
        for i in range(len(hTimeQdc0)):
            t = hTimeQdc0[i].GetTitle()
            m = hTimeQdc0[i].GetMean(2)
            r = hTimeQdc0[i].GetRMS(2)
            p = r / m
            label = '{0} : {1:.2f} #pm {2:.2f} ({3:.2f} %)'.format(t, m, r, p * 100.)
            lQdc0.AddEntry(hTimeQdc0[i], label, 'lpf')
            gopt = 'box' if i == 0 else 'boxsame'
            hTimeQdc0[i].Draw(gopt)
        lQdc0.Draw()
        c.cd(2)
        for i in range(len(hTimeQdc1)):
            t = hTimeQdc1[i].GetTitle()
            m = hTimeQdc1[i].GetMean(2)
            r = hTimeQdc1[i].GetRMS(2)
            p = r / m
            label = '{0} : {1:.2f} #pm {2:.2f} ({3:.2f} %)'.format(t, m, r, p * 100.)
            lQdc1.AddEntry(hTimeQdc1[i], label, 'lpf')
            gopt = 'box' if i == 0 else 'boxsame'
            hTimeQdc1[i].Draw(gopt)
        lQdc1.Draw()
        c.cd(3)
        for i in range(len(hTimeTdc0)):
            t = hTimeTdc0[i].GetTitle()
            m = hTimeTdc0[i].GetMean(2)
            r = hTimeTdc0[i].GetRMS(2)
            p = r / m
            label = '{0} : {1:.2f} #pm {2:.2f} ({3:.2f} %)'.format(t, m, r, p * 100.)
            lTdc0.AddEntry(hTimeTdc0[i], label, 'lpf')
            gopt = 'box' if i == 0 else 'boxsame'
            hTimeTdc0[i].Draw(gopt)
        lTdc0.Draw()
        c.cd(4)
        for i in range(len(hTimeTdc1)):
            t = hTimeTdc1[i].GetTitle()
            m = hTimeTdc1[i].GetMean(2)
            r = hTimeTdc1[i].GetRMS(2)
            p = r / m
            label = '{0} : {1:.2f} #pm {2:.2f} ({3:.2f} %)'.format(t, m, r, p * 100.)
            lTdc1.AddEntry(hTimeTdc1[i], label, 'lpf')            
            gopt = 'box' if i == 0 else 'boxsame'
            hTimeTdc1[i].Draw(gopt)
        lTdc1.Draw()
        c.cd(5)
        for i in range(len(hTimeRawPmtsum0)):
            t = hTimeRawPmtsum0[i].GetTitle()
            m = hTimeRawPmtsum0[i].GetMean(2)
            r = hTimeRawPmtsum0[i].GetRMS(2)
            p = r / m
            label = '{0} : {1:.2f} #pm {2:.2f} ({3:.2f} %)'.format(t, m, r, p * 100.)
            lPsum0.AddEntry(hTimeRawPmtsum0[i], label, 'lpf')       
            gopt = 'box' if i == 0 else 'boxsame'
            hTimeRawPmtsum0[i].Draw(gopt)
        lPsum0.Draw()
        c.cd(6)
        for i in range(len(hTimeRawHitsum0)):
            t = hTimeRawHitsum0[i].GetTitle()
            m = hTimeRawHitsum0[i].GetMean(2)
            r = hTimeRawHitsum0[i].GetRMS(2)
            p = r / m
            label = '{0} : {1:.2f} #pm {2:.2f} ({3:.2f} %)'.format(t, m, r, p * 100.)
            lHsum0.AddEntry(hTimeRawHitsum0[i], label, 'lpf')            
            gopt = 'box' if i == 0 else 'boxsame'
            hTimeRawHitsum0[i].Draw(gopt)
        lHsum0.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_run(self):
        h = self.hRun
        hHit = self.hRunRate[3:6]
        hPe = self.hRunRate[6:]

        tname = self.get_tree_name()
        if tname == 'dst':
            pot = self.hRunPot[0].Integral()
        else:
            pot = 0

        lH = TLegend(0.7, 0.1, 0.9, 0.9, 'RunRate (ALL) : %.2e P.O.T' % pot)
        lHit = TLegend(0.7, 0.1, 0.9, 0.9, 'RunRate (NhitsCut)')
        lPe = TLegend(0.7, 0.1, 0.9, 0.9, 'RunRate (NpesCut)')
        legs = [lH, lHit, lPe]
        for l in legs:
            # l.SetFillColor(0)
            l.SetFillStyle(0)
            # l.SetBorderSize(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_HitRateRun'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 3)
        c.cd(1)  # .SetLogy()
        for i in range(len(h)):
            h[i].SetFillColor(0)
            try:
                label = '{0} : {1:10d} : ({2:8.3f}%)'.format(h[i].GetTitle(), int(h[i].GetEntries()), h[i].GetEntries()/h[0].GetEntries() * 100.)
            except ZeroDivisionError:
                label = '{0} : {1:10d}'.format(h[i].GetTitle(), int(h[i].GetEntries()))
            lH.AddEntry(h[i], label, 'lpf')
            h[i].Draw() if i == 1 else h[i].Draw('same')
        lH.Draw()
        c.cd(2)
        for i in range(len(hHit)):
            title = hHit[i].GetTitle()
            integral = hHit[i].Integral()
            nbinsx = hHit[i].GetNbinsX() + 1
            ratio = integral / nbinsx
            label = '{0} : {1:10.3f}'.format(title, ratio)
            lHit.AddEntry(hHit[i], label, 'lpf')
            hHit[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hHit[i].Draw() if i == 0 else hHit[i].Draw('same')
        lHit.Draw()
        c.cd(3)
        for i in range(len(hPe)):
            title = hPe[i].GetTitle()
            integral = hPe[i].Integral()
            nbinsx = hPe[i].GetNbinsX() + 1
            ratio = integral / nbinsx
            label = '{0} : {1:10.3f}'.format(title, ratio)
            lPe.AddEntry(hPe[i], label, 'lpf')
            hPe[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hPe[i].Draw() if i == 0 else hPe[i].Draw('same')
        lPe.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_time(self):
        h = self.hTime
        hHit = self.hTimeRate[3:6]
        hPe = self.hTimeRate[6:]

        tname = self.get_tree_name()
        if tname == 'dst':
            pot = self.hTimePot[0].Integral()
        else:
            pot = 0

        lH = TLegend(0.7, 0.1, 0.9, 0.9, 'TimeRate (ALL) : %.2e P.O.T' % pot)
        lHit = TLegend(0.7, 0.1, 0.9, 0.9, 'TimeRate (NhitsCut)')
        lPe = TLegend(0.7, 0.1, 0.9, 0.9, 'TimeRate (NpesCut)')
        legs = [lH, lHit, lPe]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_HitRateTime'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 3)
        c.cd(1)
        for i in range(len(h)):
            h[i].SetFillColor(0)
            try:
                label = '{0} : {1:10d} : ({2:8.3f}%)'.format(h[i].GetTitle(), int(h[i].GetEntries()), h[i].GetEntries()/h[0].GetEntries() * 100.)
            except ZeroDivisionError:
                label = '{0} : {1:10d}'.format(h[i].GetTitle(), int(h[i].GetEntries()))
            lH.AddEntry(h[i], label, 'lpf')
            h[i].Draw() if i == 1 else h[i].Draw('same')
        lH.Draw()
        c.cd(2)
        for i in range(len(hHit)):
            label = '{0} : {1:10.3f}'.format(hHit[i].GetTitle(), hHit[i].Integral() / (hHit[i].GetNbinsX()+1))
            lHit.AddEntry(hHit[i], label, 'lpf')
            hHit[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hHit[i].Draw() if i == 0 else hHit[i].Draw('same')
        lHit.Draw()
        c.cd(3)
        for i in range(len(hPe)):
            label = '{0} : {1:10.3f}'.format(hPe[i].GetTitle(), hPe[i].Integral() / (hPe[i].GetNbinsX()+1))
            lPe.AddEntry(hPe[i], label, 'lpf')
            hPe[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hPe[i].Draw() if i == 0 else hPe[i].Draw('same')
        lPe.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_upkrun(self):
        h = self.hUpkRun
        hHit = self.hUpkRunRate[3:6]
        hPe = self.hUpkRunRate[6:]

        tname = self.get_tree_name()
        if tname == 'dst':
            pot = self.hUpkRunPot[0].Integral()
        else:
            pot = 0

        lH = TLegend(0.7, 0.1, 0.9, 0.9, 'UpkRunRate (ALL) : %.2e P.O.T' % pot)
        lHit = TLegend(0.7, 0.1, 0.9, 0.9, 'UpkRunRate (NhitsCut)')
        lPe = TLegend(0.7, 0.1, 0.9, 0.9, 'UpkRunRate (NpesCut)')
        legs = [lH, lHit, lPe]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_HitRateUpkRun'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 3)
        c.cd(1)
        for i in range(len(h)):
            h[i].SetFillColor(0)
            try:
                label = '{0} : {1:10d} : ({2:8.3f}%)'.format(h[i].GetTitle(), int(h[i].GetEntries()), h[i].GetEntries()/h[0].GetEntries() * 100.)
            except ZeroDivisionError:
                label = '{0} : {1:10d}'.format(h[i].GetTitle(), int(h[i].GetEntries()))
            lH.AddEntry(h[i], label, 'lpf')
            h[i].Draw() if i == 1 else h[i].Draw('same')
        lH.Draw()
        self.write_summary(lH)
        c.cd(2)
        for i in range(len(hHit)):
            label = '{0} : {1:10.3f}'.format(hHit[i].GetTitle(), hHit[i].Integral() / (hHit[i].GetNbinsX()+1))
            lHit.AddEntry(hHit[i], label, 'lpf')
            hHit[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hHit[i].Draw() if i == 0 else hHit[i].Draw('same')
        lHit.Draw()
        c.cd(3)
        for i in range(len(hPe)):
            label = '{0} : {1:10.3f}'.format(hPe[i].GetTitle(), hPe[i].Integral() / (hPe[i].GetNbinsX()+1))
            lPe.AddEntry(hPe[i], label, 'lpf')
            hPe[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hPe[i].Draw() if i == 0 else hPe[i].Draw('same')
        lPe.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_mrrun(self):
        h = self.hMrRun
        hHit = self.hMrRunRate[3:6]
        hPe = self.hMrRunRate[6:]

        tname = self.get_tree_name()
        if tname == 'dst':
            pot = self.hMrRunPot[0].Integral()
        else:
            pot = 0

        lH = TLegend(0.7, 0.1, 0.9, 0.9, 'mrRunRate (ALL) : %.2e P.O.T' % pot)
        lHit = TLegend(0.7, 0.1, 0.9, 0.9, 'MrRunRate (NhitsCut)')
        lPe = TLegend(0.7, 0.1, 0.9, 0.9, 'MrRunRate (NpesCut)')
        legs = [lH, lHit, lPe]
        for l in legs:
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_HitRateMrRun'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 3)
        c.cd(1)
        for i in range(len(h)):
            title = h[i].GetTitle()
            entries = h[i].GetEntries()
            e0 = h[0].GetEntries()
            ratio = entries / e0 * 100 if not e0 == 0 else 0
            label = '{0} : {1:10d} : ({2:8.3f}%)'.format(title, int(entries), ratio)
            lH.AddEntry(h[i], label, 'lpf')
            h[i].Draw() if i == 1 else h[i].Draw('same')
        lH.Draw()
        c.cd(2)
        for i in range(len(hHit)):
            label = '{0} : {1:10.3f}'.format(hHit[i].GetTitle(), hHit[i].Integral() / (hHit[i].GetNbinsX()+1))
            lHit.AddEntry(hHit[i], label, 'lpf')
            hHit[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hHit[i].Draw() if i == 0 else hHit[i].Draw('same')
        lHit.Draw()
        c.cd(3)
        for i in range(len(hPe)):
            label = '{0} : {1:10.3f}'.format(hPe[i].GetTitle(), hPe[i].Integral() / (hPe[i].GetNbinsX()+1))
            lPe.AddEntry(hPe[i], label, 'lpf')
            hPe[i].GetYaxis().SetRangeUser(Config.range_hitrate[0], Config.range_hitrate[1])
            hPe[i].Draw() if i == 0 else hPe[i].Draw('same')
        lPe.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_pot(self):
        hRun = self.hRunPot
        hTime = self.hTimePot
        hMrRun = self.hMrRunPot
        hUpkRun = self.hUpkRunPot

        lRun = TLegend(0.7, 0.7, 0.9, 0.9, 'POT / Run')
        lTime = TLegend(0.7, 0.7, 0.9, 0.9, 'POT / Time')
        lMrRun = TLegend(0.7, 0.7, 0.9, 0.9, 'POT / MR')
        lUpkRun = TLegend(0.7, 0.7, 0.9, 0.9, 'POT / UpkRun')
        legs = [lRun, lTime, lMrRun, lUpkRun]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Pot'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 4)
        
        c.cd(1)
        for i in range(len(hRun)):
            title = hRun[i].GetTitle()
            integral = hRun[i].Integral()
            label = '{0} : {1:.5e}'.format(title, integral)
            lRun.AddEntry(hRun[i], label, 'lpf')
            hRun[i].Draw() if i == 0 else hRun[i].Draw('same')
        lRun.Draw()
        
        c.cd(2)
        for i in range(len(hTime)):
            title = hTime[i].GetTitle()
            integral = hTime[i].Integral()
            label = '{0} : {1:.5e}'.format(title, integral)
            lTime.AddEntry(hTime[i], label, 'lpf')
            hTime[i].Draw() if i == 0 else hTime[i].Draw('same')
        lTime.Draw()
        
        c.cd(3)
        for i in range(len(hMrRun)):
            title = hMrRun[i].GetTitle()
            integral = hMrRun[i].Integral()
            label = '{0} : {1:.5e}'.format(title, integral)
            lMrRun.AddEntry(hMrRun[i], label, 'lpf')
            hMrRun[i].Draw() if i == 0 else hMrRun[i].Draw('same')
        lMrRun.Draw()
        
        c.cd(4)
        for i in range(len(hUpkRun)):
            title = hUpkRun[i].GetTitle()
            integral = hUpkRun[i].Integral()
            label = '{0} : {1:.5e}'.format(title, integral)
            lUpkRun.AddEntry(hUpkRun[i], label, 'lpf')
            hUpkRun[i].Draw() if i == 0 else hUpkRun[i].Draw('same')
        lUpkRun.Draw()
        
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_nsec(self, log=0):
        hNsecHit = self.hNsecFadc[3:6]
        hNsecPe = self.hNsecFadc[6:]

        lHit = TLegend(0.7, 0.2, 0.9, 0.9, "NsecFadc (NhitsCut)")
        lPe = TLegend(0.7, 0.2, 0.9, 0.9, "NsecFadc (NpesCut)")
        legs = [lHit, lPe]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_NsecFadc'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 2)
        c.cd(1).SetLogy(log)
        for i in range(len(hNsecHit)):
            label = '{0} : {1:10d}'.format(hNsecHit[i].GetTitle(), int(hNsecHit[i].GetEntries()))
            lHit.AddEntry(hNsecHit[i], label, "lpf")
            hNsecHit[i].GetXaxis().SetRangeUser(Config.range_nsec[0], Config.range_nsec[1])
            hNsecHit[i].Draw() if i == 0 else hNsecHit[i].Draw('same')
        lHit.Draw()
        c.cd(2).SetLogy(log)
        for i in range(len(hNsecPe)):
            label = '{0} : {1:10d}'.format(hNsecPe[i].GetTitle(), int(hNsecPe[i].GetEntries()))
            lPe.AddEntry(hNsecPe[i], label, "lpf")
            hNsecPe[i].GetXaxis().SetRangeUser(Config.range_nsec[0], Config.range_nsec[1])
            hNsecPe[i].Draw() if i == 0 else hNsecPe[i].Draw('same')
        lPe.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_window(self, log=0):
        hHit = self.hWindow[3:6]
        hPe = self.hWindow[6:]

        lHit = TLegend(0.7, 0.1, 0.9, 0.9, 'MapWindow (NhitsCut)')
        lPe = TLegend(0.7, 0.1, 0.9, 0.9, 'MapWindow (NpesCut)')
        legs = [lHit, lPe]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Window'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 2)
        c.cd(1).SetLogy(log)
        for i in range(len(hHit)):
            label = '{0} : {1:10d}'.format(hHit[i].GetTitle(), int(hHit[i].GetEntries()))
            lHit.AddEntry(hHit[i], label, 'lpf')
            hHit[i].GetYaxis().SetRangeUser(Config.range_window[0], Config.range_window[1])
            hHit[i].Draw() if i == 0 else hHit[i].Draw('same')
        lHit.Draw()
        c.cd(2).SetLogy(log)
        for i in range(len(hPe)):
            label = '{0} : {1:10d}'.format(hPe[i].GetTitle(), int(hPe[i].GetEntries()))
            lPe.AddEntry(hPe[i], label, 'lpf')
            hPe[i].GetYaxis().SetRangeUser(Config.range_window[0], Config.range_window[1])
            hPe[i].Draw() if i == 0 else hPe[i].Draw('same')
        lPe.Draw()
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_ntrg(self):
        hAtm = self.hNtrgAtm[:3]
        hFadc = self.hNtrgFadc[:3]
        hAtmFadc = self.hNtrgAtmFadc[:3]
        hHitAtm = self.hNtrgAtm[3:6]
        hHitFadc = self.hNtrgFadc[3:6]
        hHitAtmFadc = self.hNtrgAtmFadc[3:6]
        hPeAtm = self.hNtrgAtm[6:]
        hPeFadc = self.hNtrgFadc[6:]
        hPeAtmFadc = self.hNtrgAtmFadc[6:]

        lHitAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NtrgAtm (NhitsCut)')
        lHitFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NtrgFadc (NhitsCut)')
        lPeAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NtrgAtm (NpesCut)')
        lPeFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NtrgFadc (NpesCut)')
        legs = [lHitAtm, lHitFadc, lPeAtm, lPeFadc]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Ntrg'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 2)
        c.cd(1)
        for i in range(len(hHitAtm)):
            label = '{0} : {1:10d}'.format(hHitAtm[i].GetTitle(), int(hHitAtm[i].GetEntries()))
            lHitAtm.AddEntry(hHitAtm[i], label, 'lpf')
            gopt = '' if i == 0 else 'same'
            hHitAtm[i].Draw(gopt)
        lHitAtm.Draw()
        c.cd(2)
        for i in range(len(hHitFadc)):
            label = '{0} : {1:10d}'.format(hHitFadc[i].GetTitle(), int(hHitFadc[i].GetEntries()))
            lHitFadc.AddEntry(hHitFadc[i], label, 'lpf')
            gopt = '' if i == 0 else 'same'
            hHitFadc[i].Draw(gopt)
        lHitFadc.Draw()
        c.cd(3)
        # hAtmFadc[1].Draw('box')
        for i in range(len(hHitAtmFadc)):
            gopt = 'box' if i == 0 else 'boxsame'
            hHitAtmFadc[i].Draw(gopt)
        c.cd(4)
        for i in range(len(hPeAtm)):
            label = '{0} : {1:10d}'.format(hPeAtm[i].GetTitle(), int(hPeAtm[i].GetEntries()))
            lPeAtm.AddEntry(hPeAtm[i], label, 'lpf')
            hPeAtm[i].Draw() if i == 0 else hPeAtm[i].Draw('same')
        lPeAtm.Draw()
        c.cd(5)
        for i in range(len(hPeFadc)):
            label = '{0} : {1:10d}'.format(hPeFadc[i].GetTitle(), int(hPeFadc[i].GetEntries()))
            lPeFadc.AddEntry(hPeFadc[i], label, 'lpf')
            hPeFadc[i].Draw() if i == 0 else hPeFadc[i].Draw('same')
        lPeFadc.Draw()
        c.cd(6)
        for i in range(len(hPeAtmFadc)):
            hPeAtmFadc[i].Draw('box') if i == 0 else hPeAtmFadc[i].Draw('boxsame')
            c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_nhits(self, log=0):
        hHitAtm = self.hNhitsAtm[3:6]
        hHitFadc = self.hNhitsFadc[3:6]
        hHitAtmFadc = self.hNhitsAtmFadc[3:6]
        hPeAtm = self.hNhitsAtm[6:]
        hPeFadc = self.hNhitsFadc[6:]
        hPeAtmFadc = self.hNhitsAtmFadc[6:]

        lHitAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NhitsAtm (NhitsCut)')
        lHitFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NhitsFadc (NhitsCut)')
        lPeAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NhitsAtm (NpesCut)')
        lPeFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NhitsFadc (NpesCut)')
        legs = [lHitAtm, lHitFadc, lPeAtm, lPeFadc]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Nhits'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 2)
        c.cd(1).SetLogy(log)
        for i in range(len(hHitAtm)):
            label = '{0} : {1:10d}'.format(hHitAtm[i].GetTitle(), int(hHitAtm[i].GetEntries()))
            lHitAtm.AddEntry(hHitAtm[i], label, 'lpf')
            hHitAtm[i].Draw() if i == 0 else hHitAtm[i].Draw('same')
        lHitAtm.Draw()
        c.cd(2).SetLogy(log)
        for i in range(len(hHitFadc)):
            label = '{0} : {1:10d}'.format(hHitFadc[i].GetTitle(), int(hHitFadc[i].GetEntries()))
            lHitFadc.AddEntry(hHitFadc[i], label, 'lpf')
            hHitFadc[i].Draw() if i == 0 else hHitFadc[i].Draw('same')
        lHitFadc.Draw()
        c.cd(3).SetLogx(log)
        c.cd(3).SetLogy(log)
        for i in range(len(hHitAtmFadc)):
            hHitAtmFadc[i].Draw('box') if i == 0 else hHitAtmFadc[i].Draw('boxsame')
        c.cd(4).SetLogy(log)
        for i in range(len(hPeAtm)):
            label = '{0} : {1:10d}'.format(hPeAtm[i].GetTitle(), int(hPeAtm[i].GetEntries()))
            lPeAtm.AddEntry(hPeAtm[i], label, 'lpf')
            hPeAtm[i].Draw() if i == 0 else hPeAtm[i].Draw('same')
        lPeAtm.Draw()
        c.cd(5).SetLogy(log)
        for i in range(len(hPeFadc)):
            label = '{0} : {1:10d}'.format(hPeFadc[i].GetTitle(), int(hPeFadc[i].GetEntries()))
            lPeFadc.AddEntry(hPeFadc[i], label, 'lpf')
            hPeFadc[i].Draw() if i == 0 else hPeFadc[i].Draw('same')
        lPeFadc.Draw()
        c.cd(6).SetLogx(log)
        c.cd(6).SetLogy(log)
        for i in range(len(hPeAtmFadc)):
            hPeAtmFadc[i].Draw('box') if i == 0 else hPeAtmFadc[i].Draw('boxsame')
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_ncharges(self, log=0):
        hHitAtm = self.hNchargesAtm[3:6]
        hHitFadc = self.hNchargesFadc[3:6]
        hHitAtmFadc = self.hNchargesAtmFadc[3:6]
        hPeAtm = self.hNchargesAtm[6:]
        hPeFadc = self.hNchargesFadc[6:]
        hPeAtmFadc = self.hNchargesAtmFadc[6:]

        lHitAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NchargesAtm (NhitsCut)')
        lHitFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NchargesFadc (NhitsCut)')
        lPeAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NchargesAtm (NpesCut)')
        lPeFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NchargesFadc (NpesCut)')
        legs = [lHitAtm, lHitFadc, lPeAtm, lPeFadc]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Ncharges'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 2)
        c.cd(1).SetLogy(log)
        for i in range(len(hHitAtm)):
            label = '{0} : {1:10d}'.format(hHitAtm[i].GetTitle(), int(hHitAtm[i].GetEntries()))
            lHitAtm.AddEntry(hHitAtm[i], label, 'lpf')
            hHitAtm[i].Draw() if i == 0 else hHitAtm[i].Draw('same')
        lHitAtm.Draw()
        c.cd(2).SetLogy(log)
        for i in range(len(hHitFadc)):
            label = '{0} : {1:10d}'.format(hHitFadc[i].GetTitle(), int(hHitFadc[i].GetEntries()))
            lHitFadc.AddEntry(hHitFadc[i], label, 'lpf')
            hHitFadc[i].Draw() if i == 0 else hHitFadc[i].Draw('same')
        lHitFadc.Draw()
        c.cd(3).SetLogx(log)
        c.cd(3).SetLogy(log)
        for i in range(len(hHitAtmFadc)):
            hHitAtmFadc[i].Draw('box') if i == 0 else hHitAtmFadc[i].Draw('boxsame')
        c.cd(4).SetLogy(log)
        for i in range(len(hPeAtm)):
            label = '{0} : {1:10d}'.format(hPeAtm[i].GetTitle(), int(hPeAtm[i].GetEntries()))
            lPeAtm.AddEntry(hPeAtm[i], label, 'lpf')
            hPeAtm[i].Draw() if i == 0 else hPeAtm[i].Draw('same')
        lPeAtm.Draw()
        c.cd(5).SetLogy(log)
        for i in range(len(hPeFadc)):
            label = '{0} : {1:10d}'.format(hPeFadc[i].GetTitle(), int(hPeFadc[i].GetEntries()))
            lPeFadc.AddEntry(hPeFadc[i], label, 'lpf')
            hPeFadc[i].Draw() if i == 0 else hPeFadc[i].Draw('same')
        lPeFadc.Draw()
        c.cd(6).SetLogx(log)
        c.cd(6).SetLogy(log)
        for i in range(len(hPeAtmFadc)):
            hPeAtmFadc[i].Draw('box') if i == 0 else hPeAtmFadc[i].Draw('boxsame')
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_npes(self, log=0):
        hHitAtm = self.hNpesAtm[3:6]
        hHitFadc = self.hNpesFadc[3:6]
        hHitAtmFadc = self.hNpesAtmFadc[3:6]
        hPeAtm = self.hNpesAtm[6:]
        hPeFadc = self.hNpesFadc[6:]
        hPeAtmFadc = self.hNpesAtmFadc[6:]

        lHitAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NpesAtm (NhitsCut)')
        lHitFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NpesFadc (NhitsCut)')
        lPeAtm = TLegend(0.5, 0.5, 0.9, 0.9, 'NpesAtm (NpesCut)')
        lPeFadc = TLegend(0.5, 0.5, 0.9, 0.9, 'NpesFadc (NpesCut)')
        legs = [lHitAtm, lHitFadc, lPeAtm, lPeFadc]
        for l in legs:
            l.SetFillColor(0)
            l.SetFillStyle(0)

        wx, wy = Config.dispw, Config.disph
        cname = 'c{0}_Npes'.format(Config.prefix)
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(3, 2)
        c.cd(1).SetLogy(log)
        for i in range(len(hHitAtm)):
            label = '{0} : {1:10d}'.format(hHitAtm[i].GetTitle(), int(hHitAtm[i].GetEntries()))
            lHitAtm.AddEntry(hHitAtm[i], label, 'lpf')
            hHitAtm[i].Draw() if i == 0 else hHitAtm[i].Draw('same')
        lHitAtm.Draw()
        c.cd(2).SetLogy(log)
        for i in range(len(hHitFadc)):
            label = '{0} : {1:10d}'.format(hHitFadc[i].GetTitle(), int(hHitFadc[i].GetEntries()))
            lHitFadc.AddEntry(hHitFadc[i], label, 'lpf')
            hHitFadc[i].Draw() if i == 0 else hHitFadc[i].Draw('same')
        lHitFadc.Draw()
        c.cd(3).SetLogx(log)
        c.cd(3).SetLogy(log)
        for i in range(len(hHitAtmFadc)):
            hHitAtmFadc[i].Draw('box') if i == 0 else hHitAtmFadc[i].Draw('boxsame')
        c.cd(4).SetLogy(log)
        for i in range(len(hPeAtm)):
            label = '{0} : {1:10d}'.format(hPeAtm[i].GetTitle(), int(hPeAtm[i].GetEntries()))
            lPeAtm.AddEntry(hPeAtm[i], label, 'lpf')
            hPeAtm[i].Draw() if i == 0 else hPeAtm[i].Draw('same')
        lPeAtm.Draw()
        c.cd(5).SetLogy(log)
        for i in range(len(hPeFadc)):
            label = '{0} : {1:10d}'.format(hPeFadc[i].GetTitle(), int(hPeFadc[i].GetEntries()))
            lPeFadc.AddEntry(hPeFadc[i], label, 'lpf')
            hPeFadc[i].Draw() if i == 0 else hPeFadc[i].Draw('same')
        lPeFadc.Draw()
        c.cd(6).SetLogx(log)
        c.cd(6).SetLogy(log)
        for i in range(len(hPeAtmFadc)):
            hPeAtmFadc[i].Draw('box') if i == 0 else hPeAtmFadc[i].Draw('boxsame')
        c.Update()
        self.logger.info('Draw {0}'.format(ctitle))
        self.stop()
        return c

    ## __________________________________________________
    def draw_upk(self):
        self.logger.info('started')
        canvases = [# self.draw_pedestal(),
            self.draw_run(),
            self.draw_time(),
            self.draw_upkrun(),
            self.draw_nsec(log=1),
            self.draw_nhits(log=1),
            self.draw_ncharges(log=1),
            self.draw_npes(log=1),
            self.draw_ntrg(),
            self.draw_window(log=1)]
        self.canvases = canvases
        self.logger.info('finished')
        return canvases

    ## __________________________________________________
    def draw_dst(self):
        self.logger.info('-' * 50)
        self.logger.info('started')
        canvases = [self.draw_pot(),
                    self.draw_pedestal(),
                    self.draw_run(),
                    self.draw_time(),
                    self.draw_upkrun(),
                    self.draw_mrrun(),
                    self.draw_nsec(log=1),
                    self.draw_nhits(log=1),
                    self.draw_ncharges(log=1),
                    self.draw_npes(log=1),
                    self.draw_ntrg(),
                    self.draw_window(log=1)]
        self.canvases = canvases
        self.logger.info('finished')
        self.logger.info('-' * 50)
        return canvases

    ## __________________________________________________
    def draw_history(self):
        gNhits = self.gNhits
        gNcharges = self.gNcharges
        gNpes = self.gNpes
        gMaxCharge = self.gMaxCharge
        gMaxPe = self.gMaxPe

        hRunQdc = self.hRunQdc
        hRunQdcRms = self.hRunQdcRms
        hRunTdc = self.hRunTdc
        hRunTdcRms = self.hRunTdcRms
        hRunPe = self.hRunPe
        hRunPeRms = self.hRunPeRms
        hRunCharge = self.hRunCharge
        hRunChargeRms = self.hRunChargeRms
        #hRunTime   , hRunTimeRms    = self.hRunTime   , self.hRunTimeRms

        hTimeQdc = self.hTimeQdc
        hTimeQdcRms = self.hTimeQdcRms
        hTimeTdc = self.hTimeTdc
        hTimeTdcRms = self.hTimeTdcRms
        hTimePe = self.hTimePe
        hTimePeRms = self.hTimePeRms
        hTimeCharge = self.hTimeCharge
        hTimeChargeRms = self.hTimeChargeRms
        #hTimeTime   , hTimeTimeRms    = self.hTimeTime   , self.hTimeTimeRms

        wx, wy = Config.disp
        cname = 'cHistoryPedRun'
        ctitle = os.path.join(Config.plot_dir, cname) + '.png'
        cPedRun = TCanvas(cname, ctitle, wx, wy * 2)
        cPedRun.Divide(2, 4)
        cPedRun.cd(1)
        for i in range(len(hRunQdc)):
            hRunQdc[i].Draw('box') if i == 0 else hRunQdc[i].Draw('boxsame')
        cPedRun.cd(2)
        for i in range(len(hRunQdcRms)):
            hRunQdcRms[i].Draw('box') if i == 0 else hRunQdcRms[i].Draw('boxsame')
        cPedRun.cd(3)
        for i in range(len(hRunTdc)):
            hRunTdc[i].Draw('box') if i == 0 else hRunTdc[i].Draw('boxsame')
        cPedRun.cd(4)
        for i in range(len(hRunTdcRms)):
            hRunTdcRms[i].Draw('box') if i == 0 else hRunTdcRms[i].Draw('boxsame')
        cPedRun.cd(5)
        for i in range(len(hRunPe)):
            hRunPe[i].Draw('box') if i == 0 else hRunPe[i].Draw('boxsame')
        cPedRun.cd(6)
        for i in range(len(hRunPeRms)):
            hRunPeRms[i].Draw('box') if i == 0 else hRunPeRms[i].Draw('boxsame')
        cPedRun.Update()

        cname = 'cHistoryPedTime'
        ctitle = os.path.join(Config.plot_dir, ctitle) + '.png'
        cPedTime = TCanvas(cname, ctitle, wx, wy)
        cPedTime.Divide(2, 2)
        cPedTime.cd(1)
        for i in range(len(hTimeQdc)):
            hTimeQdc[i].Draw('box') if i == 0 else hTimeQdc[i].Draw('boxsame')
        cPedTime.cd(2)
        for i in range(len(hTimeQdcRms)):
            hTimeQdcRms[i].Draw('box') if i == 0 else hTimeQdcRms[i].Draw('boxsame')
        cPedTime.cd(3)
        for i in range(len(hTimeTdc)):
            hTimeTdc[i].Draw('box') if i == 0 else hTimeTdc[i].Draw('boxsame')
        cPedTime.cd(4)
        for i in range(len(hTimeTdcRms)):
            hTimeTdcRms[i].Draw('box') if i == 0 else hTimeTdcRms[i].Draw('boxsame')
        cPedTime.Update()

        cname = 'cHistoryNhits'
        ctitle = os.path.join(Config.plot_dir, ctitle) + '.png'
        cNhits = TCanvas(cname, ctitle, wx, wy)
        for i in range(len(gNhits)):
            gopt = 'ap' if i == 0 else 'psame'
            gNhits[i].Draw(gopt)

        cname = 'cHistoryNcharges'
        ctitle = os.path.join(Config.plot_dir, ctitle) + '.png'
        cNcharges = TCanvas(cname, ctitle, wx, wy)
        for i in range(len(gNcharges)):
            gopt = 'ap' if i == 0 else 'psame'
            gNcharges[i].Draw(gopt)

        cname = 'cHistoryNpes'
        ctitle = os.path.join(Config.plot_dir, ctitle) + '.png'
        cNpes = TCanvas(cname, ctitle, wx, wy)
        for i in range(len(gNpes)):
            gopt = 'ap' if i == 0 else 'psame'
            gNpes[i].Draw(gopt)

        cname = 'cHistoryMaxQ'
        ctitle = os.path.join(Config.plot_dir, ctitle) + '.png'
        cMaxQ = TCanvas(cname, ctitle, wx, wy)
        gMaxCharge.Draw('ap')

        cname = 'cHistoryMaxPe'
        ctitle = os.path.join(Config.plot_dir, ctitle) + '.png'
        cMaxPe = TCanvas(cname, ctitle, wx, wy)
        gMaxPe.Draw('ap')

        self.stop()
        return

    ## __________________________________________________
    def draw_bsd(self):
        hTimeRun = self.hTimeRun
        hTimeSpill = self.hTimeSpill

        wx, wy = Config.disp
        c = TCanvas('c', 'c', wx, wy)
        c.Divide(1, 2)
        c.cd(1)
        for i in range(len(hTimeRun)):
            if i == 0:
                hTimeRun[i].Draw('colz')
        c.cd(2)
        for i in range(len(hTimeSpill)):
            if i == 0:
                hTimeSpill[i].Draw('colz')
        self.stop()
        return

    ## __________________________________________________
    def save(self, ofn='tmp.root'):
        tname = self.get_tree_name()
        start = datetime.datetime.now()
        ofn = os.path.join(Config.root_dir, ofn)
        self.logger.info('-' * 50)
        self.logger.info('save "{0}" to "{1}"'.format(tname, ofn))
        self.stop()
        fout = TFile(ofn, 'recreate')
        if tname == 'upk':
            self.save_upk()
        elif tname == 'dst':
            self.save_dst()
        fout.Close()
        end = datetime.datetime.now()
        self.logger.info('finished ({0} [sec])'.format(end-start))
        self.logger.info('-' * 50)
        return

    ## __________________________________________________
    def save_upk(self):
        self.logger.info('started')
        try:
            for h in self.hRun:
                h.Write()
            for h in self.hTime:
                h.Write()
            for h in self.hNsecFadc:
                h.Write()
            for h in self.hRunRate:
                h.Write()
            for h in self.hTimeRate:
                h.Write()
            for h in self.hWindow:
                h.Write()
            for h in self.hNtrgAtm:
                h.Write()
            for h in self.hNtrgFadc:
                h.Write()
            for h in self.hNtrgAtmFadc:
                h.Write()
            for h in self.hNhitsAtm:
                h.Write()
            for h in self.hNhitsFadc:
                h.Write()
            for h in self.hNhitsAtmFadc:
                h.Write()
            for h in self.hNchargesAtm:
                h.Write()
            for h in self.hNchargesFadc:
                h.Write()
            for h in self.hNchargesAtmFadc:
                h.Write()
            for h in self.hNpesAtm:
                h.Write()
            for h in self.hNpesFadc:
                h.Write()
            for h in self.hNpesAtmFadc:
                h.Write()
            for h in self.hTimeQdc0:
                h.Write()
            for h in self.hTimeQdc1:
                h.Write()
            for h in self.hTimeTdc0:
                h.Write()
            for h in self.hTimeTdc1:
                h.Write()
            for h in self.hTimeRawPmtsum0:
                h.Write()
            for h in self.hTimeRawHitsum0:
                h.Write()
            for h in self.hTimeFadcPmtsum0:
                h.Write()
            for h in self.hTimeFadcHitsum0:
                h.Write()
            for c in self.canvases:
                c.SaveAs(c.GetTitle())
                c.Write()
        except AttributeError:
            self.logger.error('AttributeError\n')

        self.logger.info('finished')
        return

    ## __________________________________________________
    def save_dst(self):
        self.logger.info('started')
        try:
            for h in self.hRun:
                h.Write()
            for h in self.hTime:
                h.Write()
            for h in self.hNsecFadc:
                h.Write()
            for h in self.hRunRate:
                h.Write()
            for h in self.hTimeRate:
                h.Write()
            for h in self.hWindow:
                h.Write()
            for h in self.hNtrgAtm:
                h.Write()
            for h in self.hNtrgFadc:
                h.Write()
            for h in self.hNtrgAtmFadc:
                h.Write()
            for h in self.hNhitsAtm:
                h.Write()
            for h in self.hNhitsFadc:
                h.Write()
            for h in self.hNhitsAtmFadc:
                h.Write()
            for h in self.hNchargesAtm:
                h.Write()
            for h in self.hNchargesFadc:
                h.Write()
            for h in self.hNchargesAtmFadc:
                h.Write()
            for h in self.hNpesAtm:
                h.Write()
            for h in self.hNpesFadc:
                h.Write()
            for h in self.hNpesAtmFadc:
                h.Write()
            for h in self.hTimeQdc0:
                h.Write()
            for h in self.hTimeQdc1:
                h.Write()
            for h in self.hTimeTdc0:
                h.Write()
            for h in self.hTimeTdc1:
                h.Write()
            for h in self.hTimeRawPmtsum0:
                h.Write()
            for h in self.hTimeRawHitsum0:
                h.Write()
            for h in self.hTimeFadcPmtsum0:
                h.Write()
            for h in self.hTimeFadcHitsum0:
                h.Write()
            for h in self.hMrRun:
                h.Write()
            for h in self.hMrRunRate:
                h.Write()
            for c in self.canvases:
                c.Write()
                c.SaveAs(c.GetTitle())
        except AttributeError:
            self.logger.error('AttributeError\n')

        self.logger.info('finished')
        return

    ## __________________________________________________
    def write_summary(self, leg):
        l = TLegend()
        self.logger.info('started')
        ### if leg's class is NOT ROOT.TLegend skipp
        leg.Print()
        #print type(leg), isinstance(leg, type(l))
        #print leg.GetListOfPrimitives()
        self.logger.info('finished')
        return
        

    ## __________________________________________________
    def ntrg_cut(self, br, cut):
        r = False
        atm = br.GetAtmNtrg()
        fadc = br.GetFadcNtrg()
        if (atm == cut and fadc == cut):
            r = True
        return r

    ## __________________________________________________
    def nhits_cut(self, br, cut):
        r = False
        atm = br.GetAtmNhits()
        fadc = br.GetFadcNhits()
        if (atm > cut and fadc > cut):
            r = True
        return r
    
    ## __________________________________________________
    def npes_cut(self, br, cut):
        r = False
        atm = br.GetAtmNpes()
        fadc = br.GetFadcNpes()
        if (atm > cut and fadc > cut):
            r = True
        return r

    ## __________________________________________________
    def fill_runnum(self, h, br, index):
        # print 'fill_runnum'
        runnum = br.GetRunNum() % 10000
        h['hRun'][index].Fill(runnum)
        h['hRunRate'][index].Fill(runnum)

    ## __________________________________________________
    def fill_time(self, h, br, index):
        # print 'fill_time'
        time_sec = br.GetTimeSec()
        h['hTime'][index].Fill(time_sec)
        h['hTimeRate'][index].Fill(time_sec)
        return

    ## __________________________________________________
    def fill_upkrun(self, h, br, index):
        # print 'fill_upkrun'
        upkrun = br.GetUpkRun()
        h['hUpkRun'][index].Fill(upkrun)
        h['hUpkRunRate'][index].Fill(upkrun)
        return

    ## __________________________________________________
    def fill_ntrg(self, h, br, index):
        #print 'fill_ntrg'
        atm = br.GetAtmNtrg()
        fadc = br.GetFadcNtrg()
        h['hNtrgAtm'][index].Fill(atm)
        h['hNtrgFadc'][index].Fill(fadc)
        h['hNtrgAtmFadc'][index].Fill(atm, fadc)
        return

    ## __________________________________________________
    def fill_nhits(self, h, br, index):
        #print 'fill_nhits'
        atm = br.GetAtmNhits()
        fadc = br.GetFadcNhits()
        h['hNhitsAtm'][index].Fill(atm)
        h['hNhitsFadc'][index].Fill(fadc)
        h['hNhitsAtmFadc'][index].Fill(atm, fadc)
        return

    ## __________________________________________________
    def fill_npes(self, h, br, index):
        #print 'fill_npes'
        atm = br.GetAtmNpes()
        fadc = br.GetFadcNpes()
        h['hNpesAtm'][index].Fill(atm)
        h['hNpesFadc'][index].Fill(fadc)
        h['hNpesAtmFadc'][index].Fill(atm, fadc)
        return

    ## __________________________________________________
    def fill_ncharges(self, h, br, index):
        #print 'fill_ncharges'
        atm = br.GetAtmNcharges()
        fadc = br.GetFadcNcharges()
        h['hNchargesAtm'][index].Fill(atm)
        h['hNchargesFadc'][index].Fill(fadc)
        h['hNchargesAtmFadc'][index].Fill(atm, fadc)
        return

    ## __________________________________________________
    def fill_window(self, h, br, index):
        pass

    ## __________________________________________________
    def fill_nsec(self, h, br, index):
        pass

    ## __________________________________________________
    def fill_adc(self, h, br, index):
        pass

    ## __________________________________________________
    def fill_pmtsum(self, h, br, index):
        pass
                             

## __________________________________________________
if __name__ == '__main__':

    usage = '%s [options] root_files' % sys.argv[0]
    parser = OptionParser(usage)
    parser.add_option('-b', '--batch',
                      action='store_true',
                      dest='batch',
                      help='run ROOT in batch mode')
    parser.add_option('-t', '--tree',
                      choices=('upk', 'history', 'bsd', 'dst'),
                      dest='name',
                      help='choose tree name from ([upk], history, bsd, dst).')
    parser.add_option('-o', '--out',
                      dest='ofn',
                      help='set output filename')
    parser.set_defaults(batch=False,
                        tname='upk',
                        ofn='tmp.root')
    (options, args) = parser.parse_args()

    # logger.debug('options : {0}'.format(options))
    # logger.debug('args    : {0}'.format(args))

    gROOT.SetBatch(options.batch)

    if len(args) < 1:
        parser.error('Wrong number of arguments. (%d)\n' % len(args))

    name = options.name
    r = Read(name[:3], name)
    for ifn in args:
        r.add(ifn)
    r.process()
    r.draw()
    r.save(options.ofn)
