//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-10-11 02:02:07+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//
//

//#define DEBUG
//#define DEBUG_HIST

#ifndef __CINT__
#ifndef ROOT_TApplication
#include "TApplication.h"
#endif

#include "TObject.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TLine.h"
#include "TFile.h"
#include "TBranch.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TPaveText.h"
#include "TChain.h"
#include "TMath.h"
#include "TColor.h"
#include "TLegend.h"

#include <signal.h> 
#include <vector>
#include "base.h"
#include "logger.h"

#endif

#ifndef HIST_H
#define HIST_H

// __________________________________________________
const Int_t kNATMCH = 180;
const Int_t kNTYPE  = 2;

// __________________________________________________
void sigcatch(Int_t sig) {
    fprintf(stdout,"\nCaught signal %d. Exit.\n", sig);
    exit(-1);
    signal(sig, SIG_DFL);
}

// __________________________________________________
class Hist : public Base
{
private:
    TTree *thist;
    
    TH1D *hQdcPed[kNATMCH][kNTYPE];
    TH1D *hTdcPed[kNATMCH][kNTYPE];
    TH1D *hQdcPedRms[kNATMCH][kNTYPE];
    TH1D *hTdcPedRms[kNATMCH][kNTYPE];
    
    TH1D *hAtmPe[kNATMCH][kNTYPE];
    TH1D *hAtmCharge[kNATMCH][kNTYPE];
    TH1D *hAtmTime[kNATMCH][kNTYPE];
    
    TH1D *hAtmNhits;
    TH1D *hAtmNcharges;
    TH1D *hAtmNpes;
    
    TH1D *hFadcNhits;
    TH1D *hFadcNcharges;
    TH1D *hFadcNpes;

    TH1D *hAtmMaxCharge;
    TH1D *hAtmMaxPe;

    // Temp. variables
    Int_t    fTime;
    Int_t    fRunNum       , fSpillNum;
    Double_t fAtmNhits     , fAtmNhitsRms;
    Double_t fAtmNcharges  , fAtmNchargesRms;
    Double_t fAtmNpes      , fAtmNpesRms;
    Double_t fFadcNhits    , fFadcNhitsRms;
    Double_t fFadcNcharges , fFadcNchargesRms;
    Double_t fFadcNpes     , fFadcNpesRms;
    Double_t fAtmMaxCharge , fAtmMaxChargeRms;
    Double_t fAtmMaxPe     , fAtmMaxPeRms;
    
    Double_t fPed[kNATMCH][kNTYPE]      , fPedRms[kNATMCH][kNTYPE];
    Double_t fAtmPe[kNATMCH][kNTYPE]    , fAtmPeRms[kNATMCH][kNTYPE];
    Double_t fAtmCharge[kNATMCH][kNTYPE], fAtmChargeRms[kNATMCH][kNTYPE];
    Double_t fAtmTime[kNATMCH][kNTYPE]  , fAtmTimeRms[kNATMCH][kNTYPE];

public:
    // ----------
    Hist();
    ~Hist();

    Int_t ReadHists(const char* ifn);
    Int_t DrawHists();
    Int_t ReadHistory(const char* ifn);
    Int_t DrawHistory();

};

#endif

// __________________________________________________
Hist::Hist()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif
}

// __________________________________________________
Hist::~Hist()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif
}

// __________________________________________________
Int_t Hist::ReadHists(const char* ifn)
{
    LogI("Read '%s'.", ifn);
    if (PathExists(ifn, "r") != 0) return -1;

    TFile *fin = new TFile(ifn, "read");

    TString name;
    for (Int_t iatm = 0; iatm < kNATMCH; iatm++) {
        for (Int_t itype = 0; itype < kNTYPE; itype++) {
            // --------------------------------------------------
            name.Form("hQdcPed%03dT%d", iatm, itype);
            hQdcPed[iatm][itype] = (TH1D*)fin->Get(name.Data());
            // --------------------------------------------------
            name.Form("hTdcPed%03dT%d", iatm, itype);
            hTdcPed[iatm][itype] = (TH1D*)fin->Get(name.Data());
            // --------------------------------------------------
            name.Form("hQdcPedRms%03dT%d", iatm, itype);
            hQdcPedRms[iatm][itype] = (TH1D*)fin->Get(name.Data());
            // --------------------------------------------------
            name.Form("hTdcPedRms%03dT%d", iatm, itype);
            hTdcPedRms[iatm][itype] = (TH1D*)fin->Get(name.Data());
            // --------------------------------------------------
            name.Form("hAtmPe%03dT%d", iatm, itype);
            hAtmPe[iatm][itype] = (TH1D*)fin->Get(name.Data());
            // --------------------------------------------------
            name.Form("hAtmCharge%03dT%d", iatm, itype);
            hAtmCharge[iatm][itype] = (TH1D*)fin->Get(name.Data());
            // --------------------------------------------------
            name.Form("hAtmTime%03dT%d", iatm, itype);
            hAtmTime[iatm][itype] = (TH1D*)fin->Get(name.Data());
        }
    }

    // ------------------------------
    name.Form("hAtmNpes");
    hAtmNpes = (TH1D*)fin->Get(name.Data());
    // --------------------
    name.Form("hAtmNcharges");
    hAtmNcharges = (TH1D*)fin->Get(name.Data());
    // --------------------
    name.Form("hAtmNhits");
    hAtmNhits = (TH1D*)fin->Get(name.Data());
    // ------------------------------
    name.Form("hFadcNpes");
    hFadcNpes = (TH1D*)fin->Get(name.Data());
    // --------------------
    name.Form("hFadcNcharges");
    hFadcNcharges = (TH1D*)fin->Get(name.Data());
    // --------------------
    name.Form("hFadcNhits");
    hFadcNhits = (TH1D*)fin->Get(name.Data());
    // ------------------------------
    name.Form("hAtmMaxCharge");
    hAtmMaxCharge = (TH1D*)fin->Get(name.Data());
    // --------------------
    name.Form("hAtmMaxPe");
    hAtmMaxPe = (TH1D*)fin->Get(name.Data());

    return 0;
}

// __________________________________________________
Int_t Hist::DrawHists()
{
    LogI(__FUNCTION__);
    TCanvas *c1 = new TCanvas("c1", "c1", 1500, 1000);
    TString pdf = "tmp/cHists.pdf";
    c1->Print(pdf + "[", "pdf");
    c1->Divide(3, 2);
    c1->cd(1)->SetLogy();
    hFadcNhits->Draw();
    hAtmNhits->SetFillStyle(3004);
    hAtmNhits->Draw("same");
    
    c1->cd(2)->SetLogy();
    hFadcNcharges->Draw();
    hAtmNcharges->SetFillStyle(3004);
    hAtmNcharges->Draw("same");
    
    c1->cd(3)->SetLogy();
    hFadcNpes->Draw();
    hAtmNpes->SetFillStyle(3004);
    hAtmNpes->Draw("same");
    
    c1->cd(5)->SetLogy();
    hAtmMaxCharge->Draw();
    
    c1->cd(6)->SetLogy();
    hAtmMaxPe->Draw();
    
    c1->Print(pdf, "pdf");
    c1->Print(pdf + "]", "pdf");

    TString name, title;
    const char* gopt[kNTYPE] = {"", "same"};
    TCanvas *c2 = new TCanvas("c2", "c2", 1200, 600);
    c2->Divide(4, 2);

    for (Int_t iatm = 0; iatm < kNATMCH; iatm++) {
        title.Form("ATM %3d", iatm);
        pdf.Form("tmp/cQdcTdcAtm%03d.png", iatm);
        c2->SetTitle(title.Data());
        for (Int_t itype = 0; itype < kNTYPE; itype++) {
            hQdcPed[iatm][itype]->GetXaxis()->SetRangeUser(600, 1000);
            hTdcPed[iatm][itype]->GetXaxis()->SetRangeUser(1900, 2200);
            if (itype == 1) {
                hQdcPedRms[iatm][itype]->SetFillStyle(3004);
                hTdcPedRms[iatm][itype]->SetFillStyle(3004);
                hAtmCharge[iatm][itype]->SetFillStyle(3004);
                hAtmPe[iatm][itype]->SetFillStyle(3004);
                hAtmTime[iatm][itype]->SetFillStyle(3004);
            }
            c2->cd(1)->SetLogy();
            hQdcPed[iatm][itype]->Draw(gopt[itype]);
            // --------------------------------------------------
            c2->cd(2)->SetLogy();
            hTdcPed[iatm][itype]->Draw(gopt[itype]);
            // --------------------------------------------------
            c2->cd(5)->SetLogy();
            hQdcPedRms[iatm][itype]->Draw(gopt[itype]);
            // --------------------------------------------------
            c2->cd(6)->SetLogy();
            hTdcPedRms[iatm][itype]->Draw(gopt[itype]);
            // --------------------------------------------------
            c2->cd(3)->SetLogy();
            hAtmCharge[iatm][itype]->Draw(gopt[itype]);
            // --------------------------------------------------
            c2->cd(7)->SetLogy();
            hAtmPe[iatm][itype]->Draw(gopt[itype]);
            // --------------------------------------------------
            c2->cd(4)->SetLogy();
            hAtmTime[iatm][itype]->Draw(gopt[itype]);
        }
        
        fprintf(stderr, "[%s]\tCanvas '%s'\n", __FUNCTION__, c2->GetTitle());
        gSystem->ProcessEvents();
        c2->Update();
        if (!gROOT->IsBatch()) {
            Int_t r = AskContinue();
            if (r == -1) break;
            iatm = iatm + r;
        }
        else {
            c2->Print(pdf, "png");
        }
    }
    
    return 0;
}

// __________________________________________________
Int_t Hist::ReadHistory(const char* ifn)
{
    TFile *fin = new TFile(ifn, "read");
    thist = (TTree*)fin->Get("history");

    thist->SetBranchAddress("runnum"            , &fRunNum);
    thist->SetBranchAddress("spillnum"          , &fSpillNum);
    thist->SetBranchAddress("time"              , &fTime);
    thist->SetBranchAddress("atm_nhits"         , &fAtmNhits);
    thist->SetBranchAddress("atm_nhits_rms"     , &fAtmNhitsRms);
    thist->SetBranchAddress("atm_ncharges"      , &fAtmNcharges);
    thist->SetBranchAddress("atm_ncharges_rms"  , &fAtmNchargesRms);
    thist->SetBranchAddress("atm_npes"          , &fAtmNpes);
    thist->SetBranchAddress("atm_npes_rms"      , &fAtmNpesRms);
    thist->SetBranchAddress("fadc_nhits"        , &fFadcNhits);
    thist->SetBranchAddress("fadc_nhits_rms"    , &fFadcNhitsRms);
    thist->SetBranchAddress("fadc_ncharges"     , &fFadcNcharges);
    thist->SetBranchAddress("fadc_ncharges_rms" , &fFadcNchargesRms);
    thist->SetBranchAddress("fadc_npes"         , &fFadcNpes);
    thist->SetBranchAddress("fadc_npes_rms"     , &fFadcNpesRms);

    return 0;
}

// __________________________________________________
Int_t Hist::DrawHistory()
{
    TGraphErrors *geRunAtmNhits     = new TGraphErrors(0);
    TGraphErrors *geRunAtmNcharges  = new TGraphErrors(0);
    TGraphErrors *geRunAtmNpes      = new TGraphErrors(0);
    TGraphErrors *geRunFadcNhits    = new TGraphErrors(0);
    TGraphErrors *geRunFadcNcharges = new TGraphErrors(0);
    TGraphErrors *geRunFadcNpes     = new TGraphErrors(0);

    const Int_t nentries = thist->GetEntries();

    for (Int_t ientry = 0; ientry < nentries; ientry++) {
        thist->GetEntry(ientry);
        // --------------------
        Int_t np = geRunAtmNhits->GetN();
        geRunAtmNhits->SetPoint(np, fTime, fAtmNhits);
        geRunAtmNhits->SetPointError(np, 0, fAtmNhitsRms);
        // --------------------
        np = geRunAtmNcharges->GetN();
        geRunAtmNcharges->SetPoint(np, fTime, fAtmNcharges);
        geRunAtmNcharges->SetPointError(np, 0, fAtmNchargesRms);
        // --------------------
        np = geRunAtmNpes->GetN();
        geRunAtmNpes->SetPoint(np, fTime, fAtmNpes);
        geRunAtmNpes->SetPointError(np, 0, fAtmNpesRms);
        // --------------------
        np = geRunFadcNhits->GetN();
        geRunFadcNhits->SetPoint(np, fTime, fFadcNhits);
        geRunFadcNhits->SetPointError(np, 0, fFadcNhitsRms);
        // --------------------
        np = geRunFadcNcharges->GetN();
        geRunFadcNcharges->SetPoint(np, fTime, fFadcNcharges);
        geRunFadcNcharges->SetPointError(np, 0, fFadcNchargesRms);
        // --------------------
        np = geRunFadcNpes->GetN();
        geRunFadcNpes->SetPoint(np, fTime, fFadcNpes);
        geRunFadcNpes->SetPointError(np, 0, fFadcNpesRms);
    }

    TCanvas *c1 = new TCanvas("DrawHistory", "DrawHistory", 1200, 800);
    c1->Divide(1, 3);
    c1->cd(1);
    geRunAtmNhits->GetXaxis()->SetTimeDisplay(1);
    geRunAtmNhits->GetXaxis()->SetTimeFormat("#splitline{%m/%d}{%H:%M}");
    geRunAtmNhits->GetXaxis()->SetTitle("Time [date]");
    geRunAtmNhits->GetYaxis()->SetTitle("<Nhits> #pm <Nhits>_{rms} [#]");
    geRunAtmNhits->SetTitle("Average Nhits History");
    geRunAtmNhits->Draw("ap");
    geRunFadcNhits->SetMarkerColor(4);
    geRunFadcNhits->SetLineColor(4);
    geRunFadcNhits->Draw("psame");

    c1->cd(2);
    geRunAtmNcharges->GetXaxis()->SetTimeDisplay(1);
    geRunAtmNcharges->GetXaxis()->SetTimeFormat("#splitline{%m/%d}{%H:%M}");
    geRunAtmNcharges->GetXaxis()->SetTitle("Time [date]");
    geRunAtmNcharges->GetYaxis()->SetTitle("<Ncharges> #pm <Ncharges>_{rms} [pC]");
    geRunAtmNcharges->SetTitle("Average Ncharges History");
    geRunAtmNcharges->Draw("ap");
    geRunFadcNcharges->SetMarkerColor(4);
    geRunFadcNcharges->SetLineColor(4);    
    geRunFadcNcharges->Draw("psame");

    c1->cd(3);
    geRunAtmNpes->GetXaxis()->SetTimeDisplay(1);
    geRunAtmNpes->GetXaxis()->SetTimeFormat("#splitline{%m/%d}{%H:%M}");
    geRunAtmNpes->GetXaxis()->SetTitle("Time [date]");
    geRunAtmNpes->GetYaxis()->SetTitle("<Npes> #pm <Npes>_{rms} [p.e.]");
    geRunAtmNpes->SetTitle("Average Npes History");
    geRunAtmNpes->Draw("ap");
    geRunFadcNpes->SetMarkerColor(4);
    geRunFadcNpes->SetLineColor(4);    
    geRunFadcNpes->Draw("psame");
    
    return 0;
}

// __________________________________________________
Int_t Usage()
{
    fprintf(stderr, "Usage: ./%s [-hbcdt] ifn\n", __FILE__);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-b:\trun as batch mode\n");
    fprintf(stderr, "\t-c:\tdraw history\n");
    fprintf(stderr, "\t-d:\tdraw histogram\n");
    fprintf(stderr, "\n");
    return 0;
}

// __________________________________________________
Int_t hist(Int_t argc, char** argv)
{
    // DEFINE YOUR MAIN FUNCTION HERE
    // Option analysis

    TString ifn;
    Bool_t drawhistflag    = kFALSE;
    Bool_t drawhistoryflag = kFALSE;
    Bool_t testflag        = kFALSE;
    
    Int_t opt;
    while ( (opt = getopt(argc, argv, "hbcdt") ) != -1) {
        switch (opt) {
        case 'h':
            Usage();
            exit(-1);
            break;
        case 'b':
            printf("\n");
            gROOT->SetBatch();
            break;
        case 'c':
            drawhistoryflag = kTRUE;
            break;
        case 'd':
            drawhistflag = kTRUE;
            break;
        case 't':
            testflag = kTRUE;
            break;
        default:
            fprintf(stderr, "Error:\tWrong options. [%d, %s]\n", optind, optarg);
            fprintf(stderr, "\nQuit. Bye Bye\n");
            exit(-1);
        }
    }

    argc -= optind;
    argv += optind;

    for (Int_t iarg = 0; iarg < argc; iarg++) {
        printf("argc:%d\targv[%d]:\t%s\n", argc, iarg, argv[iarg]);
    }
    if (testflag)  ifn = "../mizudaq/process/unpacked_file.root";
    else if (argc == 1) ifn = argv[0];
    else  {
        fprintf(stderr, "Error:\tWrong number of argument. (%d)\n", argc);
        exit(-1);
    }

    Hist *hist = new Hist();

    if(SIG_ERR == signal(SIGINT, sigcatch)){
        fprintf(stderr,"failed to set signalhandler.\n");
        exit(1);
    }
    
    if (hist->PathExists(ifn.Data(), "r") != 0) exit(-1);
    
    if (drawhistflag) {
        hist->ReadHists(ifn.Data());
        hist->DrawHists();
    } else if (drawhistoryflag) {
        hist->ReadHistory(ifn.Data());
        hist->DrawHistory();
    } else {
        fprintf(stderr, "Error:\tWrong number of arguments. (%d)\n", argc);
        Usage();
        fprintf(stderr, "\n\tQuit. Bye Bye\n");
        exit(-1);
        hist->LogI("\tDone!!!");
        delete hist;
    }

    return 0;
}

// __________________________________________________
#ifndef __CINT__
Int_t main(Int_t argc, char** argv)
{
    TApplication histApp("drawApp", &argc, argv);
    //TApplication histApp("drawApp", 0, 0, 0, 0);
    Int_t retVal = hist(histApp.Argc(), histApp.Argv());
    if(gROOT->IsBatch()) gROOT->Idle( 1, ".q");
    histApp.Run();
    return retVal;
}
#endif

//____________________________________________________________________ 
//  
// EOF
//
