#include <iostream>
#include <assert.h>

#include <TH1.h>
#include <TH2.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TMath.h>
#include <TROOT.h>

#include "T2KWCData.h"
#include "mkplot.h"

/////////////////////////////
typedef struct {
  mkplot mk;
  T2KWCEvent *evt;

  Int_t nfiles;
  Double_t anorm;
  Int_t maxpe;
  Int_t minpe[5];
  Int_t nhits[5];
  Int_t sumpe[5];

} AnaMC;
///////////////////////////////


//
void do_analysis( AnaMC &a )
{
  // neutrino interaction info.
  T2KWCNeutInfo *neut = (T2KWCNeutInfo*)( a.evt->GetNeutInfo(0) );
  assert(neut);

  Int_t mode = TMath::Abs(neut->Mode());
  Int_t mode_index = a.mk.mode_index(mode);
  Double_t enu = neut->Energy();

  // Fill flux histogram
  Double_t surf = TMath::Pi()*70*70;
  a.mk.enu_flux->Fill(enu, neut->Norm()/surf/a.nfiles );

  // simver
  T2KWCSimVertex *simver = (T2KWCSimVertex*)( a.evt->GetSimVertex(0) );
  if(simver==NULL) return;

  Int_t vflag = simver->VFlag();

  // Fill interaction historgram
  if( vflag>0 && mode>0 ) {
    a.mk.enu_int[vflag][mode_index]->Fill(enu,a.anorm);

    // sumpe
    for(int i=0; i<NHITCUT; i++) {
      a.mk.sumpe[vflag][mode_index][i]->Fill(a.sumpe[i],a.anorm);
    }

    // observed energy
    for(int i=0; i<NHITCUT; i++) {
      for(int j=0; j<NPECUT; j++) {
        if( a.sumpe[i]>=totpecut[j] ) {
          a.mk.enu_obs[vflag][mode_index][i][j]->Fill(enu,a.anorm);
        }
      }
    }

  }

};


//
int main(int argc,char *argv[])
{
  gROOT->Reset();

  bool debug_flag = false;
  char fout_name[300] = "fout_mkplot.root";

	int c=-1;
	extern int optind;
	extern char *optarg;
	while ((c = getopt(argc, argv, "o:d")) != -1) {
    switch(c){
      case 'o':
        sprintf(fout_name, "%s", optarg);
        break;
			case 'd':
        debug_flag = true;
				break;
      case 'h':
			default:
        std::cout << "-o : output file name" << std::endl;
        std::cout << "-d : for debug run" << std::endl;
				exit(1);
		}
	}
	argc-=optind;
	argv+=optind;
  
  // chain trees for analysis
	TChain *fChain = new TChain("tree","tree");
	for (int i=0; i<argc; i++) {
    std::cout << "input files : " << argv[i] << std::endl;
		fChain->Add(argv[i]);
	}

  //
  AnaMC a;
  a.mk.Init(fout_name);

  a.nfiles = fChain->GetNtrees();
  std::cout << "Add File : " << a.nfiles << std::endl;
  if( a.nfiles==0 ) {
    std::cout << "No file added" << std::endl;
    exit(1);
  }

	// set branch address
  TBranch* EvtBr = fChain->GetBranch("mc.");
  a.evt = new T2KWCEvent();
  EvtBr -> SetAddress(&a.evt);
  fChain ->SetBranchAddress("mc.", &a.evt);

  fChain->SetBranchAddress("anorm",&a.anorm);
  fChain->SetBranchAddress("maxpe",&a.maxpe);
  fChain->SetBranchAddress("minpe",a.minpe);
  fChain->SetBranchAddress("nhits",a.nhits);
  fChain->SetBranchAddress("sumpe",a.sumpe);

  if(a.nfiles==1) fChain->SetBranchAddress("nfiles",&a.nfiles);

  // start event loop
  Long64_t nevt = fChain->GetEntries();
  std::cout << "Total # of events : " << nevt <<std::endl;
  if( nevt==0 ) exit(1);

  Long64_t fN=0;

  while( fChain->GetEntry(fN) ) {

    //
    if( fN%10000==0 ) 
      std::cout << "processed : " << fN << std::endl;

    if( debug_flag && fN>100000 ) break;
  
    //
    do_analysis( a );

    //
    a.evt->Clear();
    fN++;

	} // end of loop

  // merge histo
  a.mk.Merge();

}
