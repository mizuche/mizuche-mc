#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys, os

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem, gStyle
from ROOT import TFile, TVectorD, TGraph, TCanvas, TH1D, TGraphErrors, TBox
from ROOT import TF1, TGaxis, TChain, TH2D, TLegend
from ROOT import Double

mizulib = '../mizulib/'
from ROOT import gSystem
gSystem.Load(mizulib+'libT2KWCUnpack.so')
from ROOT import T2KWCUnpack
from ROOT import T2KWCHit

class Draw(object):
    ## __________________________________________________
    def __init__(self):
        pass

    ## __________________________________________________
    def read(self, ifn):
        if not os.path.exists(ifn):
            sys.stderr.write('Error:\t"%s" does not exist.\n' % ifn)
            sys.exit(-1)

        fin = TFile(ifn, 'read')
        return fin

    ## __________________________________________________
    def waveforms(self, ifn):
        print 'Draw waveforms in "%s".' % ifn
        fin = self.read(ifn)

        umode = fin.Get('umode')
        umode.GetEntry(0)
        mode = umode.umode
        print 'Checking unpacked mode : %d' % mode
        if not mode == 1:
            sys.stderr.write('Error:\t"%s" does not have waveform data.\n' % ifn)
            sys.stderr.write('\tQuit. Bye Bye!!!\n')
            sys.exit(-1)
        
        upk = fin.Get('upk')
        unpack = T2KWCUnpack()
        br = upk.GetBranch('upk.')
        br.SetAddress(unpack)
        upk.SetBranchAddress('upk.', unpack)

        ## bunch timing (samp. window)
        bts = [401, 550, 696, 845, 985, 1136, 1286, 1420]
        
        c = TCanvas('cWaveform', 'check waveforms', 1000, 1000)
        c.Divide(1, 2)

        nentries = upk.GetEntries()
        ientry = 0
        for _ in range(0, nentries):
            ## PMTSUM
            upk.Project('', 'fadc_pmtsum:Iteration$', 'Entry$ == %d' % ientry)
            xp = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yp = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gp = TGraph(xp, yp)
            gp.SetTitle('PMTSUM (%4d/%4d)' % (ientry, nentries))
            gp.GetXaxis().SetTitle('Tming [nsec]')
            gp.GetYaxis().SetTitle('PMTSUM [mV]')

            ## HITSUM
            upk.Project('', 'fadc_hitsum:Iteration$', 'Entry$ == %d' % ientry)
            xh = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yh = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gh = TGraph(xh, yh)
            gh.SetTitle('HITSUM (%4d/%4d)' % (ientry, nentries))
            gh.GetXaxis().SetTitle('Timing [nsec]')
            gh.GetYaxis().SetTitle('HITSUM [mV]')

            ## Bunch timing and HITSUM height
            upk.Project('', '-fadc_height:fadc_nsec/4', 'Entry$ == %d' % ientry)
            xb = TVectorD(upk.GetSelectedRows(), upk.GetV2())
            yb = TVectorD(upk.GetSelectedRows(), upk.GetV1())

            gb = TGraph(xb, yb)
            gb.SetTitle('Hit Timing (%4d/%4d)' % (ientry, nentries))
            gb.GetXaxis().SetTitle('Timing [nsec]')
            gb.GetYaxis().SetTitle('Height [mV]')
            gb.SetLineColor(2)
            gb.SetMarkerColor(2)

            nb = gb.GetN()

            win_p = []
            ymin = gp.GetYaxis().GetXmin()
            ymax = gp.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = x - 18
                xmax = x + 9
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3001)
                win_p.append(box)

            win_h = []
            ymin = gh.GetYaxis().GetXmin()
            ymax = gh.GetYaxis().GetXmax()
            for i in range(0, nb):
                x = Double()
                y = Double()
                gb.GetPoint(i, x, y)
                xmin = x + 6
                xmax = x + 45
                box = TBox(xmin, ymin, xmax, ymax)
                box.SetFillColor(2)
                box.SetLineColor(2)
                box.SetLineWidth(2)
                box.SetLineStyle(1)
                box.SetFillStyle(3001)
                win_h.append(box)                
            
            upk.GetEntry(ientry)

            fadc_ntrg     = unpack.GetFadcNtrg()
            fadc_nhits    = unpack.GetFadcNhits()
            fadc_ncharges = unpack.GetFadcNcharges()
            fadc_npes     = unpack.GetFadcNpes()
            
            atm_ntrg     = unpack.GetAtmNtrg()
            atm_nhits    = unpack.GetAtmNhits()
            atm_ncharges = unpack.GetAtmNcharges()
            atm_npes     = unpack.GetAtmNpes()

            print '\tEntry# %4d' % _
            #print '\tFADC:\t%5d [trg]\t%5d [hits]\t%8.2f [pC]\t%8.2f [p.e.]' % (fadc_ntrg, fadc_nhits, fadc_ncharges, fadc_npes)
            #print '\t ATM:\t%5d [trg]\t%5d [hits]\t%8.2f [pC]\t%8.2f [p.e.]' % (atm_ntrg, atm_nhits, atm_ncharges, atm_npes)
            upk.upk.Print()
            

            c.cd(1)
            gp.Draw("apl")
            for box in win_p:
                box.Draw()
            c.cd(2)
            gh.Draw("apl")
            if fadc_ntrg > 0:
                gb.Draw("psame")
            for box in win_h:
                box.Draw()
            c.Update()

            ans = raw_input('Select next entry or quit (#/p/[n]/q) > ')
            if not ans in ['', 'p', 'P', 'q', 'Q']:
                if int(ans) in range(0, nentries):
                    ientry = int(ans)
            elif ans in ['p', 'P']:
                ientry = ientry - 1
                if ientry < 0:
                    sys.stderr.write('Error:\tOut of lower range\n')
                    ientry = 0
            elif ans in ['q', 'Q']:
                sys.stderr.write('\tBye Bye!!!\n')
                sys.exit(-1)
            else:
                ientry = ientry + 1
                if ientry >= nentries:
                    sys.stderr.write('Error:\tOut of upper range\n')
                    ientry = nentries - 1

        return

    ## __________________________________________________
    def trg(self, ifn):
        fin = self.read(ifn)
        upk = fin.Get('upk')

        name = 'hAtmTrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hAtmTrg = TH1D(name, title, 10, 0, 10)
        upk.Project(hAtmTrg.GetName(), "atm_ntrg");
        
        name = 'hFadcTrg'
        title = '%s;Ntrg [#];Entries [#]' % name
        hFadcTrg = TH1D(name, title, 10, 0, 10)
        upk.Project(hFadcTrg.GetName(), "fadc_ntrg");

        c = TCanvas('c', 'c', 500, 500)
        hAtmTrg.SetLineColor(2)
        hAtmTrg.SetFillColor(2)
        hAtmTrg.SetFillStyle(1001)
        hAtmTrg.Draw();
        hFadcTrg.SetLineColor(4)
        hFadcTrg.SetFillColor(4)
        hFadcTrg.Draw('same');

        l = TLegend(0.5, 0.7, 0.9, 0.9)
        l.SetFillColor(0)
        l.AddEntry(hAtmTrg, 'ATM', 'lf')
        l.AddEntry(hFadcTrg, 'FADC', 'lf')
        l.Draw()
        
        self.stop();

    ## __________________________________________________
    def ncharges(self, ifn):
        fin = self.read(ifn)
        upk = fin.Get('upk')

        cut = 'atm_ntrg ==1 && fadc_ntrg==1'
        name = 'hAtmNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hAtmNcharges = TH1D(name, title, 1500, 0, 1500)
        upk.Project(hAtmNcharges.GetName(), 'atm_ncharges', cut);
        
        name = 'hFadcNcharges'
        title = '%s;Ncharges [pC];Entries [#]' % name
        hFadcNcharges = TH1D(name, title, 1500, 0, 1500)
        upk.Project(hFadcNcharges.GetName(), 'fadc_ncharges/0.088', cut);

        c = TCanvas('c', 'c', 500, 500)
        hAtmNcharges.SetLineColor(2)
        hAtmNcharges.SetFillColor(2)
        hAtmNcharges.SetFillStyle(1001)
        hAtmNcharges.Draw();
        hFadcNcharges.SetLineColor(4)
        hFadcNcharges.SetFillColor(4)
        hFadcNcharges.Draw('same');

        l = TLegend(0.1, 0.7, 0.9, 0.9)
        l.SetFillColor(0)
        l.AddEntry(hAtmNcharges,  'ATM : %d entries' % hAtmNcharges.Integral(), 'lf')
        l.AddEntry(hFadcNcharges, 'FADC: %d entries' % hFadcNcharges.Integral(), 'lf')
        l.Draw()
        
        self.stop();

    ## __________________________________________________
    def spill(self, ifn):
        fin = self.read(ifn)
        upk = fin.Get('upk')
        
        # unpack = T2KWCUnpack()
        # br = upk.GetBranch('upk.')
        # br.SetAddress(unpack)
        # upk.SetBranchAddress('upk.', unpack)

        upk.Project('', 'spillnum:time_sec + time_usec')
        xs = TVectorD(upk.GetSelectedRows(), upk.GetV2())
        ys = TVectorD(upk.GetSelectedRows(), upk.GetV1())

        gs = TGraph(xs, ys)
        gs.GetXaxis().SetTimeDisplay(1)
        gs.GetXaxis().SetTimeFormat('%m/%d')
        gs.GetXaxis().SetTitle('Date')
        gs.GetYaxis().SetTitle('Spill [#]')
        gs.GetXaxis().SetRangeUser(0, 70000)

        upk.Project('', '(runnum%10000)*50:time_sec + time_usec')
        xr = TVectorD(upk.GetSelectedRows(), upk.GetV2())
        yr = TVectorD(upk.GetSelectedRows(), upk.GetV1())
        
        gr = TGraph(xr, yr)
        gr.GetXaxis().SetTimeDisplay(1)
        gr.GetXaxis().SetTimeFormat('%m/%d')
        gr.SetMarkerStyle(4)
        #gr.SetMarkerColor(2)
        #gr.SetLineColor(2)

        xmax = gs.GetXaxis().GetXmax()
        ymin = gs.GetYaxis().GetXmin()
        ymax = gs.GetYaxis().GetXmax()

        bsd = TChain('bsd')
        bsd.Add('bsd/v01/bsd_run042*.root')
        bsd.Add('bsd/v01/bsd_run043*.root')

        bsd.Project('', '(nurun%10000)*50:trg_sec[0]')
        xn = TVectorD(bsd.GetSelectedRows(), bsd.GetV2())
        yn = TVectorD(bsd.GetSelectedRows(), bsd.GetV1())

        gn = TGraph(xn, yn)
        gn.GetXaxis().SetTimeDisplay(1)
        gn.GetXaxis().SetTimeFormat('%m/%d')
        gn.SetMarkerStyle(4)
        gn.SetMarkerColor(4)
        gn.SetLineColor(4)

        bsd.Project('', '(spillnum+1)&0xffff:trg_sec[0]')
        xb = TVectorD(bsd.GetSelectedRows(), bsd.GetV2())
        yb = TVectorD(bsd.GetSelectedRows(), bsd.GetV1())

        gb = TGraph(xb, yb)
        gb.GetXaxis().SetTimeDisplay(1)
        gb.GetXaxis().SetTimeFormat('%m/%d')
        gb.SetMarkerColor(4)
        gb.SetLineColor(4)

        axis_right = TGaxis(xmax, ymin, xmax, ymax, ymin, ymax/50, 005, "+L");
        axis_right.SetLineColor(2);
        axis_right.SetLabelColor(2);
        axis_right.SetLabelFont(132);
        axis_right.SetTitle("Run [#]");
        axis_right.SetTitleFont(132);
        axis_right.SetTitleColor(2);

        c = TCanvas('cSpill', 'cSpill', 1000, 500)
        gs.Draw('ap')
        gr.Draw('psame')
        gb.Draw('psame')
        gn.Draw('psame')
        axis_right.Draw()

        self.stop()

    ## __________________________________________________
    def eventrate(self, ifn, hname):
        fin = self.read(ifn)
        print 'Read "%s"' % hname
        h = fin.Get(hname)
        h.Draw()

        thresholds = [40., 50., 60., 70., 80., 90., 100.,
                      110., 120., 130., 140., 150., 160., 170.]

        n = h.GetEntries()
        g = TGraphErrors(0)

        for thr in thresholds:
            xmin  = h.FindBin(thr)
            xmax  = h.FindLastBinAbove()
            e     = h.Integral(xmin, xmax)
            ratio = float(e)/float(n)
            print '%5.1f\t[%3d, %3d]\t%6d /%6d = %8.3f' % (thr, xmin, xmax, e, n, ratio)
            g.SetPoint(g.GetN(), thr, e)

        return g
    
    ## __________________________________________________
    def eventrates(self, ifn):
        hnames = ['hAtmNpes', 'hFadcNpes']
        c = TCanvas('cEventRate', 'cEventRate', 1000, 500)
        frame = TH2D('frame', 'frame', 200, 0, 200, 15000, 0, 15000)
        frame.Draw()
        c.Update()

        for hname in hnames:
            g = self.eventrate(ifn, hname)
            g.Draw('psame')
            c.Update()
            self.stop()

        c.Update()
        self.stop()
        
    ## __________________________________________________
    def stop(self):
        gSystem.ProcessEvents()
        ans = raw_input('Press "q" to quit > ')
        if ans in ['q' or 'Q']:
            sys.exit(-1)
            

## __________________________________________________
if __name__ == '__main__':

    usage = 'Usage:\t%s [w] ifn' % sys.argv[0]

    parser = OptionParser(usage)
    parser.add_option('-w', '--waveform',
                      action = 'store_true',
                      dest = 'waveform',
                      help = 'draw waveforms')
    parser.add_option('-s', '--spill',
                      action = 'store_true',
                      dest = 'spill',
                      help = 'draw spill and time')
    parser.add_option('-e', '--eventrate',
                      action = 'store_true',
                      dest = 'eventrate',
                      help = 'draw spill and time')
    parser.add_option('-t', '--trg',
                      action = 'store_true',
                      dest = 'trg',
                      help = 'draw spill and time')
    parser.set_defaults(waveform = False,
                        spill    = False)

    (options, args) = parser.parse_args()

    print options
    print len(args), args

    if len(args) < 1:
        sys.stderr.write('Error:\tWrong number of arguments. (%d)\n' % len(args))
        print usage
        sys.exit(-1)

    d = Draw()
    for ifn in args:
        if options.waveform:
            d.waveforms(ifn)
        elif options.spill:
            d.spill(ifn)
        elif options.eventrate:
            d.eventrates(ifn)
        elif options.trg:
            d.trg(ifn)
        else:
            d.ncharges(ifn)
        

