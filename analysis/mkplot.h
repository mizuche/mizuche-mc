#include <TH1.h>
#include <TObjArray.h>
#include <TFile.h>
#include <TMath.h>

//#define ENABLESUMW2

//
const int NFLAV = 1;
const int NAREA = 3;  // Tank, OV, FV
const int NMODE = 10;  // ALL, CC, CCQE, NC
const int NHITCUT = 4;
const int NPECUT = 3;

//
const char *flav_name[NFLAV] = {"NM"};
const char *area_name[NAREA] = {"TANK","OV","FV"};

const char *mode_name[NMODE] = 
{"ALL","CC","CCQE","CC1PI","CCCOHPI","CCOTHERS",
 "NC","NC1PI","NCCOHPI","NCOTHERS"};

#define CC_MODE_ID 1
#define NC_MODE_ID 7


const int hitcut[NHITCUT] = {2,3,4,5};
const int totpecut[NPECUT] = {100,150,200};
//const int totpecut[NPECUT] = {60,70,100};

//
class mkplot {
public:
  mkplot(){};
  mkplot(char *output);
  ~mkplot();

  TObjArray *hcoll;

  TH1D *sumpe[NAREA][NMODE][NHITCUT];
  TH1D *enu_flux;
  TH1D *enu_int[NAREA][NMODE];
  TH1D *enu_obs[NAREA][NMODE][NHITCUT][NPECUT];

  void Init(char *output);
  void Merge();
  void Write();

  Int_t mode_index(int);

private:
  TFile *fout;

};

//
mkplot::mkplot(char *output)
{
  Init(output);
};

mkplot::~mkplot()
{
  Write();
};

void mkplot::Init(char *output)
{
  fout = new TFile(output,"recreate");

  Int_t nbins;
  Double_t xmin, xmax;
  TString name,var,conf;


  //
  hcoll = new TObjArray(0);

  // sumpe
  nbins=400;
  xmin=0;
  xmax=4000;

  for(int i=0; i<NAREA; i++) {
    for(int j=0; j<NMODE; j++) {
      for(int k=0; k<NHITCUT; k++) {
        name.Form("%s%s%sSUMPE%d",flav_name[0],area_name[i],mode_name[j],hitcut[k]);

        sumpe[i][j][k] = new TH1D(name.Data(),name.Data(),nbins,xmin,xmax);
        sumpe[i][j][k]->SetXTitle("Total p.e.");
        sumpe[i][j][k]->SetYTitle("# of events [/10^{21}POT/20p.e.]");
#ifdef ENABLESUMW2
        sumpe[i][j][k]->Sumw2();
#endif

        hcoll->Add(sumpe[i][j][k]);
      }
    }
  }

  // energy spectrum
  nbins=400;
  xmin=0;
  xmax=20;

  // Flux
  enu_flux = new TH1D("NMFLUXE","NMFLUXE",nbins,xmin,xmax);
  enu_flux->SetXTitle("Neutrino Energy [GeV]");
  enu_flux->SetYTitle("Flux [/cm^2/10^{21}POT]");
  hcoll->Add(enu_flux);

  // Interaction
  for(int i=0; i<NAREA; i++) {
    for(int j=0; j<NMODE; j++) {
      name.Form("%s%s%sE%s",flav_name[0],area_name[i],mode_name[j],"GEN");

      enu_int[i][j] = new TH1D(name.Data(),name.Data(),nbins,xmin,xmax);
#ifdef ENABLESUMW2
      enu_int[i][j]->Sumw2();
#endif
      enu_int[i][j]->SetXTitle("Neutrino Energy [GeV]");
      enu_int[i][j]->SetYTitle("# of interactions [/10^{21}POT]");

      hcoll->Add(enu_int[i][j]);
    }
  }

  // Observation (sumpe_pe2>=100)
  for(int i=0; i<NAREA; i++) {
    for(int j=0; j<NMODE; j++) {
      for(int n=0; n<NHITCUT; n++) {
        for(int c=0; c<NPECUT; c++) {

          name.Form("%s%s%sE%dPE%d",
            flav_name[0],area_name[i],mode_name[j],totpecut[c],hitcut[n]);

          enu_obs[i][j][n][c] = new TH1D(name.Data(),name.Data(),nbins,xmin,xmax);
#ifdef ENABLESUMW2
          enu_obs[i][j][n][c]->Sumw2();
#endif
          enu_obs[i][j][n][c]->SetXTitle("Neutrino Energy [GeV]");
          enu_obs[i][j][n][c]->SetYTitle("# of events [/10^{21}POT]");

          hcoll->Add(enu_obs[i][j][n][c]);
        }
      }
    }
  }
 
};

void mkplot::Merge()
{
  // FV or OV, mode sum
  for(int a=1; a<NAREA; a++) {

    // sum cc mode
    for(int m=(CC_MODE_ID+1); m<NC_MODE_ID; m++) {
      enu_int[a][CC_MODE_ID]->Add(enu_int[a][m]);

      for(int h=0; h<NHITCUT; h++) {
        sumpe[a][CC_MODE_ID][h]->Add(sumpe[a][m][h]);

        for(int t=0; t<NPECUT; t++) {
          enu_obs[a][CC_MODE_ID][h][t]->Add(enu_obs[a][m][h][t]);
        }
      }
    }

    // sum nc mode
    for(int m=(NC_MODE_ID+1); m<NMODE; m++) {
      enu_int[a][NC_MODE_ID]->Add(enu_int[a][m]);

      for(int h=0; h<NHITCUT; h++) {
        sumpe[a][NC_MODE_ID][h]->Add(sumpe[a][m][h]);

        for(int t=0; t<NPECUT; t++) {
          enu_obs[a][NC_MODE_ID][h][t]->Add(enu_obs[a][m][h][t]);
        }
      }
    }

    // sum all mode
    enu_int[a][0]->Add(enu_int[a][CC_MODE_ID], enu_int[a][NC_MODE_ID]);
    for(int h=0; h<NHITCUT; h++) {
      sumpe[a][0][h]->Add(sumpe[a][CC_MODE_ID][h], sumpe[a][NC_MODE_ID][h]);

      for(int t=0; t<NPECUT; t++) {
        enu_obs[a][0][h][t]->Add(enu_obs[a][CC_MODE_ID][h][t], enu_obs[a][NC_MODE_ID][h][t]);
      }
    }

  } // end of area loop

  // AREA=TANK, each mode
  for(int m=0; m<NMODE; m++) {
    enu_int[0][m]->Add(enu_int[1][m], enu_int[2][m]);

    for(int h=0; h<NHITCUT; h++) {
      sumpe[0][m][h]->Add(sumpe[1][m][h], sumpe[2][m][h]);

      for(int t=0; t<NPECUT; t++) {
        enu_obs[0][m][h][t]->Add(enu_obs[1][m][h][t], enu_obs[2][m][h][t]);
      }
    }
  }

};

void mkplot::Write()
{
  fout->cd();
  hcoll->Write();
  fout->Close();
};

Int_t mkplot::mode_index(int mode)
{
  Int_t mode_index = 0;

  if(TMath::Abs(mode)<30) {

    mode_index = 2;

    switch(mode) {
      case 1: 
        mode_index+=0; break;
      case 11:
      case 12:
      case 13:
        mode_index+=1; break;
      case 16:
        mode_index+=2; break;
      default:
        mode_index+=3;
    }
  }
  else if(TMath::Abs(mode)>30) {
    mode_index = 7;

    switch(mode) {
      case 31: 
      case 32: 
      case 33: 
      case 34: 
        mode_index+=0; break;
      case 36:
        mode_index+=1; break;
      default:
        mode_index+=2;
    }
  }

  return mode_index;
 
};
