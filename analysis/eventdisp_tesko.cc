#include <iostream>

#include <TH1.h>
#include <TH2.h>
#include <TVector3.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TROOT.h>

#include "MC.h"
#include "UPK.h"
#include "T2KWCEvtDisp.h"

#define BATCH
const int pe_threshold = 2;

bool draw_select(UPK *upk)
{
  if(upk->upk_runmode==1) {
//    std::cout << "# of hits : " << upk->upk_nhits << std::endl;

//    if(upk->GONG()==((upk->TRGNum()&0xffff)+1)) 
    if(upk->upk_nhits > 0) 
      return true; 
  }
  return false;
  
};

//
int main(int argc,char *argv[])
{

#ifndef BATCH 
  TROOT root("GUI","GUI");
  TApplication theApp("App",0,0);
#endif

  char tree_name[256] = "tesko";
  bool is_mc = false;
  bool is_beam = false;
  bool draw_flag = false;
  bool print_flag = false;

	int c=-1;
	while ((c = getopt(argc, argv, "pt:MB:h")) != -1) {
    switch(c){
			case 'p':
        print_flag = true;
				break;
      case 't':
        sprintf(tree_name, "%s", optarg);
        break;
      case 'h':
        std::cout << "-p : print event display" << std::endl;
        std::cout << "-t : tree name " << std::endl;
        exit(1);
			default:
				exit(1);
		}
	}
	argc-=optind;
	argv+=optind;

  //
  if( strcmp(tree_name, "tesko") == 0 ) {
    std::cerr << "Need to set input tree name!!" << std::endl;
    std::cerr << "-t <tree name>" << std::endl;
    exit(1);
  }

  //
  if( strcmp(tree_name, "upk") == 0 ) {
    std::cout << "input beam tree"<< std::endl;
    is_beam = true;
  }
  else {
    std::cout << "input mc tree ?" << std::endl;
    std::cout << tree_name << std::endl;
    is_mc = true;
  }
 
  TCanvas *canvas = new TCanvas("canvas","canvas",1350,600);
  T2KWCEvtDisp *disp = new T2KWCEvtDisp();
  disp->Init( canvas );

  TChain *fChain = new TChain(tree_name,tree_name);
  for (int i=0; i<argc; i++) {
    fChain->Add(argv[i]);
  }

  if( fChain && fChain->GetNtrees()==0 ) {
    std::cout << "No file added" << std::endl;
    exit(1);
  }
  Long64_t nevt =fChain -> GetEntries();
  std::cout << "Total # of events : " << nevt <<std::endl;
  if( nevt==0 ) exit(1);


	// set branch address
  MC *mc = NULL;
  UPK *upk = NULL;

  if(is_mc) {
    mc = new MC( fChain );
  }
  else if(is_beam) {
    upk = new UPK( fChain );
  }


  // start 
  Long64_t fN=0;
  TString buff;
  while( 1 ) {

    //
//    if(is_mc) evt->Clear();
    disp->Clear();
    draw_flag = false;

#ifndef BATCH
    std::cout << "\n\nSelect Entry# (select minu# to end):";
    std::cin >> fN;
#endif

    if( fN>=nevt ) {
      std::cout << "Too large entry#" << std::endl;
      break;
    }
    else if ( fN<0 ) {
      std::cout << "End!" << std::endl;
      break;
    }

    if(is_mc) mc->GetEntry( fN );
    else if(is_beam) upk->GetEntry( fN );
      
    if(is_mc) {
      disp->InputData( mc );
      draw_flag = true;
    }
    else if(is_beam) {
      if(draw_select(upk)) {
        disp->InputData( upk );
        draw_flag = true;
      }
    }

    if( draw_flag ) {
      disp->Draw( canvas );
      if( print_flag==true ) 
        canvas->SaveAs(Form("evtdisp_ss_%ld.pdf",(long int)fN));
    }
 
#ifdef BATCH
    fN++;
#endif

	} // end of loop

#ifndef BATCH
  theApp.Run();
#endif

}
