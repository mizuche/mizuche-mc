#include <iostream>
#include <assert.h>

#include <TH1.h>
#include <TH2.h>
#include <TVector3.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TMath.h>
#include <TROOT.h>

#include "T2KWCData.h"

////////////////////////////////////////////////////////////////
const Double_t water_mollmass = 18.02;   // [g/mol]
const Double_t water_atom     = 18;      // atomic number
const Double_t water_dens     = 1.;      // [g/cm^3]
const Double_t abogadro       = 6.02e23; // abogadro constant

const Double_t tank_r    = 70;  // [cm]
const Double_t tank_len  = 160; // [cm]
const Double_t surf_Mizu = TMath::Pi() * tank_r * tank_r;  // [cm^2]

const Double_t total_wwater = 
    surf_Mizu * tank_len * water_dens; //[g]

const Double_t totcrsne_unit = 1.e-38; // [cm^2]

const Double_t atom_wwater = 
    (total_wwater/water_mollmass) * water_atom * abogadro; // [atom]
////////////////////////////////////////////////////////////////


//////////////////////////////////////////
typedef struct {
    TTree *tree;
    T2KWCEvent *evt;
    Int_t nfiles;
    Double_t anorm;
    Int_t maxpe;
    Int_t minpe[5];
    Int_t nhits[5];
    Int_t sumpe[5];
} AnaMC;
///////////////////////////////////////


// __________________________________________________
Double_t flux_norm( T2KWCNeutInfo *neutinfo, int nfiles )
{
    // The neutrino out of Mizuche surface is rejected.
    // flux unit is converted to [/file/cm^3]
    return ( neutinfo->Norm() / nfiles / surf_Mizu );
};

// __________________________________________________
Double_t int_norm( T2KWCNeutInfo *neutinfo )
{
    // Total cross-section unit in NEUT file is 10^-38 cm^2
    return ( neutinfo->Totcrsne() * totcrsne_unit * atom_wwater );
};

// __________________________________________________
void do_analysis( AnaMC &a )
{
    // neutrino interaction info.
    T2KWCNeutInfo *neutinfo = (T2KWCNeutInfo*)( a.evt->GetNeutInfo(0) );
    assert(neutinfo);

    a.anorm = flux_norm(neutinfo, a.nfiles) * int_norm( neutinfo );

    // PMT hit analysis
    Int_t maxpe = 0;
    const Int_t xxx = 5;
    Int_t sumpe[xxx] = {0,0,0,0,0};
    Int_t nhits[xxx] = {0,0,0,0,0};
    Int_t pe_threshold[xxx] = {2,3,4,5,6};

    for( int i=0; i<(a.evt->NPMTs()); i++ ) {
        T2KWCPMTHit *pmthit = (T2KWCPMTHit*)( a.evt->GetPMTHit(i) );
        assert(pmthit);

        Int_t npe = pmthit->NPE();

        for(int j=0; j<xxx; j++) {
            if( npe >= pe_threshold[j] ) {
                sumpe[j] += npe;
                nhits[j]++;
            }
        }
    
        if( maxpe < npe ) maxpe = npe;
    }

    for(int i=0; i<xxx; i++) {
        a.minpe[i] = pe_threshold[i];
        a.sumpe[i] = sumpe[i];
        a.nhits[i] = nhits[i];
    }
    a.maxpe = maxpe;

};

// __________________________________________________
void Init_Tree( AnaMC &a )
{
    a.tree = new TTree("tree","tree");
    a.tree->SetMaxTreeSize(500000000);

    a.evt = new T2KWCEvent();
    a.tree->Branch("mc.","T2KWCEvent",&a.evt,64000,2);

    a.tree->Branch("nfiles",&a.nfiles,"nfiles/I");
    a.tree->Branch("anorm",&a.anorm,"anorm/D");
    a.tree->Branch("maxpe",&a.maxpe,"maxpe/I");
    a.tree->Branch("minpe",a.minpe,"minpe[5]/I");
    a.tree->Branch("nhits",a.nhits,"nhits[5]/I");
    a.tree->Branch("sumpe",a.sumpe,"sumpe[5]/I");

};

// __________________________________________________
int main(int argc,char *argv[])
{
    gROOT->Reset();

    bool debug_flag = false;
    char fout_name[300] = "fout_anamc.root";

	//TChain *fChain = new TChain("tree","tree");
    TChain *fChain = new TChain("mc","tree");
    AnaMC a;

	int c=-1;
	extern int optind;
	extern char *optarg;
	while ((c = getopt(argc, argv, "o:d")) != -1) {
        switch(c){
        case 'o':
            sprintf(fout_name, "%s", optarg);
            break;
        case 'd':
            debug_flag = true;
            break;
        case 'h':
        default:
            std::cout << "-o : output file name" << std::endl;
            std::cout << "-d : for debug run" << std::endl;
            exit(1);
		}
	}
	argc-=optind;
	argv+=optind;
  
    // chain trees for analysis
	for (int i=0; i<argc; i++) {
        std::cout << "input files : " << argv[i] << std::endl;
		fChain->Add(argv[i]);
	}

    a.nfiles = fChain->GetNtrees();
    std::cout << "Add File : " << a.nfiles << std::endl;
    if( a.nfiles==0 ) {
        std::cout << "No file added" << std::endl;
        exit(1);
    }

    //
    TFile *fout = new TFile(fout_name,"recreate");
    Init_Tree( a );

	// set branch address
    TBranch* EvtBr = fChain->GetBranch("mc.");
    a.evt = new T2KWCEvent();
    EvtBr -> SetAddress(&a.evt);
    fChain ->SetBranchAddress("mc.", &a.evt);
    Long64_t nevt =fChain -> GetEntries();
    std::cout << "Total # of events : " << nevt <<std::endl;
    if( nevt==0 ) exit(1);

    // start event loop
    Long64_t fN=0;

    while( fChain->GetEntry(fN) ) {

        //
        if( fN%10000==0 ) 
            std::cout << "processed : " << fN << std::endl;

        if( debug_flag && fN>10000 ) break;
  
        //
        do_analysis( a );
        a.tree->Fill();

        //
        a.evt->Clear();
        fN++;

	} // end of loop

    // output hist
    fout->cd();
    a.tree->Write();
    fout->Close();

}
