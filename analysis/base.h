// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id$ 
// Author: Shota TAKAHASHI <shotakaha@gmail.com>
// Update: 2012-10-14 23:42:25+0900
// Copyright: 2012 (C) Shota TAKAHASHI
//

#ifndef ROOT_BASE
#define ROOT_BASE

#ifndef ROOT_TObject
#include "TObject.h"
#endif

#include "logger.h"

class Base : public TObject, public Logger
{
private:
    char fDataDir[100];
    char fUpkDir[100];
    char fHistDir[100];
    char fPlotDir[100];
    Bool_t fOverwriteFlag;
    
public:
    Base();
    virtual ~Base();

    const char *GetDataDir() { return fDataDir; }
    const char *GetUpkDir()  { return fUpkDir; }
    const char *GetHistDir() { return fHistDir; }
    const char *GetPlotDir() { return fPlotDir; }

    Int_t SetDataDir(const char* dir, const char* ver);
    Int_t SetUpkDir(const char* dir, const char* ver);
    Int_t SetHistDir(const char* dir, const char* ver);
    Int_t SetPlotDir(const char* dir, const char* ver);

    Int_t PathExists(const char* ifn, const char* mode);
    Int_t ShowProgress(const Int_t &i, const Int_t &N, const Int_t &L);
    Int_t AskContinue();
    Int_t AskOverwrite(const char* ofn);
    Int_t PrintLog(const char* level, const char* format, ...);
    Int_t ROOTLogon();
    
    
    Bool_t SetOverwriteAll(Bool_t overwrite)
        { return fOverwriteFlag = overwrite; }
    Bool_t IsOverwrite() { return fOverwriteFlag; }
    

    Int_t ConvertFadcNsec(const Int_t &isamp);
    Double_t ConvertFadcVolt(const Int_t &adc);
    Double_t ConvertFadcCharge(const Double_t &adcsum);
    Double_t ConvertFadcPe(const Double_t &adcsum);

    //ClassDef(Base,0) //DOCUMENT ME
};

// __________________________________________________
Base::Base()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif    
    ROOTLogon();
    SetOverwriteAll(kFALSE);
}

// __________________________________________________
Base::~Base()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif    
}

// __________________________________________________
Int_t Base::SetDataDir(const char* dir, const char* ver)
{
    sprintf(fDataDir, "%s%s", dir, ver);
#ifdef DEBUG
    LogD("SetDataDir('%s')", GetDataDir());
#endif
    return 0;
}

// __________________________________________________
Int_t Base::SetUpkDir(const char* dir, const char* ver)
{
    sprintf(fUpkDir, "%s%s", dir, ver);
#ifdef DEBUG
    LogD("SetUpkDir('%s')", GetUpkDir());
#endif    
    return 0;
}

// __________________________________________________
Int_t Base::SetHistDir(const char* dir, const char* ver)
{
    sprintf(fHistDir, "%s%s", dir, ver);
#ifdef DEBUG
    LogD("SetHistDir('%s')", GetHistDir());
#endif        
    return 0;
}

// __________________________________________________
Int_t Base::SetPlotDir(const char* dir, const char* ver)
{
    sprintf(fPlotDir, "%s%s", dir, ver);
#ifdef DEBUG
    LogD("SetPlotDir('%s')", GetPlotDir());
#endif
    return 0;
}

// __________________________________________________
Int_t Base::ROOTLogon()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif    
    // _____ Canvas & Frame Setting _____
    gROOT->SetStyle("Modern");
    gStyle->SetCanvasDefW(500);
    gStyle->SetCanvasDefH(500);
    
    // _____ Axis & Grid Setting _____
    //  gStyle->SetOptLogx(1);    // 0:off, 1:on
    //  gStyle->SetOptLogy(1);    // 0:off, 1:on
    //  gStyle->SetOptLogz(1);    // 0:off, 1:on
    //  gStyle->SetLabelFont(fontid, "XYZ");
    gStyle->SetStripDecimals(kFALSE);
    gStyle->SetTimeOffset(-788918400);    // Time difference btw ROOT and Epoch
    //  gStyle->SetPadBorderSize(0);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetPadGridX(1);
    gStyle->SetPadGridY(1);

    // _____ Statistics Box Setting _____
    gStyle->SetOptStat(11112211);
    //  gStyle->SetStatFont(fontid);
     
    // _____ Graph & Axis Title Setting _____
    gStyle->SetTitleAlign(22);
    //  gStyle->SetTitleBorderSize(0);
    gStyle->SetTitleX(0.5);
    gStyle->SetTitleY(0.95);
    //  gStyle->SetTitleFont(fontid, "XYZ");
    //  gStyle->SetTitleFont(fontid, "");
    gStyle->SetTitleOffset(1.2, "XYZ");

    // _____ Histogram Setting _____
    gStyle->SetHistFillStyle(3004);
    gStyle->SetHistLineWidth(2);
    //     gStyle->SetHistLineColor();
    gStyle->SetHistTopMargin(0.2);


    // _____ Graph Setting _____
    gStyle->SetMarkerStyle(8);
    //     gStyle->SetMarkerSize(4);

    // _____ Function Setting _____
    //     gStyle->SetFuncColor(
    //     gStyle->SetFuncStyle(
    gStyle->SetFuncWidth(2);

    // _____ Text Setting _____
    //  gStyle->SetTextFont(fontid);


    // _____ Legend Setting _____
    gStyle->SetLegendBorderSize(1);
    gStyle->SetLegendFillColor(0);

    // _____ Color Pallete Setting _____
    gStyle->SetPalette(1);     
    gROOT->GetColor(3)->SetRGB(0., 0.7, 0.); // Green  (0, 1, 0)->(0, 0.7, 0)
    gROOT->GetColor(5)->SetRGB(1., 0.5, 0.); // Yellow (1, 1, 0)->(1, 0.5, 0)
    gROOT->GetColor(7)->SetRGB(0.6, 0.3, 0.7); // Cyan (0, 1, 1)->(0, 0.5, 0)

    return 0;
}

// __________________________________________________
Int_t Base::AskContinue()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif
    fprintf(stderr, "Press [N]ext, [P]revious, [U]pdate, [Q]uit > ");
    int answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    
    int r = 0;
    if (answer == 'q' || answer == 'Q') {
        fprintf(stderr, "\tQuit. Bye Bye\n");
        exit(-1);
    }
    else if (answer == '.') {
        fprintf(stderr, "\tQuit loop.\n");
        return -1;
    }
    else if (answer == 'p' || answer == 'P') r = -2;
    else if (answer == 'u' || answer == 'U') gSystem->ProcessEvents();
    else if (answer == 's' || answer == 'S') r = 0;
    return r;
}

// __________________________________________________
Int_t Base::AskOverwrite(const char* ofn)
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif            
    fprintf(stderr, "Do you want to ovewrite? [y/n/q] > ");
    int answer, readch;
    readch = getchar();
    answer = readch;
    while (readch != '\n' && readch != EOF) readch = getchar();
    int r = 0;
    if (answer == 'q' || answer == 'Q') {
        fprintf(stderr, "\tQuit. Bye Bye\n");
        exit(-1);
    }
    else if (answer == 'y' || answer == 'Y') {
        fprintf(stderr, "\tOverwriting '%s'.\n", ofn);
    }
    else if (answer == '.') {
        fprintf(stderr, "\tQuit loop.\n");
        return -1;
    }
    else {
        fprintf(stderr, "\tSkipped '%s'.\n", ofn);
        return -1;
    }
    return r;
}

// __________________________________________________
Int_t Base::PathExists(const char* fn, const char* mode = "r")
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif        
    // if mode = 0; check input filename path
    // if mode = 1; check output filename path
    TString m = mode;
    m.ToLower();
    FileStat_t info;
    Int_t path = gSystem->GetPathInfo(fn, info);
    // returns 0 if file exist, and 1 if not.
    if (path !=0 && m == "r") fprintf(stderr, "Error:\tFile '%s' does not exist.\n", fn);
    if (path ==0 && m == "w") fprintf(stderr, "Error:\tFile '%s' already exist.\n", fn);
    return path;
}



// __________________________________________________
Int_t Base::ShowProgress(const Int_t &i, const Int_t &N, const Int_t &L)
{
    if (i == 0 ) fprintf(stderr, "Progress:\t");
    if (i % L == 0) fprintf(stderr, "*");
    if (i == N-1) fprintf(stderr, "\n");
    return 0;
}
    
// __________________________________________________
Int_t Base::ConvertFadcNsec(const Int_t &isamp)
{
    // Using 250MS/s FADC
    // return value in nanosec;
    //const Double_t samprate = 250.;  // MHz sampling
    Int_t nsec = isamp * 4;
    return nsec;
}

// __________________________________________________
Double_t Base::ConvertFadcVolt(const Int_t &adc)
{
    const Double_t adc2mV = 0.489;
    Double_t mv = (Double_t)adc * adc2mV;
    return mv;
}

// __________________________________________________
Double_t Base::ConvertFadcCharge(const Double_t &adcsum)
{
    const Double_t adc2mv = 0.489;    // [mV/count]
    //const Double_t samprate = 250.;   // [MHz]
    const Double_t samp2ns = 4.;       // [ns/samp.point]
    const Double_t zimp   = 50.;      // [ohm]
    Double_t pc = adcsum * adc2mv * samp2ns / zimp;
    return pc;
}

// __________________________________________________
Double_t Base::ConvertFadcPe(const Double_t &adcsum)
{
    const Double_t echarge = 1.6e-19;
    Double_t pc = ConvertFadcCharge(adcsum);
    Double_t pe = pc / echarge * 1e-12;
    return pe;
}

#endif
//____________________________________________________________________ 
//  
// EOF
//

