#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import time
import datetime

from optparse import OptionParser
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gROOT, gClient, gStyle
from ROOT import TFile, TTree, TChain
from ROOT import TH1D, TH2D
from ROOT import TGraph, TMultiGraph
from ROOT import TCanvas

#gStyle.SetNdivisions(520)

## __________________________________________________
class Config(object):
    ClassName = 'HvLog'
    TreeName = 'hvlog'
    BranchDescriptor = 'utime/I:name/C:pmt/I:win/I:atm/I:mask/I:write/I:read/I:gain/D:diff/I'
    ## range
    Flow = (0, 5.5, 55)
    Temp = (17, 22, 50)
    Nch  = 180  ## maximum window number
    Duration = 3600
    dispw = gClient.GetDisplayWidth()
    disph = gClient.GetDisplayHeight()
    Disp  = (dispw, disph)
    Timeformat = '#splitline{%m/%d}{%H:%M}'
    PlotDir = 'data/plots'

## __________________________________________________
class HvLog(object):
    '''
    Create ROOT file from rlog/HVLOG.txt.
    Draw plots rootfile/HVLOG.root
    '''
    
    ## __________________________________________________
    def __init__(self):
        self.chain = TChain(Config.TreeName)
        self.canvases = []
        
    ## __________________________________________________
    def __str__(self):
        '''
        print information of files added to TChain.
        '''
        ch = self.chain
        s = '[print]\tTreeName : {0}\n'.format(ch.GetName())
        s += '[print]\tNtrees   : {0}\n'.format(ch.GetNtrees())
        s += '[print]\tEntries  : {0}\n'.format(ch.GetEntries())
        return s

    ## __________________________________________________
    def init_th1d(self, name, title, xtuple, nloop, timedisplay=0):
        '''
        Initializer for N TH1D histgrams.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        xmin, xmax, xbin = xtuple
        hists = []
        for i in range(nloop):
            color = i % 8 + 2
            h = TH1D(name % i, title % i, xbin, xmin, xmax)
            h.SetStats(0)
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            h.SetFillColor(color)
            h.GetXaxis().SetTimeFormat(Config.Timeformat)
            h.GetXaxis().SetTimeOffset(0, 'gmt')
            h.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                h.GetXaxis().SetLabelOffset(0.015)
            hists.append(h)
        return hists

    ## __________________________________________________
    def init_th2d(self, name, title, xtuple, ytuple, nloop, timedisplay=0):
        '''
        Initializer for N TH2D histgrams.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        xmin, xmax, xbin = xtuple
        ymin, ymax, ybin = ytuple
        hists = []
        for i in range(nloop):
            color = i % 8 + 2
            h = TH2D(name % i, title % i, xbin, xmin, xmax, ybin, ymin, ymax)
            h.SetStats(0)
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            h.SetFillColor(color)
            h.GetXaxis().SetTimeFormat(Config.Timeformat)
            h.GetXaxis().SetTimeOffset(0, 'gmt')
            h.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                h.GetXaxis().SetLabelOffset(0.015)
            hists.append(h)
        return hists

    ## __________________________________________________
    def init_tgraph(self, name, title, nloop, timedisplay=0):
        '''
        Initializer for N TGraph graphs.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        graphs = []
        for i in range(nloop):
            color = i % 8 + 2
            g = TGraph(0)
            g.SetName(name % i)
            g.SetTitle(title % i)
            g.SetLineColor(color)
            g.SetMarkerColor(color)
            g.SetFillStyle(0)
            g.GetXaxis().SetTimeFormat(Config.Timeformat)
            g.GetXaxis().SetTimeOffset(0, 'gmt')
            g.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                g.GetXaxis().SetLabelOffset(0.015)
            graphs.append(g)
        return graphs
    
    ## __________________________________________________
    def read(self, ifn):
        '''
        if input file is a CSV format : Convert it ROOT file.
        if input file is a ROOT format : Add TTree to TChain
        '''
        if not os.path.exists(ifn):
            sys.stderr.write('Error: "%s" does not exist.\n' % ifn)
            sys.exit(-1)

        head, tail = os.path.split(ifn)
        root, ext = os.path.splitext(tail)
        size = os.path.getsize(ifn) / 1e6
        start = datetime.datetime.now()
        if ext in ['.csv', '.CSV']:
            sys.stdout.write('Check\t: File "{0} ({1:5.2f} MB)" is a CSV file.\n'.format(ifn, size))
            raise NotImplemented()
            self.create_rootfile(ifn)
        elif ext in ['.root']:
            sys.stdout.write('Check\t: File "{0} ({1:5.2f} MB)" is a ROOT file.\n'.format(ifn, size))
            self.read_rootfile(ifn)
        end = datetime.datetime.now()            
        sys.stdout.write('[read]\tDone : {0} [sec]\n'.format(end - start))
        return
    
    ## __________________________________________________
    def create_rootfile(self, ifn):
        '''
        input filename : data/yymmdd/yymmdd-hhmmss_UG.CSV
        output filename : data/rootfile/yymmdd-hhmmss.root

        Read CSV file and create ROOT file.
        If output ROOT file exists, skip conversion.
        '''
        head, tail = os.path.split(ifn)
        ofn = os.path.join('data/rootfile', tail.lower().replace('_ug.csv', '.root'))

        if os.path.exists(ofn):
            sys.stderr.write('Skip\t: "%s" already exist.\n' % ofn)
            return 'Skip conversion'
        
        sys.stdout.write('Read\t: "{0}"\n'.format(ifn))
        with open(ifn, 'r') as fin:
            lines = fin.readlines()

        with open('temp.txt', 'w') as fout:
            for line in lines[34:]:
                line  = line.rstrip().split(',')
                date  = time.strptime(line[1], '%Y/%m/%d %H:%M:%S')
                utime = int(time.mktime(date))
                ch11  = line[12]
                ch12  = line[13]
                ch19  = line[20] if not len(line) == 19 else line[14]
                ch20  = line[21] if not len(line) == 19 else line[15]
                fout.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(utime, ch11, ch12, ch19, ch20))
                #sys.stdout.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(utime, ch11, ch12, ch19, ch20))
                
        tname = Config.TreeName
        bd = Config.BranchDescriptor
        t = TTree(tname, tail)
        t.ReadFile('temp.txt', bd)
        sys.stdout.write('TTree\t: "%s", "%s"\n' % (tail, bd))
        sys.stdout.write('Write\t: "%s"\n' % ofn)
        fout = TFile(ofn, 'recreate')
        t.Write()
        fout.Close()
        os.remove('temp.txt')
        return 'Conversion Finished'

    ## __________________________________________________
    def read_rootfile(self, ifn):
        '''
        Read ROOT file with TChain
        When reading a file:
        1. Check if the input file exists.
        2. Check if it is a ROOT file, (*.root).
        3. Assume plot-saving directory and check if it exists.
        4. Open ROOT file and hold it inside.
        '''
        ### Open ROOT file and get TTree
        sys.stdout.write('Read\t: %s\n' % ifn)
        self.chain.Add(ifn)
        return self.chain

    ## __________________________________________________
    def process(self):
        ch = self.chain
        nentries = ch.GetEntries()
        start = datetime.datetime.now()
        if not nentries == 0:
            self.process_rootfile()
        else:
            sys.exit(-1)
        end = datetime.datetime.now()            
        sys.stdout.write('[process]\tDone : {0} [sec]\n'.format(end - start))
        return

    ## __________________________________________________
    def get_utime(self, chain):
        nentries = chain.GetEntries()
        chain.GetEntry(0)
        s_utime = e_utime = chain.utime
        for ientry in range(nentries):
            chain.GetEntry(ientry)
            t = chain.utime
            s_utime = min(s_utime, t)
            e_utime = max(e_utime, t)
            
        s_date = '{0:%Y%m%d}'.format(datetime.datetime.fromtimestamp(s_utime))
        e_date = '{0:%Y%m%d}'.format(datetime.datetime.fromtimestamp(e_utime))
        self.prefix = '{0}_{1}'.format(s_date, e_date)
        print 'Start : {0} ==> {1}'.format(s_utime, s_date)
        print 'End   : {0} ==> {1}'.format(e_utime, e_date)
        return s_utime, e_utime
            
    ## __________________________________________________
    def process_rootfile(self):
        ch = self.chain
        nentries = ch.GetEntries()

        ### get start / end of unixtime
        s_time, e_time = self.get_utime(ch)
        interval = Config.Duration
        s_time = (s_time / interval) * interval
        e_time = (e_time / interval + 1 ) * interval
        n_time = (e_time - s_time) / interval
        time = (s_time, e_time, n_time)
        
        # ### Flow
        # hname = 'hFlow%d'
        # htitle = 'hFlow%d;Date/Time;Flow [V]'
        # hFlows = self.init_th2d(hname, htitle, time, Config.Flow, Config.Nch, timedisplay=1)
        # ### Temperature
        # ymin, ymax, ybin = Config.Temp
        # hname = 'hTemp%d'
        # htitle = 'hTemp%d;Date/Time;Temprature [degC]' 
        # hTemps = self.init_th2d(hname, htitle, time, Config.Temp, Config.Nch, timedisplay=1)

        # ch.Project(hFlows[0].GetName(), 'ch11:utime')
        # ch.Project(hFlows[1].GetName(), 'ch12:utime')
        # ch.Project(hTemps[0].GetName(), 'ch19:utime')
        # ch.Project(hTemps[1].GetName(), 'ch20:utime')

        ### Write HV
        gname = 'gWrite%04d'
        gtitle = 'gWrite%04d;Date/Time;Write HV [V]'
        gWrites = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)
        ### Read HV
        gname = 'gRead%04d'
        gtitle = 'gRead%04d;Date/Time;Read HV [V]'
        gReads = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)
        ### Diff HV
        gname = 'gDiff%04d'
        gtitle = 'gDiff%04d;Date/Time;Diff HV [V]'
        gDiffs = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)
        ### Gain
        gname = 'gGain%04d'
        gtitle = 'gGain%04d;Date/Time;Gain'
        gGains = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)
        ### Mask
        gname = 'gMask%04d'
        gtitle = 'gMask%04d;Date/Time;Mask'
        gMasks = self.init_tgraph(gname, gtitle, Config.Nch, timedisplay=1)

        for ientry in range(nentries):
            ch.GetEntry(ientry)
            win = ch.win
            if win > 0:
                utime = ch.utime    # unixtime
                pmt = ch.pmt
                atm = ch.atm
                mask = ch.mask
                write = ch.write
                read = ch.read
                gain = (ch.gain / 1e6 - 1.5) / 1.5
                diff = ch.diff

                gtitle = 'WIN:{0:4d} PMT:{1:4d} ATM:{2:4d}'.format(win, pmt, atm)
                g1 = gWrites[win]
                g2 = gReads[win]
                g3 = gDiffs[win]
                g4 = gGains[win]
                g5 = gMasks[win]

                g1.SetTitle('{0};Date;Write HV [V]'.format(gtitle))
                g2.SetTitle('{0};Date;Read HV [V]'.format(gtitle))
                g3.SetTitle('{0};Date;Diff HV [V]'.format(gtitle))
                g4.SetTitle('{0};Date;Gain'.format(gtitle))
                g5.SetTitle('{0};Date;Mask'.format(gtitle))

                n1 = g1.GetN()
                n2 = g2.GetN()
                n3 = g3.GetN()
                n4 = g4.GetN()
                n5 = g5.GetN()
                
                g5.SetPoint(n5, utime, mask)
                if mask == 0:
                    g1.SetPoint(n1, utime, write)
                    g2.SetPoint(n2, utime, read)
                    g3.SetPoint(n3, utime, diff)
                    g4.SetPoint(n4, utime, gain)

        ### Write HV
        self.gWrites = gWrites
        self.gReads = gReads
        self.gDiffs = gDiffs
        self.gGains = gGains
        self.gMasks = gMasks

        return

    ## __________________________________________________
    def draw(self):
        #c1 = self.draw_hist()
        c2 = self.draw_graph()
        self.stop()

    ## __________________________________________________
    def draw_hist(self):
        hTemps = self.hTemps
        hFlows = self.hFlows
        
        wx, wy = Config.Disp
        cname = 'cHvLog{0}'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'HvLog{0}'.format(self.prefix))
        c = TCanvas(cname, title, wx, wy)
        c.Divide(1, 2)
        c.cd(1)
        for i in range(len(hTemps)):
            gopt = 'box' if i == 0 else 'boxsame'
            hTemps[i].Draw(gopt)
        c.cd(2)
        for i in range(len(hFlows)):
            gopt = 'box' if i == 0 else 'boxsame'
            hFlows[i].Draw(gopt)
        c.Update()
        self.stop()

        self.canvases += c
            
        return c

    ## __________________________________________________
    def draw_graph_win(self, win):
        iwin = int(win)
        gWrite = self.gWrites[iwin]
        gRead = self.gReads[iwin]
        gDiff = self.gDiffs[iwin]
        gGain = self.gGains[iwin]
        gMask = self.gMasks[iwin]

        # graphs = [gWrite, gRead, gDiff, gGain, gMask]
        # for i, g in enumerate(graphs):
        #     color = i + 2
        #     g.SetLineColor(color)
        #     g.SetMarkerColor(color)
        #     g.SetFillColor(color)
            
        wx, wy = Config.Disp
        cname = 'cHvLog{0}_win{1:04d}_write'.format(self.prefix, iwin)
        ctitle = os.path.join(Config.PlotDir, cname)
        c1 = TCanvas(cname, ctitle, wx, wy)
        gWrite.Draw('apl')
        gWrite.GetXaxis().SetTimeDisplay(1)
        gWrite.GetXaxis().SetTimeOffset(0, 'gmt')
        gWrite.GetXaxis().SetTimeFormat(Config.Timeformat)
        
        cname = 'cHvLog{0}_win{1:04d}_read'.format(self.prefix, iwin)
        ctitle = os.path.join(Config.PlotDir, cname)
        c2 = TCanvas(cname, ctitle, wx, wy)
        gRead.Draw('apl')
        c2.Update()
        gRead.GetXaxis().SetTimeDisplay(1)
        gRead.GetXaxis().SetTimeOffset(0, 'gmt')
        gRead.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_win{1:04d}_diff'.format(self.prefix, iwin)
        ctitle = os.path.join(Config.PlotDir, cname)
        c3 = TCanvas(cname, ctitle, wx, wy)
        gDiff.Draw('apl')
        c3.Update()
        gDiff.GetXaxis().SetTimeDisplay(1)
        gDiff.GetXaxis().SetTimeOffset(0, 'gmt')
        gDiff.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_win{1:04d}_gain'.format(self.prefix, iwin)
        ctitle = os.path.join(Config.PlotDir, cname)
        c4 = TCanvas(cname, ctitle, wx, wy)
        gGain.Draw('apl')
        c4.Update()
        gGain.GetXaxis().SetTimeDisplay(1)
        gGain.GetXaxis().SetTimeOffset(0, 'gmt')
        gGain.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_win{1:04d}_mask'.format(self.prefix, iwin)
        ctitle = os.path.join(Config.PlotDir, cname)
        c5 = TCanvas(cname, ctitle, wx, wy)
        gMask.Draw('apl')
        c5.Update()
        gMask.GetXaxis().SetTimeDisplay(1)
        gMask.GetXaxis().SetTimeOffset(0, 'gmt')
        gMask.GetXaxis().SetTimeFormat(Config.Timeformat)

        self.stop()
        print '{0}.png'.format(c1.GetTitle())
        print '{0}.png'.format(c2.GetTitle())
        print '{0}.png'.format(c3.GetTitle())
        print '{0}.png'.format(c4.GetTitle())
        print '{0}.png'.format(c5.GetTitle())
        self.stop()
        return

    ## __________________________________________________
    def draw_graph(self, save):
        gWrites = self.gWrites
        gReads = self.gReads
        gDiffs = self.gDiffs
        gGains = self.gGains
        gMasks = self.gMasks

        gtitle = self.prefix.replace('_', ' - ')
        mgWrite = TMultiGraph('mgWrite', 'Write HV {0};Date;Write HV [V]'.format(gtitle))
        for g in gWrites:
            if g.GetN() > 0:
                mgWrite.Add(g, 'pl')
        mgRead = TMultiGraph('mgRead', 'Read HV {0};Date;Read HV [V]'.format(gtitle))
        for g in gReads:
            if g.GetN() > 0:
                mgRead.Add(g, 'pl')
        mgDiff = TMultiGraph('mgDiff', 'Diff HV {0};Date;Diff HV [V]'.format(gtitle))
        for g in gDiffs:
            if g.GetN() > 0:
                mgDiff.Add(g, 'pl')
        mgMask = TMultiGraph('mgMask', 'Mask {0};Date;Mask'.format(gtitle))
        for g in gMasks:
            if g.GetN() > 0:
                mgMask.Add(g, 'pl')
        mgGain = TMultiGraph('mgGain', 'Gain {0};Date;Gain'.format(gtitle))
        for g in gGains:
            if g.GetN() > 0:
                mgGain.Add(g, 'pl')
        
        wx, wy = Config.Disp
        cname = 'cHvLog{0}_write'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, cname)
        c1 = TCanvas(cname, ctitle, wx, wy)
        mgWrite.Draw('apl')
        c1.Update()
        mgWrite.GetXaxis().SetTimeDisplay(1)
        mgWrite.GetXaxis().SetTimeOffset(0, 'gmt')
        mgWrite.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_read'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, cname)
        c2 = TCanvas(cname, ctitle, wx, wy)
        mgRead.Draw('apl')
        c2.Update()
        mgRead.GetXaxis().SetTimeDisplay(1)
        mgRead.GetXaxis().SetTimeOffset(0, 'gmt')
        mgRead.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_diff'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, cname)
        c3 = TCanvas(cname, ctitle, wx, wy)
        mgDiff.Draw('apl')
        c3.Update()
        mgDiff.GetXaxis().SetTimeDisplay(1)
        mgDiff.GetXaxis().SetTimeOffset(0, 'gmt')
        mgDiff.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_gain'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, cname)
        c4 = TCanvas(cname, ctitle, wx, wy)
        mgGain.Draw('apl')
        c4.Update()
        mgGain.GetXaxis().SetTimeDisplay(1)
        mgGain.GetXaxis().SetTimeOffset(0, 'gmt')
        mgGain.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cHvLog{0}_mask'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, cname)
        c5 = TCanvas(cname, ctitle, wx, wy)
        mgMask.Draw('apl')
        c5.Update()
        mgMask.GetXaxis().SetTimeDisplay(1)
        mgMask.GetXaxis().SetTimeOffset(0, 'gmt')
        mgMask.GetXaxis().SetTimeFormat(Config.Timeformat)

        self.stop()
        print '{0}.png'.format(c1.GetTitle())
        print '{0}.png'.format(c2.GetTitle())
        print '{0}.png'.format(c3.GetTitle())
        print '{0}.png'.format(c4.GetTitle())
        print '{0}.png'.format(c5.GetTitle())
        # c.SaveAs('{0}.png'.format(c.GetTitle()))
        self.stop()
        return

    ## __________________________________________________
    def stop(self):
        ans = raw_input('Press "q" or ".q" to Quit. > ')
        if ans in ['q', '.q']:
            sys.stderr.write('\t"%s" pressed. Bye Bye.\n' % ans)
            sys.exit(-1)
            
## __________________________________________________
if __name__ == '__main__':

    usage = '%prog [options] [text or ROOT files, ...]'
    usage += '\n'
    usage += '  Create ROOT file:\n'
    usage += '    Ex1) %prog data/130320/130320-132322_UG.CSV\n'
    usage += '    Ex2) %prog data/130320/130320-*.CSV\n'
    usage += '  Create plots:\n'
    usage += '    Ex3) %prog data/rootfile/130320-132322.root\n'
    usage += '    Ex4) %prog -s data/rootfile/130508-1*.root\n'

    
    parse = OptionParser(usage)
    parse.add_option('-b',
                     action = 'store_true',
                     dest   = 'batch',
                     help   = 'set ROOT batch mode')
    parse.add_option('-s',
                     action = 'store_true',
                     dest   = 'save',
                     help   = 'save canvases in png')
    parse.add_option('-w',
                     dest = 'window',
                     help = 'specify window#')
    parse.set_defaults( batch = False,
                        save  = False,
                        window = None)
    (options, args) = parse.parse_args()

    print 'options : {0}'.format(options)
    print 'args : {0}'.format(args)

    if len(args) < 1:
        parse.error('Wrong number of arguments ({0}).' % len(args))
        
    if options.batch:
        sys.stdout.write('Set ROOT batch mode.\n')
        gROOT.SetBatch(1)

    log = HvLog()
    
    for ifn in args:
        log.read(ifn)
    log.process()
    if options.window:
        log.draw_graph_win(options.window)
    else:
        log.draw_graph(options.save)
    print '-' * 50
    
