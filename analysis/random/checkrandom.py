#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import random
import math

fv_z = 1000    ## [mm]
fv_r = 400     ## [mm]

def mizuche_now(nloop):
    ofn = "mizuche_now.txt"
    with open(ofn, 'w') as fout:
        for i in range(nloop):
            z0 = (random.uniform(0, 1) - 0.5) * fv_z
            r0 = math.sqrt(random.uniform(0, 1))
            a0 = random.uniform(0, 1) * 2 * math.pi
            c0 = math.cos(a0)
            s0 = math.sin(a0)
            x0 = c0 * r0 * fv_r
            y0 = s0 * r0 * fv_r
            #print z0, r0, a0, x0, y0
            fout.write('%f\t%f\t%f\t%f\t%f\n' % (z0, r0, a0, x0, y0))

def mizuche_new(nloop):
    ofn = "mizuche_new.txt"
    with open(ofn, 'w') as fout:
        for i in range(nloop):
            z0 = (random.uniform(0, 1) - 0.5) * fv_z
            #r0 = math.sqrt(random.uniform(0, 1))
            r0 = random.uniform(0, 1)
            a0 = random.uniform(0, 1) * 2 * math.pi
            c0 = math.cos(a0)
            s0 = math.sin(a0)
            x0 = c0 * r0 * fv_r
            y0 = s0 * r0 * fv_r
            #print z0, r0, a0, x0, y0
            fout.write('%f\t%f\t%f\t%f\t%f\n' % (z0, r0, a0, x0, y0))
    
## __________________________________________________
if __name__ == '__main__':
    mizuche_now(50000)
    mizuche_new(50000)
    

    

