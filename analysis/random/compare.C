Int_t compare()
{
    TString ifn1 = "mizuche_now.txt";
    TString ifn2 = "mizuche_new.txt";

    TTree *t1 = new TTree("t1", ifn1.Data());
    TTree *t2 = new TTree("t2", ifn2.Data());

    t1->ReadFile(ifn1.Data(), "z0/D:r0/D:a0/D:x0/D:y0/D");
    t2->ReadFile(ifn2.Data(), "z0/D:r0/D:a0/D:x0/D:y0/D");

    t1->SetLineColor(2);
    t2->SetLineColor(3);
    
    TCanvas *c1 = new TCanvas("c1", "c1", 1200, 800);
    c1->Divide(3, 2);
    c1->cd(1);
    t1->Draw("x0");
    t2->Draw("x0", "", "same");

    c1->cd(2);
    t1->Draw("y0");
    t2->Draw("y0", "", "same");

    c1->cd(3);
    t1->Draw("z0");
    t2->Draw("z0", "", "same");

    c1->cd(4);
    t1->Draw("a0");
    t2->Draw("a0", "", "same");

    c1->cd(5);
    t1->Draw("r0");
    t2->Draw("r0", "", "same");
    
    c1->cd(6);
    t1->Draw("sqrt(x0**2 + y0**2)");
    t2->Draw("sqrt(x0**2 + y0**2)", "", "same");

    TCanvas *c2 = new TCanvas("c2", "c2", 1200, 800);
    c2->Divide(3, 2);
    c2->cd(1);
    t1->Draw("x0:y0", "", "colz");
    c2->cd(2);
    t1->Draw("x0:z0", "", "colz");
    c2->cd(3);
    t1->Draw("y0:z0", "", "colz");

    c2->cd(4);
    t2->Draw("x0:y0", "", "colz");
    c2->cd(5);
    t2->Draw("x0:z0", "", "colz");
    c2->cd(6);
    t2->Draw("y0:z0", "", "colz");

    TCanvas *c3 = new TCanvas("c3", "c3", 800, 800);
    c3->Divide(2, 2);
    c3->cd(1);
    t1->Draw("sqrt(x0**2 + y0**2):z0", "", "colz");
    c3->cd(2);
    t1->Draw("(x0**2 + y0**2):z0", "", "colz");
    
    c3->cd(3);
    t2->Draw("sqrt(x0**2 + y0**2):z0", "", "colz");
    c3->cd(4);
    t2->Draw("(x0**2 + y0**2):z0", "", "colz");

    printf("Saving TCanvases\n");
    c1->SaveAs("c1.png");
    c2->SaveAs("c2.png");
    c3->SaveAs("c3.png");
    
    return 0;
}
