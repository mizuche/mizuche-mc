#! /usr/bin/env python
#! -*- coding:utf-8 -*-

import sys
import os
import time
import datetime

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(name)-12s %(module)s.%(funcName)-20s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='debug.log',
                    filemode='w')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(module)s.%(funcName)-20s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

import argparse
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True

from ROOT import gROOT, gClient
from ROOT import TFile, TTree, TChain
from ROOT import TH1D, TH2D
from ROOT import TGraph, TMultiGraph
from ROOT import TCanvas, TLegend

## __________________________________________________
class Config(object):
    ClassName = 'Ondotori'
    TreeName = 'tori'
    BranchDescriptor = 'bird/I:utime/I:temp/D:humi/D:pres/D'
    Nbirds = 4
    ## range
    Temp = (0, 30, 30)
    Humi = (0, 100, 100)
    Pres = (900, 1100, 200)
    Duration = 3600
    dispw = gClient.GetDisplayWidth()
    disph = gClient.GetDisplayHeight()
    Disp  = (dispw, disph)
    Timeformat = '#splitline{%m/%d}{%H:%M}'
    PlotDir = 'data/plots'

## __________________________________________________
class Ondotori(object):
    '''
    +  Create ROOT file from BIRD[1-4].txt.
      - bird1 : [degC, RH, hPa] : above ATM
      - bird2 : [degC, degC]    : on top of buffer tank
      - bird3 : [degC, RH, hPa] : beside Mizuche
      - bird4 : [degC, RH]      : below ATM
    + Draw plots
    '''

    ## __________________________________________________
    def __init__(self):
        self.chain = TChain(Config.TreeName)
        self.logger = logging.getLogger('Ondotori')
        
    ## __________________________________________________
    def __str__(self):
        '''
        print information of files added to TChain.
        '''
        ch = self.chain
        s = '[print]\tTreeName : {0}\n'.format(ch.GetName())
        s += '[print]\tNtrees   : {0}\n'.format(ch.GetNtrees())
        s += '[print]\tEntries  : {0}\n'.format(ch.GetEntries())
        return s
    
    ## __________________________________________________
    def init_th1d(self, name, title, xtuple, nloop, timedisplay=0):
        '''
        Initializer for N TH1D histgrams.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        xmin, xmax, xbin = xtuple
        hists = []
        for i in range(nloop):
            color = i % 8 + 2
            h = TH1D(name % i, title % i, xbin, xmin, xmax)
            h.SetStats(0)
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            h.SetFillColor(color)
            h.GetXaxis().SetTimeFormat(Config.timeformat)
            h.GetXaxis().SetTimeOffset(0, 'gmt')
            h.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                h.GetXaxis().SetLabelOffset(0.015)
            hists.append(h)
        return hists

    ## __________________________________________________
    def init_th2d(self, name, title, xtuple, ytuple, nloop, timedisplay=0):
        '''
        Initializer for N TH2D histgrams.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        xmin, xmax, xbin = xtuple
        ymin, ymax, ybin = ytuple
        hists = []
        for i in range(nloop):
            color = i % 8 + 2
            h = TH2D(name % i, title % i, xbin, xmin, xmax, ybin, ymin, ymax)
            h.SetStats(0)
            h.SetLineColor(color)
            h.SetMarkerColor(color)
            h.SetFillColor(color)
            h.GetXaxis().SetTimeFormat(Config.Timeformat)
            h.GetXaxis().SetTimeOffset(0, 'gmt')
            h.GetXaxis().SetTimeDisplay(timedisplay)
            if timedisplay == 1:
                h.GetXaxis().SetLabelOffset(0.015)
            hists.append(h)
        return hists

    ## __________________________________________________
    def init_tgraph(self, name, title, nloop, timedisplay=0):
        '''
        Initializer for N TGraph graphs.
        Format for name and title:
          name = 'hname%d'
          title = 'hname%d;x-title;y-title'
        '''
        graphs = []
        for i in range(nloop):
            color = i % 8 + 2
            g = TGraph(0)
            g.SetName(name % i)
            g.SetTitle(title % i)
            g.SetLineColor(color)
            g.SetMarkerColor(color)
            g.SetFillStyle(0)
            g.GetXaxis().SetTimeDisplay(timedisplay)
            g.GetXaxis().SetTimeOffset(0, 'gmt')
            g.GetXaxis().SetTimeFormat(Config.Timeformat)
            if timedisplay == 1:
                g.GetXaxis().SetLabelOffset(0.015)
            graphs.append(g)
        return graphs
    
    ## __________________________________________________
    def read(self, ifn):
        '''
        if input file is a text format : Convert it ROOT file.
        if input file is a ROOT format : Add TTree to TChain
        '''
        if not os.path.exists(ifn):
            self.logger.error('file "%s" does not exist. quit.' % ifn)
            sys.exit(-1)

        head, tail = os.path.split(ifn)
        root, ext = os.path.splitext(ifn)
        size = os.path.getsize(ifn) / 1e6
        start = datetime.datetime.now()
        if ext in ['.txt']:
            self.logger.info('Check\t: File "{0} ({1:5.2f} MB)" is a text file.'.format(ifn, size))
            self.create_rootfile(ifn)
        elif ext in ['.root']:
            self.logger.info('Check\t: File "{0} ({1:5.2f} MB)" is a ROOT file.'.format(ifn, size))
            self.read_rootfile(ifn)
        end = datetime.datetime.now()            
        self.logger.info('Done\t: {0} [sec]'.format(end - start))
        return
        
    ## __________________________________________________
    def create_rootfile(self, ifn):
        '''
        input filename : data/yymmdd/bird[1-4].txt
        output filename: data/rootfile2/ondotoriyymmdd_bird[1-4].root

        1. Read input file
        2. Sort data and write down to tmpfile.
        3. Read tmpfile using TTree::ReadFile()
        4. Write to ROOT file
        '''
        (path, bird) = os.path.split(ifn)
        (path, date) = os.path.split(path)
        birdnum = bird[4]
        ofn = 'ondotori{0}_{1}'.format(date, bird).replace('txt', 'root')
        ofn = os.path.join('data/rootfile2/', ofn)

        if os.path.exists(ofn):
            self.logger.warn('file "%s" already exists. skip.' % ofn)
            return 'Skip conversion'

        self.logger.info('read  : %s' % ifn)
        with open(ifn, 'r') as fin:
            lines = fin.readlines()
            
        with open('temp.txt', 'w') as fout:
            for line in lines[4:-1]:
                line = line.replace('" "', '"\t"')
                line = line.rstrip().split('\t')
                date = line[0].replace('\'', ':').replace('"', '')
                self.logger.debug('check date : %s', date)
                date = time.strptime(date, '%Y/%m/%d %H:%M:%S')
                utime = int(time.mktime(date))
                temp = line[2].replace('"', '')
                humi = line[3].replace('"', '')
                pres = line[4].replace('"', '')
                fout.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(birdnum, utime, temp, humi, pres))
                #sys.stdout.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(birdnum, utime, temp, humi, pres))
                
        tname = Config.TreeName
        bd = Config.BranchDescriptor
        self.logger.info('create TTree : "%s:%s", "%s"' % (tname, bird, bd))
        t = TTree(tname, bird)
        t.ReadFile('temp.txt', bd)
        self.logger.info('recreate : "%s"' % ofn)
        fout = TFile(ofn, 'recreate')
        self.logger.info('write to : "%s"' % ofn)
        t.Write()
        fout.Close()
        os.remove('temp.txt')
        return 'Conversion Finished'

    ## __________________________________________________
    def read_rootfile(self, ifn):
        '''
        Read ROOT file with TChain
        When reading a file:
        1. Check if the input file exists.
        2. Check if it is a ROOT file, (*.root).
        3. Assume plot-saving directory and check if it exists.
        4. Open ROOT file and hold it inside.
        '''
        ### Open ROOT file and get TTree
        self.logger.info('Read : %s' % ifn)
        self.chain.Add(ifn)
        return self.chain

    ## __________________________________________________
    def process(self):
        ch = self.chain
        nentries = ch.GetEntries()
        print nentries
        if not nentries == 0:
            self.process_rootfile()
        else:
            sys.exit(-1)
        return

    ## __________________________________________________
    def get_utime(self, chain):
        nentries = chain.GetEntries()
        chain.GetEntry(0)
        s_utime = e_utime = chain.utime
        for ientry in range(nentries):
            chain.GetEntry(ientry)
            t = chain.utime
            s_utime = min(s_utime, t)
            e_utime = max(e_utime, t)
            
        s_date = '{0:%Y%m%d}'.format(datetime.datetime.fromtimestamp(s_utime))
        e_date = '{0:%Y%m%d}'.format(datetime.datetime.fromtimestamp(e_utime))
        self.prefix = '{0}_{1}'.format(s_date, e_date)
        print 'Start : {0} ==> {1}'.format(s_utime, s_date)
        print 'End   : {0} ==> {1}'.format(e_utime, e_date)
        return s_utime, e_utime

    ## __________________________________________________
    def process_rootfile(self):
        ch = self.chain
        nentries = ch.GetEntries()

        ### get start / end of unixtime
        s_time, e_time = self.get_utime(ch)
        interval = Config.Duration
        s_time = (s_time / interval) * interval
        e_time = (e_time / interval + 1 ) * interval
        n_time = (e_time - s_time) / interval
        time = (s_time, e_time, n_time)
        
        ###
        hname = 'hTemp%d'
        htitle = 'hTemp%d;Date/Time;Temperature'
        hTemp = self.init_th2d(hname, htitle, time, Config.Temp, Config.Nbirds, timedisplay=1)
        ###
        hname = 'hHumi%d'
        htitle = 'hHumi%d;Date/Time;Humidity'
        hHumi = self.init_th2d(hname, htitle, time, Config.Humi, Config.Nbirds, timedisplay=1)
        ###
        hname = 'hPres%d'
        htitle = 'hPres%d;Date/Time;Pressure'
        hPres = self.init_th2d(hname, htitle, time, Config.Pres, Config.Nbirds, timedisplay=1)

        for i in range(Config.Nbirds):
            ch.Project(hTemp[i].GetName(), 'temp:utime', 'bird == {0}'.format(i+1))
            ch.Project(hHumi[i].GetName(), 'humi:utime', 'bird == {0}'.format(i+1))
            ch.Project(hPres[i].GetName(), 'pres:utime', 'bird == {0}'.format(i+1))

        
        ###
        gname = 'gTemp%d'
        gtitle ='gTemp%d'
        gTemps = self.init_tgraph(gname, gtitle, Config.Nbirds, timedisplay=1)
        ###
        gname = 'gHumi%d'
        gtitle ='gHumi%d'
        gHumis = self.init_tgraph(gname, gtitle, Config.Nbirds, timedisplay=1)
        ###
        gname = 'gPres%d'
        gtitle ='gPres%d'
        gPress = self.init_tgraph(gname, gtitle, Config.Nbirds, timedisplay=1)

        for ientry in range(nentries):
            ch.GetEntry(ientry)
            ibird = ch.bird - 1
            utime = ch.utime
            temp = ch.temp
            humi = ch.humi
            pres = ch.pres

            gtemp = gTemps[ibird]
            ghumi = gHumis[ibird]
            gpres = gPress[ibird]

            ### get number of points in TGraphs
            ntemp = gtemp.GetN()
            nhumi = ghumi.GetN()
            npres = gpres.GetN()

            ### set point to TGraph
            if temp > 0:
                gtemp.SetPoint(ntemp, utime, temp)
            if humi > 0:
                ghumi.SetPoint(nhumi, utime, humi)
            if 500 < pres < 2000:
                gpres.SetPoint(npres, utime, pres)
            
        self.hTemp = hTemp
        self.hHumi = hHumi
        self.hPres = hPres

        self.gTemps = gTemps
        self.gHumis = gHumis
        self.gPress = gPress

        return hTemp, hHumi, hPres

    ## __________________________________________________
    def draw_hist(self):
        hTemp = self.hTemp
        hHumi = self.hHumi
        hPres = self.hPres

        wx, wy = Config.Disp
        cname = 'cOndotori{0}'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'Ondotori{0}'.format(self.prefix))

        c = TCanvas(cname, ctitle, wx, wy)
        c.Divide(1, 3)
        c.cd(1)
        for i in range(len(hTemp)):
            gopt = 'box' if i == 0 else 'boxsame'
            hTemp[i].Draw(gopt)
        c.cd(1).BuildLegend(0.7, 0.1, 0.9, 0.5)
        c.cd(2)
        for i in range(len(hHumi)):
            gopt = 'box' if i == 0 else 'boxsame'
            hHumi[i].Draw(gopt)
        c.cd(2).BuildLegend(0.7, 0.1, 0.9, 0.5)            
        c.cd(3)
        for i in range(len(hPres)):
            gopt = 'box' if i == 0 else 'boxsame'
            hPres[i].Draw(gopt)
        c.cd(3).BuildLegend(0.7, 0.1, 0.9, 0.5)            
        c.Update()
        self.stop()
            
        return c

    ## __________________________________________________
    def draw_graph(self):
        gTemps = self.gTemps
        gHumis = self.gHumis
        gPress = self.gPress

        lTemp = TLegend(0.1, 0.7, 0.5, 0.9)
        lHumi = TLegend(0.1, 0.7, 0.5, 0.9)
        lPres = TLegend(0.1, 0.7, 0.5, 0.9)
        
        leg = [lTemp, lHumi, lPres]
        for l in leg:
            l.SetFillStyle(0)

        gtitle = self.prefix.replace('_', ' - ')
        mgTemp = TMultiGraph('mgTemp', 'Temp {0};Date;Temprature [degC]'.format(gtitle))
        mgHumi = TMultiGraph('mgHumi', 'Humi {0};Date;Humidity [%]'.format(gtitle))
        mgPres = TMultiGraph('mgPres', 'Pres {0};Date;AtmosPressure [Pa]'.format(gtitle))
        for g in gTemps:
            if g.GetN() > 0:
                mgTemp.Add(g, 'pl')
                lTemp.AddEntry(g, g.GetTitle(), 'lp')
        for g in gHumis:
            if g.GetN() > 0:
                mgHumi.Add(g, 'pl')
                lHumi.AddEntry(g, g.GetTitle(), 'lp')
        for g in gPress:
            if g.GetN() > 0:
                mgPres.Add(g, 'pl')
                lPres.AddEntry(g, g.GetTitle(), 'lp')

        wx, wy = Config.Disp
        cname = 'cOndotori{0}_temp'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'cOndotori{0}_temp'.format(self.prefix))
        c1 = TCanvas(cname, ctitle, wx, wy)
        mgTemp.Draw('ap')
        lTemp.Draw()
        c1.Update()
        mgTemp.GetXaxis().SetTimeDisplay(1)
        mgTemp.GetXaxis().SetTimeOffset(0, 'gmt')
        mgTemp.GetXaxis().SetTimeFormat(Config.Timeformat)
        
        cname = 'cOndotori{0}_humi'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'cOndotori{0}_humi'.format(self.prefix))
        c2 = TCanvas(cname, ctitle, wx, wy)
        mgHumi.Draw('ap')
        lHumi.Draw()
        c2.Update()
        mgHumi.GetXaxis().SetTimeDisplay(1)
        mgHumi.GetXaxis().SetTimeOffset(0, 'gmt')
        mgHumi.GetXaxis().SetTimeFormat(Config.Timeformat)

        cname = 'cOndotori{0}_pres'.format(self.prefix)
        ctitle = os.path.join(Config.PlotDir, 'cOndotori{0}_pres'.format(self.prefix))
        c3 = TCanvas(cname, ctitle, wx, wy)
        mgPres.Draw('ap')
        lPres.Draw()
        c3.Update()
        # if mgPres:
        #     mgPres.GetXaxis().SetTimeDisplay(1)
        #     mgPres.GetXaxis().SetTimeOffset(0, 'gmt')
        #     mgPres.GetXaxis().SetTimeFormat(Config.Timeformat)

        self.stop()

        c1.SaveAs('{0}.png'.format(c1.GetTitle()))
        c2.SaveAs('{0}.png'.format(c2.GetTitle()))
        c3.SaveAs('{0}.png'.format(c3.GetTitle()))

        self.stop()
        return


    ## __________________________________________________
    def stop(self):
        ans = raw_input('Press "q" or ".q" to Quit. > ')
        if ans in ['q', '.q']:
            sys.stderr.write('\t"%s" pressed. Bye Bye.\n' % ans)
            sys.exit(-1)
            
## __________________________________________________
if __name__ == '__main__':

    
    usage = '%(prog)s [options] [text or ROOT files, ...]'
    usage += '\n'
    usage += '  Create ROOT file from txt data\n'
    usage += '    Ex1) %(prog)s data/130430/bird1.txt\n'
    usage += '    Ex2) %(prog)s data/130430/bird*.txt\n'
    usage += '  Create plots from ROOT file\n'
    usage += '    Ex3) %(prog)s data/rootfile2/ondotori130430_bird1.root\n'
    usage += '    Ex4) %(prog)s data/rootfile2/ondotori130430_bird*.root\n'
    usage += '\n'
    
    

    desc = '(1) Create ROOT file if input file is a TEXT data\n'
    desc += '(2) Create plots if input file is a ROOT file'

    epi = 'examples:\n'
    epi += '(1) Create ROOT file from txt data\n'
    epi += '    Ex1) %(prog)s data/130430/bird1.txt\n'
    epi += '    Ex2) %(prog)s data/130430/bird*.txt\n'
    epi += '(2) Create plots from ROOT file\n'
    epi += '    Ex3) %(prog)s data/rootfile2/ondotori130430_bird1.root\n'
    epi += '    Ex4) %(prog)s data/rootfile2/ondotori130430_bird*.root\n'
    epi += '\n'

    epi += 'about birds:\n'
    epi += '  bird# | ch1  | ch2  | ch3 | placed \n'
    epi += ' -------+------+------+-----+----------------\n'
    epi += '    1   | degC | RH   | hPa | above TKO\n'
    epi += '    2   | degC | degC | -   | buffer tank\n'
    epi += '    3   | degC | RH   | hPa | near Mizuche\n'
    epi += '    4   | degC | RH   | -   | below TKO\n'
    epi += '\n'

    epi += 'others:\n'
    epi += '  + log messages are written into debug.log using logging module.\n'
    epi += '  + plots are stored as data/plots/cOndotori[StartDate]_[EndData]_[type].png.\n'
    epi += '\n'
    
    parser = argparse.ArgumentParser(description=desc,
                                     epilog=epi,
                                     #formatter_class=argparse.ArgumentDefaultsHelpFormatter)
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('ifn',
                        type=str,
                        nargs='+',
                        help='input filename(s)')
    parser.add_argument('-b',
                        dest='batch',
                        action='store_true',
                        help='set ROOT batch mode')
    parser.add_argument('-s',
                        dest='save',
                        action='store_true',
                        help='save canvases in png')
    parser.add_argument('-n',
                        dest='mkdir',
                        action='store_true',
                        help='auto mkdir plot-saving directory')
    parser.set_defaults(batch=False,
                        save=False,
                        mkdir=False)
    
    args = parser.parse_args()

    tori = Ondotori()

    tori.logger.debug('ifn    : {0}'.format(args.ifn))
    tori.logger.debug('batch  : {0}'.format(args.batch))
    tori.logger.debug('save   : {0}'.format(args.save))
    tori.logger.debug('mkdir  : {0}'.format(args.mkdir))
    
    if args.batch:
        tori.logger.info('Set ROOT batch mode.\n')
        gROOT.SetBatch(1)

    tori.save  = args.save
    tori.mkdir = args.mkdir
    for ifn in args.ifn:
        tori.read(ifn)
    tori.process()
    # tori.draw_hist()
    tori.draw_graph()
