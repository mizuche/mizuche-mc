#! /usr/bin/env python
# -*- coding:utf-8 -*-

# Based on $ROOTSYS/share/doc/tutorials/pyroot/staff.py
# example of macro to read data from an ascii file and
# create a root file with a Tree.
#
# NOTE: comparing the results of this macro with those of staff.C, you'll
# notice that the resultant file is a couple of bytes smaller, because the
# code below strips all white-spaces, whereas the .C version does not.

import os, sys
from ROOT import gROOT, TTree, TFile
import time
from optparse import OptionParser


### a C/C++ structure is required, to allow memory based access
gROOT.ProcessLine(
"struct Ondotori_t {\
   Int_t        bird;\
   Int_t        utime;\
   Int_t        date;\
   Float_t      time;\
   Float_t      temp;\
   Float_t      humi;\
   Float_t      pres;\
};" );

from ROOT import Ondotori_t
# gROOT.ProcessLine(
# "struct Ondotori_t {\
#    Int_t         bird;\
#    Int_t         utime;\
#    Int_t         date;\
#    Double_t      time;\
#    Double_t      temp;\
#    Double_t      humi;\
#    Double_t      pres;\
# };" );

class OndoTori(object):
   ## __________________________________________________
   def __init__(self, options):
      self.options = options
      self.dirs  = []
      self.birds = []
      self.dates = []
      self.initDirs()
      self.initBirds()
      self.initDates()

   ## __________________________________________________
   def __str__(self):
      di = 'Dirs : ' + (', ').join(self.dirs)
      bi = 'Birds: ' + (', ').join(self.birds)
      da = 'Dates: ' + (', ').join(self.dates)
      s = [di, bi]
      s = ('\n').join(s)
      return s

   ## __________________________________________________
   def addDirs(self, dirname):
      self.dirs.append(dirname)

   ## ___________________________________________________________________________
   def initDirs(self):
      dirs = ['120312', '120411', '120512', '120526', '120530', '120706']
      for dir in dirs:
         self.addDirs(dir)

   ## __________________________________________________
   def addBird(self, bird):
      self.birds.append(bird)

   ## __________________________________________________
   def initBirds(self):
      birds = ['1', '3', '4']
      for bird in birds:
         self.addBird(bird)
      return birds   

   ## __________________________________________________
   def addDate(self, date):
      self.dates.append(date)

   ## __________________________________________________
   def initDates(self):
      year    = '2012'
      monthes = range(3, 8)
      dates   = range(1, 32)
      for month in monthes:
         for date in dates:
            d = '%s%02d%02d' % (year, month, date)
            if month in [2, 4, 6, 9, 11] and date == 31:
               pass
            else:
               self.addDate(d)
      
   ## ___________________________________________________________________________
   def unixTime(self, localtime, format = '%Y/%m/%d %H:%M:%S'):
      l = u'%s' % localtime
      t = time.strptime(l, format)
      u = int( time.mktime(t) )
      return u

   ## ___________________________________________________________________________
   def readFile(self, ifn):
      if not os.path.exists(ifn):
         print 'Error: File "%s" does not exist.' % ifn
         return
      else:
         print 'Reading "%s".' % ifn
         with open(ifn, 'r') as fin:
            lines = fin.readlines()

         read_data = [];
         for line in lines:
            line = line.rstrip()            # 行末の改行コードを除去
            line = line.replace("'", ':')   # 「分'秒」の区切りを「分:秒」に置換
            line = line.replace('""', '0')  # 空白の項目を'0'で置換
            line = line.replace('"', '')    # 要らない"を消す
            line = line.split()             # 空白(" ", "\t"など)で区切る
            
            if len(line) == 11:
               read_data.append(line)

      return read_data

   ## ___________________________________________________________________________
   def mergeFiles(self):

      self.merged_data = []
      #dirs  = self.initDirs()
      #birds = self.initBirds()
      for dir in self.dirs:
         for bird in self.birds:
            ifn = 'OndotoriData/%s/bird%s.txt' % (dir, bird)
            read_data = self.readFile(ifn)

            if isinstance(read_data, list):
               for data in read_data:
                  datum = [bird] + data
                  self.merged_data.append(datum)

      return self.merged_data

   ## ___________________________________________________________________________
   def fillTree(self, date):
      ofn = 'OndotoriData/rootfile/ondotori' + str(date) + '.root'
      print '\t----------'
      print '\tOptions   : %s' % self.options
      overwrite = self.options.overwrite
      print (not overwrite)
      
      print '\t----------'
      print '\tOverwrite : %s' % overwrite, '\t!Overwrite : %s' % (not self.options.overwrite)
      print '\tPathExist : %s' % os.path.exists(ofn), '\t!PathExist : %s' % (not os.path.exists(ofn))
      print '\t----------'
      print '\tOverwrite and PathExist  : %s and %s => %s' % (overwrite, os.path.exists(ofn), os.path.exists(ofn) and overwrite)
      print '\t!Overwrite and PathExist : %s and %s => %s' % (overwrite, os.path.exists(ofn), os.path.exists(ofn))
      print '\t----------'
      print '\tT and T : %s' % (True and True)  , ' \tT or T : %s' % (True or True)
      print '\tT and F : %s' % (True and False) , '\tT or F : %s' % (True or False)
      print '\tF and T : %s' % (False and True) , '\tF or T : %s' % (False or True)
      print '\tF and F : %s' % (False and False), '\tF or F : %s' % (False or False)
      print '\t----------'

      if os.path.exists(ofn):
         if overwrite == False:
            print '\t%s already exists. Skipping.' % ofn
            return
         else:
            print 'Warn:\tOverwriting "%s".' % ofn
      else:
         print 'Creating "%s".' % ofn

      ondotori = Ondotori_t()
      tree = TTree( 'tori', 'OndotoriData during Mizuche data taking period' )
      # tree.Branch( 'bird', AddressOf( ondotori, 'bird' ), 'bird/I' )
      # tree.Branch( 'time', AddressOf( ondotori, 'time' ), 'time/D' )
      # tree.Branch( 'utime', AddressOf( ondotori, 'utime' ), 'utime/I' )
      # tree.Branch( 'temp', AddressOf( ondotori, 'temp' ), 'temp/D' )
      # tree.Branch( 'humi', AddressOf( ondotori, 'humi' ), 'humi/D' )
      # tree.Branch( 'pres', AddressOf( ondotori, 'pres' ), 'pres/D' )
      bd = 'bird/I:utime/I:date/I:time/F:temp/F:humi/F:pres/F'
      # bd = 'bird/I:utime/I:date/I:time/D:temp/D:humi/D:pres/D'
      tree.Branch('ondotori', ondotori, bd)
      #   tree.Branch('ondotori', ondotori, 'bird/I:utime/I:date/I:time/F:temp/F:humi/F:pres/F')
      #   tree.Branch( 'date', AddressOf( ondotori, 'date' ), 'date/C' )
      # note that the branches 'date' cannot be on the first branch
      
      ondotori.time = 0
      ondotori.temp = 0
      ondotori.humi = 0
      ondotori.pres = 0
      isdate = False

      for data in self.merged_data:
         ondotori.date = int(data[1].replace('/', ''))
         if ondotori.date == int(date):
            isdate = True
            # print type(data[4]), data[4], float(data[4])
            ondotori.bird  = int(data[0])
            ondotori.utime = self.unixTime(data[1] + ' ' + data[2])
            ondotori.time  = float(data[3])
            ondotori.temp  = float(data[4])
            ondotori.humi  = float(data[5])
            ondotori.pres  = float(data[6])
            # print 'bird:%d, utime:%e, date:%s, time:%f, temp:%f, humi:%f, pres:%f' % (ondotori.bird, ondotori.utime, ondotori.date, ondotori.time, ondotori.temp, ondotori.humi, ondotori.pres)
            tree.Fill()
            print '\tisdate : %s' % isdate
            if isdate == True:
               print '\tRecreating "%s".' % ofn
               fout = TFile( ofn, 'RECREATE' )
               tree.Write()
               fout.Close()
               return
            else:
               print 'Error:\tDate "%s" does not exist.' % str(date)
               return


   ## __________________________________________________
   def fillTrees(self, overwrite=False):
      for date in self.dates:
         self.fillTree(date, overwrite)

   ## __________________________________________________
   def readTree(self, date):
      ifn = 'OndotoriData/rootfile/ondotori' + str(date) + '.root'

   ## __________________________________________________
   def readTrees(self):
      for date in self.dates:
         self.readTree(date)

## ___________________________________________________________________________
if __name__ == '__main__':

   usage = 'usage: %prog [option]'
   parser = OptionParser(usage)
   parser.add_option('-w', '--overwrite',
                     action='store_true',
                     dest  ='overwrite',
                     default='False',
                     help  ='overwrite existing files')
   (options, args) = parser.parse_args()

   ot = OndoTori(options)
   ot.mergeFiles()
   #ot.fillTrees()
   ot.fillTree(20120705)
   #ot.readTrees()

