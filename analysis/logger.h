// -*- mode : c++ -*-

//#define DEBUG

#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

const int LOG_I = 0;    // information
const int LOG_W = 1;    // warning
const int LOG_E = 2;    // error
const int LOG_D = 3;    // debug

#define LogI(...) Log(stderr, LOG_I, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define LogW(...) Log(stderr, LOG_W, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define LogE(...) Log(stderr, LOG_E, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define LogD(...) Log(stdout, LOG_D, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)

class Logger
{
private:
    char fDebug[100];
    char fLog[100];
    char fError[100];
    char fInfo[100];
public:
    //Logger(const char* ofn);
    Logger();
    virtual ~Logger();
    int SetLogFile(const char* ofn);
    int SetDebugFile(const char* ofn);
    int SetErrorFile(const char* ofn);
    int SetInfoFile(const char* ofn);    
    const char* GetLogFile() { return fLog; }
    const char* GetDebugFile() { return fDebug; }
    const char* GetErrorFile() { return fError; }
    const char* GetInfoFile()  { return fInfo; }    

    void Log(FILE *fp, const int &logType, const char *fileName,
             const int &lineNo,  const char* funcName,
             const char *format, ...);
};

// __________________________________________________
Logger::Logger()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif
    SetLogFile("log.txt");
    SetDebugFile("debug.txt");
    SetErrorFile("error.txt");
    SetInfoFile("info.txt");    
}

// __________________________________________________
Logger::~Logger()
{
#ifdef DEBUG
    LogD(__PRETTY_FUNCTION__);
#endif

}

// __________________________________________________
int Logger::SetLogFile(const char* ofn)
{
    sprintf(fLog, "%s", ofn);
#ifdef DEBUG
    LogD("SetLogFile('%s')", GetLogFile());
#endif
    return 0;
}

// __________________________________________________
int Logger::SetDebugFile(const char* ofn)
{
    sprintf(fDebug, "%s", ofn);    
#ifdef DEBUG
    LogD("SetLogFile('%s')", GetDebugFile());
#endif
    return 0;
}

// __________________________________________________
int Logger::SetErrorFile(const char* ofn)
{
    sprintf(fError, "%s", ofn);    
#ifdef DEBUG
    LogD("SetErrorFile('%s')", GetErrorFile());
#endif
    return 0;
}

// __________________________________________________
int Logger::SetInfoFile(const char* ofn)
{
    sprintf(fInfo, "%s", ofn);    
#ifdef DEBUG
    LogD("SetInfoFile('%s')", GetInfoFile());
#endif
    return 0;
}

// __________________________________________________
void Logger::Log(FILE *fp, const int &logType, const char *fileName,
                 const int &lineNo,  const char* funcName,
                 const char *format, ...)
{
    // @param   fp          (I) pointer to FILE handler
    // @param   logType     (I) log type
    // @param   fileName    (I) filenamek
    // @param   lineNo      (I) line number
    // @param   format      (I) format
    // @param   ...         (I) variable arguments
    // @return  None

    bool kInfo = kFALSE;
    
    va_list     va;
    va_start(va, format);

    // Print Log Type
    switch (logType) {
    case LOG_I:
        fprintf(fp, "INFO:\t");
        kInfo = kTRUE;
        break;
    case LOG_W:
        fprintf(fp, "WARN:\t");
        break;
    case LOG_E:
        fprintf(fp, "ERROR:\t");
        break;
    case LOG_D:
        fprintf(fp, "DEBUG:\t");
        break;
    default:
        fprintf(fp, "%d:\t", logType);
        break;
    }

    if (kInfo) {
       fprintf(fp, "[%s]\t", funcName);
    }
    else {
        // Get current time
        time_t now;
        now = time(NULL);
        struct tm *date;
        char str[256];
        now = time(NULL);
        date = localtime(&now);
        strftime(str, 255, "%Y/%m/%d %H:%M:%S", date);
        fprintf(fp, "%s\t", str);
        // Print filename and line no.
        fprintf(fp, "[%s: L%4d: %s]\t", fileName, lineNo, funcName);
    }
    // Print rest information
    vfprintf(fp, format, va);
    fprintf(fp, "\n");
    
    va_end(va);
}

#endif
