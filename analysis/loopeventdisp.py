#! /usr/bin/env python
# -*- coding:utf-8 -*-
import os, sys

start = 0001
for num in range(start, start+300):
    plotdir = 'plots/2012May/EventDisplay'
    pdffile = 'EventDisp_R043%04d.pdf' % (num)
    ofn = plotdir + pdffile
    
    datadir = 'exp/2012May/'
    rootfile  = 'upk_run043%04d.root' % (num)
    ifn = datadir + rootfile

    if not os.path.exists(ifn):
        print 'File %s does not exist.' % ifn
    elif os.path.exists(ofn):
        print 'File %s already exists. Skipping.' % ofn
    else:
        com = "./eventdisp -t upk -r %d %s" % (num, ifn)
        print com
        sys.stdout.flush()
        os.system(com)
    
