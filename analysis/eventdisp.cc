
#include <TH1.h>
#include <TH2.h>
#include <TVector3.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TObject.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TROOT.h>
#include <TSystem.h>

#include "T2KWCData.h"
#include "T2KWCUnpack.h"
#include "T2KWCEvtDisp.h"

#define BATCH
const int pe_threshold = 2;

TString ROOTstyle = "../mizudaq/process/mizu_online_style.C";
TString savepath = "plots/2012May/EventDisplay/";

// __________________________________________________
Int_t Usage()
{
    fprintf(stderr, "Usage   : ./eventdisp [htpmr] file1[, file2, ...]\n");
    fprintf(stderr, "Options\n");
    fprintf(stderr, "\t-h    : Show help\n");
    fprintf(stderr, "\t-t    : specify input tree name [default = 'upk']\n");
    fprintf(stderr, "\t-p    : print event display     [default = False]\n");
    fprintf(stderr, "\t-m    : read MC files           [default = False]\n");
    fprintf(stderr, "\t-r    : specify run#\n");
    return 0;
}

// __________________________________________________
int main(int argc,char *argv[])
{
     
#ifndef BATCH 
     TROOT root("GUI","GUI");
     TApplication theApp("App",0,0);
#endif

     char tree_name[256] = "upk";
     bool print_flag = false;
     bool is_beam = false;
     Int_t runnum = 0;
     int c=-1;
     while ((c = getopt(argc, argv, "hpt:r:")) != -1) {
          switch(c){
          case 'p':
               print_flag = true;
               break;
          case 't':
               sprintf(tree_name, "%s", optarg);
               break;
          case 'r':
              runnum = atoi(optarg);
               break;               
          case 'h':
              Usage();
              exit(1);
          default:
              fprintf(stderr, "Error:\tWrong options\n");
              Usage();
               exit(1);
          }
     }
     argc-=optind;
     argv+=optind;

     if (argc < 1) {
         fprintf(stderr, "Error:\tToo few arguments.\n");
         Usage();
         exit(-1);
     }
     
     if( strcmp(tree_name, "tesko") == 0 ) {
         fprintf(stderr, "Error:\tNO input tree name specified.\n");
         fprintf(stderr, "\tUse '-t' option and retry.\n");
          exit(1);  
     }
     
     if( strcmp(tree_name, "upk") == 0 ) {
         fprintf(stderr, "Read Upk data.\n");
         is_beam = true;
     }
     else {
         fprintf(stderr, "Unknown tree name : '%s'.\n", tree_name);
     }
 
     ////
     //
     TCanvas *canvas    = new TCanvas("canvas","canvas",1350,600);
     T2KWCEvtDisp *disp = new T2KWCEvtDisp();
     disp->Init( canvas );

     gROOT->Macro( ROOTstyle.Data() );

     TChain *fChain = new TChain(tree_name, tree_name);
     // chain trees for analysis
     FileStat_t info;
     for (int i = 0; i < argc; i++) {
         if (gSystem->GetPathInfo(argv[i], info) != 0) {
             fprintf(stderr, "Error:\tFile '%s' does not exist.\n", argv[i]);
             exit(1);
         } else {
             fChain->Add(argv[i]);
         }
     }

     if( fChain->GetNtrees()==0 ) {
         fprintf(stderr, "NO trees in this chain. Quit.\n");
         exit(1);
     }

     // set branch address
     TBranch     *EvtBr   = NULL;
     T2KWCEvent  *evt  = NULL;
     T2KWCUnpack *upk = NULL;

     if(is_beam) {
         EvtBr = fChain->GetBranch("upk.");
         upk = new T2KWCUnpack();
         EvtBr->SetAddress(&upk);
         fChain->SetBranchAddress("upk.", &upk);
     }
     else {
         EvtBr = fChain->GetBranch("mc.");
         evt = new T2KWCEvent();
         EvtBr->SetAddress(&evt);
         fChain->SetBranchAddress("mc.", &evt);
     }

     Long64_t nevt =fChain -> GetEntries();
     fprintf(stdout, "Total # of events : %d\n", (Int_t)nevt);
     if( nevt == 0 ) {
         fprintf(stderr, "Error:\tNO entries in this chain.\n");
         exit(1);
     }
     
     // start 
     Long64_t fN=0;
     TString buff;
     
     TString plotname;
     plotname.Form("%sEventDisp_R%07d.pdf", savepath.Data(), runnum );
     
     canvas->Print(plotname + "[", "pdf");
     
     while( 1 ) {
         //
          if(is_beam) upk->Clear();
          else evt->Clear();
          disp->Clear();

#ifndef BATCH
          std::cout << "\n\nSelect Entry# (select minu# to end):";
          std::cin >> fN;
#endif

          if( fN>=nevt ) {
               std::cout << "Too large entry# against total entries" << std::endl;
               break;
          }
          else if ( fN<0 ) {
               std::cout << "End!" << std::endl;
               exit(1);
          }

          fChain->GetEntry( fN );
          if(is_beam) {
//               std::cout << "Runmode:" << upk->RunMode() << std::endl;
               if(upk->GetRunmode()==1) {
/*
  if( upk->RunMode()==1 && 
  upk->GONG()==((upk->TRGNum()&0xffff)+1) ) {
*/
                    disp->InputData( upk );
                    if (upk->GetAtmNhits() > 0) {
                        printf("[%5d]\t# of hits: %d", (int)fN, upk->GetAtmNhits() );
                        disp->Draw( canvas );
#ifdef BATCH
//                        canvas->Print(Form("plots/2012Mar/EventDisp/tmp/EventDisp%05d.png", (int)fN), "png");
#endif
                        canvas->Print(plotname, "pdf");
                    }
               }
          }
          else {
//               disp->InputData( evt );
//               disp->Draw( canvas );
          }
 

          if( print_flag==true ) {
               buff.Form("evtdisp_ss_%ld.pdf",(long int)fN);
               canvas->SaveAs(buff);
          }
 
#ifdef BATCH
          fN++;
#endif

     } // end of loop

     canvas->Print(plotname + "]", "pdf");
#ifndef BATCH
     theApp.Run();
#endif

    return 0;
}
